<?php 

include_once APPPATH.'third_party\mpdf\mpdf\src\mpdf.php';

use Endroid\QrCode\QrCode;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
 

class MAIN_Controller extends CI_Controller{ 
	
	protected $data = array();
	protected $sess = null;
	protected $is_secure = false;
	protected $breadcrumb = array();
	protected $cols = array();

	public $pdf;
	private $limit_count_file = 10;
	private $limit_count_img = 100;
	
	public function __construct(){
		parent::__construct();
		$this->sess = new Sys\Sess\SessionLoad();
		$this->load->helper('common_func_helper');
		$this->load->helper('page_generator_helper');
		$this->load->helper('session_helper');

		if(isset($_GET['docapv'])){
			$_SESSION['Is_DocumentApproval'] = true;
		}
		else{
			if(isset($_SESSION['Is_DocumentApproval'])){
				unset($_SESSION['Is_DocumentApproval']);
			}
		}		
	}	
	
	public function __destruct(){
		
	}

	protected function validate_module(){
		$module_access = $this->session->userdata('module-access')['all'];
		$selected_module = $this->uri->segment(2);
		foreach ($module_access as $module) {
			if($selected_module == $module['M_ControllerName']){
				return;
			}
		}
		$this->redirect(DOMAIN.'Err_Page');
	}
	
	protected function load_model($modelname){
		
		$this->load->helper('model_helper');				
		$model = generateModelName($modelname);
		if(!isset($this->$model)){
			$this->load->model($modelname, $model);
		}
		return $this->$model;
		
	}
	protected function load_error_page($filename){
		$content['header'] = $this->load->view('/templates/public_header', '', true);
		$content['footer'] = $this->load->view('/templates/public_footer', '', true);
		$content['content'] = $this->load->view('/'.$filename, $this->data, true);
		$this->load->view('/templates/main', $content);
	}
	
	protected function load_page_old($filename){		
		$filename = (substr($filename, 0, 1) == '/')? substr($filename, 1) : $filename;
		
		$content['header'] = '';
		$content['js'] = $this->load->view('/templates/javascript', '', true);
		$content['js_updater'] = $this->load->view('/templates/js_updater', '', true);
		$content['footer'] = '';
		$content['content'] = '';
		if($this->is_secure){
			if($this->sess->isLogin()){
				//$login = $this->getCurrentLogin($filename);
				$pages = $this->validatePageAccess($filename);
				$content['header'] = $this->load->view('/templates/'.$pages['header'], $pages['login'], true);
				$content['footer'] = $this->load->view('/templates/'.$pages['footer'], '', true);
				$content['content'] = $this->load->view('/'.$pages['content'], $this->data, true);
			}
			else{
				$this->redirect(DOMAIN.'user/login');
			}			
		}
		else{
			$content['header'] = $this->load->view('/templates/public_header', '', true);
			$content['footer'] = $this->load->view('/templates/public_footer', '', true);
			$content['content'] = $this->load->view($filename, $this->data, true);
		}
		$this->load->view('/templates/main_old', $content);		
	}	
	
	protected function load_page($filename){		
		$filename = (substr($filename, 0, 1) == '/')? substr($filename, 1) : $filename;
		
		$content['content'] = '';
		if($this->is_secure){
			if($this->sess->isLogin()){
				//Load access
				$this->data['user_access'] = getDefaultLocation();
				$content['content'] = $this->load->view($filename, $this->data, true);
				$content['breadcrumb'] = $this->breadcrumb;
				$this->load->view('/templates/main', $content);	
			}
			else{
				$this->redirect(DOMAIN.'user/login');
			}
			
		}
		else{
			$content['content'] = $this->load->view($filename, $this->data, true);
			$this->load->view('/templates/guest', $content);
		}				
	}	

	protected function load_report($filename){
		$filename = (substr($filename, 0, 1) == '/')? substr($filename, 1) : $filename;		
		$content['content'] = '';
		$content['content'] = $this->load->view($filename, $this->data, true);
		return $this->load->view('/templates/pdf_report', $content, true);
	}
	
	public function redirect($url, $permanent = false){
		
		header('Location: ' . $url, true, $permanent ? 301 : 302);		
		exit();
	}
	
	
	public function load_template($template){
		$this->load->view($template);
	}

	public function set_alert($type, $message){
		$_SESSION['alert'] = array(
								'type' => $type, 
								'message' => $message);
	}

	public function print_r($data){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}

	public function print_lastquery(){
		echo "<pre>";
		print_r($this->db->last_query());
		echo "</pre>";
		die();	
	}

	public function sql_hash($where){
		return 'CONVERT(NVARCHAR(32),HashBytes(\'MD5\', '. $where .'),2)';
	}

	public function getApprovalButtons($docNo
										, $docStatus
										, $cUserid
										, $defaultLoc
										, $table_name
										, $status_field
										, $document_field
										, $allow_posting = false
									){		

		$output = array();

		// Button Classification
		$btnSendapproval = '<button id="send-approval" type="button" class="btn btn-primary btn-outline" 
								data-toggle="tooltip" data-placement="bottom" title="Send Approval" data-doc-no="'.$docNo.'">
							<span class="fa fa-send"></span>
						 </button>';
		$btnCancelDocument = '<button id="cancelled" type="button" class="btn btn-danger btn-outline" 
								data-toggle="tooltip" data-placement="bottom" title="Cancel Document" data-doc-no="'.$docNo.'">
							<span class="glyphicon glyphicon-remove"></span>
						</button>';
		$btnApprove = '<button id="approve" type="button" class="btn btn-primary btn-outline" 
							data-toggle="tooltip" data-placement="bottom" title="Approve" data-doc-no="'.$docNo.'">
							<span class="fa fa-thumbs-o-up"></span>
						</button>';
		$btnReject = '<button id="reject" type="button" class="btn btn-danger btn-outline" 
						data-toggle="tooltip" data-placement="bottom" title="Reject" data-doc-no="'.$docNo.'">
							<span class="fa fa-thumbs-o-down"></span>
					   </button>';
		$btnReOpen = '<button id="reopen" type="button" class="btn btn-primary btn-outline" 
						data-toggle="tooltip" data-placement="bottom" title="Re Open" data-doc-no="'.$docNo.'">
							<span class="fa fa-folder-open"></span>
						</button>';
		$btnTrackDocument ='<button id="track-document" type="button" class="btn btn-success btn-outline" 
								data-toggle="tooltip" data-placement="bottom" title="Track Document" data-doc-no="'.$docNo.'">
									<span class="fa fa-search"></span>
							  </button>';
		$btnPost ='<button id="Post" type="button" class="btn btn-success btn-outline" 
								data-toggle="tooltip" data-placement="bottom" title="Post" data-doc-no="'.$docNo.'">
									<span class="fa fa-gear"></span>
							  </button>';							  

		switch (strtoupper($docStatus)) {
			case 'OPEN':
				$output = array($btnSendapproval,$btnCancelDocument,$btnTrackDocument);
				break;
			case 'PENDING':
				if($this->is_approver($docNo,$cUserid)){
					$output = array($btnApprove,$btnReject,$btnTrackDocument);
				}
				else{
					$output = array($btnTrackDocument);
				}
				break;
			case 'APPROVED':
				if($this->is_creator($docNo,$cUserid,$table_name, $status_field, $document_field)){
					if($allow_posting){
						$output = array($btnReOpen,$btnPost,$btnTrackDocument);
					}
					else{
						$output = array($btnReOpen,$btnTrackDocument);	
					}
					
				}
				else{
					$output = array($btnTrackDocument);
				}
				break;			
			default:
				$output = array($btnTrackDocument);
				break;
		}

		return $output;
	}

	public function getApplicantButtons($docno, $status, $access){

		$output = array();

		$btnHired = '';
		$btnRejected = '';
		$btnInterview = '';
		$btnReview = '';

		if(in_array("12", $access)){
			$btnHired = '<button id="hire-applicant" type="button" class="btn btn-primary btn-outline" style="margin-top: 5px"
								data-toggle="tooltip" data-placement="bottom" title="Hire Applicant" data-doc-no="'.$docno.'">
								<span class="fa fa-thumbs-o-up"></span>
							</button>';
		}

		if (in_array("4", $access)) {
			$btnRejected = '<button id="reject-applicant" type="button" class="btn btn-danger btn-outline" style="margin-top: 5px"
						data-toggle="tooltip" data-placement="bottom" title="Reject Applicant" data-doc-no="'.$docno.'">
							<span class="fa fa-thumbs-o-down"></span>
					   </button>';
		}

		if (in_array("11", $access)) {
			$btnInterview = '<button id="interview-applicant" type="button" class="btn btn-primary btn-outline" style="margin-top: 5px"
						data-toggle="tooltip" data-placement="bottom" title="Interviewed" data-doc-no="'.$docno.'">
							<span class="fa fa-comments-o"></span>
						</button>';
		}

		if (in_array("10", $access)) {
			$btnReview ='<button id="review-applicant" type="button" class="btn btn-success btn-outline" style="margin-top: 5px"
								data-toggle="tooltip" data-placement="bottom" title="Reviewed" data-doc-no="'.$docno.'">
									<span class="fa fa-pencil-square-o"></span>
							  </button>';
		}

		switch (strtoupper($status)) {
			case 'NEW':
				$output = array($btnReview, $btnRejected);
				break;
			case 'INTERVIEWED':
				$output = array($btnHired, $btnRejected);
				break;
			case 'REVIEWED':
				$output = array($btnInterview, $btnRejected);
				break;			
			default:
				break;
		}

		return $output;
	}

	private function is_approver($documentNo, $approver_user_id){
		$sql = 'SELECT dt.DT_FK_NSCode, dt.DT_DocNo, dt.DT_Approver, dt.DT_EntryNo, dt.DT_Unlimited , dt.DT_Sender 
                 FROM tblDocTracking dt LEFT OUTER JOIN tblUser u ON dt.DT_Approver = u.U_FK_Position_id 
                 WHERE (u.U_ID = \''.$approver_user_id.'\') 
                        AND (dt.DT_Status = \'Pending\') 
                        AND (dt.DT_DocNo = \''.$documentNo.'\') 
                 ORDER BY dt.DT_EntryNo DESC ';
        $result = $this->db->query($sql)->num_rows();  
        return ($result > 0) ? true : false ;
	}

	private function is_creator($documentNo, $user_id, $table_name, $status_field, $document_field){
		$sql = 'SELECT * 
				FROM '.$table_name.' 
				WHERE '.$status_field.' IN (\'Approved\',\'Open\') 
					and CreatedBy = \''.$user_id.'\' 
					and '.$document_field.' = \''.$documentNo.'\'';
		$result = $this->db->query($sql)->num_rows();
        return ($result > 0) ? true : false ;
	}

	public function validateUserRecordLock($userId, $documentNo, $moduleName, $tableName, $moduleLink, $moduleForm){
		// Validate Document if other user is editing it.
		$isUserRecordLock = $this->user_model->checkUserRecordLock($userId,$documentNo,$moduleName,$moduleLink);
		if($isUserRecordLock > 0){
			$this->set_alert('danger', '<strong><span class="fa fa-times-circle-o"></span> Error!</strong> You cannot edit this document <strong>'.$documentNo.'</strong>, other user is updating it.');
			header("location:".DOMAIN.$moduleLink);
		}
		else{
			// Insert User Record Lock
			$this->user_model->insertUserRecordLock($userId,$tableName,$documentNo,$moduleName);
			$this->load_page($moduleForm);
		}
	}

	public function generate($data){
		//Filling up unset optional data
		/*
		Format list
		- Letter
		- Legal
		- A3
		- A4
		*/

		$data['mode'] = (array_key_exists('mode', $data))? $data['mode'] : '';
		$data['format'] = (array_key_exists('format', $data))? $data['format'] : 'Letter';
		$data['font_size'] = (array_key_exists('font_size', $data))? $data['font_size'] : 10;
		$data['font'] = (array_key_exists('font', $data))? $data['font'] : 'helvetica';
		$data['margin_left'] = (array_key_exists('margin_left', $data))? $data['margin_left'] : 5;
		$data['margin_right'] = (array_key_exists('margin_right', $data))? $data['margin_right'] : 5;
		$data['margin_top'] = (array_key_exists('margin_top', $data))? $data['margin_top'] : 5;
		$data['margin_bottom'] = (array_key_exists('margin_bottom', $data))? $data['margin_bottom'] : 3;
		$data['margin_head'] = (array_key_exists('margin_head', $data))? $data['margin_head'] : 9;
		$data['margin_foot'] = (array_key_exists('margin_foot', $data))? $data['margin_foot'] : 9;
		$data['orientation'] = (array_key_exists('orientation', $data))? $data['orientation'] : 'P';
		$data['title'] = (array_key_exists('title', $data))? $data['title'] : 'Report';
        $data['is_create'] = (array_key_exists('is_create', $data))? $data['is_create'] : true;
        $data['is_qrcode'] = (array_key_exists('is_qrcode', $data))? $data['is_qrcode'] : false;
        $data['header'] = (array_key_exists('header', $data))? $data['header'] : '';
        $data['auto_top_margin'] = (array_key_exists('auto_top_margin', $data))? $data['auto_top_margin'] : false;


		$this->pdf = new \Mpdf\Mpdf([
									'mode' 				=> $data['mode'],
									'format' 			=> ($data['orientation'] == 'P')? $data['format'] : ((!$data['is_qrcode']) ? $data['format'].'-'.$data['orientation'] : $data['format']),
									'font_size' 		=> $data['font_size'],
									'font' 				=> $data['font'],
									'margin_left'		=> $data['margin_left'],
									'margin_right' 		=> $data['margin_right'],
									'margin_top' 		=> $data['margin_top'],
									'margin_bottom' 	=> $data['margin_bottom'],
									'margin_head' 		=> $data['margin_head'],
									'margin_foot' 		=> $data['margin_foot'],
									'orientation' 		=> $data['orientation'],
									'setAutoTopMargin' 	=> $data['auto_top_margin'],
									]);
		$this->pdf->SetTitle($data['title']);
		if(isset($data['footer'])){
			$this->pdf->SetFooter($data['footer']);
		}
		if(isset($data['html_header'])){
			$this->pdf->SetHeader($data['html_header']);
		}
		$this->pdf->SetHTMLHeader($data['header']);
		$this->pdf->SetAuthor('ARMATURE Inc.');
		$this->pdf->SetCreator('ARMATURE Inc.');
		// if($data['is_qrcode']){
		// 	$this->pdf->SetJS('this.print();');
		// }
		$this->pdf->WriteHTML($data['html']);
		
                if($data['is_create']){
                    echo json_encode(array("file_name"=>$this->generateFile($this->pdf)));
                }
                else{
                    $this->pdf->Output();
                }
		
	}

	private function generateFile($pdfObj){
		$dir = FCPATH.'/pdf/';

		$files = glob($dir . "*");
		$filecount = ($files)? count($files) : 0 ;
		if($filecount>=$this->limit_count_file){//Remove the oldest file
			
			$excess_count = $filecount - ($this->limit_count_file-1);
			array_multisort(
				array_map( 'filemtime', $files ),
				SORT_NUMERIC,
				SORT_ASC,
				$files
			);
			for($i=0; $i<$excess_count; $i++){
			 	unlink($files[$i]);
			}
		}

        //Close and output PDF document
        $pdf_name = bin2hex(openssl_random_pseudo_bytes(4)).'.pdf';
        $pdfObj->Output($dir.$pdf_name, 'F');
        return $pdf_name;
    }

    public function generateExcel($xlsObj){
		$dir = FCPATH.'xls/';

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		// Color Fill
		$spreadsheet->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00CCFF');
		$spreadsheet->getActiveSheet()->getStyle('A3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00CCFF');
		$spreadsheet->getActiveSheet()->getStyle('A4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00CCFF');
		$spreadsheet->getActiveSheet()->getStyle('A5')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFCC00');
		$spreadsheet->getActiveSheet()->getStyle('B5')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFCC00');
		$spreadsheet->getActiveSheet()->getStyle('C5')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFCC00');

		// Border
		$spreadsheet->getActiveSheet()->getStyle('A5')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
		$spreadsheet->getActiveSheet()->getStyle('A5')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
		$spreadsheet->getActiveSheet()->getStyle('A5')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
		$spreadsheet->getActiveSheet()->getStyle('A5')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$spreadsheet->getActiveSheet()->getStyle('B5')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
		$spreadsheet->getActiveSheet()->getStyle('B5')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
		$spreadsheet->getActiveSheet()->getStyle('B5')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		$spreadsheet->getActiveSheet()->getStyle('B5')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$spreadsheet->getActiveSheet()->getStyle('C5')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
		$spreadsheet->getActiveSheet()->getStyle('C5')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
		$spreadsheet->getActiveSheet()->getStyle('C5')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		$spreadsheet->getActiveSheet()->getStyle('C5')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

		$spreadsheet->getActiveSheet()->getStyle('A2')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		$spreadsheet->getActiveSheet()->getStyle('A2')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		$spreadsheet->getActiveSheet()->getStyle('A2')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		$spreadsheet->getActiveSheet()->getStyle('A2')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$spreadsheet->getActiveSheet()->getStyle('A3')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		$spreadsheet->getActiveSheet()->getStyle('A3')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		$spreadsheet->getActiveSheet()->getStyle('A3')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		$spreadsheet->getActiveSheet()->getStyle('A3')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$spreadsheet->getActiveSheet()->getStyle('A4')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		$spreadsheet->getActiveSheet()->getStyle('A4')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		$spreadsheet->getActiveSheet()->getStyle('A4')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		$spreadsheet->getActiveSheet()->getStyle('A4')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);


		foreach ($xlsObj as $key => $value) {
			$sheet->setCellValue($value['cell'], $value['value']);
		}

		// Number Format
		$spreadsheet->getActiveSheet()->getStyle('B6:B'.(count($xlsObj) + 5))->getNumberFormat()->setFormatCode('#,##0.00');
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

		$files = glob($dir . "*");
		$filecount = ($files)? count($files) : 0 ;
		if($filecount>=$this->limit_count_file){//Remove the oldest file
			
			$excess_count = $filecount - ($this->limit_count_file-1);
			array_multisort(
				array_map( 'filemtime', $files ),
				SORT_NUMERIC,
				SORT_ASC,
				$files
			);
			for($i=0; $i<$excess_count; $i++){
			 	unlink($files[$i]);
			}
		}

        //Close and output PDF document
        $writer = new Xlsx($spreadsheet);
        $xls_name = bin2hex(openssl_random_pseudo_bytes(4)).'.xlsx';
		$writer->save($dir.$xls_name);
        return $xls_name;
    }

    public function generateExcelPrintout($spreadsheet, $name){

    	$dir = FCPATH.'xls/';

    	$files = glob($dir . "*");
		$filecount = ($files)? count($files) : 0 ;
		if($filecount>=$this->limit_count_file){//Remove the oldest file
			
			$excess_count = $filecount - ($this->limit_count_file-1);
			array_multisort(
				array_map( 'filemtime', $files ),
				SORT_NUMERIC,
				SORT_ASC,
				$files
			);
			for($i=0; $i<$excess_count; $i++){
			 	unlink($files[$i]);
			}
		}

        //Close and output PDF document
        $writer = new Xlsx($spreadsheet);
        $xls_name = $name.'_'.Date('mdY_hiA').'.xlsx';
		$writer->save($dir.$xls_name);
        return $xls_name;
    }

    public function createQR($data){

		$qrcode = new QrCode();

		$qrcode->setSize($data['size']);
		$qrcode->setText($data['text']);
		

		// header('Content-Type: '.$qrcode->getContentType());
		return array("img_name"	=>	$this->generateQRCode($qrcode));
	}

	private function generateQRCode($qrcode){
		$dir = FCPATH.'/qrcode/';

		$files = glob($dir . "*");
		$filecount = ($files)? count($files) : 0 ;
		if($filecount>=$this->limit_count_img){//Remove the oldest file
			
			$excess_count = $filecount - ($this->limit_count_img-1);
			array_multisort(
				array_map( 'filemtime', $files ),
				SORT_NUMERIC,
				SORT_ASC,
				$files
			);
			for($i=0; $i<$excess_count; $i++){
			 	unlink($files[$i]);
			}
		}

		
		$img_name = bin2hex(openssl_random_pseudo_bytes(4)).'.png';
		$qrcode->writeFile($dir.$img_name);

		return $img_name;
	}

	public function silentPrint($fileName){
		$urlParts = parse_url($_SERVER['REQUEST_URI']);
 
		if (isset($urlParts['query'])) {
		    $rawQuery = $urlParts['query'];
		    parse_str($rawQuery, $qs);
		    if (isset($qs[WebClientPrint::CLIENT_PRINT_JOB])) {
		 
		        $useDefaultPrinter = ($qs['useDefaultPrinter'] === 'checked');
		        $printerName = urldecode($qs['printerName']);
		     
		        $filePath = $qs['imageFileName']; 
		 
		        //Create a ClientPrintJob obj that will be processed at the client side by the WCPP
		        $cpj = new ClientPrintJob();
		        //Create a PrintFile object with the PNG file
		        $cpj->printFile = new PrintFile($filePath, $fileName, null);
		        if ($useDefaultPrinter || $printerName === 'null'){
		            $cpj->clientPrinter = new DefaultPrinter();
		        }else{
		            $cpj->clientPrinter = new InstalledPrinter($printerName);
		        }
		 
		        //Send ClientPrintJob back to the client
		        ob_start();
		        ob_clean();
		        header('Content-type: application/octet-stream');
		        echo $cpj->sendToClient();
		        ob_end_flush();
		        exit();
		         
		    }
		}
	}

    public function getStoreName($SP_ID){
    	return $this->db->where('SP_ID',$SP_ID)->get('tblStoreProfile')->row_array()['SP_StoreName'];
    }

    public function getItemDescription($IM_Item_Id){
    	return $this->db->where('IM_Item_Id',$IM_Item_Id)->get('tblItem')->row_array()['IM_Sales_Desc'];
    }

    public function error_message($hasDocNoSeries,
    							  $isIDInput,
    							  $hasDetails, 
    							  $hasSubDetails, 
    							  $data,
    							  $doc_no = '',
    							  $module_id,
    							  $location = '',
    							  $header_str = '',
    							  $header_table = '',
    							  $type = '_DocNo'
    							){

    	$success = true;


    	try {

	    	if($hasDocNoSeries){

	    		if($data['todo'] == 'add'){

					$checkSeries = checkNoSeries($module_id, $location);

		    		if(empty($checkSeries)){
		    			throw new Exception (implode( getErrorMessage(4), ' , ' ));
		    		}

		    		if($doc_no == ''){
		    			throw new Exception (implode( getErrorMessage(3), ' , ' ));
		    		}

	    		}
	    		else{
	    			if($doc_no == ''){
		    			throw new Exception (implode( getErrorMessage(3), ' , ' ));
		    		}
	    		}

	    	}
	    	else{
	    		if($isIDInput){
	    			if( isset($data[$header_str.'_ID']) ){
	    				if($data['todo'] == 'add'){
		    				$checkID = checkExisitingID($header_str, $header_table, $data[$header_str.'_ID']);

		    				if(!empty($checkID)){
		    					throw new Exception (implode( getErrorMessage(6), ' , ' ));
		    				}
	    				}
	    			}
	    			elseif( isset($data[$header_str.'_Id']) ){
	    				if($data['todo'] == 'add'){
		    				$checkID = checkExisitingID($header_str, $header_table, $data[$header_str.'_Id']);

		    				if(!empty($checkID)){
		    					throw new Exception (implode( getErrorMessage(6), ' , ' ));
		    				}
	    				}
	    			}
	    			elseif( isset($data[$header_str.'_Code']) ){
		    			if($data['todo'] == 'add'){
		    				$checkID = checkExisitingCode($header_str, $header_table, $data[$header_str.'_Code']);

		    				if(!empty($checkID)){
		    					throw new Exception (implode( getErrorMessage(6), ' , ' ));
		    				}
	    				}
	    			}
	    		}
	    		else{
	    			if($data['todo'] == 'add'){
	    				$checkID = checkExisitingDocument($header_str, $header_table, $data[$header_str.$type], $type);

	    				if(!empty($checkID)){
	    					throw new Exception (implode( getErrorMessage(6), ' , ' ));
	    				}
	    				else{
	    					if($data[$header_str.'_DocNo'] == ''){
	    						throw new Exception (implode( getErrorMessage(3), ' , ' ));
	    					}
	    				}
    				}
	    		}
	    	}


	    	if($hasDetails){
	    		if( !isset($data['details']) ){
	    			throw new Exception (implode( getErrorMessage(2), ' , ' ));
	    		}
	    	}

	    	if($hasSubDetails){
	    		if( !isset($data['sub-details']) ){
	    			throw new Exception (implode( getErrorMessage(5), ' , ' ));
	    		}
	    	}

			$output = array(
				'success'  	=> $success,
				'message'  	=> ''
			);

			return $output;
    		
    	} 
    	catch (Exception $e) {

    		$output = array(
    			'success'  => false,
    			'message'  => $e->getMessage()
    		);
    		

    		return $output;
    	}

    }

    public function deleteUserRecordLock($userId = ''){

    	$userId = $_SESSION['current_user']['login-user'];

      	$where_array = array(
                            'UR_FK_User_id'   => $userId
                          );
      	$this->db->delete('tblUserRecordLock',$where_array);

    }

    public function deleteUserRecordLock_ajax(){

    	$userId = $_SESSION['current_user']['login-user'];

    	$data = $this->input->post();

      	$where_array = array(
                            'UR_FK_User_id'   => $userId,
                            'UR_PrimaryKey'   => $data['UR_PrimaryKey']
                          );
      	$this->db->delete('tblUserRecordLock',$where_array);

      	echo json_encode(true);

    }

    public function deleteUserRecordLock_php($doc_no){

    	$userId = $_SESSION['current_user']['login-user'];

      	$where_array = array(
                            'UR_FK_User_id'   => $userId,
                            'UR_PrimaryKey'   => $doc_no
                          );
      	$this->db->delete('tblUserRecordLock',$where_array);

    }


    public function insertUserRecordLock($userId = '',$table = '',$documentNo = '',$processAction = ''){


      if($this->input->is_ajax_request()){
      		$data = $this->input->post();
      		$this->deleteUserRecordLock_php($data['UR_PrimaryKey']); // Delete User Record Lock
      		
      		$insert_array = array(
	                            'UR_PrimaryKey'         => $data['UR_PrimaryKey'],
	                            'UR_TableName'          => $data['UR_TableName'],
	                            'UR_ProcessAction'      => $data['UR_ProcessAction'],
	                            'UR_FK_User_id'         => $_SESSION['current_user']['login-user'],
	                            'UR_DateTimeLock'       => date('m/d/Y h:m:s'),
	                          );
	      $this->db->insert('tblUserRecordLock',$insert_array);

	      echo json_encode(true);
      }
      else{
      		$this->deleteUserRecordLock_php($documentNo); // Delete User Record Lock
	      	$insert_array = array(
	                            'UR_PrimaryKey'         => $documentNo,
	                            'UR_TableName'          => $table,
	                            'UR_ProcessAction'      => $processAction,
	                            'UR_FK_User_id'         => $userId,
	                            'UR_DateTimeLock'       => date('m/d/Y h:m:s'),
	                          );
	      $this->db->insert('tblUserRecordLock',$insert_array);
      }
      
    }

    public function checkUserRecordLock($userId = '',$documentNo = ''){

    	if($this->input->is_ajax_request()){
    		$data = $this->input->post();

    		$result = $this->db->where('UR_PrimaryKey',$data['UR_PrimaryKey'])
	                           ->where('UR_FK_User_id <>',$_SESSION['current_user']['login-user'])
	                           ->get('tblUserRecordLock')->num_rows();

      		echo json_encode($result);
    	}
    	else{
	      	$result = $this->db->where('UR_PrimaryKey',$documentNo)
	                        ->where('UR_FK_User_id <>',$userId)
	                        ->get('tblUserRecordLock')->num_rows();

      		return $result;
    	}

    }

    public function checkIfDocNoExist($doc_no, $doc_no_name, $table){
    	$query_string = 'SELECT 	'.$doc_no_name.' FROM '.$table.' WHERE '.$doc_no_name.' = \''.$doc_no.'\'';

    	return $this->db->query($query_string)->row_array();
    }

    public function checkIfDetailExist($doc_no, $doc_no_name, $line_no, $line_no_name, $table){
    	$query_string = 'SELECT 	'.$doc_no_name.' FROM '.$table.' WHERE '.$doc_no_name.' = \''.$doc_no.'\' AND '.$line_no_name.' = \''.$line_no.'\'';

    	return $this->db->query($query_string)->row_array();
    }

    public function fixSQLString($str){
    	return trim(addslashes($str));
    }

    public function clearReference($ref_from, $ref_to, $ref_docno, $ref_table){
    	//Clear Ref DocNo
    	$this->db->where($ref_from, $ref_docno);
    	$this->db->set($ref_to, 'NULL', false);
    	$this->db->update($ref_table);
    }

    public function updateReference($ref_from, $ref_to, $ref_docno, $ref_table, $ref_item, $docno, $item){
   
    	$this->db->where($ref_from, $ref_docno);
    	$this->db->where($ref_item, $item);
    	$this->db->set($ref_to, $docno);
    	$this->db->update($ref_table);
    }

    public function getFiles($docno, $module_id){
    	$query_string = 'SELECT 		A_PathName, A_FilePath, 
    									A_FileExtension
    					 FROM 			tblAttachment
    					 WHERE 			A_DocNo = \''.$docno.'\' AND 
    					 				A_ModuleID = \''.$module_id.'\'
    					 ORDER BY 		A_EntryNo';

    	return $this->db->query($query_string)->result_array();
    }
}