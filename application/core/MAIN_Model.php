<?php 


class MAIN_Model extends CI_Model{

	protected $_table = '';

	public function __construct(){
		parent::__construct();	
	}

	// public function insert($data){
 //       return $this->db->insert($this->_table, $data);
 //    }

 //    public function update($data, $filter){
 //        $this->db->where($filter);
 //        return $this->db->update($this->_table, $data);
 //    }

 //    public function delete($filter){
 //        $this->db->where($filter);
 //        return $this->db->delete($this->_table);
 //    }

    // Added by Jekzel Leonidas
    public function print_r($data){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    public function print_lastquery(){
        echo "<pre>";
        print_r($this->db->last_query());
        echo "</pre>";
        die();
    }

    public function sql_hash($where){
        return 'CONVERT(NVARCHAR(32),HashBytes(\'MD5\', '. $where .'),2)';
    }

    public function set_alert($type, $message){
        $_SESSION['alert'] = array(
                                'type' => $type, 
                                'message' => $message);
    }

    protected function load_model($modelname){
        
        $this->load->helper('model_helper');                
        $model = generateModelName($modelname);
        if(!isset($this->$model)){
            $this->load->model($modelname, $model);
        }
        return $this->$model;
        
    }

    public function fixSQLString($str){
        return trim(addslashes($str));
    }

    // Standard Saving Module (Add) - Created by Joshua Limbing (11/26/2019)
    public function on_save_module($hasDocNoseries, $data, $table, $header_str, $ns_id, $location, $detail_str = '', $sub_detail_str = '', $type = '_DocNo'){
        $noseries_model = $this->load_model('administration/master_file/bom/noseries_model');
        $employee_profile_model = '';
        if ($header_str == 'Emp') {
            $employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
        }

        $table_header = array();
        $table_detail = array();
        $table_sub_detail = array();
        

        // Header
        foreach($data as $key => $header){
            if(strstr(key($data), $header_str) !== False){
                if(key($data) == $header_str.'_DocNo' || key($data) == $header_str.'_ID'){

                    if($hasDocNoseries){
                        $loop = 1;

                        for($i=1; $i<=$loop; $i++){
                            if ($header_str == 'Emp') {
                                $employee_profile_model->currentEmployeeID($data['Emp_CompanyId'], $data['Emp_CompanyId'].'-'.date_format(date_create($data['Emp_DateHired']), 'Y'), true);
                            }
                            else{
                                $docno = $noseries_model->getNextAvailableNumber($ns_id, true, $location);
                            }

                            if(!empty(checkExisitingDocument($header_str, $table['header'], $docno, $type))){
                                if ($header_str == 'Emp') {
                                    $employee_profile_model->currentEmployeeID($data['Emp_CompanyId'], $data['Emp_CompanyId'].'-'.date_format(date_create($data['Emp_DateHired']), 'Y'), true);
                                }
                                else{
                                    $docno = $noseries_model->getNextAvailableNumber($ns_id, true, $location);
                                }
                                $loop++;
                            }
                            else{
                                $table_header[key($data)] = $docno;
                            }
                        }
                    }
                    else{
                        $table_header[key($data)] = $header;
                    }
                    

                }
                else{
                    $table_header[key($data)] = $header;
                }
            }
            next($data);
        }
        
        $table_header['CreatedBy'] = $this->session->userdata('current_user')['login-user'];
        $table_header['DateCreated'] = date('Y-m-d H:i:s');

        $this->db->insert($table['header'], $table_header);

        // Detail
        if(isset($data['details'])){
            foreach ($data['details'] as $key => $detail) {
                unset($data['details'][$key]['rowState']);

                $temp_detail = array();

                $count_loop = count($data['details'][$key]);

                for($i=1; $i<=$count_loop; $i++){
                    if(key($data['details'][$key]) == $detail_str.'_'.$header_str.'_DocNo'){
                        $temp_detail[key($data['details'][$key])] = $table_header[$header_str.'_DocNo'];
                        next($data['details'][$key]);
                    }
                    elseif(key($data['details'][$key]) == $detail_str.'_DocNo' && $isFixedDetailTable){
                        $temp_detail[key($data['details'][$key])] = $table_header[$header_str.'_DocNo'];
                        next($data['details'][$key]);
                    }
                    elseif(key($data['details'][$key]) == $detail_str.'_'.$header_str.'_Id'){
                        $temp_detail[key($data['details'][$key])] = $table_header[$header_str.'_Id'];
                        next($data['details'][$key]);
                    }
                    elseif(key($data['details'][$key]) == $detail_str.'_'.$header_str.'_ID'){
                        $temp_detail[key($data['details'][$key])] = $table_header[$header_str.'_ID'];
                        next($data['details'][$key]);
                    }
                    else{
                        $temp_detail[key($data['details'][$key])] = $detail[key($data['details'][$key])];
                        next($data['details'][$key]);
                    }
                }

                if($detail_str != 'CA'){
                    $temp_detail[$detail_str.'_LineNo'] = $key + 1;
                    $temp_detail['CreatedBy'] = $this->session->userdata('current_user')['login-user'];
                    $temp_detail['DateCreated'] = date('Y-m-d H:i:s');
                }

                array_push($table_detail, $temp_detail);

            }

            foreach($table_detail as $key => $details){
                $this->db->insert($table['detail'], $table_detail[$key]);
            }
        }

        // Sub-Detail
        if(isset($data['sub-details'])){
            foreach ($data['sub-details'] as $key => $sub_detail) {

                unset($data['sub-details'][$key]['rowState']);

                $temp_sub_detail = array();

                $count_loop = count($data['sub-details'][$key]);

                for($i=1; $i<=$count_loop; $i++){
                    if(key($data['sub-details'][$key]) == $sub_detail_str.'_'.$detail_str.'_'.$header_str.'_DocNo'){
                        $temp_sub_detail[key($data['sub-details'][$key])] = $table_header[$header_str.'_DocNo'];
                        next($data['sub-details'][$key]);
                    }
                    else{
                        $temp_sub_detail[key($data['sub-details'][$key])] = $sub_detail[key($data['sub-details'][$key])];
                        next($data['sub-details'][$key]);
                    }
                }

                $temp_sub_detail[$sub_detail_str.'_LineNo'] = $key + 1;
                $temp_sub_detail['CreatedBy'] = $this->session->userdata('current_user')['login-user'];
                $temp_sub_detail['DateCreated'] = date('Y-m-d H:i:s');
                array_push($table_sub_detail, $temp_sub_detail);

            }

            foreach($table_sub_detail as $key => $sub_details){
                $this->db->insert($table['sub-detail'], $table_sub_detail[$key]);
            }
        }

    } // End of Saving Function (Add)

    // Standard Update Module (Update) - Created by Joshua Limbing (11/26/2019)
    // updated for different approach of saving/updating detail (4/20/2020)
    public function on_update_module($data, $table, $header_str, $ns_id, $location, $detail_str = '', $sub_detail_str = '', $isFixedDetailTable = false){

        $table_header = array();
        $table_detail = array();

        $identifier = '';
        $identifier_line = '';
        $identifier_line_no = '';

        $loop_count_hdr = count($data);

        // Header
        for($j=1; $j<=$loop_count_hdr; $j++){
            if(strstr(key($data), $header_str) !== False){
                if(key($data) != $header_str.'_DocNo' && key($data) != $header_str.'_ID' && key($data) != $header_str.'_Id'){
                    $table_header[key($data)] = $data[key($data)];
                    if (stripos(key($data), 'LineNo')) {
                        $identifier_line = key($data);
                        $identifier_line_no = $data[key($data)];
                        unset($data[key($data)]);
                    }
                }
                else{
                    if(key($data) == $header_str.'_DocNo'){
                        $identifier = 'Document';
                    }
                    elseif(key($data) == $header_str.'_ID'){
                        $identifier = 'ID';
                    }
                    elseif(key($data) == $header_str.'_Id'){
                        $identifier = 'Id';
                    }
                }
            }
            next($data);
        }

        $table_header['ModifiedBy'] = $this->session->userdata('current_user')['login-user'];
        $table_header['DateModified'] = date('Y-m-d H:i:s');

        if($identifier == 'Document'){
            $this->db->where($header_str.'_DocNo', $data[$header_str.'_DocNo'])
                     ->update($table['header'], $table_header);            
        }
        elseif($identifier == 'ID'){
            if($table['header'] == 'tblRoleAccess'){
                $module_code = $data['RA_FK_Modules_Code'];
                unset($data['RA_FK_Modules_Code']);

                $this->db->where($header_str.'_ID', $data[$header_str.'_ID'])
                         ->where($header_str.'_FK_Modules_Code', $module_code)
                         ->update($table['header'], $table_header); 
            }
            else{
                $this->db->where($header_str.'_ID', $data[$header_str.'_ID'])
                         ->update($table['header'], $table_header);  
            }
        }
        elseif($identifier == 'Id'){
            $this->db->where($header_str.'_Id', $data[$header_str.'_Id'])
                     ->update($table['header'], $table_header); 
        }
        else{
            $identifier = key($table_header);
            $identifier_doc = $table_header[key($table_header)];
            unset($table_header[key($table_header)]);

            if ($identifier_line != '') {
                $this->db->where($identifier, $identifier_doc)
                         ->where($identifier_line, $identifier_line_no)
                         ->update($table['header'], $table_header); 
            }
            else{
                $this->db->where($identifier, $identifier_doc)
                         ->update($table['header'], $table_header); 

            }
        }

        // Detail
        if(isset($data['details'])){
            foreach ($data['details'] as $key => $detail) {

                if($detail['rowState'] == 'created' || $detail['rowState'] == 'modified' || $detail['rowState'] == 'saved') {
                    $temp_detail = array();

                    $count_loop = count($data['details'][$key]);

                    for($i=1; $i<=$count_loop; $i++){
                        if(key($data['details'][$key]) == $detail_str.'_'.$header_str.'_DocNo'){
                            $temp_detail[key($data['details'][$key])] = $data[$header_str.'_DocNo'];
                            next($data['details'][$key]);
                        }
                        elseif(key($data['details'][$key]) == $detail_str.'_'.$header_str.'_Id'){
                            $temp_detail[key($data['details'][$key])] = $data[$header_str.'_Id'];
                            next($data['details'][$key]);
                        }
                        elseif(key($data['details'][$key]) == $detail_str.'_'.$header_str.'_ID'){
                            $temp_detail[key($data['details'][$key])] = $data[$header_str.'_ID'];
                            next($data['details'][$key]);
                        }
                        else{
                            $temp_detail[key($data['details'][$key])] = $detail[key($data['details'][$key])];
                            next($data['details'][$key]);
                        }
                    }

                    if($detail['rowState'] == 'created'){
                        if($detail_str != 'CA'){
                            $temp_detail['CreatedBy'] = $this->session->userdata('current_user')['login-user'];
                            $temp_detail['DateCreated'] = date('Y-m-d H:i:s');
                        }
                    }
                    elseif($detail['rowState'] == 'modified'){
                        if($detail_str != 'CA'){
                            $temp_detail['ModifiedBy'] = $this->session->userdata('current_user')['login-user'];
                            $temp_detail['DateModified'] = date('Y-m-d H:i:s');
                        }
                    }
                    array_push($table_detail, $temp_detail);
                }
                else{
                    if($detail['rowState'] == 'deleted'){
                        if(isset($data[$header_str.'_DocNo'])){
                            $this->db->where($detail_str.'_'.$header_str.'_DocNo', $data[$header_str.'_DocNo']);
                            if($detail_str != 'CA'){
                                $this->db->where($detail_str.'_LineNo', $data['details'][$key][$detail_str.'_LineNo']);
                            }
                            if(isset($data['details'][$key][$detail_str.'_ItemNo'])){
                                $this->db->where($detail_str.'_ItemNo', $data['details'][$key][$detail_str.'_ItemNo']);
                            }
                            $this->db->delete($table['detail']);
                        }
                        elseif (isset($data[$header_str.'_ID']) || isset($data[$header_str.'_id']) || isset($data[$header_str.'_ID'])) {
                            $this->db->where($detail_str.'_'.$header_str.'_ID', $data[$header_str.'_ID']);
                            if($detail_str != 'CA'){
                                $this->db->where($detail_str.'_LineNo', $data['details'][$key][$detail_str.'_LineNo']);
                            }
                            if(isset($data['details'][$key][$detail_str.'_ItemNo'])){
                                $this->db->where($detail_str.'_ItemNo', $data['details'][$key][$detail_str.'_ItemNo']);
                            }
                            $this->db->delete($table['detail']);
                        }
                    }
                }

            }

            foreach($table_detail as $key => $details){
                if($details['rowState'] == 'created'){
                    unset($table_detail[$key]['rowState']);
                    if($detail_str != 'CA'){
                        $table_detail[$key][$detail_str.'_LineNo'] = $key + 1;
                    }
                    $this->db->insert($table['detail'], $table_detail[$key]);
                }
                else{
                    unset($table_detail[$key]['rowState']);
                    if($detail_str != 'CA'){
                        $line_no = $details[$detail_str.'_LineNo'];
                        $table_detail[$key][$detail_str.'_LineNo'] = $key + 1;
                    }
                    else{
                        unset($table_detail[$key]['CA_U_ID']);
                    }

                    if(isset($data[$header_str.'_DocNo'])){

                        if($isFixedDetailTable){
                            $this->db->where($detail_str.'_DocNo', $data[$header_str.'_DocNo'])
                                 ->where($detail_str.'_LineNo', $line_no)
                                 ->update($table['detail'], $table_detail[$key]);
                        }
                        else{
                            $this->db->where($detail_str.'_'.$header_str.'_DocNo', $data[$header_str.'_DocNo'])
                                 ->where($detail_str.'_LineNo', $line_no)
                                 ->update($table['detail'], $table_detail[$key]);    
                        }
                        
                    }
                    else{
                        $this->db->where($detail_str.'_'.$header_str.'_ID', $data[$header_str.'_ID']);
                        if($detail_str != 'CA'){
                            $this->db->where($detail_str.'_LineNo', $line_no);
                        }
                        $this->db->update($table['detail'], $table_detail[$key]);
                    }
                }
            }
        }

        // Sub-Detail
        if(isset($data['sub-details'])){
            foreach ($data['sub-details'] as $key => $sub_detail) {

                if($sub_detail['rowState'] == 'created' || $sub_detail['rowState'] == 'modified' || $sub_detail['rowState'] == 'saved') {
                    $temp_sub_detail = array();

                    $count_loop = count($data['sub-details'][$key]);

                    for($i=1; $i<=$count_loop; $i++){
                        if(key($data['sub-details'][$key]) == $sub_detail_str.'_'.$detail_str.'_'.$header_str.'_DocNo'){
                            $temp_sub_detail[key($data['sub-details'][$key])] = $data[$header_str.'_DocNo'];
                            next($data['sub-details'][$key]);
                        }
                        else{
                            $temp_sub_detail[key($data['sub-details'][$key])] = $detail[key($data['sub-details'][$key])];
                            next($data['sub-details'][$key]);
                        }
                    }

                    if($sub_detail['rowState'] == 'created'){
                        $temp_sub_detail['CreatedBy'] = $this->session->userdata('current_user')['login-user'];
                        $temp_sub_detail['DateCreated'] = date('Y-m-d H:i:s');
                    }
                    elseif($sub_detail['rowState'] == 'modified'){
                        $temp_sub_detail['ModifiedBy'] = $this->session->userdata('current_user')['login-user'];
                        $temp_sub_detail['DateModified'] = date('Y-m-d H:i:s');
                    }
                    array_push($table_sub_detail, $temp_sub_detail);
                }
                else{
                    if($sub_detail['rowState'] == 'deleted'){
                        $this->db->where($sub_detail_str.'_'.$detail_str.'_'.$header_str.'_DocNo', $data[$header_str.'_DocNo']);
                        $this->db->where($sub_detail_str.'_LineNo', $data['sub-details'][$key][$sub_detail_str.'_LineNo']);
                        if(isset($data['sub-details'][$key][$sub_detail_str.'_ItemNo'])){
                            $this->db->where($sub_detail_str.'_ItemNo', $data['sub-details'][$key][$sub_detail_str.'_ItemNo']);
                        }
                        $this->db->delete($table['sub-detail']);
                    }
                }

            }

            foreach($table_sub_detail as $key => $sub_details){
                if($sub_details['rowState'] == 'created'){
                    unset($table_sub_detail[$key]['rowState']);
                    $table_sub_detail[$key][$detail_str.'_LineNo'] = $key + 1;
                    $this->db->insert($table['sub-detail'], $table_sub_detail[$key]);
                }
                else{
                    unset($table_sub_detail[$key]['rowState']);
                    $line_no = $sub_details[$sub_detail_str.'_LineNo'];
                    $table_sub_detail[$key][$sub_detail_str.'_LineNo'] = $key + 1;
                    $this->db->where($sub_detail_str.'_'.$detail_str.'_'.$header_str.'_DocNo', $data[$header_str.'_DocNo'])
                             ->where($sub_detail_str.'_LineNo', $line_no)
                             ->update($table['sub-detail'], $table_sub_detail[$key]);
                }
            }
        }

    } // End of Update Function (Update)

    public function getHeaderByDocNo($query_items, $table, $where_docno, $join_table = array(), $where = array()){

        $this->db->select($query_items);
        if(!empty($join_table)){
            foreach($join_table as $key => $join){
                $this->db->join($join['tbl_name'], $join['tbl_on'], $join['tbl_join']);
            }
        }
        $this->db->where($this->sql_hash(key($where_docno)), '= \''.$where_docno[key($where_docno)].'\'', false);
        if(!empty($where)){
            foreach($where as $key => $whr){
                $this->db->where(key($whr), $whr);
                next($whr);
            }
        }

        return $this->db->get($table)->row_array();

    }

    public function getDetails($query_items, $table, $where_docno = '', $where = array(), $join_table = array(), $order_by = ''){

        $this->db->select($query_items);
        if(!empty($join_table)){
            foreach($join_table as $key => $join){
                $this->db->join($join['tbl_name'], $join['tbl_on'], $join['tbl_join']);
            }
        }

        if($where_docno != ''){
            $this->db->where($this->sql_hash(key($where_docno)), '= \''.$where_docno[key($where_docno)].'\'', false);
        }

        if(!empty($where)){
            foreach($where as $key => $list){
                $this->db->where(key($where), $list);
            }
        }

        if($order_by != ''){
            $this->db->order_by($order_by);
        }


        return $this->db->get($table)->result_array();

    }

    public function getSubDetails($query_items, $table, $where_docno = '', $where = array(), $join_table = array(), $order_by = ''){

        $this->db->select($query_items);
        if(!empty($join_table)){
            foreach($join_table as $key => $join){
                $this->db->join($join['tbl_name'], $join['tbl_on'], $join['tbl_join']);
            }
        }

        if($where_docno != ''){
            $this->db->where($this->sql_hash(key($where_docno)), '= \''.$where_docno[key($where_docno)].'\'', false);
        }

        if(!empty($where)){
            foreach($where as $key => $list){
                $this->db->where(key($where), $list);
            }
        }

        if($order_by != ''){
            $this->db->order_by($order_by);
        }


        return $this->db->get($table)->result_array();

    }

    public function getItemTypes(){
        return $this->db
            ->select(' LTRIM(RTRIM(IT_id)) AS Code, LTRIM(RTRIM(IT_Description)) AS Description',false)
            ->where('IT_Active','1')
            ->where('IT_id <>','GA')
            ->order_by('IT_Description')
            ->get('tblItemType')
            ->result_array();
    }

    public function convert24hrFormat($time){
        if(strlen($time) == 7){
            $hours = intval(substr($time,0,2));
            $minutes = intval(substr($time,3,2));
            $AMPM = substr($time,5,2);
        }
        else{
            $hours = intval(substr($time,0,1));
            $minutes = intval(substr($time,2,2));
            $AMPM = substr($time,4,2);
        }

        if($AMPM == "PM" && $hours<12){$hours = $hours+12;}
        if($AMPM == "AM" && $hours==12){$hours = $hours-12;}

        $sHours = $hours;
        $sMinutes = $minutes;
        if($hours<10) {$sHours = "0".$sHours;}
        if($minutes<10) {$sMinutes = "0".$sMinutes;}

        return $sHours.$sMinutes;
    }

    private function convert12hrFormat($time){
        $hours = substr($time,0,2);
        $minutes = substr($time,2,2);
        $AMPM = '';


        if(intval($hours) >= 12){
            if(intval($hours >= 24)){
                $hours = intval($hours) - 24;
                
                if($hours == 0){
                    $hours = "12";
                    $AMPM = "AM";
                }
                else if($hours == 12){
                    $AMPM = "PM";
                }
                else if($hours > 12){
                    $hours = intval($hours) - 12;
                    $AMPM = "PM";
                }
                else if($hours < 12 && $hours > 0){
                    $AMPM = "AM";
                }
            }
            else{
                if(intval($hours) != 12){
                    $hours = intval($hours) - 12;
                    $AMPM = "PM";
                }
                else{
                    $AMPM = "PM";
                }
            }

            if(intval($hours) < 10){
                $hours = "0".$hours;
            }
        }
        else{
            if($hours == "00"){
                $hours = "12";
                $AMPM = "AM";
            }

            if($hours != "00" && intval($hours) < 12){
                $AMPM = "AM";
            }
        }

        return $hours.":".$minutes.$AMPM;
    }

	
}