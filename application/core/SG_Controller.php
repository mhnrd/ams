<?php 


class SG_Controller extends MAIN_Controller{ 
	
	public function getDate(){
		return date_format(date(),'m/d/Y');
	}

	public function getDocNo($NS_Id, $location){
		$result = $this->db->select('NS_LastNoUsed')
		         ->where(array('NS_Id' => $NS_Id, 'NS_Location' => $location))
		         ->get('tblNoSeries')->row_array();
		return ($result['NS_LastNoUsed']+1);
	}

}