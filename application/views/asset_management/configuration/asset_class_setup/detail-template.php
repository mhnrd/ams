<div class="modal inmodal animated fadeInUp" id="detail-template" data-backdrop="static" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<strong>ASSET CLASS SETUP</strong>
				</div>
				<div class="modal-body">
					<form id="form-header">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Asset Class ID:</label>
										</div>
										<div class="col-8">
											<input type="text" required class="form-control p-xxs" name="FACS_ID" value="">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Description</label>
										</div>
										<div class="col-8">
											<input type="text" required class="form-control p-xxs" name="FACS_Description" value="">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Useful Life:</label>
										</div>
										<div class="col-8 row">
											<div class="col-6">
												<input type="number" required class="form-control p-xxs text-right" name="FACS_UsefulLifeNo" value="" min="0">
											</div>
											<div class="col-6" style="padding-left: 0; padding-right: 0">
												<select required name="FACS_UsefulLifeNoUOM" class="js-select2 form-control p-xxs" style="width: 100%">
													<option value=""></option>
													<option value="M">Month(s)</option>
													<option value="Y">Year(s)</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-outline btn-xs save-asset-class" id="save-new" data-action="save-new" value="Add"><span class="glyphicon glyphicon-floppy-saved"></span> Add and New</button>
					<button type="button" class="btn btn-primary btn-outline btn-xs save-asset-class" id="save-close" data-action="save-close" value="Add"><span class="glyphicon glyphicon-floppy-disk"></span> Add and Close</button>
					<button class="btn btn-warning btn-outline btn-xs" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
				</div>
			</div>
		</div>
	</div>