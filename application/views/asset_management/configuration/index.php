<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<link href="<?php echo EXTENSION ?>inspinia/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">

<style>
.ibox-content, .ibox-title, .panel-body{
	background: #DDD8E7!important;
}
</style>

<section>
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<div class="pull-left m-r-md">
                <a href="<?= DOMAIN.'dashboard' ?>" id="home">
					<i class="fa fa-home" style="font-size:25px; margin-top: 7px"></i>
				</a>
            </div>
            <div class="pull-right">
            	<a href="<?= DOMAIN.'dashboard' ?>" class="btn btn-outline btn-xs btn-warning" role="button" style="margin-top: 7px">
            		<i class="fa fa-reply"></i>
            	</a>
            </div>
			<h2 style="font-weight: bold; padding-top: 1px; color: #2980b9!important" class="text-success">CONFIGURATION</h2>
		</div>
		<div class="ibox-content">
			<div class="col-12" align="center">
				<div class="form-horizontal">
					<div class="row">
						<?php foreach (get_sub_modules(400000) as $key => $value): ?>
							<div class="form-group col-6" align="center">
								<a href="<?php echo DOMAIN.$value['M_Trigger'] ?>">
									<div align="center" style="font-weight: bold; color: white!important; background: #2980b9; height: 100px; border-radius: 20px; padding: 10px">
										<span class="<?= $value['M_Icon'] ?> text-success" style="font-size: 50px; padding-top: 6px; color: white!important"></span>
										<div style="padding-top: 5px"><?= strtoupper($value['M_DisplayName']) ?></div>
									</div>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>

		</div>
	</div>
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/fullcalendar/moment.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	
	<!-- Full Calendar -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/fullcalendar/fullcalendar.min.js"></script>
	<!-- Page-Level Scripts -->
	<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
	<script>

	</script>
	
</section>


