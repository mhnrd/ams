<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<!-- Clockpicker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker.css" rel="stylesheet">
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo EXTENSION ?>moment.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

<style>
	.red{
		color:red;
	}
</style>

<section class="wrapper wrapper-content">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="pull-left m-r-md">
                <a href="<?= DOMAIN.'dashboard' ?>" id="home">
                    <i class="fa fa-home" style="font-size:25px; margin-top: 7px"></i>
                </a>
            </div>
            <div class="pull-right">
                <a href="<?= DOMAIN.'asset_management/reports' ?>" class="btn btn-outline btn-xs btn-warning" role="button" style="margin-top: 7px">
                    <i class="fa fa-reply"></i>
                </a>
            </div>
            <h2 style="font-weight: bold; padding-top: 1px; color: #2980b9!important" class="text-success">REPORTS</h2>
        </div>
        <!-- Modules -->
        <div class="ibox-content">
        	<div class="col-12">
        		<div class="row">
        			<div class="col-md-3 col-12">
				        <div class="col-md-12 wrapper white-bg page-heading" style="padding-bottom: 1px!important; padding-top: 1px!important; padding-left: 1px!important">
				            <ul class="nav nav-pills nav-justified mod-list" style="display: table!important; width: 100%!important; text-align: left">
				                <?php foreach (get_sub_modules('$500000') as $key => $value): ?>
				                    <?php if ($key == 3 || ($key + 1) % 4 == 0): ?>
				                        </ul>
				                        <ul class="nav nav-pills nav-justified mod-list" style="display: table!important; width: 100%!important; text-align: center">
				                            <li class="mother-mod<?= ($this->uri->segment(2) == $value['M_ControllerName'] ? ' active-parent-mod' : '')  ?>">
				                                <a href="<?= DOMAIN.$value['M_Trigger'] ?>">
				                                    <i class="<?= $value['M_Icon'] ?>"></i>&nbsp;<?= $value['M_DisplayName'] ?>
				                                </a>
				                            </li>
				                    <?php else: ?>
				                        <li class="mother-mod<?= ($this->uri->segment(2) == $value['M_ControllerName'] ? ' active-parent-mod' : '')  ?>">
				                            <a href="<?= DOMAIN.$value['M_Trigger'] ?>">
				                                <i class="<?= $value['M_Icon'] ?>"></i>&nbsp;<?= $value['M_DisplayName'] ?>
				                            </a>
				                        </li>
				                    <?php endif ?>
				                <?php endforeach; ?>
				            </ul>
				        </div>
				    </div>
			        <div class="col-md-9 col-12">
						<h4 style="font-weight: bold; padding-top: 1px; color: #2980b9!important" class="text-success">INVENTORY REPORT</h4>
			            <div class="row">
			                <div class="col-md-12">
								<form id="reports-form">
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label><span class="red">*</span> Division:</label>
											</div>
											<div class="col-md-6">
												<select required style="width:100%" name="FA_DepartmentID" class="form-control p-xxs">
													<option value=""></option>
													<?php foreach ($department as $key => $value): ?>
														<option value="<?= $value['AD_ID'] ?>"><?= $value['AD_Desc'] ?></option>
													<?php endforeach ?>
												</select>
											</div>
										</div>
						        	</div>
						        	<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Line No.:</label>
											</div>
											<div class="col-md-6">
												<select style="width:100%" name="FA_Line" class="form-control p-xxs">
													<option value=""></option>
													<?php foreach ($FA_Line as $key => $value): ?>
														<option value="<?= $value['SL_Name'] ?>"><?= $value['SL_Name'] ?></option>
													<?php endforeach ?>
												</select>
											</div>
										</div>
						        	</div>
						        	<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Model:</label>
											</div>
											<div class="col-md-6">
												<select style="width:100%" name="FA_Model" class="form-control p-xxs">
													<option value=""></option>
													<?php foreach ($FA_Model as $key => $value): ?>
														<option value="<?= $value['AD_ID'] ?>"><?= $value['AD_Desc'] ?></option>
													<?php endforeach ?>
												</select>
											</div>
										</div>
						        	</div>
						        	<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Model Year:</label>
											</div>
											<div class="col-md-6">
												<select style="width:100%" name="FA_ModelYear" class="form-control p-xxs">
													<option value=""></option>
													<?php foreach ($FA_ModelYear as $key => $value): ?>
														<option value="<?= $value['AD_ID'] ?>"><?= $value['AD_Desc'] ?></option>
													<?php endforeach ?>
												</select>
											</div>
										</div>
						        	</div>
						        	<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Family:</label>
											</div>
											<div class="col-md-6">
												<select style="width:100%" name="FA_ModelYear" class="form-control p-xxs">
													<option value=""></option>
													<?php foreach ($FA_Family as $key => $value): ?>
														<option value="<?= $value['AD_ID'] ?>"><?= $value['AD_Desc'] ?></option>
													<?php endforeach ?>
												</select>
											</div>
										</div>
						        	</div>
						        	<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Month / Year:</label>
											</div>
											<div class="col-md-3">
												<select style="width:100%" name="Month" class="form-control p-xxs">
													<option value=""></option>
													<option value="1" <?= intval(date('m')) == 1 ? 'selected' : '' ?>>January</option>
													<option value="2" <?= intval(date('m')) == 2 ? 'selected' : '' ?>>February</option>
													<option value="3" <?= intval(date('m')) == 3 ? 'selected' : '' ?>>March</option>
													<option value="4" <?= intval(date('m')) == 4 ? 'selected' : '' ?>>April</option>
													<option value="5" <?= intval(date('m')) == 5 ? 'selected' : '' ?>>May</option>
													<option value="6" <?= intval(date('m')) == 6 ? 'selected' : '' ?>>June</option>
													<option value="7" <?= intval(date('m')) == 7 ? 'selected' : '' ?>>July</option>
													<option value="8" <?= intval(date('m')) == 8 ? 'selected' : '' ?>>August</option>
													<option value="9" <?= intval(date('m')) == 9 ? 'selected' : '' ?>>September</option>
													<option value="10" <?= intval(date('m')) == 10 ? 'selected' : '' ?>>October</option>
													<option value="11" <?= intval(date('m')) == 11 ? 'selected' : '' ?>>November</option>
													<option value="12" <?= intval(date('m')) == 12 ? 'selected' : '' ?>>December</option>
												</select>
											</div>
											<div class="col-3" style="padding-left: 1px;">
												<div class="input-group">
													<input required type="text" class="form-control p-xxs" name="Year" value="<?= date('Y') ?>"  data-mask="9999">
				            						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
										</div>                               
						        	</div>
						        	<div class="form-group row">
						        		<div class="col-md-4"></div>
						        		<div class="col-md-8 text-right">
						        			<button id="create-report" data-report-module="<?= $number_series ?>" class="btn btn-success btn-outline">
							                    <span class="glyphicon glyphicon-print"></span> Generate Report
							                </button>	
						        		</div>
						        	</div>
								</form>
							</div>
			        	</div>
					</div>
        		</div>
		    </div>
        </div>
	</div>

<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.js"></script>
<!-- <script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script> -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo EXTENSION ?>moment.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- Page-Level Scripts -->
<script type="text/javascript">
	var tableindex;
	var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
	var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
	var number_series 			= '<?= $number_series ?>';
	var module_url 				= '<?= DOMAIN.'asset_management/inventory_report' ?>';
</script>

<script src="<?php echo JS_DIR?>app/asset_management/inventory_report/index.js"></script>

</section>