<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<!-- Clockpicker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker.css" rel="stylesheet">
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo EXTENSION ?>moment.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

<style>
	.red{
		color:red;
	}
</style>

<section class="wrapper wrapper-content">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="pull-left m-r-md">
                <a href="<?= DOMAIN.'dashboard' ?>" id="home">
                    <i class="fa fa-home" style="font-size:25px; margin-top: 7px"></i>
                </a>
            </div>
            <div class="pull-right">
                <a href="<?= DOMAIN.'asset_management/reports' ?>" class="btn btn-outline btn-xs btn-warning" role="button" style="margin-top: 7px">
                    <i class="fa fa-reply"></i>
                </a>
            </div>
            <h2 style="font-weight: bold; padding-top: 1px; color: #2980b9!important" class="text-success">REPORTS</h2>
        </div>
        <!-- Modules -->
		<div class="ibox-content">
			<div class="col-12">
        		<div class="row">
					<div class="col-md-3 col-12">
				        <div class="col-md-12 wrapper white-bg page-heading" style="padding-bottom: 1px!important; padding-top: 1px!important; padding-left: 1px!important">
				            <ul class="nav nav-pills nav-justified mod-list" style="display: table!important; width: 100%!important; text-align: left">
				                <?php foreach (get_sub_modules('$500000') as $key => $value): ?>
				                    <?php if ($key == 3 || ($key + 1) % 4 == 0): ?>
				                        </ul>
				                        <ul class="nav nav-pills nav-justified mod-list" style="display: table!important; width: 100%!important; text-align: center">
				                            <li class="mother-mod<?= ($this->uri->segment(2) == $value['M_ControllerName'] ? ' active-parent-mod' : '')  ?>">
				                                <a href="<?= DOMAIN.$value['M_Trigger'] ?>">
				                                    <i class="<?= $value['M_Icon'] ?>"></i>&nbsp;<?= $value['M_DisplayName'] ?>
				                                </a>
				                            </li>
				                    <?php else: ?>
				                        <li class="mother-mod<?= ($this->uri->segment(2) == $value['M_ControllerName'] ? ' active-parent-mod' : '')  ?>">
				                            <a href="<?= DOMAIN.$value['M_Trigger'] ?>">
				                                <i class="<?= $value['M_Icon'] ?>"></i>&nbsp;<?= $value['M_DisplayName'] ?>
				                            </a>
				                        </li>
				                    <?php endif ?>
				                <?php endforeach; ?>
				            </ul>
				        </div>
				    </div>
				    <div class="col-md-9 col-12">
						<h4 style="font-weight: bold; padding-top: 1px; color: #2980b9!important" class="text-success">LAPSING SCHEDULE</h4>
				        <div class="row">
				            <div class="col-md-12">
								<form id="reports-form">
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label><span class="red">*</span> Company:</label>
											</div>
											<div class="col-md-6">
												<select required style="width:100%" name="Company" class="form-control p-xxs">
													<option value=""></option>
													<?php foreach ($Company as $key => $value): ?>
														<option value="<?= $value['COM_Id'] ?>"><?= $value['COM_Name'] ?></option>
													<?php endforeach ?>
												</select>
											</div>
										</div>                               
						        	</div>
						        	<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label><span class="red">*</span> From:</label>
											</div>
											<div class="col-md-4">
												<div class="input-group date">
													<input required type="text" class="form-control p-xxs text-center" name="dtFrom" value=""  data-mask="99/99/9999">
				            						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
											<div class="col-md-1 text-right" style="padding-left: 0">
												<label><span class="red">*</span> to:</label>
											</div>
											<div class="col-md-4">
												<div class="input-group date">
													<input required type="text" class="form-control p-xxs text-center" name="dtTo" value=""  data-mask="99/99/9999">
				            						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
										</div>
						        	</div>
						        	<div class="form-group row">
						        		<div class="col-md-4"></div>
						        		<div class="col-md-8 text-right">
						        			<button id="create-report" data-report-module="<?= $number_series ?>" class="btn btn-success btn-outline">
							                    <span class="glyphicon glyphicon-print"></span> Generate Report
							                </button>	
						        		</div>
						        	</div>
								</form>
							</div>
				    	</div>
				    </div>
			    </div>
			</div>
		</div>
	</div>

<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.js"></script>
<!-- <script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script> -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo EXTENSION ?>moment.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- Page-Level Scripts -->
<script type="text/javascript">
	var tableindex;
	var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
	var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
	var number_series 			= '<?= $number_series ?>';
	var module_url 				= '<?= DOMAIN.'asset_management/lapsing_schedule' ?>';
</script>

<script src="<?php echo JS_DIR?>app/asset_management/lapsing_schedule/index.js"></script>

</section>