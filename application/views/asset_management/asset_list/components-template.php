<div class="modal inmodal animated fadeInUp" id="components-template" data-backdrop="static" role="dialog" aria-hidden="true">
		<input type="hidden" name="FAC_FA_LineNo" value=""/>
		<div class="modal-dialog modal-md">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<strong>COMPONENTS</strong>
				</div>
				<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Item ID:</label>
										</div>
										<div class="col-8">
											<select class="form-control js-select2" required name="FAC_ItemID" style="width: 100%">
												<option value=""></option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Description:</label>
										</div>
										<div class="col-8">
											<input type="text" required class="form-control p-xxs" name="FAC_Description" value="" readonly>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Quantity:</label>
										</div>
										<div class="col-8">
											<input type="number" required class="form-control text-right p-xxs" name="FAC_Qty">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Cost:</label>
										</div>
										<div class="col-8">
											<input type="number" required class="form-control text-right p-xxs" name="FAC_UnitCost" value="">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Total Cost:</label>
										</div>
										<div class="col-8">
											<input type="text" required class="form-control text-right" name="FAC_TotalCost" value="" readonly>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success btn-outline btn-xs save-components" id="save-new-components" data-action="save-new" value="Add"><span class="glyphicon glyphicon-floppy-saved"></span> Add and New</button>
					<button type="button" class="btn btn-success btn-outline btn-xs save-components" id="save-close-components" data-action="save-close" value="Add"><span class="glyphicon glyphicon-floppy-saved"></span> Add and Close</button>
					<button class="btn btn-warning btn-outline btn-xs" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
				</div>
			</div>
		</div>
	</div>