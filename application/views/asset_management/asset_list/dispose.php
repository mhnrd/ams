<div class="modal inmodal animated fadeInUp" id="dispose-template" data-backdrop="static" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<strong>DISPOSE</strong>
				</div>
				<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Asset ID:</label>
										</div>
										<div class="col-8">
											<input type="text" required class="form-control p-xxs" name="FAMH_FA_AssetID" value="" readonly>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Description:</label>
										</div>
										<div class="col-8">
											<input type="text" class="form-control p-xxs" name="FAMH_Description" value="" readonly>
										</div>
									</div>
								</div>
								<div class="hr-line-dashed"></div>
								<h4>Disposal Information</h4>
								<div class="hr-line-dashed"></div>
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Effective Date:</label>
										</div>
										<div class="col-8">
											<div class="input-group date">
												<input type="text" data-mask="99/99/9999" class="form-control datepick" name="FAMH_Date">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Remarks:</label>
										</div>
										<div class="col-8">
											<textarea name="FAMH_Remarks" class="form-control p-xxs"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success btn-outline btn-xs" id="save-dispose" data-action="save-close" value="Add"><span class="glyphicon glyphicon-floppy-saved"></span> Process Disposal</button>
					<button class="btn btn-warning btn-outline btn-xs" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
				</div>
			</div>
		</div>
	</div>