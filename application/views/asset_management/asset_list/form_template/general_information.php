<!-- General Information -->
<div class="col-12">
	<div class="form-group row">
		<label class="control-label col-4">Description:</label>	
		<div class="col-8">
			<textarea id="" name="FA_Description" class="form-control"><?= $header['FA_Description'] ?></textarea>
		</div>
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Asset ID:</label>	
		<div class="col-8">
			<input type="text" class="form-control" name="FA_ID" readonly value="<?= $header['FA_ID'] ?>">
		</div>			
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Asset Code:</label>	
		<div class="col-8">
			<select class="form-control js-select2" name="FA_AssetCode" required style="width: 100%">
				<option value=""></option>
				<?php foreach ($asset_code as $key => $value): ?>
					<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_AssetCode'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
				<?php endforeach ?>
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Asset Type:</label>	
		<div class="col-8">
			<select class="form-control js-select2" name="FA_AssetType" required style="width: 100%">
				<option value=""></option>
				<?php foreach ($asset_type as $key => $value): ?>
					<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_AssetType'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
				<?php endforeach ?>
			</select>
		</div>
	</div>
	<!-- <div class="form-group row" <?= ($type == 'update' ? 'style="display: none"' : '') ?>> -->
	<div class="form-group row">
		<label class="control-label col-4">Division:</label>	
		<div class="col-8">
			<select class="form-control js-select2" name="FA_DepartmentID" style="width: 100%">
				<option value=""></option>
				<?php foreach ($department as $key => $value): ?>
					<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_DepartmentID'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
				<?php endforeach ?>
			</select>
		</div>
	</div>

	<!-- Revised/Added by JDC 071720241 -->
	<div class="form-group row">
		<label class="control-label col-4">Bay No.:</label>	
		<div class="col-8">
			<select class="form-control js-select2" name="FA_Bay" disabled required style="width: 100%">
				<option value=""></option>

				<?php foreach ($asset_bay as $key => $value): ?>
					<option value="<?= $value['SL_Id'] ?>" <?= ($value['SL_Id'] == $header['FA_Bay'] ? 'selected' : '') ?>><?= $value['SL_Name'] ?></option>
				<?php endforeach ?>

            </select>
		</div>
	</div>

	<!-- Revised/Added by JDC 071720241 -->
	<div class="form-group row">
		<label class="control-label col-4">Line No.:</label>	
		<div class="col-8">
			<select class="form-control js-select2" name="FA_Line" disabled required style="width: 100%">
				<option value=""></option>

				<?php foreach ($asset_line as $key => $value): ?>
					<option value="<?= $value['SL_Id'] ?>" <?= ($value['SL_Id'] == $header['FA_Line'] ? 'selected' : '') ?>><?= $value['SL_Name'] ?></option>
				<?php endforeach ?>
				
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Item Reference:</label>	
		<div class="col-8">
			<select class="form-control" name="FA_ItemID" style="width: 100%">
				<option value=""></option>
				<?php if ($type != 'add'): ?>
					<option value="<?= $header['FA_ItemID'] ?>" selected><?= $header['FA_ItemCode'].' - '.$header['FA_ItemDesc'] ?></option>
				<?php endif ?>
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Asset Class:</label>	
		<div class="col-8">
			<select class="form-control js-select2" name="FA_AssetClass" style="width: 100%">
				<option value=""></option>
				<?php foreach ($asset_class as $key => $value): ?>
					<option value="<?= $value['FACS_ID'] ?>" <?= ($value['FACS_ID'] == $header['FA_AssetClass'] ? 'selected' : '') ?>><?= $value['FACS_Description'] ?></option>
				<?php endforeach ?>
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Model:</label>	
		<div class="col-8">
			<input type="text" class="form-control" name="FA_Model" value="<?= $header['FA_Model'] ?>">
		</div>			
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Model Year:</label>	
		<div class="col-8">
			<input type="text" class="form-control" name="FA_ModelYear" value="<?= $header['FA_ModelYear'] ?>">
		</div>			
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Family:</label>	
		<div class="col-8">
			<input type="text" class="form-control" name="FA_Family" value="<?= $header['FA_Family'] ?>">
		</div>			
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Ref. No.:</label>	
		<div class="col-8">
			<input type="text" class="form-control" name="FA_RefNo" value="<?= $header['FA_RefNo'] ?>">
		</div>			
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Date Placed in Services:</label>	
		<div class="col-8">
			<div class="input-group date">
				<input type="text" class="form-control datepick" data-mask="99/99/9999" name="FA_DateUsed" value="<?= ($header['FA_DateUsed'] == '' ? $header['FA_DateUsed'] : date_format(date_create($header['FA_DateUsed']), "m/d/Y") ) ?>">
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			</div>
		</div>			
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Supplier Code:</label>	
		<div class="col-8">
			<select class="form-control js-select2" name="FA_SupplierID" style="width: 100%">
				<option value=""></option>
				<?php foreach ($supplier as $key => $value): ?>
					<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_SupplierID'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
				<?php endforeach ?>
			</select>
		</div>
	</div>
			
	<div class="form-group row" <?= ($type == 'update' ? 'style="display: none"' : '') ?>>
		<label class="control-label col-4">Car Line:</label>	
		<div class="col-8">
			<select class="form-control js-select2" name="FA_AffiliatesID" style="width: 100%">
				<option value=""></option>
				<?php foreach ($affiliates as $key => $value): ?>
					<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_AffiliatesID'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
				<?php endforeach ?>
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Process:</label>	
		<div class="col-8">
			<select class="form-control js-select2" name="" required style="width: 100%">
				<option value=""></option>
				<?php foreach ($process_type as $key => $value): ?>
					<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_Process'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
				<?php endforeach ?>
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label class="control-label col-4">Status:</label>	
		<div class="col-8">
			<select class="form-control js-select2" name="FA_Status" style="width: 100%">
				<option value=""></option>
				<option value="Active" <?= ($header['FA_Status'] == 'Active' ? 'selected' : '') ?>>Active</option>
				<option value="Inactive" <?= ($header['FA_Status'] == 'Inactive' ? 'selected' : '') ?>>Inactive</option>
				<option value="Disposed" <?= ($header['FA_Status'] == 'Disposed' ? 'selected' : '') ?> disabled>Disposed</option>
			</select>
		</div>
	</div>
</div>