<!-- Acquisition Information -->
			<div class="col-12">
				<div class="form-group row">
					<label class="control-label col-4">Acquisition Date:</label>
					<div class="col-8">
						<div class="input-group date">
							<input type="text" data-mask="99/99/9999" class="form-control datepick" name="FA_AcquisitionDate" value="<?= ($header['FA_AcquisitionDate'] == '' ? $header['FA_AcquisitionDate'] : date_format(date_create($header['FA_AcquisitionDate']), "m/d/Y") ) ?>">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-4">Serial No:</label>
					<div class="col-8">
						<input type="text" class="form-control" name="FA_SerialNo" value="<?= $header['FA_SerialNo'] ?>">
					</div>	
				</div>
				<div class="form-group row">
					<label class="control-label col-4">Acquisition Cost:</label>
						<div class="col-4">
							<select class="form-control js-select2" name="FA_AcquisitionCostCY" style="width: 100%">
									<?php foreach ($currency as $key => $value): ?>
										<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_AcquisitionCostCY'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
									<?php endforeach ?>
							</select>
						</div>
						<div class="col-4">
							<input type="number" class="form-control text-right" name="FA_AcquisitionCost" value="<?= number_format($header['FA_AcquisitionCost'], 2, ".", "") ?>">
					    </div>
				</div>
				<div class="form-group row">
					<label class="control-label col-4">Exchange Rate:</label>
					<div class="col-8">
						<input type="number" class="form-control text-right" name="FA_ExchangeRate" value="<?= number_format($header['FA_ExchangeRate'], 2, ".", "") ?>" min="0">
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-4">Net Book Value:</label>
						<div class="col-4">
							<select class="form-control js-select2" name="FA_AcquisitionNetBookCY" style="width: 100%">
								<?php foreach ($currency as $key => $value): ?>
									<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_AcquisitionNetBookCY'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-4">
							<input type="text" class="form-control text-right" id="FA_NetBookValue" readonly>
					    </div>
				</div>

				<div class="form-group row">
					<label class="control-label col-4">Warranty Period:</label>
					<div class="col-4">
						<input type="text" class="form-control text-right" name="FA_WarrantyPeriodNo" value="<?= number_format($header['FA_WarrantyPeriodNo'], 0, ".", "") ?>" min="0">
					</div>
					<div class="col-4">
						<select class="form-control js-select2" style="width: 100%" name="FA_WarrantyPeriodUOM">
							<option value=""></option>
							<option value="M" <?= ($header['FA_WarrantyPeriodUOM'] == 'M' ? 'selected' : '') ?>>Month(s)</option>
							<option value="Y" <?= ($header['FA_WarrantyPeriodUOM'] == 'Y' ? 'selected' : '') ?>>Year(s)</option>
						</select>
					</div>	
				</div>
				<div class="form-group row">
					<label class="control-label col-4">Warranty Date:</label>
					<div class="col-8">
						<input type="text" data-mask="99/99/9999" class="form-control" id="FA_WarrantyDate" readonly>
					</div>	
				</div>
				<div class="form-group row">
					<label class="control-label col-4">Useful Life:</label>
					<div class="col-4">
						<input type="text" pattern="\d*" class="form-control text-right" name="FA_UsefulLifeNo" value="<?= number_format($header['FA_UsefulLifeNo'], 0, ".", "") ?>" min="0" max="999">
					</div>
					<div class="col-4">
						<select class="form-control js-select2" style="width: 100%" name="FA_UsefulLifeNoUOM">
							<option value=""></option>
							<option value="M" <?= ($header['FA_UsefulLifeNoUOM'] == 'M' ? 'selected' : '') ?>>Month(s)</option>
							<option value="Y" <?= ($header['FA_UsefulLifeNoUOM'] == 'Y' ? 'selected' : '') ?>>Year(s)</option>
						</select>
					</div>	
				</div>
				<div class="form-group row">
					<label class="control-label col-4" style="padding-right: 0px">Accu. Depre. Beg.:</label>
						<div class="col-4">
							<select class="form-control js-select2" id="FA_AccuDepreBegCY" style="width: 100%" disabled>
								<?php foreach ($currency as $key => $value): ?>
									<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_AcquisitionNetBookCY'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-4">
							<input type="text" class="form-control text-right" id="FA_AccuDepreBeg" readonly value="0">
					    </div>
				</div>
				<div class="form-group row">
					<label class="control-label col-4" style="padding-right: 0px">Accu. Depre. End.:</label>
						<div class="col-4">
							<select class="form-control js-select2" id="FA_AccuDepreEndCY" style="width: 100%" disabled>
								<?php foreach ($currency as $key => $value): ?>
									<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_AcquisitionNetBookCY'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-4">
							<input type="text" class="form-control text-right" id="FA_AccuDepreEnd" readonly value="0">
					    </div>
				</div>
				<div class="form-group row">
					<label class="control-label col-4">Depre. - Monthly:</label>
						<div class="col-4">
							<select class="form-control js-select2" id="FA_DepreMonthlyCY" style="width: 100%" disabled>
								<?php foreach ($currency as $key => $value): ?>
									<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_AcquisitionNetBookCY'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-4">
							<input type="text" class="form-control text-right" id="FA_DepreMonthly" readonly value="0">
					    </div>
				</div>
				<div class="form-group row">
					<label class="control-label col-4">Depre. - YTD:</label>
						<div class="col-4">
							<select class="form-control js-select2" id="FA_DepreYTDCY" style="width: 100%" disabled>
								<?php foreach ($currency as $key => $value): ?>
									<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['FA_AcquisitionNetBookCY'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-4">
							<input type="text" class="form-control text-right" id="FA_DepreYTD" readonly value="0">
					    </div>
				</div>
			</div>