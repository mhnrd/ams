<style>
.ibox-content, .ibox-title, .panel-body{
	background: #DDD8E7!important;
	width: 100%;
}
tfoot {
	display: table-header-group;
}
@media screen and (max-width: 425px){
	#advance-search-small{
		display: inline!important;
	}
	.table-tools{
		margin-right: 5px!important;
		margin-top: 5px!important;
	}
	#clear-search-small{
		margin-top: 4px!important;
	}
}
/* Dropdown Button */
.dropbtn {
  background-color: #1D84C6;
  color: white;
  padding: 6px 6px 6px;
  font-size: 10px;
  border: none;
  transition: 0.3s;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  right: 0;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #58CCED;}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<section>
<!-- <section class="wrapper wrapper-content"> -->
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<div class="pull-left m-r-md">
				<a href="<?= DOMAIN.'dashboard' ?>" id="home">
					<i class="fa fa-home" style="font-size:25px; margin-top: 7px"></i>
				</a>
      </div>
      <div class="pull-right">
      	<a href="<?= DOMAIN.'dashboard' ?>" class="btn btn-outline btn-xs btn-warning" role="button" style="margin-top: 7px">
      		<i class="fa fa-reply"></i>
      	</a>
      </div>
			<h2 style="font-weight: bold; padding-top: 1px; color: #2980b9!important" class="text-success">ASSET LIST</h2>
		</div>
		
		<div class="ibox-content">
			<?= generate_filter($filter) ?>
			<?= generate_table($table_hdr)?>
		</div>
	</div>

<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.js"></script>
<!-- Sweet alert -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Moment -->
<script src="<?php echo EXTENSION ?>moment.js"></script>
<!--Select2 -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
<!-- Date Range Picker -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Page-Level Scripts -->
<script type="text/javascript">
	var tableindex;
	var col_center		= <?= json_encode($dtl_center) ?>;
	var process_action 	= 'Asset List';
	var table_name 		= 'tblFA';
	var controller_url 	= 'asset_management/asset_list/';
	var table_id 		= '<?= key($table_hdr) ?>';
	var module_url 		= 'asset_management/asset_list';
</script>
<script src="<?php echo JS_DIR ?>app/asset_management/asset_list/index.js"></script>
<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
<script src="<?php echo JS_DIR ?>app/reports/report.js"></script>
<script src="<?php echo JS_DIR ?>app/rules/index-search.js"></script>
<style>
	/*@media screen and (max-width: 425px){
		.pull-right{
			text-align: center!important;
		}
	}*/
</style>

</section>