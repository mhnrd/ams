<style>
.ibox-content, .ibox-title{
	background: #DDD8E7!important;
}
.nav-link:not(.active){
  color: black!important;
}

.nav-tabs .nav-link.active{
	color: #3585BC!important;
	border-color: #3585BC #3585BC #fff;
}
.panel-body{
	padding-left: 0px!important;
	padding-right: 0px!important;
}
tfoot {
	display: table-header-group;
}
@media screen and (max-width: 425px){
	#advance-search-small{
		display: inline!important;
	}
	.table-tools{
		margin-right: 5px!important;
		margin-top: 5px!important;
	}
	#advance-search{
		display: none;
	}
	#scan_rfid_filter{
		margin-left: -50px!important;
	}
	#clear-search-small{
		margin-top: 4px!important;
	}
	#scan_rfid_filter{
		margin-right: 0px!important;
	}
}

.dataTables_scrollHeadInner {
	width: 100% !important;
}
.dataTables_scrollHeadInner table {
	width: 100% !important;
}

@media screen and (max-width: 768px){
	#scan_rfid_filter input{
		width: 60%!important;
	}
	#scan_rfid_filter{
		margin-right: 500px!important;
	}
}

/* Dropdown Button */
.dropbtn {
  background-color: #1D84C6;
  color: white;
  padding: 6px 6px 6px;
  font-size: 10px;
  border: none;
  transition: 0.3s;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  right: 0;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #58CCED;}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Datepicker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker.css" rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">

<section>
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<!-- <div class="pull-left m-r-md">
                <i class="fa fa-list text-success mid-icon" style="margin-bottom: 10px!important"></i>
            </div> -->
            <div class="pull-left m-r-md">
	            <a href="<?= DOMAIN.'dashboard' ?>" id="home">
					<i class="fa fa-home" style="font-size:25px; margin-top: 7px"></i>
				</a>
            </div>
            <div class="pull-right">
            	<a href="<?= DOMAIN.'asset_management/asset_list' ?>" id="back" class="btn btn-sm btn-outline btn-xs btn-warning" role="button" style="margin-top: 6px">
            		<i class="fa fa-reply"></i>
            	</a>
            	<?php if ($type != 'view'): ?>
            	<a href="javascript:void(0)" class="btn btn-sm btn-outline btn-xs btn-success save" role="button" style="margin-top: 6px" data-action="save-close"><i class="glyphicon glyphicon-floppy-saved"></i></a>
            	
				  	<?php if ($type == 'update' && count($access_detail) > 0): ?>
						<div class="dropdown">
						  <button class="dropbtn"><i class="fa fa-gear"></i> <i class="fa fa-caret-down"></i></button>
						  <div class="dropdown-content" style="float: right">
							    <?php foreach ($access_detail as $value): ?>
							    	<a href="javascript:void(0)" id="<?= str_replace(' ', '', $value) ?>"><?= $value ?></a>
							  	<?php endforeach ?>
						  </div>
						</div>
				  	<?php endif ?>
            	<?php endif ?>
            </div>
			<h2 style="font-weight: bold; padding-top: 2px; color: #2980b9!important" class="text-success">ASSET INFORMATION</h2>
		</div>
		<div class="ibox-content" style="padding-left: 0px!important; padding-right: 0px!important" align='left'>
				<div id="form-header">
					<div class="form-group row">
						<?php if ($type == 'add'): ?>
							<label class="control-label col-3">Asset ID:</label>	
							<div class="col-4" style="padding-left: 5px!important; padding-right: 5px!important">
								<input type="text" class="form-control" name="FA_ID" readonly value="<?= $header['FA_ID'] ?>">
							</div>
						<?php else: ?>
							<label class="control-label col-7" style="font-size: 12px!important; color: #2980b9!important; font-weight: bold;"><?= strtoupper($header['FA_Description'].'  ['.$header['FA_ID'].']') ?></label>
							<input type="hidden" class="form-control" name="FA_ID" readonly value="<?= $header['FA_ID'] ?>">
						<?php endif ?>

						<?php if ($type != 'add'): ?>
							<label class="control-label col-1 text-success text-center"><span class="fa fa-rss"></span></label>	
							<div class="col-4 col-md-4" style="padding-left: 5px!important; padding-right: 15px!important">
								<input type="text" class="form-control" name="FA_RFID" value="<?= $header['FA_RFID'] ?>" readonly>
							</div>
							
						<?php endif ?>

					</div>
					<div class="tabs-container">
						 <ul class="nav nav-tabs" role="tablist">
						 	<?php foreach (get_sub_modules(300000) as $key => $value): ?>
						 			<?php if ($type == 'add'): ?>
						 				<?php if ($value['M_DisplayName'] != 'Asset Ledger' && $value['M_DisplayName'] != 'Components'): ?>
						 					<li><a style="padding: 6px!important" class="nav-link <?= ($key+1 == 1 ? 'active' : '') ?>" data-toggle="tab" href="#tab-<?= $key+1 ?>"><?= $value['M_DisplayName'] ?></a></li>
						 				<?php endif ?>
						 			<?php else: ?>
		                            	<li><a style="padding: 6px!important" class="nav-link <?= ($key+1 == 1 ? 'active' : '') ?>" data-toggle="tab" href="#tab-<?= $key+1 ?>"><?= $value['M_DisplayName'] ?></a></li>
						 			<?php endif ?>
		                    <?php endforeach ?>
			             </ul>
			    		<div class="tab-content">
			    			<?php foreach (get_sub_modules(300000) as $key => $value): ?>
		                        <div role="tabpanel" id="tab-<?=$key + 1?>" class="tab-pane <?= ($key+1 == 1 ? 'active' : '') ?>">
		                            <div class="panel-body">
		                            	<?php if ($type == 'add'): ?>
		                            		<?php if ($value['M_DisplayName'] != 'Asset Ledger' && $value['M_DisplayName'] != 'Components'): ?>
		                            			<?php $this->load->view($value['M_Trigger']) ?>
		                            		<?php endif ?>
		                            	<?php else: ?>	
		                            		<?php $this->load->view($value['M_Trigger']) ?>
		                            	<?php endif ?>
		                            </div>
		                        </div>
			    			<?php endforeach ?>
						</div>
					</div>
				</div>
		</div>
		<?php $this->load->view('asset_management/asset_list/change_location'); ?>
		<?php $this->load->view('asset_management/asset_list/dispose'); ?>
		<?php $this->load->view('asset_management/asset_list/maintenance_log'); ?>
		<?php $this->load->view('asset_management/asset_list/components-template'); ?>
	</div>



<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.js"></script>
<!-- Sweet alert -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Moment -->
<script src="<?php echo EXTENSION ?>moment.js"></script>
<!-- Sweet Alert -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Datepicker -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<!--Select2 -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
<!-- Page-Level Scripts -->
<script type="text/javascript">
	var table_movement;
	var table_maintenance;
	var table_components;
	var last_location			= '<?= $header['FA_DepartmentID'] ?>';
	var last_affiliates			= '<?= $header['FA_AffiliatesID'] ?>';
	var id;
	var module_url 				= '<?= DOMAIN.'/asset_management/asset_list' ?>';
	var module_folder 			= 'asset_management';
	var module_controller 		= 'Asset_list';
	var module_name 			= 'Asset List';
	var isRequiredDetails 		= false;
	var output_type 			= '<?= $type ?>';
	var status 					= '<?= $header['FA_Status'] ?>';
	var process_action 			= 'Asset Information';
	var update_link				= '<?= './update?id='.md5($header['FA_ID'])?>';
	var table_name 				= 'tblFA';
	var controller_url 			= 'asset_management/asset_list/';
	var table_id_movement		= '<?= key($table_movement) ?>';
	var table_id_maintenance 	= '<?= key($table_maintenance) ?>';
	var table_id_components		= '<?= key($table_components) ?>';
	var col_center_movement	 	= '<?= json_encode($col_center_movement) ?>';
	var col_center_maintenance 	= '<?= json_encode($col_center_maintenance) ?>';
	var col_center_components 	= '<?= json_encode($col_center_components) ?>';
</script>
<script src="<?php echo JS_DIR ?>app/asset_management/asset_list/form.js"></script>
<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
<!-- <script src="<?php echo JS_DIR ?>app/rules/advsearch.js"></script> -->
<style>
	div.dataTables_length {
	  float: left;
	}
	div.dataTables_filter {
	  float: right;
	}
	.dataTables_length label select{
		width: 50%!important;
	}
	.dataTables_length{
		padding-top: 2px!important;
	}
	@media screen and (max-width: 376px){
		.dataTables_filter label input{
			width: 70%!important;
		}
		.dataTables_filter{
			width: 50%!important;
			text-align: right!important;
		}
	}
</style>

</section>