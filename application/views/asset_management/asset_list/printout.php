<link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/css/bootstrap3.min.css">
<style type="text/css">

main{
	padding: 5px 5px 5px 5px;
	width: 45%;
	color: black!important;
	
}

main table tbody tr td, th{
	font-family: 'Arial Black!important';
}

table{
	width: 100%;
    table-layout: fixed;
    word-wrap: break-word;
    font-weight: 900;
    /*border: 1px solid black;*/
}

/*.header, .header td{
    font-size: 12px;
    color: black!important;
}*/

.detail, .detail td{
	font-size: 22px;
	font-weight: 900;
	color: black!important;
	width: 60%;
}

table.detail, table.detail td, table.detail th{
	padding: 5px;
	font-weight: bold;
	padding-left: 10px;
}

table.header, table.header td, table.header th{
	border-color: black;
	padding: 20px;
}

/*.bottomborder td{
	border-bottom: 1px solid black;
}

.header > div > table > tr > td strong{
	font-size:14px;
	font-weight:bold;
}

.colheader{
	width: 33.33%!important;
}*/

</style>
<!-- <htmlpageheader name="header">

</htmlpageheader> -->

<main>
	<table class="table header">
		  <tr>
		    <td><img width="240" height="240" src="<?= DOMAIN.'qrcode/'.$qr_code['img_name'] ?>" alt="<?= $barcode ?>"></td>
		  </tr>
	</table>
	<table class="table detail" style="width: 80%">
		  <tr>
		    <td colspan="2"><?= $header['FA_Description'] ?></td>
		  </tr>
		  <tr>
		    <td>Asset ID:</td>
		    <td><?= $header['FA_ID'] ?></td>
		  </tr>
		  <!-- <tr>
		    <td>RFID Tag:</td>
		    <td><?= $header['FA_RFID'] ?></td>
		  </tr> -->
		  <tr>
		    <td>Date Acquired:</td>
		    <td><?=  date_format(date_create($header['FA_AcquisitionDate']),'m/d/Y') ?></td>
		  </tr>
	</table>
</main>