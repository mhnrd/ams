<div class="modal inmodal animated fadeInUp" id="detail-template" data-backdrop="static" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<strong><?= strtoupper($module) ?></strong>
				</div>
				<div class="modal-body">
					<form id="form-header">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Code:</label>
										</div>
										<div class="col-8">
											<input type="hidden" required class="form-control p-xxs" name="AD_ID" value="">
											<input type="text" required class="form-control p-xxs" name="AD_Code" value="">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Description:</label>
										</div>
										<div class="col-8">
											<input type="text" required class="form-control p-xxs" name="AD_Desc" value="">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4">
											<label>Active:</label>
										</div>
										<div class="col-8">
											<input type="checkbox" class="" name="AD_Active" value="">
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-outline btn-xs save-attribute" id="save-new" data-action="save-new" value="Add"><span class="glyphicon glyphicon-floppy-disk"></span> Add and New</button>
					<button type="button" class="btn btn-primary btn-outline btn-xs save-attribute" id="save-close" data-action="save-close" value="Add"><span class="glyphicon glyphicon-floppy-disk"></span> Add and Close</button>
					<button class="btn btn-warning btn-outline btn-xs" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>	
				</div>
			</div>
		</div>
	</div>