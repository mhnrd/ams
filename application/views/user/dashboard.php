<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<link href="<?php echo EXTENSION ?>inspinia/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">

<style>
.ibox-content, .ibox-title, .panel-body{
	background: #DDD8E7;
}
</style>

<section>
	<div class="ibox">
		<div class="ibox-title" align="center" style="padding-left: 10px!important; padding-right: 10px!important">
            <h2 style="font-weight: bold!important; color: #2980b9!important" class="text-success">ASSET MANAGEMENT SYSTEM</h2>
		</div>
		<div class="ibox-content">	
			<div class="col-12" align="center">
				<div class="form-horizontal">
					<div class="row">
						<?php foreach (get_mother_modules() as $key => $value): ?>
							<div class="form-group col-6" align="center">
								<a href="<?php echo DOMAIN.$value['M_Trigger'] ?>">
									<div align="center" style="font-weight: bold; color: white!important; background: #2980b9; height: 100px; border-radius: 20px; padding: 10px">
										<span class="<?= $value['M_Icon'] ?>" style="font-size: 50px; color: white; padding-top: 6px"></span>
										<div style="padding-top: 5px"><?= strtoupper($value['M_DisplayName']) ?></div>
									</div>
								</a>
							</div>
						<?php endforeach; ?>
						<div class="form-group <?= (count(get_mother_modules()) % 2 == 0 ? 'col-6 offset-3' : 'col-6') ?>" align="center">
								<a href="<?php echo DOMAIN ?>logout">
									<div style="font-weight: bold; color: white!important; background: #2980b9; height: 100px; border-radius: 20px; padding: 10px">
											<span class="fa fa-sign-out" style="font-size: 50px; color: white; padding-top: 6px"></span>
											<div style="padding-top: 5px">LOGOUT</div>
									</div>
								</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/fullcalendar/moment.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	
	<!-- Full Calendar -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/fullcalendar/fullcalendar.min.js"></script>
	<!-- Page-Level Scripts -->

	<script>

	    $(document).ready(function() {

	        /* initialize the calendar
	         -----------------------------------------------------------------*/
	        $('.calendar').fullCalendar({
	            header: {
	                left: 'prev,next today',
	                center: 'title',
	                right: 'month,agendaWeek,agendaDay'
	            },
	            editable: false,
	            droppable: false
	        });

	        // $('#page-wrapper').removeClass('dashbard-1');
	        // $('#page-wrapper').removeAttr('id');
	        $('a.navbar-minimalize').remove();

	    });

	    $('.wrapper').removeClass('wrapper');
        $('body').removeClass('fixed-sidebar no-skin-config full-height-layout pace-done');

	</script>
	
</section>


