<link rel="stylesheet" href="<?php echo CSS_DIR; ?>style_login.css"> 
<style type="text/css">
    body{
        background: gray!important;

    }
</style>

    <div class="loginColumns animated fadeInDown">
        <div class="row">
            <div class="col-md-6 offset-md-3 col-12">
                <div class="ibox-title" align="center" style="padding-bottom: 2px!important">
                    <img src="<?= IMG_DIR.'ytmi-login-2.png' ?>" alt="YTMI" width="220" height="70">
                    <h4 class="font-weight-lighter" align="center" style="color: black">ASSET MANAGEMENT SYSTEM</h4>
                </div>
                <div class="ibox-content" align="center" style="background: white!important">     
                    
                    <form class="m-t" role="form" method="POST" action="?">
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="Username" required="">
                        </div>
                         <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Password" required="">
                        </div>
                        <button type="submit" class="btn btn-success block full-width m-b">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<script>
    $(document).ready(function() {
        $('.wrapper').removeClass('wrapper');
        $('.pace-done').removeClass(' pace-done');
        $('body').removeAttr('style')
    });
</script>
