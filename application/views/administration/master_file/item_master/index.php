<style>
tfoot {
	display: table-header-group;
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins" id="ibox1">
		<div class="ibox-title">
			<h5>Item Master</h5>
			<div class="pull-right ibox-tools">
				<button class="btn btn-success" id="btnActive" disabled>
					<i class="fa fa-check"></i> Activate
				</button>
				<button class="btn btn-danger" id="btnDeactive" disabled>
					<i class="fa fa-times"></i> Deactivate
				</button>	
			</div>
		</div>
		<div class="ibox-content">
			<div class="table-responsive datatable">
				<table class="table table-striped table-bordered table-condensed table-hover dataTables-example compact" id="item-list">
					<thead>
						<tr>
							<th class="text-center col-xs-1" style="width: 10%">
								<?php if($access_header === true): ?>
								<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="<?= DOMAIN;?>master_file/item-master/add"><i class="fa fa-plus"></i></a>
								<?php endif; ?>
							</th>
							<th class="text-center col-xs-1"><input type="checkbox" id="chkSelectAll" class="icheckbox_square-green"></th>
							<th class="text-center col-xs-2 sort-asc">Item No</th>
							<th class="text-center col-xs-2">Barcode</th>
							<th class="text-center col-xs-3">Item Description</th>
							<th class="text-center col-xs-1">SOH</th>
							<th class="text-center col-xs-1">Base UOM</th>
							<th class="text-center col-xs-1">Status</th>
						</tr>
					</thead>
					<tfoot style="display: none"></tfoot>					
				</table>
			</div>
		</div>
	</div>

	<?php $this->load->view('master_file/item_master/soh-breakdown-template'); ?>

	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- Moment -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Page-Level Scripts -->
	<script type="text/javascript">
		var tableindex;
	</script>
	<script src="<?php echo JS_DIR?>app/master_file/item_master/index.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/advsearch.js"></script>
</section>