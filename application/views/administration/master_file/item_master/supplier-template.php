<style type="text/css">	.clockpicker-popover { z-index: 999999 !important;} div.form-group{margin-bottom: 5px;}</style>
<div class="modal inmodal animated fadeInUp" id="supplier-detail" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content<?php /* animated flipInY */ ?>">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px; color: black!important;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Supplier - Item Information</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body" style="padding-bottom: 10px">

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Supplier:</label>
								<div >
									<select id="supplier" data-type="supplier-name" class="item" style="width: 100%">
										<option></option>
									</select>	
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Item Code</label>
								<div >
									<input id="supplier-item-code" type="text"  class="text-right form-control "/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Last PO Cost:</label>
								<div >
									<input id="unit-cost" type="number" class="text-right form-control" data-unit-cost="0.00" value="0.00"/> 
								</div>
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Old Item Code:</label>
								<div>
									<input id="old-item-code" type="text" class="text-right form-control"  value=""/> 
								</div>
							</div>
							<div class="form-group" class="form-check">
								<label class="control-label" class="control-label" for="active" style="padding-left: 0px!important">Active:</label>
								<div style="margin-top: 5px;">
									 <input class="form-check-input i-checks" type="checkbox"  name="IM_Supplier_Active" id="active">
								</div>
							</div>
						</div>	
					</div>	
				</div>

				<!-- MODAL FOOTER -->
				<div class="modal-footer bg-default">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="add-new" class="btn btn-primary" value="add">Add & New</button>
					<button type="button" id="add-close" class="btn btn-primary" value="add">Add & Close</button>	
				</div>
		</div>
	</div>
</div>