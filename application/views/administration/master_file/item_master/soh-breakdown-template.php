<style type="text/css">	.clockpicker-popover { z-index: 999999 !important;} div.form-group{margin-bottom: 5px;}</style>
<div class="modal inmodal animated fadeInUp" id="soh-detail" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-lg">
		<div class="modal-content<?php /* animated flipInY */ ?>">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px; color: black!important;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Stock on Hand Breakdown</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body" style="padding-bottom: 10px">
					<div id="form-soh-breakdown" class="form-horizontal">
						<div class="row">
							<div class="form-group">
								<label class="col-md-2 control-label">Item No:</label>
								<div class="col-md-10">
									<input type="text" id="barcode-item" style="width: 95%" readonly name="IM_Item_Id" class="form-control"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Item Desc:</label>
								<div class="col-md-10">
									<input type="text" id="barcode-desc" style="width: 95%" readonly name="IM_Sales_Desc" class="form-control"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">UOM:</label>
								<div class="col-md-10">
									<input type="text" id="barcode-seq-no" style="width: 95%" readonly name="AD_Code" class="form-control"/>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="hr-line-dashed"></div>
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" id="soh-detail-list">
									<thead>
										<tr>
											<th class="col-xs-6 text-center" style="background: #FFFFFF">Location</th>
											<th class="col-xs-4 text-center" style="background: #FFFFFF">Sub-location</th>
											<th class="col-xs-2 text-center" style="background: #FFFFFF">Qty</th>		
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				
		</div>
	</div>
</div>