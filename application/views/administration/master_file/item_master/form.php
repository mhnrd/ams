<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">


<section  class="wrapper wrapper-content">
	
		<div class="ibox-title">
			<h5>Item Master</h5>
			<div class="pull-right ibox-tools">
				<a href="<?= DOMAIN."master_file/".$this->uri->segment(2); ?>" class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back">
					<span class="fa fa-reply"></span>					
				</a>
				<?php if($type != 'view'): ?>
					<button id="save" type="button" class="btn btn-primary btn-outline" data-toggle="tooltip" data-placement="bottom" title="<?= ($type == 'add') ? "Save" : "Update" ?>" data-todo="<?= $type ?>" data-doc-no="<?= ($header['IM_Item_Id'])?>">
						<span class="glyphicon glyphicon-floppy-disk"></span>
					</button>	
				<?php endif; ?> 
			</div>
		</div>

		<div class="ibox-content">	
			<!-- IDENTIFIER -->
			<div class="row">
				<div class="col-md-6">
					<h4>Identifier</h4>
					<div class="form-horizontal">									
							<div class="form-group">
								<label class="control-label col-md-4">Item Type:</label>
								<div class="col-md-8">
									<?php if($type == 'add'): ?>
										<select name="IM_FK_ItemType_id" class="js-select2 required-field" style="width: 100%">
											<?= ($type == 'add') ? "<option selected disabled></option>" : "" ?>
											<?php foreach($item_type as $row){ ?>
												<option value="<?= $row['IT_id'] ?>" <?= ($header['IM_FK_ItemType_id'] == $row['IT_id']) ? 'selected' : ''?>><?= $row['IT_Description'] ?></option>
											<?php }?>
										</select>
									<?php else: ?>
										<input type="text" name="IM_FK_ItemType_id" value="<?= $detail['ItemType_name']?>" readonly class="form-control" data-itemtype-id="<?= $detail['ItemType_id']?>">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Category:</label>
								<div class="col-md-8">
									<?php if($type == 'add'): ?>
										<select type="text" class="form-control js-select2 required-field" style="width: 100%" name="IM_FK_Category_id" >
										</select>
									<?php else: ?>
										<input type="text" name="IM_FK_Category_id" value="<?= $detail['Category_name']?>" readonly class="form-control" data-category-id="<?= $detail['Category_id']?>">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Sub Category:</label>
								<div class="col-md-8">
									<?php if($type == 'add'): ?>
										<select type="text" class="form-control js-select2 required-field" style="width: 100%" name="IM_FK_SubCategory_id" >
										</select>
									<?php else: ?>
									<input type="text" name="IM_FK_SubCategory_id" value="<?= $detail['SubCategory_name']?>" readonly class="form-control" data-subcategory-id="<?= $detail['SubCategory_id']?>">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Production Area:</label>
								<div class="col-md-8">
									<?php if($type == 'add' || $type == 'update'): ?>
										<select type="text" class="form-control not-required-field" style="width: 100%" name="IM_ProductionArea">
											<option></option>
											<?php foreach($all_location as $location){ ?>
												<option data-location-id="<?php echo $location['SP_ID'] ?>" value="<?php echo $location['SP_ID'] ?>" <?php echo (($type == 'add' ? getDefaultLocation() : $detail['Production_area']) == $location['SP_ID']) ? 'selected' : '' ?>><?= $location['SP_StoreName'] ?></option>
											<?php }?>
										</select>
									<?php else: ?>
									<input type="text" name="IM_ProductionArea" value="<?= $detail['Production_area_name']?>" readonly class="form-control" data-prod-area="<?= $detail['SubCategory_id']?>">
									<?php endif; ?>
								</div>
							</div>
							<?php /* ?>
							<div class="form-group">
								<label class="control-label col-md-4">Image:</label>
								<div class="col-md-8">
									<input type="file" class="form-control-file " name="" >
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4"></label>
								<div class="col-md-8">
									<picture>
									  <source srcset="..." type="image/svg+xml">
									  <img src="..." class="img-fluid img-thumbnail" alt="...">
									</picture>
								</div>
							</div>
							<?php */ ?>
					</div>								
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<!-- GENERAL INFORMATION -->
			<div class="row">
				<div class="col-md-12">
					<h4>General Information</h4>
					<div class="col-md-6">
						<div class="form-horizontal">
							<div class="form-group">
								<label class="control-label col-md-4">Item Code:</label>
									<div class="col-md-8">
										<input type="text" class="form-control required-field" name="IM_Item_Id" readonly value="<?= $header['IM_Item_Id'] ?>">
									</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Universal Product Code:</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="IM_UPCCode" value="<?= $header['IM_UPCCode'] ?>">
									</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Item Description:</label>
									<div class="col-md-8">
										<input type="text" class="form-control required-field" name="IM_Sales_Desc" value="<?= $header['IM_Sales_Desc'] ?>">
									</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Purchase Description:</label>
									<div class="col-md-8">
										<input type="text" class="form-control required-field" name="IM_Purchased_Desc" value="<?= $header['IM_Purchased_Desc'] ?>">
									</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Short Description:</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="IM_Short_Desc" value="<?= $header['IM_Short_Desc'] ?>">
									</div>
							</div>
							<div class="form-group" style="display:none!important">
								<label class="control-label col-md-4">Manufacturing No:</label>
									<div class="col-md-8">
										<input type="text" class="form-control " name="IM_ManufacturerPartNo" value="<?= $header['IM_ManufacturerPartNo'] ?>" >
									</div>
							</div>
							<div class="form-group" style="display:none!important">
								<label class="control-label col-md-4">Model No:</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="IM_Model" value="<?= $header['IM_Model'] ?>">
									</div>
							</div>
							<div class="form-group" style="display:none!important">
								<label class="control-label col-md-4">Weight:</label>
									<div class="col-md-8">
										<input type="number" class="form-control text-right" name="IM_Weight" value="<?= number_format($header['IM_Weight'],2) ?>" >
									</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Stock On Hand:</label>
									<div class="col-md-8">
										<input type="text" class="form-control text-right" name="IM_SOH" style="padding-right: 27px" value="<?= number_format($header['IM_SOH'],2) ?>" readonly >
									</div>
							</div>
							<div class="form-group" style="display:none!important">
								<label class="control-label col-md-4">Landed Cost:</label>
								<div class="col-md-8">
									<input type="number" class="form-control text-right" name="IM_CostOfGoods" readonly="" value="<?= number_format($header['IM_CostOfGoods'],2) ?>">
								</div>
							</div>
							<div class="form-group" style="display:none!important">
								<label class="control-label col-md-4">Unit Price:</label>
								<div class="col-md-8">
									<input type="number" class="form-control text-right required-field" name="IM_UnitPrice" value="<?= number_format($header['IM_UnitPrice'],2) ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Waste(%):</label>
								<div class="col-md-8">
									<input type="number" class="form-control text-right" name="IM_Item_Waste" value="<?= number_format($header['IM_Item_Waste'],2) ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Expiry Days:</label>
								<div class="col-md-8">
									<input type="number" class="form-control text-right" name="IM_ExpiryDays" value="<?= number_format($header['IM_ExpiryDays'],0) ?>">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-horizontal">
							<div class="form-group">
								<label class="control-label col-md-4">Base UOM:</label>
								<div class="col-md-8">
									<?php if($type != 'view'): ?>
										<select name="IM_FK_Attribute_UOM_id" class="js-select2 required-field" style="width: 100%">
											<?= ($type == 'add') ? "<option selected disabled></option>" : "" ?>
											<?php foreach($uom_list as $row){ ?>
											<option value="<?= $row['AD_Id'] ?>" <?= ($header['IM_FK_Attribute_UOM_id'] == $row['AD_Id']) ? 'selected' : ''?>><?= $row['AD_Code'] ?></option>
											<?php }?>
										</select>
									<?php else: ?>
										<input type="text" name="IM_FK_Attribute_UOM_id" value="<?= $detail['BaseUOM_name']?>" readonly class="form-control">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Sales UOM:</label>
								<div class="col-md-8">
									<?php if($type != 'view'): ?>
										<select name="IM_SalesUOM" class="js-select2 required-field" style="width: 100%">
											<?= ($type == 'add') ? "<option selected disabled></option>" : "" ?>
											<?php foreach($uom_list as $row){ ?>
											<option value="<?= $row['AD_Id'] ?>" <?= ($header['IM_SalesUOM'] == $row['AD_Id']) ? 'selected' : ''?>><?= $row['AD_Code'] ?></option>
											<?php }?>
										</select>
									<?php else: ?>
										<input type="text" name="IM_SalesUOM" value="<?= $detail['SalesUOM_name']?>" readonly class="form-control">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Purchase UOM:</label>
								<div class="col-md-8">
									<?php if($type != 'view'): ?>
										<select name="IM_PurchaseUOM" class="js-select2 required-field" style="width: 100%">
											<?= ($type == 'add') ? "<option selected disabled></option>" : "" ?>
											<?php foreach($uom_list as $row){ ?>
											<option value="<?= $row['AD_Id'] ?>" <?= ($header['IM_PurchaseUOM'] == $row['AD_Id']) ? 'selected' : ''?>><?= $row['AD_Code'] ?></option>
											<?php }?>
										</select>
									<?php else: ?>
										<input type="text" name="IM_PurchaseUOM" value="<?= $detail['PurchUOM_name']?>" readonly class="form-control">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Production UOM:</label>
								<div class="col-md-8">
									<?php if($type != 'view'): ?>
										<select name="IM_Production_UOM_id" style="width: 100%">
											<option></option>
										</select>
									<?php else: ?>
										<input type="text" name="IM_Production_UOM_id" value="<?= $header['ProductionUOM_Name']?>" readonly class="form-control">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Transfer UOM:</label>
								<div class="col-md-8">
									<?php if($type != 'view'): ?>
										<select name="IM_Transfer_UOM_id" style="width: 100%">
											<option></option>
										</select>
									<?php else: ?>
										<input type="text" name="IM_Transfer_UOM_id" value="<?= $header['TransferUOM_Name']?>" readonly class="form-control">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Order Forecast UOM:</label>
								<div class="col-md-8">
									<?php if($type != 'view'): ?>
										<select name="IM_OF_UOM_id" style="width: 100%">
											<option></option>
										</select>
									<?php else: ?>
										<input type="text" name="IM_OF_UOM_id" value="<?= $header['OF_UOM_Name']?>" readonly class="form-control">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Last PO Cost:</label>
								<div class="col-md-8">
									<input type="number" class="form-control text-right" name="IM_LastPOCost" readonly value="<?= ($type == 'add') ? number_format(0,2) : number_format($header['IM_LastPOCost'],2) ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Unit Cost:</label>
								<div class="col-md-8">
									<input type="number" class="form-control text-right required-field" name="IM_UnitCost" value="<?= number_format($header['IM_UnitCost'],2) ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Selling Price:</label>
								<div class="col-md-8">
									<input type="number" class="form-control text-right required-field" name="IM_SalePrice" value="<?= number_format($header['IM_SalePrice'],2) ?>">
								</div>
							</div>
							<div class="form-group" <?= ($type != 'add' ? 'style="display: none"' : '') ?>>
								<label class="control-label col-md-4">Scrap:</label>
								<div class="col-md-8">
									<input type="checkbox" class="chk-scrap" name="chkScrap" value="<?= $header['IM_Scrap'] ?>" style="margin-top: 10px" <?= $header['IM_Scrap'] == true ? 'checked' : '' ?>>
								</div>
							</div>
							<div class="form-group source-fg" <?= ($type == 'add' ? 'style="display: none"' : ($header['IM_Scrap'] == false ? 'style="display: none"' : '')) ?>>
								<label class="control-label col-md-4">Source FG:</label>
								<div class="col-md-5">
									<select name="IM_Scrap_Item" <?= ($type != 'add' ? 'disabled' : '') ?> id="scrap-item" class="form-control scrap-select2" style="width: 110%">
										<option></option>
										<?php if($type != 'add'): ?>
											<option value="<?= $header['IM_Scrap_Item_Id'] ?>" selected><?= $header['ScrapParentDesc'] ?></option>
										<?php endif; ?>
									</select>
								</div>
								<div class="col-md-3">
									<label class="control-label col-md-4">Pct:</label>
									<div class="col-md-8">
										<input type="number" style="margin-left: 15px" name="scrap-pct" class="form-control text-right" value="<?= ($type == 'add' ? '' : number_format($header['IM_Scrap_Perc'], 2, ".", "")) ?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- supplier - Item Information -->
			<div class="hr-line-dashed"></div>
			<h4>Supplier - Item Information</h4>
			<div class="table-responsive">				
				<table id="supplier-detail-list" class="table table-striped table-bordered table-hover " >
					<thead>
						<tr>
							<?php if($type != 'view'): ?>
								<th class="col-xs-1 text-center">
									<button type="button" id="add-supp" class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="#"><i class="fa fa-plus"></i></button>
								</th>
							<?php endif; ?>
							<th class="text-center col-xs-3">Supplier Name</th>
							<th class="text-center col-xs-2">Supplier<br>Item Code</th>
							<th class="text-center col-xs-2">Old Item Code</th>
							<th class="text-center col-xs-2">Last PO Cost</th>
							<th class="text-center col-xs-1">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php if($type == 'update' || $type == 'view'): ?>
							<?php foreach ($detail['Supplier_list'] as $key => $row): ?>
								<tr class="supplier-detail" data-action="<?= $type ?>" data-line-no="<?= $key+1 ?>">
									<?php if($type != 'view'): ?>
										<td class="text-center">
											<button class="edit-supplier-detail btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" style="margin-right:10px;"><i class="fa fa-pencil"></i></button>
											<button class="delete-supplier-detail btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" style=""><i class="fa fa-trash-o"></i></button>
										</td>
									<?php endif; ?>
									<td data-supplier-id="<?= $row['IS_FK_Suppllier_id'] ?>" data-supplier-name="<?= $row['S_Name'] ?>"><?= $row['S_Name'] ?></td>
									<td class="text-center" data-supplier-item-code="<?= $row['IS_SupplierItemCode'] ?>"><?= $row['IS_SupplierItemCode'] ?></td>
									<td data-old-item-code="<?= $row['IS_OldItemCode'] ?>"><?= $row['IS_OldItemCode'] ?></td>
									<td class="text-right" data-unit-cost="<?= $row['IS_LastPOCost'] ?>"><?= number_format($row['IS_LastPOCost'],2) ?></td>
									<td class="text-center" data-status="<?= ($row['IS_Active'] == 1) ? "true" : "false" ?>"><?= ($row['IS_Active'] == 1) ? "Active" : "Inactive"  ?></td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
			<!-- UOM Conversion / Item Classification and Grouping -->
			<div class="hr-line-dashed"></div>
			<div class="row">
				<div class="col-md-6">
					<h4>UOM Conversion</h4>
					<div class="table-responsive">				
						<table id="uom-detail-list"  class="table table-striped table-bordered table-hover " >
							<thead>
								<tr>
									<?php if($type != 'view'): ?>
										<th class="col-xs-2 text-center">
											<button type="button" id="add-uom" class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="#"><i class="fa fa-plus"></i></button>
										</th>
									<?php endif; ?>
									<th class="text-center col-xs-5">UOM</th>
									<th class="text-center col-xs-5">Qty</th>
								</tr>
							</thead>
							<tbody>
								<?php if($type == 'update' || $type == 'view'): ?>
									<?php foreach ($detail['UOM_list'] as $key => $row): ?>
											<tr class="uom-detail" data-action="<?= $type ?>" data-line-no="<?= $key+1 ?>">
												<?php if($type != 'view'): ?>
													<td class="text-center">
														<button class="edit-uom-detail btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button"><i class="fa fa-pencil"></i></button> 
														<button class="delete-uom-detail btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" style=""><i class="fa fa-trash-o"></i></button>
													</td>
												<?php endif; ?>
												<td data-uom-name="<?= $row['AD_Code'] ?>" data-uom="<?= $row['IUC_FK_UOM_id'] ?>" class="text-center"><?= $row['AD_Code'] ?></td>
												<td class="text-right" data-quantity="<?= $row['IUC_Quantity'] ?>"><?= number_format($row['IUC_Quantity'],4) ?></td>
											</tr>
										<?php endforeach; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>	
				<div class="col-md-6">
					<h4>Item Classification and Grouping</h4>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="control-label col-md-4">Inv Posting Group:</label>
							<div class="col-md-8">
								<?php if($type != 'view'): ?>
									<select name="IM_INVPosting_Group" class="js-select2 required-field" style="width: 100%">
										<?= ($type == 'add') ? "<option selected disabled></option>" : "" ?>
										<?php foreach($inv_posting_group as $row){ ?>
										<option value="<?= $row['IPG_Code'] ?>" <?= ($header['IM_INVPosting_Group'] == $row['IPG_Code']) ? 'selected' : ''?>><?= ucwords($row['IPG_Code']) ?></option>
										<?php }?>
									</select>
								<?php else: ?>
									<input type="text" name="IM_INVPosting_Group" value="<?= ($detail['Inventory_PG_id']) ?>" readonly class="form-control">
								<?php endif; ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">VAT Posting Group:</label>
							<div class="col-md-8">
								<?php if($type != 'view'): ?>
									<select name="IM_VATProductPostingGroup" class="js-select2 required-field" style="width: 100%">
										<?= ($type == 'add') ? "<option selected disabled></option>" : "" ?>
										<?php foreach($vat_posting_group as $row){ ?>
										<option value="<?= $row['VPPG_Code'] ?>" <?= ($header['IM_VATProductPostingGroup'] == $row['VPPG_Code']) ? 'selected' : ''?>><?= ucwords($row['VPPG_Description']) ?></option>
										<?php }?>
									</select>
								<?php else: ?>
									<input type="text" name="IM_VATProductPostingGroup" value="<?= ucwords($detail['VAT_name'])?>" readonly class="form-control">
								<?php endif; ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">WHT Posting Group:</label>
							<div class="col-md-8">
								<?php if($type != 'view'): ?>
									<select name="IM_WHTProductPostingGroup" class="js-select2 required-field" style="width: 100%">
										<?= ($type == 'add') ? "<option selected disabled></option>" : "" ?>
										<?php foreach($wht_posting_group as $row){ ?>
										<option value="<?= $row['WPPG_Code'] ?>" <?= ($header['IM_WHTProductPostingGroup'] == $row['WPPG_Code']) ? 'selected' : ''?>><?= ucwords($row['WPPG_Description']) ?></option>
										<?php }?>
									</select>
								<?php else: ?>
									<input type="text" name="IM_WHTProductPostingGroup" value="<?= ucwords($detail['WHT_name'])?>" readonly class="form-control">
								<?php endif; ?>
							</div>
						</div>
						<div class="form-group" class="form-check" style="margin-top: 10px;display: none">
							<label class="control-label col-md-4" class="form-check-label" for="Serialized">Serialized:</label>
							<div class="col-md-8">
								 <input class="form-check-input i-checks" type="checkbox"  name="IM_Serialize" id="Serialized">
							</div>
						</div>
						<div class="form-group" class="form-check" style="display: none">
							<label class="control-label col-md-4" class="form-check-label" for="Imported">Imported:</label>
							<div class="col-md-8">
								 <input class="form-check-input i-checks" type="checkbox"  name="IM_Imported" id="Imported">
							</div>
						</div>
						<div class="form-group" class="form-check" style="display: none">
							<label class="control-label col-md-4" class="form-check-label" for="PAR">Fixed Asset:</label>
							<div class="col-md-8">
								 <input class="form-check-input i-checks" type="checkbox"  name="IM_PAR_Item" id="PAR">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
	<a href="#" class="btnTop btn btn-primary" role="button" data-toggle="tooltip" data-placement="top" title="Go to Top"><i class="fa fa-arrow-up"></i></a>
	

		
	<?php $this->load->view('master_file/item_master/supplier-template');?>
	<?php $this->load->view('master_file/item_master/uom-template');?>
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['IM_Item_Id'] ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/master_file/item_master' ?>';
		var item_isScrap			= '<?= $header['IM_Scrap'] ?>'

		if(output_type == 'update'){
			var prod_uom 	= '<?= $header['IM_Production_UOM_id'] ?>';
			var trans_uom 	= '<?= $header['IM_Transfer_UOM_id'] ?>';
			var of_uom 		= '<?= $header['IM_OF_UOM_id'] ?>';
		}
	</script>

	<script src="<?php echo JS_DIR?>app/master_file/item_master/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
	
	

</section>

