<style>
	tfoot{
		display: table-header-group;
	}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
	.select2-results__options{
	        font-size:11px !important;
	 }
	@media only screen and (max-width: 768px){
		.fixed-header{
			display: none;
		}
	}
</style>
<section id="add-cost-profit-center" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<label style="color:black"><h5>Cost/Profit Center</h5></label>
		</div>
		<div class="ibox-content"> 
			<?php
				/*
				Converting submit to ajax is more better in this project
				*/
			?>
			<div id="form-cost-profit-center" class="form-horizontal">
				<form id="form-header">
					<div class="row">
						<div class="col-12">
							<div class="col-md-5 col-12"> 
								<div class="form-group">
									<div class="row">	
										<div class="col-3" style="text-align: right;">	
											<label class="control-label"><b class="text-danger">*</b> CPC ID:</label>
										</div>
										<div class="col-9">
											<input type="text" id="CPC_Id" required name="CPC_Id"  class="form-control required-field" value="<?= $header['CPC_Id']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic;font-size: 12px; display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label"><b class="text-danger">*</b> Description:</label>
										</div>
										<div class="col-9">
											<input type="text" id="CPC_Desc" required name="CPC_Desc" class="form-control required-field" value="<?= $header['CPC_Desc']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label"><b class="text-danger">*</b> Cost/Profit Class:</label>
										</div>
										<div class="col-9">
											<select name="CPC_FK_Class" class="form-control cpcclass-js-select2 required-field" required style="width: 100%">
												<?php foreach($cpc_class_list as $cpc_class): ?>
													<option></option>
													<option value="<?= $cpc_class['AD_Id'] ?>" <?= ($header['CPC_FK_Class'] == $cpc_class['AD_Desc']) ? 'selected' : '' ?>> <?= $cpc_class['AD_Desc']?></option>
												<?php endforeach; ?>									
											</select>
											<div class="required-label text-danger" style="font-size: italic; font-size: 12px; display: none;">* Required Field</div>
										</div>
									</div>
								</div>	
							</div>	
						</div>
					</div>	
				</form>
			</div>
			<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['CPC_Id'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['CPC_Id'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['CPC_Id'] ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/master_file/cost_profit_center' ?>';
		var module_folder 			= 'administration/master_file';
		var module_controller 		= 'cost-profit-center'
		var module_name 			= 'Cost Profit Center'; 
		var isRequiredDetails 		= false;
		<?php if($type == 'view'): ?>
			
			var target_url = '<?= DOMAIN.'master_file/cost-profit-center/' ?>';
		<?php endif; ?>
	</script>
	<script src="<?php echo JS_DIR?>app/administration/master_file/cost_profit_center/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
	
</section>