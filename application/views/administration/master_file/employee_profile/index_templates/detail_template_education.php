<style>
hr{
		margin: 5px 0 5px 0;
	}

</style>
<div class="modal inmodal animated fadeInUp" id="detail-template-education" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-lg" style="width:90%">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4><span class="fa fa-graduation-cap"></span> Education</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<!-- GENERATE TABLE HERE -->
							<?= generate_table_standard($table_education)?>   
						</div>
					</div>
					<hr>
					<form id='form-education'>
						<input type="hidden" name="Emp_Id">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 no-margin-top text-right">Continue Study?</div>
										<div class="col-7">										
											<input name="ED_ContinueStudy" type="checkbox">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Course:</label>
										</div>
										<div class="col-7">
											<input readonly type="text" class="form-control p-xxs" name="ED_Course" value="">
										</div>
									</div>                               
						    	</div>
						    	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>When:</label>
										</div>
										<div class="col-7">
											<input readonly type="number" class="form-control p-xxs" name="ED_ContinueStudyWhen" value="">
										</div>
									</div>                               
						    	</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 no-margin-top text-right">Exams Taken?</div>
										<div class="col-7">
											<input name="ED_Exam" type="checkbox">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Type:</label>
										</div>
										<div class="col-7">
											<select required style="width:100%" name="ED_ExamType" class="form-control p-xxs" tabindex="1">
												<option value=""></option>
												<option value="1">Prof. Civil Service Exam</option>
												<option value="2">Non-Prof. Civil Service Exam</option>
												<option value="3">Others</option>
											</select>
										</div>
									</div>                               
						    	</div>
						    	<div class="form-group fieldExamDesc" style="display:none">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Description:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="ED_ExamDesc" value="">
										</div>
									</div>                               
						    	</div>
						    	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Rating:</label>
										</div>
										<div class="col-7">
											<input readonly type="number" class="form-control p-xxs" name="ED_ExamRating" value="" max="99" min="0">
										</div>
									</div>                               
						    	</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">					
					<button type="button" class="btn btn-warning btn-outline close-modal" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
					<button type="button" id="btnEducationUpdate" class="btn btn-primary btn-outline" value="Update"><span class="glyphicon glyphicon-floppy-disk"></span> Update</button>
				</div>
			</div>
		</div>
	</div>