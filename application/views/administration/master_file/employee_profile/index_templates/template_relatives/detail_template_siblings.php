<div class="modal inmodal animated fadeInUp" id="detail-template-siblings" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md" style="margin-top: 100px">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close close-modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4><span class="fa fa-users"></span> Siblings</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body">
					<form id='form-siblings'>
						<input type="hidden" name="mode">
						<input type="hidden" name="R_EmpId">
						<input type="hidden" name="R_RecordNum">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Name:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="R_Name" value="" tabindex="1">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Relation:</label>
										</div>
										<div class="col-7">
											<select required style="width:100%" name="R_FK_RelationAtt" class="form-control p-xxs" tabindex="2">
												<option value=""></option>
												<?php foreach($relationship_list as $row): ?>
													<option value="<?= $row['Id'] ?>">
														<?= ucfirst($row['Description']) ?>
													</option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Birthdate:</label>
										</div>
										<div class="col-7">
											<div class="input-group date">
												<input required type="text" class="form-control p-xxs" name="R_Birthdate" value="" tabindex="3">
	                    						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Status:</label>
										</div>
										<div class="col-7">
											<select required style="width:100%" name="Status" class="form-control p-xxs" tabindex="4">
												<option value=""></option>
												<option value="S">Single</option>
												<option value="M">Married</option>
												<option value="D">Divored</option>
												<option value="S1">Seperated</option>
											</select>
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Occupation:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="WorkType" value="" tabindex="5">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> Company and Address:</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="CompanyAdd" value="" tabindex="6">
										</div>
									</div>                               
					        	</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">					
					<button type="button" class="btn btn-warning btn-outline close-modal"><span class="fa fa-reply"></span></button>
					<button type="button" id="btnSiblingSaveandNew" class="btn btn-primary btn-outline" value="SaveandNew"><span class="glyphicon glyphicon-floppy-disk"></span> Save and New</button>
					<button type="button" id="btnSiblingSaveandClose" class="btn btn-primary btn-outline" value="SaveandClose"><span class="glyphicon glyphicon-floppy-disk"></span> Save and Close</button>
				</div>
		</div>
	</div>
</div>