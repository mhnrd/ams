<div class="modal inmodal animated fadeInUp" id="detail-template-resign" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4><i class="fa fa-user-times"></i> Employee Resign</h4>
				</div>
				<div class="modal-body">
					<form id='form-resign'>
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" name="Emp_Id">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4"><span class="red">*</span> Date Hired:</div>
										<div class="col-7">
											<div class="input-group date">
												<input required type="text" class="form-control p-xxs" name="Emp_DateHired" value="" tabindex="1" data-mask="99/99/9999">
	                    						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4"><span class="red">*</span> Date Resign:</div>
										<div class="col-7">
											<div class="input-group date">
												<input required type="text" class="form-control p-xxs" name="Emp_PayrollStart" value="" tabindex="2" data-mask="99/99/9999">
	                    						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</div>                               
					        	</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning btn-outline close-modal" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
					<button type="button" class="btn btn-success"id="btnApplyResign">Apply Date</button>                    
				</div>
			</div>
		</div>
	</div>