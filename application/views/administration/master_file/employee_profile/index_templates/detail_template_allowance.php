
<div class="modal inmodal animated fadeInUp" id="detail-template-allowance" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Allowance</h4>
				</div>
				<div class="modal-body">
					<form id='form-allowance'>
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" name="mode">
								<input type="hidden" name="FK_Emp_Id">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Item:</label>
										</div>
										<div class="col-7">
											<select required style="width:100%" name="Allow_Id" class="form-control p-xxs" tabindex="1">
												<option value=""></option>
											</select>
										</div>
									</div>                               
					        	</div> 
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span>
										 	<label>Description:</label>
										 </div>
										<div class="col-7">
											<select required style="width:100%" name="Allow_Desc" class="form-control p-xxs" tabindex="1">
												<option value=""></option>
											</select>
										</div>
									</div>                               
					        	</div> 
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 no-margin-top text-right">
											Taxable
										</div>
										<div class="col-7">
											<input name="Taxable" type="checkbox" disabled>
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Deduction Type:</label>
										</div>
										<div class="col-7">
											<input readonly type="text" class="form-control p-xxs" value="" name="DeductionType">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Cut-off:</label>
										</div>
										<div class="col-7">
											<input readonly type="text" class="form-control p-xxs" value="" name="Cutoff">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Amount:</label>
										</div>
										<div class="col-7">
											<input type="number" class="form-control p-xxs" name="Allow_Amount" value="<?= number_format(0,2) ?>">
										</div>
									</div>                               
					        	</div>
								
								<input type="hidden" class="form-control p-xxs" name="Emp_CompanyId" readonly>
								<input type="hidden" class="form-control p-xxs" name="Emp_Id" readonly>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-warning btn-outline close-modal" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
					<button type="button" id="btnSaveandNew" class="btn btn-primary btn-outline" value="SaveandNew"><span class="glyphicon glyphicon-floppy-disk"></span> Save and New</button>
					<button type="button" id="btnSaveandClose" class="btn btn-primary btn-outline" value="SaveandClose"><span class="glyphicon glyphicon-floppy-disk"></span> Save and Close</button>
				</div>
			</div>
		</div>
	</div>