<style>
	#detail-template-relatives hr{
		margin: 5px 0 5px 0;
	}
	
</style>

<div class="modal inmodal animated fadeInUp" id="detail-template-relatives" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" style="width:90%">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4><span class="fa fa-users"></span> Family Information</h4>
				</div>
				<div class="modal-body">
					<form id='form-relatives'>
						<input type="hidden" name="Emp_Id" value=""/>
						<div class="row">
							<div class="col-md-12">
								<label><h4><b>Siblings</b></h4></label>
								<?= generate_table_standard($table_relatives_siblings)?>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12">
								<label><h4><b>Beneficiaries</b></h4></label>
								<?= generate_table_standard($table_relatives_beneficiaries)?>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Father's Name:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_FatherName" value="">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Occupation:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_FatherOccupation" value="">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Company:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_FatherCompany" value="">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Contact No:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_FatherContact" value="">
										</div>
									</div>                               
					        	</div>					        	
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Mother's Name:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_MotherName" value="">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Occupation:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_MotherOccupation" value="">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Company:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_MotherCompany" value="">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Contact No:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_MotherContact" value="">
										</div>
									</div>                               
					        	</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Spouse Name:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_SpouseName" value="">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Occupation:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_SpouseOccupation" value="">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Company:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_SpouseCompany" value="">
										</div>
									</div>                               
					        	</div>					        	
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Contact No / Age:</label>
										</div>
										<div class="col-4">
											<input type="text" class="form-control p-xxs" name="Emp_SpouseContact" value="">
										</div>
										<div class="col-3">
											<input type="text" class="form-control p-xxs" name="Emp_SpouseAge" value="">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right">
											<label>Spouse Employeement Status:</label>
										</div>
										<div class="col-7">			
											<select required style="width:100%" name="ED_SpouseEmploymentStat" class="form-control p-xxs" tabindex="1">
												<option value=""></option>
												<option value="R">Regular</option>
												<option value="P">Probationary</option>
												<option value="C">Contractual</option>
												<option value="S">Seasonal</option>
											</select>
										</div>
									</div>                               
						    	</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">					
					<button type="button" class="btn btn-warning btn-outline close-modal" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
					<button type="button" id="btnRelativeUpdate" class="btn btn-primary btn-outline" value="Update"><i class="glyphicon glyphicon-floppy-disk"></i> Update</button>
				</div>
			</div>
		</div>
	</div>