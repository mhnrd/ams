<div class="modal inmodal animated fadeInUp" id="detail-template-id" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>ID History</h4>
				</div>
				<div class="modal-body">
					<form id='form-id-history'>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span>
										  <label>Company:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" id="CompanyName" readonly>
											<input type="hidden" class="form-control p-xxs" name="Emp_CompanyId" readonly>
											<input type="hidden" class="form-control p-xxs" name="Emp_Id" readonly>
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span>
											<label>Employee ID Card#:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_ID_Card" value="">
										</div>
									</div>                               
					        	</div>  
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Biometrix No:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="EID_BiometrixNo" value="">
										</div>
									</div>                               
					        	</div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span>
											<label>Barcode ID:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="Emp_BarcodeID" value="">
										</div>
									</div>                               
					        	</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning btn-outline close-modal" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
					<button class="btn btn-success btn-outline" id="btnGenerateBarcodeID">
                    	<i class="fa fa-barcode"></i><strong> Generate Barcode</strong>
                    </button>
                    <button class="btn btn-primary btn-outline" id="btnSaveID">
                    	<i class="glyphicon glyphicon-floppy-disk"></i><strong> Save</strong>
                    </button>  
				</div>
			</div>
		</div>
	</div>