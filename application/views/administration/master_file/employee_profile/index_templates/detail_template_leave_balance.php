
<div class="modal inmodal animated fadeInUp" id="detail-template-leave-balance" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Leave Balance</h4>
				</div>
				<div class="modal-body">
					<form id='form-leave-balance'>
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" name="mode">
								<input type="hidden" name="PKFK_Empid">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Leave Balance:</label>
										</div>
										<div class="col-7">
											<select required style="width:100%" name="PKFK_LeaveType" class="form-control p-xxs" tabindex="1">
												<option value=""></option>
												<?php /* foreach($leavetype_list as $row): ?>
													<option value="<?= $row['Id'] ?>"><?= ucwords($row['Description']) ?></option>
												<?php endforeach; */ ?>
											</select>
										</div>
									</div>                               
					        	</div> 
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Leave Balance:</label>
										</div>
										<div class="col-7">
											<input type="number" class="form-control p-xxs" value="" name="LT_Balance">
										</div>
									</div>                               
					        	</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-warning btn-outline close-modal" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
					<button type="button" id="btnLeaveBalSaveandNew" class="btn btn-primary btn-outline" value="SaveandNew"><span class="glyphicon glyphicon-floppy-disk"></span> Save and New</button>
					<button type="button" id="btnLeaveBalSaveandClose" class="btn btn-primary btn-outline" value="SaveandClose"><span class="glyphicon glyphicon-floppy-disk"></span> Save and Close</button>
				</div>
			</div>
		</div>
	</div>