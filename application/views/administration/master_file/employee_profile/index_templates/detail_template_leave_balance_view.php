
<div class="modal inmodal animated fadeInUp" id="detail-template-leave-balance-view" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-lg">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4><span class="glyphicon glyphicon-search"></span> Leave Balance View</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<?php echo generate_table($table_leave_view) ?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-warning btn-outline close-modal" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
				</div>
			</div>
		</div>
	</div>