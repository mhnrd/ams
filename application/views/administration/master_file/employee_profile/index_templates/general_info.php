	<div class="row">
		<div class="col-md-4">
			<form method="POST" enctype="multipart/form-data" action="<?= DOMAIN.'administration/master_file/employee_profile/saveImageURL'; ?>">
	    		<div>
		            <img src="<?php echo EMP_PHOTO_DIR ?>Blank.jpg" width="96px" height="96px" id="image-preview" class="img-responsive">
		            <p class="help-block"></p>
		            <!-- <input type="file" id="input-image" name="image" class="btn btn-primary"> -->
		            <!-- <label title="Upload image file" for="inputImage" class="btn btn-primary invisible" style="color:white!important">
	                    <input type="file" accept="image/*" name="file" id="inputImage" style="display:none">
	                    <span class="glyphicon glyphicon-download"></span> Upload image
	                </label> -->
	                <input type="hidden" name="Emp_Id_Photo">
	                <div class="btn-group">
	                	<input required type="file" accept="image/*" name="flPhoto" class="btn btn-xs invisible" style="width:100%;border: 0.5px solid #E7EAEC">
						<input type="hidden" name="FileURL">
						<input type="submit" name="btnUpdateImage" value="Update Image" class="btn btn-primary btn-xs invisible" style="color:white!important">
	                </div>
		        </div>
			</form>
    	</div>		                                	
    	<div class="col-md-4">
    		<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Employee ID:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="EmployeeID" value="">
					</div>
				</div>                               
        	</div>  
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Employee Status:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="EmployeeStatus" value="">
					</div>
				</div>                               
	    	</div>
	    	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Department:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="Department" value="">
					</div>
				</div>                               
	    	</div>
	    	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Position:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="Position" value="">
					</div>
				</div>                               
	    	</div>
    	</div>
    	<div class="col-md-4">
	    	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>UPC Expiry Date:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="UPCExpiryDate" value="">
					</div>
				</div>                               
	    	</div>
	    	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Date Hired:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="DateHired" value="">
					</div>
				</div>                               
	    	</div>
	    	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Separation Date:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="SeparationDate" value="">
					</div>
				</div>                               
	    	</div>
	    	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Payroll Start Date:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PayrollStartDate" value="">
					</div>
				</div>                               
	    	</div>                       
    	</div>                 	
	</div>
	<hr>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group mt-2">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Title:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="Title" value="">
					</div>
				</div>                               
        	</div>
		</div>
		<div class="col-md-4">
			<div class="form-group mt-2">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Nick Name:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="NickName" value="">
					</div>
				</div>                               
        	</div>
		</div>	
		<div class="col-md-4">&nbsp;</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>First Name:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="FirstName" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Gender:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="Gender" value="">
					</div>
				</div>                               
        	</div>        	
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Phone No:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PhoneNumber" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Weight (Kg.):</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="Weight" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Height (ft.):</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="Height" value="">
					</div>
				</div>                               
        	</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Middle Name</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="MiddleName" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Birth Date:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="BirthDate" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Nationality:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="Nationality" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Mobile No:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="MobileNumber" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Email Address:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="EmailAddress" value="">
					</div>
				</div>                               
        	</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Last Name:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="LastName" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Civil Status:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="CivilStatus" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Place of Birth:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PlaceOfBirth" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Religion:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="Religion" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Mother's Maiden Name:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="MotherMaidedName" value="">
					</div>
				</div>                               
        	</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group mt-2">
				<div class="row">
					<div class="col-md-4 text-right" style="padding-top: 6px">
						<label>Current Address:</label>
					</div>
					<div class="col-7">
						<textarea readonly name="CurrentAddress" style="resize: none" class="form-control p-xxs"></textarea>
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Tel No:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="CurrentTelNo" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Mobile No:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="CurrentMobileNo" value="">
					</div>
				</div>                               
        	</div>
		</div>
		<div class="col-md-6">
			<div class="form-group mt-2">
				<div class="row">
					<div class="col-md-4 text-right" style="padding-top: 6px">
						<label>Provincial Address:</label>
					</div>
					<div class="col-7">
						<textarea readonly name="ProvincialAddress" style="resize: none" class="form-control p-xxs"></textarea>
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Tel No:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="ProvincialTelNo" value="">
					</div>
				</div>                               
        	</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Contact Person in case of emergency:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="ContactPerson" value="">
					</div>
				</div> 
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Telephone No:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="ContactPersonTelNo" value="">
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right" >
						<label>Relationship:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="ContactPersonRelationship" value="">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right" style="padding-top: 4px">
						<label>Address:</label>
					</div>
					<div class="col-7">
						<textarea readonly name="ContactPersonAddress" style="resize: none" class="form-control p-xxs"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group" style="display: none">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Professional License?</label>
					</div>
					<div class="col-7">
						<input name="ProfessionalLicense" type="checkbox" disabled>
					</div>
				</div>                               
        	</div>
        	<div class="form-group" style="display: none">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Professional Description:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="ProfessionalDescription" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Principal:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="Principal" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Basic Rate:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="BasicRate" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Basic Rate (Enc):</label>
					</div>
					<div class="col-5">
						<input readonly type="text" class="form-control p-xxs" name="BasicRateEnc" value="">
					</div>
					<div class="col-md-2"><button class="btn btn-success btn-sm" id="btnBasicRateEnc"><i class="fa fa-gears"></i></button></div>
				</div>                               
        	</div>  
		</div>
		<div class="col-md-6">
			<div class="form-group" style="display: none">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Prof. License No:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="ProfessionalLicenseNo" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Work Location:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="WorkLocation" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Pay Group:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PayGroupName" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PayGroupID" value="">
					</div>
				</div>                               
        	</div>      	
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
        	<label><h4><b>ID History</b></h4></label>
        	<br><br>
        	<?= generate_table_standard($table_id)?>        	
		</div>
	</div>