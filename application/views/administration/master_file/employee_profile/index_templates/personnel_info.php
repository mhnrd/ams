<!-- <form> -->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Exemption Code:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="ExemptionCode" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>SSS No.:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="SSSNo" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>TIN No:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="TinNo" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Pagibig No.:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PagIbigNo" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Philhealth No.:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PhilhealthNo" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Grace Period (Mins):</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="GracePeriod" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Work Hour/s:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="WorkHours" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Separation Savings Fund:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="SeparationSavingsFund" value="">
					</div>
				</div>                               
        	</div>
        	<!-- CheckBoxes -->
        	<hr>
        	<div class="row">
        		<div class="col-md-6">
	        		<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="color: black">
								Compute SSS?
							</div>
							<div class="col-2">
								<input name="ComputeSSS" type="checkbox" disabled>
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="color: black">Taxable?</div>
							<div class="col-2">
								<input name="Taxable" type="checkbox" disabled>
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="color: black">Compute Pagibig?</div>
							<div class="col-2">
								<input name="ComputePagibig" type="checkbox" disabled>
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="color: black">Compute Philhealth?</div>
							<div class="col-2">
								<input name="ComputePhilhealth" type="checkbox" disabled>
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group" style="display: none">
						<div class="row">
							<div class="col-md-8 text-right" style="color: black">Agency?</div>
							<div class="col-2">
								<input name="Agency" type="checkbox" disabled>
							</div>
						</div>                               
		        	</div>
	        	</div>
	        	<div class="col-md-6">
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="color: black">Active?</div>
							<div class="col-2">
								<input name="Active" type="checkbox" disabled>
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="color: black">Last Payroll?</div>
							<div class="col-2">
								<input name="LastPay" type="checkbox" disabled>
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="color: black">Hold Payroll?</div>
							<div class="col-2">
								<input name="HoldPayroll" type="checkbox" disabled>
							</div>
						</div>                               
		        	</div>
	        	</div>	        	        
        	</div>
        	<hr>
        		<h4 style="margin-bottom: 10px;color:black"><b>Time Keeping Setup</b></h4>
        		<div class="row">        			
        			<div class="col-md-6">
        				<div class="form-group">
							<div class="row">
								<div class="col-md-8 text-right" style="color: black">Compressed Schedule?</div>
								<div class="col-2">
									<input name="CompressedSchedule" type="checkbox" disabled>
								</div>
							</div>                               
			        	</div>
        				<div class="form-group">
							<div class="row">
								<div class="col-md-8 text-right" style="color: black">Process Time Attendance?</div>
								<div class="col-2">
									<input name="ProcessTimeAttendance" type="checkbox" disabled>
								</div>
							</div>                               
			        	</div>
        			</div>
        			<div class="col-md-6">
        				<div class="form-group">
							<div class="row">
								<div class="col-md-8 text-right" style="color: black">Check Break?</div>
								<div class="col-2">
									<input name="CheckBreak" type="checkbox" disabled>
								</div>
							</div>                               
			        	</div>
        				<div class="form-group">
							<div class="row">
								<div class="col-md-8 text-right" style="color: black">Undertime Exempt?</div>
								<div class="col-2">
									<input name="UndertimeExempt" type="checkbox" disabled>
								</div>
							</div>                               
			        	</div>
			        	<div class="form-group">
							<div class="row">
								<div class="col-md-8 text-right" style="color: black">Tardy Exempt?</div>
								<div class="col-2">
									<input name="TardyExempt" type="checkbox" disabled>
								</div>
							</div>                               
			        	</div>
        			</div>
        		</div>
        	<hr>
        	<div class="row">
        		<div class="col-md-12">
        			<button class="btn btn-success btn-outline invisible" id="btnRelative">
							<i class="fa fa-users"></i> Relatives
						</button> &nbsp;
					<button class="btn btn-success btn-outline invisible" id="btnEducation">
						<i class="fa fa-graduation-cap"></i> Education
					</button>
        		</div>
        	</div>        	
		</div>
		<!-- 2nd Page -->
		<div class="col-md-6">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Payroll Mode:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PayrollMode" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Bank Code:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="BankCode" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group" style="display: none">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="BankName" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Bank Account:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="BankAccount" value="">
					</div>
				</div>                               
        	</div>
        	<hr>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>1st Evaluation Schedule:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="FirstEvaluationSchedule" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>2nd Evaluation Schedule:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="SecondEvaluationSchedule" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>3rd Evaluation Schedule:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="ThirdEvaluationSchedule" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Clearance Date:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="ClearanceDate" value="">
					</div>
				</div>                               
        	</div>
        	<hr>
        	<label><h4><b>Allowance</b></h4></label>
        	<br><br>
        	<?= generate_table($table_allowance)?>
		</div>
	</div>
<!-- </form> -->