<div class="modal inmodal animated fadeInUp" id="detail-template-child-education" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md" style="margin-top: 100px">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close close-modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4><span class="fa fa-graduation-cap"></span> Education</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body">
					<form id='form-child-education'>
						<input type="hidden" name="mode">
						<input type="hidden" name="EDU_Id">
						<input type="hidden" name="EDU_LineNo">						
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Level:</label>
										</div>
										<div class="col-7">			
											<select required style="width:100%" name="EDU_Levels" class="form-control p-xxs" tabindex="1">
												<option value=""></option>
												<option value="Elementary">Elementary</option>
												<option value="High School">High School</option>
												<option value="Senior High">Senior High</option>
												<option value="College">College</option>
												<option value="Vocational">Vocational</option>
												<option value="Masteral">Masteral</option>
												<option value="Doctorate">Doctorate</option>	
											</select>
										</div>
									</div>                               
						    	</div>
						    	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Year Graduated:</label>
										</div>
										<div class="col-7">			
											<select required style="width:100%" name="EDU_SchoolYear" class="form-control p-xxs" tabindex="2">
												<option value=""></option>
												<?php //for($i = 1950; $i <= date("Y"); $i++): ?>
												<?php for($i = date("Y"); $i >= 1900; $i--): ?>
													<option value="<?= $i ?>"><?= $i ?></option>
												<?php endfor; ?>
											</select>
										</div>
									</div>                               
						    	</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span>
											<label>School:</label>
										 </div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="EDU_SchoolAttended" value="" tabindex="3">
										</div>
									</div>                               
						    	</div>
						    	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Course:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="EDU_CourseTaken" value="" tabindex="4">
										</div>
									</div>                               
						    	</div>
						    	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="red">*</span> 
											<label>Honor Received:</label>
										</div>
										<div class="col-7">
											<input type="text" class="form-control p-xxs" name="EDU_HonorReceived" value="" tabindex="5">
										</div>
									</div>                               
						    	</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">	
					<button class="btn btn-warning btn-outline close-modal" data-toggle="tooltip" data-placement="bottom" title="Back"><span class="fa fa-reply"></span></button>	
					<button type="button" id="btnChildEducationSaveandNew" class="btn btn-primary" value="SaveandNew">Save and New</button>
					<button type="button" id="btnChildEducationSaveandClose" class="btn btn-primary" value="SaveandClose">Save and Close</button>
				</div>
		</div>
	</div>
</div>