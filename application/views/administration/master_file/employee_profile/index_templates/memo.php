<div class="row" style="height: 250px!important">
	<div class="col-md-12">
		<strong>Memo History</strong>
		<div class="row form-group">
			<div class="col-md-9 text-right">
                <label>Filtered by:</label>
            </div>
			<div class="col-3">
                <select required style="width:100%" name="Memo_filteredBy" class="form-control p-xxs" tabindex="3">
                    <option value=""></option>
                    <option value="Personnel Action Notice">Personnel Action Notice</option>
                    <option value="Payroll Inclusion Memo">Payroll Inclusion Memo</option>
                </select>
            </div>
		</div>
		<?= generate_table($table_memo)?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		<strong>Memo File Documents</strong>
		<form method="POST" id="memo-employee-document" enctype="multipart/form-data" action="<?= DOMAIN.'administration/master_file/employee_profile/saveEmployeeDocument'; ?>">
			<input type="hidden" name="memo_employee_id">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Document Name:</label>
					</div>
					<div class="col-7">
						<input type="text" class="form-control p-xxs" name="memo_document_name" value="">
					</div>
				</div>                               
		    </div>
	    	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Remarks:</label>
					</div>
					<div class="col-7">
						<textarea name="memo_document_remarks" class="form-control p-xxs"></textarea>
					</div>
				</div>                               
	    	</div>
	    	<div class="form-group">
    			<div class="row">
    				<div class="col-md-4"></div>
    				<div class="col-md-7">
    					<div class="btn-group group-upload-document">
		                	<input required type="file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,text/plain, application/pdf, image/*" name="flDocument" class="btn btn-xs invisible" style="width:100%;border: 0.5px solid #E7EAEC">
							<input type="hidden" name="FileDocumentURL">
							<input type="submit" name="btnUploadDocument" value="Upload Document" class="btn btn-primary btn-xs invisible" style="color:white!important">
		                </div>
    				</div>
    			</div>
	    	</div>
		</form>
		<?= generate_table($table_memo_document)?>
	</div>
</div>