<!-- <form> -->
	<div class="row">
		<div class="col-md-6">
			<label><h5><b>SSS</b></h5></label>
			<br><br>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>SSS EE Amount:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="SSSEEAmount" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>SSS ER Amount:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="SSSERAmount" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Fixed EC Amount:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="FixedECAmount" value="">
					</div>
				</div>                               
        	</div>
		</div>                          	
		<div class="col-md-6">
			<label><h5><b>Pagibig</b></h5></label>
			<br><br>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Pag-ibig EE Amount:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PagIbigEEAmount" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">	
						<label>Pag-ibig ER Amount:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PagIbigERAmount" value="">
					</div>
				</div>                               
        	</div>
		</div>                          	
	</div>
	<hr>
	<div class="row">
		<div class="col-md-6">
			<label><h5><b>Philhealth</b></h5></label>
			<br><br>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Philhealth EE Amount:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PhilhealthEEAmount" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Philhealth ER Amount:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="PhilhealthERAmount" value="">
					</div>
				</div>                               
        	</div>
		</div>                          	
		<div class="col-md-6">
			<label><h5><b>Tax</b></h5></label>
			<br><br>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Tax Contribution Basis Amount:</label>
					</div>
					<div class="col-7">
						<input readonly type="text" class="form-control p-xxs" name="TaxContributionBasisAmount" value="">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>13th Month Basis?</label>
					</div>
					<div class="col-7">
						<input name="13thMonthBasis" type="checkbox" disabled>
					</div>
				</div>                               
        	</div>
		</div>                          	
	</div>
<!-- </form> -->