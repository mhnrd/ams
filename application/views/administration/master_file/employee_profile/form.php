<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<style>
	.select2-results__options{
	        font-size:11px !important;
	 }*/

	body .tooltip{
	  z-index: 1151 !important;
	}

	.red{
		color:red!important;
	}

	.row .col-md-4{
	    color: black;
	}
</style>

<section class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
		<h5>Employee Profile</h5>
			<div class="pull-right ibox-tools">
				<button type="button" href="<?= DOMAIN."hris/".$this->uri->segment(2); ?>" class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="79">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
				
				<button  type="button" class="btn btn-primary btn-outline save" data-toggle="tooltip" data-placement="bottom" title="<?= ($type=='add') ? "Save" : "Update" ?>" data-todo="<?= $type ?>" data-doc-no="<?= $header['Emp_Id'] ?>" data-action="save-close" tabindex="80">
							<span class="glyphicon glyphicon-floppy-disk"></span>
				</button>

			<?php endif; ?>
			</div>
		</div>
		<div class="ibox-content">
				<div id="form-employee-profile" class="form-horizontal">
					<form id="form-header">
						<div class="col-lg-12">
							<div class="tabs-container">
		                        <ul class="nav nav-tabs" role="tablist">
		                            <li><a class="nav-link active" data-toggle="tab" href="#tab-1"><i class="fa fa-user-o"></i>General Info</a></li>
		                            <li><a class="nav-link" data-toggle="tab" href="#tab-2"><i class="fa fa-vcard"></i>Personnel Info</a></li>
		                            <li><a class="nav-link" data-toggle="tab" href="#tab-3"><i class="fa fa-archive"></i>Customized Contribution</a></li>
		                        </ul>
		                        <div class="tab-content">
		                            <div role="tabpanel" id="tab-1" class="tab-pane active">
		                                <div class="panel-body">
		                                	<?php $this->load->view('/administration/master_file/employee_profile/form_templates/general_info') ?>
		                                </div>
		                            </div>
		                            <div role="tabpanel" id="tab-2" class="tab-pane">
		                                <div class="panel-body">
		                                    <?php $this->load->view('/administration/master_file/employee_profile/form_templates/personnel_info') ?>
		                                </div>
		                            </div>
		                            <div role="tabpanel" id="tab-3" class="tab-pane">
		                                <div class="panel-body">
		                                	<label><h5><b>Goverment Fixed Contribution</b></h5></label>
		                                	<hr>
		                                    <?php $this->load->view('/administration/master_file/employee_profile/form_templates/goverment_fixed_contribution') ?>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
						</div>
					</form>
				</div>
			</div>

		</div>

	</div> 


	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/clockpicker/clockpicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['Emp_Id'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['Emp_Id']) ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/master_file/employee_profile' ?>';
		var module_folder 			= 'administration/master_file';
		var module_controller 		= 'employee_profile';
		var module_name 			= 'Employee Profile';
		var isRequiredDetails 		= false;
		var table_id; 				

		// additional variable
		var selected_storeid 		= '<?= $header['Emp_FK_StoreID'] ?>'
	</script>

	<script src="<?php echo JS_DIR?>app/administration/master_file/employee_profile/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/index_functions.js"></script>	

	</section>