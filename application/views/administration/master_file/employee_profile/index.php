<style>
tfoot {
	display: table-header-group;
}
.dataTables-example tbody tr td a.delete-button:hover{
	color: white!important;
}
.advance-search:hover, .advance-search-small:hover{
	color: white!important;
}

/*.group-upload-document{
    margin-left: 260px;
}*/

@media screen and (max-width: 425px){
	#advance-search-small{
		display: inline!important;
	}
	.table-tools{
		margin-right: 5px!important;
		margin-top: 5px!important;
	}
	#advance-search{
		display: none;
	}
	#employee_profile_details_filter{
		margin-left: -50px!important;
	}
	#clear-search-small{
		margin-top: 4px!important;
	}
	#employee_profile_details_filter{
		margin-right: 0px!important;
	}
}

@media screen and (max-width: 768px){
	#employee_profile_details_filter input{
		width: 60%!important;
	}
	#employee_profile_details_filter{
		margin-right: 500px!important;
	}

    /*.group-upload-document{
        margin-left: 0;
    }*/
}
.form-group{
	margin-bottom: 5px!important;
	font-size: 11px;
}
.form-control{
	font-size: 11px;
	text-transform: capitalize;
}

body .dataTables_scrollFootInner{
    display: none!important;
}

table > tbody > tr > td {
	padding-top: 5px!important;
	padding-bottom: 0px!important;
}

.no-margin-top{
    margin-top: 0px!important;
}

.row .col-md-4{
    color: black;
}

.tooltip{
      z-index: 5000 !important;
}

.red{
    color:red!important;
}

</style>

<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<!-- Clockpicker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo EXTENSION ?>moment.js"></script>

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<label><h5>Employee Profile</h5></label>
			<div class="ibox-tools">
                <a class="fullscreen-link">
                    <i class="fa fa-expand"></i>
                </a>               
            </div>
		</div>
		<div class="ibox-content">
			<div class="row">
				<div class="col-lg-4">
                    <div class="col-md-12 text-right">
                        <label>InActive Employee:</label> <input name="chkInactiveEmployee" type="checkbox" >
                        <button class="btn btn-success" id="btnHired" disabled data-toggle="tooltip" data-placement="bottom" title="Hired">
                            <i class="fa fa-user-plus"></i>
                        </button>
                        <button class="btn btn-danger" id="btnResign" disabled data-toggle="tooltip" data-placement="bottom" title="Resign">
                            <i class="fa fa-user-times"></i>
                        </button>
                    </div>
                    <div class="col-md-12">
                        <div class="row form-group">
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label>Filtered by:</label>
                                    </div>
                                    <div class="col-7">
                                        <select required style="width:100%" name="filteredBy" class="form-control p-xxs" tabindex="3">
                                            <option value=""></option>
                                            <option value="Department">Department</option>
                                            <option value="Position">Position</option>
                                            <option value="Location">Location</option>
                                            <option value="Principal">Principal</option>
                                            <option value="PayGroup">Pay Group</option>
                                        </select>
                                    </div>
                                </div>
                            </div>                               
                            <div class="col-md-5">
                                <div class="row">
                                    <div class="col-md-12">
                                        <select required style="width:100%" name="searchedBy" class="form-control p-xxs" tabindex="3">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>                               
                        </div>
                    </div>
					<?= generate_table_standard($table_hdr)?>
				</div>
				<div class="col-lg-8">
					<!-- FOR VIEWING PER EMPLOYEE -->
					<div class="tabs-container">
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a class="nav-link active" data-toggle="tab" href="#tab-1"><i class="fa fa-user"></i>General Info</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#tab-2"><i class="fa fa-vcard"></i>Personnel Info</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#tab-3"><i class="fa fa-archive"></i>Customized Contribution</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#tab-4"><i class="fa fa-file-excel-o"></i>Leave Monitoring</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#tab-5"><i class="fa fa-calendar"></i>Work Schedule</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#tab-6"><i class="fa fa-comments"></i>Notes</a></li>
                            <!-- <li><a class="nav-link" data-toggle="tab" href="#tab-7"><i class="fa fa-exchange"></i>Employee Movement</a></li> -->
                            <li><a class="nav-link" data-toggle="tab" href="#tab-8"><i class="fa fa-file-pdf-o"></i>Memo</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                	<?php $this->load->view('/administration/master_file/employee_profile/index_templates/general_info') ?>
                                </div>
                            </div>
                            <div role="tabpanel" id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/personnel_info') ?>
                                </div>
                            </div>
                            <div role="tabpanel" id="tab-3" class="tab-pane">
                                <div class="panel-body">
                                	<label><h5><b>Goverment Fixed Contribution</b></h5></label>
                                	<hr>
                                    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/goverment_fixed_contribution') ?>
                                </div>
                            </div>
                            <div role="tabpanel" id="tab-4" class="tab-pane">
                                <div class="panel-body">
                                    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/leave_monitoring') ?>
                                </div>
                            </div>
                            <div role="tabpanel" id="tab-5" class="tab-pane">
                                <div class="panel-body">
                                    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/work_schedule') ?>
                                </div>
                            </div>
                            <div role="tabpanel" id="tab-6" class="tab-pane">
                                <div class="panel-body">
                                   <?php $this->load->view('/administration/master_file/employee_profile/index_templates/notes') ?>
                                </div>
                            </div>
                            <?php /* ?>
                            <div role="tabpanel" id="tab-7" class="tab-pane">
                                <div class="panel-body">
                                    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/employee_movement') ?>
                                </div>
                            </div>
                            <?php */ ?>
                            <div role="tabpanel" id="tab-8" class="tab-pane">
                                <div class="panel-body">
                                    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/memo') ?>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>

    <!-- LOAD MODAL -->
    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/detail_template_id') ?>
    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/detail_template_relatives') ?>
        <!-- CHILD MODAL -->
        <?php $this->load->view('/administration/master_file/employee_profile/index_templates/template_relatives/detail_template_siblings') ?>
        <?php $this->load->view('/administration/master_file/employee_profile/index_templates/template_relatives/detail_template_beneficiaries') ?>

    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/detail_template_education') ?>
        <!-- CHILD MODAL -->
        <?php $this->load->view('/administration/master_file/employee_profile/index_templates/template_education/detail_template_childeducation') ?>
    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/detail_template_allowance') ?>
    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/detail_template_hired') ?>
    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/detail_template_resign') ?>
    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/detail_template_employee_notes') ?>
    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/detail_template_leave_balance') ?>
    <?php $this->load->view('/administration/master_file/employee_profile/index_templates/detail_template_leave_balance_view') ?>


<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo EXTENSION ?>moment.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<!-- Page-Level Scripts -->
<script type="text/javascript">
	var tableindex;
	var col_center		= <?= json_encode($dtl_center) ?>;
	var process_action 	= 'Employee Profile';
	var table_name 		= 'tblEmployee';
	var controller_url 	= 'administration/master_file/employee_profile/';
	var table_id 		= '<?= key($table_hdr) ?>';
    var blankimage_url  = '<?= EMP_PHOTO_DIR."Blank.jpg" ?>';
    var EMP_PHOTO_DIR   = '<?= EMP_PHOTO_DIR ?>';
    var base_url        = '<?= DOMAIN.'application/administration/master_file/employee_profile' ?>';
</script>
<script src="<?php echo JS_DIR ?>others/image-utils.js"></script>
<script src="<?php echo JS_DIR ?>app/administration/master_file/employee_profile/index.js"></script>
<script src="<?php echo JS_DIR ?>app/administration/master_file/employee_profile/index_functions.js"></script>
<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>

</section>