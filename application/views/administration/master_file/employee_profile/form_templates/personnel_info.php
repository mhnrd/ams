<!-- <form> -->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right"><span class="red">*</span> 
						<label>Exemption Code:</label>
					</div>
					<div class="col-7">
						<select required tabindex="42" style="width:100%" name="Emp_TaxExemptId" class="form-control p-xxs">
								<option value=""></option>
								<?php foreach($header['taxExcempt_list'] as $row): ?>
									<option value="<?= $row['TC_ID'] ?>" <?= $row['TC_ID'] == $header['Emp_TaxExemptId'] ? "selected" : ""  ?>>
										<?= ucfirst($row['TC_ExemtDesc']) ?>
									</option>
								<?php endforeach; ?>
						</select>
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>SSS No.:</label>
					</div>
					<div class="col-7">
						<input type="text" tabindex="43" class="form-control p-xxs" name="Emp_SSSNum" value="<?= $header['Emp_SSSNum'] ?>">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>TIN No:</label>
					</div>
					<div class="col-7">
						<input type="text" tabindex="44" class="form-control p-xxs" name="Emp_TINNum" value="<?= $header['Emp_TINNum'] ?>">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Pagibig No.:</label>
					</div>
					<div class="col-7">
						<input type="text" tabindex="45" class="form-control p-xxs" name="Emp_PagIbigNum" value="<?= $header['Emp_PagIbigNum'] ?>">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Philhealth No.:</label>
					</div>
					<div class="col-7">
						<input type="text" tabindex="46" class="form-control p-xxs" name="Emp_PhilHealthNum" value="<?= $header['Emp_PhilHealthNum'] ?>">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Grace Period (Mins):</label>
					</div>
					<div class="col-7">
						<input type="number" tabindex="47" class="form-control p-xxs" name="Emp_GracePeriod" value="<?= numeric($header['Emp_GracePeriod']) ?>">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Work Hour/s:</label>
					</div>
					<div class="col-7">
						<input type="text" class="form-control p-xxs" name="Emp_WorkHours" value="<?= $header['Emp_WorkHours'] ?>" tabindex="48">
					</div>
				</div>                               
	    	</div>        	
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Separation Savings Fund:</label>
					</div>
					<div class="col-7">
						<input type="number" tabindex="49" class="form-control p-xxs" name="Emp_SavingsFund" value="<?= numeric($header['Emp_SavingsFund']) ?>">
					</div>
				</div>                               
        	</div>
        	<!-- CheckBoxes -->
        	<hr>
        	<div class="row">
        		<div class="col-md-6">
	        		<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black"> Compute SSS?</div>
							<div class="col-2">
								<input name="Emp_ComputeSSS" tabindex="50" type="checkbox" <?= $header['Emp_ComputeSSS'] ?> >
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black">Taxable?</div>
							<div class="col-2">
								<input name="Emp_ComputeTax" tabindex="51" type="checkbox" <?= $header['Emp_ComputeTax'] ?> >
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black">Compute Pagibig?</div>
							<div class="col-2">
								<input name="Emp_ComputePagIbig" tabindex="52" type="checkbox" <?= $header['Emp_ComputePagIbig'] ?> >
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black">Compute Philhealth?</div>
							<div class="col-2">
								<input name="Emp_ComputePhilHealth" tabindex="53" type="checkbox" <?= $header['Emp_ComputePhilHealth'] ?> >
							</div>
						</div>                               
		        	</div>
	        	</div>
	        	<div class="col-md-6">
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black">Active?</div>
							<div class="col-2">
								<input name="Emp_Active" tabindex="54" type="checkbox" <?= $header['Emp_Active'] ?> >
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black">Last Payroll?</div>
							<div class="col-2">
								<input name="Emp_LastPay" tabindex="55" type="checkbox" <?= $header['Emp_LastPay'] ?> >
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black">Hold Payroll?</div>
							<div class="col-2">
								<input name="Emp_HoldPayroll" tabindex="56" type="checkbox" <?= $header['Emp_HoldPayroll'] ?> >
							</div>
						</div>                               
		        	</div>     		
	        	</div>	        	
        	</div>
        	<hr>
        	<label><h5><b>Time Keeping Setup</b></h5></label>
        	<br><br>
        	<div class="row">
        		<div class="col-md-6">
        			<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black">Compressed Schedule?</div>
							<div class="col-2">
								<input name="Emp_CompressSched" tabindex="57" type="checkbox" <?= $header['Emp_CompressSched'] ?>>
							</div>
						</div>                               
		        	</div>	
        			<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black">Process Time Attendance?</div>
							<div class="col-2">
								<input name="Emp_WithTimeAttendance" tabindex="58" type="checkbox" <?= $header['Emp_WithTimeAttendance'] ?>>
							</div>
						</div>                               
		        	</div>		        	
        		</div>
        		<div class="col-md-6">
        			<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black">Check Break?</div>
							<div class="col-2">
								<input name="Emp_CheckBreak" tabindex="59" type="checkbox" <?= $header['Emp_CheckBreak'] ?>>
							</div>
						</div>                               
		        	</div>
        			<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black">Undertime Exempt?</div>
							<div class="col-2">
								<input name="Emp_UndertimeExempt" tabindex="60" type="checkbox" <?= $header['Emp_UndertimeExempt'] ?>>
							</div>
						</div>                               
		        	</div>
		        	<div class="form-group">
						<div class="row">
							<div class="col-md-8 text-right" style="font-size: 11px; color: black">Tardy Exempt?</div>
							<div class="col-2">
								<input name="Emp_TardyExemp" tabindex="61" type="checkbox" <?= $header['Emp_TardyExemp'] ?>>
							</div>
						</div>                               
		        	</div>
        		</div>
        	</div>
		</div>
		<!-- 2nd Page -->
		<div class="col-md-6">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right"><span class="red">*</span> 
						<label>Payroll Mode:</label>
					</div>
					<div class="col-7">
						<select required style="width:100%" tabindex="62" name="Emp_PayrollMode" class="form-control p-xxs">
							<option value=""></option>
							<?php foreach($header['payrollmode_list'] as $id => $row): ?>
								<option value="<?= $id ?>" <?= $id == $header['Emp_PayrollMode'] ? "selected" : "" ?>>
									<?= $row ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Bank Code:</label>
					</div>
					<div class="col-7">
						<select style="width:100%" tabindex="63" class="form-control p-xxs" id="BankName">
							<option value=""></option>
							<?php foreach($header['bank_list'] as $row): ?>
								<option value="<?= $row['Id'] ?>" <?= $row['Id'] == $header['Emp_AttrBankCode'] ? "selected" : "" ?>>
									<?= ucfirst($row['Description']) ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>                               
        	</div>
        	<div class="form-group" style="display: none">
				<div class="row">
					<div class="col-md-4 text-right"></div>
					<div class="col-7">
						<input readonly type="text" tabindex="64" class="form-control p-xxs" name="Emp_AttrBankCode" value="<?= $header['Emp_AttrBankCode'] ?>">
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Bank Account:</label>
					</div>
					<div class="col-7">
						<input type="text" tabindex="65" class="form-control p-xxs" name="Emp_BankAccountNum" value="<?= $header['Emp_BankAccountNum'] ?>">
					</div>
				</div>                               
        	</div>
        	<hr>
        	<label><h5><b>Employee Evaluation Schedule</b></h5></label>
        	<br><br>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>1st Evaluation Schedule:</label>
					</div>
					<div class="col-7">
						<div class="input-group date">
                            <input type="text" tabindex="66" class="form-control p-xxs" name="E_FirstEval" value="<?= $header['E_FirstEval'] ?>">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>2nd Evaluation Schedule:</label>
					</div>
					<div class="col-7">
						<div class="input-group date">
                            <input type="text" tabindex="67" class="form-control p-xxs" name="E_SecondEval" value="<?= $header['E_SecondEval'] ?>">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>3rd Evaluation Schedule:</label>
					</div>
					<div class="col-7">
						<div class="input-group date">
                            <input type="text" tabindex="68" class="form-control p-xxs" name="E_ThirdEval" value="<?= $header['E_ThirdEval'] ?>">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
					</div>
				</div>                               
        	</div>
        	<div class="form-group">
				<div class="row">
					<div class="col-md-4 text-right">
						<label>Clearance Date:</label>
					</div>
					<div class="col-7">
						<div class="input-group date">
                            <input type="text" tabindex="69" class="form-control p-xxs" name="Emp_ClearanceDate" value="<?= format_fullyear($header['Emp_ClearanceDate']) ?>">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
					</div>
				</div>                               
        	</div>
		</div>
	</div>