<div class="row">	                                	
	<div class="col-md-6">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Employee ID:</label>
				</div>
				<div class="col-7">
					<input tabindex="1" required <?= $config['103012-001'] == 1 || $type != 'add' ? 'readonly' : '' ?> type="text" class="form-control p-xxs" name="Emp_Id" value="<?= $header['Emp_Id'] ?>" >
				</div>
			</div>                               
    	</div> 
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Employee Status:</label>
				</div>
				<div class="col-7">
					<input required tabindex="2" readonly type="text" class="form-control p-xxs" name="Emp_Status" value="<?= $header['Emp_Status'] ?>" >
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span>
					<label>Company:</label>
				</div>
				<div class="col-7">			
					<select required style="width:100%" name="Emp_CompanyId" class="form-control p-xxs" tabindex="3">
						<option value=""></option>
						<?php foreach($header['company_list'] as $row): ?>
							<option value="<?= $row['COM_Id'] ?>" <?= ( $row['COM_Id'] == $header['Emp_CompanyId']) ? "selected" : "" ?>>
								<?= strtoupper($row['COM_Name'])?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Work Location:</label>
				</div>
				<div class="col-7">
					<select required style="width:100%" name="Emp_FK_StoreID" class="form-control p-xxs" tabindex="4">	
						<option value=""></option>
						<?php if($type != 'add'): ?>					
							<?php foreach($header['work_location_list'] as $row): ?>
								<option value="<?= $row['Id'] ?>" <?= ( $row['Id'] == $header['Emp_FK_StoreID']) ? "selected" : "" ?>>
									<?= strtoupper($row['Description'])?>
								</option>
							<?php endforeach; ?>
						<?php endif; ?>					
					</select>
				</div>
			</div>                               
    	</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Department:</label>
				</div>
				<div class="col-7">
					<select required style="width:100%" name="Emp_DepartmentId" class="form-control p-xxs" tabindex="5">
						<option value=""></option>
							<?php foreach($header['department_list'] as $row): ?>
								<option value="<?= $row['Id'] ?>" <?= ( $row['Id'] == $header['Emp_DepartmentId']) ? "selected" : "" ?>>
									<?= strtoupper($row['Description'])?>
								</option>
							<?php endforeach; ?>
					</select>
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Position:</label>
				</div>
				<div class="col-7">
					<select required style="width:100%" name="Emp_PositionId" class="form-control p-xxs" tabindex="6">
						<option value=""></option>
						<?php if($type != 'add'): ?>					
							<?php foreach($header['position_list'] as $row): ?>
								<option value="<?= $row['Id'] ?>" <?= ( $row['Id'] == $header['Emp_PositionId']) ? "selected" : "" ?>>
									<?= strtoupper($row['Description'])?>
								</option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Date Hired:</label>
				</div>
				<div class="col-7">
					<div class="input-group date">
                        <input type="text" tabindex="7" class="form-control p-xxs" name="Emp_DateHired" value="<?= $header['Emp_DateHired'] ?>" >
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>UPC Expiry Date:</label>
				</div>
				<div class="col-7">
					<div class="input-group date">
                        <input type="text" tabindex="8" class="form-control p-xxs" name="Emp_BarcodeExpiry" value="<?= $header['Emp_BarcodeExpiry'] ?>">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
				</div>
			</div>                               
    	</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Title:</label>
				</div>
				<div class="col-7">
					<select required style="width:100%" name="Emp_Title" class="form-control p-xxs" tabindex="9">
						<option value=""></option>
						<?php foreach($header['title_list'] as $id => $row): ?>
							<option value="<?= $id ?>" <?= ($id == $header['Emp_Title']) ? "selected" : "" ?>>
								<?= $row ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>                               
    	</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Nick Name:</label>
				</div>
				<div class="col-7">
					<input  type="text" class="form-control p-xxs" name="Emp_NickName" value="<?= $header['Emp_NickName'] ?>" tabindex="10">
				</div>
			</div>                               
    	</div>
	</div>	
	<div class="col-md-4">&nbsp;</div>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>First Name:</label>
				</div>
				<div class="col-7">
					<input required  type="text" class="form-control p-xxs" name="Emp_FirstName" value="<?= $header['Emp_FirstName'] ?>" tabindex="11">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Gender:</label>
				</div>
				<div class="col-7">
					<select required style="width:100%" name="Emp_Gender" class="form-control p-xxs" tabindex="14">
						<option value=""></option>
						<?php foreach($header['gender_list'] as $id => $row): ?>
							<option value="<?= $id ?>" <?= ($id == $header['Emp_Gender']) ? "selected" : "" ?>>
								<?= $row ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>                               
    	</div>        	
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Phone No:</label>
				</div>
				<div class="col-7">
					<input type="text" class="form-control p-xxs" name="Emp_PhoneNo" value="<?= $header['Emp_PhoneNo'] ?>" tabindex="17">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Weight (Kg.):</label>
				</div>
				<div class="col-7">
					<input type="number" class="form-control p-xxs" name="Emp_Weight" value="<?= numeric($header['Emp_Weight']) ?>" tabindex="20">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Height (ft.):</label>
				</div>
				<div class="col-7">
					<input type="number" class="form-control p-xxs" name="Emp_Height" value="<?= numeric($header['Emp_Height'],1) ?>" tabindex="23">
				</div>
			</div>                               
    	</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Middle Name</label>
				</div>
				<div class="col-7">
					<input required  type="text" class="form-control p-xxs" name="Emp_MiddleName" value="<?= $header['Emp_MiddleName'] ?>" tabindex="12">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Birth Date:</label>
				</div>
				<div class="col-7">
					<div class="input-group date">
						<input required type="text" class="form-control p-xxs" name="Emp_Birthdate" value="<?= $header['Emp_Birthdate'] ?>" tabindex="15">
	                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>	
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Nationality:</label>
				</div>
				<div class="col-7">
					<input required  type="text" class="form-control p-xxs" name="Emp_Nationality" value="<?= $header['Emp_Nationality'] ?>" tabindex="18">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Mobile No:</label>
				</div>
				<div class="col-7">
					<input type="text" class="form-control p-xxs" name="Emp_MobileNo" value="<?= $header['Emp_MobileNo'] ?>" tabindex="21">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Email Address:</label>
				</div>
				<div class="col-7">
					<input type="email" class="form-control p-xxs" name="Emp_EmailAdd" value="<?= $header['Emp_EmailAdd'] ?>" tabindex="24">
				</div>
			</div>                               
    	</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Last Name:</label>
				</div>
				<div class="col-7">
					<input required  type="text" class="form-control p-xxs" name="Emp_LastName" value="<?= $header['Emp_LastName'] ?>" tabindex="13">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Civil Status:</label>
				</div>
				<div class="col-7">
					<select required style="width:100%" name="Emp_CivilStatus" class="form-control p-xxs" tabindex="16">
						<option value=""></option>
						<?php foreach($header['marital_list'] as $id => $row): ?>
							<option value="<?= $id ?>" <?= $id == $header['Emp_CivilStatus'] ? "selected" :"" ?>>
								<?= ucfirst($row) ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Place of Birth:</label>
				</div>
				<div class="col-7">
					<input type="text" class="form-control p-xxs" name="Emp_PlaceofBirth" value="<?= $header['Emp_PlaceofBirth'] ?>" tabindex="19">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Religion:</label>
				</div>
				<div class="col-7">
					<select style="width:100%" name="Emp_Religion" class="form-control p-xxs" tabindex="22">
						<option value=""></option>
						<?php foreach($header['religion_list'] as $row): ?>
							<option value="<?= $row['Id'] ?>" <?= $row['Id'] == $header['Emp_Religion'] ? "selected" :"" ?>>
								<?= ucfirst($row['Description']) ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Mother's Maiden Name:</label>
				</div>
				<div class="col-7">
					<input type="text" class="form-control p-xxs" name="Emp_MotherMaidenName" value="<?= $header['Emp_MotherMaidenName'] ?>" tabindex="25">
				</div>
			</div>                               
    	</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Current Address:</label>
				</div>
				<div class="col-7">
					<textarea required name="Emp_CurrentAddress" style="resize: none" class="form-control p-xxs" tabindex="26"><?= $header['Emp_CurrentAddress'] ?></textarea>
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Tel No:</label>
				</div>
				<div class="col-7">
					<input type="text" class="form-control p-xxs" name="Emp_CurrentAddressTelNo" value="<?= $header['Emp_CurrentAddressTelNo'] ?>" tabindex="27">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Mobile No:</label>
				</div>
				<div class="col-7">
					<input type="text" class="form-control p-xxs" name="Emp_CurrentAddressMobileNo" value="<?= $header['Emp_CurrentAddressMobileNo'] ?>" tabindex="28">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Same as Current Address?</label>
				</div>
				<div class="col-7">
					<input name="chkSameCurrentAddress" type="checkbox" tabindex="29">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Provincial Address:</label>
				</div>
				<div class="col-7">
					<textarea  name="Emp_PermanentAddress" class="form-control p-xxs" style="resize: none" tabindex="30"><?= $header['Emp_PermanentAddress'] ?></textarea>
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Contact No:</label>
				</div>
				<div class="col-7">
					<input type="text" class="form-control p-xxs" name="Emp_PermanentAddressTelNo" value="<?= $header['Emp_PermanentAddressTelNo'] ?>"  tabindex="31">
				</div>
			</div>                               
    	</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Contact Person in case of emergency:</label>
				</div>
				<div class="col-7">
					<input  type="text" class="form-control p-xxs" name="Emp_ContactPerson" value="<?= $header['Emp_ContactPerson'] ?>" tabindex="32">
				</div>
			</div> 
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Contact No:</label>
				</div>
				<div class="col-7">
					<input  type="text" class="form-control p-xxs" name="Emp_ContactNumber" value="<?= $header['Emp_ContactNumber'] ?>" tabindex="33">
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Relationship:</label>
				</div>
				<div class="col-7">
					<select style="width:100%" name="Emp_ContactRelationship" class="form-control p-xxs" tabindex="34">
						<option value=""></option>
						<?php foreach($header['relationship_list'] as $row): ?>
							<option value="<?= $row['Id'] ?>" <?= $row['Id'] == $header['Emp_ContactRelationship'] ? "selected" : ""  ?>>
								<?= ucfirst($row['Description']) ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Address:</label>
				</div>
				<div class="col-7">
					<textarea name="Emp_ContactAddress" style="resize: none;" class="form-control p-xxs" tabindex="35"></textarea>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Basic Rate:</label>
				</div>
				<div class="col-7">
					<select style="width:100%" name="Emp_BaseRate" class="form-control p-xxs" tabindex="36">						
						<option value=""></option>
						<?php foreach($header['baseRate_list'] as $id => $row): ?>
							<option value="<?= $id ?>" <?= $id == $header['Emp_BaseRate'] ? "selected" : ""  ?>>
								<?= $row ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right">
					<label>Basic Rate (Enc):</label>
				</div>
				<div class="col-7">
					<input type="number" class="form-control p-xxs" name="Emp_BasicRate" value="<?= numeric($header['Emp_BasicRate']) ?>" tabindex="37">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Status:</label>
				</div>
				<div class="col-7">
					<select required style="width:100%" name="Emp_FK_Status_Attribute_id" class="form-control p-xxs" tabindex="38">
						<option value=""></option>
						<?php foreach($header['employee_status_list'] as $row): ?>
							<option value="<?= $row['Id'] ?>" <?= $row['Id'] == $header['Emp_FK_Status_Attribute_id'] ? "selected" : ""  ?>>
								<?= ucfirst($row['Description']) ?>
							</option>
						<?php endforeach; ?>					
					</select>
				</div>
			</div>                               
    	</div>  		
	</div>

	<?php 

		// additional conditional - JML 02.27.20
		$server_name = $this->db->database;
		$bln_Allowed = false;

		if($server_name == 'ARMATURE_HRIS'){
			$bln_Allowed = true;			
		}
	?>

	<div class="col-md-6">
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><?= $bln_Allowed ? '<span class="red">*</span>' : '' ; ?>
					<label>Principal:</label>
				</div>
				<div class="col-7">
					<select <?= $bln_Allowed ? 'required' : '' ; ?> style="width:100%" name="Emp_CustomerId" class="form-control p-xxs" tabindex="39">
						<option value=""></option>
						<?php foreach($header['principal_list'] as $row): ?>
							<option value="<?= $row['C_Id'] ?>" <?= $row['C_Id'] == $header['Emp_CustomerId'] ? "selected" : ""  ?>>
								<?= ucfirst($row['C_Name']) ?>
							</option>
						<?php endforeach; ?>							
					</select>
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><?= $bln_Allowed ? '<span class="red">*</span>' : '' ; ?>
					<label>Pay Group:</label>
				</div>
				<div class="col-7">
					<select <?= $bln_Allowed ? 'required' : '' ; ?> style="width:100%" name="Emp_FK_GroupId" class="form-control p-xxs" tabindex="40">
						<option value=""></option>	
						<?php if($type != 'add'): ?>					
							<?php foreach($header['paygroup_list'] as $row): ?>
								<option value="<?= $row['Id'] ?>" <?= ( $row['Id'] == $header['Emp_FK_GroupId']) ? "selected" : "" ?>>
									<?= strtoupper($row['Description'])?>
								</option>
							<?php endforeach; ?>
						<?php endif; ?>							
					</select>
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><?= $bln_Allowed ? '<span class="red">*</span>' : '' ; ?>
					<label>Attendance Group</label>
				</div>
				<div class="col-7">
					<select <?= $bln_Allowed ? 'required' : '' ; ?> style="width:100%" name="Emp_AttendanceType" class="form-control p-xxs" tabindex="41">
						<option value=""></option>	
						<?php if($type != 'add'): ?>					
							<?php foreach($header['attendance_list'] as $row): ?>
								<option value="<?= $row['Id'] ?>" <?= ( $row['Id'] == $header['Emp_AttendanceType']) ? "selected" : "" ?>>
									<?= strtoupper($row['Description'])?>
								</option>
							<?php endforeach; ?>
						<?php endif; ?>						
					</select>
				</div>
			</div>                               
    	</div>
	</div>
</div>
