
<div class="row">
	<div class="col-md-6">
		<label><h5><b>SSS</b></h5></label>
		<br><br>
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>SSS EE Amount:</label>
				</div>
				<div class="col-7">
					<input type="number" tabindex="70" class="form-control p-xxs" name="Emp_FixedSSSEEAmt" value="<?= numeric($header['Emp_FixedSSSEEAmt']) ?>">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>SSS ER Amount:</label>
				</div>
				<div class="col-7">
					<input type="number" tabindex="71" class="form-control p-xxs" name="Emp_FixedSSSERAmt" value="<?= numeric($header['Emp_FixedSSSERAmt']) ?>">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span>
					<label> Fixed EC Amount:</label>
				</div>
				<div class="col-7">
					<input type="number" tabindex="72" class="form-control p-xxs" name="Emp_FixedECAmt" value="<?= numeric($header['Emp_FixedECAmt']) ?>">
				</div>
			</div>                               
    	</div>
	</div>                          	
	<div class="col-md-6">
		<label><h5><b>Pagibig</b></h5></label>
		<br><br>
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span>
					<label> Pag-ibig EE Amount:</label>
				</div>
				<div class="col-7">
					<input type="number" tabindex="73" class="form-control p-xxs" name="Emp_FixedPagibigEEAmt" value="<?= numeric($header['Emp_FixedPagibigEEAmt']) ?>">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span>
					<label>Pag-ibig ER Amount:</label>
				</div>
				<div class="col-7">
					<input type="number" tabindex="74" class="form-control p-xxs" name="Emp_FixedPagibigERAmt" value="<?= numeric($header['Emp_FixedPagibigERAmt']) ?>">
				</div>
			</div>                               
    	</div>
	</div>                          	
</div>
<hr>
<div class="row">
	<div class="col-md-6">
		<label><h5><b>Philhealth</b></h5></label>
		<br><br>
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span>
					<label>Philhealth EE Amount:</label>
				</div>
				<div class="col-7">
					<input type="number" tabindex="75" class="form-control p-xxs" name="Emp_FixedPHealthEEAmt" value="<?= numeric($header['Emp_FixedPHealthEEAmt']) ?>">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span>
					<label>Philhealth ER Amount:</label>
				</div>
				<div class="col-7">
					<input type="number" tabindex="76" class="form-control p-xxs" name="Emp_FixedPHealthERAmt" value="<?= numeric($header['Emp_FixedPHealthERAmt']) ?>">
				</div>
			</div>                               
    	</div>
	</div>                          	
	<div class="col-md-6">
		<label><h5><b>Tax</b></h5></label>
		<br><br>
		<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right"><span class="red">*</span> 
					<label>Tax Contribution Basis Amount:</label>
				</div>
				<div class="col-7">
					<input type="number" tabindex="77" class="form-control p-xxs" name="Emp_FixedTax" value="<?= numeric($header['Emp_FixedTax']) ?>">
				</div>
			</div>                               
    	</div>
    	<div class="form-group">
			<div class="row">
				<div class="col-md-4 text-right" style="font-size: 11px; color: black">13th Month Basis?</div>
				<div class="col-7">
					<input name="Emp_Month13thBasis" tabindex="78" type="checkbox" <?= $header['Emp_Month13thBasis'] ?>>
				</div>
			</div>                               
    	</div>
	</div>                          	
</div>