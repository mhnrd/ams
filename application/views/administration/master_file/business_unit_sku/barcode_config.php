<style type="text/css">	.clockpicker-popover { z-index: 999999 !important;} div.form-group{margin-bottom: 5px;}</style>
<div class="modal inmodal animated fadeInUp" id="barcode-config" data-backdrop="static"  role="dialog" aria-hidden="true" style="margin-top: 30px">
		<div class="modal-dialog modal-md">
		<div class="modal-content<?php /* animated flipInY */ ?>">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close close-config" style="margin-right: 15px;" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Barcode Settings</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body" style="padding-bottom: 10px">
					<form id="form-barcode">
						<input type="hidden" name="BUSD_ItemCategory" required>
						<div class="form-group">
							<div class="row">
								<label class="text-left col-3">BU Code</label>
								<div class="col-9">
									<input type="text" class="form-control text-left" readonly name="BU_Code">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<label class="text-left col-3">Price Type</label>
								<div class="col-9">
									<select class="form-control" name="BU_Desc" style="width: 100%" required>
										<option value=""></option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<label class="text-left col-3">Quantity</label>
								<div class="col-9">
									<input type="number" class="text-left form-control" name="BU_QtyToPrint" value="1" required>
								</div>
							</div>
						</div>
					</form>
				</div>
				<!-- MODAL FOOTER -->
				<div class="modal-footer bg-default">
					<button type="button" class="btn btn-default close-config" data-dismiss="modal">Close</button>
					<button type="button" id="barcode-print" class="btn btn-primary">Print</button>	
				</div>
		</div>
	</div>
</div>