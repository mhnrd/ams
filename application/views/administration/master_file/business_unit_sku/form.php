<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">

<style>
	input:read-only{
		background: #E9ECEF!important;
	}
	input{
		font-size: 12px!important;
	}
	.select2-selection__rendered {
		margin-top: -3px!important;
	}
	div.sa-button-container .cancel{
		background: #DD6B55!important
	}
	div.sa-button-container .cancel:hover{
		opacity: 0.8!important;
	}
	.md-price-btn, .reg-price-btn, .prc-off-btn, .prc-pt-btn{
		color: #3895FB!important;
	}
	.md-price-btn:hover, .reg-price-btn:hover, .prc-off-btn:hover, .prc-pt-btn:hover{
		text-decoration: underline!important;
	}
</style>


<section  class="wrapper wrapper-content">
	
		<div class="ibox-title">
			<h5>Business unit SKU</h5>
		</div>

		<div class="ibox-content">	
			<!-- IDENTIFIER -->
			<form id="form-header">
				<h4>Identifier</h4>									
				<div class="row">
					<div class="col-md-6 col-12">
							<div class="form-group row">
								<label class="col-md-4">Business Unit:</label>
								<div class="col-md-8">
									<?php if($type == 'add'): ?>
										<select name="BUS_BU_ID" class="js-select2 " required style="width: 100%">
											<option value=""></option>
											<?php foreach($bu_code as $row){ ?>
												<option value="<?= $row['BU_ID'] ?>"><?= $row['BU_Description'] ?></option>
											<?php }?>
										</select>
									<?php else: ?>
										<input type="hidden" name="BUS_BU_ID" value="<?= $header['BUS_BU_ID']?>" required readonly class="form-control">
										<input type="text" value="<?= $header['BU_Description']?>" required readonly class="form-control">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-4">Doc No.:</label>
								<div class="col-md-8">
									<input type="text" name="BUS_ID" value="<?= $header['BUS_ID'] ?>" required readonly class="form-control" style="width: 100%">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-4">SKU Code:</label>
								<div class="col-md-8">
									<input type="text" name="BUS_SKUCode" value="<?= $header['BUS_SKUCode'] ?>" class="form-control" style="width: 100%">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-4">Description:</label>
								<div class="col-md-8">
									<input type="text" name="BUS_Desc" value="<?= $header['BUS_Desc'] ?>" class="form-control" style="width: 100%">
								</div>
							</div>					
							<div class="form-group row">
								<label class="col-md-4">UPC Code:</label>
								<div class="col-md-8">
									<input type="text" name="BUS_UPC" value="<?= $header['BUS_UPC'] ?>" class="form-control" style="width: 100%">
								</div>
							</div>
					</div>
					<div class="col-md-6 col-12">
						<div class="form-group row">
							<label class="col-md-4">Short Desc:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="BUS_ShortDesc" value="<?= $header['BUS_ShortDesc'] ?>">
								</div>
						</div>
						<div class="form-group row">
							<label class="col-md-4">Full Desc:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="BUS_FullDesc" value="<?= $header['BUS_FullDesc'] ?>">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-4">Vendor Code:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="BUS_VendorCode" value="<?= $header['BUS_VendorCode'] ?>">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-4">Hierarchy:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="BUS_Hierarchy" value="<?= $header['BUS_Hierarchy'] ?>">
							</div>
						</div>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<!-- GENERAL INFORMATION -->
				<div class="row">
					<div class="col-md-12">
						<h4>General Information</h4>
						<div class="row">
							<div class="col-md-4 col-12">
								<div class="form-horizontal">
									<div class="form-group row">
										<label class="col-md-4">Department:</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="BUS_Dept" value="<?= $header['BUS_Dept'] ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4">Sub Dept:</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="BUS_SubDept" value="<?= $header['BUS_SubDept'] ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4">Class:</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="BUS_Class" value="<?= $header['BUS_Class'] ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4">Sub Class:</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="BUS_SubClass" value="<?= $header['BUS_SubClass'] ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4">Buyer:</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="BUS_Buyer" value="<?= $header['BUS_Buyer'] ?>">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-12">
								<div class="form-horizontal">
									<div class="form-group row">
										<label class="col-md-4">SKU Type:</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="BUS_SKUType" value="<?= $header['BUS_SKUType'] ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4">Selling UOM:</label>
										<div class="col-md-8">
											<select name="BUS_SellUOMCode" class="js-select2 " style="width: 100%">
												<option value=""></option>
												<?php foreach ($uom_list as $key => $uom): ?>
													<option value="<?= $uom['AD_Code'] ?>" <?= ($uom['AD_Code'] == $header['BUS_SellUOMCode'] ? 'selected' : '') ?>><?= $uom['AD_Desc'] ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4">Buying UOM:</label>
										<div class="col-md-8">
											<select name="BUS_BuyingUOMCode" class="js-select2 " style="width: 100%">
												<option value=""></option>
												<?php foreach ($uom_list as $key => $uom): ?>
													<option value="<?= $uom['AD_Code'] ?>" <?= ($uom['AD_Code'] == $header['BUS_BuyingUOMCode'] ? 'selected' : '') ?>><?= $uom['AD_Desc'] ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4">Inner Pack:</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="BUS_InnerPack" value="<?= $header['BUS_InnerPack'] ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4">Gross Cost:</label>
										<div class="col-md-8">
											<input type="number" class="form-control text-right" name="BUS_GrossCost" value="<?= number_format($header['BUS_GrossCost'], 2, ".", "") ?>">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-12">
								<div class="form-group row">
									<label class="col-md-4">GM Percentage:</label>
									<div class="col-md-8">
										<input type="number" class="form-control text-right" name="BUS_GMPerc" value="<?= number_format($header['BUS_GMPerc'], 2, ".", "") ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-4">UPC Type:</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="BUS_UPCType" value="<?= $header['BUS_UPCType'] ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-4">INV Group:</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="BUS_InvGroup" value="<?= $header['BUS_InvGroup'] ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-4">SMKT:</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="BUS_SMKT" value="<?= $header['BUS_SMKT'] ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-4">E3:</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="BUS_E3" value="<?= $header['BUS_E3'] ?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<?php if ($type != 'add'): ?>
				<!-- SKU Details -->
				<div class="hr-line-dashed"></div>
				<h4>SKU Details</h4>
				<?= generate_table($table_detail); ?>
				<div class="hr-line-dashed"></div>
				<h4>SKU Sub Details</h4>
				<?= generate_table($table_subdetail); ?>
			<?php endif ?>
			<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['BUS_ID'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['BUS_ID'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
			
			
		</div>
</div>
	<a href="#" class="btnTop btn btn-primary" role="button" data-toggle="tooltip" data-placement="top" title="Go to Top"><i class="fa fa-arrow-up"></i></a>
	
	<?php $this->load->view('administration/master_file/business_unit_sku/detail-template') ?>
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['BUS_ID'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['BUS_ID']) ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/master_file/business_unit_sku' ?>';
		var module_folder 			= 'administration/master_file';
		var module_controller 		= 'business_unit_sku';
		var module_name 			= 'Business Unit SKU';
		var isRequiredDetails 		= false;
		var SellUOM 				= '<?= $header['BUS_SellUOMCode'] ?>'
		var BuyUOM 					= '<?= $header['BUS_BuyingUOMCode'] ?>'
		<?php if($type != 'add'): ?>
			var table_id 				= '<?= key($table_detail) ?>'
			var tabledetail;
			var table_subdetail;
		<?php endif; ?>
		var action;
		var line_no;
	</script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
	<script src="<?php echo JS_DIR?>app/administration/master_file/business_unit_sku/form.js"></script>
	
	

</section>

