<div class="modal inmodal animated fadeInUp" id="detail-template" data-backdrop="static"  role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-md">
		<div class="modal-content<?php /* animated flipInY */ ?>">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" style="margin-right: 15px" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>SKU Details</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body" style="padding-bottom: 10px">
					<div class="form-group row" style="text-align: left!important">
						
						<label class="col-md-3" style="font-weight: bold;">Style:</label>
						<div class="col-md-9">
							<select class="form-control text-left" name="BUSD_ItemCategory[]" multiple style="width: 100%">
								
							</select>
						</div>
						
					</div>
					<div class="form-group row" style="text-align: left!important">
						
						<label class="col-md-3" style="font-weight: bold;">Season:</label>
						<div class="col-md-9">
							<select class="form-control text-left" name="BUSD_ItemSeason[]" disabled multiple required style="width: 100%">
								
							</select>
						</div>
						
					</div>
					<div class="form-group row" style="text-align: left!important">
						
						<label class="col-md-3" style="font-weight: bold;">Gender:</label>
						<div class="col-md-9">
							<select class="form-control text-left" name="BUSD_ItemGender[]" disabled multiple required style="width: 100%">
								
							</select>
						</div>
						
					</div>
					<div class="form-group row" style="text-align: left!important">
						
						<label class="col-md-3" style="font-weight: bold;">Department:</label>
						<div class="col-md-9">
							<select class="form-control text-left" name="BUSD_ItemDept[]" disabled multiple required style="width: 100%">
								
							</select>
						</div>
						
					</div>
					<div class="form-group row" style="text-align: left!important">
						
						<label class="col-md-3" style="font-weight: bold;">Type:</label>
						<div class="col-md-9">
							<select class="form-control text-left" name="BUSD_ItemType[]" disabled required multiple style="width: 100%">
								
							</select>
						</div>
						
					</div>
					<div class="form-group row" style="text-align: left!important">
						
						<label class="col-md-3" style="font-weight: bold;">Size:</label>
						<div class="col-md-9">
							<select class="form-control text-left" name="BUSSD_ItemSize[]" multiple style="width: 100%">
								
							</select>
						</div>
						
					</div>
					<div class="form-group row" style="text-align: left!important">
						
						<label class="col-md-3" style="font-weight: bold;">Price Point:</label>
						<div class="col-md-9">
							<div class="row">
								<div class="col-2">
									<input type="checkbox" id="chkPricePoint" style="margin-top: 5px; margin-left: -7px">
								</div>
								<div class="col-10 toggle-select" style="display: none">
									<select class="form-control text-left" name="BUSD_PricePoint[]" multiple style="width: 100%">
										<?php foreach ($price_point_list as $key => $value): ?>
											<option value="<?= $value['ID'] ?>"><?= number_format($value['ID'], 2) ?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
						</div>
						
					</div>
					<div class="form-group row" style="text-align: left!important">
						
						<label class="col-md-3" style="font-weight: bold;">MD Price:</label>
						<div class="col-md-9">
							<input type="number" class="form-control text-right" required name="BUSD_MDPrice">
						</div>
						
					</div>
				</div>
				<!-- MODAL FOOTER -->
				<div class="modal-footer bg-default">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="add-close" class="btn btn-primary" value="add">Add</button>	
				</div>
		</div>
	</div>
</div>