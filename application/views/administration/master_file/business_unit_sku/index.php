<style>
tfoot {
	display: table-header-group;
}
div.sa-button-container .cancel{
	background: #DD6B55!important
}
div.sa-button-container .cancel:hover{
	opacity: 0.8!important;
}

</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link rel="stylesheet" href="<?= EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">

<style>

</style>
<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Business Unit SKU</h5>
		</div>
		<div class="ibox-content">
			<?= generate_table($table_hdr)?>
		</div>
	</div>
	<?php $this->load->view('administration/master_file/business_unit_sku/barcode_config') ?>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<!-- Moment -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
	<!-- Page-Level Scripts -->


	<!-- GLOBAL VARIABLE -->
	<script type="text/javascript">
		var tableindex;
		var table_id 		= '<?= key($table_hdr) ?>';
		var process_action 	= 'Business Unit SKU';
		var table_name 		= 'tblBusinessUnitSKU';
		var controller_url 	= 'administration/master_file/business_unit_sku/';
		var col_center 		= <?= json_encode($hdr_center)?>
	</script>


	<script src="<?php echo JS_DIR?>app/administration/master_file/business_unit_sku/index.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>	
	<script src="<?php echo JS_DIR?>app/rules/advsearch.js"></script>
	

</section>