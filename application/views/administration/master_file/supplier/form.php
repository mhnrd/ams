<style>
	tfoot{
		display: table-header-group;
	}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
	.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: black;
    line-height: 22px;
}
	label{
		padding-top: 5px
	}
	.form-group{
		margin-bottom: 5px!important;
		font-size: 11px;
	}
	.form-control{
		font-size: 11px;
		text-transform: capitalize;
	}
	.select2-results__options{
	        font-size:11px !important;
	 }
	@media only screen and (max-width: 768px){
		.fixed-header{
			display: none;
		}
	}
</style>
<section id="add-supplier" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<h5>Supplier</h5>
			<div class="pull-right ibox-tools">
				<a href="<?= (isset($_SESSION['Is_DocumentApproval']) && $_SESSION['Is_DocumentApproval'] === true) ? $_SESSION['document-approval-url'] : DOMAIN."master_file/".$this->uri->segment(2); ?>" class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" >
					<span class="fa fa-reply"></span>					
				</a>
				<?php if($type != 'view'): ?>
					<button type="button" class="btn btn-primary btn-outline save" data-toggle="tooltip" data-placement="bottom" title="<?= ($type=='add') ? "Save and New" : "Update and New" ?>" data-todo="<?= $type ?>" data-doc-no="<?= $header['S_DocNo'] ?>" data-action="save-new" style="display:none">
						<span class="glyphicon glyphicon-floppy-open"></span>
					</button>	
					<button  type="button" class="btn btn-primary btn-outline save" data-toggle="tooltip" data-placement="bottom" title="<?= ($type=='add') ? "Save" : "Update" ?>" data-todo="<?= $type ?>" data-doc-no="<?= $header['S_DocNo'] ?>" data-action="save-close">
						<span class="glyphicon glyphicon-floppy-disk"></span>
					</button>
				<?php else: ?>

					<?php  if(isset($header['functions'])):
								foreach($header['functions'] as $row): ?>
									<?= $row ?>
					<?php 		endforeach; 
						   endif; ?>
					<!-- IN VIEW  -->
					<button id="print" type="button" class="btn btn-info btn-outline" data-toggle="tooltip" data-placement="bottom" title="Print" data-todo="<?= $type ?>" data-doc-no="<?= md5($header['S_DocNo'])?>">
						<span class="glyphicon glyphicon-print"></span>
					</button>

				<?php endif; ?>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content"> 
			<?php
				/*
				Converting submit to ajax is more better in this project
				*/
			?>
			<div id="form-supplier">
				<form id="form-header" autocomplete="off">
				<!--GENERAL INFORMATION-->
				<h4>General Information</h4>
				<div class="row">
						<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right; padding-top: 5px;">
											Supplier ID:
										</div>
										<div class="col-7">
											<input type="text" name="S_DocNo" id="S_DocNo" readonly required class="form-control required-field" value="<?= $header['S_DocNo']?>"/>
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="text-align: right; padding-top: 5px;">
										<div class="col-5">
											Supplier Name:
										</div>
										<div class="col-7">
											<input type="text" name="S_Name" id="S_Name"  required  class="form-control" value="<?= $header['S_Name']?>" placeholder="Supplier Name">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right;  padding-top: 5px;">
											Address:
										</div>
										<div class="col-7">
											<textarea name="S_Address" id="S_Address" class="form-control" value="<?= $header['S_Address']?>" style="resize: none;" placeholder="Address"><?= $header['S_Address']?></textarea>
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right; padding-top: 5px;">
											Country:
										</div>
										<div class="col-7">
											<input type="text" name="S_Country" id="S_Country"  required class="form-control" value="<?= $header['S_Country'] ?>" placeholder="Country" >
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right; padding-top: 5px">
											Postal Code
										</div>
										<div class="col-7">
											<input type="text" name="S_Addr_PostalCode" required id="S_Addr_PostalCode" class="form-control" value="<?= $header['S_Addr_PostalCode']?>" placeholder=" Postal Code">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right; padding-top: 5px">
											Supplier Type:
										</div>
										<div class="col-7">	
											<select  name="S_SupplierType" id="S_SupplierType" class="suppliertype-js-select2 form-control" style="width: 100%;">
												<?php foreach($suppliertype_list as $suppliertype): ?>
													<option></option>
													<option value="<?= $suppliertype['AD_Id'] ?>" <?= ($header['S_SupplierType'] == $suppliertype['AD_Id']) ? 'selected' : '' ?>> <?= $suppliertype['AD_Desc'] ?> </option>
												<?php endforeach; ?>
											</select>
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>

										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right;padding-top: 5px">
											Payment Terms:
										</div>
										<div class="col-7">
												<select  name="S_FK_PayTerms" required id="S_FK_PayTerms" class="payterms-js-select2 form-control" style="width: 100%;">
													<?php foreach ($s_fk_payterm_list as $s_fk_payterm): ?>
														<option></option>
														<option value="<?= $s_fk_payterm['PT_id'] ?>" <?= ($header['S_FK_PayTerms'] == $s_fk_payterm['PT_id']) ? 'selected' : '' ?>> <?= $s_fk_payterm['PT_id'] ?></option>
													<?php endforeach; ?>
												</select>
												<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right; padding-top: 5px;">
											Currency:
										</div>
										<div class="col-7">
											<?php if($type == 'add' || $type == 'update'):?>
											<select name="S_FK_Attribute_Currency_id" id="S_FK_Attribute_Currency_id" class=" currency-js-select2 form-control" style="width: 100%;">
												<?php  foreach($currency_list as $currency): ?>
													<option></option>
													<option value="<?= $currency['AD_Id'] ?>" <?=($header['S_FK_Attribute_Currency_id'] == $currency['AD_Id']) ? 'selected' : '' ?>> <?= $currency['AD_Desc']?></option>
												<?php endforeach; ?>
											</select>
											<?php else: ?>
												<input type="text" value="<?= $header['S_FK_Attribute_Currency_id']?>" readonly class="form-control" placeholder="Null">
												<input type="hidden" name="S_FK_Attribute_Currency_id" value="<?= $header['S_FK_Attribute_Currency_id']?>" readonly class="form-control">
											<?php endif;?>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right; padding-top: 5px">
											TIN No:
										</div>
										<div class="col-7">
											<input type="text" name="S_TinNum" required class="form-control" value="<?= $header['S_TinNum']?>" placeholder=" TIN Number">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
						</div>
					</div>
					<!--Contact Information-->
					<div class="hr-line-dashed"></div>
					<h4>Contact Information</h4>
					<div class="row">
							<div class="col-md-5 col-12"> 
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right;padding-top: 5px">
											Contact:
										</div>
										<div class="col-7">
											<input type="text" id="S_Contact" required name="S_Contact" class="form-control"  value="<?= $header['S_Contact']?>" placeholder="Contact">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right; padding-top: 5px;">
											Telephone #:
										</div>
										<div class="col-7">
											<input type="text" id="S_TelNum" required name="S_TelNum"  class="form-control" value="<?= $header['S_TelNum']?>" placeholder="Telephone #">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right; padding-top: 5px;">
											Fax #:
										</div>
										<div class="col-7">
											<input type="text" id="S_FaxNUm" required name="S_FaxNUm" class="form-control" value="<?= $header['S_FaxNUm'] ?>" placeholder="Fax #" >
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right;padding-top: 5px;">
											Email 1:
										</div>
										<div class="col-7">
											<input type="email" id="S_EmailAdd1" required name="S_EmailAdd1" class="form-control" value="<?= $header['S_EmailAdd1'] ?>" placeholder="Email 1">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right;padding-top: 5px;">
											Email 2:
										</div>
										<div class="col-7">
											<input type="email" id="S_EmailAdd2" required name="S_EmailAdd2" class="form-control" value="<?= $header['S_EmailAdd2'] ?>" placeholder="Email 2">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
							</div>
					</div>
					<!-- Bank And Credit Information -->
					<div class="hr-line-dashed"></div>
					<h4>Bank And Credit Information</h4>
					<div class="row">
							<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right;padding-top: 5px;">
											Bank Account:
										</div>
										<div class="col-7">
											<input type="text" id="S_BankAccountNo" required  name="S_BankAccountNo" class="form-control" value="<?= $header['S_BankAccountNo'] ?>" placeholder="Bank Account">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5"  style="text-align: right;padding-top: 5px">
											Bank Name:
										</div>
										<div class="col-7">
											<input type="text"  id="S_BankName" required name="S_BankName" class="form-control" value="<?= $header['S_BankName'] ?>" placeholder="Bank Name">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right;padding-top: 5px;">
											Bank Address:
										</div>
										<div class="col-7">
											<textarea id="S_BankAddress" required name="S_BankAddress" class="form-control" value=" <?= $header['S_BankAddress'] ?>" style="resize: none;" placeholder="Bank Address"><?= $header['S_BankAddress']?></textarea>
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
											
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right;padding-top: 5px;">
											Print Check As:
										</div>
										<div class="col-7">
											<input type="text"  id="S_PrintCheckAs" required name="S_PrintCheckAs" class="form-control" value="<?= $header['S_PrintCheckAs']?>" placeholder="Print Check As">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right;padding-top: 5px;">
											Credit Limit:
										</div>
										<div class="col-7">
											<input type="number" id="S_CreditLimit" required name="S_CreditLimit" class="form-control text-right" placeholder="0.00" value="<?= $header['S_CreditLimit']?>">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right;padding-top: 5px">
											<label class="control-label ">Running Balance:</label>
										</div>
										<div class="col-7">
											<input type="number" id="S_BalanceAsOf" required name="S_BalanceAsOf" class="form-control text-right" value="<?= $header['S_BalanceAsOf']?>" placeholder="0.00">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<!-- <div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right; font-weight:bold;padding-top: 5px;">
											<label class="control-label font-weight-bold">Credit Balance:</label>
										</div>
										<div class="col-7">
											<input type="text"  id="" name="S_Credit_Balance" class="form-control text-right" placeholder="0.00" readonly>
										</div>
									</div>
								</div> -->
								<div class="form-group">
									<div class="row">
										<div class="col-5" style="text-align: right;">
											<label class="control-label " >Swift Code:</label>
										</div>
										<div class="col-7">
											<input type="text" id="S_SwiftCode" required name="S_SwiftCode" class="form-control" value="<?= $header['S_SwiftCode'] ?>" placeholder="Swift Code">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
							</div>
							
					</div>
					<div class="hr-line-dashed"></div>
					<!--Other Information-->
					<h4>Other Information</h4>
					<div class="col-12">
						<div class="col-md-5 col-12"> 
							<div class="form-group">
								<div class="row">
									<div class="col-5" style="text-align: right;">
										<label class="control-label">Supplier Posting Group:</label>
									</div>
									<div class="col-7">
											<select name="S_SupplierPostingGroup" required class="spg-js-select2 form-control" style="width: 100%;">
												<?php foreach ($s_supplierpostinggroup_list as $s_supplierpostinggroup):?>
													<option></option>
													<option value="<?= $s_supplierpostinggroup['SPG_CODE']?>" <?= ($header['S_SupplierPostingGroup'] ==  $s_supplierpostinggroup['SPG_CODE']) ? 'selected' : '' ?>><?= $s_supplierpostinggroup['SPG_CODE'] ?> </option>
												<?php endforeach;?>
											</select>
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-5" style="text-align: right;">
										<label class="control-label">WHT Posting Group:</label>
									</div>
									<div class="col-7">
											<select name="S_WHT_PostingGroup" required class="wht-js-select2 form-control" style="width: 100%;" value=""	>
												<?php foreach ($s_wht_postinggroup_list as $s_wht_postinggroup): ?>
													<option></option>
													<option value="<?= $s_wht_postinggroup['WBPG_Code']?>" <?= ($header['S_WHT_PostingGroup'] == $s_wht_postinggroup['WBPG_Code']) ? 'selected' : '' ?>><?= $s_wht_postinggroup['WBPG_Code'] ?></option>
												<?php endforeach;?>
											</select>
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-5" style="text-align: right;">
										<label class="control-label">VAT Posting Group:</label>
									</div>
									<div class="col-7">	
											<select name="S_Vat_PostingGroup" required class="vat-js-select2 form-control" style="width: 100%;" value="">
												<?php foreach ($s_vat_postinggroup_list as $s_vat_postinggroup): ?>
													<option></option>
													<option value="<?= $s_vat_postinggroup['VBPG_Code']?>" <?= ($header['S_Vat_PostingGroup'] == $s_vat_postinggroup['VBPG_Code']) ? 'selected' : '' ?>> <?= $s_vat_postinggroup['VBPG_Code'] ?></option>
												<?php endforeach; ?>
											</select>
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<a href="#" class="btnTop btn btn-primary" role="button" data-toggle="tooltip" data-placement="top" title="Go to Top"><i class="fa fa-arrow-up"></i></a>
	
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['S_DocNo'] ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/master_file/supplier' ?>';
		var module_folder 			= 'master_file';
		var module_controller 		= 'supplier';
		var module_name 			= 'Supplier';
		var isRequiredDetails		= false;
		<?php if($type == 'view'): ?>
			
			var target_url = '<?= DOMAIN.'master_file/supplier/' ?>';
		<?php endif; ?>

		
	</script>

	<script src="<?php echo JS_DIR?>app/master_file/supplier/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
</section>
