<style>
tfoot {
	display: table-header-group;
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Supplier List</h5>
			<div class="pull-right ibox-tools">
				<button class="btn btn-success" id="btnActive" disabled>
					<i class="fa fa-check"></i> Activate
				</button>
				<button class="btn btn-danger" id="btnDeactive" disabled>
					<i class="fa fa-times"></i> Deactivate
				</button>	
			</div>
		</div>
		<div class="ibox-content">
			<!--<div class="table-responsive datatable">
				<table class="table table-striped table-bordered table-hover dataTables-example compact" id="supplier">
					<thead>
						<tr>
							<th class="text-center col-xs-1">
								<?php if($access_header === true): ?>
									<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="<?= DOMAIN;?>master_file/supplier/add"><i class="fa fa-plus"></i></a>
								<?php endif; ?>
							</th>
							<th class="text-center col-xs-1"><input type="checkbox" id="chkSelectAll" class="icheckbox_square-green"></th>
							<th class="text-center col-xs-1">Supplier ID</th>
							<th class="text-center col-xs-2">Name</th>
							<th class="text-center col-xs-2">Address</th>
							<th class="text-center col-xs-1">Tel No.</th>
							<th class="text-center col-xs-1">Tin No.</th>
							<th class="text-center col-xs-1">Active</th>
						</tr>
					</thead>
					<tfoot style="display: none"></tfoot>
				</table>
			</div>-->
			<?= generate_table($table_hdr)?>

		</div>
	</div>
	
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- Moment -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Page-Level Scripts -->
	<script type="text/javascript">
		var tableindex;
		var process_action 	= 'Supplier';
		var table_name 		= 'tblSupplier';
		var controller_url 	= 'master_file/supplier/';
		var col_center 		= <?= json_encode($hdr_center)?>;
		var table_id 		= '<?= key($table_hdr) ?>';	
	</script>
	<script src="<?= JS_DIR;?>app/master_file/supplier/index.js"></script>
	<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/advsearch.js"></script>
</section>





