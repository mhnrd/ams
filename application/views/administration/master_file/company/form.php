<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">

<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 24px;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
.form-group{
	margin-bottom: 5px!important;
	font-size: 11px;
	color: #2d3436;
}
.form-control{
	font-size: 11px;
	text-transform: capitalize;
}

</style>
<section class="wrapper wrapper-content">

	<div class="ibox-title">
		<h5 style="color:black">Company</h5>
		<div class="pull-right ibox-tools">
			
		</div>
	</div>

	<div class="ibox-content">
		<!-- GENERAL INFORMATION -->
		<div id="form-company">
			<form id="form-header" class="form-horizontal" autocomplete="off">
				<h4 style="color:black">General Information</h4>
				<div class="row">
					<div class="col-12">
						<div class="row">
							<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">	
										<div class="col-sm-3" style="text-align: right;">
											<label class="control-label"><span style="color: red">*</span>  Company ID:</label></div>
										<div class="col-9">
											<input type="text" class="form-control" tabindex="1" required name="COM_Id" value="<?= $header['COM_Id']?>">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-3" style="text-align: right;">
											<label class="control-label">
										<span style="color:red">*</span> Company Name:</label></div>
										<div class="col-9">
											<input type="text" class="form-control" tabindex="2" required name="COM_Name" value="<?= $header['COM_Name']?>">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
										
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label">Address:</label></div>
										<div class="col-9">
											 <input type="text" name="COM_Address" tabindex="3" class="form-control" value="<?= $header['COM_Address']?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label">Telephone No.:</label></div>
										<div class="col-9">
											<input type="text" class="form-control" tabindex="4" name="COM_PhoneNo" value="<?= $header['COM_PhoneNo']?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label">Fax No.:</label></div>
										<div class="col-9">
											<input type="text" class="form-control" tabindex="5"name="COM_FaxNum" value="<?= $header['COM_FaxNum']?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label">Email:</label></div>
										<div class="col-9">
											<input type="text" class="form-control" tabindex="6" name="COM_Email" value="<?= $header['COM_Email']?>">
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label">Website:</label></div>
										<div class="col-9">
											<input type="text" class="form-control" tabindex="7" name="COM_Website" value="<?= $header['COM_Website']?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label">Business Type:</label></div>
										<div class="col-9">
											<input type="text" class="form-control" tabindex="8" name="COM_BusinessType" value="<?= $header['COM_BusinessType']?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label">TIN:</label></div>
										<div class="col-9">
											<input type="text" class="form-control" tabindex="9" name="COM_Tin" value="<?= $header['COM_Tin']?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label">SSS ID:</label></div>
										<div class="col-9">
											<input type="text" class="form-control" tabindex="10" name="COM_SSSNo" value="<?= $header['COM_SSSNo']?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label">PhilHealth ID:</label></div>
										<div class="col-9">
											<input type="text" class="form-control" tabindex="11" name="COM_PhilhealthNo" value="<?= $header['COM_PhilhealthNo']?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label">Pagibig ID:</label></div>
										<div class="col-9">
											<input type="text" class="form-control" tabindex="12" name="COM_PagibigNo" value="<?= $header['COM_PagibigNo']?>">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="hr-line-dashed"></div>
						<!-- Setup -->
						<div class="row">
							<div class="col-md-5 col-12">
								<h4 style="color:black">Setup</h4>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right; padding-top: 5px;">
											<label class="control-label">Fiscal Year Start:</label></div>
										<div class="col-9">
											<div id="datepicker" class="datepicker input-group date">
											<input type="text" readonly class="form-control text-right" tabindex="13" name="COM_FY_Start" value="<?= $header['COM_FY_Start']?>" style="background: #FFFF"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right; padding-top: 5px;">
											<label class="control-label">Fiscal Year End:</label></div>
										<div class="col-9">
											<div id="datepicker" class="datepicker input-group date">
											<input type="text" readonly class="form-control text-right" tabindex="14" name="COM_FY_End" value="<?= $header['COM_FY_End']?>" style="background: #FFFF"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											 </div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right; padding-top: 5px; color:black">
										Currency:</div>
										<div class="col-9">
											<select name="COM_Currency" tabindex="15" class="js-select2-currency form-control required-field" style="width: 100%">
												<?php foreach($currency_list as $currency) {?>
													<option></option>
													<option value="<?= $currency['AD_Id']?>" <?= ($header['COM_Currency'] == $currency['AD_Id'])? 'selected' : ''?>><?= $currency['AD_Desc']?></option>
												<?php }?>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="16">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['COM_Id'] ?>" data-action="save-new" tabindex="17">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['COM_Id'] ?>" data-action="save-close" tabindex="18">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
	<!-- <a href="#" class="btnTop btn btn-primary" role="button" data-toggle="tooltip" data-placement="top" title="Go to Top"><i class="fa fa-arrow-up"></i></a> -->

	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['COM_Id'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['COM_Id']) ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/master_file/company' ?>';
		var module_folder 			= 'administration/master_file';
		var module_controller 		= 'company';
		var module_name 			= 'Company';
		var isRequiredDetails 		= false;

	</script>

	<script src="<?php echo JS_DIR?>app/administration/master_file/company/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>

</section>