<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">

<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 24px;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
.form-group{
	margin-bottom: 5px!important;
	font-size: 11px;
}
.form-control{
	font-size: 11px;
	text-transform: capitalize;
}
.col-5, h4,  h5, small{
	color: black!important;
}
</style>

<section class="wrapper wrapper-content">

	<div class="ibox-title">
		<h5>Principal</h5>
	</div>

	<div class="ibox-content">
		<!--  GENERAL INFORMATION -->
		<div id="form-principal" class="form-horizontal">
			<form id="form-header">
				<h4>General Information</h4>
					<div class="row">
						<div class="col-md-5 col-12">
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right">
										<label class="control-label">
										<span style="color:red">*</span> Principal ID:</label></div>
									<div class="col-9">
										<input type="text" class="form-control required-field" readonly required name="C_DocNo" value="<?= $header['C_DocNo'] ?>">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right">
									<label class="control-label">
									<span style="color:red">*</span> Principal Name:</label></div>
									<div class="col-9">
										<input type="text" class="form-control required-field" required name="C_Name" value="<?= $header['C_Name'] ?>">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right">
									<label class="control-label">Address:</label></div>
									<div class="col-9">
										<input type="text" class="form-control" name="C_Address1" required value="<?= $header['C_Address1'] ?>">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right">
									<label class="control-label">City:</label></div>
									<div class="col-9">
										<input type="text" class="form-control" name="C_City" required value="<?= $header['C_City'] ?>">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right">
									<label class="control-label">Postal Code:</label></div>
									<div class="col-9">
										<input type="text" class="form-control" name="C_PostalCode" required value="<?= $header['C_PostalCode']?>">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right">
									<label class="control-label">Country:</label></div>
									<div class="col-9">
										<input type="text" class="form-control" name="C_Country" required value="<?= $header['C_Country'] ?>">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-5 col-12">
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right">
									<label class="control-label">Company Name:</label></div>
									<div class="col-9">
										<input type="text" name="C_CompanyName" class="form-control" value="<?= $header['C_CompanyName']?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;color: black; padding-top:5px">
									Principal Type:</div>
									<div class="col-9">
										<?php if($type == 'add' || $type == 'update'): ?>
											<select id="cust_type" name="C_FK_CustomerType" class="js-select2 form-control" style="width: 100%">
												<?php foreach($cust_type_list as $cust_type){?>
													<option></option>
													<option value="<?= $cust_type['CT_Id']?>" <?= ($header['C_FK_CustomerType'] == $cust_type['CT_Id']) ? 'selected' : ''?>><?= $cust_type['CT_Description']?></option>
												<?php }?>
											</select>
										<?php else: ?>
											<input type="text" value="<?= $header['C_FK_CustomerType']?>" readonly class="form-control">
											<input type="hidden" name="C_FK_CustomerType" value="<?= $header['C_FK_CustomerType']?>" readonly class="form-control">
										<?php endif;?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;color:black; padding-top:5px">
									Payment Terms:</div>
									<div class="col-9">
										<?php if($type == 'add' || $type == 'update'): ?>
										<select id="payment_terms" name="C_FK_PayTerms"  class="js-select2 form-control" style="width: 100%">
											<?php foreach($pay_terms_list as $pay_terms){?>
												<option></option>
												<option value="<?= $pay_terms['PT_id']?>" <?= ($header['C_FK_PayTerms'] == $pay_terms['PT_id']) ? 'selected' : ''?>><?= $pay_terms['PT_Desc']?></option>
											<?php }?>
										</select>
										<?php else: ?>
											<input type="text" value="<?= $header['C_FK_PayTerms']?>" readonly class="form-control">
											<input  type="hidden" name="C_FK_PayTerms" value="<?= $header['C_FK_PayTerms']?>" readonly class="form-control">
										<?php endif;?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;color:black; padding-top:5px">
									<span style="color:red">*</span> Bill To:</span></div>
									<div class="col-9">
										<?php if($type == 'add' || $type == 'update'):?>
											 <select id="bill_to" name="C_BillTo" class="js-select2 form-control" style="width: 100%">
											 	<?php foreach($bill_to_list as $bill_to){?>
											 		<option></option>
												 	<option value="<?= $bill_to['C_DocNo']?>" <?= ($header['C_BillTo'] == $bill_to['C_DocNo'])? 'selected' : ''?>><?= $bill_to['C_Name']?></option> 
												<?php }?>
											</select>
											<!-- <small style="font-size: 10px">*Leave blank if bill to is the same as the principal you are currently creating.</small> -->
										<?php else: ?>
											<input type="text" value="<?= $header['C_Name']?>" readonly class="form-control">
											<input type="hidden" name="C_BillTo" value="<?= $header['C_BillTo']?>" readonly class="form-control">
										<?php endif;?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;color:black; padding-top:5px">
									Bill Period:</div>
									<div class="col-9">
										<select name="C_BillDate" class="js-select2-bill form-control" style="width: 100%">
											<option></option>	
											<option value="0" <?= ($header['C_BillDate'] == '0' ? 'selected' : '')?>>Monthly</option>
											<option value="1" <?= ($header['C_BillDate'] == '1' ? 'selected' : '')?>>Semi-Monthly</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;color:black; padding-top:5px">
									Currency:</div>
									<div class="col-9">
										<select name="C_FK_Attribute_Currency_id" class="js-select2-currency form-control" style="width: 100%">
											<?php foreach($currency_list as $currency){?>
												<option></option>
												<option value="<?= $currency['AD_Id']?>" <?= ($header['C_FK_Attribute_Currency_id'] == $currency['AD_Id'])? 'selected' : ''?>><?= $currency['AD_Desc']?></option>
											<?php }?>
										</select>	
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="hr-line-dashed"></div>
					<!-- Contact Information -->
					<h4>Contact Information</h4>
					<div class="row">
						<div class="col-md-5 col-12">
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
									<label class="control-label">Contact Name:</label></div>
									<div class="col-9">
										<input  type="text" class="form-control" name="C_ContactName" value="<?= $header['C_ContactName']?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
									<label class="control-label">Contact Title:</label></div>
									<div class="col-9">
										<input  type="text" class="form-control" name="C_ContactTitle" value="<?= $header['C_ContactTitle']?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
									<label class="control-label">Email:</label></div>
									<div class="col-9">
										<input  type="text" class="form-control" name="C_Email" value="<?=  $header['C_Email']?>">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-5 col-12">
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
									<label class="control-label">Telephone No.:</label></div>
									<div class="col-9">
										<input  type="text" class="form-control" name="C_TelNum" value="<?= $header['C_TelNum']?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
									<label class="control-label">Fax No.:</label></div>
									<div class="col-9">
										<input  type="text" class="form-control" name="C_FaxNum" value="<?= $header['C_FaxNum']?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				
					<div class="hr-line-dashed"></div>
					<!-- Bank and Credit Information -->
					<h4>Back and Credit Information</h4>
					<div class="row">
						<div class="col-md-5 col-12">
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
									<label class="control-label">Bank Account:</label></div>
									<div class="col-9">
										<input  id="bankaccount" type="text" class="form-control" name="C_AccountNo" value="<?= $header['C_AccountNo']?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
									<label class="control-label">Bank Name:</label></div>
									<div class="col-9">
										<input  type="text" class="form-control" name="C_BankName" value="<?= $header['C_BankName']?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
									<label class="control-label">Bank Address:</label></div>
									<div class="col-9">
										<input  type="text" class="form-control" name="C_BankAddress" value="<?= $header['C_BankAddress']?>">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-5 col-12">
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
									<label class="control-label">
										<span style="color:red">*</span> Credit Limit:</label></div>
									<div class="col-9">
										<input  type="number" class="form-control text-right" name="C_CreditLimit" value="<?= $header['C_CreditLimit'] ?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
									<label class="control-label">Running Balance:</label></div>
									<div class="col-9">
										<input id="runningbalance" type="number" class="form-control text-right" readonly name="C_BalanceAsOf" value="<?= $header['C_BalanceAsOf']?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
									<label class="control-label">Credit Balance:</label></div>
									<div class="col-9">
										<input  type="number" class="form-control text-right" readonly name="C_CreditBalance" value="<?= $header['C_CreditBalance']?>">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="hr-line-dashed"></div>
						<!-- Other Information -->
					<div class="row">
						<div class="col-md-5 col-12">
							<h4>Other Information</h4>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;color:black; padding-top:5px">
											Principal Posting Group:</div>
										<div class="col-9">
											<?php if($type == 'add' || $type == 'update'): ?>
												<select id="cust_posting" name="C_CustomerPostingGroup" class="form-control js-select2" style="width: 100%">
													<?php foreach($cust_post_list as $cust_post){ ?>
														<option></option>
														<option value="<?= $cust_post['CPG_Code']?>" <?= ($header['C_CustomerPostingGroup'] == $cust_post['CPG_Code']) ? 'selected' : ''?>><?= $cust_post['CPG_Code']?></option>
													<?php } ?>
												</select>
											<?php else: ?>
												<input type="text" value="<?= $header['C_CustomerPostingGroup']?>" readonly class="form-control">
												<input type="hidden" name="C_CustomerPostingGroup" value="<?= $header['C_CustomerPostingGroup']?>" readonly class="form-control">
											<?php endif;?>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right; color:black; padding-top:5px">
											WHT Posting Group:</div>
										<div class="col-9">
										<?php if($type == 'add' || $type == 'update'): ?>
											<select  id="WHTPG" name="C_WHTPostingGroup" value="<?= $header['C_WHTPostingGroup']?>" class="form-control js-select2" style="width: 100%">
												<?php foreach($wht_list as $wht){?>
													<option></option>
													<option value="<?= $wht['WBPG_Code']?>" <?= ($header['C_WHTPostingGroup'] == $wht['WBPG_Code'])  ? 'selected' : ''?>><?= $wht['WBPG_Code']?></option>
												<?php }?>
											</select>
											<?php else: ?>
												<input type="text" value="<?= $header['C_WHTPostingGroup']?>" readonly  class="form-control">
												<input type="hidden" name="C_WHTPostingGroup" value="<?= $header['C_WHTPostingGroup']?>">
											<?php endif; ?>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;color:black; padding-top:5px">
											VAT Posting Group:</div>
										<div class="col-9">
											<?php if($type == 'add' || $type == 'update'): ?>
												<select id="VATPG" name="C_VATPostingGroup" value="<?= $header['C_VATPostingGroup']?>" class="form-control js-select2" style="width: 100%; text-align: center">
													<?php foreach($vat_list as $vat){?>
														<option></option>
														<option value="<?= $vat['VBPG_Code']?>" <?= ($header['C_VATPostingGroup'] == $vat['VBPG_Code']) ? 'selected' : ''?>><?= $vat['VBPG_Code']?></option>
													<?php }?>
												</select>
												<?php else: ?>
													<input type="text" value="<?= $header['C_VATPostingGroup']?>" readonly  class="form-control">
													<input type="hidden" name="C_VATPostingGroup" value="<?= $header['C_VATPostingGroup']?>">
											<?php endif; ?>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label">TIN:</label></div>
										<div class="col-9">
											<input  type="number" class="form-control" name="C_TIN_No" value="<?= $header['C_TIN_No']?>">
										</div>
									</div>
								</div>
						</div>
					</div>
				</form>		
			</div>
			<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['C_DocNo'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['C_DocNo'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
		</div>
		<a href="#" class="btnTop btn btn-primary" role="button" data-toggle="tooltip" data-placement="top" title="Go to Top"><i class="fa fa-arrow-up"></i></a>

	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['C_DocNo'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['C_DocNo']) ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/master_file/customer' ?>';
		var module_folder 			= 'administration/master_file';
		var module_controller 		= 'customer';
		var module_name 			= 'Customer';
		var isRequiredDetails 		= false;
	

	</script>

	<script src="<?php echo JS_DIR?>app/administration/master_file/customer/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>

</section>