<style type="text/css">	.clockpicker-popover { z-index: 999999 !important;} div.form-group{margin-bottom: 5px;}</style>
<div class="modal inmodal animated fadeInUp" id="position_setup-detail" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content<?php /* animated flipInY */ ?>">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px; color: black!important;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Department Position Setup: Add</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body" style="padding-bottom: 10px">

				<div class="form-horizontal">
					<div class="row">
						<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-4">Position </label>
									<div class="col-md-8">
										<select id="position" class="js-select2 form-control" style="width: 100%">
											<option></option>
										</select>	
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Position ID </label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="" value="">	
									</div>
								</div>
							</div>
							</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group" style="margin-top: 5%">
									 <span class="col-md-4">
									 	<input type="text" class="form-control" name="" value="" disabled>
									 </span>
									 <span class="col-md-4">
									 	<input type="text" class="form-control" name="" value="" disabled>
									 </span>
									 <span class="col-md-4">
									 	<input type="text" class="form-control" name="" value="" disabled>
									 </span>
								</div>
							</div>
						</div>
					</div>
			</div>

				<!-- MODAL FOOTER -->
				<div class="modal-footer bg-default">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="add-new" class="btn btn-primary" value="add">Add & New</button>
					<button type="button" id="add-close" class="btn btn-primary" value="add">Add & Close</button>	
				</div>
		</div>
	</div>
</div>

<script>

	$('#position').select2({
			placeholder: 'Enter Currency Here',
			allowClear: true
		});

</script>