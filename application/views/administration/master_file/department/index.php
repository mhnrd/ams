<style>
tfoot {
	display: table-header-group;
}
.dataTables-example tbody tr td a.delete-button:hover{
	color: white!important;
}
.advance-search:hover, .advance-search-small:hover{
	color: white!important;
}
@media screen and (max-width: 425px){
	#advance-search-small{
		display: inline!important;
	}
	.table-tools{
		margin-right: 5px!important;
		margin-top: 5px!important;
	}
	#advance-search{
		display: none;
	}
	#department_filter{
		margin-left: -50px!important;
	}
	#clear-search-small{
		margin-top: 4px!important;
	}
	#department_filter{
		margin-right: 0px!important;
	}
}

@media screen and (max-width: 768px){
	#department_filter input{
		width: 60%!important;
	}
	#department_filter{
		margin-right: 500px!important;
	}
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Department Class</h5>
			<div class="ibox-tools pull-right">
				<button class="btn btn-success" id="btnActive" disabled>
					<i class="fa fa-check"></i> Activate
				</button>
				<button class="btn btn-danger" id="btnDeactive" disabled>
					<i class="fa fa-times"></i> Deactivate
				</button>
			</div>
		</div>
		<div class="ibox-content">
			<?= generate_table($table_hdr)?>
		</div>
	</div>

	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Department Setup</h5>
			<div class="ibox-tools pull-right">
			</div>
		</div>
		<div class="ibox-content">
			<?= generate_table($detail['table_dtl']) ?>
		</div>
	</div>
	

	<?php $this->load->view('administration/master_file/department/position_setup_template'); ?>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- Moment -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Page-Level Scripts -->
	<script type="text/javascript">
		var tableindex;
		var tableindex2;
		var col_center 		= <?= json_encode($hdr_center)?>;
		var process_action	= 'Department';
		var table_name		= 'tblDepartmentClass';
		var controller_url	= 'administration/master_file/department';
		var table_id 		= '<?= key($table_hdr) ?>';
		var table_dtl_id 	= '<?= key($detail['table_dtl'])?>';
		
	</script>
	<script src="<?php echo JS_DIR?>app/administration/master_file/department/index.js"></script>
	<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/advsearch.js"></script>
</section>