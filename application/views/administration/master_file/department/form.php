<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">

<section class="wrapper wrapper-content">

	<div class="ibox-title">
		<h5>Department</h5>
		<div class="ibox-tools pull-right">
			<a href="<?= DOMAIN. "master_file/".$this->uri->segment(2); ?>" class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back">
				<span class="fa fa-reply"></span>
			</a>
			<?php if($type != 'view'): ?>
				
				<button  type="button" class="btn btn-primary btn-outline save" data-toggle="tooltip" data-placement="bottom" title="<?= ($type=='add') ? "Save" : "Update" ?>" data-todo="<?= $type ?>" data-doc-no="<?= $header['DEP_Id'] ?>" data-action="save-close">
							<span class="glyphicon glyphicon-floppy-disk"></span>
				</button>

			<?php endif; ?>
		</div>
	</div>

	<div class="ibox-content">
		<div id="form-department" class="form-horizontal">
			<form id="form-header">
					<div class="col-12">
						<div class="col-md-5 col-12">
							<div class="form-group">
								<div class="row">
									<div class="col-5" style="padding-top: 5px; font-weight: bold">
									Department ID:</div>
									<div class="col-md-7">
										<input type="text" class="form-control" required name="DEP_Id" value="<?= $header['DEP_Id']?>" placeholder="Enter Department ID">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-5" style="padding-top: 5px; font-weight: bold">
										Description:</div>
									<div class="col-md-7">
										<input type="text" class="form-control" name="DEP_Description" value="<?= $header['DEP_Description']?>" placeholder="Enter Description">
									</div>
								</div>
							</div>
						</div>
					</div>
		
					<!-- Department Position Setup -->
				<!-- <div class="row">
					<div class="col-md-12">
						<h4>Position Setup</h4>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover dataTables-example compact" id="position-setup-list">
								<thead>
									<tr>
										<th class="text-center col-xs-1" style="width: 10%">
											<a type="button" id="add-position-setup" class="btn btn-success btn-xs btn-outline" ><i class="fa fa-plus"></i></a>
										</th>
										<th class="text-left col-xs-2">Position</th>
									</tr>
								</thead>
								<tfoot style="display: none"></tfoot>
							</table>
						</div>
					</div>
				</div> -->
			</form>
		</div>
	</div>

	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

		<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['DEP_Id'] ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var update_link 			= '<?= './update?id='.md5($header['DEP_Id']) ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/master_file/department' ?>';
		var module_folder 			= 'administration/master_file';
		var module_controller 		= 'department';
		var module_name 			= 'Department';
		var isRequiredDetails 		= false;
	</script>

	<script src="<?php echo JS_DIR?>app/administration/master_file/department/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>

</section>