<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">

<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 25px;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
.form-group{
	margin-bottom: 5px!important;
	font-size: 11px;
}
.form-control{
	font-size: 11px;
	text-transform: capitalize;
}
</style>

<section class="wrapper wrapper-content">

	<div class="ibox-title">
		<h5 style="color:black">Location</h5>
		<div class="pull-right ibox-tools">
	
		</div>
	</div>

	<div class="ibox-content">
		<div id="form-location" class="form-horizontal">
			<form id="form-header">
				<div class="row">
					<div class="col-md-5 col-12">
						<div class="form-group">
							<div class="row">
								<div class="col-3" style="text-align: right;">
									<label class="control-label"><b class="text-danger">*</b> Loc ID.:</label></div>
								<div class="col-9">
									<input type="text" tabindex="1" class="form-control required-field" required name="SP_ID" value="<?= $header['SP_ID']?>">
									<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-3" style="text-align: right;">
								<label class="control-label"><b class="text-danger">*</b>  Loc Name:</label></div>
								<div class="col-9">
									<input type="text" tabindex="2" class="form-control required-field" required name="SP_StoreName" value="<?= $header['SP_StoreName']?>">
									<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-3" style="text-align: right;">
								<label class="control-label">Address:</label></div>
								<div class="col-9">
									 <input type="text" tabindex="3" name="SP_Address" class="form-control" value="<?= $header['SP_Address']?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-3" style="text-align: right;color:black">
									<label class="control-label"><b class="text-danger">*</b> Company Name:</label></div>
								<div class="col-9">
										<select id="company_name" tabindex="4" required name="SP_FK_CompanyID" class="form-control js-select2-comname required-field" style="width: 100%">
											<?php foreach($company_name_list as $company_name){?>
												<option></option>
												<option value="<?= $company_name['COM_Id']?>" <?= ($header['SP_FK_CompanyID'] == $company_name['COM_Id'])? 'selected' : ''?>><?= $company_name['COM_Name']?></option>	

											<?php }?>	
										</select>
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5 col-12">
						<div class="form-group">
							<div class="row">
								<div class="col-3" style="text-align: right;">
								<label class="control-label">Tel No.:</label></div>
								<div class="col-9">
									<input type="text" tabindex="5" class="form-control required-field" name="SP_TelNo" value="<?= $header['SP_TelNo']?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-3" style="text-align: right;">
								<label class="control-label">Fax No.:</label></div>
								<div class="col-9">
									<input type="text" tabindex="6" class="form-control required-field"name="SP_FaxNo" value="<?= $header['SP_FaxNo']?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-3" style="text-align: right;">
								<label class="control-label">Tin No.:</label></div>
								<div class="col-9">
									<input type="text" tabindex="7" class="form-control required-field" name="SP_TinNo" value="<?= $header['SP_TinNo']?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-3" style="text-align: right;color:black">
								<label class="control-label"><b class="text-danger">*</b> Cost/Profit Center:</label></div>
								<div class="col-9">
									<?php if($type == 'add' || $type == 'update'): ?>
										<select id="CPC" tabindex="8" required name="SP_FK_CPC_id" class="form-control js-select2 required-field" style="width: 100%">
											<?php foreach($cpc_list as $cpc){?>
												<option></option>
												<option value="<?= $cpc['CPC_Id']?>" <?= ($header['SP_FK_CPC_id'] == $cpc['CPC_Id'])? 'selected' : ''?>><?= $cpc['CPC_Desc']?></option>	
											<?php }?>
										</select>
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										<?php else: ?>
											<input type="text" value="<?= $header['CPC_Desc']?>" readonly required class="form-control">
											<input type="hidden" name="SP_FK_CPC_id" value="<?= $header['CPC_Desc']?>" readonly required class="form-control">
									<?php endif;?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>

		<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="9">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['SP_ID'] ?>" data-action="save-new" tabindex="10">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['SP_ID'] ?>" data-action="save-close" tabindex="11">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
	</div>


	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['SP_ID'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['SP_ID']) ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/master_file/location' ?>';
		var module_folder 			= 'administration/master_file';
		var module_controller 		= 'Location';
		var module_name 			= 'location';
		var isRequiredDetails 		= false;

	</script>

	<script src="<?php echo JS_DIR?>app/administration/master_file/location/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>

</section>