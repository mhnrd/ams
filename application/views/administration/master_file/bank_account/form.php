
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox-title">
		<h5>Bank Account</h5>
		<div class="pull-right ibox-tools">
			<a href="<?= DOMAIN. "master_file/".$this->uri->segment(2); ?>" class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back">
				<span class="fa fa-reply"></span>
			</a>
			<?php if($type != 'view'): ?>
				
				<button  type="button" class="btn btn-primary btn-outline save" data-toggle="tooltip" data-placement="bottom" title="<?= ($type=='add') ? "Save" : "Update" ?>" data-todo="<?= $type ?>" data-doc-no="<?= $header['BA_BankID'] ?>" data-action="save-close">
							<span class="glyphicon glyphicon-floppy-disk"></span>
				</button>
			<?php endif; ?>
		</div>
	</div>

	<div class="ibox-content">
		<!-- Bank Account : Edit -->
		<form id="form-header">
			<div id="form-bank-account" class="form-horizontal">
				<h4>Bank Account: Edit</h4>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-md-6">Bank Posting Group:</label>
							<div class="col-md-8">
								<select id="BPG" required name="BPG_Code" class="js-select2 form-control required-field" style="width: 100%">
									<?php foreach($bank_posting_group_list as $bank_posting_group){?>
										<option></option>
										<option value="<?= $bank_posting_group['BPG_Code']?>"><?= $bank_posting_group['BPG_Code']?></option>
									<?php }?>
								</select>
								<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Bank ID:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" readonly name="BA_BankID" value="<?= $header['BA_BankID']?>">
							<!--<span class="col-md-4" style="padding-right: 0px;">
								<input type="text" class="form-control" name="" value="" disabled>
							</span>-->
							<!--<span class="col-md-4" style="padding-left: 5px;">
								<input type="text" class="form-control" name="" value="" disabled>
							</span>-->
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Bank Name:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" readonly name="BA_BankName" value="<?= $header['BA_BankName']?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-6">Bank Account Type:</label>
							<div class="col-md-8">
								<select id="BAT" class="js-select2 form-control" required name="BA_BankAccountType" style="width: 100%">
									<option></option>
									<option value="CA" <?= ($header['BA_BankAccountType'] == 'Current Account' ? 'selected' : '')?>>Current Account</option>
									<option value="SA" <?= ($header['BA_BankAccountType'] == 'Savings Account' ? 'selected' : '')?>>Savings Account</option>	
								</select>
								<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Account No.:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="BA_BankAccountNo" value="<?= $header['BA_BankAccountNo']?>">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-md-6">Bank Address:</label>
							<div class="col-md-8">
								<input type="text" class="form-control required-field" name="BA_BankAddress" value="<?= $header['BA_BankAddress']?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-6">Contact Name:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="BA_ContactName" value="<?= $header['BA_ContactName']?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-6">Contact No.:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="BA_ContactNo" value="<?= $header['BA_ContactNo']?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Fax No.:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="BA_FaxNo" value="<?= $header['BA_FaxNo']?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Currency:</label>
							<div class="col-md-8">
								<select id="currency" class="js-select2 form-control" name="BA_Currency" style="width: 100%">
									<option></option>
									<?php foreach($currency_list as $currency) {?>
										<option></option>
										<option value="<?= $currency['AD_Id']?>" <?= ($header['BA_Currency'] == $currency['AD_Id'])? 'selected' : ''?>><?= $currency['AD_Code']?></option>
									<?php }?>	
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-md-6">Book Balance:</label>
							<div class="col-md-8">
								<input type="text" class="form-control text-left" name="BA_BookBalance" value="<?= $header['BA_BookBalance']?>" disabled placeholder="0.00">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-6">Maint. Balance:</label>
							<div class="col-md-8">
								<input type="number" class="form-control text-left" name="" value="" placeholder="0.00">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-6">Date Opened:</label>
							<div class="col-md-8">
								<input type="text" class="form-control datepicker" readonly style="background: #FFFFFF" name="BA_DateOpened" value="<?= $header['BA_DateOpened']?>" placeholder="mm/dd/yyyy">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-6">Date Close:</label>
							<div class="col-md-8">
								<input type="text" class="form-control datepicker" readonly style="background: #FFFFFF" name="BA_DateClosed" value="<?= $header['BA_DateClosed']?>" placeholder="mm/dd/yyyy">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Status:</label>
							<div class="col-md-8">
								<input type="checkbox" class="iCheck" name="BA_Active" value="<?= $header['BA_Active']?>">
							</div>
						</div>
					</div>
				</div>
				
				<div class="hr-line-dashed"></div>
				<!-- Bank Account Series -->
				<h4>Bank Account Series</h4>
				<hr style="background:black">
				<div class="table-responsive datatable">
						<table class="table table-striped table-bordered table-hover dataTables-example compact" id="bank-account-details">
							<thead>
								<tr>
									<th class="bg-secondary text-light">Reference Type</th>
									<th class="bg-secondary text-light">No. Series Code</th>
									<th class="bg-secondary text-light">Starting No.</th>
									<th class="bg-secondary text-light">Ending No</th>
									<th class="bg-secondary text-light">Last No. Used</th>
									<th class="bg-secondary text-light">Active</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="col-xs-1" style="width: 3%">Check No. (In-Use)</td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_InUseCode" value="<?= $header['BA_InUseCode']?>" placeholder="Code"></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_InUseStartingNo" value="<?= $header['BA_InUseStartingNo']?>" placeholder="Ending No."></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_InUseEndingNo" value="<?= $header['BA_InUseEndingNo']?>" placeholder="Ending No."></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_InUseLastNoUsed" value="<?= $header['BA_InUseLastNoUsed']?>" placeholder="Last No. Used"></td>
									<td class="col-xs-1" style="width: 1%"><input type="checkbox" class="i-checks" name="BA_InUseActive" value="<?= $header['BA_InUseActive']?>" ></td>
								</tr>
								<tr>
									<td class="col-xs-1" style="width: 3%">Check No. (Reserved)</td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_ReserveCode" value="<?= $header['BA_ReserveCode']?>" placeholder="Code"></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_ReserveStartingNo" value="<?= $header['BA_ReserveStartingNo']?>" placeholder="Starting No."></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_ReserveEndingNo" value="<?= $header['BA_ReserveEndingNo']?>" placeholder="Ending No."></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_ReserveLastNoUsed" value="<?= $header['BA_ReserveLastNoUsed']?>" placeholder="Last No. Used"></td>
									<td class="col-xs-1" style="width: 1%"><input type="checkbox" class="i-checks" name="BA_ReserveActive" value="<?= $header['BA_ReserveActive']?>" ></td>
								</tr>
								<tr>
									<td class="col-xs-1" style="width: 3%">Debit Memo</td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_DMCode" value="<?= $header['BA_DMCode']?>" placeholder="Code"></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" readonly name="BA_DMStartingNo" value="<?= $header['BA_DMStartingNo']?>" placeholder="Starting No."></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" readonly name="BA_DMEndingNo" value="<?= $header['BA_DMEndingNo']?>" placeholder="Ending No."></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_DMLastNoUsed" value="<?= $header['BA_DMLastNoUsed']?>" placeholder="Last No. Used"></td>
									<td class="col-xs-1" style="width: 1%"><input type="checkbox" class="i-checks" name="BA_DMActive" value="<?= $header['BA_DMActive']?>" ></td>
								</tr>
								<tr>
									<td class="col-xs-1" style="width: 3%">Widthdrawal</td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_WCode" value="<?= $header['BA_WCode']?>" placeholder="Code"></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" readonly name="BA_WStartingNo" value="<?= $header['BA_WStartingNo']?>" placeholder="Starting No."></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" readonly name="BA_WEndingNo" value="<?= $header['BA_WEndingNo']?>" placeholder="Ending No."></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_WLastNoUsed" value="<?= $header['BA_WLastNoUsed']?>" placeholder="Last No. Used"></td>
									<td class="col-xs-1" style="width: 1%"><input type="checkbox" class="i-checks" name="BA_WActive" value="<?= $header['BA_WActive']?>" ></td>
								</tr>
								<tr>
									<td class="col-xs-1" style="width: 3%">Telegraphic Transfer</td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_TTCode" value="<?= $header['BA_TTCode']?>" placeholder="Code"></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" readonly name="BA_TTStartingNo" value="<?= $header['BA_TTStartingNo']?>" placeholder="Starting No."></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" readonly name="BA_TTEndingNo" value="<?= $header['BA_TTEndingNo']?>" placeholder="Ending No."></td>
									<td class="col-xs-1" style="width: 6%"><input type="text" class="form-control" name="BA_TTLastNoUsed" value="<?= $header['BA_TTLastNoUsed']?>" placeholder="Last No. Used"></td>
									<td class="col-xs-1" style="width: 1%"><input type="checkbox" class="i-checks" name="BA_TTActive" value="<?= $header['BA_TTActive']?>" ></td>
								</tr>
							</tbody>
								<tfoot style="display: none"></tfoot>
						</table>
					</div>
				
				<div class="hr-line-dashed"></div>
				<!-- Bank Account Ledger -->
				<div class="form-horizontal">
					<div class="row">
						<div class="col-md-6">
							<h4>Bank Account Ledger</h4>
								<div class="form-group">
									<label class="control-label col-md-4">From:</label>
									<div class="col-md-8">
										<input type="text" class="form-control datepicker" readonly style="background: #FFFFFF" name="BAL_DocDate" placeholder="mm/dd/yyyy">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">End:</label>
									<div class="col-md-8">
										<input type="text" class="form-control datepicker" readonly style="background: #FFFFFF" name="BAL_DateCreated" placeholder="mm/dd/yyyy">
									</div>
								</div>
									<div class="form-group">
										<label class="control-label col-md-4">Document Type:</label>
										<div class="col-md-8">
											<select id="document_type" name="BAL_DocType" class="js-select2 form-control" style="width: 100%">
												
												<option></option>
												
											</select>
										</div>
									</div>
								</div>
							<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Cost/Profit Center:</label>
										<div class="col-md-8">
											<select id="CPC" name="BAL_CPC" class="js-select2 form-control" style="width: 100%">
												<?php foreach($cpc_list as $cpc){?>
													<option></option>
													<option value="<?= $cpc['CPC_Id']?>"><?= $cpc['CPC_Desc']?></option>	
												<?php }?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Bank Account:</label>
										<div class="col-md-8">
											<select id="BA" name="BAL_BalAccountNo" class="js-select2 form-control" style="width: 100%">
												<option></option>
											</select>
										</div>
									</div>
								
							</div>
						</div>
					</div>
					<div class="table-responsive datatable">
						<table tid="tbl-detail" class="table table-striped table-bordered table-hover dataTables-example compact">
							<thead>
								<tr>
									<?php if($type != 'view'):?>
										<th>Action</th>
									<?php endif; ?>
									<th>Entry No.</th>
									<th>Document Type</th>
									<th>Document No</th>
									<th>Document Date</th>
									<th>Cost/Profit Center</th>
									<th>Bank Account No.</th>
									<th>Bank Account Name</th>
									<th>Debit</th>
									<th>Credit</th>
									<th>Currency</th>
									<th>Amount</th>
									<th>Amount LCY</th>
									<th>Status</th>
									<th>Posted By</th>
									<th>Date Posted</th>
								</tr>
							</thead>
							<tbody>
								<?php if($type != 'add'):?>
								<?php foreach($detail as $row_detail):?>
								<tr class="ba-detail" data-todo="<?= $type?>" data-line-no> 
									<?php if($type != 'view'): ?>
									<td class="text-center">
										<button class="edit-ba-detail btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button"><i class="fa fa-pencil"></i></button> 
										<button class="delete-ba-detail btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" style=""><i class="fa fa-trash-o"></i></button>
									<?php endif;?>
									</td>
									<td></td>
									<td class="col-xs-1 text-center"></td>
									<td></td>
									<td class="col-xs-1 text-center" data-doctype="<?= $row_detail['BAL_DocDate']?>"><?= $row_detail['BAL_DocDate']?></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							<?php endforeach;?>
							<?php endif; ?>
							</tbody>
								<tfoot style="display: none"></tfoot>
						</table>
						<div class="pull-right">
							<button type="button" id="add-new" class="btn btn-primary" value="add">Add & New</button>
							<button type="button" id="add-close" class="btn btn-primary" value="add">Add & Close</button>
						</div>
					</div>
		</form>
	</div>



	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

		<!-- GLOBAL VARIABLE -->
	<script>

		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['BA_BankID'] ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/master_file/bank_account' ?>';
		var module_folder 			= 'administration/master_file';
		var module_controller 		= 'Bank_account';
		var module_name 			= 'Bank Account';
		var isRequiredDetails 		= true;
	</script>

	<script src="<?php echo JS_DIR?>app/administration/master_file/bank_account/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
</section>