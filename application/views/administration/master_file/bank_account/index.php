<style>
tfoot {
	display: table-header-group;
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Bank Account</h5>
			<div class="pull-right ibox-tools">
			</div>
		</div>
		<div class="ibox-content">
			<?= generate_table($table_hdr)?>
		</div>
	</div>


	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- Moment -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Page-Level Scripts -->
	<script type="text/javascript">
		var tableindex;
		var col_center	 = <?= json_encode($hdr_center)?>;
		var table_id 	 = '<?= key($table_hdr)?>';
	</script>
	<script src="<?php echo JS_DIR?>app/administration/master_file/bank_account/index.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/advsearch.js"></script>
</section>