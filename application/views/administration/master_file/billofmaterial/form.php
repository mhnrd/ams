<style>
tfoot {
	display: table-header-group;
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style type="text/css">
	@media only screen and (max-width: 768px) {
		.fixed-header{
			display: none;
		}
	}
</style>

<section id="add-bom" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
		<h5>Bill Of Material</h5>
			<div class="pull-right ibox-tools">
				<a href="<?= (isset($_SESSION['Is_DocumentApproval']) && $_SESSION['Is_DocumentApproval'] === true) ? $_SESSION['document-approval-url'] : DOMAIN."master_file/".$this->uri->segment(2); ?>" class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back">
					<span class="fa fa-reply"></span>					
				</a>
				<?php if($type != 'view'): ?>
				<button id="save" type="button" class="btn btn-primary btn-outline" data-toggle="tooltip" data-placement="bottom" title="<?php echo ($type == 'add') ? 'Save' : 'Update' ?>" data-todo="<?= $type ?>" data-doc-no="<?= $header['BOM_DocNo'] ?>"><span class="glyphicon glyphicon-floppy-disk"></span>
				</button>
				<?php else: ?>
				<?php  if(isset($header['functions'])):
								foreach($header['functions'] as $row): ?>
									<?= $row ?>
					<?php 		endforeach; 
						   endif; ?>
				<button id="print" type="button" class="btn btn-info btn-outline" data-toggle="tooltip" data-placement="bottom" title="Print" data-todo="<?= $type ?>" data-doc-no="<?= $header['BOM_DocNo'] ?>">
					<span class="glyphicon glyphicon-print"></span>
				</button>
				<?php endif; ?>		
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">
			<?php
				/*
				Converting submit to ajax is more better in this project
				*/
			?>
			<div id="form-bom" class="form-horizontal">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-3 control-label">Doc No:</label>
							<div class="col-md-9">
								<input type="text" name="BOM_DocNo" class="form-control" readonly value="<?php echo $header['BOM_DocNo']?>"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Item No:</label>
							<div class="col-md-9">
								<?php if($type != 'view'): ?>
								<select id="bom-item-no" data-type="item-no" class="bom-item" style="width: 100%">
									<option></option>
									<?php if($type == 'update'): ?>
										<?php foreach($item_list as $row): ?>
											<option value="<?php echo $row['IM_Item_Id'] ?>" selected><?php echo $row['IM_Item_Id'] ?></option>
										<?php endforeach; ?>
									<?php endif; ?>
								</select>
								<?php else: ?>
									<input type="text" class="form-control" readonly name="BOM_ItemNo" value="<?php echo $header['BOM_ItemNo']?>">
								<?php endif;  ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Description:</label>
							<div class="col-md-9">
								<?php if($type != 'view'): ?>
								<select id="bom-item-description" data-type="item-description" class="bom-item" style="width: 100%">
									<option></option>
									<?php if($type == 'update'): ?>
										<?php foreach($item_list as $row): ?>
											<option value="<?php echo $row['IM_Sales_Desc'] ?>" selected><?php echo $row['IM_Sales_Desc'] ?></option>
										<?php endforeach; ?>
									<?php endif; ?>
								</select>
								<?php else: ?>
									<input type="text" class="form-control" readonly name="BOM_ItemDescription" value="<?php echo $header['BOM_ItemDescription']?>">
								<?php endif;  ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">BOM Description:</label>
							<div class="col-md-9">
								<input id="bom-description" type="text" name="BOM_BOMDescription" class="form-control" placeholder="Enter BOM Description" value="<?= $header['BOM_BOMDescription']?>">
							</div>
						</div>
					</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="col-md-3 control-label">Location:</label>
								<div class="col-md-9">
									<?php if($type == 'add'): ?>
										<select name="BOM_Location" class="js-select2" style="width: 100%">
											<?php foreach($location_list as $location){ ?>
											<option value="<?= $location['SP_ID'] ?>" <?= ($header['BOM_Location'] == $location['SP_ID']) ? 'selected' : ''?>><?= $location['SP_StoreName'] ?></option>
											<?php }?>
										</select>
									<?php else: ?>
										<input type="text" value="<?= $header['SP_StoreName']?>" readonly class="form-control">
										<input type="hidden" name="BOM_Location" value="<?= $header['BOM_Location']?>" readonly class="form-control">
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Doc Date:</label>
								<div class="col-md-9">
									<input id="bom-doc-date" type="text" class="form-control" readonly value="<?php echo $header['BOM_DocDate']?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Yield:</label>
								<div class="col-md-4" style="padding-right: 0px;">								
									<input id="bom-yield" min="1" type="number" name="BOM_YieldQty" class="form-control text-right" value="<?php echo number_format($header['BOM_YieldQty'], 2, ".", "") ?>">
								</div>
								<div class="col-md-5">
									<?php if($type != 'view'): ?>
										<?php if($type == 'add'): ?>
											<select id="bom-yield-uom" class="js-select2" style="width: 100%"></select>
										<?php else: ?>
											<select id="bom-yield-uom" class="js-select2" style="width: 100%">
												<?php foreach($uom as $list): ?>
													<option value="<?= $list['AD_Id'] ?>" data-qty="<?= $list['IUC_Quantity'] ?>" <?= $list['AD_Id'] == $header['BOM_YieldUomID'] ? 'selected' : '' ?>><?= $list['AD_Desc'] ?></option>
												<?php endforeach; ?>
											</select>
										<?php endif; ?>
									<?php else: ?>
										<input type="text" id="bom-yield-uom" class="form-control" readonly name="BOM_YieldUomID" value="<?php echo $header['BOM_YieldUomName']?>">
									<?php endif;  ?>	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Batch Size:</label>
								<div class="col-md-4" style="padding-right: 0px;">								
									<input id="bom-batch-size" min="1" name="BOM_BatchQty" type="number" class="form-control text-right" value="<?php echo number_format(1, 2) ?>" readonly>
								</div>
								<div class="col-md-5">
									<?php if($type != 'view'): ?>
									<select id="bom-batch-size-uom" class="js-select2" style="width: 100%">
										<?php foreach($uom_list as $uom){ ?>
										<option value="<?php echo $uom['AD_Id'] ?>" <?php echo ($uom['AD_Id'] == $header['BOM_BatchUomID']) ? "selected" : "" ?>><?php echo $uom['AD_Code'] ?></option>
										<?php }?>
									</select>
									<?php else: ?>
										<input id="bom-batch-size-uom" type="text" class="form-control" readonly name="BOM_BatchUomID" value="<?php echo $header['BOM_BatchUomName']?>">
									<?php endif;  ?>	
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="col-md-3 control-label">Status:</label>
								<div class="col-md-9">
									<input id="bom-status" type="text" class="form-control" readonly value="<?php echo $header['BOM_Status'] ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Version:</label>
								<div class="col-md-9">
									<input type="text" id="bom-version" class="version form-control" readonly value="<?php echo $header['BOM_Version']?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Remarks:</label>
								<div class="col-md-9">
									<textarea id="bom-remarks" type="text" name="BOM_Remarks" class="form-control" placeholder="Enter Remarks"><?php echo $header['BOM_Remarks']?></textarea>
								</div>
							</div>
						</div>	
				</div>
			</div>	

				

				<div class="hr-line-dashed"></div>

				<?php if($type != 'view'): ?>
					<div class="table-responsive fixed-header" style="width: 100%;margin-bottom: -20px">
						<table class="table table-bordered table-striped" id="bom-fixed-header">
							<thead>
								<tr>
									<?php if($type != 'view'): ?>
									<th class="col-xs-1 text-center">
										<button type="button" id="add-detail-template" class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="#"><i class="fa fa-plus"></i></button>
									</th>
									<?php endif; ?>
									<th class="col-xs-1 text-center">Item No</th>
									<th class="col-xs-2 text-center">Item Description</th>
									<th class="col-xs-1 text-center">Qty</th>
									<th class="col-xs-1 text-center">UOM</th>
									<th class="col-xs-1 text-center">Unit Cost</th>
									<th class="col-xs-1 text-center">Total Cost</th>
									<th class="col-xs-1 text-center">Waste</th>
									<th class="col-xs-1 text-center">Required Qty</th>
									<th class="col-xs-2 text-center">Comment</th>
								</tr>
							</thead>
						</table>
					</div>
				<?php endif; ?>
				
				<div class="table-responsive datatable" <?= ($type != 'view' ? 'style="max-height: 200px"' : '') ?>>				
					<table id="bom-detail-list" class="table table-striped table-bordered table-hover dataTables-example" style="width:100%" >
						<?php if($type != 'view'): ?>
							<thead class="orig-header" style="display: none">
								<tr>
									<?php if($type != 'view'): ?>
									<th class="col-xs-1 text-center">
										<button type="button" id="add-detail-template" class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="#"><i class="fa fa-plus"></i></button>
									</th>
									<?php endif; ?>
									<th class="col-xs-1 text-center">Item No</th>
									<th class="col-xs-2 text-center">Item Description</th>
									<th class="col-xs-1 text-center">Qty</th>
									<th class="col-xs-1 text-center">UOM</th>
									<th class="col-xs-1 text-center">Unit Cost</th>
									<th class="col-xs-1 text-center">Total Cost</th>
									<th class="col-xs-1 text-center">Waste</th>
									<th class="col-xs-1 text-center">Required Qty</th>
									<th class="col-xs-2 text-center">Comment</th>
								</tr>
							</thead>
						<?php endif; ?>
						<?php if($type == 'view'): ?>	
							<thead>	
								<tr>
									<th class="col-xs-1 text-center">Item No</th>
									<th class="col-xs-2 text-center">Item Description</th>
									<th class="col-xs-1 text-center">Qty</th>
									<th class="col-xs-1 text-center">UOM</th>
									<th class="col-xs-1 text-center">Unit Cost</th>
									<th class="col-xs-1 text-center">Total Cost</th>
									<th class="col-xs-1 text-center">Waste</th>
									<th class="col-xs-1 text-center">Required Qty</th>
									<th class="col-xs-2 text-center">Comment</th>
								</tr>
							</thead>
						<?php endif; ?>
						<tfoot style="display: none"></tfoot>
						<tbody>
							<?php if($type == 'update'): ?>
								<?php foreach($bom_detail as $row_detail): ?>
									<tr class="bom-detail" data-action="<?= $type ?>" data-line-no="<?= $row_detail['BOMD_LineNo'] ?>">
										<?php if($type != 'view'): ?>
											<td class="text-center col-xs-1">
												<button class="edit-bom-detail btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button">
													<i class="fa fa-pencil"></i>
												</button> 
												<button class="delete-bom-detail btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button">
													<i class="fa fa-trash-o"></i>
												</button>
											</td>
										<?php endif; ?>
										<td class="text-center col-xs-1" data-item-no="<?= $row_detail['BOMD_ItemNo'] ?>" data-is-bom="<?= $row_detail['BOMD_isBOM'] ?>" data-bom-no="<?= $row_detail['BOMD_BOMNo'] ?>"><?= $row_detail['BOMD_ItemNo'] ?></td>
										<td class="col-xs-2" data-item-description="<?= $row_detail['IM_Sales_Desc'] ?>"><?= $row_detail['IM_Sales_Desc'] ?></td>
										<td class="text-right col-xs-1" data-quantity="<?= $row_detail['BOMD_Qty'] ?>"><?= number_format($row_detail['BOMD_Qty'], 4) ?></td>
										<td class="text-center col-xs-1" data-uom="<?= $row_detail['BOMD_UomID'] ?>" data-base-uom-id="<?= $row_detail['BOMD_BaseUomID'] ?>" data-base-uom-qty="<?= $row_detail['BOMD_BaseUomQty'] ?>"><?= $row_detail['BOMD_UOM'] ?></td>
										<td class="text-right col-xs-1" data-unit-cost="<?= $row_detail['BOMD_UnitCost'] ?>"><?= number_format($row_detail['BOMD_UnitCost'], 4) ?></td>
										<td class="text-right col-xs-1" data-total-cost="<?= $row_detail['BOMD_TotalCost'] ?>"><?= number_format($row_detail['BOMD_TotalCost'], 2) ?></td>
										<td class="text-right col-xs-1" data-waste="<?= $row_detail['BOMD_Waste'] ?>"><?= number_format($row_detail['BOMD_Waste'], 2) ?></td>
										<td class="text-right col-xs-1" data-required-qty="<?= $row_detail['BOMD_RequiredQty'] ?>"><?= number_format($row_detail['BOMD_RequiredQty'], 4) ?></td>
										<td class="col-xs-2" data-comment="<?= $row_detail['BOMD_Comment'] ?>"><?= $row_detail['BOMD_Comment'] ?></td>
									</tr>
								<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
				
				
		</div>
	</div>
	
	<?php $this->load->view('master_file/billofmaterial/detail-template') ?>

    <!-- Select2 -->
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
    <!-- Moment.js -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<!-- Moment.js -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>

	<!-- GLOBAL VARIABLES -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['BOM_DocNo'] ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/master_file/bom' ?>'
		var tableindex
		<?php if($type == 'view'): ?>
			var ns_location = '<?= $header['BOM_Location'] ?>';
			var target_url = '<?= DOMAIN.'master_file/bom/' ?>';
		<?php endif; ?>
	</script>

	<!-- VIEW MODAL -->
	<?php  if($type == 'view'): ?>
		<?php $this->load->view('components/track_document'); ?>
	<?php endif;  ?>

	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
	<script src="<?php echo JS_DIR?>app/master_file/billofmaterial/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/advsearch.js"></script>
</section>