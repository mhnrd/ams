<link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/css/bootstrap.min.css">
<style type="text/css">
main{
	padding: 110px 0px 80px;
	width: 100%;
}
table {
	width: 100%;
    table-layout: fixed;
    word-wrap: break-word;
}

.indented{
	padding-left: 20px;
}
.header, .header td{
    font-size: 11px;
}

.detail, .detail td{
	font-size: 9px;
}
table.detail, table.detail td, table.detail th{
	border-color: black;
	padding: 2px;
}
.gray, .gray th, .gray td{
	background-color:lightgrey;
}
.light-gray, .light-gray th, .light-gray td{
	background-color:#f1f1f1;
}
.bottomborder td{
	border-bottom: 1px solid black;
}

.header > div > table > tr > td strong{
	font-size:14px;
	font-weight:bold;
}

.colheader{
	width: 33.33%!important;
}

</style>
<htmlpageheader name="header">
		<!-- TITLE REPORT -->
		<table class=" table detail">
			<thead>
				<tr>
					<td style="width: 60%"></td>
					<td class="text-right"><?= Date('m/d/Y') ?></td>
					<td class="text-right">Page {PAGENO} of {nbpg}</td>
				</tr>
			</thead>
		</table>
	</htmlpageheader>

<main>
	<div>
		<table style=" width: 100%; height: 100%; margin-top: -80px" class="table header">
			<thead>
				<tr>
					<th class="text-left" style="font-size:14px;font-weight:bold; width: 6%">
						<img src="<?php echo IMG_DIR; ?>ttech-logo.png" alt="TTC Icon" width="32" height="32" style="margin-left: 0px;width: 40px; height:40px">
					</th>
					<th style="font-size:14px;font-weight:bold">TORRES TRADING CORP<br>BILL OF MATERIAL</th>-
				</tr>
			</thead>	
		</table>
		<table class="table header">
					<tr>
						<td><span style="font-weight:bold;">Doc No:</span> <?= $header['BOM_DocNo']?></td>
						<td><span style="font-weight:bold;">Location:</span> <?= $header['SP_StoreName']?></td>
						<td><span style="font-weight:bold;">Status:</span> <?= $header['BOM_Status']?></td>
					</tr>
					<tr>
						<td><span style="font-weight:bold;">Item No:</span> <?= $header['BOM_ItemNo']?></td>
						<td><span style="font-weight:bold;">Doc Date:</span> <?= date_format(date_create($header['BOM_DocDate']),'m/d/Y')?></td>
						<td><span style="font-weight:bold;">Version:</span> <?= $header['BOM_Version']?></td>
					</tr>
					<tr>
						<td><span style="font-weight:bold;">Description:</span> <?= $header['BOM_ItemDescription']?></td>
						<td><span style="font-weight:bold;">Yield:</span> <?= number_format($header['BOM_YieldUomID'],2) ?> <?= $header['BOM_YieldUomName']?></td>
						<td><span style="font-weight:bold;">Remarks:</span> <?= $header['BOM_Remarks']?></td>
					</tr>
					<tr>
						<td><span style="font-weight:bold;">BOM Description:</span> <?= $header['BOM_BOMDescription']?></td>
						<td><span style="font-weight:bold;">Batch Size:</span> <?= number_format($header['BOM_BatchUomID'],2) ?> <?= $header['BOM_BatchUomName']?></td>		
					</tr>
		</table>

	</div>

</htmlpageheader>
<htmlpagefooter name="footer">

</htmlpagefooter>
<sethtmlpageheader name="header" value="on" show-this-page="1" />
<sethtmlpagefooter name="footer" value="on" />

	<div>
		<table class="table table-bordered detail table-responsive" id="main-table" style="margin-top: 0px">
			<thead>
				<tr class="gray">
					<th class="text-center">Item No</th>
					<th class="text-center">Item Description</th>
					<th class="text-center">Qty</th>
					<th class="text-center">UOM</th>
					<th class="text-center">Unit Cost</th>
					<th class="text-center">Total Cost</th>
					<th class="text-center">Waste</th>
					<th class="text-center">Required Qty</th>
					<th class="text-center">Comments</th>
				</tr>

			</thead>
			<tbody>
				<?php foreach($bom_detail as $row): ?>
					<tr>
						<td class="text-center"><?= $row['BOMD_ItemNo']?></td>
						<td><?= $row['IM_Sales_Desc']?></td>
						<td class="text-right"><?= number_format($row['BOMD_Qty'],4) ?></td>
						<td class="text-center"><?= $row['AD_Desc']?></td>
						<td class="text-right"><?= number_format($row['BOMD_UnitCost'],2) ?></td>
						<td class="text-right"><?= number_format($row['BOMD_TotalCost'],2) ?></td>
						<td class="text-right"><?= number_format($row['BOMD_Waste'],2)?></td>
						<td class="text-right"><?= number_format($row['BOMD_RequiredQty'],4)?></td>
						<td><?= $row['BOMD_Comment']?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</main>
