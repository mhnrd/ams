<style>
tfoot {
	display: table-header-group;
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Bill of Material List</h5>
		</div>
		<div class="ibox-content">
			<div class="table-responsive datatable">
				<table class="table table-striped table-bordered table-hover dataTables-example compact" id="tbl-bom">
					<thead>
						<tr>
							<th class="text-center col-xs-1">
								<?php if($access_header === true): ?>
								<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="<?= DOMAIN;?>master_file/bom/add"><i class="fa fa-plus"></i></a>
								<?php endif; ?>
							</th>
							<th class="text-center col-xs-2">BOM No.</th>
							<th class="text-center col-xs-1">Item No.</th>
							<th class="text-center col-xs-2">BOM Description</th>
							<th class="text-center col-xs-2">Yield / UOM</th>
							<th class="text-center col-xs-1">Status</th>
							<th class="text-center col-xs-2">Remarks</th>
						</tr>
					</thead>
					<tfoot style="display: none"></tfoot>
				</table>
			</div>

		</div>
	</div>
	
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- Moment -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Page-Level Scripts -->
	<script type="text/javascript">
		var tableindex;
	</script>
	<script src="<?= JS_DIR;?>app/master_file/billofmaterial/index.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/advsearch.js"></script>
</section>