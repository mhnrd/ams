<style type="text/css">	.clockpicker-popover { z-index: 999999 !important;} div.form-group{margin-bottom: 5px;}</style>
<div class="modal inmodal animated fadeInUp" id="bom-detail" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content<?php /* animated flipInY */ ?>">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px; color: black!important;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Bill of Material Detail</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body" style="padding-bottom: 10px">

					<div class="row">
						<div class="col-md-12">
						<div class="form-group">
								<label class="control-label">Item No:</label>
								<div >
									<select id="item-no" data-type="item-no" class="item" style="width: 100%">
										<option></option>
									</select>	
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Description:</label>
								<div >
									<select id="item-description" data-type="item-description"  class="item" style="width: 100%">
										<option></option>
									</select>
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Qty:</label>
								<div >
									<input id="quantity" type="number" min="1" value="1" class="text-right form-control waste-compute"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">UOM:</label>
								<div >
									<select id="uom-list" class="js-select2" style="width: 100%" name="BOMD_UOM">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Waste %:</label>
								<div >
									<input id="waste" type="number" min="1" max="100" value="1" class="text-right form-control waste-compute"/>
								</div>
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Unit Cost:</label>
								<div >
									<input id="unit-cost" type="number" readonly class="text-right form-control" data-unit-cost="0.00" value="0.00"/> 
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Total Cost:</label>
								<div >
									<input readonly id="total-cost" type="text" data-total-cost="5" class="text-right form-control" style="padding-right: 25px!important" /> 
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Req. Qty:</label>
								<div >
									<input type="text" id="required-qty" readonly class="text-right form-control" value="0.0000">
								</div>
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Comment:</label>
								<div >
									<textarea id="comment" class="form-control"></textarea>
								</div>
							</div>
						</div>		
					</div>		
				</div>
				<!-- Input type Hidden -->
				<input type="hidden" name="BOMD_BaseUOM">
				<input type="hidden" name="BOMD_BaseUomQty" value="1">
				<input type="hidden" id="line-no" name="BOMD_LineNo" value=""/>

				<!-- MODAL FOOTER -->
				<div class="modal-footer bg-default">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="add-new" class="btn btn-primary" value="add">Add & New</button>
					<button type="button" id="add-close" class="btn btn-primary" value="add">Add & Close</button>	
				</div>
		</div>
	</div>
</div>