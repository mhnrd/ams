<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">

<section id="form-ig" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
		<h5>Item Group</h5>
			<div class="pull-right ibox-tools">
				<a href="<?= DOMAIN."application_setup/".$this->uri->segment(2); ?>" class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" >
					<span class="fa fa-reply"></span>					
				</a>
				<button id="save" type="button" class="btn btn-primary btn-outline" data-toggle="tooltip" data-placement="bottom" title="Save" data-todo="<?= $type ?>" data-id-no="<?= $header['IG_Id'] ?>">
					<span class="glyphicon glyphicon-floppy-disk"></span>
				</button>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">	
			<div id="form-item-group" class="form-horizontal">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group" style="width: 50%">
							<label class="col-md-3 control-label">Code:</label>
							<div class="col-md-9">
								<input type="text" class="form-control" <?= ($type=='add')?"":"readonly";?> name="IG_Id" value="<?= $header['IG_Id']?>"/>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group" style="width: 50%">
							<label class="col-md-3 control-label">Description:</label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="IG_Desc" value="<?= $header['IG_Desc']?>"/>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
	
	<?php $this->load->view('application_setup/item_group/detail-template'); ?>
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <!-- Select2 -->
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/clockpicker/clockpicker.js"></script>
	<!-- Global Declaration -->
	<script>
		var output_type 			= '<?= $type ?>';
		var doc_no 					= '<?= $header['IG_Id'] ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/application_setup/item-group' ?>'
	</script>
	<script src="<?php echo JS_DIR?>app/application_setup/item_group/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
	

</section>