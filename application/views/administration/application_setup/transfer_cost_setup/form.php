<style>
	tfoot{
		display: table-header-group;
	}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
	@media only screen and (max-width: 768px){
		.fixed-header{
			display: none;
		}
	}
</style>
<section id="add-supplier" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<h5>Transfer Cost Setup</h5>
			<div class="pull-right ibox-tools">
				<a href="<?= DOMAIN."application_setup/".$this->uri->segment(2);?>" class="btn btn-outline btn-warning" data-toggle="tooltip" data-placement="bottom" title="Back" id="back"><span class="fa fa-reply"></span></a>

				<button id="save" type="button" class="btn btn-info btn-outline" data-toggle="tooltip" data-placement="bottom" title="Save"><span class="glyphicon glyphicon-floppy-disk"></span>
				</button>
			</div>
		</div>
		<div class="ibox-content"> 
			<?php
				/*
				Converting submit to ajax is more better in this project
				*/
			?>
			<div id="form-cost-profit-center" class="form-horizontal">
				<!--GENERAL INFORMATION-->
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6"> 
							<div class="form-group">
								<label class="col-md-4 control-label">ID:</label>
								<div class="col-md-8">
									<input type="text" id="CPC_Id" name="CPC_Id"  class="form-control" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Description:</label>
								<div class="col-md-8">
									<input type="text" id="CPC_Desc" name="CPC_Desc" class="form-control" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Class:</label>
								<div class="col-md-8">
									<select name="CPC_FK_Class" class="js-select2 form-control" style="width: 100%;" value=""	>
								</select>
									
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
	<a href="#" class="btnTop btn btn-primary" role="button" data-toggle="tooltip" data-placement="top" title="Go to Top"><i class="fa fa-arrow-up"></i></a>
	
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['CPC_Id'] ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/master_file/cost_profit_center' ?>';

		
	</script>
	
</section>