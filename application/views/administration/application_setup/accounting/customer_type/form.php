<style>
	tfoot{
		display: table-header-group;
	}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
	/*.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: black;
    line-height: 22px;
}*/
	/*.form-group{
		margin-bottom: 5px!important;
		font-size: 11px;
	}*/
	.form-control{
		font-size: 11px;
		text-transform: capitalize;
	}
	.select2-results__options{
	        font-size:11px !important;
	 }
	@media only screen and (max-width: 768px){
		.fixed-header{
			display: none;
		}
	}
</style>
<section id="add-cost-profit-center" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<label><h5>Principal Type</h5></label>
		</div>
		
		<div class="ibox-content"> 
			<?php
				/*
				Converting submit to ajax is more better in this project
				*/
			?>
			<div id="form-cost-profit-center" class="form-horizontal">
				<form id="form-header">
						<div class="col-12">
							<div class="col-md-5 col-12"> 
								<div class="form-group">
									<div class="row">	
										<div class="col-3" style="text-align: right;">	
											<label class="control-label"> <b class="text-danger">*</b>Principal Type ID:</label>
										</div>
										<div class="col-9">
											<input type="text" id="CT_Id" name="CT_Id" required class="form-control required-field" value="<?= $header['CT_Id']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic;font-size: 12px; display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label class="control-label"> <b class="text-danger">*</b>Principal Type:</label>
										</div>
										<div class="col-9">
											<input type="text" id="CT_Description"  required name="CT_Description" class="form-control required-field" value="<?= $header['CT_Description']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
										</div>
									</div>
								</div>	
							</div>	
						</div>
				</form>
			</div>
			<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['CT_Id'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['CT_Id'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['CT_Id'] ?>';
		var update_link				= '<?= './update?id='.md5($header['CT_Id'])?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/application_setup/accounting/customer_type' ?>';
		var module_folder 			= 'administration/application_setup/accounting';
		var module_controller 		= 'customer_type'
		var module_name 			= 'Customer Type'; 
		var isRequiredDetails 		= false;
		<?php if($type == 'view'): ?>
			
			var target_url = '<?= DOMAIN.'administration/application_setup/accounting/customer_type/' ?>';
		<?php endif; ?>
	</script>
	<script src="<?php echo JS_DIR?>app/administration/application_setup/accounting/customer_type/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
	
</section>