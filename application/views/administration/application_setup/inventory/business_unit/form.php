<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<style>
	div.sa-button-container .cancel{
		background: #DD6B55!important
	}
	div.sa-button-container .cancel:hover{
		opacity: 0.8!important;
	}
</style>

<section class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<h5>Business Unit</h5>
		</div>
		<div class="ibox-content">
			<!-- Location -->
				<div id="form-user-role-setup" class="form-horizontal">
					<form id="form-header">
						<div class="col-12">
							<div class="row">
								
								<div class="col-md-6 col-12">
									<div class="form-group">
										<div class="row">
											<label class="col-3">BU Code:</label>
											<div class="col-9">													
												<input type="text" name="BU_ID" tabindex="1" class="form-control" required <?= ($type != 'add' ? 'readonly' : '') ?> value="<?= $header['BU_ID'] ?>">
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="row">
											<label class="col-3">Description:</label>
											<div class="col-9">
												<input type="text" name="BU_Description" tabindex="2" class="form-control" required <?= ($type == 'view' ? 'readonly' : '') ?> value="<?= $header['BU_Description'] ?>">
											</div>
										</div>
									</div>

									
								</div>

							</div>
						</div>

					</form>

				</div>

				<hr>
				<div>
					<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="3">
						<span class="fa fa-reply"></span>					
					</button>
					<?php if($type != 'view'): ?>
						<?php if ($type == 'add'): ?>
							<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['BU_ID'] ?>" data-action="save-new" tabindex="4">
								<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
							</button>
						<?php endif ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['BU_ID'] ?>" data-action="save-close" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
						</button>
					<?php endif; ?>
				</div>

			</div>

		</div>


	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/clockpicker/clockpicker.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>

		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['BU_ID'] ?>';
		var update_link				= '<?= './update?id='.md5($header['BU_ID'])?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'administration/application_setup/inventory/business_unit' ?>';
		var module_folder 			= 'administration/application_setup/inventory';
		var module_controller 		= 'business_unit';
		var module_name 			= '<?= getDisplayName($this->uri->segment(4)) ?>';
		var isRequiredDetails 		= false;
	</script>

	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>	

	</section>