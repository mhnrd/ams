<style>
tfoot {
	display: table-header-group;
}
.dataTables-example tbody tr td a.delete-button:hover{
	color: white!important;
}
.advance-search:hover, .advance-search-small:hover{
	color: white!important;
}
@media screen and (max-width: 425px){
	#advance-search-small{
		display: inline!important;
	}
	.table-tools{
		margin-right: 5px!important;
		margin-top: 5px!important;
	}
	#advance-search{
		display: none;
	}
	#holiday_filter{
		margin-left: -50px!important;
	}
	#holiday_detail_filter{
		margin-left: -50px!important;
	}
	#clear-search-small{
		margin-top: 4px!important;
	}
	#holiday_filter{
		margin-right: 0px!important;
	}
	#holiday_detail_filter{
		margin-right: 0px!important;
	}
}

@media screen and (max-width: 768px){
	#holiday_filter input{
		width: 60%!important;
	}
	#holiday_filter{
		margin-right: 500px!important;
	}
	#holiday_detail_filter input{
		width: 60%!important;
	}
	#holiday_detail_filter{
		margin-right: 500px!important;
	}
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<label><h5>Tax Matrix</h5></label>
		</div>
		<div class="ibox-content" style="display: none">
			<?= generate_table($table_hdr)?>
		</div>
		<div class="ibox-content">
			<?= generate_table($detail['table_dtl']) ?>
		</div>
	</div>
	
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- Moment -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Page-Level Scripts -->
	<script type="text/javascript">	
		var col_center		= <?= json_encode($hdr_center)?>;
		var table_id 		= '<?= key($table_hdr) ?>';
		var process_action 	= 'Tax Exemptions';
		var table_name 		= 'tblTaxMatrix';
		var controller_url 	= 'administration/application_setup/payroll/tax_exemptions/';	
		var table_dtl_id 	= '<?= key($detail['table_dtl'])?>';
		var tableindex;
		var tableindex2;
	</script>
	<script src="<?php echo JS_DIR?>app/administration/application_setup/payroll/tax_exemptions/index.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/advsearch.js"></script>
	<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
	<script src="<?php echo JS_DIR ?>app/rules/index_functions.js"></script>
</section>