<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<style>
	.select2-results__options{
	        font-size:11px !important;
	 }
	 input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
	    -webkit-appearance: none;
	}
	@media only screen and (max-width: 768px){
		.fixed-header{
			display: none;
		}
	}
</style>

<section class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<label><h5>Tax Matrix</h5></label>
		</div>

		<div class="ibox-content">
			<!-- Tax Matrix -->
			<div id="form-tax-exemptions" class="form-horizontal">
				<form id="form-header">
					<div class="row">
						<div class="col-md-5 col-12">
							<div class="form-group"  style="display: none">
								<div class="row">
									<div class="col-3">
										<label>Tax ID: </label>
									</div>
									<div class="col-7">
										<input type="number" class="text-right form-control required-field" required name="TM_ID" value="<?= $header['TM_ID']?>" style="display: none">
									</div>
								</div>
							</div>	
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
										<label>Tax Type: </label>
									</div>
									<div class="col-7">
										<select class="js-select2 form-control required-field" required name="TM_TaxType" style="width: 100%">
											<option value="Yearly" <?= ($header['TM_TaxType'] == 'Yearly' ? 'selected' : '')?>>Yearly</option>			
										</select>
									</div>
								</div>
							</div>	
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
										<label><b class="text-danger">*</b>Over Amount: </label>
									</div>
									<div class="col-7">
										<input type="number" class="text-right form-control required-field" required name="TM_OverAmount" value="<?= $header['TM_OverAmount']?>" placeholder="">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
										<label><b class="text-danger">*</b>Not Over Amount: </label>
									</div>
									<div class="col-7">
										<input type="number" class="form-control required-field text-right" required name="TM_NotOver" value="<?= $header['TM_NotOver'] ?>" placeholder="">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
										<label><b class="text-danger">*</b>Tax Due: </label>
									</div>
									<div class="col-7">
										<input type="number" class="text-right form-control required-field" required name="TM_TaxDue" value="<?= $header['TM_TaxDue']?>" placeholder="">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
										<label><b class="text-danger">*</b>Percent: </label>
									</div>
									<div class="col-7">
										<input type="number" class="text-right form-control required-field" required name="TM_Percent" value="<?= $header['TM_Percent']?>" placeholder="">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>

									</div>
								</div>
							</div>	
						</div>
						
					</div>
			</form>
			<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['TM_ID'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['TM_ID'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
		</div>
	</div>


	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
    
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['TM_ID'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['TM_ID']) ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/application_setup/payroll/tax_exemptions' ?>';
		var module_folder 			= 'administration/application_setup/payroll';
		var module_controller 		= 'tax_exemptions';
		var module_name 			= 'Tax Exemptions';
		var isRequiredDetails 		= true;
		var tabledetail2;
		var action;
		var line_no;
		
	</script>

	<script src="<?php echo JS_DIR?>app/administration/application_setup/payroll/tax_exemptions/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>	

	</section>