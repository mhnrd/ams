<style>
	tfoot{
		display: table-header-group;
	}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
	 input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
	    -webkit-appearance: none;
	}
	@media only screen and (max-width: 768px){
		.fixed-header{
			display: none;
		}
	}
</style>
<section id="add-cost-profit-center" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<label><h5>SSS Table</h5></label>
		</div>
		<div class="ibox-content"> 
			<?php
				/*
				Converting submit to ajax is more better in this project
				*/
			?>
			<div id="form-cost-profit-center" class="form-horizontal">
				<form id="form-header">
						<div class="col-12">
							<div class="col-md-5 col-12">
							<div class="form-group">
									<div class="row">	
										<div class="col-3" style="text-align: right;padding-top: 5px;">	
											
										</div>
										<div class="col-9">
											<input type="hidden" id="ST_ID" required name="ST_ID"  class="form-control required-field" value="<?= $header['ST_ID']?>" placeholder="">
										</div>
									</div>
								</div> 
								<div class="form-group">
									<div class="row">	
										<div class="col-3" style="text-align: right;">	
											<label>Salary Range From:</label>
										</div>
										<div class="col-9">
											<input type="number" id="ST_SalaryRangeFrom" required name="ST_SalaryRangeFrom"  class="form-control required-field text-right" value="<?= $header['ST_SalaryRangeFrom']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label>Salary Range To:</label> 
										</div>
										<div class="col-9">
											<input type="number" id="ST_SalaryRangeTo" name="ST_SalaryRangeTo" required class="form-control required-field text-right" value="<?= $header['ST_SalaryRangeTo']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label>Credit Salary:</label> 
										</div>
										<div class="col-9">
											<input type="number" id="ST_SalaryCredit" name="ST_SalaryCredit" required class="form-control required-field text-right" value="<?= $header['ST_SalaryCredit']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label>EC:</label> 
										</div>
										<div class="col-9">
											<input type="number" id="ST_EC" name="ST_EC" required class="form-control required-field text-right" value="<?= $header['ST_EC']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label>Employer Share:</label> 
										</div>
										<div class="col-9">
											<input type="number" id="ST_Emprshare" name="ST_Emprshare" required class="form-control required-field text-right" value="<?= $header['ST_Emprshare']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label>Employee Share:</label> 
										</div>
										<div class="col-9">
											<input type="number" id="ST_Empeshare" name="ST_Empeshare" required class="form-control required-field text-right" value="<?= $header['ST_Empeshare']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="text-align: right;">
											<label>Total Contribution:</label> 
										</div>
										<div class="col-9">
											<input type="number" id="totalContribution" name="" readonly required class="form-control required-field text-right" value=""  >
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
										</div>
									</div>
								</div>		
							</div>	
						</div>
				</form>
			</div>
			<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['ST_ID'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['ST_ID'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['ST_ID'] ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/application_setup/payroll/sss_table' ?>';
		var module_folder 			= 'administration/application_setup/payroll';
		var module_controller 		= 'sss_table'
		var module_name 			= 'SSS Table'; 
		var isRequiredDetails 		= false;
		var action;
		var line_no;
		<?php if($type == 'view'): ?>
			
			var target_url = '<?= DOMAIN.'application_setup/payroll/SSS_table' ?>';
		<?php endif; ?>
	</script>
	<script src="<?php echo JS_DIR?>app/administration/application_setup/payroll/sss_table/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
	
</section>