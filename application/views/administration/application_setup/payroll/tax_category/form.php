<style>
	tfoot{
		display: table-header-group;
	}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
	    -webkit-appearance: none;
	}
	.select2-results__options{
	        font-size:11px !important;
	 }
	@media only screen and (max-width: 768px){
		.fixed-header{
			display: none;
		}
	}
</style>
<section id="add-supplier" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<label><h5>Tax Category</h5></label>
			</div>
		
		<div class="ibox-content"> 
			<?php
				/*
				Converting submit to ajax is more better in this project
				*/
			?>
			<div id="form-cost-profit-center" class="form-horizontal">
				<form id="form-header">
						<div class="col-12">
							<div class="col-md-5 col-12"> 
								<div class="form-group">
									<div class="row">
										<div class="col-4" style="text-align: right;">	
											<label class="control-label"><b class="text-danger">*</b>Tax Category Code:</label>
										</div>
										<div class="col-8">
											<input type="text" tabindex="1" id="TC_ID" name="TC_ID" required  class="form-control required-field" value="<?= $header['TC_ID']?>">
											<div class="required-label text-danger" style="font-style: italic;font-size: 12px; display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4" style="text-align: right;">
											<label class="control-label"><b class="text-danger">*</b>Tax Category Description:</label>
										</div>
										<div class="col-8">
											<input type="text" tabindex="2" id="TC_ExemtDesc" required name="TC_ExemtDesc" class="form-control required-field" value="<?= $header['TC_ExemtDesc']?>" >
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-4" style="text-align: right;">
											<label class="control-label"><b class="text-danger">*</b>Tax Exemption Amount:</label>
										</div>
										<div class="col-8">
											<input type="number" tabindex="3" id="TC_ExemptAmount" required name="TC_ExemptAmount" class="form-control required-field text-right" value="<?= $header['TC_ExemptAmount']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px;display: none;">*Required Field</div>
										</div>
									</div>
								</div>		
							</div>	
						</div>
				</form>
			</div>
			<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['TC_ID'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['TC_ID'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
				
			</div>
		</div>
	<!-- <a href="#" class="btnTop btn btn-primary" role="button" data-toggle="tooltip" data-placement="top" title="Go to Top"><i class="fa fa-arrow-up"></i></a> -->
	
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';
		var todo 					= '<?= $type ?>';		
		var doc_no 					= '<?= $header['TC_ID'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['TC_ID']) ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/application_setup/payroll/tax_category' ?>';
		var module_folder 			= 'administration/application_setup/payroll';
		var module_controller 		= 'tax_category';
		var module_name 			= 'Tax Category';
		var isRequiredDetails		= true;
		<?php if($type == 'view'): ?>
			
			var target_url = '<?= DOMAIN.'administration/application_setup/payroll/tax_category/' ?>';
		<?php endif; ?>

		
	</script>

	<script src="<?php echo JS_DIR?>app/administration/application_setup/payroll/tax_category/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
	
</section>