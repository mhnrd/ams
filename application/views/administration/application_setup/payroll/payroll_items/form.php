<style>
	tfoot{
		display: table-header-group;
	}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
	#formhides {

		display:none;

	}
	#formhides1 {

		display:none;

	}
	#formhides2 {

		display:none;

	}
	#formhides3 {

		display:none;

	}
	#formhides4 {

		display:none;

	}
	#formhides5 {

		display:none;

	}
	@media only screen and (max-width: 768px){
		.fixed-header{
			display: none;
		}
	}
</style>
<section id="add-supplier" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<label><h5>Payroll Items</h5></label>
			
		</div>
		<div class="ibox-content"> 
			<div id="form-supplier" class="form-horizontal">
				<form id="form-header">
					<div class="row">
						<div class="col-12">
							<div class="col-md-5 col-12"> 
								<div class="form-group">
									<div class="row">	
										<div class="col-3"  style="text-align: right;">
											<label class="control-label">Code:</label>
										</div>
										<div class="col-9">
											<input type="text" id="PI_id" name="PI_ID"  class="form-control required-field" required value="<?= $header['PI_ID']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic;font-size: 12px; display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
									<div class="col-3"  style="text-align: right;">	
											<label class="control-label">Description:</label>
										</div>
										<div class="col-9">
											<input type="text" id="PI_Desc" name="PI_Desc" required class="form-control required-field" value="<?= $header['PI_Desc']?>" placeholder="">
											<div class="required-label text-danger" style="font-style: italic;font-size: 12px; display: none;">*Required Field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
									<div class="col-3"  style="text-align: right;">	
											<label class="control-label">Type:</label>
										</div>
										<div class="col-9">
											<select name="PI_Type"  id="PI_Type" value="<?= $header['PI_Type'] ?>" class="form-control pitype-js-select2" style="width: 100%">
													<option></option>
													<option value="E" <?= ($header['PI_Type'] == 'E' ? 'selected' : '') ?>>Earnings</option>
													<option value="D" <?= ($header['PI_Type'] == 'D' ? 'selected' : '') ?>>Deduction</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
									<div class="col-3"  style="text-align: right;">	
											<label class="control-label">Basis To Compute:</label>
										</div>
										<div class="col-9">
											<select name="PI_Basis2Compute"  id="PI_Basis2Compute" value="<?= $header['PI_Basis2Compute'] ?>" class="form-control basis2compute-js-select2" style="width: 100%">
												<option></option>
												<option value="D" <?= ($header['PI_Basis2Compute'] == 'D' ? 'selected' : '') ?>>Daily</option>
												<option value="H" <?= ($header['PI_Basis2Compute'] == 'H' ? 'selected' : '') ?>>Hourly</option>
												<option value="M" <?= ($header['PI_Basis2Compute'] == 'M' ? 'selected' : '') ?>>Minute</option>
												<option value="A" <?= ($header['PI_Basis2Compute'] == 'A' ? 'selected' : '') ?>>Amount</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
									<div class="col-3"  style="text-align: right;">	
											<label class="control-label">Special Rate:</label>
										</div>
										<div class="col-9">
											<input type="text" id="PI_Sperate" name="PI_Sperate"  class="form-control text-right" value="<?= $header['PI_Sperate']?>" placeholder="">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
									<div class="col-3"  style="text-align: right;">	
											<label class="control-label">Regular Rate:</label>
										</div>
										<div class="col-9">
											<input type="text" id="PI_Regrate" name="PI_Regrate"  class="form-control text-right" value="<?= $header['PI_Regrate']?>" placeholder="">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
									<div class="col-3"  style="text-align: right;">	
											<label class="control-label">Taxable:</label>
											</div>							
										<div class="col-9">
											<input type="checkbox" name="PI_Taxable" value="<?= $header['PI_Taxable'] ?>" <?= ($header['PI_Taxable'] == 1 ? 'checked' : '') ?>>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
									<div class="col-3"  style="text-align: right;">	
											<label class="control-label">Allowance:</label>
										</div>
										<div class="col-9">
											<input type="checkbox" name="PI_Allowance" value="<?= $header['PI_Allowance'] ?>" <?= ($header['PI_Allowance'] == 1 ? 'checked' : '') ?>>
										</div>
									</div>
								</div>
								<div class="panel panel-default border border-dark" id="box" style="padding: 10px;">
									<div class="form-group" id="formhides">
										<div class="row">
										<div class="col-3"  style="text-align: right;">		
												<label class="control-label">Earnings Type:</label>
											</div>
											<div class="col-9">
												<select name="PI_CutoffType"  id="PI_CutoffType" value="<?= $header['PI_CutoffType'] ?>" class="form-control pi-cutofftype-js-select2"  style="width: 100%">
														<option></option>
														<option value="W" <?= ($header['PI_CutoffType'] == 'W' ? 'selected' : '') ?>>Weekly</option>
														<option value="S" <?= ($header['PI_CutoffType'] == 'S' ? 'selected' : '') ?>>Semi-Monthly</option>
														<option value="M" <?= ($header['PI_CutoffType'] == 'M' ? 'selected' : '') ?>>Monthly</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group" id="formhides1">
										<div class="row">
											<div class="col-3"  style="text-align: right;">
													<label class="control-label">Cut-Off:</label>
												</div>
											<div class="col-9">
												<select name="PI_Cutoff"  id="PI_Cutoff" value="<?= $header['PI_Cutoff'] ?>" class="form-control pi-cutoff-js-select2"  style="width: 100%">
													<option></option>
													<option value="1" <?= ($header['PI_Cutoff'] == '1' ? 'selected' : '') ?>>1st Cutoff</option>
													<option value="2" <?= ($header['PI_Cutoff'] == '2' ? 'selected' : '') ?>>2nd Cutoff</option>
													<option value="3" <?= ($header['PI_Cutoff'] == '3' ? 'selected' : '') ?>>3rd Cutoff</option>
													<option value="4" <?= ($header['PI_Cutoff'] == '4' ? 'selected' : '') ?>>4th Cutoff</option>
													<option value="5" <?= ($header['PI_Cutoff'] == '5' ? 'selected' : '') ?>>Split</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group" id="formhides2">
										<div class="row">
											<div class="col-3"  style="text-align: right;">
												<label class="control-label">Allowance Type:</label>
											</div>
											<div class="col-9">
												<select name="PI_AttAllowTypeId" class="form-control allowancetype-js-select2" style="width:100%">
													<?php foreach($allowance_type_list as $allowance_type): ?>
														<option></option>
														<option value="<?= $allowance_type['AD_Id'] ?>" <?= ($header['PI_AttAllowTypeId'] == $allowance_type['AD_Desc']) ? 'selected' : '' ?>> <?= $allowance_type['AD_Desc']?></option>
													<?php endforeach; ?>									
												</select>
											</div>
										</div>
									</div>
									<div class="form-group" id="formhides3">
										<div class="row">
											<div class="col-3"  style="text-align: right;">	
												<label class="control-label">Per Actually Worked Day</label>
											</div>							
											<div class="col-7">
												<input type="checkbox" name="PI_AttendanceBasis" value="<?= $header['PI_AttendanceBasis'] ?>" <?= $header['PI_AttendanceBasis'] == 1 ? 'checked' : '' ?>>
												<span style="font-size: 10px; color: #3498db; padding-top: 5px">The allowance will be credited to the employee if they actually clocked in and out on any specific day.</span>
											</div>

										</div>
									</div>
									<hr class="hr-line-dashed">
									<div class="form-group" id="formhides4">
										<div class="row">
										<div class="col-3"  style="text-align: right;">	
												<label class="control-label">Variability Type:</label>
											</div>
											<div class="col-9">
												<select name="PI_Variability"  id="PI_Variability" value="<?= $header['PI_Cutoff'] ?>" class="form-control pi-variability-js-select2"  style="width: 100%">
													<option></option>
													<option value="1" <?= ($header['PI_Variability'] == '1' ? 'selected' : '') ?>>According to time worked.</option>
													<option value="2" <?= ($header['PI_Variability'] == '2' ? 'selected' : '') ?>>Deduct absences only.</option>
													<option value="3" <?= ($header['PI_Variability'] == '3' ? 'selected' : '') ?>>Deduct absences and paid leave.</option>
													<option value="4" <?= ($header['PI_Variability'] == '4' ? 'selected' : '') ?>>Deduct overtime, late, undertime and night differential.</option>
													<option value="5" <?= ($header['PI_Variability'] == '5' ? 'selected' : '') ?>>Apply flat rate.</option>
													<option value="6" <?= ($header['PI_Variability'] == '6' ? 'selected' : '') ?>>Apply flat rate but deduct absences.</option>
													<option value="7" <?= ($header['PI_Variability'] == '7' ? 'selected' : '') ?>>Apply flat rate but deduct absences, late, undertime.</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group" id="formhides5">
										<div class="row">
											<div class="col-3"  style="text-align: right;">
												<label class= "control-label">Divisor</label>
											</div>
											<div class="col-3">
												<input type="text" id="PI_Divisor" name="PI_Divisor"  class="form-control text-right" value="<?= $header['PI_Divisor']?>" placeholder="">										
											</div>
											<span style="font-size: 10px; color: #3498db; padding-top: 5px;">If no divisor put 0 (zero).</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['PI_ID'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['PI_ID'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['PI_ID'] ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var update_link				= '<?= './update?id='.md5($header['PI_ID'])?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/application_setup/payroll/payroll_items' ?>';
		var module_folder 			= 'administration/application_setup/payroll';
		var module_controller 		= 'payroll_items'
		var module_name 			= 'Payroll Items'; 
		var isRequiredDetails 		= false;
		

		
	</script>

	<script src="<?php echo JS_DIR?>app/administration/application_setup/payroll/payroll_items/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
</section>
