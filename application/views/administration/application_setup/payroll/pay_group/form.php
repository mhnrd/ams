<style>
	tfoot{
		display: table-header-group;
	}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
	@media only screen and (max-width: 768px){
		.fixed-header{
			display: none;
		}
	}
</style>
<section id="add-paygroup" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<label><h5>Pay Group</h5></label>
		</div>
		<div class="ibox-content"> 
			<?php
				/*
				Converting submit to ajax is more better in this project
				*/
			?>
			<div id="form-supplier">
				<form id="form-header">
							<div class="row">	
							<!-- <div class="col-12"> -->
								 <div class="col-md-5 col-12">  
									<div class="form-group">
										<div class="row">
											<div class="col-3" style="text-align: right;">
												<label class="control-label"><b class="text-danger">*</b>Company:</label>
											</div>
											<div class="col-9">
												<?php if($type == 'add'): ?>
													<select name="G_FK_CompanyId" class="form-control company-js-select2 required-field" required style="width: 100%;">
														<?php foreach($category_list as $category):?>
															<option></option>
															<option value="<?= $category['COM_Id']?>" <?= ($header['G_FK_CompanyId'] == $category['COM_Id']) ? 'selected' : ''?>><?= $category['COM_Id'] ?></option>
														<?php endforeach;?>
													</select>
													<div class="required-label text-danger" style="font-style: italic;font-size: 12px; display: none;">*Required Field</div>
												<?php else: ?>
													<input type="text" name="" value="<?= $header['COM_Id'] ?>" readonly required class="form-control">
													<input type="hidden" name="G_FK_CompanyId" value="<?= $header['COM_Id']?>" readonly required class="form-control">
													<div class="required-label text-danger" style="font-style:italic; font-size: 12px; display:none;">*Required Field</div>
												<?php endif; ?>	
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-3"  style="text-align: right;">
												<label class="control-label"><b class="text-danger">*</b>Pay Group ID:</label>
											</div>
											<div class="col-9">
												<input type="text" name="G_ID" id="G_ID"  class="form-control required-field" required value="<?= $header['G_ID']?>" placeholder="">
												<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none;">*Required Field</div>
												
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-3"  style="text-align: right;">
												<label class="control-label">Group Description:</label>
											</div>
											<div class="col-9">
												<input type="text" name="G_Desc" id="G_Desc"  class="form-control required-field" required value="<?= $header['G_Desc']?>" placeholder="">
												<div class="required-label text-danger" style="font-style: italic; font-size:12px; display: none;">*Required Field</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-5 col-12">
									<div class="form-group">
										<div class="row">
											<div class="col-3"  style="text-align: right;">
												<label class="control-label">Work Days:</label>
											</div>
											<div class="col-9">
												<input type="text" name="G_PAY_WorkDays" id="G_PAY_WorkDays" class="form-control required-field" required value="<?= $header['G_PAY_WorkDays']?>" placeholder="">
												
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-3"  style="text-align: right;">
												<label class="control-label">Bank Account:</label>
											</div>
											<div class="col-9">
												<input type="text" name="G_PAY_BankAccount" id="G_PAY_BankAccount" class="form-control required-field" required value="<?= $header['G_PAY_BankAccount']?>" placeholder="">
												
											</div>
										</div>
									</div> 
							<!-- </div> -->
							</div>	
						</div>
						<!-- END OF FIRST ROW -->
					
					
					<div class="hr-line-dashed"></div>
					
					<div class="row">
						<div class="col-md-5 col-12"> 
							<!--<div class="form-group">
								<label class="col-md-4 control-label">Tax Deduction:</label>
								<div class="col-md-8">
									<select name="COM_PAY_TaxDeduct"  id="COM_PAY_TaxDeduct" value="<?= $header['COM_PAY_TaxDeduct'] ?>" class="form-control taxdeduction-js-select2 required-field" required style="width: 100%;">
									<option></option>
									<option value="S" <?= ($header['COM_PAY_TaxDeduct'] == 'S' ? 'selected' : '') ?>>Semi-Monthly</option>
									</select>
									<div class="required-label text-danger" style="font-style:italic; font-size: 12px; display:none;">*Required Field</div>
								</div>
							</div>-->
							<div class="form-group">
								<div class="row">
									<div class="col-3"  style="text-align: right;">
										<label class="control-label">Tax Deduction Cut-off:</label>
									</div>
									<div class="col-9">
										<select name="G_PAY_TaxCuttoff"  id="G_PAY_TaxCuttoff" value="<?= $header['G_PAY_TaxCuttoff'] ?>" class="form-control taxdeduction-cutoff-js-select2 required-field" required style="width: 100%;">
											<option></option>
											<option  value="2" <?= ($header['G_PAY_TaxCuttoff'] == '2' ? 'selected' : '') ?>>2nd Cutoff</option>
											<option  value="5" <?= ($header['G_PAY_TaxCuttoff'] == '5' ? 'selected' : '') ?>>Split</option>
										</select>
										
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3"  style="text-align: right;">
										<label class="control-label">SSS Deduction:</label>
									</div>
									<div class="col-9">
										<select name="G_PAY_SSSDeduct"  id="G_PAY_SSSDeduct" value="<?= $header['G_PAY_SSSDeduct'] ?>" class="form-control sssdeduction-js-select2 required-field" required style="width: 100%;">
											<option></option>
											<option value="W" <?= ($header['G_PAY_SSSDeduct'] == 'W' ? 'selected' : '') ?>>Weekly</option>
											<option  value="S" <?= ($header['G_PAY_SSSDeduct'] == 'S' ? 'selected' : '') ?>>Semi-Monthly</option>
											<option value="M" <?= ($header['G_PAY_SSSDeduct'] == 'M' ? 'selected' : '') ?>>Monthly</option>
										</select>
										
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3"  style="text-align: right;">
										<label class="control-label">SSS Deduction Cut-off</label>
									</div>
									<div class="col-9">
										<select name="G_PAY_SSSCutoff"  id="G_PAY_SSSCutoff" value="<?= $header['G_PAY_SSSCutoff'] ?>" class="form-control sssdeduction-cutoff-js-select2 required-field" required style="width: 100%">
											<option></option>
											<option value="1" <?= ($header['G_PAY_SSSCutoff'] == '1' ? 'selected' : '') ?>>1st Cutoff</option>
											<option value="2" <?= ($header['G_PAY_SSSCutoff'] == '2' ? 'selected' : '') ?>>2nd Cutoff</option>
											<option value="3" <?= ($header['G_PAY_SSSCutoff'] == '3' ? 'selected' : '') ?>>3rd Cutoff</option>
											<option value="4" <?= ($header['G_PAY_SSSCutoff'] == '4' ? 'selected' : '') ?>>4th Cutoff</option>
											<option value="5" <?= ($header['G_PAY_SSSCutoff'] == '5' ? 'selected' : '') ?>>Split</option>
										</select>
										
									</div>
								</div>
							</div>
						</div>

							<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-3"  style="text-align: right;">
											<label class="control-label">Pag-Ibig Deduction :</label>
										</div>
										<div class="col-9">
											<select name="G_PAY_PagibigDeduct"  id="G_PAY_PagibigDeduct" value="<?= $header['G_PAY_PagibigDeduct'] ?>" class="form-control pagibigdeduction-js-select2 required-field" required style="width: 100%;">
												<option></option>
												<option value="W" <?= ($header['G_PAY_PagibigDeduct'] == 'W' ? 'selected' : '') ?>>Weekly</option>
												<option  value="S" <?= ($header['G_PAY_PagibigDeduct'] == 'S' ? 'selected' : '') ?>>Semi-Monthly</option>
												<option value="M" <?= ($header['G_PAY_PagibigDeduct'] == 'M' ? 'selected' : '') ?>>Monthly</option>
											</select>
											
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-3"  style="text-align: right;">
											<label class="control-label">Pag-Ibig Deduction Cut Off:</label>
										</div>
										<div class="col-9">
											<select name="G_PAY_PagibigCutoff"  id="G_PAY_PagibigCutoff" value="<?= $header['G_PAY_PagibigCutoff'] ?>" class="form-control pagibigdeduction-cutoff-js-select2 required-field" required style="width: 100%;">
												<option></option>
												<option value="1" <?= ($header['G_PAY_PagibigCutoff'] == '1' ? 'selected' : '') ?>>1st Cutoff</option>
												<option value="2" <?= ($header['G_PAY_PagibigCutoff'] == '2' ? 'selected' : '') ?>>2nd Cutoff</option>
												<option value="3" <?= ($header['G_PAY_PagibigCutoff'] == '3' ? 'selected' : '') ?>>3rd Cutoff</option>
												<option value="4" <?= ($header['G_PAY_PagibigCutoff'] == '4' ? 'selected' : '') ?>>4th Cutoff</option>
												<option value="5" <?= ($header['G_PAY_PagibigCutoff'] == '5' ? 'selected' : '') ?>>Split</option>
											</select>
											
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3"  style="text-align: right;">
											<label class="control-label">Philhealth Deduction:</label>
										</div>
										<div class="col-9">
											<select name="G_PAY_PhilhealthDeduct"  id="G_PAY_PhilhealthDeduct" value="<?= $header['G_PAY_PhilhealthDeduct'] ?>" class="form-control philhealthdeduction-js-select2 required-field" required style="width: 100%">
													<option></option>
													<option value="W" <?= ($header['G_PAY_PhilhealthDeduct'] == 'W' ? 'selected' : '') ?>>Weekly</option>
													<option value="S" <?= ($header['G_PAY_PhilhealthDeduct'] == 'S' ? 'selected' : '') ?>>Semi-Monthly</option>
													<option value="M" <?= ($header['G_PAY_PhilhealthDeduct'] == 'M' ? 'selected' : '') ?>>Monthly</option>
											</select>
											
										</div>	
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3"  style="text-align: right;">
											<label class="control-label">Philhealth Deduction Cut Off:</label>
										</div>	
										<div class="col-9">
											<select name="G_PAY_PhilhealthCutoff"  id="G_PAY_PhilhealthCutoff" value="<?= $header['G_PAY_PhilhealthCutoff'] ?>" class="form-control philhealthdeduction-cutoff-js-select2 required-field" required style="width: 100%">
												<option></option>
												<option value="1" <?= ($header['G_PAY_PhilhealthCutoff'] == '1' ? 'selected' : '') ?>>1st Cutoff</option>
												<option value="2" <?= ($header['G_PAY_PhilhealthCutoff'] == '2' ? 'selected' : '') ?>>2nd Cutoff</option>
												<option value="3" <?= ($header['G_PAY_PhilhealthCutoff'] == '3' ? 'selected' : '') ?>>3rd Cutoff</option>
												<option value="4" <?= ($header['G_PAY_PhilhealthCutoff'] == '4' ? 'selected' : '') ?>>4th Cutoff</option>
												<option value="5" <?= ($header['G_PAY_PhilhealthCutoff'] == '5' ? 'selected' : '') ?>>Split</option>
											</select>
											
										</div>
									</div>
								</div>
							</div>		
						</div>	
					</form>
				<div class="hr-line-dashed"></div>	
					<div>
						<?= generate_table($table_detail) ?>
					</div>
			</div>

			<?php $this->load->view('administration/application_setup/payroll/pay_group/detail-template') ?>

			<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['G_ID'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['G_ID'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['G_ID'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['G_ID']) ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/application_setup/payroll/pay_group' ?>';
		var module_folder 			= 'administration/application_setup/payroll';
		var module_controller 		= 'pay-group';
		var module_name 			= 'Pay Group';
		var isRequiredDetails		= true;
		var tabledetail;
		var action;
		var line_no;
		if(output_type != 'add'){
			var details = <?= json_encode($details) ?>;
		}
	</script>

	<script src="<?php echo JS_DIR?>app/administration/application_setup/payroll/pay_group/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
</section>
