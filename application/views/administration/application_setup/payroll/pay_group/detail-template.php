<style type="text/css">	.clockpicker-popover { z-index: 999999 !important;} div.form-group{margin-bottom: 5px;}</style>
<div class="modal inmodal animated fadeInUp" id="detail-template" data-backdrop="static"  role="dialog" aria-hidden="true" style="margin-top: 30px">
		<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content<?php /* animated flipInY */ ?>">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" style="margin-right: 15px;" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Add User</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body" style="padding-bottom: 10px" align="center">
					<div class="form-group" style="text-align: left!important">
						<div class="row">
							<label class="col-md-3" style="text-align: center!important">User ID:</label>
							<div class="col-md-9">
								<select class="form-control text-left" name="GA_FK_User_Id"  required style="width: 100%" required>
									<option value=""></option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group" style="text-align: left!important">
						<div class="row">
							<label class="col-md-3" style="text-align: center!important">Employee ID:</label>
							<div class="col-md-9">
								<input type="text" class="form-control" readonly required  name="U_AssociatedEmployeeId">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label class="col-md-3" style="">Username:</label>
							<div class="col-md-9">
								<input type="text" class="form-control" readonly name="U_Username">
							</div>
						</div>
					</div>
				</div>
				<!-- MODAL FOOTER -->
				<div class="modal-footer bg-default">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="add-new" class="btn btn-primary" value="add">Add & New</button>	
					<button type="button" id="add-close" class="btn btn-primary" value="add">Add & Close</button>	
				</div>
		</div>
	</div>
</div>