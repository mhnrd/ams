<style>
  tfoot{
    display: table-header-group;
  }
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
  @media only screen and (max-width: 768px){
    .fixed-header{
      display: none;
    }
  }
</style>
<section id="add-supplier" class="wrapper wrapper-content">
<div class="ibox">
 <div class="ibox-title">
      <h5>Employee 201 File</h5>
      <div class="pull-right ibox-tools">
        <a href="<?= DOMAIN.'application_setup/'.$this->uri->segment(2);?>" class="btn btn-outline btn-warning" data-toggle="tooltip" data-placement="bottom" title="Back" id="back"><span class="fa fa-reply"></span></a>

        <button id="save" type="button" class="btn btn-info btn-outline" data-toggle="tooltip" data-placement="bottom" title="Save"><span class="glyphicon glyphicon-floppy-disk"></span>
        </button>
      </div>
    </div>

    <div class="ibox-content">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-3">
            <div class="form-group">
              <label class="col-md-7 control-label">InActiveEmployee:</label>
              <div class="col-md-5">
                <input type="checkbox" name="inactiveemployee">  
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="col-md-3 control-label">Search:</label>
              <div class="col-md-9">
                <input type="text" name="search" class="form-control">
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <div class="col-md-12">
                <select type="text" name="" class="form-control"></select>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <div class="col-md-12">
                <select type="text" name="" class="form-control"></select>
              </div>
            </div>
          </div>
        </div>
      </div><br>
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" class="small" href="#home"><span class="fa fa-users"></span> General Info</a></li>
            <li><a data-toggle="tab" class="small" href="#menu1"><span class="fa fa-user"></span> Personnel Info</a></li>
            <li><a data-toggle="tab" class="small" href="#menu2"><span class="fa fa-book"></span> Employee Movement</a></li>
            <li><a data-toggle="tab" class="small" href="#menu3"><span class="fa fa-paper-plane"></span> Leave Monitoring</a></li>
            <li><a data-toggle="tab" class="small" href="#menu4"><span class="fa fa-file-text-o"></span> Memo</a></li>
            <li><a data-toggle="tab" class="small" href="#menu5"><span class="fa fa-calendar"></span> Work Schedule</a></li>
            <li><a data-toggle="tab" class="small" href="#menu6"><span class="fa fa-folder-open-o"></span> Customized Contribution</a></li>
          </ul><br>
       
      <div id="form-employee-201-file" class="form-horizontal">
        <div class="tab-content">
          <div id="home" class="tab-pane fade in active">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">Employee ID:</label>
                    <div class="col-md-7">
                      <input type="text" name="employee_id" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <div>
                      <label class="col-md-5 control-label">Employee Status:</label>
                      <div class="col-md-7">
                        <input type="text" name="employee_status" class="form-control" value="">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Department:</label>
                      <div class="col-md-7">
                        <input type="text" name="department" class="form-control" value="">
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Position:</label>
                      <div class="col-md-7">
                        <input type="text" name="position" class="form-control" value="">
                      </div>
                  </div>
                 </div>
                 <div class="col-md-6">
                  <div class="form-group">
                      <label class="col-md-5 control-label">Barcode ID:</label>
                      <div class="col-md-7">
                        <input type="text" name="barcode_id" class="form-control datepicker" value="">
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">UPC Expiry Date:</label>
                      <div class="col-md-7">
                        <input type="text" name="upc_expiry_date" class="form-control datepicker" value="" placeholder="mm/dd/yyyy">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">Date Hired:</label>
                    <div class="col-md-7">
                      <input type="text" name="date_hired" class="form-control datepicker" value="" placeholder="mm/dd/yyyy">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">Seperation Date:</label>
                    <div class="col-md-7">
                      <input type="text" name="seperation_date" class="form-control datepicker" value="" placeholder="mm/dd/yyyy">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">Payroll Start Date:</label>
                    <div class="col-md-7">
                        <input type="text" name="payroll_start_date" class="form-control datepicker" value="" placeholder="mm/dd/yyyy">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="hr-line-dashed"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">ID:</label>
                    <div class="col-md-7">
                      <select type="text" name="employee_id" class="form-control" value=""></select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">First Name:</label>
                    <div class="col-md-7">
                      <input type="text" name="first_name" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">Middle Name:</label>
                    <div class="col-md-7">
                      <input type="text" name="middle_name" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">Last Name:</label>
                    <div class="col-md-7">
                      <input type="text" name="last_name" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">Nick Name:</label>
                    <div class="col-md-7">
                      <input type="text" name="nick_name" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Birth Date:</label>
                      <div class="col-md-7">
                        <input type="text" name="birth_date" class="form-control datepicker" value="" placeholder="mm/dd/yyyy">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Gender:</label>
                      <div class="col-md-7">
                        <input type="text" name="gender" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Civil Status:</label>
                      <div class="col-md-7">
                        <input type="text" name="civil_status" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Nationality:</label>
                      <div class="col-md-7">
                        <input type="text" name="nationality" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Place of Birth:</label>
                      <div class="col-md-7">
                        <input type="text" name="place_of_birth" class="form-control" value="">
                      </div> 
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <label class="col-md-5 control-label">Phone Number:</label>
                      <div class="col-md-7">
                        <input type="text" name="phone_number" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Mobile Number:</label>
                      <div class="col-md-7">
                        <input type="text" name="mobile_number" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Weight:</label>
                      <div class="col-md-7">
                        <input type="text" name="weight" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Height:</label>
                      <div class="col-md-7">
                        <input type="text" name="height" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Religion:</label>
                      <div class="col-md-7">
                        <input type="text" name="religion" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Email Address:</label>
                      <div class="col-md-7">
                        <input type="text" name="email_address" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Current Address:</label>
                      <div class="col-md-7">
                        <input type="text" name="current_address" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Tel no.:</label>
                      <div class="col-md-7">
                        <input type="text" name="tel_no" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Mobile No.:</label>
                      <div class="col-md-7">
                        <input type="text" name="mobile_number" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Permanent Address</label>
                      <div class="col-md-7">
                        <input type="text" name="permanent_address" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Tel no.:</label>
                      <div class="col-md-7">
                        <input type="text" name="tel_no" class="form-control" value="">
                      </div> 
                  </div>
                </div>
              </div>
            </div>
            <div class="hr-line-dashed"></div>
             <div class="row">
                  <div class="col-md-12">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">Contact person in case of emergency:</label>
                    <div class="col-md-7">
                      <input type="text" name="contact_person" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Telephone No.:</label>
                      <div class="col-md-7">
                        <input type="text" name="telephone_number" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Relationship</label>
                      <div class="col-md-7">
                        <input type="text" name="relationships" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Address</label>
                      <div class="col-md-7">
                        <input type="text" name="address" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Professional Lisence?</label>
                      <div class="col-md-7">
                        <input type="checkbox" name="license" class="" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Prof. License No.:</label>
                      <div class="col-md-7">
                        <input type="text" name="license_number" class="form-control" value="">
                      </div> 
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <label class="col-md-5 control-label">Prof. Description:</label>
                      <div class="col-md-7">
                        <input type="text" name="prof_description" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Work Location:</label>
                      <div class="col-md-7">
                        <input type="text" name="work_location" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Principal:</label>
                      <div class="col-md-7">
                        <input type="text" name="principal" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Base Rate:</label>
                      <div class="col-md-7">
                        <input type="text" name="base_rate" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Base Rate(Enc):</label>
                      <div class="col-md-7">
                        <input type="text" name="base_rate_enc" class="form-control" value="">
                      </div> 
                  </div>
                  <div class="form-group">
                      <label class="col-md-5 control-label">Pay Group</label>
                      <div class="col-md-7">
                        <input type="text" name="pay_group" class="form-control" value="">
                      </div> 
                  </div>
                </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <label><b>ID History</b></label>
                    <table class="table table-responsive table-hover table-striped table-bordered dataTables-example compact">
                      <thead>
                        <th class="text-center col-md-1">Line No</th>
                        <th class="text-center col-md-1">ID Number</th>
                        <th class="text-center col-md-1">Barcode</th>
                        <th class="text-center col-md-1">Biometrix No.</th>
                      </thead>
                    </table>
                  </div>
                </div>
          </div>
          <div id="menu1" class="tab-pane fade">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label">Exemption Code:</label>
                    <div class="col-md-8">
                      <input type="text" name="exemption_code" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">SSS No.:</label>
                    <div class="col-md-8">
                      <input type="text" name="sss_no" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Tin No.:</label>
                    <div class="col-md-8">
                      <input type="text" name="tin_no" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Pagibig No.:</label>
                    <div class="col-md-8">
                      <input type="text" name="pagibig_no" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Philhealth No.:</label>
                    <div class="col-md-8">
                      <input type="text" name="philhealth_no" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">TardyExempt?</label>
                    <div class="col-md-7">
                      <input type="checkbox" name="tardyexempt" class="" value="">
                    </div>    
                  </div> 
                  <div class="form-group">
                    <label class="col-md-4 control-label">Grace Period(Mins):</label>
                    <div class="col-md-8">
                      <input type="text" name="grace_period" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Work Hours(s):</label>
                    <div class="col-md-8">
                      <input type="text" name="work_hours" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Separation Savings Fund.:</label>
                    <div class="col-md-8">
                      <input type="text" name="seperation_savings_fund" class="form-control" value="">
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                      <label class="control-label col-md-3" for="">Check Break?</label>
                      <span class="col-md-4">
                          <input type="checkbox" class="" name="check_break" placeholder="" value="">
                      </span>
                      <span class="control-label" style="font-weight: bold;">
                          Compressed Schedule?
                      </span>
                      <span class="">
                          <input type="checkbox" class="" name="compressed_schedule"  placeholder="" value="">
                      </span>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3" for="">Compute SSS?</label>
                      <span class="col-md-4">
                          <input type="checkbox" class="" name="compute_sss" placeholder="" value="">
                      </span>
                      <span class="control-label" style="font-weight: bold;">
                          Process Time Attendance
                      </span>
                      <span class="">
                          <input type="checkbox" class="" name="process_time_attendance"  placeholder="" value="">
                      </span>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3" for="">Taxable?</label>
                      <span class="col-md-4">
                          <input type="checkbox" class="" name="taxable" placeholder="" value="">
                      </span>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3" for="">Compute Pagibig?</label>
                      <span class="col-md-4">
                          <input type="checkbox" class="" name="compute_pagibig" placeholder="" value="">
                      </span>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3" for="">Compute Philhealth?</label>
                      <span class="col-md-4">
                          <input type="checkbox" class="" name="compute_philhealth" placeholder="" value="">
                      </span>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3" for="">Agency?</label>
                      <span class="col-md-4">
                          <input type="checkbox" class="" name="agency" placeholder="" value="">
                      </span>
                      <span class="control-label" style="font-weight: bold;">
                          Last Pay?
                      </span>
                      <span class="">
                          <input type="checkbox" class="" name="last_pay"  placeholder="" value="">
                      </span>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3" for="">Active?</label>
                      <span class="col-md-4">
                          <input type="checkbox" class="" name="active" placeholder="" value="">
                      </span>
                      <span class="control-label" style="font-weight: bold;">
                          Hold Payroll
                      </span>
                      <span class="">
                          <input type="checkbox" class="" name="hold_payroll"  placeholder="" value="">
                      </span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label">Payroll Mode:</label>
                    <div class="col-md-8">
                      <input type="text" name="payroll_mode" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Bank Code:</label>
                    <div class="col-md-8">
                      <input type="text" name="bank_code" class="form-control" value="">
                      <input type="text" name="bank_code" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Bank Account:</label>
                    <div class="col-md-8">
                      <input type="text" name="bank_account" class="form-control" value="">
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">First Evaluation Schedule:</label>
                    <div class="col-md-8">
                      <input type="text" name="first_evaluation_schedule" class="form-control datepicker" value="" placeholder="mm/dd/yyyy" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Second Evaluation Schedule:</label>
                    <div class="col-md-8">
                      <input type="text" name="second_evaluation_schedule" class="form-control datepicker" value="" placeholder="mm/dd/yyyy" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Third Evaluation Schedule</label>
                    <div class="col-md-8">
                      <input type="text" name="third_evaluation_schedule" class="form-control datepicker" value="" placeholder="mm/dd/yyyy" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Clearance Date:</label>
                    <div class="col-md-8">
                      <input type="text" name="clearance_date" class="form-control datepicker" value=""  placeholder="mm/dd/yyyy">
                    </div>
                  </div>
                  <label class="control-label">Allowance</label><br><br>
                  <table class="table table-striped table-bordered table-hover dataTables-example compact" id="cost-profit-center-list">
                    <thead>
                      <tr>
                        <th class="text-center col-xs-1">Allowance</th>
                        <th class="text-center col-xs-2">Amount</th>
                        <th class="text-center col-xs-2">Taxable</th>
                        <th class="text-center col-xs-2">Type</th>
                      </tr>
                    </thead>
                    <tfoot style="display: none"></tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div id="menu2" class="tab-pane fade">
            <table class="table table-striped table-bordered table-hover dataTables-example compact">
              <thead>
                <tr>
                  <th class="text-center col-xs-1">Applied Date</th>
                  <th class="text-center col-xs-1">Principal</th>
                  <th class="text-center col-xs-1">Job Title</th>
                  <th class="text-center col-xs-1">Department</th>
                  <th class="text-center col-xs-1">Basic Salary</th>
                  <th class="text-center col-xs-1">Category</th>
                  <th class="text-center col-xs-1">Leave Credits</th>
                  <th class="text-center col-xs-1">Cash Bond</th>
                </tr>
              </thead>
            </table>
          </div>
          <div id="menu3" class="tab-pane fade">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Leave Type</label>
                    <div class="col-md-8">
                      <select type="text" name="leave_type" class="form-control" value=""></select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <table class="table table-striped table-bordered table-hover dataTables-example compact">
                  <thead>
                    <tr>
                      <th class="text-center col-xs-1">Leave Type</th>
                      <th class="text-center col-xs-1">Days(s)</th>
                      <th class="text-center col-xs-1">Applied Date</th>
                      <th class="text-center col-xs-1">Date(From)</th>
                      <th class="text-center col-xs-1">Date(To)</th>
                      <th class="text-center col-xs-1">With Approval?</th>
                      <th class="text-center col-xs-1">Remarks</th>
                    </tr>
                  </thead>
                  
                </table>
              </div>
            </div>
          </div>
          <div id="menu4" class="tab-pane fade">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                      <span class="col-md-8">
                          <select type="text" class="form-control datepicker" name=""  value=""></select>
                      </span>
                      <span class="col-md-4">
                          <input type="text" class="form-control " name=""  value="" readonly>
                      </span>
                  </div>
                </div>
                <table class="table table-striped table-bordered table-hover dataTables-example compact">
                    <thead>
                      <tr>
                        <th class="text-center col-xs-1">Doc. No.</th>
                        <th class="text-center col-xs-1">Doc. Date</th>
                        <th class="text-center col-xs-1">Applied Date</th>
                        <th class="text-center col-xs-1">Status</th>
                        <th class="text-center col-xs-1">Created By</th>
                        <th class="text-center col-xs-1">Date Created</th>
                      </tr>
                    </thead>
                  </table>
              </div>
            </div>
          </div>
          <div id="menu5" class="tab-pane fade">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="col-md-4 control-label">Applied Date:</label>
                    <div class="col-md-8">
                      <input type="text" name="applied_date" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="col-md-4 control-label">To:</label>
                    <div class="col-md-8">
                      <input type="text" name="to" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <div class="col-md-1">
                      <input type="checkbox" name="filter_date" class="">
                    </div>
                     <label class="control-label">Filter Date</label>
                  </div>
                </div>
                <table class="table table-striped table-bordered table-hover dataTables-example compact">
                <thead>
                  <tr>
                    <th class="text-center col-xs-1">Applied Date</th>
                    <th class="text-center col-xs-1">WeekDay</th>
                    <th class="text-center col-xs-1">Shift Code</th>
                    <th class="text-center col-xs-1">Shift Description</th>
                    <th class="text-center col-xs-1">Start Time</th>
                    <th class="text-center col-xs-1">End Time</th>
                  </tr>
                </thead>  
              </table>
              </div>
            </div>
          </div>
          <div id="menu6" class="tab-pane fade">
            <div class="row">
              <div class="col-md-12">
                <h4 class="text-info">Government Fixed Contribution</h4>
                <div class="col-md-6">
                  <h5><b>SSS</b></h5>
                  <div class="form-group">
                    <label class="col-md-5 control-label">SSS EE Amount:</label>
                    <div class="col-md-7">
                      <input type="text" name="sss_ee_amount" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">SSS ER Amount:</label>
                    <div class="col-md-7">
                      <input type="text" name="sss_er_amount" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">Fixed EC Amount:</label>
                    <div class="col-md-7">
                      <input type="text" name="fixed_ec_amount" class="form-control" value="">
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <h5><b>Philhealth</b></h5>
                  <div class="form-group">
                    <label class="col-md-5 control-label">PhilHealth EE Amount:</label>
                    <div class="col-md-7">
                      <input type="text" name="philhealth_ee_amount" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">PhilHealth ER Amount:</label>
                    <div class="col-md-7">
                      <input type="text" name="philhealth_er_amount" class="form-control" value="">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <h5><b>Pag-Ibig</b></h5>
                  <div class="form-group">
                    <label class="col-md-5 control-label">Pag-Ibig EE Amount:</label>
                    <div class="col-md-7">
                      <input type="text" name="pagibig_ee_amount" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">Pag-Ibig ER Amount:</label>
                    <div class="col-md-7">
                      <input type="text" name="pagibig_er_amount" class="form-control">
                    </div>
                  </div>
                  <h5><b>Tax</b></h5>
                  <div class="form-group">
                    <label class="col-md-5 control-label">Tax Contribution Basis Amount:</label>
                    <div class="col-md-7">
                      <input type="text" name="tax_contribution_basis_amount" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label">13th Month Basis?</label>
                    <div class="col-md-7">
                      <input type="checkbox" name="13th_month_basis" value="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<a href="#" class="btnTop btn btn-primary" role="button" data-toggle="tooltip" data-placement="top" title="Go to Top"><i class="fa fa-arrow-up"></i></a>
  
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
  <script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
  <script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
  <script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
  <script src="<?php echo EXTENSION ?>moment.js"></script>

  <!-- GLOBAL VARIABLE -->
  <script>
    var output_type       = '<?= $type ?>';   
    var doc_no          = '<?= $header['S_Id'] ?>';
    var cUserid         = '<?= getCurrentUser()['login-user'] ?>';
    var cDefaultLoc       = '<?= getDefaultLocation() ?>';
    var header_table      = '<?= $header_table ?>';
    var header_doc_no_field   = '<?= $header_doc_no_field ?>';
    var header_status_field   = '<?= $header_status_field ?>';
    var number_series       = '<?= $number_series ?>';
    var module_url        = '<?= DOMAIN.'application/application_setup/employee_201_file' ?>';

    
  </script>

  <script src="<?php echo JS_DIR?>app/application/application_setup/employee_201_file/form.js"></script>
  <script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
</section>
