<div class="modal inmodal animated fadeInUp" id="detail-template-work-schedule" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Work Schedule Setup</h4>
				</div>
				<div class="modal-body">
					<form id='form-work-schedule'>
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" name="mode">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="text-danger">*</span> 
											<label>Doc Date:</label>
										</div>
										<div class="col-7">
											
											<input type="text" class="form-control p-xxs" id="WS_DocDate" name="WS_DocDate" readonly>
										</div>
									</div>                               
					        	</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="text-danger">*</span>
										  <label>Week Day:</label>
										</div>
										<div class="col-7">
											<select name="WS_WeekDay" id="WS_WeekDay" class="form-control weekday-js-select2" required style="width: 100%">
												<option></option>
												<option value="1">Sunday</option>
												<option value="2">Monday</option>
												<option value="3">Tuesday</option>
												<option value="4">Wednesday</option>
												<option value="5">Thursday</option>
												<option value="6">Friday</option>
												<option value="7">Saturday</option>
											</select>
											<div class="required-label text-danger" style="font-style: italic; font-size: 9px; display: none;">*Required Field</div>
										</div>                               
					        	</div>
					        </div>
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="text-danger">*</span> 
											<label>Shift ID:</label>
										</div>
										<div class="col-7">
											<select required style="width:100%" name="WS_FK_Shift_id" class="form-control shift-js-select2 p-xxs" tabindex="2">
												<option value=""></option>
												<?php foreach ($shift_list as $row): ?>
													<option value="<?= $row['S_ID'] ?>">
														<?= ucfirst($row['S_ID']) ?>
													</option>
												<?php endforeach; ?>
											</select>
											<div class="required-label text-danger" style="font-style: italic; font-size: 9px; display: none;">*Required Field</div>
										</div>
									</div>                               
					        	</div>  
					        	<div class="form-group">
									<div class="row">
										<div class="col-md-4 text-right"><span class="text-danger">*</span> 
											<label>Description:</label>
										</div>
										<div class="col-7">
											<input type="text" id="WS_Description" class="form-control p-xxs" name="WS_Description" value="">
											<input type="hidden" class="form-control p-xxs" name="WS_Emp_id" readonly>
											<input type="hidden" class="form-control p-xxs" name="WS_LineNo" readonly>
											<div class="required-label text-danger" style="font-style: italic; font-size: 9px; display: none;">*Required Field</div>
										</div>
									</div>                               
					        	</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<!-- <button type="button" class="btn btn-warning btn-outline close-modal" data-toggle="tooltip" data-placement="bottom" title="Back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
                    <button class="btn btn-primary btn-outline" id="btnSaveID">
                    	<i class="glyphicon glyphicon-floppy-disk"></i><strong> Save</strong>
                    </button>   -->
                     <button type="button" class="btn btn-warning btn-outline close-modal" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" data-dismiss="modal"><span class="fa fa-reply"></span></button>
					<button type="button" id="btnWorkScheduleSaveandNew" class="btn btn-primary btn-outline" value="SaveandNew"><!-- <span class="glyphicon glyphicon-floppy-disk"> --></span> Save and New</button>
					<button type="button" id="btnWorkScheduleSaveandClose" class="btn btn-primary btn-outline" value="SaveandClose"><span class="glyphicon glyphicon-floppy-disk"></span> Save and Close</button>
				</div>
			</div>
		</div>
	</div>