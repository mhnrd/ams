<style>
tfoot {
    display: table-header-group;
}
.selected {
    color: red;
}
table{
    cursor: pointer;
}
table tbody tr.selected{
    border: 2px solid #19aa8d;
    background-color: #0E76C7;
    color: #F7F8F8;
}
.select2-results__option[aria-selected=true] {
    display: none;
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<!-- Clockpicker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo EXTENSION ?>moment.js"></script>

<section class="wrapper wrapper-content">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <label><h5>Work Schedule Setup</h5></label>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-lg-4">
                    <!-- <div class="col-md-12 text-right">
                        <button class="btn btn-success" id="btnActive" disabled>
                            <i class="fa fa-check"></i> Activate
                        </button>
                        <button class="btn btn-danger" id="btnDeactive" disabled>
                            <i class="fa fa-times"></i> Deactivate
                        </button>
                    </div> -->
                    <!-- <div class="col-md-12">
                        <div class="row form-group">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-5"><label>Company:</label></div>
                                    <div class="col-7">
                                        <select required style="width:100%" id="company" name="filteredBy" class="form-control p-xxs" tabindex="3">    
                                        <?php foreach($company_list as $company): ?>  
                                        <option></option>                                   
                                            <option value="<?= $company['COM_Id'] ?>" <?= ($company['COM_Id']) ? 'selected' : '' ?>><?= $company['COM_Name'] ?></option>
                                        <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                               
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-5"><label>Pay Group:</label></div>
                                    <div class="col-md-7">
                                        <select required style="width:100%" id="paygroup" name="searchedBy" class="form-control p-xxs" tabindex="3">
                                            <?php foreach ($paygroup_list as $paygroup): ?>
                                                <option></option> 
                                                <option value="<?= $paygroup['G_ID'] ?>" <?= ($paygroup['G_ID']) ? 'select' : '' ?>><?= $paygroup['G_ID'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                               
                        </div>
                    </div> -->
                     <div class="col-md-12">
                        <div class="row form-group">
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label>Filtered by:</label>
                                    </div>
                                    <div class="col-7">
                                        <select required style="width:100%" name="filteredBy" class="form-control p-xxs" tabindex="3">
                                            <option value=""></option>
                                            <option value="Department">Department</option>
                                            <option value="Position">Position</option>
                                            <option value="Location">Location</option>
                                            <option value="Principal">Principal</option>
                                            <option value="PayGroup">Pay Group</option>
                                        </select>
                                    </div>
                                </div>
                            </div>                               
                            <div class="col-md-5">
                                <div class="row">
                                    <div class="col-md-12">
                                        <select required style="width:100%" name="searchedBy" class="form-control p-xxs" tabindex="3">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>                               
                        </div>
                    </div>
                    <?= generate_table_standard($table_hdr)?>
                </div>
                <div class="col-lg-8"> 
                <div class="pull-right">
                    <form method="post" action="<?= DOMAIN.'administration/application_setup/time_keeping/work_schedule_setup/action_work' ?>">
                        <button  type="submit" name="export" class="btn btn-primary">Export Document</button>
                        <!-- onclick="return confirm('Are Your Sure You Want To Export Document?')" -->
                    </form> 
                </div>
                    <?= generate_table_standard($table_dtl)?>
                </div>
            </div>
        </div>
    </div>
     <?php $this->load->view('/administration/application_setup/time_keeping/work_schedule_setup/detail-template') ?>
    
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo EXTENSION ?>moment.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <!-- Page-Level Scripts -->
    <script type="text/javascript">
        var tableindex;
        var tbl_IdHistory;
        var col_center              = <?= json_encode($hdr_center)?>;
        var table_id                = '<?= key($table_hdr) ?>';
        var process_action          = 'Work Schedule Setup';
        var table_name              = 'tblWorkSchedule';
        var controller_url          = 'administration/application_setup/time_keeping/work_schedule_setup/';
        
        
    </script>
    <script src="<?php echo JS_DIR?>app/administration/application_setup/time_keeping/work_schedule_setup/index.js"></script>
    <script src="<?php echo JS_DIR?>app/administration/application_setup/time_keeping/work_schedule_setup/index_functions.js"></script>
    <script src="<?php echo JS_DIR ?>app/rules/index_functions.js"></script>
</section>
