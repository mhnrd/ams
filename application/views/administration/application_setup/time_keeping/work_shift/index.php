<style>
tfoot {
	display: table-header-group;
}
.dataTables-example tbody tr td a.delete-button:hover{
	color: white!important;
}
.advance-search:hover, .advance-search-small:hover{
	color: white!important;
}
@media screen and (max-width: 425px){
	#advance-search-small{
		display: inline!important;
	}
	.table-tools{
		margin-right: 5px!important;
		margin-top: 5px!important;
	}
	#advance-search{
		display: none;
	}
	#work_shift_details_filter{
		margin-left: -50px!important;
	}
	#clear-search-small{
		margin-top: 4px!important;
	}
	#work_shift_details_filter{
		margin-right: 0px!important;
	}
}

@media screen and (max-width: 768px){
	#work_shift_details_filter input{
		width: 60%!important;
	}
	#work_shift_details_filter{
		margin-right: 500px!important;
	}
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<!-- Clockpicker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<label><h5>Shift</h5></label>
			<div class="pull-right ibox-tools">
				<button class="btn btn-success" id="btnActive" disabled>
					<i class="fa fa-check"></i> Activate
				</button>
				<button class="btn btn-danger" id="btnDeactive" disabled>
					<i class="fa fa-times"></i> Deactivate
				</button>
			</div>
		</div>
		<div class="ibox-content">
			<?= generate_table($table_hdr)?>
		</div>
	</div>



<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<!-- Sweet alert -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Moment -->
<script src="<?php echo EXTENSION ?>moment.js"></script>
<!-- Date Range Picker -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/clockpicker/clockpicker.js"></script>
<!-- Page-Level Scripts -->
<script type="text/javascript">
	var tableindex;
	var col_center		= <?= json_encode($dtl_center) ?>;
	var process_action 	= 'Shift';
	var table_name 		= 'tblShift';
	var controller_url 	= 'administration/application_setup/time_keeping/work_shift/';
	var table_id 		= '<?= key($table_hdr) ?>';
</script>
<script src="<?php echo JS_DIR ?>app/administration/application_setup/time_keeping/work_shift/index.js"></script>
<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
<script src="<?php echo JS_DIR ?>app/rules/advsearch.js"></script>
<script src="<?php echo JS_DIR ?>app/rules/index_functions.js"></script>

</section>