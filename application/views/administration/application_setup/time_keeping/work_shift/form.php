<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<link href="<?php echo EXTENSION; ?>timepicker/jquery.timepicker.min.css" rel="stylesheet"> 

<section class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
		<label><h5>Shift</h5></label>

		</div>
		<div class="ibox-content">
			<!-- Location -->
				<div id="form-work-shift" class="form-horizontal">
					<form id="form-header">
						<div class="row">
							<div class="col-12">
								<input type="hidden" name="S_Active" value="<?= $header['S_Active'] ?>">
								<div class="row">
									<div class="col-md-5 col-12">
										<div class="form-group">
											<div class="row">
												<label class="control-label col-3 text-right">Shift ID:</label>
												<div class="col-9">
													<?php if($type == 'update') ?>
													<input type="text" class="form-control" required name="S_ID" value="<?= $header['S_ID']?>"  tabindex="1">
													<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="control-label col-3 text-right">Shift Description:</label>
												<div class="col-9">
													<input type="text" class="form-control" required name="S_Name" value="<?= $header['S_Name'] ?>" tabindex="2">
													<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-5 col-12">
										<div class="form-group">
											<div class="row">
												<label class="control-label col-3 text-right">Time In:</label>
												<div class="col-9">
													<div class="input-group">
														<input type="text" class="form-control clockpicker-in required-field" name="S_StartTime" required readonly value="<?= $header['S_StartTime']?>" style="background: #FFFFFF" tabindex="3">
														<span class="input-group-addon">
													        <span class="fa fa-clock-o"></span>
													    </span>
													</div>
						                    		<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="control-label col-3 text-right">Time Out:</label>
												<div class="col-9">
													<div class="input-group">
														<input type="text" class="form-control clockpicker-out required-field" name="S_EndTime" required readonly value="<?= $header['S_EndTime']?>" style="background: #FFFFFF"  tabindex="4">
														<span class="input-group-addon">
													        <span class="fa fa-clock-o"></span>
													    </span>
													</div>
						                    		<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr>
								<div class="row">
									<div class="col-md-4 col-12">
										<div style="font-weight: bold; color: black!important">Break 1</div>
										<hr>
										<div class="form-group">
											<div class="row">
												<label class="control-label col-3 text-right">Break Out:</label>
												<div class="col-9">
													<div class="input-group">
														<input type="text" class="form-control clockpicker-break required-field" data-mask="99:99aa" name="S_Break1Out" required value="<?= $header['S_Break1Out']?>" style="background: #FFFFFF" tabindex="5">
														<span class="input-group-addon">
													        <span class="fa fa-clock-o"></span>
													    </span>
													</div>
						                    		<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="control-label col-3 text-right">Break In:</label>
												<div class="col-9">
													<div class="input-group">
														<input type="text" class="form-control clockpicker-break required-field" data-mask="99:99aa" name="S_Break1In" required value="<?= $header['S_Break1In']?>" style="background: #FFFFFF" tabindex="6">
														<span class="input-group-addon">
													        <span class="fa fa-clock-o"></span>
													    </span>
													</div>
						                    		<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-4 col-12">
										<div style="font-weight: bold; color: black!important">Break 2</div>
										<hr>
										<div class="form-group">
											<div class="row">
												<label class="control-label col-3 text-right">Break Out:</label>
												<div class="col-9">
													<div class="input-group">
														<input type="text" class="form-control clockpicker-break required-field" data-mask="99:99aa" name="S_Break2Out" value="<?= $header['S_Break2Out']?>" style="background: #FFFFFF" tabindex="7">
														<span class="input-group-addon">
													        <span class="fa fa-clock-o"></span>
													    </span>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="control-label col-3 text-right">Break In:</label>
												<div class="col-9">
													<div class="input-group">
														<input type="text" class="form-control clockpicker-break required-field" data-mask="99:99aa" name="S_Break2In" value="<?= $header['S_Break2In']?>" style="background: #FFFFFF" tabindex="8">
														<span class="input-group-addon">
													        <span class="fa fa-clock-o"></span>
													    </span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-4 col-12">
										<div style="font-weight: bold; color: black!important">Break 3</div>
										<hr>
										<div class="form-group">
											<div class="row">
												<label class="control-label col-3 text-right">Break Out:</label>
												<div class="col-9">
													<div class="input-group">
														<input type="text" class="form-control clockpicker-break required-field" data-mask="99:99aa" name="S_Break3Out" value="<?= $header['S_Break3Out']?>" style="background: #FFFFFF" tabindex="9">
														<span class="input-group-addon">
													        <span class="fa fa-clock-o"></span>
													    </span>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="control-label col-3 text-right">Break In:</label>
												<div class="col-9">
													<div class="input-group">
														<input type="text" class="form-control clockpicker-break required-field" data-mask="99:99aa" name="S_Break3In" value="<?= $header['S_Break3In']?>" style="background: #FFFFFF" tabindex="10">
														<span class="input-group-addon">
													        <span class="fa fa-clock-o"></span>
													    </span>
													</div>
												</div>
											</div>
										</div>
									</div>
									
								</div>

							</div>

						</div>

					</form>

				</div>
				<hr>
				<div>
					<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="11">
						<span class="fa fa-reply"></span>					
					</button>
					<?php if($type != 'view'): ?>
						<?php if ($type == 'add'): ?>
							<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['S_ID'] ?>" data-action="save-new" tabindex="12">
								<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
							</button>
						<?php endif ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['S_ID'] ?>" data-action="save-close" tabindex="13">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
						</button>
					<?php endif; ?>
				</div>
			</div>

		</div>

	</div> 


	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/clockpicker/clockpicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="<?php echo EXTENSION; ?>timepicker/jquery.timepicker.min.js"></script> 

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['S_ID'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['S_ID']) ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/application_setup/time_keeping/work_shift' ?>';
		var module_folder 			= 'administration/application_setup/time_keeping';
		var module_controller 		= 'Work_shift';
		var module_name 			= 'Shift';
		var isRequiredDetails 		= false;
	</script>

	<script src="<?php echo JS_DIR?>app/administration/application_setup/time_keeping/work_shift/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>	

	</section>