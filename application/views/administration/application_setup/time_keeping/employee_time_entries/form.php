<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 25px;
}

.form-group{
	margin-bottom: 5px!important;
	font-size: 11px;
}
.form-control{
	font-size: 11px;
	text-transform: capitalize;
}
h4,  h5{
	color: black!important;
}
</style>
<section class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<h5>Employee Time Entries</h5>
			<div class="pull-right ibox-tools">
			
			</div>
		</div>
		<div class="ibox-content">
			<!-- Location -->
			<div id="form-holiday"  class="form-horizontal">
				<form id="form-header" autocomplete="off">
					<div class="row">
						<div class="col-12">
							<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">
										<label class="control-label col-3" style="text-align: right"> 
											<span style="color: red">*</span> Employee ID</label>
										<div class="col-9">
											<input type="text" class="form-control required-field" required name="Emp_Id" value="<?= $header['Emp_Id']?>">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<label class="control-label col-3" style="text-align: right">
											<span style="color: red">*</span> Employee Name:</label>
										<div class="col-9">
											<input type="text" class="form-control required-field" required name="Emp_FirstName" value="<?= $header['Emp_FirstName']?>">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>		
				</form>
			</div>
	
		<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['Emp_Id'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['Emp_Id'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
	</div>
</div> 


	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['Emp_Id'] ?>';
		var update_link				= '<?= './update?id='.md5($header['Emp_Id'])?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/application_setup/time_keeping/employee_time_entries' ?>';
		var module_folder 			= 'administration/application_setup/time_keeping';
		var module_controller 		= 'Employee_time_entries';
		var module_name 			= 'Employee Time Entries';
		var isRequiredDetails 		= false;
	</script>

	<script src="<?php echo JS_DIR?>app/administration/application_setup/time_keeping/employee_time_entries/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>	

</section>