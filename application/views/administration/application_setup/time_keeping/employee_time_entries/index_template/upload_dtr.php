<style>
	#detail-template-upload-dtr .row .col-md-4{
		margin-top: 7px;
	}
	h4{
	color: white!important;
	}
	.form-group{
	margin-bottom: 5px!important;
	font-size: 11px;
	}
</style>
<div class="modal inmodal animated fadeInUp" id="detail-template-upload-dtr" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog  modal-dialog-centered modal-lg" style="width:40%">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					 <i class="fa fa-upload"></i><strong> Upload DTR</strong>
				</div>
				<div class="modal-body">
					<form method="post" id="import_form" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-12">
								<div class="custom-file">
								    <input id="file" type="file" name="file" class="custom-file-input" required accept=".xls, .xlsx">
								    <label for="file" class="custom-file-label">Choose file...</label>
								</div> 
							</div>
						</div>	
					</div>
					<div class="modal-footer">
	                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" name="import" class="btn btn-success" id="btnGenerateDTR">
	                    	<i class=""></i><strong> Generate</strong>
	                    </button>
	                    
					</div>
				</form>
			</div>
		</div>
	</div>