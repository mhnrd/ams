<style>
	#detail-template-gtk .row .col-md-4{
		margin-top: 7px;
	}
	h4{
	color: white!important;
	}
	.form-group{
	margin-bottom: 5px!important;
	font-size: 11px;
	}
</style>
<div class="modal inmodal animated fadeInUp" id="detail-template-gtk" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog  modal-dialog-centered modal-lg" style="width:40%">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					 <i class="fa fa-folder-open-o"></i><strong> Generate Time Keeping</strong>
				</div>
				<div class="modal-body">
					<form id="form-generate-client-time-entries">
						<div class="row">
							<div class="col-md-6 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-md-3" style="color:black; text-align: right">
										Pay Group:</div>
										<div class="col-9">
											<select name="" value="" class="js-select2 form-control required-field" style="width: 100%;">
												<option></option>
											</select>
										</div>
									</div>                               
					        	</div> 
					        </div>
					    </div>
					    <div class="row">
					    	<div class="col-md-6">
					    		<div class="form-group">
									<div class="row">
										<label class="control-label col-3" style="padding-top: 5px; color:black; text-align: right">From:</label>
										<div class="datepicker input-group date col-9">
											<input type="text" readonly class="form-control required-field" required name="" value="" style="background: #ffff; text-align: left!important">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
					    	</div>
					    	<div class="col-md-6">
					    		<div class="form-group">
									<div class="row">
										<label class="control-label col-3" style="padding-top: 5px; color:black; text-align: right">To:</label>
										<div class="datepicker input-group date col-9">
											<input type="text" readonly class="form-control required-field" required name="" value="" style="background: #ffff; text-align: left!important">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
					    	</div>
					    </div>
					    <div class="row">
					    	<div class="col-md-6">
					    		<div class="form-group">
									<div class="row">
										<label class="control-label col-3" style="height:20px;color:black; text-align: right">Pay Period:</label>
										<div class="datepicker input-group date col-9">
											<input type="text" readonly class="form-control required-field" required name="" value="" style="background: #ffff; text-align: left!important">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
					    	</div>
					    </div>
					</form>
				</div>
				<div class="modal-footer">
                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button class="btn btn-success" id="btnGenerateTime">
                    	<i class=""></i><strong> Generate</strong>
                    </button>
				</div>
			</div>
		</div>
	</div>