<style>
	#detail-template-print .row .col-md-4{
		margin-top: 7px;
	}
	h4{
	color: white!important;
	}
	.form-group{
	margin-bottom: 5px!important;
	font-size: 11px;
	}
	
	#checkbox {
	  width: 13px;
	  height: 13px;
	  padding: 0;
	  margin:0;
	  vertical-align: bottom;
	  position: relative;
	  top: -1px;
	  *overflow: hidden;
	}
	.radio {
	  width: 13px;
	  height: 13px;
	  padding: 0;
	  margin:0;
	  vertical-align: bottom;
	  position: relative;
	  top: -1px;
	  *overflow: hidden;
	}
</style>
<div class="modal inmodal animated fadeInUp" id="detail-template-print" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog  modal-dialog-centered modal-md">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<strong> Timekeeping Report</strong>
				</div>
				<div class="modal-body">
					<form id="form-print">
						<div class="row">
							<div class="col-md-6 col-12">
								<div>
								   <label><input  id="checkbox" type="checkbox" checked /> All Employee</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-md-3" style="text-align:right">
											<label class="control-label">Employee:</label>
										</div>
										<div class="col-9">
											<select id="employee" class="js-select2 form-control" readonly style="width:100%">
												<?php foreach($employee_list as $employee){?>
													<option></option>
													<option value="<?= $employee['Emp_Id']?>"><?= $employee['Emp_Name']?></option>	
												<?php }?>	
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-3" style="text-align:right">
											<label class="control-label">Principal:</label>
										</div>
										<div class="col-9">
											<select id="principal" class="js-select2 form-control" style="width:100%">
												<?php foreach($principal_list as $principal){?>
													<option></option>
													<option value="<?= $principal['C_DocNo']?>"><?= $principal['C_Name']?></option>
												<?php }?>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed"></div>
						<h5>Report Type</h5>
						<div class="row">
							<div class="col-md-5 col-12">
								<div><label> <input class="radio" type="radio" checked name=""> Per Employee</label></div>
							</div>
							<div class="col-md-5 col-12">
								<div><label><input class="radio" type="radio" name=""> Per Principal</label></div>
							</div>
						</div>
						<div class="hr-line-dashed"></div>
						<h5>Date Range</h5>
						<div class="row">
							<div class="col-md-6 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-md-3">
										<label class="control-label">Date From:</label></div>
										<div class="col-9">
											<div class="datepicker input-group date">
												<input type="text" readonly class="form-control" value="" style="background: #ffff; text-align: left!important">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>	
									</div>
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-md-3">
										<label class="control-label">Date From:</label></div>
										<div class="col-9">
											<div class="datepicker input-group date">
												<input type="text" readonly class="form-control" value="" style="background: #ffff; text-align: left!important">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>	
									</div>
								</div>
							</div>
						</div> 
						<div class="hr-line-dashed"></div>
						<div class="row">
							<div class="col-md-5 col-12">
								<div><label> <input class="radio" type="radio" checked name="optionsRadios"> Time Attendance</label></div>
							</div>
							<div class="col-md-5 col-12">
								<div><label><input class="radio" type="radio" name=""> Undertime</label></div>
							</div>
						</div>  
						<div class="row">
							<div class="col-md-5 col-12">
								<div><label> <input class="radio" type="radio" name="optionsRadios"> Incomplete In/Out</label></div>
							</div>
							<div class="col-md-5 col-12">
								<div><label><input class="radio" type="radio" name=""> Absence</label></div>
							</div>
						</div> 
					</form>
				</div>
				<div class="modal-footer">
                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button class="btn btn-success" id="btnGenerateTime">
                    	<i class=""></i><strong> Ok</strong>
                    </button>
				</div>
			</div>
		</div>
	</div>