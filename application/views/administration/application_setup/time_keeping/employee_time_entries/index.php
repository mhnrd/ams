<style>
tfoot {
	display: table-header-group;
}
h5{
	color: black!important;
}
tr.row_selected td
{
	border: 2px solid #19aa8d;
	background-color: #0E76C7 !important;

}
/*.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 25px;
}*/
/*tr.row_selected:hover td{
	border: 2px solid #19aa8d;
	background-color: #0E76C7;
	
}*/
.dataTables-example tbody tr td a.delete-button:hover{
	color: white!important;
}
.advance-search:hover, .advance-search-small:hover{
	color: white!important;
}
@media screen and (max-width: 425px){
	#advance-search-small{
		display: inline!important;
	}
	.table-tools{
		margin-right: 5px!important;
		margin-top: 5px!important;
	}
	#advance-search{
		display: none;
	}
	#employee_time_entries_filter{
		margin-left: -50px!important;
	}
	#clear-search-small{
		margin-top: 4px!important;
	}
	#employee_time_entries_filter{
		margin-right: 0px!important;
	}
}

@media screen and (max-width: 768px){
	#employee_time_entries_filter input{
		width: 60%!important;
	}
	#employee_time_entries_filter{
		margin-right: 500px!important;
	}
}
</style>

<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Employee Time Entries</h5>
			<div class="pull-right ibox-tools">
				<button class="btn btn-primary btn-outline" id="btnPrint" data-toggle="tooltip" data-placement="top" title="Print">
                    <i class="fa fa-print"></i><strong></strong>
                </button>
                <button class="btn btn-warning btn-outline" id="btnUploadtDTR" data-toggle="tooltip" data-placement="top" title="Upload DTR">
                    <i class="fa fa-upload"></i><strong> DTR</strong>
                </button>
                <button class="btn btn-success btn-outline" id="btnProcessAttendance" data-toggle="tooltip" data-placement="top" title="Process Attendance">
                    <i class="fa fa-clock-o"></i><strong></strong>
                </button>
                <button class="btn btn-warning btn-outline" id="btnUploadPayroll" data-toggle="tooltip" data-placement="top" title="Upload Payroll Period">
                    <i class="fa fa-upload"></i><strong> Payroll</strong>
                </button>
                <button class="btn btn-success" id="btnGCT" data-toggle="tooltip" data-placement="top" title="Generate Client Time Keeping">
                    <i class="fa fa-folder-open-o"></i><strong></strong>
                </button>

			</div>
		</div>
		<div class="ibox-content">
			<div class="row">
				<div class="col-sm-4">
					<div id="form-delivery" class="form-horizontal">
						<form id="form-header">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<div class="row">
											<label class="control-label col-md-4" style="text-align: right">Commpany:</label>
											<div class="col-md-8">
												<select name="filteredBy" id="company" value="" class="js-select2 form-control required-field" style="width: 100%;">
													<option value=""></option>
		                                            <option value="Department">Department</option>
		                                            <option value="Position">Position</option>
		                                            <option value="Location">Location</option>
		                                            <option value="Principal">Principal</option>
		                                            <option value="PayGroup">Pay Group</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<label class="control-label col-md-4" style="text-align: right">Pay Group</label>
											<div class="col-md-8">
												<select name="searchedBy" id="pay_group" value="" class="js-select2 form-control required-field" style="width: 100%;">
													<option></option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<?= generate_table($table_hdr)?>
				</div>
        		<div class="col-lg-8">
				<!-- FOR VIEWING PER EMPLOYEE -->

					<div class="tabs-container">
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a class="nav-link active" data-toggle="tab" href="#tab-1">Time Entries</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#tab-2">Time Log</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                	<h3 style="color:black; font-size: 14px">Time Entries</h3>
	               					<?= generate_table_standard($table_detail) ?>
									<?php $this->load->view('administration/application_setup/time_keeping/employee_time_entries/detail_template'); ?>
                                </div>
                            </div>
                            <div role="tabpanel" id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                 	 <?= generate_table_standard($table_time_log) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
				<br>
				<h5 style="color:black; font-size: 14px">Processed DTR</h5>
				   <?= generate_table_standard($table_process_time_attendance) ?> 	
				</div>
			</div>

		</div>             
</div>

	<!-- LOAD MODAL -->

	<?php $this->load->view('administration/application_setup/time_keeping/employee_time_entries/index_template/generate_time'); ?>
	<?php $this->load->view('administration/application_setup/time_keeping/employee_time_entries/index_template/upload_dtr'); ?>
	<?php $this->load->view('administration/application_setup/time_keeping/employee_time_entries/index_template/print'); ?>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/clockpicker/clockpicker.js"></script>
	<!-- Page-Level Scripts -->
	<script type="text/javascript">
		var tableindex;
		var tbl_time_entries;
		var tbl_time_log;
		var table_id 		= '<?= key($table_hdr) ?>';
		var col_center		= <?= json_encode($hdr_center)?>;
		var process_action 	= 'Employee Time Entries';
		var table_name 		= 'tblEmployee';
		var controller_url 	= 'administration/application_setup/time_keeping/employee_time_entries/';

	</script>
	<script src="<?php echo JS_DIR?>app/administration/application_setup/time_keeping/employee_time_entries/index.js"></script>
	<script src="<?php echo JS_DIR ?>app/administration/application_setup/time_keeping/employee_time_entries/index_functions.js"></script>
	<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
	<script src="<?php echo JS_DIR ?>app/rules/index_functions.js"></script>
</section>