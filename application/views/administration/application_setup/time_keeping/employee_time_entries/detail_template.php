<style>
	#detail-template-ete .row .col-md-4{
		margin-top: 7px;
	}
	h4{
	color: white!important;
	}
	.form-group{
	margin-bottom: 5px!important;
	font-size: 11px;
	}
</style>
<style type="text/css">	.clockpicker-popover { z-index: 999999 !important;} div.form-group{margin-bottom: 5px;}</style>
<div class="modal inmodal animated fadeInUp" id="detail-template-ete" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog  modal-dialog-centered modal-lg" style="width:100%">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #1AB394; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>&nbsp; Employee Time Entries Setup</h4>
				</div>
				<div class="modal-body">
					<form id="form-employee-time-entries">
						<input type="hidden" name="mode">
						<div class="row">
							<div class="col-md-6 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4" style="text-align: right">
										<label class="control-label">Employee ID:</label></div>
										<div class="col-8">
											<input type="text" class="form-control p-xxs mt-2" readonly id="EmployeeID" name="C_FK_Emp_id">
											<input type="hidden" name="C_LineNo">
											<input type="hidden" value="1" name="C_BioUpload">
										</div>
									</div>                               
					        	</div> 
					        </div>
						    <div class="col-md-6 col-12">
							    <div class="form-group">
									<div class="row">
										<div class="col-md-4" style="text-align: right">
										<label class="control-label">Date Swipped:</label></div>
										<div class="col-8">
											<div class="datepicker input-group date">
												<input id="DateSwipe" type="text" readonly class="form-control" name="C_DateSwipe" value="" style="background: #ffff; text-align: left!important">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
											<div class="required-label text-danger" style="font-style: italic; font-size: 11px; display: none">*Required field</div>
										</div>	
									</div>                               
							    </div>
						    </div>
					    </div>
					    <div class="row">
					    	<div class="col-md-6 col-12">
					    		<div class="form-group">
									<div class="row">
										<div class="col-md-4" style="text-align: right">
											<label class="control-label">Employee Name:</label></div>
										<div class="col-8">
											<input type="text" id="EmployeeName" class="employee_data form-control p-xxs mt-2" data-employee-name readonly>
										</div>
									</div>                               
						        </div>
					    	</div>
					    </div>
					    <div class="row">
							<div class="col-md-6 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4" style="text-align: right">
										<label class="control-label">DOW In:</label></div>
										<div class="col-8 mt-2">
											<select id="dowin" name="C_DOW_In" class="js-select2 form-control" style="width:100%; color:black">
												<option></option>
												<option value="1">Sunday</option>
												<option value="2">Monday</option>
												<option value="3">Tuesday</option>
												<option value="4">Wednesday</option>
												<option value="5">Thursday</option>
												<option value="6">Friday</option>
												<option value="7">Saturday</option>
											</select>
											<div class="required-label text-danger" style="font-style: italic; font-size: 11px; display: none">*Required field</div>
										</div>
									</div>                               
					        	</div>
					        	</div> 
					        	<div class="col-md-6 col-12">
						        	<div class="form-group">
									<div class="row">
										<div class="col-md-4"  style="text-align: right">
											<label class="control-label">DOW Out:</label></div>
										<div class="col-8 mt-2">
											<select id="dowout" name="C_DOW_Out" class="js-select2 form-control" style="width:100%; color:black">
												<option></option>
												<option value="1">Sunday</option>
												<option value="2">Monday</option>
												<option value="3">Tuesday</option>
												<option value="4">Wednesday</option>
												<option value="5">Thursday</option>
												<option value="6">Friday</option>
												<option value="7">Saturday</option>
											</select>
											<div class="required-label text-danger" style="font-style: italic; font-size: 11px; display: none">*Required field</div>
										</div>
									</div>                               
						        </div>
						    </div>
					    </div>
					    <div class="row">
							<div class="col-md-6 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4" style="text-align: right">
										<label class="control-label" style="padding-top:5px">Time In:</label></div>
										<div class="col-8">
											<div class="input-group clockpicker-in mt-2">
												<input id="time" type="text" class="form-control" name="C_TimeIn" readonly style="background: #FFFFFF" tabindex="7">
												<span class="input-group-addon">
													<span class="fa fa-clock-o"></span>
											 	</span>
											</div>
											<div class="required-label text-danger" style="font-style: italic; font-size: 11px; display: none">*Required field</div>
										</div>
									</div>                               
					        	</div>
					        	</div> 
					        	<div class="col-md-6 col-12">
						        	<div class="form-group">
									<div class="row">
										<div class="col-md-4" style="text-align: right">
											<label class="control-label" style="padding-top:5px">Time Out:</label></div>
										<div class="col-8">
											<div class="input-group clockpicker-out mt-2">
												<input type="text" class="form-control" name="C_TimeOut" readonly value="" style="background: #FFFFFF" tabindex="7">
												<span class="input-group-addon">
													<span class="fa fa-clock-o"></span>
											 	</span>
											</div>
											<div class="required-label text-danger" style="font-style: italic; font-size: 11px; display: none">*Required field</div>
										</div>
									</div>                               
						        </div>
						    </div>
					    </div>
					    <div class="row">
							<div class="col-md-4 col-12">
								<div style="font-weight: bold; color: black!important" class="mt-2">Break 1</div>
								<hr>
								<div class="form-group">
									<div class="row">
										<label class="control-label col-3">Break Out:</label>
										<div class="col-9">
											<div class="input-group clockpicker-break">
												<input type="text" class="form-control hidden" name="C_BreakOut_1" readonly value="" style="background: #FFFFFF" tabindex="6">
												<span class="input-group-addon">
													<span class="fa fa-clock-o"></span>
												</span>
											</div> 
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										
										<label class="control-label col-3">Break In:</label>
										<div class="col-9">
											<div class="input-group clockpicker-break">
												<input type="text" class="form-control hidden" name="C_BreakIn_1" readonly value="" style="background: #FFFFFF" tabindex="5">
												<span class="input-group-addon">
													<span class="fa fa-clock-o"></span>
												</span>
											</div> 
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-12">
								<div style="font-weight: bold; color: black!important" class="mt-2">Break 2</div>
								<hr>
								<div class="form-group">
									<div class="row">
										<label class="control-label col-3">Break Out:</label>
										<div class="col-9">
											<div class="input-group clockpicker-break">
												<input type="text" class="form-control hidden" name="C_BreakOut_2" readonly value="" style="background: #FFFFFF" tabindex="8">
												<span class="input-group-addon">
													<span class="fa fa-clock-o"></span>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										
										<label class="control-label col-3">Break In:</label>
										<div class="col-9">
											<div class="input-group clockpicker-break">
												<input type="text" class="form-control hidden" name="C_BreakIn_2" readonly value="" style="background: #FFFFFF" tabindex="7">
												<span class="input-group-addon">
													<span class="fa fa-clock-o"></span>
											 	</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-12">
								<div style="font-weight: bold; color: black!important" class="mt-2">Break 3</div>
								<hr>
								<div class="form-group">
									<div class="row">
										<label class="control-label col-3">Break Out:</label>
										<div class="col-9">
											<div class="input-group clockpicker-break">
												<input type="text" class="form-control hidden" name="C_BreakOut_3" readonly value="" style="background: #FFFFFF" tabindex="10">
												<span class="input-group-addon">
													<span class="fa fa-clock-o"></span>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										
										<label class="control-label col-3">Break In:</label>
										<div class="col-9">
											<div class="input-group clockpicker-break">
												<input type="text" class="form-control hidden" name="C_BreakIn_3" readonly value="" style="background: #FFFFFF" tabindex="9">
												<span class="input-group-addon">
													<span class="fa fa-clock-o"></span>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>	
					    </div>
					</form>
				</div>
				<div class="modal-footer">
                   <button class="btn btn-warning btn-outline close-modal"><span class="fa fa-reply"></span></button>
					<!-- <button class="btn btn-primary btn-outline" id="btnSaveID">
                    	<i class="glyphicon glyphicon-floppy-disk"></i><strong> Save</strong>
                    </button> -->

                    <!-- <button type="button" id="btnSaveandNew" class="btn btn-primary btn-outline" value="SaveandNew"><span class="glyphicon glyphicon-floppy-disk"></span> Save and New</button> -->
					<button type="button" id="btnSaveandClose" class="btn btn-primary btn-outline" value="SaveandClose"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
				</div>
			</div>
		</div>
	</div>