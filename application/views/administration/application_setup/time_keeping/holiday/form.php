<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 25px;
}

.form-group{
	margin-bottom: 5px!important;
	font-size: 11px;
}
.form-control{
	font-size: 11px;
	text-transform: capitalize;
}
#tbl-detail_wrapper{
	width: 50%!important;
	margin-left: 0px!important;
}
</style>
<section class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<h5 style="color: black">Holiday</h5>
			<div class="pull-right ibox-tools">
			
			</div>
		</div>
		<div class="ibox-content">
			<!-- Location -->
			<div id="form-holiday"  class="form-horizontal">
				<form id="form-header" autocomplete="off">
					<div class="row">
						<div class="col-12">
							<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">
										<label class="control-label col-3" style="text-align: right"> 
											<span style="color: red">*</span> Holiday ID:</label>
										<div class="col-9">
											<input type="text" class="form-control required-field" required name="H_ID" value="<?= $header['H_ID']?>">
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<label class="control-label col-3" style="text-align: right">Description:</label>
										<div class="col-9">
											<input type="text" class="form-control required-field" required name="H_Desc" value="<?= $header['H_Desc'] ?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="display: none">
										<label class="control-label col-3">Date:</label>
										<div class="col-9">
											<input style="display: none;" type="text" readonly class="form-control required-field" required name="H_Date" value="<?= $header['H_Date']?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="padding-top: 5px; color:black; text-align: right">
										<span style="color: red">*</span> Type:</div>
										<div class="col-9">
											<select name="H_Type" id="type" value="<?= $header['H_Type']?>" required class="js-select2 form-control required-field" style="width: 100%;">
												<option></option>
												<option value="R" <?= ($header['H_Type'] == 'R' ? 'selected' : '')?>>Regular</option>
												<option value="S" <?= ($header['H_Type'] == 'S' ? 'selected' : '')?>>Special</option>
											</select>
											<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="padding-top: 5px; color:black; text-align: right">
											<span style="color: red">*</span> Payroll Policy:</div>
										<div class="col-9">
											<?php if($type == 'add' || $type == 'update'): ?>
												<select id="payroll_policy" name="H_Policy" value="<?= $header['H_Policy']?>" required class="js-select2 form-control required-field" style="width: 100%">
													<option></option>	
													<option value="Not absent before" <?= ($header['H_Policy'] == 'Not absent before' ? 'selected' : '')?>>Not absent before</option>
														<option value="Not absent after" <?= ($header['H_Policy'] == 'Not absent after' ? 'selected' : '')?>>Not absent after</option>
														<option value="Both" <?= ($header['H_Policy'] == 'Both' ? 'selected' : '')?>>Both</option>
															<option value="None" <?= ($header['H_Policy'] == 'None' ? 'selected' : '')?>>None</option>
												</select>
												<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
												<?php else: ?>
													<input type="text" value="<?= $header['H_Policy']?>" readonly required class="form-control">
													<input type="hidden" name="H_Policy" value="<?= $header['H_Policy']?>" readonly required class="form-control">
													<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
											<?php endif;?>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">
										<label class="control-label col-3" style="padding-top: 5px; color:black; text-align: right">Holiday Start:</label>
										<div class="datepicker input-group date col-9">
											<input type="text" readonly class="form-control required-field" required name="H_HolidayStart" value="<?= ($header['H_HolidayStart'] == '') ? '' : date_format(date_create($header['H_HolidayStart']), 'm/d/Y') ?>" style="background: #ffff; text-align: left!important">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<label class="control-label col-3" style="padding-top: 5px; color:black; text-align: right">Holiday End:</label>
										<div class="datepicker  input-group date col-9">
											<input type="text" readonly class="form-control required-field" required name="H_HolidayEnd" value="<?= ($header['H_HolidayEnd'] == '') ? '' : date_format(date_create($header['H_HolidayEnd']), 'm/d/Y') ?>" style="background: #ffff">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						
				</form>
			</div>
		</div>
		<hr>
		<?= generate_table($detail_table) ?>
		<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['H_ID'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['H_ID'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
	</div>
</div>


	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>

		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['H_ID'] ?>';
		var update_link				= '<?= './update?id='.md5($header['H_ID'])?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/application_setup/time_keeping/holiday' ?>';
		var module_folder 			= 'administration/application_setup/time_keeping';
		var module_controller 		= 'holiday';
		var module_name 			= 'Holiday';
		var isRequiredDetails 		= true;
		var tabledetail;
		var detail 					= <?= json_encode($details) ?>;
	</script>

	<script src="<?php echo JS_DIR?>app/administration/application_setup/time_keeping/holiday/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>	

</section>