<style>
tfoot {
	display: table-header-group;
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<label><h5>System Configuration</h5></label>
		</div>
		<div class="ibox-content">
			<h4 style="color: black">Modules</h4>
			<div>
				<a role="button" class="btn btn-outline btn-success btn-sm" href="<?= DOMAIN.'administration/application_setup/general/system_config/add' ?>"><span class="fa fa-plus"></span> Add another config</a>
			</div>
			<div class="col-md-7 col-12">
				<?php foreach ($modules as $key => $value): ?>
					<div class="hr-line-dashed" style="background: black!important"></div>
					<h4 style="color: black"><?= $value['M_DisplayName'] ?></h4>
					<?php $configs = getConfigs($value['SC_ModuleID']) ?>
					<?php foreach ($configs as $key => $conf): ?>
						<div class="form-group row">
							<label class="col-md-4 col-12 text-left"><?= $conf['SC_Description'] ?>:</label>
							<div class="col-md-8 col-12">
								<input type="<?= ($conf['SC_DataType'] == 'numeric' ? 'number' : 'checkbox') ?>" data-doc-no="<?= $conf['SC_ID'] ?>" <?= ($conf['SC_DataType'] == 'bit' ? ($conf['SC_Value'] == 1 ? 'checked ' : '') : 'value="'.$conf['SC_Value'].'"' ) ?> <?= ($conf['SC_DataType'] == 'numeric' ? 'class="form-control text-right numeric-input"' : 'class="bit-input"') ?>>
							</div>
						</div>
					<?php endforeach ?>
				<?php endforeach ?>
			</div>
		</div>
	</div>
	
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- Moment -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Page-Level Scripts -->
	<script type="text/javascript">
		var tableindex;
		var process_action 	= 'System Configuration';
		var table_name 		= 'tblSystemConfig';
		var controller_url 	= 'administration/application_setup/general/system_config/';
	</script>
	<script src="<?= JS_DIR;?>app/administration/application_setup/general/system_config/index.js"></script>
	<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
</section>


