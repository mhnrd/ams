<style>
tfoot {
	display: table-header-group;
}

@media screen and (max-width: 768px){
	#attribute_filter input{
		width: 60%!important;
	}
	#attribute_filter{
		margin-right: 500px!important;
	}
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<label class="control-label"><h5><?= $module ?></label></h5>
			<div class="ibox-tools pull-right">
				<button class="btn btn-success" id="btnActive" disabled>
					<i class="fa fa-check"></i> Activate
				</button>
				<button class="btn btn-danger" id="btnDeactive" disabled>
					<i class="fa fa-times"></i> Deactivate
				</button>
			</div>
		</div>
		<div class="ibox-content">
			<?= generate_table($table_hdr)?>
		</div>	
	</div>

    <script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- Moment -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Page-Level Scripts -->
	<script type="text/javascript">
		var tableindex;
		var col_center 		= <?= json_encode($hdr_center)?>;
		var process_action 	= 'Attributes';
		var table_name 		= 'tblAttribute';
		var attr 			= '<?= $this->uri->segment(4) ?>';
		var attr_module 	= '<?= $this->uri->segment(3) ?>'
		var controller_url 	= 'administration/application_setup/general/attributes/';
		var table_id 		= '<?= key($table_hdr) ?>';
	</script>
	<script src="<?php echo JS_DIR?>app/administration/application_setup/general/attributes/index.js"></script>
	<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/advsearch.js"></script>
	<script src="<?php echo JS_DIR ?>app/rules/index_functions.js"></script>
</section>