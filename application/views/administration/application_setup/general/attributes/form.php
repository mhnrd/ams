<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">

<style>
.col-5, h5, small{
	color: black!important;
}
</style>
<section class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<h5><?= $module ?></h5>
		</div>
		<div class="ibox-content">
			<!-- Attribute: Cost/Profit Class -->
			<div id="form-attribute-cpc"  class="form-horizontal">
				<form id="form-header">
					<div class="row">
						<div class="col-md-5 col-12">
							<input type="hidden" name="AD_FK_Code" value="<?= $this->uri->segment(4) ?>">
							<input type="hidden" name="AD_ID" value="<?= $header['AD_ID'] ?>">
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
										<label class="control-label">Code:</label></div>
									<div class="col-9">
										<input type="text" class="form-control required-field" required name="AD_Code" value="<?= $header['AD_Code']?>" <?= ($type == 'update' ? 'readonly' : '') ?> tabindex="1">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
										<label class="control-label">Description:</label></div>
									<div class="col-9">
										<input type="text" class="form-control required-field" required name="AD_Desc" value="<?= $header['AD_Desc'] ?>" tabindex="2">
										<div class="required-label text-danger" style="font-style: italic; font-size: 12px; display: none">*Required field</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
						<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-primary btn-outline save" data-doc-no="<?= $header['AD_ID'] ?>" data-action="save-new" tabindex="5">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-primary btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['AD_ID'] ?>" data-action="save-close" tabindex="6">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
		</div>
	</div>


	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>

		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['AD_ID'] ?>';
		var update_link				= '<?= './update?id='.md5($header['AD_ID'])?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/application_setup/general/attributes' ?>';
		var module_folder 			= 'administration/application_setup/general';
		var module_controller 		= 'attributes';
		var module_name 			= '<?= getDisplayName($this->uri->segment(4)) ?>';
		var attr_url 				= '<?= DOMAIN.'administration/application_setup/'.$this->uri->segment(3).'/'.$this->uri->segment(4) ?>';
		var attr_folder 			= 'administration/application_setup/<?= $this->uri->segment(3) ?>';
		var attr_controller 		= '<?= $this->uri->segment(4) ?>';
		var attr_module 			= '<?= $this->uri->segment(3) ?>'
		var isRequiredDetails 		= false;
	</script>

	<script src="<?php echo JS_DIR?>app/administration/application_setup/general/attributes/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>	

</section>