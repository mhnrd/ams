<style>
tfoot {
	display: table-header-group;
}
</style>
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Clockpicker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section id="add-bom" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<h5>Sub-location Setup</h5>
			<div class="pull-right ibox-tools">
				<a href="<?= DOMAIN."application_setup/".$this->uri->segment(2); ?>" class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back">
					<span class="fa fa-reply"></span>					
				</a>
				<?php if($type != 'view'): ?>
					<button id="save" type="button" class="btn btn-primary btn-outline" data-toggle="tooltip" data-placement="bottom" title="<?php echo ($type == 'add') ? 'Save' : 'Update' ?>" data-todo="<?= $type ?>">
							<span class="glyphicon glyphicon-floppy-disk"></span>
					</button>
				<?php endif; ?>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">
			<div id="form-transfer-request" class="form-horizontal">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group" style="margin-right: 50%">
							<label class="control-label col-md-3">Location ID:</label>
								<div class="col-md-9">
									<input type="text" class="form-control"  name="LOC_Id" placeholder="Enter Location ID" value="<?= $header['LOC_Id'] ?>">
								</div>
						</div>
						<div class="form-group" style="margin-right: 50%">
							<label class="control-label col-md-3">Location Name:</label>
								<div class="col-md-9">
									<input type="text" class="form-control" placeholder="Enter Location Name" name="LOC_Name" value="<?= $header['LOC_Name'] ?>">
								</div>
						</div>
						<div class="form-group" style="margin-right: 50%">
							<label class="control-label col-md-3">Location Type:</label>
								<div class="col-md-9">
									<select class="location-type" id="location-type" name="LOC_FK_Type_id" style="width: 100%">
										<option value="1" <?= (($header['LOC_FK_Type_id'] == 1) ? 'selected' : '') ?>>Warehouse</option>
										<option value="2" <?= (($header['LOC_FK_Type_id'] == 2) ? 'selected' : '') ?>>Business Unit</option>
										<option value="3" <?= (($header['LOC_FK_Type_id'] == 3) ? 'selected' : '') ?>>Department</option>
									</select>
								</div>
						</div>
						<div class="form-group" style="margin-right: 50%">
							<label class="control-label col-md-3">Level:</label>
								<div class="col-md-9">
									<select class="location-level" id="location-level" name="LOC_FK_Level_id" style="width: 100%">
										<option value='1' <?= (($header['LOC_FK_Level_id'] == 1) ? 'selected' : '') ?>>Level 1</option>
										<option value='2' <?= (($header['LOC_FK_Level_id'] == 2) ? 'selected' : '') ?>>Level 2</option>
										<option value='3' <?= (($header['LOC_FK_Level_id'] == 3) ? 'selected' : '') ?>>Level 3</option>
										<option value='4' <?= (($header['LOC_FK_Level_id'] == 4) ? 'selected' : '') ?>>Level 4</option>
										<option value='5' <?= (($header['LOC_FK_Level_id'] == 5) ? 'selected' : '') ?>>Level 5</option>
									</select>
								</div>
						</div>
						<div class="form-group" style="margin-right: 50%">
							<label class="control-label col-md-3">Parent:</label>
								<div class="col-md-9">
									<select class="location-parent" id="location-parent" name="LOC_Parent_id" style="width: 100%">
										<?php if($type == 'view'): ?>
											<option value="<?= (($header['LOC_Parent_id'] == null ? 'Parent' : $header['LOC_Parent_id'])) ?>"><?= (($header['LOC_Parent_id'] == null ? 'Parent' : $header['LOC_Name'])) ?></option>
										<?php endif; ?>
										<?php if($type == 'update'): ?>
											<?php if($header['LOC_Parent_id'] != null): ?>
												<?php foreach($parent as $key => $option): ?>
													<option value="<?= $option['LOC_Id'] ?>" <?= (($header['LOC_Parent_id'] == $option['LOC_Id']) ? 'selected' : '') ?>><?php echo $option['LOC_Name'] ?></option>
												<?php endforeach; ?>
											<?php else: ?>
												<option value="Parent" selected>Parent</option>
											<?php endif; ?>
										<?php endif; ?>
									</select>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <!-- Select2 -->
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/clockpicker/clockpicker.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<!-- Moment -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<script>
		var output_type = '<?= $type ?>';
		var parent = '<?php echo ($header['LOC_Parent_id'] == null ? 'Parent' : $header['LOC_Parent_id']) ?>'
	</script>
	<script src="<?php echo JS_DIR?>app/application_setup/sub_location_setup/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
	

</section>