<div class="modal inmodal animated fadeInUp" id="detail-template-location" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 80px">
		<input type="hidden" id="line-no-loc" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content<?php /* animated flipInY */ ?>">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;color: black!important;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Order Forecast Matrix Location</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="form-group">
								<label>Location:</label>
								<div>
									<select id="location-list" data-type="location-list" class="js-select2" style="width: 100%">
										<?php foreach($location_list as $location){ ?>
									<option value="<?php echo $location['CA_FK_Location_id'] ?>" <?php echo ($location['CA_DefaultLocation'])? 'selected' : ''?>><?php echo $location['SP_StoreName'] ?></option>
									<?php }?>
								</select>
								</div>
							</div>
							
							<div class="form-group">
								<label>Category:</label>
								<div>
									<select id="item-group-list" data-type="item-group-list" class="js-select2" style="width: 100%">
									<?php foreach($category_list as $category){ ?>
									<option value="<?php echo $category['IG_Id'] ?>"><?php echo $category['IG_Desc'] ?></option>
									<?php }?>
								</select>
								</div>									
							</div>
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" id="add-new-loc" class="btn btn-primary" value="add">Add & New</button>
					<button type="button" id="add-close-loc" class="btn btn-primary" value="add">Add & Close</button>
				</div>
			</div>
		</div>
	</div>