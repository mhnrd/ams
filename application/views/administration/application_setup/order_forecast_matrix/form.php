<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">

<section id="form-ig" class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
		<h5>Order Forecast Matrix Setup</h5>
			<div class="pull-right ibox-tools">
				<a href="<?= DOMAIN."application_setup/".$this->uri->segment(2); ?>" class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" >
					<span class="fa fa-reply"></span>					
				</a>
				<?php if($type != 'view'): ?>
				<button id="save" type="button" class="btn btn-primary btn-outline" data-toggle="tooltip" data-placement="bottom" title="Save" data-todo="<?= $type ?>" data-id-no="<?= $header['OFM_Code'] ?>">
					<span class="glyphicon glyphicon-floppy-disk"></span>
				</button>
				<?php endif; ?>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">	
			<div id="form-order-forecast-matrix" class="form-horizontal">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-3 control-label">Code:</label>
							<div class="col-md-9">
								<input type="text" class="form-control" <?= ($type=='add')?"":"readonly";?> name="OFM_Code" value="<?= $header['OFM_Code']?>"/>
							</div>
						</div>
						<div class="form-group">
								<label class="col-md-3 control-label">Remarks:</label>
								<div class="col-md-9">
								<textarea name="OFM_Remarks"class="form-control" value="<?= $header['OFM_Remarks']?>"><?= $header['OFM_Remarks']?></textarea>
								</div>
						</div>
					</div>
			
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-3 control-label">Location:</label>
								<div class="col-md-9">
								<?php if($type == 'add'): ?>
									<select name="OFM_Location" class="js-select2">
										<?php foreach($location_list as $location){ ?>
										<option value="<?= $location['CA_FK_Location_id'] ?>" <?= ($header['OFM_Location'] == $location['CA_FK_Location_id']) ? 'selected' : ''?>><?= $location['SP_StoreName'] ?></option>
										<?php }?>
									</select>
								<?php else: ?>
									<input type="text" name="OFM_Location" value="<?= $header['OFM_Location']?>" readonly class="form-control">
								<?php endif; ?>
							</div>
						</div>
					</div>

					
				</div>	
			</div>			
		 		<div class="hr-line-dashed"></div>
	<div id="form-order-forecast-matrix-2" class="form-horizontal">
		<div class="row">
			<div class="col md-12">
				<div class="col-md-6">
					<h4>Item List</h4>
					<div class="table-responsive">				
						<table id="detail-template-list" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<?php if($type != 'view'): ?>
										<th class="text-center">
											<a id="add-detail-template" class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="#"><i class="fa fa-plus"></i></a>
										</th>
									<?php endif; ?>
									<th class="text-center">Item No</th>
									<th class="text-center">Item Description</th>
									<th class="text-center">UOM</th>
								</tr>
							</thead>
							<tbody>
							<?php if($type == 'update' || $type == 'view'): ?>
								<?php foreach($details_item as $row_detail): ?>
									<tr class="detail-template" data-action="<?= $type ?>" data-line-no="<?= $row_detail['OFMI_EntryNo'] ?>">
										<?php if($type != 'view'): ?>
											<td class="text-center">
												<!-- <button class="edit-detail-template btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button">
													<i class="fa fa-pencil"></i>
												</button>  -->
												<button class="delete-detail-template btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button">
													<i class="fa fa-trash-o"></i>
												</button>
											</td>
										<?php endif; ?>
										<td class="text-center" data-item-no="<?= $row_detail['OFMI_ItemNo']?>"><?= $row_detail['OFMI_ItemNo']?></td>
										<td data-item-description="<?= $row_detail['IM_Sales_Desc'] ?>"><?= $row_detail['IM_Sales_Desc'] ?></td>
										<td class="text-center" data-uom="<?= $row_detail['AD_Id'] ?>" data-uom-name="<?= $row_detail['AD_Desc'] ?>"><?= $row_detail['AD_Desc'] ?></td>
									</tr>
								<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="col-md-6">
							<h4>Location List</h4>
					<div class="table-responsive">				
						<table id="detail-template-list-location" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<?php if($type != 'view'): ?>
										<th class="text-center">
											<a id="add-detail-template-location" class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="#"><i class="fa fa-plus"></i></a>
										</th>
									<?php endif; ?>
										<th class="text-center">Location</th>
										<th class="text-center">Description</th>
										<th class="text-center">Category</th>
								</tr>
							</thead>
							<tbody> 
							<?php if($type == 'update' || $type == 'view'): ?>
								<?php foreach($details_location as $location_detail): ?>
									<tr class="detail-template-location" data-action="<?= $type ?>" data-line-no="<?= $location_detail['OFML_EntryNo'] ?>">
										<?php if($type != 'view'): ?>
											<td class="text-center">
												<!-- <button class="edit-detail-template-location btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button">
													<i class="fa fa-pencil"></i>
												</button>  -->
												<button class="delete-detail-template-location btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button">
													<i class="fa fa-trash-o"></i>
												</button>
											</td>
										<?php endif; ?>
										<td data-location="<?= $location_detail['OFML_Location'] ?>"><?= $location_detail['OFML_Location'] ?></td>
										<td data-description="<?= $location_detail['SP_StoreName']?>"><?= $location_detail['SP_StoreName'] ?></td>
										<td class="text-center" data-category="<?php echo $location_detail['OFML_Category'] ?>"> <?php echo $location_detail['IG_Desc'] ?></td>
									</tr>
								<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div> <!-- end of col md6 -->
		</div> <!-- end of col md12 -->
	</div> <!-- end of row -->
</div> <!-- end of formitemgroup2 -->
</div>
</div>

	
	<?php $this->load->view('application_setup/order_forecast_matrix/detail-template'); ?>;
	<?php $this->load->view('application_setup/order_forecast_matrix/detail-template-location'); ?>

<!-- detail template location here -->
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <!-- Select2 -->
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/clockpicker/clockpicker.js"></script>
	<!-- Global Declaration -->
	<script>var output_type = '<?= $type ?>'</script>


	<script src="<?php echo JS_DIR?>app/application_setup/order_forecast_matrix/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>

	

</section>