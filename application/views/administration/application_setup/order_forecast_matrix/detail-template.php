<div class="modal inmodal animated fadeInUp" id="detail-template" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 80px">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md">
		<div class="modal-content<?php /* animated flipInY */ ?>">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px;color: black!important;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Order Forecast Matrix Detail</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="form-group">
								<label>Item No:</label>
								<div>
									<select id="item-no" data-type="item-no" class="item" style="width: 100%">
										<option></option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label>Item Description:</label>
								<div>
									<select id="item-description" data-type="item-description"  class="item" style="width: 100%">
										<option></option>
									</select>
								</div>									
							</div>
							<div class="form-group">
								<label>UOM:</label>
								<div>
									<input type="text" class="form-control" readonly style="width: 100%" name="OFD_UomName">
									<input type="hidden" class="form-control" readonly style="width: 100%" name="OFD_UomID">
								</div>									
							</div>
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" id="add-new" class="btn btn-primary" value="add">Add & New</button>
					<button type="button" id="add-close" class="btn btn-primary" value="add">Add & Close</button>
				</div>
			</div>
		</div>
	</div>