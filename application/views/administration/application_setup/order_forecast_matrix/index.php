<style>
tfoot {
	display: table-header-group;
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Order Forecast Matrix List</h5>
		</div>
		<div class="ibox-content">
			<div class="table-responsive datatable">
				<table class="table table-striped table-bordered table-hover dataTables-example compact">
					<thead>
						<tr>
							<th class="no-sort text-center col-xs-2">
								<?php if($access_header === true): ?>
								<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="<?= DOMAIN;?>application_setup/order_forecast_matrix/add"><i class="fa fa-plus"></i></a>
								<?php endif; ?>
							</th>							
							<th class="text-center col-xs-2 sort-asc">Code</th>
							<th class="text-center col-xs-5">Location</th>
							<th class="text-center col-xs-2">Remarks</th>
						</tr>
					</thead>
					<tfoot style="display: none"></tfoot>
				</table>
			</div>
		</div>
	</div>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<!-- Sweet alert -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- Moment -->
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Page-Level Scripts -->
	<script type="text/javascript">
		var tableindex;
	</script>
	<script src="<?php echo JS_DIR?>app/application_setup/order_forecast_matrix/index.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/advsearch.js"></script>
</section>
