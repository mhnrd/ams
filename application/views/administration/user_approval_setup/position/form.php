<style>
	tfoot{
		display: table-header-group;
	}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- Text spinners style -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/textSpinners/spinners.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
	.select2-results__options{
	        font-size:11px !important;
	 }
	@media only screen and (max-width: 768px){
		.fixed-header{
			display: none;
		}
	}
</style>
<section class="wrapper wrapper-content">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="pull-left m-r-md">
                <a href="<?= DOMAIN.'dashboard' ?>" id="home">
                    <i class="fa fa-home" style="font-size:25px; margin-top: 7px"></i>
                </a>
            </div>
            <!-- <div class="pull-right">
                <a href="<?= DOMAIN.'asset_management/configuration' ?>" class="btn btn-outline btn-xs btn-warning" role="button" style="margin-top: 7px">
                    <i class="fa fa-reply"></i>
                </a>
            </div> -->
            <h2 style="font-weight: bold; padding-top: 1px; color: #2980b9!important" class="text-success">POSITION LIST</h2>
        </div>
		<div class="ibox-content">
			<?php
				/*
				Converting submit to ajax is more better in this project
				*/
			?>
			<div id="form-cost-profit-center" class="form-horizontal">
				<form id="form-header">
					<div class="col-12">
						<div class="col-md-8 col-12"> 
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
										<label><b class="text-danger">*</b> Position Type:</label>
									</div>
									<div class="col-9">
										<select name="P_Type" id="P_Type" tabindex="1" required class="pt-js-select2 form-control" style=" width: 100%;">
											<option></option>
											<?php foreach($position_type_list as $position_type): ?>
												<option value="<?= $position_type['PT_PositionType_id'] ?>" <?= ($header['P_Type'] == $position_type['PT_PositionType_id']) ? 'selected' : '' ?>><?= $position_type['PT_PositionType'] ?></option>
											<?php endforeach; ?>
										</select>
										<div class="required-label text-danger" style="font-style: italic;font-size: 12px; display: none;">*Required Field</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
										<label class="control-label">Position ID:</label>
									</div>
									<div class="col-9">
										<input type="text" id="P_ID" tabindex="2" required name="P_ID" class="form-control required-field" value="<?= $header['P_ID'] ?>" readonly>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">	
										<label class="control-label">Position:</label>
									</div>
									<div class="col-9">	
										<input type="text" name="P_Position" tabindex="3" required id="P_Position" class="form-control required-field" value="<?= $header['P_Position']?>">	
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">
										<label>Parent:</label>
									</div>
									<div class="col-9">
										<select name="P_Parent" id="P_Parent" tabindex="4" class="pts-js-select2 form-control" style="width : 100%;">
											<option value=""></option>
											<?php if ($type != 'add'): ?>
												<?php foreach ($parent_list as $key => $value): ?>
													<option value="<?= $value['P_ID'] ?>" <?= ($value['P_ID'] == $header['P_Parent'] ? 'selected' : '') ?>><?= $value['P_Position'] ?></option>
												<?php endforeach ?>
											<?php endif ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-3" style="text-align: right;">	
										<label class="control-label">Allow Half Day:</label>
									</div>
									<div class="col-9">	
										<input type="checkbox" tabindex="5" name="P_AllowHalfDay" required id="P_AllowHalfDay" <?= ($header['P_AllowHalfDay'] ? 'checked' : '') ?>>	
									</div>
								</div>
							</div>
						</div>
					</div>
					</form>
				</div>
				<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="6">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-success btn-outline save" data-doc-no="<?= $header['P_ID'] ?>" data-action="save-new" tabindex="7">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-success btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['P_ID'] ?>" data-action="save-close" tabindex="8">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
			</div>
		</div>
	</div>
	
	
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['P_ID'] ?>';
		var update_link				= '<?= './update?id='.md5($header['P_ID'])?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'administration/user_approval_setup/position' ?>';
		var module_folder 			= 'administration/user_approval_setup';
		var module_controller 		= 'position'
		var module_name 			= 'Position'; 
		var isRequiredDetails 		= false;
		<?php if($type == 'view'): ?>
			
			var target_url = '<?= DOMAIN.'administration/user_approval_setup/position/' ?>';
		<?php endif; ?>
	</script>
	<script src="<?php echo JS_DIR?>app/administration/user_approval_setup/position/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>
	
</section>