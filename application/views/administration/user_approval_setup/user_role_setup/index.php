<style>
tfoot {
	display: table-header-group;
}
@media screen and (max-width: 425px){
	#advance-search-small{
		display: inline!important;
	}
	.table-tools{
		margin-right: 5px!important;
		margin-top: 5px!important;
	}
	#advance-search{
		display: none;
	}
	#user_role_setup_filter{
		margin-left: -50px!important;
	}
	#clear-search-small{
		margin-top: 4px!important;
	}
	#user_role_setup_filter{
		margin-right: 0px!important;
	}
}

@media screen and (max-width: 768px){
	#user_role_setup_filter input{
		width: 60%!important;
	}
	#user_role_setup_filter{
		margin-right: 500px!important;
	}
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<!-- Clockpicker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

<section class="wrapper wrapper-content">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="pull-left m-r-md">
                <a href="<?= DOMAIN.'dashboard' ?>" id="home">
                    <i class="fa fa-home" style="font-size:25px; margin-top: 7px"></i>
                </a>
            </div>
            <div class="pull-right">
                <a href="<?= DOMAIN.'asset_management/configuration' ?>" class="btn btn-outline btn-xs btn-warning" role="button" style="margin-top: 7px">
                    <i class="fa fa-reply"></i>
                </a>
            </div>
            <h2 style="font-weight: bold; padding-top: 1px; color: #2980b9!important" class="text-success">USER ROLE SETUP</h2>
        </div>
		<div class="ibox-content">
			<?= generate_table($table_hdr)?>
		</div>
	</div>


<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.js"></script>
<!-- <script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script> -->
<!-- <script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script> -->
<!-- Sweet alert -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Moment -->
<script src="<?php echo EXTENSION ?>moment.js"></script>
<!-- Date Range Picker -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Clockpicker -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/clockpicker/clockpicker.js"></script>
<!-- Page-Level Scripts -->
<script type="text/javascript">
	var tableindex;
	var col_center		= <?= json_encode($dtl_center) ?>;
	var process_action 	= 'User Role Setup';
	var table_name 		= 'tblGroup';
	var controller_url 	= 'administration/user_approval_setup/user_role_setup/';
	var table_id 		= '<?= key($table_hdr) ?>';
</script>
<script src="<?php echo JS_DIR ?>app/administration/user_approval_setup/user_role_setup/index.js"></script>
<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
<script src="<?php echo JS_DIR ?>app/rules/advsearch.js"></script>
<style>
	/*@media screen and (max-width: 425px){
		.pull-right{
			text-align: center!important;
		}
	}*/
</style>

</section>