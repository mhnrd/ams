<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">


<section class="wrapper wrapper-content">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="pull-left m-r-md">
                <a href="<?= DOMAIN.'dashboard' ?>" id="home">
                    <i class="fa fa-home" style="font-size:25px; margin-top: 7px"></i>
                </a>
            </div>
            <!-- <div class="pull-right">
                <a href="<?= DOMAIN.'asset_management/configuration' ?>" class="btn btn-outline btn-xs btn-warning" role="button" style="margin-top: 7px">
                    <i class="fa fa-reply"></i>
                </a>
            </div> -->
            <h2 style="font-weight: bold; padding-top: 1px; color: #2980b9!important" class="text-success">USER ROLE SETUP</h2>
        </div>
		<div class="ibox-content">
			<!-- Location -->
				<div id="form-user-role-setup" class="form-horizontal">
					<form id="form-header">
						<div class="col-12">
							<div class="col-md-5 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-4"><label class="control-label">Role Code:</label></div>
										<div class="col-8">
											<input type="text" readonly name="R_ID" placeholder="Enter Role Code" required class="form-control" value="<?= $header['R_ID'] ?>" tabindex="1">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-4"><label class="control-label">Role Name:</label></div>
										<div class="col-8">
											<input type="text" name="R_Name" placeholder="Enter Role Name" required class="form-control" value="<?= $header['R_Name'] ?>" tabindex="2">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-4"><label class="control-label">Role Description:</label></div>
										<div class="col-8">
											<input type="text" name="R_Description" placeholder="Enter Role Description" class="form-control" value="<?= $header['R_Description'] ?>" tabindex="3">
										</div>
									</div>
								</div>
							</div>
						</div>

					</form>
					
					<hr>

					<div>
						<h4>Modules</h4>
					</div>
					<div class="row">
						<?= generate_table_ur($table_detail) ?>
						<?= generate_table_rf($table_sub_detail) ?>
					</div>
				</div>


				<hr>
				<div>
					<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="4">
						<span class="fa fa-reply"></span>					
					</button>
					<?php if($type != 'view'): ?>
						<?php if ($type == 'add'): ?>
							<button  type="button" class="btn btn-success btn-outline save" data-doc-no="<?= $header['R_ID'] ?>" data-action="save-new" tabindex="5">
								<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
							</button>
						<?php endif ?>
						<button  type="button" class="btn btn-success btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['R_ID'] ?>" data-action="save-close" tabindex="6">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
						</button>
					<?php endif; ?>
				</div>

			</div>
		</div>


	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.js"></script>
	<!-- <script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script> -->
	<!-- <script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script> -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/clockpicker/clockpicker.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['R_ID'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['R_ID']) ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/user_approval_setup/user_role_setup' ?>';
		var module_folder 			= 'administration/user_approval_setup';
		var module_controller 		= 'user_role_setup';
		var module_name 			= 'User Role Setup';
		var isRequiredDetails 		= true;
		var tabledetail;
		var table_subdetail;
		var modules 				= <?= json_encode($module) ?>;
		var functions 				= <?= json_encode($functions) ?>;
	</script>

	<script src="<?php echo JS_DIR?>app/administration/user_approval_setup/user_role_setup/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>	

	</section>