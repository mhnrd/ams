<style>
tfoot {
	display: table-header-group;
}
tr.row_selected td{
	background-color: #c7ecee !important;
}
table{
	cursor: pointer;
}
.dataTables-example tbody tr td a.delete-button:hover{
	color: white!important;
}
.advance-search:hover, .advance-search-small:hover{
	color: white!important;
}

.tooltip{
      z-index: 5000 !important;
}

@media screen and (max-width: 425px){
	#advance-search-small{
		display: inline!important;
	}
	.table-tools{
		margin-right: 5px!important;
		margin-top: 5px!important;
	}
	#advance-search{
		display: none;
	}
	#approval_setup_filter{
		margin-left: -50px!important;
	}
	#approval_setup_detail_filter{
		margin-left: -50px!important;
	}
	#clear-search-small{
		margin-top: 4px!important;
	}
	#approval_setup_filter{
		margin-right: 0px!important;
	}
	#approval_setup_detail_filter{
		margin-right: 0px!important;
	}
}

@media screen and (max-width: 768px){
	#approval_setup_filter input{
		width: 60%!important;
	}
	#approval_setup_filter{
		margin-right: 500px!important;
	}
	#approval_setup_detail_filter input{
		width: 60%!important;
	}
	#approval_setup_detail_filter{
		margin-right: 500px!important;
	}
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section class="wrapper wrapper-content">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Module</h5>
			<div class="pull-right ibox-tools">
	
			</div>
		</div>
		<div class="ibox-content">
			<div class="row">
				<div class="col-sm-5">
					<input type="hidden" class="form-control p-xxs" name="Noseries" value="">
					<?= generate_table($table_hdr)?>
				</div>
				<div class="col-lg-7">
					<?= generate_table_standard($table_detail) ?>
					<?php $this->load->view('administration/user_approval_setup/approval_setup/detail-template'); ?>
				</div>
			</div>
		</div>
	</div>


	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>
	<!-- Date Range Picker -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Page-Level Scripts -->
	<script type="text/javascript">
		var tableindex;
		var tabledetail;
		
		var table_id 		= '<?= key($table_hdr) ?>';
		var col_center		= <?= json_encode($hdr_center)?>;
		var process_action 	= 'Approval Setup';
		var table_name 		= 'tblApprovalSetup';
		var controller_url 	= 'administration/user_approval_setup/approval_setup/';
	
	</script>
	<script src="<?php echo JS_DIR?>app/administration/user_approval_setup/approval_setup/index.js"></script>
	<script src="<?php echo JS_DIR ?>app/administration/user_approval_setup/approval_setup/index_functions.js"></script>
	<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>

</section>