<style type="text/css">	.clockpicker-popover { z-index: 999999 !important;} div.form-group{margin-bottom: 5px;}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 22px;
}
</style>
<div class="modal inmodal animated fadeInUp" id="detail-template-ap" data-backdrop="static"  role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document"style="width: 30%">
		<div class="modal-content<?php /* animated flipInY */ ?>">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" style="margin-right: 15px" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>&nbsp;Setup Entry</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body">
					<form id="form-approval-setup">
						<input type="hidden" name="mode">
						<input type="hidden" id="NoseriesID" name="AS_FK_NS_id">	
						<input type="hidden" name="AS_Sequence">	
						<div class="row">
							<div class="col-12">
						<div class="form-group">
							<div class="row">
								<div class="col-md-3"><label><span style="color:red">*</span> Position:</label></div>
								<div class="col-9">
									<select class="js-select2 form-control required-field" required name="AS_FK_Position_id" style="width: 100%;">
										<option value=""></option>
									</select>	
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-3"><label><span style="color:red">*</span> Amount:</label></div>
								<div class="col-9">
									<input type="number" class="form-control text-right ap-amount" name="AS_Amount" required placeholder="0.00">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-3"><label>Unlimited:</label></div>
								<div class="col-9">
									<input type="checkbox" class="i-checks" name="AS_Unlimited" value="1">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-3"><label>Required:</label></div>
								<div class="col-9">
								<input type="checkbox" class="i-checks" name="AS_Required" value="1"></div>
							</div>
						</div>
					</div>
				</div>
					</form>
				</div>

				<!-- MODAL FOOTER -->
				<div class="modal-footer">
					<button class="btn btn-warning btn-outline close-modal" data-dismiss="modal" data-toggle="tooltip" data-placement="bottom" title="Back"><span class="fa fa-reply"></span></button>	
					<button type="button" id="btnSaveandNew" class="btn btn-primary" value="SaveandNew">Save and New</button>
					<button type="button" id="btnSaveandClose" class="btn btn-primary" value="SaveandClose">Save and Close</button>
				</div>
		</div>
	</div>
</div>