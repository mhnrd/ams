<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 22px;
}
</style>
<section class="wrapper wrapper-content">
	<div class="ibox">
		<div class="ibox-title">
			<h5>Approval Setup</h5>
			<div class="pull-right ibox-tools">
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" >
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
				
				<button  type="button" class="btn btn-primary btn-outline save" data-toggle="tooltip" data-placement="bottom" title="<?= ($type=='add') ? "Save" : "Update" ?>" data-todo="<?= $type ?>" data-doc-no="<?= $header['AS_FK_Position_id'] ?>" data-action="save-close">
							<span class="glyphicon glyphicon-floppy-disk"></span>
				</button>

			<?php endif; ?>
			</div>
		</div>
		<div class="ibox-content">
			<?= generate_table($table_detail) ?>
		</div>
	</div>


	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>

		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['AS_FK_Position_id'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['AS_FK_Position_id']) ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/user_approval_setup/approval_setup' ?>';
		var module_folder 			= 'administration';
		var module_controller 		= 'approval_setup';
		var module_name 			= 'Approval Setup';
		var isRequiredDetails 		= true;
		var modules 				= <?= json_encode($module) ?>;
		//var tabledetail;
	</script>

	<script src="<?php echo JS_DIR?>app/administration/user_approval_setup/approval_setup/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>	

</section>