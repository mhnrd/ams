<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<style>
	label{
		font-weight: bold;
	}
	div.sa-button-container .cancel{
		background: #DD6B55!important
	}
	div.sa-button-container .cancel:hover{
		opacity: 0.8!important;
	}
</style>

<section class="wrapper wrapper-content">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="pull-left m-r-md">
                <a href="<?= DOMAIN.'dashboard' ?>" id="home">
                    <i class="fa fa-home" style="font-size:25px; margin-top: 7px"></i>
                </a>
            </div>
            <!-- <div class="pull-right">
                <a href="<?= DOMAIN.'asset_management/configuration' ?>" class="btn btn-outline btn-xs btn-warning" role="button" style="margin-top: 7px">
                    <i class="fa fa-reply"></i>
                </a>
            </div> -->
            <h2 style="font-weight: bold; padding-top: 1px; color: #2980b9!important" class="text-success">USERS</h2>
        </div>
		<div class="ibox-content">
			<!-- Location -->
			<div id="form-user-role-setup" class="form-horizontal">
				<form id="form-header">
					<div class="col-12">
						<div class="row">
							<div class="col-md-7 col-12">
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="padding-top: 5px">User ID:</div>
										<div class="col-9">													
											<input type="text" name="U_ID" minlength="8" maxlength="16" class="form-control" required value="<?= $header['U_ID'] ?>" tabindex="1">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="padding-top: 10px">Name:</div>
										<div class="col-9">
											<input type="text" name="U_Username" tabindex="2" class="form-control" required value="<?= $header['U_Username'] ?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="padding-top: 10px">Position:</div>
										<div class="col-9">
											<select name="U_FK_Position_id" class="form-control" tabindex="3" style="width: 100%">
												<option value=""></option>
												<?php foreach ($position as $key => $value): ?>
													<option value="<?= $value['P_ID'] ?>" <?= ($value['P_ID'] == $header['U_FK_Position_id'] ? 'selected' : '') ?>><?= $value['P_Position'] ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="padding-top: 10px">Default Location:</div>
										<div class="col-9">
											<select name="DefaultLocation" class="form-control" tabindex="4" style="width: 100%">
												<option value=""></option>
												<?php foreach ($location_list as $key => $value): ?>
													<option value="<?= $value['AD_ID'] ?>" <?= ($value['AD_ID'] == $header['CA_DefaultLocation'] ? 'selected' : '') ?>><?= $value['AD_Desc'] ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="padding-top: 10px">E-mail:</div>
										<div class="col-9">
											<input type="email" name="U_UserEmailAddress" tabindex="5" class="form-control" value="<?= $header['U_UserEmailAddress'] ?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="padding-top: 10px">Password:</div>
										<div class="col-9">
											<input type="password" minlength="8" maxlength="16" tabindex="6" name="U_Password" class="form-control" value="<?= $header['U_Password'] ?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="padding-top: 10px">Parent User:</div>
										<div class="col-9">
											<select name="U_ParentUserID" class="form-control" tabindex="7" style="width: 100%">
												<option value=""></option>
												<?php foreach ($user_list as $key => $value): ?>
													<option value="<?= $value['U_ID'] ?>" <?= ($value['U_ID'] == $header['U_ParentUserID'] ? 'selected' : '') ?>><?= $value['U_Username'] ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-3" style="padding-top: 10px">User Role:</div>
										<div class="col-9">
											<select name="UR_FK_RoleID[]" class="form-control" tabindex="8" multiple="multiple" style="width: 100%">
												<?php foreach ($role as $key => $value): ?>
													<option value="<?= $value['R_ID'] ?>" <?= (array_search($value['R_ID'], $header['UR_FK_RoleID']) !== false ? 'selected' : '') ?>><?= $value['R_Description'] ?></option>
												<?php endforeach ?>													
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-12">
								<?= generate_table($table_detail) ?>
							</div>

						</div>
					</div>

				</form>

			</div>
			<hr>
			<div>
				<button class="btn btn-warning btn-outline" data-toggle="tooltip" data-placement="bottom" title="Back" id="back" tabindex="9">
					<span class="fa fa-reply"></span>					
				</button>
				<?php if($type != 'view'): ?>
					<?php if ($type == 'add'): ?>
						<button  type="button" class="btn btn-success btn-outline save" data-doc-no="<?= $header['U_ID'] ?>" data-action="save-new" tabindex="10">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save & New
						</button>
					<?php endif ?>
					<button  type="button" class="btn btn-success btn-outline save" data-todo="<?= $type ?>" data-doc-no="<?= $header['U_ID'] ?>" data-action="save-close" tabindex="11">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;<?= ($type == 'add' ? 'Save & Close' : 'Update') ?>
					</button>
				<?php endif; ?>
			</div>
		</div>
	</div>


	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.js"></script>
	<!-- <script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script> -->
	<!-- <script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script> -->
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo EXTENSION ?>moment.js"></script>
    <script src="<?php echo EXTENSION ?>inspinia/js/plugins/clockpicker/clockpicker.js"></script>

	<!-- GLOBAL VARIABLE -->
	<script>
		var output_type 			= '<?= $type ?>';		
		var doc_no 					= '<?= $header['U_ID'] ?>';
		var update_link 			= '<?= './update?id='.md5($header['U_ID']) ?>';
		var cUserid 				= '<?= getCurrentUser()['login-user'] ?>';
		var cDefaultLoc 			= '<?= getDefaultLocation() ?>';
		var header_table 			= '<?= $header_table ?>';
		var header_doc_no_field 	= '<?= $header_doc_no_field ?>';
		var header_status_field 	= '<?= $header_status_field ?>';
		var number_series 			= '<?= $number_series ?>';
		var module_url 				= '<?= DOMAIN.'/administration/user_approval_setup/users' ?>';
		var module_folder 			= 'administration/user_approval_setup';
		var module_controller 		= 'Users';
		var module_name 			= 'Users';
		var isRequiredDetails 		= true;
		var location_list 			= <?= json_encode($location_list) ?>;
		var tabledetail;
		var action;
		var line_no;
	</script>

	<script src="<?php echo JS_DIR?>app/administration/user_approval_setup/users/form.js"></script>
	<script src="<?php echo JS_DIR?>app/rules/form_rules.js"></script>	

	</section>