<style>
tfoot {
	display: table-header-group;
}

@media screen and (max-width: 768px){
	#users_filter input{
		width: 60%!important;
	}
	#users_filter{
		margin-right: 500px!important;
	}
}
</style>
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Date Range Picker -->
<link href="<?php echo EXTENSION ?>inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<section class="wrapper wrapper-content">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="pull-left m-r-md">
                <a href="<?= DOMAIN.'dashboard' ?>" id="home">
                    <i class="fa fa-home" style="font-size:25px; margin-top: 7px"></i>
                </a>
            </div>
            <div class="pull-right">
                <a href="<?= DOMAIN.'asset_management/configuration' ?>" class="btn btn-outline btn-xs btn-warning" role="button" style="margin-top: 7px">
                    <i class="fa fa-reply"></i>
                </a>
            </div>
            <h2 style="font-weight: bold; padding-top: 1px; color: #2980b9!important" class="text-success">USERS</h2>
        </div>
		<div class="ibox-content">
			<div class="pull-right">
				<button class="btn btn-success" id="btnActive" disabled>
					<i class="fa fa-check"></i> Activate
				</button>
				<button class="btn btn-danger" id="btnDeactive" disabled>
					<i class="fa fa-times"></i> Deactivate
				</button>
			</div>
			<?= generate_table($table_hdr)?>
		</div>
	</div>
</section>

<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.js"></script>
<!-- <script src="<?php echo EXTENSION ?>inspinia/js/plugins/dataTables/datatables.min.js"></script> -->
<!-- <script src="<?= EXTENSION ?>inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script> -->
<!-- Sweet alert -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Moment -->
<script src="<?php echo EXTENSION ?>moment.js"></script>
<!-- Date Range Picker -->
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Page-Level Scripts -->
<script type="text/javascript">
	var tableindex;
	var col_center 			= <?= json_encode($hdr_center)?>;
	var table_id 			= '<?= key($table_hdr) ?>';	
	var process_action 		= 'Users';
	var table_name 			= 'tblUser';
	var controller_url 		= 'administration/user_approval_setup/users/';
</script>
<script src="<?php echo JS_DIR?>app/administration/user_approval_setup/users/index.js"></script>
<script src="<?php echo JS_DIR ?>app/rules/form_rules.js"></script>
<script src="<?php echo JS_DIR?>app/rules/advsearch.js"></script>