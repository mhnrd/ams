<style type="text/css">
.sweet-alert #remarks{
    display:block !important;
}
</style>

<style type="text/css">	.clockpicker-popover { z-index: 999999 !important;} div.form-group{margin-bottom: 5px;} #of-copy-detail-list_wrapper{padding-bottom: 0px!important}</style>
<div class="modal inmodal animated fadeInUp" id="track-document-modal" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-lg" style="margin-top: 10px;width:60%">
		<div class="modal-content<?php /* animated flipInY */ ?>">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px; color: black!important;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Document Tracking</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body" style="padding-bottom: 10px">
					<div class="table-responsive">
						<table class="table table-condensed table-striped table-bordered table-hover dataTables-of" id="track-document-list">
							<thead>
								<tr>
									<th class="text-center col-xs-1">Document No</th>
									<th class="text-center col-xs-2">Entry Date</th>
									<!-- <th class="text-center col-xs-1">Sender</th> -->
									<th class="text-center col-xs-2">Location</th>
									<th class="text-center col-xs-2">Approver Position</th>
									<th class="text-center col-xs-1">Approved ID</th>
									<th class="text-center col-xs-2">Date</th>
									<th class="text-center col-xs-1">Status</th>
									<th class="text-left col-xs-1">Remarks</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
		</div>
	</div>
</div>
<script src="<?php echo JS_DIR?>app/components/trackdocument/form.js"></script>

