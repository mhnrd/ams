<?php 
    /*<!--
        SG Standard ERP System
        Designed and Develop BY: 
            * JOSHUA
            * MICHAEL
    -->*/ 
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo IMG_DIR; ?>ytmi-login.png" type="image/x-icon">
        <link rel="icon" href="<?php echo IMG_DIR; ?>SG.png" type="image/x-icon">
        <link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/css/animate.css">
        <link href="<?php echo EXTENSION; ?>inspinia/css/plugins/codemirror/codemirror.css" rel="stylesheet">
        <link href="<?php echo EXTENSION; ?>inspinia/css/plugins/codemirror/ambiance.css" rel="stylesheet">        
        <!-- Toastr style -->
        <link href="<?php echo EXTENSION; ?>inspinia/css/plugins/toastr/toastr.min.css" rel="stylesheet">
        <script src="<?php echo EXTENSION; ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
        <link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/css/plugins/sweetalert/sweetalert.css">
        <script src="<?php echo EXTENSION; ?>inspinia/js/plugins/pace/pace.min.js"></script>
        <link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/css/style.css">
        <link rel="stylesheet" href="<?php echo CSS_DIR; ?>style.css"> 
        <title>Asset Management System</title>
    </head> 
    <body data-spy="scroll" data-target=".navbar-default" style="background: #DDD8E7" class="col-12">
		<div style="padding: 0!important;">
