<div class="row border-bottom">
	<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
		<div class="navbar-header">
			<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			<!-- <span class="m-r-sm text-muted welcome-message">PRODUCTION AND INVENTORY SYSTEM</span> -->
		</div>
		
		<ul class="nav navbar-top-links navbar-right">
			<li class="dropdown noti-num">
				<a style="color: black" class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell" data-toggle="tooltip" data-placement="bottom" title="Notifications"></i>
                    <?php //echo '<span class="label label-primary"></span>'; ?>
                </a>
                <ul class="dropdown-menu dropdown-alerts notif" style="padding:5px">
                    <li></li>
                </ul>
			</li>
			<li>
				<a style="color: black" href="#">
					<span data-toggle="tooltip" data-placement="bottom" title="Change Password" id="changepass"><i class="fa fa-key"></i></span>
				</a>
			</li>
			<!-- <li>
				<a target="_blank" href="<?php echo UPLOADS ?>User_manual.pdf" style="cursor: help">
					<span data-toggle="tooltip" data-placement="bottom" title="User Manual"><i class="fa fa-book"></i></span>
				</a>
			</li> -->
			<li>
				<a style="color: black" href="<?php echo DOMAIN ?>logout">
					<span><i class="fa fa-sign-out"></i> Log out</span>
				</a>
			</li>
		</ul>		
	</nav>
</div>