<?php if(!empty($_SESSION['alert'])){ ?>	
	<div class="alert alert-<?php echo $_SESSION['alert']['type']?> alert-dismissable dashboard-header">
	    <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="padding-right: 15px;">×</button>
	    <?php echo $_SESSION['alert']['message']?>
	</div>
<?php 
unset($_SESSION['alert']);
}?>	