<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="slimScrollBar" style="position: relative;overflow: hidden; width: auto; height: 100%;">
		<div class="sidebar-collapse" style="overflow: hidden; width: auto; height: 100%;">
			<div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
			<a class="close-canvas-menu"><i class="fa fa-times"></i></a>
			<ul class="nav metismenu" id="side-menu">
				
				<li class="nav-header">
					<div class="dropdown profile-element"> 
						
						<span>
							<img alt="image" class="img-circle img-responsive" src="<?php echo IMG_DIR.'SG.png' ?>" style="width: 48px;height: 48px">
						</span>
						
						<span class="clear"> 
							<span class="block m-t-xs"> 
								<strong class="font-bold" style="color:white;"><?php echo $this->session->userdata('current_user')['login-name']; ?></strong>
						 	</span> 
						 	<span class="text-muted text-xs block">
						 		<?= $this->session->userdata('current_user')['login-position-name'] ?>
						 	</span>
						 </span>
					</div>
				</li> 

				<!-- CREATED BY JOSHUA LIMBING -->
				
				<?php $tree_menu = getAllModulesList() ?>

				<?php foreach ($tree_menu as $key => $lvl2): ?>
					<li class="nav-first-level <?= ($this->uri->segment(2) == $lvl2['M_ControllerName'] ? 'active' : '') ?>">
						<a style="color: white" class="<?= (!empty($tree_menu[$key]['lvl3']) ? '' : 'url-change ') ?><?= (empty($tree_menu[$key]['lvl3']) && $this->uri->segment(2) == $lvl2['M_ControllerName'] ? 'active-module' : '') ?>" 
							href="<?= (!empty($tree_menu[$key]['lvl3']) ? 'javascript:void(0)' : ($lvl2['M_Trigger'] == 'ess' ? $lvl2['M_Trigger'] : DOMAIN.$lvl2['M_Trigger'])) ?>">
							<i class="<?= $lvl2['M_Icon'] ?>"></i> 
							<span class="nav-label "><?= $lvl2['M_DisplayName'] ?></span> 
							<?= (!empty($tree_menu[$key]['lvl3']) ? '<span class="fa arrow"></span>' : '') ?> 
						</a>
						<?php if (!empty($tree_menu[$key]['lvl3'])): ?>
							<ul class="nav nav-second-level">
								<?php foreach ($tree_menu[$key]['lvl3'] as $index => $lvl3): ?>
									<li <?= ($this->uri->segment(3) == $lvl3['M_ControllerName'] ? 'class="active"' : '') ?>>
										<a style="color: white" class="<?= (!empty($tree_menu[$key]['lvl3'][$index]['lvl4']) ? '' : 'url-change ') ?><?= (empty($tree_menu[$key]['lvl3'][$index]['lvl4']) && $this->uri->segment(3) == $lvl3['M_ControllerName'] ? 'active-module' : '') ?>" 
											href="<?= (!empty($tree_menu[$key]['lvl3'][$index]['lvl4']) ? 'javascript:void(0)' : DOMAIN.$lvl3['M_Trigger']) ?>">
											<i class="<?= $lvl3['M_Icon'] ?>"></i> 
											<span class="nav-label "><?= $lvl3['M_DisplayName'] ?></span> 
											<?= (!empty($tree_menu[$key]['lvl3'][$index]['lvl4']) ? '<span class="fa arrow"></span>' : '') ?> 
										</a>
										<?php if (!empty($tree_menu[$key]['lvl3'][$index]['lvl4'])): ?>
											<ul class="nav nav-third-level">
												<?php foreach ($tree_menu[$key]['lvl3'][$index]['lvl4'] as $index_no => $lvl4): ?>
													<li <?= ($this->uri->segment(4) == $lvl4['M_ControllerName'] ? 'class="active"' : '') ?>>
														<a style="color: white" href="<?= DOMAIN.$lvl4['M_Trigger'] ?>" 
															class="url-change <?= ($this->uri->segment(4) == $lvl4['M_ControllerName'] ? 'active-module' : '') ?>">
															<i class="<?= $lvl4['M_Icon'] ?>"></i> 
															<span class="nav-label "><?= $lvl4['M_DisplayName'] ?></span>  
														</a>
													</li>
												<?php endforeach ?>
											</ul>
										<?php endif ?>
									</li>
								<?php endforeach ?>
							</ul>
						<?php endif ?>
					</li>
				<?php endforeach ?>

			</ul>
		</div>
	</div>
	
</nav>
