<!-- Mother Modules -->
<div class="row wrapper white-bg page-heading" style="padding-bottom: 5px!important; padding-top: 5px!important">
    
    <ul class="nav nav-pills nav-justified mod-list" style="display: table!important; width: 100%!important; text-align: center">
        <li style="display: table-cell;" class="mother-mod<?= ($this->uri->segment(2) == 'dashboard' ? ' active-parent-mod' : '')  ?>">
            <a href="<?= DOMAIN.'user/dashboard' ?>" style="color: black">
                <i class="fa fa-dashboard"></i>&nbsp;Dashboard
            </a>
        </li>
        <?php foreach (get_mother_modules() as $key => $value): ?>
            <?php if ($key == 3 || ($key + 1) % 4 == 0): ?>
                </ul>
                <ul class="nav nav-pills nav-justified mod-list" style="display: table!important; width: 100%!important; text-align: center">
                    <li style="display: table-cell;" class="mother-mod<?= ($this->uri->segment(1) == $value['M_ControllerName'] ? ' active-parent-mod' : '')  ?>">
                        <a href="<?= DOMAIN.$value['M_Trigger'] ?>">
                            <i class="<?= $value['M_Icon'] ?>"></i>&nbsp;<?= $value['M_DisplayName'] ?>
                        </a>
                    </li>
            <?php else: ?>
                <li style="display: table-cell;" class="mother-mod<?= ($this->uri->segment(1) == $value['M_ControllerName'] ? ' active-parent-mod' : '')  ?>">
                    <a href="<?= DOMAIN.$value['M_Trigger'] ?>">
                        <i class="<?= $value['M_Icon'] ?>"></i>&nbsp;<?= $value['M_DisplayName'] ?>
                    </a>
                </li>
            <?php endif ?>
        <?php endforeach; ?>
    </ul>
   
    <div class="col-sm-12">
        <hr style="margin: 5px 0px 5px 0px">
        <!-- <h3>SG Enterprise Resource Planning System</h3> -->
        <?php if(!empty($breadcrumb)): ?>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo DOMAIN ?>" class="url-change">Home</a></li>
                <?php foreach($breadcrumb as $crumb) {
                    if ($crumb !== end($breadcrumb)){?>
                    <li class="breadcrumb-item">
                        <?php if(!empty($crumb['url'])){?>
                        <a style="color: black!important" href="<?php echo DOMAIN.$crumb['url'] ?>" class="url-change"><?php echo $crumb['html'] ?></a>
                        <?php } 
                        else{ echo $crumb['html']; }?>
                    </li>
                <?php } else { ?>
                <li class="breadcrumb-item active">
                    <strong style="color: black!important"><?php echo $crumb['html'] ?></strong>
                </li>
                <?php } }?>
            </ol>
        <?php endif; ?>
    </div>
</div>
<!-- END -->
<?php /* if(!empty($breadcrumb)): ?>
<div class="row wrapper white-bg page-heading" style="padding-bottom: 5px; padding-top: 0px!important; margin-top: 5px">
    <div class="col-sm-12">
        <h3>SG Enterprise Resource Planning System</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo DOMAIN ?>" class="url-change">Home</a></li>
            <?php foreach($breadcrumb as $crumb) {
                if ($crumb !== end($breadcrumb)){?>
                <li class="breadcrumb-item">
                    <?php if(!empty($crumb['url'])){?>
                    <a href="<?php echo DOMAIN.$crumb['url'] ?>" class="url-change"><?php echo $crumb['html'] ?></a>
                    <?php } 
                    else{ echo $crumb['html']; }?>
                </li>
            <?php } else { ?>
            <li class="breadcrumb-item active">
                <strong><?php echo $crumb['html'] ?></strong>
            </li>
            <?php } }?>
        </ol>
    </div>
</div>
<?php endif; */ ?>


