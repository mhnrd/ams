<script>
	const global = {
		site_name : '<?php echo DOMAIN ?>'
	};
</script>
<script src="<?php echo JS_DIR?>lib/ux.js"></script>
<script src="<?php echo EXTENSION; ?>inspinia/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo EXTENSION; ?>inspinia/js/popper.min.js"></script>
<script src="<?php echo EXTENSION; ?>inspinia/js/bootstrap.min.js"></script>
<script src="<?php echo EXTENSION; ?>inspinia/js/inspinia.js"></script>
<script src="<?php echo EXTENSION; ?>inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo EXTENSION; ?>inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo JS_DIR; ?>lib/ref.js"></script>
<script src="<?php echo JS_DIR; ?>lib/app.js"></script>
<script src="<?php echo JS_DIR; ?>app/rules/input_filter.js"></script>
<script src="<?php echo EXTENSION ?>inspinia/js/plugins/validate/jquery.validate.min.js"></script>
<script src="<?= EXTENSION ?>inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="<?php echo JS_DIR; ?>app/rules/save.js"></script>
<script src="<?php echo EXTENSION; ?>inspinia/js/plugins/toastr/toastr.min.js"></script>
<script src="<?php echo EXTENSION; ?>print.min.js"></script>
