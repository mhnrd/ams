<div class="modal inmodal animated fadeInDown" id="change-pass-modal" data-backdrop="static"  tabindex="-1" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-md" style="margin-top: 100px">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #17B294; color:white; padding: 15px 0 15px 0">
					<button type="button" class="close" data-dismiss="modal" style="margin-right: 15px; color: black!important;">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4>Change Password</h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body" style="padding-bottom: 10px">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="m-t" role="form" action="index.html">
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Old Password" required name="oldpassword" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="New Password" required name="newpassword" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Confirm New Password" required name="confirmpassword" autocomplete="off">
                                </div>
                                <button type="submit" id="btnChangePass" class="btn btn-primary block full-width m-b"><span class="fa fa-key"></span> Set new password</button>
                            </form>
                        </div>
                    </div>
				</div>
		</div>
	</div>
</div>