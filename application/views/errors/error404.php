<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo IMG_DIR; ?>TTC.png" type="image/x-icon">
        <link rel="icon" href="<?php echo IMG_DIR; ?>TTC.png" type="image/x-icon">
        <title>Page 404 Error</title>
        <link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/css/animate.css">
        <link href="<?php echo EXTENSION; ?>inspinia/css/plugins/codemirror/codemirror.css" rel="stylesheet">
        <link href="<?php echo EXTENSION; ?>inspinia/css/plugins/codemirror/ambiance.css" rel="stylesheet">        
        <link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/css/style.css">
        <link rel="stylesheet" href="<?php echo CSS_DIR; ?>style.css"> 

        <!-- Toastr style -->
        <link href="<?php echo EXTENSION; ?>inspinia/css/plugins/toastr/toastr.min.css" rel="stylesheet">
        <script src="<?php echo EXTENSION; ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
        <link rel="stylesheet" href="<?php echo EXTENSION; ?>inspinia/css/plugins/sweetalert/sweetalert.css">
    </head>
    <body class="gray-bg">
        <div class="middle-box text-center animated fadeInDown">
            <h1>404</h1>
            <h3 class="font-bold">Page Not Found</h3>

            <div class="error-desc">
                Sorry, but the page you are looking for has not been found. Please contact your System Administrator, then click the dashboard button to go back.
                <br><br><a class="btn btn-primary" href="<?= DOMAIN ?>user/dashboard">Back to Dashboard</a>
            </div>
        </div>
        <!-- Mainly scripts -->
        <script src="<?php echo JS_DIR?>lib/ux.js"></script>
        <script src="<?php echo EXTENSION; ?>inspinia/js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo EXTENSION; ?>inspinia/js/bootstrap.min.js"></script>
        <script src="<?php echo EXTENSION; ?>inspinia/js/inspinia.js"></script>
        <script src="<?php echo EXTENSION; ?>inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="<?php echo EXTENSION; ?>inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo JS_DIR; ?>lib/ref.js"></script>
        <script src="<?php echo JS_DIR; ?>lib/app.js"></script>

        <script src="<?php echo JS_DIR; ?>lib/app.js"></script>
        <script src="<?php echo EXTENSION; ?>inspinia/js/plugins/toastr/toastr.min.js"></script>


    </body>
</html>
