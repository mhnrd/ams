<!doctype html>
<html lang="en">
	<head>
		<link rel="stylesheet" href="<?php echo DOMAIN; ?>assets/third_party/inspinia/css/bootstrap.min.css">
		<link rel="icon" href="<?php echo IMG_DIR; ?>stargate-logo.ico" type="image/x-icon">
	</head>
	<body>
<?php echo $content; ?>
	</body>
</html>