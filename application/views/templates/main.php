<?php 
$this->load->view('/components/header');
$this->load->view('/components/javascriptlist');

// if($this->uri->segment(2) != 'dashboard'){
// 	$this->load->view('/components/mainleftnav');
// }

?>
<style type="text/css">
	@media screen and (max-width:768px) { 
		.ibox-content, .dataTables_wrapper  {
			padding-left: 0px!important;
			padding-right: 0px!important;
		}
		h1{
			font-size: 22px!important;
		}
	}
		h2{
			font-size: 22px!important;
		}
	}

</style>
<div >
	<?php
		// $this->load->view('/components/headingpage', $breadcrumb);
		$this->load->view('/components/changepass');
	?>
	<div class="row">
		<div class="col-md-8 offset-md-2 col-12 animated fadeInRight">
		<?php
		echo $content;
		?>
		</div>
	</div>
</div>
<script src="<?php echo EXTENSION?>jquerymd5.js"></script>

<?php
$this->load->view('/components/footer');