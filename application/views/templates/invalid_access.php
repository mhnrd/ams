<script data-main="<?php echo JS_DIR; ?>app/home.js" src="<?php echo JS_DIR; ?>app/lib/require.js"></script>
<div class="alert alert-danger">
	<strong>STOP! </strong>You are not authorized to access this page!
	<a href="<?php echo DOMAIN; ?>user/dashboard" class="btn btn-xs btn-success pull-right">Go Back</a>
	
</div>