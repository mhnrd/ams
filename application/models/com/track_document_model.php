<?php

/*

    Author       : Jekzel Leonidas
    Date Created : 01/29/2019 9:30:00
    base on SG Standard Document Approval

*/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class track_document_model extends MAIN_Model {

    protected $_table = '';

    private $header_table           = '';
    private $header_doc_no_field    = '';
    private $header_status_field    = '';
    private $order_column = array('DT_DocNo','DT_EntryDate','DT_Sender','SP_StoreName','P_Position', 'DT_ApprovedBy','DT_DateApproved','DT_Status','DT_Remarks');

    public function __construct() {
        parent::__construct();
    }

    // Document Approval Setup
    public function process($action,$numberSeriesID,
                            $DocumentNo,
                            $DocumentDate = null,
                            $Amount = 0,
                            $SenderID,
                            $SenderLocation,
                            $header_table,
                            $header_doc_no_field,
                            $header_status_field,
                            $ns_id,
                            $target_url,
                            $Remarks = ''){
        $output = '';

        $this->header_table         = $header_table;
        $this->header_doc_no_field  = $header_doc_no_field;
        $this->header_status_field  = $header_status_field;

        switch ($action) {
            case 'sendrequest': 
                $output = $this->sendrequest($numberSeriesID,$DocumentNo,$DocumentDate,$Amount,$SenderID,$SenderLocation,$ns_id,$target_url);          
                break;
            case 'approve':
                $output = $this->approve($numberSeriesID,$DocumentNo,$Amount,$SenderID);              
                break;
            case 'reject':                
                $output = $this->reject($numberSeriesID,$DocumentNo,$SenderID,$Remarks);
                break;
            case 'reopen':                
                $output = $this->reopen($numberSeriesID,$DocumentNo,$DocumentDate,$SenderID,$SenderLocation,$Remarks);
                break;
            case 'cancelrequest':              
                $output = $this->cancelrequest($numberSeriesID,$DocumentNo,$SenderID);  
                break;
            case 'cancel':                
                $output = $this->cancel($numberSeriesID,$DocumentNo,$DocumentDate,$SenderID,$SenderLocation);
                break;
            default:
                break;
        }

        // Additional by Jekzel Leonidas - for Order Forecast
        if($this->header_table == 'tblOrderForecast'){
            $sql = 'UPDATE '.$this->header_table.' 
                SET OF_DateRequested = \''.date_format(date_create(),'m/d/Y').'\'
                WHERE '.$this->header_doc_no_field.' = \''.$DocumentNo.'\' ';            
            $this->db->query($sql);
        }

        // Additional by Jekzel Leonidas - for Bill of Material
        if($this->header_table == 'tblBOM' && $output == 'Approved'){
            $sql = 'SELECT *
                    FROM tblBOM
                    WHERE BOM_ItemNo IN (SELECT BOM_ItemNo FROM tblBOM WHERE (BOM_DocNo = \''.$DocumentNo.'\'))
                            AND BOM_Status IN (\'Approved\',\'Active\')';
            $result = $this->db->query($sql)->num_rows();
            if($result == 0){
                $output = 'Active';
            }
        }

        // Additional by Jekzel Leonidas - for Material Request Plan
        if($this->header_table == 'tblMaterialRequestPlan' && $output == 'Cancelled'){
            $sql = 'UPDATE tblManufacturingOrder
                    SET MO_MRP_DocNo = NULL
                    WHERE (MO_MRP_DocNo =  \''.$DocumentNo.'\')';
            $this->db->query($sql);
        }

        $sql = 'UPDATE '.$this->header_table.' 
                SET '.$this->header_status_field.' = \''.$output.'\'
                WHERE '.$this->header_doc_no_field.' = \''.$DocumentNo.'\' ';
        $result = $this->db->query($sql);
        if($result){
            return $output = array('success'=>'1');
        }
        else{
            return $output = array('success'=>'0');
        }


        return $output;

    }

    private function sendrequest($NS_ID,$DocNo,$DocDate,$Amount,$cUserID,$cUserLoc,$numseriesID,$target_url){

        $gResponse = '';

        // get latest DT_Entry No
        $sql_query = 'SELECT TOP 1 ISNULL(DT_EntryNo,0) AS DT_EntryNo FROM tblDocTracking ORDER BY DT_EntryNo DESC';
        $nEntryNo = $this->db->query($sql_query)->row_array()['DT_EntryNo'];
        $nEntryNo = ($nEntryNo > 0) ? $nEntryNo + 1 : 1 ;

        // get all approver
        $sql = 'SELECT AS_FK_NS_id, 
                      AS_FK_Position_id AS Approver, 
                      ISNULL(AS_Unlimited,0) as AS_Unlimited , 
                      ISNULL(AS_Required,0) as AS_Required, 
                      AS_Amount, 
                      AS_Sequence, 
                      0 AS Viewed 
                FROM tblApprovalSetup 
                WHERE (AS_FK_NS_id = \''.$numseriesID['NS_Id'].'\') ORDER BY AS_Sequence';
        $result = $this->db->query($sql); 

        if($result->num_rows() > 0 ):
            foreach ($result->result_array() as $row):
                
                // Set default approver a standard
                $cSenderApprover = $row['Approver'];
                $blnIncludeApprover = true;

                // Perform discrete Business Rules for Supervisor, Managers & Directors. Consider approval based on position
                $sql = 'SELECT P_Id, P_Parent, ISNULL(P_Type,\'\') as P_Type 
                        FROM tblPosition 
                        WHERE (P_Id = \''.$cSenderApprover.'\')';
                $resultApprover = $this->db->query($sql);



                if($resultApprover->num_rows() > 0):
                    if($resultApprover->row_array()['P_Type'] == ""):

                        //Use sender position approval setup
                        $sql = 'SELECT u.U_ID, u.U_FK_Position_id, isnull(p.P_Parent,\'\') as P_Parent 
                                FROM tblUser u 
                                    LEFT OUTER JOIN tblPosition p ON u.U_FK_Position_id = p.P_Id AND
                                        u.U_FK_Position_id = p.P_Id 
                                WHERE (u.U_ID = \''.$cUserID.'\')';  
                        $resultSendApprover = $this->db->query($sql);                        

                        if($resultSendApprover->num_rows() > 0){
                            //Replace default approver by sender supervisor/manager
                            $cSenderApprover = $resultSendApprover->row_array()['P_Parent'];
                        }

                    endif;
                endif;

                // Do not include Approver if the amount to approve is below his/her approval amount but if the approver is setup to required,
                // include the approver
                if($Amount < $row['AS_Amount']):
                    $blnIncludeApprover = false;
                    if($row['AS_Required'] == true || $row['AS_Required'] == 1):
                        $blnIncludeApprover = true;
                    endif;
                endif;

                //Do not include approver whose position is also the sender
                $sql = 'SELECT U_ID, U_FK_Position_id 
                        FROM tblUser 
                        WHERE (U_ID = \''.$cUserID.'\')';
                $resultValidationPosition = $this->db->query($sql);
                if($resultValidationPosition->num_rows() > 0):
                    if($resultValidationPosition->row_array()['U_FK_Position_id'] == $cSenderApprover):
                        if($result->num_rows() > 1): // More than 1 approver position
                            $blnIncludeApprover = true; //Include user who also the sender and also the approver
                        endif;
                    endif;
                    $cSenderPosition = $resultValidationPosition->row_array()['U_FK_Position_id'];
                endif;
                

                if($blnIncludeApprover):
                    $sql = 'INSERT INTO tblDocTracking
                                       (DT_EntryNo, DT_FK_NSCode, DT_DocNo, DT_DocDate, DT_Sender, DT_Approver, DT_TargetURL, DT_Status,
                                        DT_Location, DT_EntryDate, DT_ApprovedBy, DT_DateApproved, DT_Unlimited, DT_Required, DT_Amount,
                                        DT_Remarks, DT_Viewed)
                             VALUES ('.(($nEntryNo>1) ? $nEntryNo : 1).', \''.$NS_ID.'\',\''.$DocNo.'\',\''.date_format(date_create(), 'm/d/Y h:i:s').'\',
                                        \''.$cSenderPosition.'\',\''.$cSenderApprover.'\',\''.$target_url.'\',';
                
                    if($cSenderPosition == $cSenderApprover):
                        $sql .= ' \'Skipped\',\''.$cUserLoc.'\',\''.date_format(date_create(), 'm/d/Y h:i:s').'\',NULL,NULL,
                                    \''.($row['AS_Unlimited'] ? 1 : 0).'\',
                                    \''.($row['AS_Required'] ? 1 : 0).'\',
                                    \''.$row['AS_Amount'].'\',\'\',0)';
                    else:
                        $sql .= ' \'Pending\',\''.$cUserLoc.'\',\''.date_format(date_create(), 'm/d/Y h:i:s').'\',NULL,NULL,
                                    \''.($row['AS_Unlimited'] ? 1 : 0).'\',
                                    \''.($row['AS_Required'] ? 1 : 0).'\',
                                    \''.$row['AS_Amount'].'\',\'\',0)';
                    endif;

                    $this->db->query($sql);

                    if($result->num_rows() == 1):
                        if($cSenderPosition == $cSenderApprover):
                            $gResponse = "Approved";
                        else:
                            $gResponse = "Pending";
                        endif;
                    else:
                        $gResponse = "Pending";
                    endif;

                endif;

                $nEntryNo += 1;

            endforeach;
        
        elseif($result->num_rows() == 0):
            $gResponse = 'Approved';
        endif;

        return $gResponse;
    }

    private function approve($NS_ID,$DocNo,$Amount){

        $gResponse = 'Pending';
        $gApprove = "Pending";

        $sql = 'SELECT dt.DT_FK_NSCode, dt.DT_DocNo, dt.DT_Approver, dt.DT_EntryNo, dt.DT_Unlimited , dt.DT_Sender 
                 FROM tblDocTracking dt INNER JOIN tblUser u ON dt.DT_Approver = u.U_FK_Position_id 
                 WHERE (u.U_ID = \''.getCurrentUser()['login-user'].'\') 
                        AND (dt.DT_Status = \'Pending\') 
                        AND (dt.DT_DocNo = \''.$DocNo.'\') 
                        AND (dt.DT_FK_NSCode = \''.$NS_ID.'\') 
                 ORDER BY dt.DT_EntryNo DESC ';
        $result = $this->db->query($sql);                
        if($result->num_rows() > 0){

            $gPositionID = $result->row_array()['DT_Sender'];
            $sql = 'UPDATE tblDocTracking  
                    SET DT_ApprovedBy = \''.getCurrentUser()['login-user'].'\'
                      , DT_DateApproved = GETDATE() 
                      , DT_Status = \'Approved\'  
                    WHERE (DT_EntryNo =  \''.$result->row_array()['DT_EntryNo'].'\' )';
            $this->db->query($sql);

            if($result->row_array()['DT_Unlimited'] = true || $result->row_array()['DT_Unlimited'] = 1){
                $sql = 'UPDATE tblDocTracking 
                        SET DT_ApprovedBy = \''.getCurrentUser()['login-user'].'\'
                            , DT_DateApproved = GETDATE() , DT_Status = \'Skipped\' 
                        WHERE (DT_EntryNo < \''.$result->row_array()['DT_EntryNo'].'\') 
                            AND (DT_Required = 0) 
                            AND (DT_Status = \'Pending\') 
                            AND (DT_DocNo = \''.$DocNo.'\') 
                            AND (DT_FK_NSCode =\''.$NS_ID.'\')';
                $this->db->query($sql);
            }
            
            $gApprove = "Approved";
        }
        else{
            $gApprove = "Pending";
        }

        //Check if document was approved by all approver
        $sql = 'SELECT DT_FK_NSCode, DT_DocNo, DT_Approver, DT_EntryNo, DT_Unlimited, DT_Status 
                FROM tblDocTracking dt 
                WHERE (DT_DocNo = \''.$DocNo.'\') 
                    AND (DT_FK_NSCode = \''.$NS_ID.'\') 
                ORDER BY DT_EntryNo DESC';
        $result = $this->db->query($sql);

        if($result->num_rows() > 1){
            $approvers = $result->result_array();
            foreach($approvers as $approver){
                if($approver['DT_Status'] == 'Pending'){
                    $gApprove = "Pending";
                    $gResponse = "Pending";
                    break;
                }
            }

            if($gApprove != 'Pending'){
                $gApprove = "Approved";
                $gResponse = "Approved";
            }
        }
        else{
            if($result->num_rows() == 1){
                if($result->row_array()['DT_Status'] == 'Approved'){
                    $gResponse = "Approved";
                }
                else{
                    $gResponse = "Pending";   
                }        
            }
        }           

        return $gResponse;
    }

    private function reject($NS_ID,$DocNo,$SenderID,$Remarks){

        $gResponse = 'Pending';

        $sql = 'SELECT dt.DT_FK_NSCode, dt.DT_DocNo, dt.DT_Approver, dt.DT_EntryNo, dt.DT_Unlimited, dt.DT_Sender 
                FROM tblDocTracking dt INNER JOIN tblUser u ON dt.DT_Approver = u.U_FK_Position_id 
                WHERE (u.U_ID = \''.getCurrentUser()['login-user'].'\') 
                    AND (dt.DT_Status = \'Pending\') 
                    AND (dt.DT_DocNo = \''.$DocNo.'\') 
                    AND (dt.DT_FK_NSCode = \''.$NS_ID.'\') 
                ORDER BY dt.DT_EntryNo DESC';
        $result = $this->db->query($sql);

        if($result->num_rows() > 0){

            $data = $result->row_array();

            $gPositionID = $data['DT_Sender'];
            $sql = 'UPDATE tblDocTracking 
                    SET   DT_Remarks= \''.$Remarks.'\'
                        , DT_ApprovedBy = \''.getCurrentUser()['login-user'].'\'
                        , DT_DateApproved = GETDATE() 
                        , DT_Status =\'Rejected\' 
                    WHERE (DT_EntryNo = '.$data['DT_EntryNo'].') ';
            $this->db->query($sql);

            if($data['DT_Unlimited'] == true || $data['DT_Unlimited'] == 1){
                $sql = 'UPDATE tblDocTracking
                        SET DT_ApprovedBy = \''.getCurrentUser()['login-user'].'\'
                          , DT_DateApproved = GETDATE() 
                          , DT_Status = \'Skipped\'
                        WHERE (DT_EntryNo < '.$data['DT_EntryNo'].' ) 
                            AND (DT_Required = 0) 
                            AND (DT_Status = \'Pending\') 
                            AND (DT_DocNo = \''.$DocNo.'\') 
                            AND (DT_FK_NSCode = \''.$NS_ID.'\')';
                $this->db->query($sql);
            }

            $sql = 'UPDATE tblDocTracking
                    SET DT_Remarks=\''.$Remarks.'\'
                       ,DT_ApprovedBy = \''.getCurrentUser()['login-user'].'\'
                       , DT_DateApproved = GETDATE() 
                       , DT_Status = \'Skipped\' 
                    WHERE (DT_EntryNo > '.$data['DT_EntryNo'].') 
                        AND (DT_Status = \'Pending\') 
                        AND (DT_DocNo = \''.$DocNo.'\') AND (DT_FK_NSCode =\''.$NS_ID.'\')';
            $this->db->query($sql);


            $gResponse = "Open";
        }

        return $gResponse;   
    }

    private function reopen($NS_ID,$DocNo,$DocDate,$cUserID,$cUserLoc,$remarks){
        $gResponse = 'Open';

        //Use sender position approval setup
        $sql = 'SELECT u.U_ID, u.U_FK_Position_id, isnull(p.P_Parent,\'\') as P_Parent 
                FROM tblUser u 
                    LEFT OUTER JOIN tblPosition p ON u.U_FK_Position_id = p.P_Id AND
                        u.U_FK_Position_id = p.P_Id 
                WHERE (u.U_ID = \''.$cUserID.'\')';  
        $resultReOpen = $this->db->query($sql)->row_array();

        // get latest DT_Entry No
        $sql_query = 'SELECT TOP 1 ISNULL(DT_EntryNo,0) AS DT_EntryNo FROM tblDocTracking ORDER BY DT_EntryNo DESC';
        $nEntryNo = $this->db->query($sql_query)->row_array()['DT_EntryNo'];
        $nEntryNo = ($nEntryNo > 0) ? $nEntryNo + 1 : 1 ;

        $insert_array = array(
                                'DT_EntryNo'            => $nEntryNo, 
                                'DT_FK_NSCode'          => $NS_ID, 
                                'DT_DocNo'              => $DocNo,  
                                'DT_DocDate'            => $DocDate,  
                                'DT_Sender'             => $resultReOpen['U_FK_Position_id'],  
                                'DT_Approver'           => $resultReOpen['U_FK_Position_id'],  
                                'DT_Status'             => 'Re-Opened', 
                                'DT_Location'           => $cUserLoc,  
                                'DT_EntryDate'          => date_format(date_create(),'m/d/y h:i:s'),   
                                'DT_ApprovedBy'         => NULL,   
                                'DT_DateApproved'       => NULL,   
                                'DT_Unlimited'          => 0,   
                                'DT_Required'           => 0,   
                                'DT_Amount'             => 0,  
                                'DT_Remarks'            => $remarks,   
                                'DT_Viewed'             => 0                              
                            );

        $this->db->insert('tblDocTracking',$insert_array);
        return $gResponse;
    }

    private function cancelrequest($NS_ID,$DocNo,$cUserID){

    }

    private function cancel($NS_ID,$DocNo,$DocDate,$cUserID,$cUserLoc){

        //Use sender position approval setup
        $sql = 'SELECT u.U_ID, u.U_FK_Position_id, isnull(p.P_Parent,\'\') as P_Parent 
                FROM tblUser u 
                    LEFT OUTER JOIN tblPosition p ON u.U_FK_Position_id = p.P_Id AND
                        u.U_FK_Position_id = p.P_Id 
                WHERE (u.U_ID = \''.$cUserID.'\')';  
        $resultReOpen = $this->db->query($sql)->row_array();

        // get latest DT_Entry No
        $sql_query = 'SELECT TOP 1 ISNULL(DT_EntryNo,0) AS DT_EntryNo FROM tblDocTracking ORDER BY DT_EntryNo DESC';
        $nEntryNo = $this->db->query($sql_query)->row_array()['DT_EntryNo'];
        $nEntryNo = ($nEntryNo > 0) ? $nEntryNo + 1 : 1 ;

        $insert_array = array(
                                'DT_EntryNo'            => $nEntryNo, 
                                'DT_FK_NSCode'          => $NS_ID, 
                                'DT_DocNo'              => $DocNo,  
                                'DT_DocDate'            => $DocDate,  
                                'DT_Sender'             => $resultReOpen['U_FK_Position_id'],  
                                'DT_Approver'           => $resultReOpen['U_FK_Position_id'],  
                                'DT_Status'             => 'Cancelled', 
                                'DT_Location'           => $cUserLoc,  
                                'DT_EntryDate'          => date_format(date_create(),'m/d/y h:i:s'),   
                                'DT_ApprovedBy'         => NULL,   
                                'DT_DateApproved'       => NULL,   
                                'DT_Unlimited'          => 0,   
                                'DT_Required'           => 0,   
                                'DT_Amount'             => 0,  
                                'DT_Remarks'            => '',   
                                'DT_Viewed'             => 0                              
                            );

        $this->db->insert('tblDocTracking',$insert_array);        

        return 'Cancelled';
    }

    public function getAll($docNo){
        $sql = 'SELECT dt.DT_DocNo, dt.DT_EntryDate, dt.DT_Sender, dt.DT_Location, p.P_Position, dt.DT_ApprovedBy, dt.DT_DateApproved,
                   dt.DT_Status , dt.DT_Remarks
                FROM tblDocTracking dt 
                    LEFT OUTER JOIN tblPosition p ON dt.DT_Approver = p.P_Id
                WHERE (dt.DT_DocNo = \''.$docNo.'\')
                ORDER BY dt.DT_DocNo';
        
        $result = $this->db->query($sql)->result_array();

        if(empty($result)){
            $output['rows_data'] = 0;
            $output['rows_count'] = 0;
        }
        else{
            $output['rows_data'] = $result;
            $output['rows_count'] = $this->db->query($sql)->num_rows();
        }

        return $output;
    }

    public function getNSId($module_id, $location){
        $query_string = 'SELECT        NS_Id
                         FROM            tblNoSeries
                         WHERE        NS_FK_Module_id = \''.$module_id.'\' AND NS_Location = \''.$location.'\'';

        $result = $this->db->query($query_string)->row_array();

        return $result;
    }

    /* datatable query starts here */
    public function table_data($document_no){        
        $this->db->select('DT_DocNo, DT_EntryDate, DT_Sender, DT_Location, P_Position, DT_ApprovedBy, DT_DateApproved,
                   DT_Status , DT_Remarks, SP_StoreName');
        $this->db->join('tblPosition','DT_Approver = P_Id','left');
        $this->db->join('tblStoreProfile','DT_Location = SP_ID','left');
        $this->db->where('DT_DocNo',$document_no);
        $this->db->from('tblDocTracking');  
        $result_count = $this->db->count_all_results(); 

        $output['data']              = $this->datatables($document_no);
        $output['num_rows']          = $this->get_filtered_data($document_no);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query($document_no = null){
        $this->db->select('DT_DocNo, DT_EntryDate, DT_Sender, DT_Location, P_Position, DT_ApprovedBy, DT_DateApproved,
                   DT_Status , DT_Remarks, SP_StoreName');        

        if(isset($_POST["search"]["value"])){
            if($_POST["search"]["value"] != ''){  
                $this->db->like("DT_DocNo", $_POST["search"]["value"]);  
                $this->db->or_like("DT_Sender", $_POST["search"]["value"]);  
                $this->db->or_like("SP_StoreName", $_POST["search"]["value"]);
                $this->db->or_like("P_Position", $_POST["search"]["value"]);
                $this->db->or_like("DT_ApprovedBy", $_POST["search"]["value"]);
                $this->db->or_like("DT_Status", $_POST["search"]["value"]);
                $this->db->or_like("DT_Remarks", $_POST["search"]["value"]);
            }              
        }
        
      
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('DT_DocNo', 'DESC');  
        }
    }

    public function datatables($document_no = null){
        $this->query($document_no); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $this->db->join('tblPosition','DT_Approver = P_Id','left');
        $this->db->join('tblStoreProfile','DT_Location = SP_ID','left');
        $this->db->where('DT_DocNo',$document_no);
        $query = $this->db->get('tblDocTracking');
        return $query->result_array();
    }

    public function get_filtered_data($document_no = null){  
           $this->query($document_no);
           $this->db->join('tblPosition','DT_Approver = P_Id','left');
           $this->db->join('tblStoreProfile','DT_Location = SP_ID','left');
            $this->db->where('DT_DocNo',$document_no);
           $query = $this->db->get('tblDocTracking');  
           return $query->num_rows();  
    }

    /* datatable query ends here */

}
