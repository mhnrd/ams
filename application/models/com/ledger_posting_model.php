<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ledger_posting_model extends MAIN_Model {
    
    public function __construct() {
        parent::__construct();
    }

    public function GLPosting($action , $documentNo){

        $output = array(
                        'success' => 0,
                        'message' => 'No Function Found'
                            );
        
        switch ($action){
            case "PO_RECEIVE":
                $output = $this->PostToReceive($documentNo);
                break;

            default:
                break;
                
        }

        return $output;

    }

    public function CreateBatchTracking($batchType,$documentNo,$documentDate,$supplier){
        
        $this->load->model('master_file/bom/noseries_model');
        
        $Batch_DocNo = '';
        $Lot_DocNo = $this->noseries_model->getNextAvailableNumber("10003", false, "", true);

        if($batchType == 'Production'){
            $Batch_DocNo = $this->noseries_model->getNextAvailableNumber("10001", true, "", true);
        }
        else if($batchType == 'Receiving'){
            $Batch_DocNo = $this->noseries_model->getNextAvailableNumber("10002", true, "", true);
        }
        
        // Validation No Series
        if($Batch_DocNo == ''){
            throw new Exception("Please Setup Batch Tracking Series, Contact your administrator.");
        }

        if($Lot_DocNo == ''){
            throw new Exception("Please Setup Batch Tracking Series, Contact your administrator.");
        }

        $insert_fields = array('BT_BatchID'         =>  $Batch_DocNo
                                , 'BT_BatchType'    =>  $batchType
                                , 'BT_Supplier'     =>  $supplier
                                , 'BT_RedNo'        =>  $documentNo
                                , 'BT_RefDate'      =>  date_format(date_create($documentDate),'m/d/Y')
                                , 'CreatedBy'       =>  getCurrentUser()['login-user']
                                , 'DateCreated'     =>  date_format(date_create($documentDate),'m/d/Y')
                                , 'ModifiedBy'      =>  getCurrentUser()['login-user']
                                , 'DateModified'    =>  date_format(date_create($documentDate),'m/d/Y')
                            );

        $this->db->insert('tblBatchTracking',$insert_fields);

        return $Batch_DocNo;

    }

    public function PostProductionOutput($docno ,$itemNo, $actualQty, $location, $documentNo, $loctype = '', $sublocation = '', $lineNum){
            $itemData = $this->db->where('IM_Item_Id',$itemNo)->get('tblItem')->row_array();
            $MO_Details = $this->db->where(array(
                                    'MOD_MO_DocNo' => $docno,
                                    'MOD_ItemNo' => $itemNo
                                ))
                                ->join('tblItem','MOD_ItemNo = IM_Item_Id', 'left')
                                ->join('tblItemUOMConv','MOD_ItemNo = IUC_FK_Item_id AND MOD_YieldUOM = IUC_FK_UOM_id', 'left')
                                ->get('tblManufacturingOrderDetail')->row_array();
            $MO_SubDetails = $this->db
                                ->join('tblItem','MODS_MOD_ItemNo = IM_Item_Id', 'left')
                                ->where(array(
                                    'MODS_MOD_MO_DocNo' => $docno,
                                    'MODS_MOD_ItemNo' => $itemNo
                                ))
                                ->get('tblManufacturingOrderSubDetail')->result_array();


            // Insert BOM Details to Item Ledger
            $lineNo = 1;                                
            foreach ($MO_SubDetails as $index => $MO_SubDetail) {

                if($loctype == 'Production'){
                    // Validate Qty per Item
                    // $this->CheckStockPerItem($BOM_Detail['IM_Item_Id']
                    //                     ,$BOM_Detail['IM_Sales_Desc']
                    //                     ,$location
                    //                     ,$BOM_Detail['BOMD_Qty']);

                    $this->CreateItemLedger('Production Output'
                                            ,$location
                                            ,$location
                                            ,'Production Output'
                                            ,$documentNo
                                            ,$lineNo
                                            ,$MO_SubDetail['IM_FK_ItemType_id']
                                            ,$MO_SubDetail['IM_Item_Id']
                                            ,$MO_SubDetail['IM_Sales_Desc']
                                            ,$MO_SubDetail['MODS_Qty'] * $MO_SubDetail['MODS_BaseUomQty'] * ($actualQty * -1)
                                            ,$MO_SubDetail['MODS_UnitCost']
                                            ,$location
                                            ,$MO_SubDetail['IM_FK_Attribute_UOM_id']
                                            ,$location
                                            ,GOODS
                    );
                    $lineNo += 1;
                }
            }

            if($loctype == 'Production'){
                // Insert BOM Header to Item Ledger
                $this->CreateItemLedger('Production Output'
                                            ,$location
                                            ,$location
                                            ,'Production Output'
                                            ,$documentNo
                                            ,$lineNo
                                            ,$MO_Details['IM_FK_ItemType_id']
                                            ,$MO_Details['IM_Item_Id']
                                            ,$MO_Details['IM_Sales_Desc']
                                            ,($actualQty * -1) * $MO_Details['IUC_Quantity']
                                            ,$MO_Details['IM_UnitCost']
                                            ,$location
                                            ,$MO_Details['IM_FK_Attribute_UOM_id']
                                            ,$location
                                            ,GOODS
                    );
            }
            elseif($loctype == 'QA'){
                // Insert BOM Header to Item Ledger
                $this->CreateItemLedger('Production Output'
                                            ,$location
                                            ,$location
                                            ,'Production Output'
                                            ,$documentNo
                                            ,$lineNo
                                            ,$MO_Details['IM_FK_ItemType_id']
                                            ,$MO_Details['IM_Item_Id']
                                            ,$MO_Details['IM_Sales_Desc']
                                            ,$actualQty * $MO_Details['IUC_Quantity']
                                            ,$MO_Details['IM_UnitCost']
                                            ,$location
                                            ,$MO_Details['IM_FK_Attribute_UOM_id']
                                            ,$location
                                            ,'QA WHSE'
                    );
            }


    }

    public function CreateLotTracking($batchId
                                        ,$itemNo
                                        ,$lineNo
                                        ,$seqNo
                                        ,$expDate
                                        ,$qty
                                        ,$unitCost
                                        ,$UOM
                                        ,$baseQty
                                        ,$baseUOM
                                        ,$remQty
                                        ,$location
                                        ,$subLocation
                                        ,$supplier
                                    )
    {
        
        $this->load->model('master_file/bom/noseries_model');
        $dtExpiryDate   = date_format(date_create(),'m/d/Y');   // TODO FOR AUTOMATIC COMPUTATION OF EXPIRY DATE PER ITEM
        $Lot_DocNo      = $this->noseries_model->getNextAvailableNumber("10003", true, "", true);
        $lotTrackingId  = $itemNo .'-'.$supplier.'-'.date_format(date_create($dtExpiryDate),'Mdy').'-'.$batchId.'-'.$Lot_DocNo;

        $insert_fields = array('LT_BT_BatchID'          =>  $batchId
                                , 'LT_LotID'            =>  $lotTrackingId
                                , 'LT_EntryNo'          =>  $Lot_DocNo
                                , 'LT_ItemNo'           =>  $itemNo
                                , 'LT_LineNo'           =>  $lineNo
                                , 'LT_SeqNo'            =>  $seqNo
                                , 'LT_ExpiryDate'       =>  date_format(date_create($expDate),'m/d/Y')
                                , 'LT_Qty'              =>  $qty
                                , 'LT_UnitCost'         =>  $unitCost
                                , 'LT_UOM'              =>  $UOM
                                , 'LT_BaseQty'          =>  $baseQty
                                , 'LT_BaseUOM'          =>  $baseUOM
                                , 'LT_RemQty'           =>  $remQty
                                , 'LT_Location'         =>  $location
                                , 'LT_SubLocation'      =>  $subLocation     
                                , 'CreatedBy'           =>  getCurrentUser()['login-user']
                                , 'DateCreated'         =>  date_format(date_create(),'m/d/Y')
                                , 'ModifiedBy'          =>  getCurrentUser()['login-user']
                                , 'DateModified'        =>  date_format(date_create(),'m/d/Y')
                            );

        $this->db->insert('tblLotTracking',$insert_fields);

        return $lotTrackingId;

    }

    public function CreateLotTrackingProdOutput($batchId
                                        ,$itemNo
                                        ,$lineNo
                                        ,$seqNo
                                        ,$expDate
                                        ,$qty
                                        ,$unitCost
                                        ,$UOM
                                        ,$baseQty
                                        ,$baseUOM
                                        ,$remQty
                                        ,$location
                                        ,$subLocation
                                        ,$supplier
                                        ,$uom_name
                                    )
    {
        
        $this->load->model('master_file/bom/noseries_model');
        $dtExpiryDate   = date_format(date_create(),'m/d/Y');   // TODO FOR AUTOMATIC COMPUTATION OF EXPIRY DATE PER ITEM
        $Lot_DocNo      = $this->noseries_model->getNextAvailableNumber("10003", true, "", true);
        $lotTrackingId  = $itemNo .'-'.$supplier.'-'.date_format(date_create($dtExpiryDate),'Mdy').'-'.$batchId.'-'.$Lot_DocNo.'-'.$qty.$uom_name;

        $insert_fields = array('LT_BT_BatchID'          =>  $batchId
                                , 'LT_LotID'            =>  $lotTrackingId
                                , 'LT_EntryNo'          =>  $Lot_DocNo
                                , 'LT_ItemNo'           =>  $itemNo
                                , 'LT_LineNo'           =>  $lineNo
                                , 'LT_SeqNo'            =>  $seqNo
                                , 'LT_ExpiryDate'       =>  date_format(date_create($expDate),'m/d/Y')
                                , 'LT_Qty'              =>  $qty
                                , 'LT_UnitCost'         =>  $unitCost
                                , 'LT_UOM'              =>  $UOM
                                , 'LT_BaseQty'          =>  $baseQty
                                , 'LT_BaseUOM'          =>  $baseUOM
                                , 'LT_RemQty'           =>  $remQty
                                , 'LT_Location'         =>  $location
                                , 'LT_SubLocation'      =>  $subLocation     
                                , 'CreatedBy'           =>  getCurrentUser()['login-user']
                                , 'DateCreated'         =>  date_format(date_create(),'m/d/Y')
                                , 'ModifiedBy'          =>  getCurrentUser()['login-user']
                                , 'DateModified'        =>  date_format(date_create(),'m/d/Y')
                            );

        $this->db->insert('tblLotTracking',$insert_fields);

        return $lotTrackingId;

    }

    private function PostToReceive($documentNo){

        $output = array(
                                'success' => 1,
                                'message' => ''
                            );

        $sql_query = 'SELECT h.PO_ExchangeRate, h.PO_Currency,h.PO_ReceivingDate, h.PO_SupplierInvoice,
                             h.PO_SupplierDR,h.PO_DocNo, h.PO_DocDate, h.PO_SupplierID, h.PO_SupplierName, 
                             h.PO_SupplierAddress, d.POD_Comment, h.PO_DueDate, h.PO_Terms, h.PO_Remarks, 
                             h.PO_ExpectedDeliveryDate, h.PO_ValidityDate, h.PO_Amount, h.PO_Company, h.PO_VATPostingGroup, 
                             h.PO_WHTPostingGroup, h.PO_SupplierPostingGroup, h.PO_SupplierInvoice, h.PO_Buyer, d.POD_LineNo, d.POD_ItemType, 
                             d.POD_ItemNo, d.POD_ItemDescription, d.POD_Location, d.POD_Qty, d.POD_UOM, d.POD_UnitPrice, d.POD_Total, 
                             d.POD_Currency, d.POD_RefFrom, d.POD_RefTo, d.POD_EstimatedCost, d.POD_VAT, d.POD_WHT, d.POD_QtyToReceive, 
                             d.POD_QtyReceived, d.CreatedBy , d.DateCreated, d.ModifiedBy, d.DateModified, d.POD_RefFromLineNo, 
                             IPG.IPG_COA_FK_AccountNo, SPG.SPG_COA_FK_AccountNo, d.POD_IPG
                       FROM tblSupplier s 
                            LEFT OUTER JOIN tblACC_SupplierPostingGroup SPG ON s.S_SupplierPostingGroup = SPG.SPG_Code  
                            RIGHT OUTER JOIN tblPO h INNER JOIN tblPODetail d ON h.PO_DocNo = d.POD_PO_DocNo ON  
                                s.S_Id = h.PO_SupplierID LEFT OUTER JOIN tblACC_InvPostingGroup IPG RIGHT OUTER JOIN  
                                tblItem i ON IPG.IPG_Code = i.IM_INVPosting_Group ON d.POD_ItemNo = i.IM_Item_Id  
                       WHERE (h.PO_DocNo = \''.$documentNo.'\' )';

        $resultPOCount = $this->db->query($sql_query)->num_rows();
        $resultPO = $this->db->query($sql_query)->result_array();

        if($resultPOCount <= 0):
            $output = array(
                                'success' => 0,
                                'message' => 'Qty to Receive is zero!'
                            );
            return $output;
        else:

            $qty            = $this->db->select('sum("POD_Qty") as "POD_Qty"')
                                    ->where('POD_PO_DocNo',$documentNo)->get('tblPODetail')->row_array()['POD_Qty'];
            $qtytoreceive   = 0;
            $qtyreceived    = 0;

            $dblTotalBaseAmnt = 0;
            foreach ($resultPO as $key => $detail) {
                if($detail['POD_QtyToReceive'] > 0 ){
                    $dblTotalBaseAmnt += $detail['POD_QtyToReceive'] * $detail['POD_UnitPrice'];
                }
            }


            // GENERATE BATCH TRACKING
            $batchId = $this->CreateBatchTracking('Receiving'
                                                    ,$resultPO[0]['PO_DocNo']
                                                    ,$resultPO[0]['PO_DocDate']
                                                    ,$resultPO[0]['PO_SupplierID']
            );

            // GENERATE RECEIVING RECEIPT
            $RR_DocNo = $this->CreateReceiving($resultPO[0]['PO_SupplierID']
                                                , $resultPO[0]['PO_Remarks']
                                                , $resultPO[0]['PO_Terms']
                                                , $resultPO[0]['PO_DueDate']
                                                , $resultPO[0]['PO_ExpectedDeliveryDate']
                                                , $resultPO[0]['PO_ValidityDate']
                                                , $resultPO[0]['PO_Company']
                                                , $dblTotalBaseAmnt
                                                , getCurrentUser()['login-user']
                                                , $resultPO[0]['PO_SupplierInvoice']
                                                , $resultPO[0]['PO_SupplierDR']
                                                , $resultPO[0]['PO_DocDate']
                                                , $resultPO[0]['PO_Currency']
                                                , $resultPO[0]['PO_ExchangeRate']
            );

            $lineNo = 1;
            $subLineNo = 1;
            foreach ($resultPO as $index => $detail) {
                // $this->print_r($resultPO);
                // $qty            += $detail['POD_Qty'];
                $qtyreceived    += $detail['POD_QtyReceived'];

                $dblValidatePerDetail = 0;
                $pod_qty = 0;


                if(number_format($detail['POD_Qty'], 4, ".", "") == number_format($detail['POD_QtyToReceive'], 4, ".", "")){

                    $this->db->where('POD_PO_DocNo', $documentNo)->where('POD_ItemNo', $detail['POD_ItemNo'])->where('POD_LineNo', $detail['POD_LineNo'])->set('POD_QtyToReceive', $detail['POD_Qty'])->update('tblPODetail');

                    $pod_qty = $detail['POD_Qty'];

                }
                else{
                    $pod_qty = $detail['POD_QtyToReceive'];
                }

                if(($detail['POD_QtyReceived'] + $pod_qty) < $detail['POD_Qty'] ){
                    $dblValidatePerDetail += 1;
                }

                // Query for getting Unit Cost Per Item
                $query_rr = 'SELECT        RRD_ItemNo, RRD_RefFrom, RRD_RefFromLineNo, RRD_UOM, SUM(RRD_QtyReceived) AS RRD_QtyReceived
                         FROM            tblRRDetail
                         GROUP BY RRD_ItemNo, RRD_RefFrom, RRD_RefFromLineNo, RRD_UOM
                         HAVING        (RRD_RefFromLineNo = '.$detail['POD_LineNo'].') AND (RRD_RefFrom = \''.$resultPO[0]['PO_DocNo'].'\') AND (RRD_ItemNo = \''.$detail['POD_ItemNo'].'\')';

                $resultRR = $this->db->query($query_rr)->row_array();

                if(($resultRR['RRD_QtyReceived'] + $detail['POD_QtyToReceive']) > $detail['POD_QtyReceived']){
                    $qty_total_cost = $detail['POD_Qty'] * $detail['POD_UnitPrice'];
                    $qty_rr_total_cost = $resultRR['RRD_QtyReceived'] * $detail['POD_UnitPrice'];
                    $qty_to_receive_total_cost = $detail['POD_QtyToReceive'] * $detail['POD_UnitPrice'];

                    $diff = $qty_total_cost - $qty_rr_total_cost;

                    $lastPOcost = $diff/$qty_to_receive_total_cost; 

                    // Update Last PO Cost
                    $sql_query = '  UPDATE tblItem 
                                    SET IM_LastPOCost = '.$lastPOcost.',
                                        IM_UnitCost = CASE WHEN ISNULL(IM_UnitCost,0) = 0 THEN '.$lastPOcost.' ELSE IM_UnitCost END 
                                    WHERE IM_Item_Id = ? 
                    ';
                    $this->db->query($sql_query,array($detail['POD_ItemNo']));
                }
                else{
                    // Update Last PO Cost
                    $sql_query = '  UPDATE tblItem 
                                    SET IM_LastPOCost = '.$detail['POD_UnitPrice'].',
                                        IM_UnitCost = CASE WHEN ISNULL(IM_UnitCost,0) = 0 THEN '.$detail['POD_UnitPrice'].' ELSE IM_UnitCost END 
                                    WHERE IM_Item_Id = ? 
                    ';

                    $this->db->query($sql_query,array($detail['POD_ItemNo']));
                }


                if($detail['POD_QtyToReceive'] > 0 ):
                    $qtytoreceive   += $detail['POD_QtyToReceive'];

                    $query_pods = 'SELECT PODS_PO_DocNo, PODS_RefLineNo, PODS_LineNo, PODS_ItemNo, PODS_QtyToReceive, PODS_Location,
                                          PODS_Sublocation, PODS_ExpiryDate, ISNULL(IUC_Quantity,0) AS IUC_Quantity , IM_FK_Category_id,
                                          IM_FK_Attribute_UOM_id, AD_Code, IM_Transfer_UOM_id
                                   FROM tblPODetailExpDate
                                        LEFT OUTER JOIN tblItem ON PODS_ItemNo = IM_Item_Id
                                        LEFT OUTER JOIN tblItemUOMConv ON IUC_FK_UOM_id = IM_Transfer_UOM_id AND IUC_FK_Item_id = IM_Item_Id
                                        LEFT OUTER JOIN tblAttributeDetail ON AD_Id = IM_Transfer_UOM_id
                                   WHERE PODS_PO_DocNo = \''.$resultPO[0]['PO_DocNo'].'\' AND PODS_ItemNo = \''.$detail['POD_ItemNo'].'\'';

                    $resultPODS = $this->db->query($query_pods)->result_array();

                    $pods_qty = 0;
                    
                    foreach($resultPODS as $key => $sub_detail){
                        
                        if(number_format($detail['POD_Qty'], 4, ".", "") == number_format($sub_detail['PODS_QtyToReceive'], 4, ".", "")){
                            
                            $pods_qty = $detail['POD_Qty'];

                        }
                        else{
                            $pods_qty = $sub_detail['PODS_QtyToReceive'];
                        }

                        if($sub_detail['IM_FK_Category_id'] == 'FG'){


                            if($sub_detail['IM_Transfer_UOM_id'] == '' || $sub_detail['IM_Transfer_UOM_id'] == null || $sub_detail['IM_Transfer_UOM_id'] == $detail['POD_UOM']){


                                // GENERATE LOT TRACKING
                                $LotTrackingId = $this->CreateLotTracking($batchId
                                                        ,$sub_detail['PODS_ItemNo']
                                                        ,$sub_detail['PODS_LineNo']
                                                        ,$subLineNo
                                                        ,$sub_detail['PODS_ExpiryDate']
                                                        ,round($pods_qty, 12)
                                                        ,$detail['POD_UnitPrice']
                                                        ,$detail['POD_UOM']
                                                        ,round($pods_qty * $sub_detail['IUC_Quantity'], 12)
                                                        ,$detail['POD_UOM']
                                                        ,$pods_qty
                                                        ,$sub_detail['PODS_Location']
                                                        ,$sub_detail['PODS_Sublocation']
                                                        ,$detail['PO_SupplierID']
                                                    );

                                $baseUOM = $this->db->where(array('IUC_Quantity'=>1,'IUC_FK_Item_id'=>$detail['POD_ItemNo']))
                                                ->get('tblItemUOMConv')->row_array()['IUC_FK_UOM_id'];

                                // GENERATE LOT LEDGER
                                $ll_details = array(
                                    'module-id'     => '2210',
                                    'doc-date'      => date('Y-m-d H:i:s'),
                                    'doc-no'        => $resultPO[0]['PO_DocNo'],
                                    'line-no'       => $index+1,
                                    'batch-id'      => $batchId,
                                    'lot-id'        => $LotTrackingId,
                                    'item-no'       => $sub_detail['PODS_ItemNo'],
                                    'quantity'      => round($pods_qty, 12),
                                    'uom-id'        => $detail['POD_UOM'],
                                    'unit-cost'     => $detail['POD_UnitPrice'],
                                    'total-cost'    => $pods_qty * $detail['POD_UnitPrice'] * $sub_detail['IUC_Quantity'],
                                    'base-qty'      => round($sub_detail['IUC_Quantity'] * $pods_qty, 12),
                                    'base-uom'      => $baseUOM,
                                    'location'      => $sub_detail['PODS_Location'],
                                    'sub-location'  => $sub_detail['PODS_Sublocation'],
                                    'status'        => 1, 
                                    'module-name'   => 'PO Receiving'
                                );

                                $this->CreateLotLedger($ll_details,false);
                                
                            }
                            else{

                                // $this->print_r($sub_detail);

                                $row_num        = $pods_qty / $sub_detail['IUC_Quantity'];
                                $ceil_num       = ceil($row_num);
                                $actual_qty     = $pods_qty;
                                $prod_uom_qty   = $sub_detail['IUC_Quantity'];


                                for ($i = 1; $i <= $ceil_num ; $i++){
                                    if($actual_qty > $prod_uom_qty){
                                        $div = $prod_uom_qty/$prod_uom_qty;
                                        $actual_qty -= $prod_uom_qty;
                                    }
                                    elseif($prod_uom_qty >= $actual_qty){
                                        $div = $actual_qty/$prod_uom_qty;
                                    }

                                    $lot_id = $this->CreateLotTrackingProdOutput( $batchId
                                                                                , $sub_detail['PODS_ItemNo']
                                                                                , $sub_detail['PODS_RefLineNo']
                                                                                , $sub_detail['PODS_LineNo']
                                                                                , $sub_detail['PODS_ExpiryDate']
                                                                                , round($div, 12)
                                                                                , $detail['POD_UnitPrice']
                                                                                , $sub_detail['IM_Transfer_UOM_id']
                                                                                , round($sub_detail['IUC_Quantity'] * $div, 12)
                                                                                , $sub_detail['IM_FK_Attribute_UOM_id']
                                                                                , $div
                                                                                , $sub_detail['PODS_Location']
                                                                                , $sub_detail['PODS_Sublocation']
                                                                                , ''
                                                                                , $sub_detail['AD_Code']
                                    );

                                        $ll_details = array(
                                            'module-id'     => '2210',
                                            'doc-date'      => date('Y-m-d H:i:s'),
                                            'doc-no'        => $resultPO[0]['PO_DocNo'],
                                            'line-no'       => $key+1,
                                            'batch-id'      => $batchId,
                                            'lot-id'        => $lot_id,
                                            'item-no'       => $sub_detail['PODS_ItemNo'],
                                            'quantity'      => round($div, 12),
                                            'uom-id'        => $sub_detail['IM_Transfer_UOM_id'],
                                            'unit-cost'     => $detail['POD_UnitPrice'],
                                            'total-cost'    => round($detail['POD_UnitPrice'] * $div * $sub_detail['IUC_Quantity'], 12),
                                            'base-qty'      => round($sub_detail['IUC_Quantity'] * $pods_qty, 12),
                                            'base-uom'      => $sub_detail['IM_FK_Attribute_UOM_id'],
                                            'location'      => $sub_detail['PODS_Location'],
                                            'sub-location'  => $sub_detail['PODS_Sublocation'],
                                            'status'        => 1, 
                                            'module-name'   => 'PO Receiving'
                                        );

                                        $this->CreateLotLedger($ll_details);
                                }

                            }
                        }
                        elseif ($sub_detail['IM_FK_Category_id'] == 'RM') {

                            // GENERATE LOT TRACKING
                            $LotTrackingId = $this->CreateLotTracking($batchId
                                                    ,$sub_detail['PODS_ItemNo']
                                                    ,$sub_detail['PODS_LineNo']
                                                    ,$subLineNo
                                                    ,$sub_detail['PODS_ExpiryDate']
                                                    ,round($pods_qty, 12)
                                                    ,$detail['POD_UnitPrice']
                                                    ,$detail['POD_UOM']
                                                    ,round($pods_qty * $sub_detail['IUC_Quantity'], 12)
                                                    ,$detail['POD_UOM']
                                                    ,round($pods_qty, 12)
                                                    ,$sub_detail['PODS_Location']
                                                    ,$sub_detail['PODS_Sublocation']
                                                    ,$detail['PO_SupplierID']
                                                );

                            $baseUOM = $this->db->where(array('IUC_Quantity'=>1,'IUC_FK_Item_id'=>$detail['POD_ItemNo']))
                                            ->get('tblItemUOMConv')->row_array()['IUC_FK_UOM_id'];

                            // GENERATE LOT LEDGER
                            $ll_details = array(
                                'module-id'     => '2210',
                                'doc-date'      => date('Y-m-d H:i:s'),
                                'doc-no'        => $resultPO[0]['PO_DocNo'],
                                'line-no'       => $index+1,
                                'batch-id'      => $batchId,
                                'lot-id'        => $LotTrackingId,
                                'item-no'       => $sub_detail['PODS_ItemNo'],
                                'quantity'      => round($pods_qty, 12),
                                'uom-id'        => $detail['POD_UOM'],
                                'unit-cost'     => $detail['POD_UnitPrice'],
                                'total-cost'    => round($pods_qty * $detail['POD_UnitPrice'] * $sub_detail['IUC_Quantity'], 12),
                                'base-qty'      => round($pods_qty * $sub_detail['IUC_Quantity'], 12),
                                'base-uom'      => $baseUOM,
                                'location'      => $sub_detail['PODS_Location'],
                                'sub-location'  => $sub_detail['PODS_Sublocation'],
                                'status'        => 1, 
                                'module-name'   => 'PO Receiving'
                            );

                            $this->CreateLotLedger($ll_details);
                        }
                        
                        

                        // GENERATE RR DETAILS
                        $this->CreateRRDetails($RR_DocNo
                                                , $subLineNo
                                                , $detail['POD_ItemType']
                                                , $sub_detail['PODS_ItemNo']
                                                , $detail['POD_ItemDescription']
                                                , $sub_detail['PODS_Location']
                                                , round($pods_qty, 12)
                                                , $detail['POD_UOM']
                                                , $detail['POD_UnitPrice']
                                                , $resultPO[0]['PO_Currency']
                                                , $detail['POD_Comment']
                                                , $resultPO[0]['PO_DocNo']
                                                , $detail['POD_LineNo']
                                                , getCurrentUser()['login-user']
                                                , $detail['POD_VAT']
                                                , $detail['POD_WHT']
                                                , $sub_detail['PODS_Sublocation']
                                                , $sub_detail['PODS_ExpiryDate']
                                                
                        );

                        // GENERATE ITEM LEDGER
                        $this->CreateItemLedger('Receiving'
                                            ,$sub_detail['PODS_Location']
                                            ,$sub_detail['PODS_Location']
                                            ,'Receiving'
                                            ,$resultPO[0]['PO_DocNo']
                                            ,$index
                                            ,$detail['POD_ItemType']
                                            ,$sub_detail['PODS_ItemNo']
                                            ,$detail['POD_ItemDescription']
                                            ,round($sub_detail['IUC_Quantity'] * $pods_qty, 12) 
                                            ,$detail['POD_UnitPrice']
                                            ,$sub_detail['PODS_Location']
                                            ,$sub_detail['IM_FK_Attribute_UOM_id']
                                            ,$sub_detail['PODS_Location']
                                            ,$sub_detail['PODS_Sublocation']
                        );

                        $subLineNo += 1;
                    }

                    $this->db->where('PODS_PO_DocNo', $resultPO[0]['PO_DocNo'])->where('PODS_ItemNo', $detail['POD_ItemNo'])->delete('tblPODetailExpDate');


                    
                    $lineNo += 1;

                endif;

                $sql_query = 'UPDATE tblPODetail 
                                SET POD_QtyReceived = POD_QtyReceived + \''.round($pod_qty, 12).'\'
                              WHERE POD_PO_DocNo = ? AND POD_LineNo = ? ';
                $params = array($detail['PO_DocNo'],$detail['POD_LineNo']);
                $this->db->query($sql_query,$params);


            }

            // Status Update
            $status = 'Approved';
            if($dblValidatePerDetail == 0){
                $status = 'Closed';
            }
            else{
                $status = 'Partially Received';
            }

            //Update PO Detail Qty To Receive
            $this->db->update('tblPODetail'
                                              ,array('POD_QtyToReceive'=>0)
                                              ,array('POD_PO_DocNo'=>$documentNo));

            //Update PO Status
            $this->db->update('tblPO'
                                        ,array('PO_Status'=>$status,'PO_ReceivingDate'=> null,'PO_SupplierInvoice'=>'','PO_SupplierDR'=>'')
                                        ,array('PO_DocNo'=>$documentNo));

        endif;

        return $output;

    }

    private function getSupplierWHT($supplierID){
        return $this->db->select(array('S_WHT_PostingGroup'))
                        ->where('S_Id',$supplierID)
                        ->get('tblSupplier')->row_array()['S_WHT_PostingGroup'];
    }

    private function CreateSupplierLedger($docType
                                            ,$dtDocDate
                                            ,$cDocumentNo
                                            ,$dblAmount
                                            ,$dblAmountLCY
                                            ,$cSupplierID
                                            ,$cSupplierName
                                            ,$cInvoiceNo
                                            ,$cCurrency
                                            ,$cSupplierPostingGroup
                                            ,$cPayTerms
                                            ,$dDueDate
                                            ,$cBuyer
                                            ,$cLocationID
                                            ,$cCompanyid
                                            ,$nRemAmount
                                            ,$cDocCurrency
                                            ,$nExchangeRate
                                        )
    {
        $EntryNo = 1;
        $sql_query = 'SELECT TOP 1 SL_EntryNo FROM tblSupplierLedger ORDER BY SL_EntryNo DESC';
        $resultSL = $this->db->query($sql_query);

        if($resultSL->num_rows() > 0){
            $EntryNo = $resultSL->row_array()['SL_EntryNo'];
        }
        else{
            $EntryNo = 1;
        }

        $insert_fields = array(  'SL_EntryNo'                       => $EntryNo
                                ,'SL_DocType'                       => $docType
                                ,'SL_DocNo'                         => $cDocumentNo
                                ,'SL_DocDate'                       => date_format(date_create(),'m/d/Y')
                                ,'SL_DocAmount'                     => $dblAmount
                                ,'SL_DocAmountLCY'                  => $dblAmount * $nExchangeRate
                                ,'SL_SupplierID'                    => $cSupplierID
                                ,'SL_SupplierName'                  => $cSupplierName
                                ,'SL_ExtDocNo'                      => $cInvoiceNo
                                ,'SL_Currency'                      => $cCurrency
                                ,'SL_SupplierPostingGroup'          => $cSupplierPostingGroup
                                ,'SL_DueDate'                       => date_format(date_create($dDueDate),'m/d/Y')
                                ,'SL_Buyer'                         => $cBuyer
                                ,'SL_RequestedBy'                   => getCurrentUser()['login-user']
                                ,'SL_Company'                       => $cCompanyid
                                ,'SL_Location'                      => $cLocationID
                                ,'SL_RefDocType'                    => ''
                                ,'SL_RefDocNo'                      => ''
                                ,'SL_RefDocAmount'                  => 0
                                ,'SL_AppliesToDocType'              => ''
                                ,'SL_AppliesToDocNo'                => ''
                                ,'SL_AppliesToID'                   => ''
                                ,'SL_AppliesToDocDate'              => NULL
                                ,'SL_AppliedAmount'                 => 0
                                ,'SL_RemAmount'                     => $nRemAmount
                                ,'SL_PostedBy'                      => getCurrentUser()['login-user']
                                ,'SL_DatePosted'                    => date_format(date_create(),'m/d/Y')
                                ,'SL_ExchangeRate'                  => $nExchangeRate
                            );

        $this->db->insert('tblSupplierLedger',$insert_fields);


    }

    private function CreateAPVoucher($supplierID,
                                      $suplierName,
                                      $supplierAddress,
                                      $documentNo,
                                      $remarks,
                                      $dtDueDate,
                                      $cPaymentTerms,
                                      $dblAmount,
                                      $dblAmountLCY,
                                      $LocationID,
                                      $Company_id,
                                      $VPG,
                                      $WPG,
                                      $SPG,
                                      $APVSupplierNo,
                                      $Currency,
                                      $dblExchangeRate,
                                      $dtDocDate
                                  )
    {

        $noseries_model = $this->load_model('master_file/bom/noseries_model');
        $APV_DocNo = $noseries_model->getNextAvailableNumber("3220", true, getDefaultLocation());

        if($APV_DocNo == ''){
            throw new Exception("Setup Document Series for Module AP Voucher");
        }

        $insert_fields = array(
                                 'APV_DocNo'                    => $APV_DocNo
                                ,'APV_DocDate'                  => date_format(date_create(),'m/d/Y')
                                ,'APV_SupplierID'               => $supplierID
                                ,'APV_SupplierName'             => $suplierName
                                ,'APV_SupplierAddress'          => $supplierAddress
                                ,'APV_RefDocNo'                 => $documentNo
                                ,'APV_Remarks'                  => $remarks
                                ,'APV_DateRequired'             => date_format(date_create(),'m/d/Y')
                                ,'APV_PaymentTerms'             => $cPaymentTerms
                                ,'APV_Status'                   => 'Posted'
                                ,'APV_Amount'                   => $dblAmount
                                ,'APV_AmountLCY'                => $dblAmount * $dblExchangeRate
                                ,'APV_PaymentDiscount'          => 0
                                ,'APV_RequestedBy'              => getCurrentUser()['login-user']
                                ,'APV_Location'                 => $LocationID
                                ,'APV_Company'                  => $Company_id
                                ,'APV_AppliesToDocNo'           => ''
                                ,'APV_VATPostingGroup'          => $VPG
                                ,'APV_WHTPostingGroup'          => $WPG
                                ,'APV_SupplierPostingGroup'     => $SPG
                                ,'APV_DateReceived'             => date_format(date_create(),'m/d/Y')
                                ,'CreatedBy'                    => getCurrentUser()['login-user']
                                ,'DateCreated'                  => date_format(date_create(),'m/d/Y')
                                ,'ModifiedBy'                   => getCurrentUser()['login-user']
                                ,'DateModified'                 => date_format(date_create(),'m/d/Y')
                                ,'APV_RemAmount'                => $dblAmount
                                ,'APV_SupplierRefNo'            => $APVSupplierNo
                                ,'APV_Currency'                 => $Currency
                                ,'APV_ExchangeRate'             => $dblExchangeRate
                            );

        $this->db->insert('tblACC_APV',$insert_fields);

        return $APV_DocNo;
    }

    private function CreateReceiving($cSupplierID,
                                    $cRemarks,
                                    $cPayTerms,
                                    $dDueDate,
                                    $dExpectedDeliveryDate,
                                    $dValidityDate,
                                    $cCompany_id,
                                    $dblAmountLCY,
                                    $cUser_id,
                                    $cSupplierSI,
                                    $cSupplierDR,
                                    $dDocDate,
                                    $cDocCurrency,
                                    $nExchangeRate
                                )
    {
        
        $this->load->model('master_file/bom/noseries_model');
        $RR_DocNo = $this->noseries_model->getNextAvailableNumber("2300", true, getDefaultLocation());

        if($RR_DocNo == ''){
            throw new Exception("Setup Document Series for Receiving Report Module");
        }

        $insert_fields = array(
                                   'RR_DocNo'                   => $RR_DocNo
                                ,  'RR_DocDate'                 => date('m/d/Y h:i:s')
                                , 'RR_SupplierID'               => $cSupplierID
                                , 'RR_Remarks'                  => $cRemarks
                                , 'RR_Terms'                    => $cPayTerms
                                , 'RR_DueDate'                  => $dDueDate
                                , 'RR_ExpectedDeliveryDate'     => $dExpectedDeliveryDate
                                , 'RR_ValidityDate'             => $dValidityDate
                                , 'RR_Status'                   => 'Posted'
                                , 'RR_Company'                  => $cCompany_id
                                , 'RR_Amount'                   => $dblAmountLCY
                                , 'CreatedBy'                   => getCurrentUser()['login-user']
                                , 'DateCreated'                 => date('m/d/Y h:i:s')
                                , 'ModifiedBy'                  => getCurrentUser()['login-user']
                                , 'DateModified'                => date('m/d/Y h:i:s')
                                , 'RR_SupplierSI'               => $cSupplierSI
                                , 'RR_SupplierDR'               => $cSupplierDR
                                , 'RR_Currency'                 => $cDocCurrency
                                , 'RR_ExchangeRate'             => $nExchangeRate
        );

        $this->db->insert('tblRR',$insert_fields);

        return $RR_DocNo;
    }

    public function CreateRRDetails($cRRDocument 
                                    , $intRow
                                    , $cItemType
                                    , $cItemNo 
                                    , $cItemDesc 
                                    , $cLocation_id 
                                    , $dblQty
                                    , $cUOM
                                    , $dblPrice
                                    , $intCurrency
                                    , $cComment
                                    , $cPODocNo
                                    , $intPOLine
                                    , $cUser_id
                                    , $cVAT
                                    , $cWHT
                                    , $SubLocation
                                    , $ExpiryDate
                                )
    {

        if($cRRDocument == ''){
            throw new Exception("Setup Document Series for Receiving Report Module");
        }

        $insert_fields = array(
                                'RRD_RR_DocNo'              =>  $cRRDocument
                                , 'RRD_LineNo'              =>  $intRow
                                , 'RRD_ItemType'            =>  $cItemType
                                , 'RRD_ItemNo'              =>  $cItemNo
                                , 'RRD_ItemDescription'     =>  $cItemDesc
                                , 'RRD_Location'            =>  $cLocation_id
                                , 'RRD_QtyReceived'         =>  $dblQty
                                , 'RRD_UOM'                 =>  $cUOM
                                , 'RRD_UnitPrice'           =>  $dblPrice
                                , 'RRD_Total'               =>  $dblPrice * $dblQty
                                , 'RRD_Currency'            =>  $intCurrency
                                , 'RRD_Comment'             =>  $cComment
                                , 'RRD_RefFrom'             =>  $cPODocNo
                                , 'RRD_RefFromLineNo'       =>  $intPOLine
                                , 'CreatedBy'               =>  getCurrentUser()['login-user']
                                , 'DateCreated'             =>  date('m/d/Y h:i:s')
                                , 'ModifiedBy'              =>  getCurrentUser()['login-user']
                                , 'DateModified'            =>  date('m/d/Y h:i:s')
                                , 'RRD_VAT'                 =>  $cVAT
                                , 'RRD_WHT'                 =>  $cWHT
                                , 'RRD_RefTo'               =>  NULL
                                , 'RRD_RefToLineNo'         =>  NULL
                                , 'RR_SubLocation'          =>  $SubLocation
                                , 'RR_ExpiryDate'           =>  $ExpiryDate
        );

        $this->db->insert('tblRRDetail',$insert_fields);

    }

    public function CreateLotLedger($ll_details, $isSQLTransaction = true){

        if($isSQLTransaction){
            $this->db->trans_begin();
        }

        $_details = array(
                            'LL_ModuleID'       => $ll_details['module-id'],
                            'LL_DocDate'        => $ll_details['doc-date'],
                            'LL_DocNo'          => $ll_details['doc-no'],
                            'LL_LineNo'         => $ll_details['line-no'],
                            'LL_BatchID'        => $ll_details['batch-id'],
                            'LL_LotID'          => $ll_details['lot-id'],
                            'LL_ItemNo'         => $ll_details['item-no'],
                            'LL_Qty'            => $ll_details['quantity'],
                            'LL_UOM'            => $ll_details['uom-id'],
                            'LL_UnitCost'       => $ll_details['unit-cost'],
                            'LL_Amount'         => $ll_details['total-cost'],
                            'LL_BaseQty'        => $ll_details['base-qty'],
                            'LL_BaseUOM'        => $ll_details['base-uom'],
                            'LL_Location'       => $ll_details['location'],
                            'LL_SubLocation'    => $ll_details['sub-location'],
                            'LL_Status'         => $ll_details['status'],
                            'LL_ModuleName'     => $ll_details['module-name'], 
                            'CreatedBy'         => getCurrentUser()['login-user'],
                            'DateCreated'       => date('m/d/Y h:i:s')
                    );
        $this->db->insert('tblLotLedger', $_details);        

        if($isSQLTransaction){
            //validation of saving
            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $output = array(
                                    'success'=>false
                                ,   'message'=>$this->db->error()
                            );
            }
            else{
                $this->db->trans_commit();
            }
        }


    }

    public function getLotLedgerQty($barcode, $docno, $itemNo, $module){
        $query_string = 'SELECT LL_Qty, LL_BaseQty FROM tblLotLedger WHERE LL_LotID = \''.$barcode.'\' AND LL_DocNo = \''.$docno.'\' AND LL_ItemNo = \''.$itemNo.'\' AND LL_ModuleName = \''.$module.'\' AND LL_Status = 0 AND LL_Qty > 0';

        $result = $this->db->query($query_string)->row_array();

        return $result;
    }

    public function getProdLotLedgerQty($barcode, $docno, $itemNo, $module){
        $query_string = 'SELECT LL_Qty, LL_BaseQty FROM tblLotLedger WHERE LL_LotID = \''.$barcode.'\' AND LL_DocNo = \''.$docno.'\' AND LL_ItemNo = \''.$itemNo.'\' AND LL_ModuleName = \''.$module.'\' AND LL_Status = 0';

        $result = $this->db->query($query_string)->row_array();

        return $result;
    }

    public function getLotLedgerQtyOutgoing($barcode, $docno, $itemNo, $module){
        $query_string = 'SELECT LL_Qty, LL_BaseQty FROM tblLotLedger WHERE LL_LotID = \''.$barcode.'\' AND LL_DocNo = \''.$docno.'\' AND LL_ItemNo = \''.$itemNo.'\' AND LL_ModuleName = \''.$module.'\' AND LL_Status = 0 AND LL_Qty < 0';

        $result = $this->db->query($query_string)->row_array();

        return $result;
    }

     public function getLotLedgerQtyReceiving($barcode, $docno, $itemNo, $substr){
        $query_string = 'SELECT LL_Qty FROM tblLotLedger WHERE LL_LotID = \''.$barcode.'\' AND LL_DocNo = \''.$docno.'\' AND LL_ItemNo = \''.$itemNo.'\' AND LL_Qty LIKE \''.$substr.'%\'';

        $result = $this->db->query($query_string)->num_rows();

        return $result;
    }

    public function CreateItemLedger($cAccountType,
                                     $cAccountName, 
                                     $cGLAccount_id, 
                                     $cDocType, 
                                     $cDocNo, 
                                     $LineNo,
                                     $cItemType, 
                                     $cItemNo, 
                                     $cItemDesc, 
                                     $dblQty, 
                                     $dblPrice, 
                                     $cLocation_id, 
                                     $cUOM, 
                                     $cCPC,
                                     $cSublocation = ''
                                )
    {

        // get latest Entry No
        $IL_EntryNo = $this->db->order_by('IL_EntryNo DESC')->limit(1)->get('tblItemLedger')->row_array()['IL_EntryNo'];
        $IL_EntryNo = (empty($IL_EntryNo)) ? 1 : $IL_EntryNo + 1;

        if($cSublocation == ''){
            $cSublocation = 'QA WHSE';
        }

        $insert_fields = array(
                                'IL_EntryNo'                    => $IL_EntryNo, 
                                'IL_AccountType'                => $cAccountType, 
                                'IL_AccountID'                  => $cAccountName, 
                                'IL_AccountName'                => $cGLAccount_id, 
                                'IL_DocType'                    => $cDocType, 
                                'IL_DocNo'                      => $cDocNo, 
                                'IL_LineNo'                     => $LineNo, 
                                'IL_ItemType'                   => $cItemType, 
                                'IL_ItemNo'                     => $cItemNo, 
                                'IL_ItemDescription'            => $cItemDesc, 
                                'IL_Qty'                        => $dblQty, 
                                'IL_RemQty'                     => $dblQty, 
                                'IL_UnitPrice'                  => $dblPrice, 
                                'IL_Amount'                     => $dblQty * $dblPrice, 
                                'IL_AmountLCY'                  => $dblQty * $dblPrice, 
                                'IL_Location'                   => $cLocation_id, 
                                'IL_SubLocation'                => $cSublocation, 
                                'IL_UOM'                        => $cUOM, 
                                'IL_Comment'                    => '', 
                                'IL_CPC'                        => $cLocation_id, 
                                'IL_VAT'                        => 'NVAT', 
                                'IL_WHT'                        => 'NOWHT', 
                                'IL_PostedBy'                   => getCurrentUser()['login-user'], 
                                'IL_DatePosted'                 => date_format(date_create(),'m/d/Y h:m:s'), 
                                'CreatedBy'                     => getCurrentUser()['login-user'], 
                                'ModifiedBy'                    => getCurrentUser()['login-user'], 
                                'DateCreated'                   => date_format(date_create(),'m/d/Y h:m:s'), 
                                'DateModified'                  => date_format(date_create(),'m/d/Y h:m:s'), 
                                'IL_ExtDocNo'                   => '', 
                                'IL_Currency'                   => 1, 
                                'IL_ExchangeRate'               => 1
        );
        $this->db->insert('tblItemLedger',$insert_fields);
    }

    public function CheckStockPerItem($itemNo,$itemDesc, $location, $qty){

        $qty_stock_onhand = $this->db->select('ISNULL(SUM("IL_Qty"),0) as "IL_Qty"')
                                     ->where('IL_ItemNo',$itemNo)
                                     ->where('IL_Location', 'Central Warehouse')
                                     ->where('IL_SubLocation', 'PRODUCTION')
                                     ->get('tblItemLedger')
                                     ->row_array()['IL_Qty'];

        if($qty > $qty_stock_onhand){
            $qty_needed = ($qty_stock_onhand - $qty) * -1;
            throw new Exception("Not Enough Stock on Item: {$itemDesc} \r\n Quantity needed: {$qty_needed}");
        }

    }

    public function getLotTracking($batch_id, $lineNo){
        $query_string = 'SELECT        LT_LotID, LT_LineNo, LT_BT_BatchID
                         FROM          tblLotTracking
                         WHERE         LT_LineNo = \''.$lineNo.'\' AND LT_BT_BatchID = \''.$batch_id.'\'';

        $result = $this->db->query($query_string)->row_array();

        return $result['LT_LotID'];
    }

    public function getEntryNo($batch_id, $lot_id){
        $query_string = 'SELECT        LT_BT_BatchID, LT_LotID, LT_EntryNo, LT_ItemNo, LT_LineNo
                        FROM            tblLotTracking
                        WHERE        LT_LotID = \''.$lot_id.'\' AND LT_BT_BatchID = \''.$batch_id.'\'';

        $result = $this->db->query($query_string)->row_array();

        return $result;

    }
    
}
