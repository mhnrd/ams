<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class companyaccess_model extends MAIN_Model {

    protected $_table = 'tblCompanyAccess';
    public function __construct() {
        parent::__construct();
    }

    public function getAllActiveLocation(){
        return $this->db->where('SP_Active','1')->get('tblStoreProfile')->result_array();
    }

    public function getLocationByUserId($userId){
       $query_string = 'SELECT [CA_FK_Location_id]
                        FROM tblCompanyAccess
                        WHERE CA_DefaultLocation = \'1\'
                        AND CA_Active = \'1\' AND CA_U_ID = ?';
        $params = array($userId);
        $query = $this->db->query($query_string, $params);
        return $query->row_array();
    }

    public function getLocationListByUserId($userId){
       $query_string = 'SELECT [CA_FK_Location_id], [CA_DefaultLocation]
                        FROM tblCompanyAccess
                        WHERE CA_U_ID = ?';
        $params = array($userId);
        $query = $this->db->query($query_string, $params);
        return $query->result_array();
    }

    public function getLocationDescriptionByUserId($userId, $location = ''){
        $query_string = 'SELECT        CA.CA_FK_Location_id, CA.CA_DefaultLocation, CA.CA_U_ID,
                                       SP.SP_StoreName
                         FROM          tblCompanyAccess as CA LEFT OUTER JOIN
                                       tblStoreProfile as SP ON CA.CA_FK_Location_id = SP.SP_ID
                         WHERE         CA.CA_U_ID = \''.$userId.'\' 
                         ORDER BY SP.SP_StoreName';

        $result = $this->db->query($query_string)->result_array();
        foreach ($result as $key => $data) {
            if($location == $data['CA_FK_Location_id']){
                unset($result[$key]);
            }
        }
        $result = array_values($result);
        return $result;
    }

    public function getLocationByProductionArea($userId){
        $query_string = 'SELECT SP_ID, SP_StoreName, SP_StoreType
                        FROM tblStoreProfile
                        WHERE (SP_StoreType = 4) 
                        ORDER BY SP_StoreName';

        $result = $this->db->query($query_string)->result_array();
        return $result;
    }
    
    public function getStoreTypebyUserID($userId){
        $query_string = 'SELECT        CA.CA_FK_Location_id, CA.CA_DefaultLocation, SP.SP_StoreType, SP.SP_StoreName, SP.SP_ID
                         FROM          tblStoreProfile AS SP RIGHT OUTER JOIN
                                       tblCompanyAccess AS CA ON SP.SP_ID = CA.CA_FK_Location_id
                         WHERE        CA.CA_U_ID = \''.$userId.'\' 
                         ORDER BY SP.SP_StoreName';

        $result = $this->db->query($query_string)->result_array();
        return $result;                 
    }


    public function getDefaultStoreTypebyUserID($userId){
        $query_string = 'SELECT        CA.CA_FK_Location_id, CA.CA_DefaultLocation, SP.SP_StoreType
                         FROM          tblStoreProfile AS SP RIGHT OUTER JOIN
                                       tblCompanyAccess AS CA ON SP.SP_ID = CA.CA_FK_Location_id
                         WHERE        CA.CA_U_ID = \''.$userId.'\' AND CA_FK_Location_id = \''.getDefaultLocation().'\'';

        $result = $this->db->query($query_string)->row_array();
        return $result;                 
    }

     public function getDefaultStoreNamebyUserID($userId){
        $query_string = 'SELECT        CA.CA_FK_Location_id, CA.CA_DefaultLocation, SP.SP_StoreType, SP.SP_StoreName
                         FROM          tblStoreProfile AS SP RIGHT OUTER JOIN
                                       tblCompanyAccess AS CA ON SP.SP_ID = CA.CA_FK_Location_id
                         WHERE        CA.CA_U_ID = \''.$userId.'\' AND CA_FK_Location_id = \''.getDefaultLocation().'\' 
                         ORDER BY SP.SP_StoreName ';

        $result = $this->db->query($query_string)->row_array();
        return $result;                 
    }

    public function getSubLocationbyUserId($userId, $storeType, $location){
        $query_string = 'SELECT        tblLocation.LOC_Id, tblCompanyAccess.CA_FK_Location_id, tblStoreProfile.SP_StoreName, tblCompanyAccess.CA_U_ID, tblLocation.LOC_Name
                         FROM            tblCompanyAccess LEFT OUTER JOIN
                                                 tblStoreProfile ON tblCompanyAccess.CA_FK_Location_id = tblStoreProfile.SP_ID LEFT OUTER JOIN
                                                 tblLocation ON tblStoreProfile.SP_StoreType = tblLocation.LOC_FK_Type_id
                         WHERE        tblLocation.LOC_FK_Type_id = \''.$storeType.'\' AND (tblLocation.LOC_Id NOT IN
                                                     (SELECT        LOC_Parent_id
                                                       FROM            tblLocation AS tblLocation_1
                                                       WHERE        (LOC_FK_Type_id = \''.$storeType.'\') AND (LOC_Parent_id IS NOT NULL)
                                                       GROUP BY LOC_Parent_id)) AND (tblCompanyAccess.CA_U_ID = \''.$userId.'\' AND (tblCompanyAccess.CA_FK_Location_id = \''.$location.'\'))';

        $result = $this->db->query($query_string)->result_array();

        return $result;
    }

    public function getAllUserLocationAccess($userId){
        $query_string = 'SELECT        CA_FK_Location_id
                         FROM          tblCompanyAccess
                         WHERE        CA_U_ID = \''.$userId.'\'';

        $result = $this->db->query($query_string)->result_array();
        return $result;                 
    }

    public function getAllLocationAccess($userId){
        $query_string = 'SELECT        CA_FK_Location_id
                         FROM          tblCompanyAccess
                         WHERE        CA_U_ID = \''.$userId.'\'';

        $result = array_column($this->db->query($query_string)->result_array(), 'CA_FK_Location_id');
        return $result;                 
    }

    public function getStoreTypebyLocation($location){
        $query_string = 'SELECT        SP_ID, SP_StoreName, SP_StoreType
                         FROM            tblStoreProfile
                         WHERE        SP_ID = \''.$location.'\' 
                         ORDER BY SP_StoreName';

        $result = $this->db->query($query_string)->row_array();
        return $result;                 
    }

    public function getAllWarehouseLocation(){
        $query_string = 'SELECT        SP_ID, SP_StoreName, SP_StoreType
                        FROM            tblStoreProfile
                        WHERE        (SP_StoreType = 1) 
                        ORDER BY SP_StoreName';

        $result = $this->db->query($query_string)->result_array();

        return $result;
    }

    public function getAllLocation(){
        $query_string = 'SELECT        SP_ID, SP_StoreName, SP_StoreType
                        FROM            tblStoreProfile
                        WHERE SP_Active = \'1\' 
                        ORDER BY SP_StoreName';

        $result = $this->db->query($query_string)->result_array();

        return $result;
    }

    public function getAllActiveUsers(){
        return $this->db->select(array('U_ID','U_Username'))
                        ->where('U_Status','1')
                        ->get('tblUser')
                        ->result_array();
    }
}
