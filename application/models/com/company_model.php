<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class company_model extends MAIN_Model {

    protected $_table = 'tblCompany';
    public function __construct() {
        parent::__construct();
    }

    public function getByID($userId){
       $query_string = 'SELECT *
                        FROM tblCompany
                        WHERE COM_Id = ?';
        $params = array($userId);
        $query = $this->db->query($query_string, $params);
        return $query->row_array();
    }
}
