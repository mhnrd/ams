<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_role_setup_sub_detail_model extends MAIN_Model {

	private $id = NULL;
    private $order_column = array('M_DisplayName', 'RA_Add', 'RA_Edit', 'RA_Print', 'RA_Delete', 'RA_View', 'RA_Menu');

    public function __construct() {
        parent::__construct();
    }

     /* datatable query starts here */
    public function table_data($group_id){

        
        $this->db->select("M_DisplayName, RA_Add, RA_Edit, RA_Print, RA_Delete, RA_View, RA_Menu");  
        $this->db->from('tblRoleAccess');
        $this->db->join('tblModule', 'RA_FK_Modules_Code = M_Module_id');  
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables($group_id);
        $output['num_rows']          = $this->get_filtered_data($group_id);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query($group_id){
        $this->db->select("M_DisplayName, RA_Add, RA_Edit, RA_Print, RA_Delete, RA_View, RA_Menu"); 
        $this->db->join('tblModule', 'RA_FK_Modules_Code = M_Module_id');
        $this->db->where('RA_FK_Groups_Code', $group_id);

        // Individual column search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('G_Name',trim($_POST['columns'][1]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('G_Description',trim($_POST['columns'][2]["search"]["value"]));
        }

        // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("M_DisplayName", trim($_POST["search"]["value"])); 
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('M_Sequence', 'ASC');  
        }
    }

    public function datatables($group_id){
        $this->query($group_id); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblRoleAccess');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data($group_id){  
           $this->query($group_id);  
           $query = $this->db->get('tblRoleAccess');  
           return $query->num_rows();  
    }

    public function getAllModuleFunctions($role = '', $type = 'add'){

        $query_string = '';

        if($type == 'add'){
            $query_string = 'SELECT     F_Function_id, F_FunctionName, F_FK_Module_id, M_DisplayName
                             FROM       tblFunction LEFT OUTER JOIN 
                                        tblModule as M ON M_Module_id = F_FK_Module_id
                             WHERE      M_Active = 1 AND F_Trigger = 1 AND 
                                        M_Module_id NOT IN (SELECT DISTINCT MD.M_WebParent 
                                                            FROM tblModule as MD 
                                                            WHERE MD.M_WebParent IS NOT NULL)
                             ORDER BY   M_Module_id, F_Order_id ASC';
        }
        else{
            $query_string = 'SELECT     F_Function_id, F_FunctionName, F_FK_Module_id, M_DisplayName, 
                                        CASE WHEN (SELECT RF_Active 
                                                  FROM tblRoleFunction 
                                                  WHERE RF_FK_RoleID = \''.$role.'\' AND RF_FK_ModuleID = F_FK_Module_id AND RF_FK_FunctionID = F_Function_id AND RF_Active = 1) = 1 THEN 1 ELSE 0 END as DispFunc
                             FROM       tblFunction LEFT OUTER JOIN 
                                        tblModule as M ON M_Module_id = F_FK_Module_id
                             WHERE      M_Active = 1 AND F_Trigger = 1 AND 
                                        M_Module_id NOT IN (SELECT DISTINCT MD.M_WebParent 
                                                        FROM tblModule as MD 
                                                        WHERE MD.M_WebParent IS NOT NULL)
                             ORDER BY   M_Module_id, F_Order_id ASC';
        }

        return $this->db->query($query_string)->result_array();
    }

    public function saveRoleFunctions($data, $type = 'add'){
        if($type == 'add'){
            $this->db->set('DateCreated', 'GETDATE()', false);
            $this->db->set('CreatedBy', getCurrentUser()['login-user']);
            $this->db->insert('tblRoleFunction', $data);
        }
        else{
            $this->db->where('RF_FK_RoleID', $data['RF_FK_RoleID'])
                     ->where('RF_FK_FunctionID', $data['RF_FK_FunctionID'])
                     ->where('RF_FK_ModuleID', $data['RF_FK_ModuleID'])
                     ->delete('tblRoleFunction');

            $this->db->set('DateCreated', 'GETDATE()', false);
            $this->db->set('CreatedBy', getCurrentUser()['login-user']);
            $this->db->insert('tblRoleFunction', $data);
        }
    }

}