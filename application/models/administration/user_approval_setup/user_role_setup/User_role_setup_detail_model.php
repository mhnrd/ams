<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_role_setup_detail_model extends MAIN_Model {

	private $id = NULL;
    private $order_column = array('M_DisplayName', 'RA_Add', 'RA_Edit', 'RA_Edit', 'RA_Delete', 'RA_View', 'RA_Menu');

    public function __construct() {
        parent::__construct();
    }

     /* datatable query starts here */
    public function table_data($group_id){

        
        $this->db->select("M_DisplayName, RA_Add, RA_Edit, RA_Edit, RA_Delete, RA_View, RA_Menu");  
        $this->db->from('tblRoleAccess');
        $this->db->join('tblModule', 'RA_FK_Modules_Code = M_Module_id');  
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables($group_id);
        $output['num_rows']          = $this->get_filtered_data($group_id);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query($group_id){
        $this->db->select("M_DisplayName, RA_Add, RA_Edit, RA_Delete, RA_View, RA_Menu"); 
        $this->db->join('tblModule', 'RA_FK_Modules_Code = M_Module_id');
        $this->db->where('RA_ID', $group_id);

        // Individual column search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('G_Name',trim($_POST['columns'][1]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('G_Description',trim($_POST['columns'][2]["search"]["value"]));
        }

        // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("M_DisplayName", trim($_POST["search"]["value"])); 
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('M_Sequence', 'ASC');  
        }
    }

    public function datatables($group_id){
        $this->query($group_id); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblRoleAccess');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data($group_id){  
           $this->query($group_id);  
           $query = $this->db->get('tblRoleAccess');  
           return $query->num_rows();  
    }

    public function getAllActiveModules($type, $doc_no = ''){
        if($type == 'add'){
        	$query_string = 'SELECT  	M_DisplayName, M_Module_id, M_WebLevel, M_WebParent
        					 FROM 		tblModule
        					 WHERE 		M_Active = 1
        					 ORDER BY 	M_Sequence';

        }
        else{
            $query_string = 'SELECT     M_DisplayName, M_WebParent, M_WebLevel, M_Module_id, 
                                   CASE (SELECT ISNULL(CAST(RA_Add as INT), 0) 
                                         FROM tblRoleAccess
                                         WHERE RA_FK_Modules_Code = M.M_Module_id AND '.$this->sql_hash('RA_ID').' = \''.$doc_no.'\') WHEN 1 THEN 1 ELSE 0 END as RA_Add,
                                   CASE (SELECT ISNULL(CAST(RA_Edit as INT), 0) 
                                         FROM tblRoleAccess
                                         WHERE RA_FK_Modules_Code = M.M_Module_id AND '.$this->sql_hash('RA_ID').' = \''.$doc_no.'\') WHEN 1 THEN 1 ELSE 0 END as RA_Edit,
                                   CASE (SELECT ISNULL(CAST(RA_Delete as INT), 0) 
                                         FROM tblRoleAccess
                                         WHERE RA_FK_Modules_Code = M.M_Module_id AND '.$this->sql_hash('RA_ID').' = \''.$doc_no.'\') WHEN 1 THEN 1 ELSE 0 END as RA_Print,
                                   CASE (SELECT ISNULL(CAST(RA_Delete as INT), 0) 
                                         FROM tblRoleAccess
                                         WHERE RA_FK_Modules_Code = M.M_Module_id AND '.$this->sql_hash('RA_ID').' = \''.$doc_no.'\') WHEN 1 THEN 1 ELSE 0 END as RA_Delete,
                                   CASE (SELECT ISNULL(CAST(RA_View as INT), 0) 
                                         FROM tblRoleAccess
                                         WHERE RA_FK_Modules_Code = M.M_Module_id AND '.$this->sql_hash('RA_ID').' = \''.$doc_no.'\') WHEN 1 THEN 1 ELSE 0 END as RA_View,
                                   CASE (SELECT ISNULL(CAST(RA_Menu as INT), 0) 
                                         FROM tblRoleAccess
                                         WHERE RA_FK_Modules_Code = M.M_Module_id AND '.$this->sql_hash('RA_ID').' = \''.$doc_no.'\') WHEN 1 THEN 1 ELSE 0 END as RA_Menu
                        FROM       tblModule as M
                        WHERE      M_Active = 1
                        ORDER BY   M_Sequence';
        }

    	return $this->db->query($query_string)->result_array();
    }

    public function getLastUGMCount(){
        $query_string = 'SELECT     RA_ID
                         FROM       tblRoleAccess';

        return $this->db->query($query_string)->num_rows() + 1;
    }
}