<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_role_setup_model extends MAIN_Model {

	private $id = NULL;
    private $order_column = array(null, 'R_ID', 'R_Name', 'R_Description');

    public function __construct() {
        parent::__construct();
    }

     /* datatable query starts here */
    public function table_data(){

        
        $this->db->select("*");  
        $this->db->from('tblRole');  
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select("*");

        // Individual column search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('R_Name',trim($_POST['columns'][1]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('R_Description',trim($_POST['columns'][2]["search"]["value"]));
        }

        // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("R_Name", trim($_POST["search"]["value"]));  
                $this->db->or_like("R_Description", trim($_POST["search"]["value"]));  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('R_ID', 'DESC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblRole');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblRole');  
           return $query->num_rows();  
    }

    /* datatable query ends here */ 

    public function getLastGroupCode(){
    	$query_string = 'SELECT 	* 
    					 FROM 		tblRole';

    	return $this->db->query($query_string)->num_rows() + 1;
    }

}