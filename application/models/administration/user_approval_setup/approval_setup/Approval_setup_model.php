<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Approval_setup_model extends MAIN_Model{

	protected $_table = 'tblNoSeries';
    private $id = NULL;

    private $order_column = array(null,null,'NS_Id','NS_Description');

    public function __construct() {
        parent::__construct();
    }

	// // DATA FOR  APPROVAL SETUP HEADER TABLE STARTS HERE
	//  public function getAllNoseries(){
 //        $query_string = 'SELECT     NS_Id, NS_Description
 //                         FROM       tblNoSeries
 //                         WHERE      NS_Active = 1
 //                         ORDER BY   NS_FK_Module_id';

 //        return $this->db->query($query_string)->result_array();
 //    }

       // DATA FOR APPROVAL SETUP DETAIL TABLE STARTS HERE
    public function table_data($status = null){
        $this->db->select('NS_Id, NS_Description');
        $this->db->from('tblNoSeries');
        $result_count = $this->db->count_all_results();

        $output['data']                 = $this->datatables($status);
        $output['num_rows']             = $this->get_filtered_data($status);
        $output['count_all_results']    = $result_count;

        return $output;
    }

    public function query($status){
        $this->db->select('NS_Id, NS_Description');

        // INDIVIDUAL COLUMN SEARCH

        if(isset($_POST['columns'][0]["search"]["value"]) && !empty($_POST['columns'][0]["search"]["value"])){
            $this->db->like('NS_Id',trim($_POST['columns'][0]["search"]["value"]));
        }

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('NS_Description',trim($_POST['columns'][1]["search"]["value"]));
        }


        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("NS_Id", trim($_POST["search"]["value"]));  
                $this->db->or_like("NS_Description", trim($_POST["search"]["value"]));  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('NS_Id', 'DESC');  
        }
    }  

    public function datatables($status){
       $this->query($status); 
       if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblNoSeries');  
        return $query->result_array();
    }

    public function get_filtered_data($status){  
           $this->query($status); 
           $this->db->from('tblNoSeries');
           $query = $this->db->count_all_results(); 
           return $query;  
    }

    
    
}