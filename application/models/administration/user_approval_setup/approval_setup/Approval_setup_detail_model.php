<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Approval_setup_detail_model extends MAIN_Model {

    protected $_table = 'tblNoSeries';
    private $id = NULL;
    private $order_column = array(null,null,'NS_Id','NS_Description');
    private $order_column_detail = array(null,'AS_FK_NS_id','AS_Sequence','AS_FK_Position_id','P_Position','AS_Amount','AS_Unlimited','AS_Required');

    public function __construct() {
        parent::__construct();
    }

     public function table_data($status = null){
        $this->db->select('NS_Id, NS_Description');
        $this->db->from('tblNoSeries');
        $result_count = $this->db->count_all_results();

        $output['data']                 = $this->datatables($status);
        $output['num_rows']             = $this->get_filtered_data($status);
        $output['count_all_results']    = $result_count;

        return $output;
    }

    public function query($status = null){
        $this->db->select('NS_Id, NS_Description');

        // INDIVIDUAL COLUMN SEARCH

        if(isset($_POST['columns'][0]["search"]["value"]) && !empty($_POST['columns'][0]["search"]["value"])){
            $this->db->like('NS_Id',trim($_POST['columns'][0]["search"]["value"]));
        }

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('NS_Description',trim($_POST['columns'][1]["search"]["value"]));
        }

        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("NS_Id", trim($_POST["search"]["value"]));  
                $this->db->or_like("NS_Description", trim($_POST["search"]["value"]));  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('NS_Id', 'DESC');  
        }
    }  

    public function datatables($status = null){
       $this->query($status); 
       if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblNoSeries');  
        return $query->result_array();
    }

    public function get_filtered_data($status = null){  
           $this->query($status); 
           $this->db->from('tblNoSeries');
           $query = $this->db->count_all_results(); 
           return $query;  
    }


     // DATA FOR APPROVAL SETUP DETAIL TABLE STARTS HERE
    public function table_data_detail($status = null){
        $this->db->select('AS_FK_NS_id, AS_Sequence, AS_FK_Position_id, P_Position, AS_Amount, AS_Unlimited, AS_Required');
        $this->db->from('tblApprovalSetup');
        $this->db->join('tblPosition ','AS_FK_Position_id = P_ID', 'left');
         $this->db->join('tblNoSeries ','AS_FK_NS_id = NS_Id', 'left');
        $this->db->where('AS_FK_NS_id', $status);
        $result_count = $this->db->count_all_results();

        $output['data']                 = $this->datatables_detail($status);
        $output['num_rows']             = $this->get_filtered_data_detail($status);
        $output['count_all_results']    = $result_count;

        return $output;

    }

    public function query_detail($status = null){
        $this->db->select('AS_FK_NS_id, AS_Sequence, AS_FK_Position_id, P_Position, AS_Amount, AS_Unlimited, AS_Required');
         $this->db->where('AS_FK_NS_id', $status);

        // INDIVIDUAL COLUMN SEARCH

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('AS_Sequence',trim($_POST['columns'][1]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('P_Position',trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('P_Position',trim($_POST['columns'][2]["search"]["value"]));
        }
        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('AS_Amount',trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('AS_Unlimited',trim($_POST['columns'][3]["search"]["value"]));
        }

        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("AS_Sequence", trim($_POST["search"]["value"]));  
                $this->db->or_like("AS_FK_Position_id", trim($_POST["search"]["value"]));  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column_detail[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('AS_Sequence', 'DESC');  
        }
    }  

    public function datatables_detail($status = null){
       $this->query_detail($status); 
       if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $this->db->join('tblPosition ','AS_FK_Position_id = P_ID', 'left');
        $query = $this->db->get('tblApprovalSetup');  
        return $query->result_array();
    }

    public function get_filtered_data_detail($status = null){  
           $this->query_detail($status); 
           $this->db->from('tblApprovalSetup');
           $this->db->join('tblPosition ','AS_FK_Position_id = P_ID', 'left'); 
           $query = $this->db->count_all_results(); 
           return $query;  
    }

    // DATA TABLE QUERY ENDS HERE

    public function saveTimeEntryRecord($data){
        try {

          $result = $this->db->where('AS_FK_NS_id',$data['AS_FK_NS_id'])
                        ->order_by('AS_Sequence','DESC')
                        ->limit(1)
                        ->get('tblApprovalSetup');

          $lineNo = 1;
          if($result->num_rows() > 0){
            $lineNo = $result->row_array()['AS_Sequence'] + 1;
          }

          if($data['mode'] == 'add'){

            // insert
            unset($data['mode']);
            $data['AS_FK_NS_id']        = $data['AS_FK_NS_id'];
            $data['AS_Sequence']        = $lineNo;
            $data['AS_FK_Position_id']  = $data['AS_FK_Position_id'];
            $data['AS_Amount']          = number_format($data['AS_Amount'], 2);
            $data['AS_Unlimited']       = $data['AS_Unlimited'];
            $data['AS_Required']        = $data['AS_Required'];
            $data['CreatedBy']          = getCurrentUser()['login-user'];
            $data['DateCreated']        = date_format(date_create(),'m/d/Y');
            $this->db->insert('tblApprovalSetup',$data);
          }
          else{
            // update
            $where = array('AS_FK_NS_id' => $data['AS_FK_NS_id'],'AS_Sequence' => $data['AS_Sequence']);

            unset($data['mode']);
            unset($data['AS_FK_NS_id']);
            unset($data['AS_Sequence']);
            $data['AS_FK_Position_id']  = $data['AS_FK_Position_id'];
            $data['AS_Amount']       = number_format($data['AS_Amount'],2);
            $data['AS_Unlimited']      = $data['AS_Unlimited'];
            $data['AS_Required'] =  $data['AS_Required'];
            $data['CreatedBy']    = getCurrentUser()['login-user'];
            $data['DateCreated']  = date_format(date_create(),'m/d/Y');
            $this->db->where($where)->update('tblApprovalSetup',$data);
          }
        } catch (Exception $e) {
          throw new Exception($e->getMessage().", Error , error");
        }
      }
 

     public function getAll($docno){
       $query_string = 'SELECT A.AS_FK_NS_id, A.AS_Sequence, A.AS_Unlimited, A.AS_Amount, A.CreatedBy, A.AS_Required, A.AS_FK_User_id, NS.NS_Description, NS.NS_Id, A.AS_FK_Position_id, P.P_ID, P.P_Position, P.P_Type
                         tblApprovalSetup AS A LEFT OUTER JOIN
                         tblPosition AS P ON A.AS_FK_Position_id = P.P_ID LEFT OUTER JOIN
                         tblNoSeries AS NS ON A.AS_FK_NS_id = NS.NS_Id';
               
        if(!empty($docno)){
            $query_string .= ' WHERE ' . $this->sql_hash('AS_FK_NS_id' ). ' = \''.$docno.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();
        }
        return $result;
    }

    public function searchPositions($type, $filter = '', $position = array()){

        $where = '';
        
        if($filter != ''){
            $where = $type.' LIKE \'%'.$filter.'%\'';
        }

        if(count($position) > 0){
            $where = ( ($filter != '') ? "AND" : "" ).' P_ID NOT IN (\''.implode('\', \'', $position).'\')';
        }

        $query_string = 'SELECT RTRIM(P_Type) as P_Type, RTRIM(P_Position) as P_Position, P_ID
                         FROM tblPosition';

        if($where != ''){
            $query_string .= ' WHERE ' . $where;
        }

        return $this->db->query($query_string)->result_array();
    }   

    public function getAllPositionID(){
        $query_string = 'SELECT P_ID, P_Position
                        FROM tblPosition as PI
                        WHERE P_Active = \'1\'';

        return $this->db->query($query_string)->result_array(); 
    }

    public function generateApprovalSetup($ns_id){
        $query_string = 'SELECT      AS_FK_NS_id, AS_Sequence, AS_FK_Position_id, AS_Amount, AS_Unlimited, AS_Required,
                                    P_Position, P_ID
                         FROM       tblApprovalSetup LEFT OUTER JOIN
                                    tblPosition ON AS_FK_Position_id = P_ID
                         WHERE      AS_FK_NS_id = \''.$ns_id.'\'';

        return $this->db->query($query_string)->result_array(); 

    }

    public function generatePtype($p_type){
        $query_string = 'SELECT     P_Type
                         FROM       tblPosition LEFT OUTER JOIN
                                    tblApprovalSetup ON AS_FK_Position_id = P_Type
                         WHERE      P_Type = \''.$p_type.'\'';

        $current_docno = $this->db->query($query_string)->num_rows() + 1;

        return $p_type.'-'.$this->padLeftZero(10, $current_docno);

    }

    public function generateSequence($sequence){
        $query_string = 'SELECT     AS_Sequence
                         FROM       tblApprovalSetup
                         WHERE      AS_Sequence = \''.$sequence.'\'';

        $current_docno = $this->db->query($query_string)->num_rows() + 1;


        return ''.''.$this->padLeftZero(1, $current_docno);


    }

    private function padLeftZero($num, $txt){

        $pcnt = $num - strlen($txt);
        $output = '';

        for ($i=0; $i < $pcnt; $i++) { 
            $output .= '0';
        }

        $output = $output.$txt;

        return $output;

    }

    public function getAll_Active($click, $filter, $des_type){
         $query_string = 'SELECT     AS_Sequence, AS_FK_Position_id, P_Position, AS_Amount, AS_Unlimited, AS_Required
    
                         FROM       tblCOM_ApprovalSetup A LEFT OUTER JOIN tblCOM_Position P ON AS_FK_Position_id = P_id
                         WHERE     ('.$click.' LIKE \'%'.$filter.'%\' OR AS_FK_NS_id = \''.$ns_id.'\') ORDER BY A.AS_Sequence';

        $current_docno = $this->db->query($query_string)->num_rows() + 1;

        return $ns_id.'-'.$this->padLeftZero(10, $current_docno);
    }

    public function getLastGroupCode(){
        $query_string = 'SELECT     * 
                         FROM       tblApprovalSetup';

        return $this->db->query($query_string)->num_rows() + 1;
    }
}