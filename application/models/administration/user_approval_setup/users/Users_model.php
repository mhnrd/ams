<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends MAIN_Model {

    private $id = NULL;
    private $order_column = array(null,null,'U_ID','U_Username','U_FK_Position_id','U_Status');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data(){

        
        $this->db->select("*");  
        $this->db->from('tblUser');  
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('U_ID, U_Username, P_Position  AS U_FK_Position_id, CASE U_Status WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as U_Status', false);

        // Individual column search

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('U_ID',trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('U_Username',trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('P_Position',trim($_POST['columns'][4]["search"]["value"]));
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('CASE U_Status WHEN 1 THEN \'Active\' ELSE \'Inactive\' END',trim($_POST['columns'][5]["search"]["value"]));
        }


        // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->group_start();
                $this->db->like("U_ID", trim($_POST["search"]["value"]));  
                $this->db->or_like("U_Username", trim($_POST["search"]["value"]));  
                $this->db->or_like("P_Position", trim($_POST["search"]["value"]));  
                $this->db->or_like('CASE U_Status WHEN 1 THEN \'Active\' ELSE \'Inactive\' END', trim($_POST["search"]["value"])); 
                $this->db->group_end(); 
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('U_ID', 'DESC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }

        $this->db->join('tblPosition','P_ID = U_FK_Position_id','left');
        $query = $this->db->get('tblUser');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $this->db->join('tblPosition','P_ID = U_FK_Position_id','left');
           $query = $this->db->get('tblUser');  
           return $query->num_rows();  
    }

    /* datatable query ends here */ 

    public function getUserPass($user){
        $query_string = 'SELECT       U_Password
                         FROM         tblUser
                         WHERE        U_ID = \''.$user.'\'';

        return $this->db->query($query_string)->row_array()['U_Password'];
    }

    public function getAllActivePosition(){
        $query_string = 'SELECT     P_ID, P_Position
                         FROM       tblPosition
                         WHERE      P_Active = 1';

        return $this->db->query($query_string)->result_array();
    }

    public function getAllUserRoles(){
        $query_string = 'SELECT     R_ID, R_Description
                         FROM       tblRole';

        return $this->db->query($query_string)->result_array();
    }

    public function saveUserRoles($user, $user_role, $type = 'add'){

        if($type == 'add'){
            foreach ($user_role as $key => $value) {
                $array_ur = array(
                    'UR_FK_UserID'  => $user,
                    'UR_FK_RoleID'  => $value
                );

                $this->db->insert('tblUserRole', $array_ur);
            }
        }
        else{

            $this->db->where('UR_FK_UserID', $user)
                     ->delete('tblUserRole');

            foreach ($user_role as $key => $value) {
                $array_ur = array(
                    'UR_FK_UserID'  => $user,
                    'UR_FK_RoleID'  => $value
                );

                $this->db->insert('tblUserRole', $array_ur);
            }

        }
    }

    public function loadLocAccess($user){
        $query_string = 'SELECT AD_ID,
                                AD_Desc
                        FROM    tblAttributeDetail
                        WHERE   AD_FK_Code = 400003 AND
                                AD_Active = 1';

        return $this->db->query($query_string)->result_array();
    }

    public function loadAllUsers(){
        $query_string = 'SELECT     U_ID, U_Username
                         FROM       tblUser
                         WHERE      U_Status = 1';

        return $this->db->query($query_string)->result_array();
    }

    public function getUsersDefaultLocation($user, $emp_id){
        $query = 'SELECT    CA_FK_Location_id
                  FROM      tblCompanyAccess
                  WHERE     CA_U_ID = \''.$user.'\' AND CA_DefaultLocation = 1';

        $location = $this->db->query($query)->row_array()['CA_FK_Location_id'];

        if($location == '' || $location === null){
            $query = 'SELECT    Emp_FK_StoreID
                      FROM      tblEmployee
                      WHERE     Emp_Id = \''.$emp_id.'\'';

            $location = $this->db->query($query)->row_array()['Emp_FK_StoreID'];
        }

        return $location;
    }
    public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */

        $query_role = 'SELECT   UR_FK_UserID
                       FROM     tblUserRole
                       WHERE    UR_FK_UserID = \''.$docno.'\'';

        $roles = $this->db->query($query_role)->result_array();

        if(!empty($roles)){
            return array('success'=>0);
        }

        $this->db->delete('tblUser',array('U_ID'=>$docno));
        
        if ($this->db->trans_status() === FALSE){
                return array('success'=>0);
        }
        else{
                return array('success'=>1);
        }

    }
    public function status_data($docno,$action){
  
        foreach ($docno as $row) {
            if ($action == 'active') {
                $this->db->where('U_ID',$row)->update('tblUser', array('U_Status' => '1'));
            }else{
                $this->db->where('U_ID',$row)->update('tblUser', array('U_Status' => '0'));
            }
        }
        if ($this->db->trans_status() === FALSE){
                return array('success'=>0);
        }
        else{
                return array('success'=>1);
        }
    }

}
