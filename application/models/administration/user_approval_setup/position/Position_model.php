<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Position_model extends MAIN_Model {

    protected $_table = 'tblPosition';

    private $id = NULL;
    private $order_column = array(null, null, 'CAST(P_ID as int)', 'P_Position', 'P_Type', 'P_Active', 'PT_PositionType', 'P_Active');

    public function __construct() {
        parent::__construct();
    }

    public function searchStyles($type, $filter){
        $query_string = 'SELECT     RTRIM(PT_PositionType_id) as PT_PositionType_id,RTRIM(PT_PositionType) as PT_PositionType
                         FROM       tblPositionType
                         WHERE      '.$type.' LIKE \'%'.$filter.'%\'';

        return $this->db->query($query_string)->result_array();
    }

    public function searchParentID($type, $filter){
        $query_string = 'SELECT     RTRIM(P_ID) as P_ID,RTRIM(P_Position) as P_Position
                         FROM       tblPosition
                         WHERE      '.$type.' LIKE \'%'.$filter.'%\'';

        return $this->db->query($query_string)->result_array();
    }

  

    /* datatable query starts here */
    public function table_data($status = null){

        
        $this->db->select('P_ID, P_Position ,P_Type, P_Active, PT_PositionType, PT_PositionType_id');
        //$this->db->where('tblCPCenter.CreatedBy', getCurrentUser()['login-user']);
        $this->db->from('tblPosition');
        $this->db->join('tblPositionType', 'PT_PositionType_id = P_Type', 'left');
        $this->db->where('CAST(P_ID as int) > 0');
        $this->db->where('P_Type IS NOT NULL');
        $this->db->where('PT_PositionType_id IS NOT NULL');
        $result_count = $this->db->count_all_results(); 

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('P_ID, P_Position ,P_Type, CASE P_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as P_Active, PT_PositionType, PT_PositionType_id', false);
        $this->db->join('tblPositionType', 'PT_PositionType_id = P_Type', 'left');
        $this->db->where('CAST(P_ID as int) > 0');
        $this->db->where('P_Type IS NOT NULL');
        $this->db->where('PT_PositionType_id IS NOT NULL');
        //$this->db->where('tblCPCenter.CreatedBy', getCurrentUser()['login-user']);

        // Individual Column Search

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('P_ID', trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('P_Position', trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('PT_PositionType', trim($_POST['columns'][4]["search"]["value"]));
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('CASE P_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END', trim($_POST['columns'][5]["search"]["value"]), 'after');
        }
        
        // End
        
        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){
                $this->db->group_start();
                $this->db->like("P_ID", trim($_POST["search"]["value"])); 
                $this->db->or_like("P_Position", trim($_POST["search"]["value"]));
                $this->db->or_like("PT_PositionType", trim($_POST["search"]["value"]));          
                $this->db->or_like("CASE P_Active WHEN 1 THEN 'Active' ELSE 'Inactive' END", trim($_POST["search"]["value"]), 'after');
                $this->db->group_end();     
            }
        }


        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir'], false);  
        }  
        else{  
            $this->db->order_by('P_ID', 'ASC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblPosition');  
        return $query->result_array();
    }

     public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblPosition');  
           return $query->num_rows();  
    }

    public function getPositionType(){
        $query_string = 'SELECT PT_PositionType_id, PT_PositionType, PT_Active
                         FROM tblPositionType AS PT
                         WHERE PT_Active = \'1\'';
        return $this->db->query($query_string)->result_array();
    }
    public function getPositionParent($p_type){
        $query_string = 'SELECT P_ID, P_Position   
                         FROM tblPosition as P
                         WHERE P_Active = \'1\' AND CAST(P_Type as INT) > '.$p_type;
        return $this->db->query($query_string)->result_array();
    }
    public function getParent(){
        $query_string = 'SELECT PT_PositionType
                         FROM tblPositionType as P
                         WHERE PT_Active = \'1\'';
        return $this->db->query($query_string)->result_array();
    }

    public function generatePID($p_id){
        $query_string = 'SELECT     TOP 1 CAST(P_ID as int) as P_ID
                         FROM       tblPosition
                         WHERE      P_Type = \''.$p_id.'\'
                         ORDER BY   P_ID DESC';

        $current_docno = $this->db->query($query_string)->row_array()['P_ID'] + 1;


        return $current_docno;

    }


}
