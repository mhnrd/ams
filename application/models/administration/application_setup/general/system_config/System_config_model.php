<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class System_config_model extends MAIN_Model {

    protected $_table = 'tblSystemConfig';

    private $id = NULL;
    private $order_column = array(null, 'SC_ID', 'SC_ModuleID','SC_Description', 'SC_Value');

    public function __construct() {
        parent::__construct();
    }

  

    /* datatable query starts here */
    public function table_data(){

        
        $this->db->select('SC_ID, SC_ModuleID, SC_Description, SC_Value');
        $this->db->from('tblSystemConfig');
        $result_count = $this->db->count_all_results(); 

        $result_item =  $this->datatables();
       foreach ($result_item as $key => $value) {
            
        }

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('SC_ID, M_Description, SC_Description, SC_Value');
        $this->db->join('tblModule', 'M_Module_id = SC_ModuleID', 'left');

        // Individual Column Search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('SC_ID', trim($_POST['columns'][1]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('M_Description', trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('SC_Description', trim($_POST['columns'][3]["search"]["value"]));
        }

         if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('CAST(SC_Value as varchar)', trim($_POST['columns'][4]["search"]["value"]), 'after');
        }

        // End
        
        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){
                $this->db->like("SC_ID", trim($_POST["search"]["value"])); 
                $this->db->or_like("M_Description", trim($_POST["search"]["value"]));
                $this->db->or_like("SC_Description", trim($_POST["search"]["value"]), 'after');           
                $this->db->or_like("CAST(SC_Value as varchar)", trim($_POST["search"]["value"]), 'after');           
            }
        }


        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('SC_ID', 'DESC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblSystemConfig');  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblSystemConfig');  
           return $query->num_rows();  
    }

    public function getAllConfigModules(){

        $query_string = 'SELECT DISTINCT        CAST(SC_ModuleID as INT) as SC_ModuleID, M_DisplayName
                         FROM                   tblSystemConfig LEFT OUTER JOIN
                                                tblModule ON M_Module_id = SC_ModuleID
                         ORDER BY               CAST(SC_ModuleID as INT) ASC';

        return $this->db->query($query_string)->result_array();

    }

    public function searchModulesByFilter($filter){
        $query_string = 'SELECT         M_Module_id, M_DisplayName
                         FROM           tblModule
                         WHERE          (M_Module_id LIKE \'%'.$filter.'%\') OR (M_DisplayName LIKE \'%'.$filter.'%\')';

        return $this->db->query($query_string)->result_array();
    }

    public function generateConfigID($module){
        $query_string = 'SELECT         TOP (1) SC_ID
                         FROM           tblSystemConfig
                         WHERE          SC_ModuleID = \''.$module.'\'
                         ORDER BY       SC_ID DESC';

        $result = $this->db->query($query_string)->row_array()['SC_ID'];

        if($result == ''){
            $current_id = '1';
        }
        else{
            $current_id = substr($result, -1, 1) + 1;
        }

        return $module.'-'.$this->padLeftZero(3, $current_id);
    }

    private function padLeftZero($num, $txt){

        $pcnt = $num - strlen($txt);
        $output = '';

        for ($i=0; $i < $pcnt; $i++) { 
            $output .= '0';
        }

        $output = $output.$txt;

        return $output;

    }
    

}
