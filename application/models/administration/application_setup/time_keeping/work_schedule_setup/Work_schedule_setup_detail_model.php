<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Work_schedule_setup_detail_model extends MAIN_Model {
 private $order_column = array('WS_Weekday', 'S_Name');
    // Detail Datatable

    public function table_data($doc_no){        
        
        $this->db->select('DOW_Desc, S_Name');
        $this->db->from('tblWorkSchedule');
        $this->db->join('tblDayOfWeek', 'DOW_ID = WS_WeekDay', 'left');
        $this->db->join('tblShift', 'S_ID = WS_FK_Shift_id', 'left');
        $this->db->where('WS_DocDate', $doc_no);
      
        $result_count = $this->db->count_all_results();

        $output['data']              = $this->datatables($doc_no);
        $output['num_rows']          = $this->get_filtered_data($doc_no);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query($doc_no){
        $this->db->select('DOW_Desc, RTRIM(S_ID) + \' - \' + S_Name + \' (\' + S_StartTime + \' - \' + S_EndTime + \')\' as Shift');
        $this->db->join('tblDayOfWeek', 'DOW_ID = WS_WeekDay', 'left');
        $this->db->join('tblShift', 'S_ID = WS_FK_Shift_id', 'left');
        $this->db->where('WS_DocDate', $doc_no);

        // End

        if(isset($_POST["search"]["value"])){  
            if($_POST["search"]["value"] != ''){
                $this->db->group_start();
                $this->db->like("DOW_Desc", $_POST["search"]["value"]);  
                $this->db->or_like('RTRIM(S_ID) + \' - \' + S_Name + \' (\' + S_StartTime + \' - \' + S_EndTime + \')\'', $_POST["search"]["value"]);
                $this->db->group_end();  
            }
        } 

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{
            $this->db->order_by('WS_Weekday', 'ASC');  
        }
    }

    public function datatables($doc_no){
        $this->query($doc_no); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblWorkSchedule');  
        return $query->result_array();
    }

    public function get_filtered_data($doc_no){  
           $this->query($doc_no);  
           $query = $this->db->get('tblWorkSchedule');  
           return $query->num_rows();  
    }

    // END

    private function getShiftTime($shift){

        $query_string = 'SELECT     S_StartTime, S_EndTime
                         FROM       tblShift
                         WHERE      S_ID = \''.$shift.'\'';

        return $this->db->query($query_string)->row_array();

    }
    function getAllShifts(){

    $CI = get_instance();

    $query = 'SELECT     RTRIM(S_ID) as S_ID, RTRIM(S_Name) as S_Name, RTRIM(S_StartTime) as S_StartTime, RTRIM(S_EndTime) as S_EndTime
              FROM       tblShift';

    return $CI->db->query($query)->result_array();
}
}