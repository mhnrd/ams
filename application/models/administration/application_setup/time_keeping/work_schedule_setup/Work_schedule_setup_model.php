<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Work_schedule_setup_model extends MAIN_Model {

    protected $_table = 'tblWorkSchedule';

    private $id = NULL;
    private $order_column = array(null,'Emp_Id','Emp_Name',);
      private $order_column_id_history = array(null,'WS_DocDate','WS_WeekDay', 'WS_FK_Shift_id', 'WS_Description');
    public function __construct() {
        parent::__construct();
    }
    public function searchStyles($type, $filter){
        $query_string = 'SELECT     RTRIM(S_ID) as S_ID,RTRIM(S_Name) as S_Name
                         FROM       tblShift
                         WHERE      '.$type.' LIKE \'%'.$filter.'%\'';

        return $this->db->query($query_string)->result_array();
    }
    

  

    /* datatable query starts here */
    public function table_data($status = null){

        
        $this->db->select('LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1))) AS Emp_Name,Emp_Id',false);
        //$this->db->where('tblPAY_Group.CreatedBy', getCurrentUser()['login-user']);
        if(count($status) == 0){
          $this->db->where('isnull(Emp_Active,0)','1');
        }
        else{
          if(isset($status['chkInactive'])){
            if($status['chkInactive'] == 'true'){
              $this->db->where('isnull(Emp_Active,0)', '1');
            }
            else{
              $this->db->where('isnull(Emp_Active,0)', '0'); 
            }
          }

          if(isset($status['filterBy']) && isset($status['searchBy'])){
              switch ($status['filterBy']) {
                case 'Department':
                  $this->db->where('Emp_DepartmentId',$status['searchBy']);
                  break;
                case 'Location':
                  $this->db->where('Emp_FK_StoreID',$status['searchBy']);
                  break;
                case 'Position':
                  $this->db->where('Emp_PositionId',$status['searchBy']);
                  break;
                case 'Principal':
                  $this->db->where('Emp_CustomerId',$status['searchBy']);
                  break;
                case 'PayGroup':
                  $this->db->where('Emp_FK_GroupId',$status['searchBy']);
                  break;
                default:
                  break;
              }
          }
      }
        
        $this->db->from('tblEmployee');
        $result_count = $this->db->count_all_results(); 
        $output['data']              = $this->datatables($status);
        $output['num_rows']          = $this->get_filtered_data($status);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query($status = null){
        $this->db->select('Emp_Id,LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1))) AS Emp_Name',false);
         // $this->db->join('');
        if(count($status) == 0){
      $this->db->where('isnull(Emp_Active,0)','1');
    }
    else{
      if(isset($status['chkInactive'])){
        if($status['chkInactive'] != 'true'){
          $this->db->where('isnull(Emp_Active,0)', '1');
        }
        else{
          $this->db->where('isnull(Emp_Active,0)', '0'); 
        }
      }

      if(isset($status['filterBy']) && isset($status['searchBy'])){
          switch ($status['filterBy']) {
            case 'Department':
              $this->db->where('Emp_DepartmentId',$status['searchBy']);
              break;
            case 'Location':
              $this->db->where('Emp_FK_StoreID',$status['searchBy']);
              break;
            case 'Position':
              $this->db->where('Emp_PositionId',$status['searchBy']);
              break;
            case 'Principal':
              $this->db->where('Emp_CustomerId',$status['searchBy']);
              break;
            case 'PayGroup':
              $this->db->where('Emp_FK_GroupId',$status['searchBy']);
              break;
            default:
              break;
          }
      }
    }

        //$this->db->where('tblPAY_Group.CreatedBy', getCurrentUser()['login-user']);
        // Individual Column Search
        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('Emp_Id',trim($_POST['columns'][1]["search"]["value"]));
        }
        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1)))',trim($_POST['columns'][2]["search"]["value"]));
        }

        

        // if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
        //     $this->db->like('CASE Emp_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END',trim($_POST['columns'][4]["search"]["value"]));
        // }

       


        // End
        
        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){
                 $this->db->group_start();
                $this->db->like("Emp_ID", trim($_POST["search"]["value"]));
                $this->db->or_like('LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1)))', $_POST["search"]["value"],false);
                // $this->db->or_like("Emp_FirstName", trim($_POST["search"]["value"]));
                // $this->db->or_like("CASE Emp_Active WHEN 1 THEN 'Active' ELSE 'Inactive' END", trim($_POST["search"]["value"]), 'after');
               $this->db->group_end();  


            }
        }

        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('Emp_Id', 'ASC');  
        }
    }

    public function datatables($status = null){
        $this->query($status); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblEmployee');  
        return $query->result_array();
    }
   
    public function get_filtered_data($status = null){  
           $this->query($status);  
           $query = $this->db->get('tblEmployee');  
           return $query->num_rows();  
    }

// DATA ID HISTORY TABLE STARTS HERE
  public function table_data_workSchedule($status = null){

    $this->db->select('WS_DocDate,CASE WS_WeekDay WHEN \'1\' THEN \'Sunday\' 
                                     WHEN \'2\' THEN \'Monday\'
                                     WHEN \'3\' THEN \'Tuesday\'
                                     WHEN \'4\' THEN \'Wednesday\'
                                     WHEN \'5\' THEN \'Thursday\'
                                     WHEN \'6\' THEN \'Friday\'
                                     WHEN \'7\' THEN \'Saturday\'
                                     ELSE \'\' END as WS_WeekDay, WS_FK_Shift_id, WS_Description,WS_LineNo,WS_Emp_id',false);
    $this->db->join('tblEmployee','WS_Emp_id = Emp_id','left');
    $this->db->where('WS_Emp_id',$status);
    $this->db->from('tblWorkSchedule');
    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_WorkSchedule($status);
    $output['num_rows']       = $this->get_filtered_data_WorkSchedule($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_WorkSchedule($status = null){
    
    $this->db->select('WS_DocDate,CASE WS_WeekDay WHEN \'1\' THEN \'Sunday\' 
                                     WHEN \'2\' THEN \'Monday\'
                                     WHEN \'3\' THEN \'Tuesday\'
                                     WHEN \'4\' THEN \'Wednesday\'
                                     WHEN \'5\' THEN \'Thursday\'
                                     WHEN \'6\' THEN \'Friday\'
                                     WHEN \'7\' THEN \'Saturday\'
                                     ELSE \'\' END as WS_WeekDay, WS_FK_Shift_id, WS_Description,WS_LineNo,WS_Emp_id',false);

    $this->db->where('WS_Emp_id',$status);

    // INDIVIDUAL COLUMN SEARCH
    if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('WS_DocDate',trim($_POST['columns'][1]["search"]["value"]));
        }

    if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('CASE WS_WeekDay WHEN \'1\' THEN \'Sunday\'  WHEN \'2\' THEN \'Monday\' WHEN \'3\' THEN \'Tuesday\' WHEN \'4\' THEN \'Wednesday\' WHEN \'5\' THEN \'Thursday\' WHEN \'6\' THEN \'Friday\' WHEN \'7\' THEN \'Saturday\'  ELSE \'\' END ',trim($_POST['columns'][2]["search"]["value"]));
        }
         if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('WS_FK_Shift_id',trim($_POST['columns'][3]["search"]["value"]));
        }

        if (isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])) {
            $this->db->like('WS_Description', trim($_POST['columns'][4]["search"]["value"]));
        }

    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){ 
            $this->db->like("WS_DocDate", trim($_POST["search"]["value"])); 
            $this->db->or_like('CASE WS_WeekDay WHEN \'1\' THEN \'Sunday\' WHEN \'2\' THEN \'Monday\' WHEN \'3\' THEN \'Tuesday\' WHEN \'4\' THEN \'Wednesday\' WHEN \'5\' THEN \'Thursday\' WHEN \'6\' THEN \'Friday\' WHEN \'7\' THEN \'Saturday\' ELSE \'Split\' END', trim($_POST["search"]["value"]));
            $this->db->or_like("WS_FK_Shift_id", trim($_POST["search"]["value"]));
            $this->db->or_like("WS_Description", trim($_POST["search"]["value"]));
              
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_id_history[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('WS_DocDate', 'ASC');  
    }
  }

  public function datatables_WorkSchedule($status = null){
    $this->query_WorkSchedule($status); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblWorkSchedule');  
        return $query->result_array();
  }

  public function get_filtered_data_WorkSchedule($status = null){  
         $this->query_WorkSchedule($status);  
         $query = $this->db->get('tblWorkSchedule');  
         return $query->num_rows();  
  }

  // END 


    public function getCompany(){
        $query_string ='SELECT COM_Id,COM_Name
                        FROM tblCompany 
                        WHERE COM_Active = \'1\' ';
        return $this->db->query($query_string)->result_array();
    }
    public function getPayGroup(){
        $query_string ='SELECT G_ID 
                        FROM tblPayGroup 
                         ';
        return $this->db->query($query_string)->result_array();
    }
    public function getShiftID(){
        $query_string ='SELECT S_ID 
                        FROM tblShift 
                         ';
        return $this->db->query($query_string)->result_array();
    }


    

     public function getAllEmployeeData($employee_id){
        $output         = array();
        $output['general_info']     = $this->getGeneralInfo($employee_id);
        return $output;
    }

    public function getGeneralInfo($employee_id){

        $attributes_model     = $this->load_model('administration/application_setup/general/attributes/attributes_model');

        $sql_query = 'SELECT a.Emp_Id 
                      FROM tblEmployee AS a 
                           LEFT OUTER JOIN tblAttributeDetail AS ad 
                                        ON a.Emp_AttendanceType = ad.AD_Id 
                           LEFT OUTER JOIN tblAttributeDetail AS i 
                                        ON a.Emp_DepartmentId = i.AD_Id 
                           LEFT OUTER JOIN tblAttributeDetail AS h 
                                        ON a.Emp_FK_Status_Attribute_id = h.AD_Id 
                           LEFT OUTER JOIN tblCustomer AS g 
                                        ON a.Emp_CustomerId = g.C_Id 
                           LEFT OUTER JOIN tblStoreProfile AS f 
                                        ON a.Emp_FK_StoreID = f.SP_ID 
                                           AND a.Emp_CompanyId = f.SP_FK_CompanyID 
                           LEFT OUTER JOIN tblCompany AS c 
                                        ON a.Emp_CompanyId = c.COM_Id 
                           LEFT OUTER JOIN tblPayGroup AS e 
                                        ON a.Emp_CompanyId = e.G_FK_CompanyId 
                                           AND a.Emp_FK_GroupId = e.G_Id 
                           LEFT OUTER JOIN tblTaxCategory AS d 
                                        ON a.Emp_TaxExemptId = d.TC_ID 
                           LEFT OUTER JOIN tblPosition AS b 
                                        ON a.Emp_PositionId = b.P_id 
                     WHERE ( '.$this->sql_hash('a.Emp_Id').' = \''.$employee_id.'\' ) ';
        $result = $this->db->query($sql_query)->row_array();

        $result['Emp_Name']                 = ucfirst(strtolower($result['Emp_LastName'] . ', ' . $result['Emp_FirstName'] . ' ' . substr($result['Emp_MiddleName'],1,1)));

       

        return $result;
    }

     public function getFilteredData($type, $filter, $addtl = null, $filter_input = null){

      $where = '';
      switch ($type) {
        case 'store':
          if($filter_input != ''){
            $where = "AND SP_StoreName LIKE '%". $filter_input ."%' ";
          }
          $query_string = 'SELECT LTRIM(RTRIM(SP_StoreName)) as Description,  LTRIM(RTRIM(SP_ID)) AS Id  
                           From tblStoreProfile  
                           WHERE (SP_FK_CompanyID = \''.$filter.'\') '.$where.'
                           ORDER BY Description';
          break;
        case 'department':
          if($filter_input != ''){
              // $where = "AND b.P_Position LIKE '%". $filter_input ."%' ";
              $where = "AND P_Position LIKE '%". $filter_input ."%' ";
          }
          // $query_string = 'SELECT LTRIM(RTRIM(b.P_Position)) as Description,  LTRIM(RTRIM(a.DS_FK_Position_id)) AS Id   
          //                  FROM tblDepartmentSetup AS a LEFT OUTER JOIN tblPosition AS b ON a.DS_FK_Position_id = b.P_id  
          //                  WHERE (a.DS_FK_Department_id = \''.$filter.'\') AND (ISNULL(b.P_Active, 0) = 1)   '.$where.'
          //                  ORDER BY Description';
          $query_string = 'SELECT LTRIM(RTRIM(P_Position)) as Description,  LTRIM(RTRIM(P_ID)) AS Id   
                           FROM  tblPosition
                           WHERE (P_Dept = \''.$filter.'\') AND (ISNULL(P_Active, 0) = 1)   '.$where.'
                           ORDER BY Description';
          break;
        case 'paygroup':
          if($filter_input != ''){
            $where = "AND G_Desc LIKE '%". $filter_input ."%' ";
          }
          $query_string = 'SELECT LTRIM(RTRIM(G_Desc)) as Description,  LTRIM(RTRIM(G_Id)) AS Id
                           FROM tblPAYGroup 
                           WHERE (G_FK_CompanyId = \''.$filter.'\')
                           ORDER BY Description';
          break;
        case 'attendancegroup':
          if($filter_input != ''){
            $where = "AND a.AD_Desc LIKE '%". $filter_input ."%' ";
          }
          $query_string  = " SELECT LTRIM(RTRIM(a.AD_Desc)) as Description,  LTRIM(RTRIM(tr.TR_AttendanceType)) AS Id
                             FROM tblPAYTimekeepingRules AS tr LEFT OUTER JOIN tblAttributeDetail AS a ON tr.TR_AttendanceType = a.AD_Id 
                             WHERE  (tr.TR_PKFK_CompanyId = '".$filter."') AND (tr.TR_GroupId ='".$addtl."')  ".$where."
                             ORDER BY Description";
          break;
        case 'payitems':
          if($filter_input != '' && trim($addtl) == 'Description ASC'){
            $where = "AND PI_Desc LIKE '%". $filter_input ."%' ";
          }
          else if($filter_input != '' && trim($addtl) == 'Id ASC'){
            $where = "AND PI_id LIKE '%". $filter_input ."%' ";
          }
          $query_string = 'SELECT LTRIM(rTRIM(PI_id)) AS Id, LTRIM(RTRIM(PI_Desc)) AS Description  
                           From tblPAYItems  
                           WHERE (IsNull(PI_Allowance, 0) = 1) '.$where.' AND  
                                   (PI_id NOT IN (SELECT   Allow_Id  
                                                   From tblAllowance  
                                                   WHERE     (Allow_Id = tblPAYItems.PI_id) AND (FK_Emp_Id = \''.$filter.'\')))
                           ORDER BY '.$addtl.' ';
          break;
        default:
          return;
          break;
      }
      
      return $this->db->query($query_string)->result_array();
  }

    public function getFilteredByData($filter, $filter_input = null){

      $where = '';
      switch ($filter) {
        case 'Department':
          $where = " AND LTRIM(RTRIM(AD_Desc)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(AD_Desc)) as Description,  LTRIM(RTRIM(AD_Id)) AS Id   
                           FROM  tblAttributeDetail
                           WHERE (ISNULL(AD_Active, 0) = 1) AND (AD_FK_Code = \''.DEPARTMENT.'\') '.$where.'
                           ORDER BY Description';
          break;
        case 'Position':
          $where = " AND LTRIM(RTRIM(P_Position)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(P_Position)) as Description,  LTRIM(RTRIM(P_ID)) AS Id   
                           FROM  tblPosition
                           WHERE (ISNULL(P_Active, 0) = 1) '.$where.'
                           ORDER BY Description';
          break;
        case 'Location':
          $where = " AND LTRIM(RTRIM(SP_StoreName)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(SP_StoreName)) as Description,  LTRIM(RTRIM(SP_ID)) AS Id   
                           FROM  tblStoreProfile
                           WHERE (ISNULL(SP_Active, 0) = 1) '.$where.'
                           ORDER BY Description';
          break;
        case 'Principal':
          $where = " AND LTRIM(RTRIM(C_Name)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(C_Name)) as Description,  LTRIM(RTRIM(C_Id)) AS Id   
                           FROM  tblCustomer
                           WHERE (ISNULL(C_Active, 0) = 1) '.$where.'
                           ORDER BY Description';
          break;
        case 'PayGroup':
          $where = "WHERE LTRIM(RTRIM(G_Desc)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(G_Desc)) as Description,  LTRIM(RTRIM(G_Id)) AS Id   
                           FROM  tblPayGroup '.$where.'
                           ORDER BY Description';
          break;
        default:
          return;
          break;
      }
      return $this->db->query($query_string)->result_array();
  }
  public function saveWorkScheduleRecord($data){
    try {

      $result = $this->db->where('WS_Emp_id',$data['WS_Emp_id'])
                    ->order_by('WS_LineNo','DESC')
                    ->limit(1)
                    ->get('tblWorkSchedule');

      $lineNo = 1;
      if($result->num_rows() > 0){
        $lineNo = $result->row_array()['WS_LineNo'] + 1;
      }

      if($data['mode'] == 'add'){
        // insert
        unset($data['mode']);
        $data['WS_LineNo']  = $lineNo;
        $data['WS_DocDate']  = date_format(date_create(),'m/d/Y');
        $data['CreatedBy']    = getCurrentUser()['login-user'];
        $data['DateCreated']  = date_format(date_create(),'m/d/Y');
        $this->db->insert('tblWorkSchedule',$data);
      }
      else{
        // update
        $where = array('WS_Emp_id' => $data['WS_Emp_id'],'WS_LineNo' => $data['WS_LineNo']);

        unset($data['mode']);
        unset($data['WS_Emp_id']);
        unset($data['WS_LineNo']);
        $data['ModifiedBy']    = getCurrentUser()['login-user'];
        $data['DateModified']  = date_format(date_create(),'m/d/Y');
        $this->db->where($where)->update('tblWorkSchedule',$data);
      }
    } catch (Exception $e) {
      throw new Exception($e->getMessage().", Error , error");
    }
  }

  public function fetch_data()
    {
        $this->db->order_by('WS_Emp_id', 'ASC');
        $query = $this->db->get('tblWorkSchedule');
        return $query->result();
    }

  public function getDescription(){
    $query_string = " SELECT 
       [Description] = value

    FROM  INFORMATION_SCHEMA.COLUMNS i_s
         LEFT OUTER JOIN
          sys.extended_properties s
    ON
           s.major_id = OBJECT_ID(i_s.TABLE_SCHEMA+'.'+i_s.TABLE_NAME)
           AND s.minor_id = i_s.ORDINAL_POSITION
           AND s.name = 'MS_Description'
    WHERE TABLE_NAME = 'tblWorkSchedule'

    ORDER BY ORDINAL_POSITION ";
      
    return $this->db->query($query_string)->result_array();
  }




}
