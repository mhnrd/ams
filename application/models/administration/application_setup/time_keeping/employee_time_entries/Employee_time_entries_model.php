<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employee_time_entries_model extends MAIN_Model {

	protected $_table = 'tblEmployee';

    private $id = NULL;
    private $order_column = array(null,null,'Emp_Id','Emp_Name');
    private $order_column_time_entry_dtl = array(null,'C_DateSwipe', 'C_DOW_In', 'C_DOW_Out','C_TimeIn', 'C_TimeOut','C_BioUpload','C_BreakOut_1','C_BreakIn_1','C_BreakOut_2','C_BreakIn_2','C_BreakOut_3','C_BreakIn_3');
   private $order_column_time_log = array(null,'C_DateSwipe','C_DOW_In','C_TimeIn','C_TimeOut','C_Schedule','C_WorkHrs','C_RegHrs','C_AbsentHrs','C_LateHrs','C_UnderTimeHrs','C_OTHrs','C_NightDiff','C_RestDayOT','C_Holiday','C_HolidayHrs','C_LeavePay');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data($status = null){

       $this->db->select('LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1))) AS Emp_Name,Emp_Id',false);

        if(count($status) == 0){
          $this->db->where('isnull(Emp_Active,0)','1');
        }
        else{
          if(isset($status['chkInactive'])){
            if($status['chkInactive'] == 'true'){
              $this->db->where('isnull(Emp_Active,0)', '1');
            }
            else{
              $this->db->where('isnull(Emp_Active,0)', '0'); 
            }
          }

          if(isset($status['filterBy']) && isset($status['searchBy'])){
              switch ($status['filterBy']) {
                case 'Department':
                  $this->db->where('Emp_DepartmentId',$status['searchBy']);
                  break;
                case 'Location':
                  $this->db->where('Emp_FK_StoreID',$status['searchBy']);
                  break;
                case 'Position':
                  $this->db->where('Emp_PositionId',$status['searchBy']);
                  break;
                case 'Principal':
                  $this->db->where('Emp_CustomerId',$status['searchBy']);
                  break;
                case 'PayGroup':
                  $this->db->where('Emp_FK_GroupId',$status['searchBy']);
                  break;
                default:
                  break;
              }
          }

        }


        $this->db->from('tblEmployee');
            $result_count = $this->db->count_all_results();
            $output['data']                 = $this->datatables($status);
            $output['num_rows']             = $this->get_filtered_data($status);
            $output['count_all_results']    = $result_count;

            return $output;
    }   

    public function query($status = null){

        $this->db->select('LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1))) AS Emp_Name,Emp_Id',false);

        if(count($status) == 0){
          $this->db->where('isnull(Emp_Active,0)','1');
        }
        else{
          if(isset($status['chkInactive'])){
            if($status['chkInactive'] != 'true'){
              $this->db->where('isnull(Emp_Active,0)', '1');
            }
            else{
              $this->db->where('isnull(Emp_Active,0)', '0'); 
            }
          }

          if(isset($status['filterBy']) && isset($status['searchBy'])){
              switch ($status['filterBy']) {
                case 'Department':
                  $this->db->where('Emp_DepartmentId',$status['searchBy']);
                  break;
                case 'Location':
                  $this->db->where('Emp_FK_StoreID',$status['searchBy']);
                  break;
                case 'Position':
                  $this->db->where('Emp_PositionId',$status['searchBy']);
                  break;
                case 'Principal':
                  $this->db->where('Emp_CustomerId',$status['searchBy']);
                  break;
                case 'PayGroup':
                  $this->db->where('Emp_FK_GroupId',$status['searchBy']);
                  break;
                default:
                  break;
              }
          }
    }
        // INDIVIDUAL COLUMN SEARCH

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1)))',$_POST['columns'][1]["search"]["value"],false);
        }

        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("Emp_ID", $_POST["search"]["value"]);  
                $this->db->or_like('LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1)))', $_POST["search"]["value"],false);  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('Emp_Name', 'DESC');  
        }
    }

    public function datatables($status = null){
        $this->query($status); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblEmployee');  
        return $query->result_array();
    }

    public function get_filtered_data($status = null){  
         $this->query($status);  
         $query = $this->db->get('tblEmployee');  
         return $query->num_rows();  
  }
      // END

       public function getFilteredData($type, $filter, $addtl = null, $filter_input = null){

      $where = '';
      switch ($type) {
        case 'store':
          if($filter_input != ''){
            $where = "AND SP_StoreName LIKE '%". $filter_input ."%' ";
          }
          $query_string = 'SELECT LTRIM(RTRIM(SP_StoreName)) as Description,  LTRIM(RTRIM(SP_ID)) AS Id  
                           From tblStoreProfile  
                           WHERE (SP_FK_CompanyID = \''.$filter.'\') '.$where.'
                           ORDER BY Description';
          break;
        case 'department':
          if($filter_input != ''){
              // $where = "AND b.P_Position LIKE '%". $filter_input ."%' ";
              $where = "AND P_Position LIKE '%". $filter_input ."%' ";
          }
          // $query_string = 'SELECT LTRIM(RTRIM(b.P_Position)) as Description,  LTRIM(RTRIM(a.DS_FK_Position_id)) AS Id   
          //                  FROM tblDepartmentSetup AS a LEFT OUTER JOIN tblPosition AS b ON a.DS_FK_Position_id = b.P_id  
          //                  WHERE (a.DS_FK_Department_id = \''.$filter.'\') AND (ISNULL(b.P_Active, 0) = 1)   '.$where.'
          //                  ORDER BY Description';
          $query_string = 'SELECT LTRIM(RTRIM(P_Position)) as Description,  LTRIM(RTRIM(P_ID)) AS Id   
                           FROM  tblPosition
                           WHERE (P_Dept = \''.$filter.'\') AND (ISNULL(P_Active, 0) = 1)   '.$where.'
                           ORDER BY Description';
          break;
        case 'paygroup':
          if($filter_input != ''){
            $where = "AND G_Desc LIKE '%". $filter_input ."%' ";
          }
          $query_string = 'SELECT LTRIM(RTRIM(G_Desc)) as Description,  LTRIM(RTRIM(G_Id)) AS Id
                           FROM tblPAYGroup 
                           WHERE (G_FK_CompanyId = \''.$filter.'\')
                           ORDER BY Description';
          break;
        case 'attendancegroup':
          if($filter_input != ''){
            $where = "AND a.AD_Desc LIKE '%". $filter_input ."%' ";
          }
          $query_string  = " SELECT LTRIM(RTRIM(a.AD_Desc)) as Description,  LTRIM(RTRIM(tr.TR_AttendanceType)) AS Id
                             FROM tblPAYTimekeepingRules AS tr LEFT OUTER JOIN tblAttributeDetail AS a ON tr.TR_AttendanceType = a.AD_Id 
                             WHERE  (tr.TR_PKFK_CompanyId = '".$filter."') AND (tr.TR_GroupId ='".$addtl."')  ".$where."
                             ORDER BY Description";
          break;
        case 'payitems':
          if($filter_input != '' && trim($addtl) == 'Description ASC'){
            $where = "AND PI_Desc LIKE '%". $filter_input ."%' ";
          }
          else if($filter_input != '' && trim($addtl) == 'Id ASC'){
            $where = "AND PI_id LIKE '%". $filter_input ."%' ";
          }
          $query_string = 'SELECT LTRIM(rTRIM(PI_id)) AS Id, LTRIM(RTRIM(PI_Desc)) AS Description  
                           From tblPAYItems  
                           WHERE (IsNull(PI_Allowance, 0) = 1) '.$where.' AND  
                                   (PI_id NOT IN (SELECT   Allow_Id  
                                                   From tblAllowance  
                                                   WHERE     (Allow_Id = tblPAYItems.PI_id) AND (FK_Emp_Id = \''.$filter.'\')))
                           ORDER BY '.$addtl.' ';
          break;
        default:
          return;
          break;
      }
      
      return $this->db->query($query_string)->result_array();
  } 

    public function getFilteredByData($filter, $filter_input = null){

      $where = '';
      switch ($filter) {
        case 'Department':
          $where = " AND LTRIM(RTRIM(AD_Desc)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(AD_Desc)) as Description,  LTRIM(RTRIM(AD_Id)) AS Id   
                           FROM  tblAttributeDetail
                           WHERE (ISNULL(AD_Active, 0) = 1) AND (AD_FK_Code = \''.DEPARTMENT.'\') '.$where.'
                           ORDER BY Description';
          break;
        case 'Position':
          $where = " AND LTRIM(RTRIM(P_Position)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(P_Position)) as Description,  LTRIM(RTRIM(P_ID)) AS Id   
                           FROM  tblPosition
                           WHERE (ISNULL(P_Active, 0) = 1) '.$where.'
                           ORDER BY Description';
          break;
        case 'Location':
          $where = " AND LTRIM(RTRIM(SP_StoreName)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(SP_StoreName)) as Description,  LTRIM(RTRIM(SP_ID)) AS Id   
                           FROM  tblStoreProfile
                           WHERE (ISNULL(SP_Active, 0) = 1) '.$where.'
                           ORDER BY Description';
          break;
        case 'Principal':
          $where = " AND LTRIM(RTRIM(C_Name)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(C_Name)) as Description,  LTRIM(RTRIM(C_Id)) AS Id   
                           FROM  tblCustomer
                           WHERE (ISNULL(C_Active, 0) = 1) '.$where.'
                           ORDER BY Description';
          break;
        case 'PayGroup':
          $where = "WHERE LTRIM(RTRIM(G_Desc)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(G_Desc)) as Description,  LTRIM(RTRIM(G_Id)) AS Id   
                           FROM  tblPayGroup '.$where.'
                           ORDER BY Description';
          break;
        default:
          return;
          break;
      }
      return $this->db->query($query_string)->result_array();
  }

      // Data time entries starts here
    public function table_data_detail($status = null){

    $this->db->select('C_DateSwipe, CASE C_DOW_In WHEN \'1\' THEN \'Sunday\' 
                                     WHEN \'2\' THEN \'Monday\'
                                     WHEN \'3\' THEN \'Tuesday\'
                                     WHEN \'4\' THEN \'Wednesday\'
                                     WHEN \'5\' THEN \'Thursday\'
                                     WHEN \'6\' THEN \'Friday\'
                                     WHEN \'7\' THEN \'Saturday\'
                                     ELSE \'\' END as C_DOW_In, CASE C_DOW_Out WHEN \'1\' THEN \'Sunday\' 
                                     WHEN \'2\' THEN \'Monday\'
                                     WHEN \'3\' THEN \'Tuesday\'
                                     WHEN \'4\' THEN \'Wednesday\'
                                     WHEN \'5\' THEN \'Thursday\'
                                     WHEN \'6\' THEN \'Friday\'
                                     WHEN \'7\' THEN \'Saturday\'
                                     ELSE \'\' END as C_DOW_Out, C_TimeIn, C_TimeOut, C_BioUpload, C_BreakOut_1, C_BreakIn_1, C_BreakOut_2, C_BreakIn_2, C_BreakOut_3, C_BreakIn_3, C_FK_Emp_id, C_LineNo',false);
    $this->db->join('tblEmployee','C_FK_Emp_id = Emp_Id','left');
    $this->db->where('C_FK_Emp_id',$status);
    
    $this->db->from('tblTimeEntry');
    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_detail($status);
    $output['num_rows']       = $this->get_filtered_data_detail($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_detail($status = null){
    
    $this->db->select('C_DateSwipe, CASE C_DOW_In WHEN \'1\' THEN \'Sunday\' 
                                     WHEN \'2\' THEN \'Monday\'
                                     WHEN \'3\' THEN \'Tuesday\'
                                     WHEN \'4\' THEN \'Wednesday\'
                                     WHEN \'5\' THEN \'Thursday\'
                                     WHEN \'6\' THEN \'Friday\'
                                     WHEN \'7\' THEN \'Saturday\'
                                     ELSE \'\' END as C_DOW_In, CASE C_DOW_Out WHEN \'1\' THEN \'Sunday\' 
                                     WHEN \'2\' THEN \'Monday\'
                                     WHEN \'3\' THEN \'Tuesday\'
                                     WHEN \'4\' THEN \'Wednesday\'
                                     WHEN \'5\' THEN \'Thursday\'
                                     WHEN \'6\' THEN \'Friday\'
                                     WHEN \'7\' THEN \'Saturday\'
                                     ELSE \'\' END as C_DOW_Out, C_TimeIn, C_TimeOut, C_BioUpload, C_BreakOut_1, C_BreakIn_1, C_BreakOut_2, C_BreakIn_2, C_BreakOut_3, C_BreakIn_3,C_FK_Emp_id,C_LineNo',false);

    $this->db->where('C_FK_Emp_id',$status);

    // INDIVIDUAL COLUMN SEARCH

    if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
      $this->db->like('C_DateSwipe',trim($_POST['columns'][1]["search"]["value"]));
    }

    if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('CASE C_DOW_In WHEN \'1\' THEN \'Sunday\'  WHEN \'2\' THEN \'Monday\' WHEN \'3\' THEN \'Tuesday\' WHEN \'4\' THEN \'Wednesday\' WHEN \'5\' THEN \'Thursday\' WHEN \'6\' THEN \'Friday\' WHEN \'7\' THEN \'Saturday\'  ELSE \'\' END ',trim($_POST['columns'][2]["search"]["value"]));
        }
    if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('CASE C_DOW_Out WHEN \'1\' THEN \'Sunday\'  WHEN \'2\' THEN \'Monday\' WHEN \'3\' THEN \'Tuesday\' WHEN \'4\' THEN \'Wednesday\' WHEN \'5\' THEN \'Thursday\' WHEN \'6\' THEN \'Friday\' WHEN \'7\' THEN \'Saturday\'  ELSE \'\' END ',trim($_POST['columns'][3]["search"]["value"]));
        }
    if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
      $this->db->like('C_TimeIn',trim($_POST['columns'][4]["search"]["value"]));
    }
    if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
      $this->db->like('C_TimeOut',trim($_POST['columns'][5]["search"]["value"]));
    }
    if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
      $this->db->like('C_BioUpload',$_POST['columns'][6]["search"]["value"]);
    }

    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){   
            $this->db->like('C_DateSwipe', $_POST["search"]["value"]);  
            $this->db->or_like('CASE C_DOW_In WHEN \'1\' THEN \'Sunday\'  WHEN \'2\' THEN \'Monday\' WHEN \'3\' THEN \'Tuesday\' WHEN \'4\' THEN \'Wednesday\' WHEN \'5\' THEN \'Thursday\' WHEN \'6\' THEN \'Friday\' WHEN \'7\' THEN \'Saturday\'  ELSE \'\' END ', $_POST["search"]["value"]);  
            $this->db->or_like('CASE C_DOW_Out WHEN \'1\' THEN \'Sunday\'  WHEN \'2\' THEN \'Monday\' WHEN \'3\' THEN \'Tuesday\' WHEN \'4\' THEN \'Wednesday\' WHEN \'5\' THEN \'Thursday\' WHEN \'6\' THEN \'Friday\' WHEN \'7\' THEN \'Saturday\'  ELSE \'\' END ', $_POST["search"]["value"]);  
            $this->db->or_like('C_TimeIn', $_POST["search"]["value"]);  
            $this->db->or_like('C_TimeOut', $_POST["search"]["value"]);
            $this->db->or_like('C_BioUpload', $_POST["search"]["value"]); 
        }
    }  

    if(isset($_POST["order"])){
        $this->db->order_by($this->order_column_time_entry_dtl[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else{  
        $this->db->order_by('C_DateSwipe', 'DESC');  
    }

  }

  public function datatables_detail($status = null){
    $this->query_detail($status); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblTimeEntry');  
        return $query->result_array();
  }

  public function get_filtered_data_detail($status = null){  
         $this->query_detail($status);  
         $query = $this->db->get('tblTimeEntry');  
         return $query->num_rows();  
  }

  public function getAllEmployeeData($employee_id){
        $query_string = 'SELECT    C_DateSwipe, C_DOW_In, C_DOW_Out, C_TimeIn, C_TimeOut, C_BioUpload,
                                    
                         FROM       tblTimeEntry LEFT OUTER JOIN
                                    tblEmployee ON C_FK_Emp_id = Emp_Id
                         WHERE      C_FK_Emp_id = \''.$employee_id.'\'';

        return $this->db->query($query_string)->result_array(); 

    }

    public function getActiveEmployee(){
        $query_string = 'SELECT LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(
                        Emp_MiddleName, 1))) AS Emp_Name,Emp_Id
                         FROM tblEmployee as EM
                         WHERE Emp_Status = \'Hired\' AND Emp_Active = \'1\'';

        return $this->db->query($query_string)->result_array();
    }

    public function getActivePrincipal(){

       $query_string = 'SELECT  LTRIM(RTRIM(C_Name)) AS C_Name, LTRIM(RTRIM(C_DocNo)) as C_DocNo
                        From tblCustomer as C
                        Where (IsNull(C_Active, 0) = 1)
                        ORDER BY C_Name ';

      return $this->db->query($query_string)->result_array();
    }

    public function select_data($employee_id){
      $this->db->order_by('C_FK_Emp_id', 'DESC');
      $this->db->where('C_FK_Emp_id',$employee_id);
      $query_string = $this->db->get('tblTimeEntry');
      return $query_string;
    }

    public function insert_batch($data){
      $this->db->insert('tblTimeEntry',$data);
    }

    public function saveTimeEntryRecord($data){
    try {

      $result = $this->db->where('C_FK_Emp_id',$data['C_FK_Emp_id'])
                    ->order_by('C_LineNo','DESC')
                    ->limit(1)
                    ->get('tblTimeEntry');

      $lineNo = 1;
      if($result->num_rows() > 0){
        $lineNo = $result->row_array()['C_LineNo'] + 1;
      }

      if($data['mode'] == 'add'){
        // insert
        unset($data['mode']);
        $data['C_FK_Emp_id'] =  $data['C_FK_Emp_id'];
        $data['C_DateSwipe']  = $data['C_DateSwipe'];
        $data['C_LineNo']  = $lineNo;
       // Adjust time format for database saving 
        $data['C_DOW_In']       = $data['C_DOW_In'];
        $data['C_DOW_Out']      = $data['C_DOW_Out'];
        $data['C_TimeIn'] = str_replace(":", "", $data['C_TimeIn']);
        $data['C_TimeOut'] = str_replace(":", "", $data['C_TimeOut']);
        $data['C_BioUpload']      = $data['C_BioUpload'];
        $data['C_BreakOut_1'] = str_replace(":", "", $data['C_BreakOut_1']);
        $data['C_BreakIn_1'] = str_replace(":", "", $data['C_BreakIn_1']);
        $data['C_BreakOut_2'] = str_replace(":", "", $data['C_BreakOut_2']);
        $data['C_BreakIn_2'] = str_replace(":", "", $data['C_BreakIn_2']);
        $data['C_BreakOut_3'] = str_replace(":", "", $data['C_BreakOut_3']);
        $data['C_BreakIn_3'] = str_replace(":", "", $data['C_BreakIn_3']);

        $data['CreatedBy']    = getCurrentUser()['login-user'];
        $data['DateCreated']  = date_format(date_create(),'m/d/Y');
        $this->db->insert('tblTimeEntry',$data);
      }
      else{
        // update
        $where = array('C_FK_Emp_id' => $data['C_FK_Emp_id'],'C_LineNo' => $data['C_LineNo']);

        unset($data['mode']);
        unset($data['C_FK_Emp_id']);
        unset($data['C_LineNo']);
        // Adjust time format for database saving 
        $data['C_DateSwipe']       = $data['C_DateSwipe'];
        $data['C_DOW_In']       = $data['C_DOW_In'];
        $data['C_DOW_Out']      = $data['C_DOW_Out'];
        $data['C_TimeIn'] = str_replace(":", "", $data['C_TimeIn']);
        $data['C_TimeOut'] = str_replace(":", "", $data['C_TimeOut']);
        $data['C_BioUpload']      = $data['C_BioUpload'];
        $data['C_BreakOut_1'] = str_replace(":", "", $data['C_BreakOut_1']);
        $data['C_BreakIn_1'] = str_replace(":", "", $data['C_BreakIn_1']);
        $data['C_BreakOut_2'] = str_replace(":", "", $data['C_BreakOut_2']);
        $data['C_BreakIn_2'] = str_replace(":", "", $data['C_BreakIn_2']);
        $data['C_BreakOut_3'] = str_replace(":", "", $data['C_BreakOut_3']);
        $data['C_BreakIn_3'] = str_replace(":", "", $data['C_BreakIn_3']);
        $data['ModifiedBy']    = getCurrentUser()['login-user'];
        $data['DateModified']  = date_format(date_create(),'m/d/Y');
        $this->db->where($where)->update('tblTimeEntry',$data);
      }
    } catch (Exception $e) {
      throw new Exception($e->getMessage().", Error , error");
    }
  }


    // Data time-log starts here
    public function table_data_timelog($status = null){

    $this->db->select('C_FK_Location_id, C_TimeLog, C_LogType');
    $this->db->from('tblTimeEntryDetail');
    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_time_log();
    $output['num_rows']       = $this->get_filtered_data_time_log();
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_time_log($status = null){
    
    $this->db->select('C_FK_Location_id, C_TimeLog, C_LogType');

    // INDIVIDUAL COLUMN SEARCH


    if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
      $this->db->like('C_FK_Location_id',$_POST['columns'][1]["search"]["value"]);
    }
    if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
      $this->db->like('C_TimeLog',$_POST['columns'][2]["search"]["value"]);
    }
    if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
      $this->db->like('C_LogType',$_POST['columns'][3]["search"]["value"]);
    }

    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){   
            $this->db->or_like('C_FK_Location_id', $_POST["search"]["value"]);  
            $this->db->or_like('C_TimeLog', $_POST["search"]["value"]);  
            $this->db->or_like('C_LogType', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){
        $this->db->order_by($this->order_column_time_log[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else{  
        $this->db->order_by('C_FK_Location_id', 'DESC');  
    }

  }

  public function datatables_time_log($status = null){
    $this->query_time_log($status); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblTimeEntryDetail');  
        return $query->result_array();
  }

  public function get_filtered_data_time_log($status = null){  
         $this->query_time_log($status);  
         $query = $this->db->get('tblTimeEntryDetail');  
         return $query->num_rows();  
  }

     // Data time-log starts here
    public function table_data_PTA($status = null){

    $this->db->select('C_DateSwipe,CASE C_DOW_In WHEN \'1\' THEN \'Sunday\' 
                                     WHEN \'2\' THEN \'Monday\'
                                     WHEN \'3\' THEN \'Tuesday\'
                                     WHEN \'4\' THEN \'Wednesday\'
                                     WHEN \'5\' THEN \'Thursday\'
                                     WHEN \'6\' THEN \'Friday\'
                                     WHEN \'7\' THEN \'Saturday\'
                                     ELSE \'\' END as C_DOW_In, C_TimeIn, C_TimeOut, C_Schedule, C_WorkHrs, C_RegHrs, C_AbsentHrs, C_LateHrs, C_UnderTimeHrs, C_OTHrs, C_NightDiff, C_RestDayOT, C_Holiday, C_HolidayHrs, C_LeavePay');
    $this->db->from('tblProcessedTime');
    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_PTA();
    $output['num_rows']       = $this->get_filtered_data_PTA();
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_PTA($status = null){
    
    $this->db->select('C_DateSwipe, CASE C_DOW_In WHEN \'1\' THEN \'Sunday\' 
                                     WHEN \'2\' THEN \'Monday\'
                                     WHEN \'3\' THEN \'Tuesday\'
                                     WHEN \'4\' THEN \'Wednesday\'
                                     WHEN \'5\' THEN \'Thursday\'
                                     WHEN \'6\' THEN \'Friday\'
                                     WHEN \'7\' THEN \'Saturday\'
                                     ELSE \'\' END as C_DOW_In, C_TimeIn, C_TimeOut, C_Schedule, C_WorkHrs, C_RegHrs, C_AbsentHrs, C_LateHrs, C_UnderTimeHrs, C_OTHrs, C_NightDiff, C_RestDayOT, C_Holiday, C_HolidayHrs, C_LeavePay');

    // INDIVIDUAL COLUMN SEARCH


    if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
      $this->db->like('C_DateSwipe',$_POST['columns'][1]["search"]["value"]);
    }
    if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
      $this->db->like('C_DOW_In',$_POST['columns'][2]["search"]["value"]);
    }
    if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
      $this->db->like('C_DOW_Out',$_POST['columns'][3]["search"]["value"]);
    }
    if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
      $this->db->like('C_TimeIn',$_POST['columns'][4]["search"]["value"]);
    }
    if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
      $this->db->like('C_TimeOut',$_POST['columns'][5]["search"]["value"]);
    }
    if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
      $this->db->like('C_Schedule',$_POST['columns'][5]["search"]["value"]);
    }

    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){   
            $this->db->or_like('C_DateSwipe', $_POST["search"]["value"]);  
            $this->db->or_like('C_DOW_In', $_POST["search"]["value"]);  
            $this->db->or_like('C_DOW_Out', $_POST["search"]["value"]);  
            $this->db->or_like('C_TimeIn', $_POST["search"]["value"]);  
            $this->db->or_like('C_TimeOut', $_POST["search"]["value"]);
            $this->db->or_like('C_Schedule', $_POST["search"]["value"]); 
        }
    }  

    if(isset($_POST["order"])){
        $this->db->order_by($this->order_column_time_log[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else{  
        $this->db->order_by('C_DateSwipe', 'DESC');  
    }

  }

  public function datatables_PTA($status = null){
    $this->query_PTA($status); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblProcessedTime');  
        return $query->result_array();
  }

  public function get_filtered_data_PTA($status = null){  
         $this->query_PTA($status);  
         $query = $this->db->get('tblProcessedTime');  
         return $query->num_rows();  
  }

}