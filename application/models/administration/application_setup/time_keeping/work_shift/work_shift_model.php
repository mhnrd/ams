<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class work_shift_model extends MAIN_Model{


	protected $_table = 'tblHoliday';
	
	private $id = NULL;
	private $order_column = array(null,null,'S_ID','S_Name','S_StartTime','S_EndTime', 'S_Break1Out', 'S_Break1In', 'S_Break2Out', 'S_Break2In', 'S_Break3Out', 'S_Break3In', 'S_Active');

	public function __construct() {
		parent::__construct();
	}

	// DATA TABLE STARTS HERE
	public function table_data($status = null){

		$this->db->select('S_ID, S_Name, S_StartTime, S_EndTime, (CASE S_Active WHEN 0 THEN \'Inactive\' ELSE \'Active\' END) as S_Active, S_Break1Out, S_Break1In, S_Break2Out, S_Break2In, S_Break3Out, S_Break3In');
        $this->db->from('tblShift');
		$result_count = $this->db->count_all_results();

		$output['data']					= $this->datatables($status);
		$output['num_rows']				= $this->get_filtered_data($status);
		$output['count_all_results']	= $result_count;

		return $output;
	}	

	public function query($status = null){
		
		$this->db->select('S_ID, S_Name, S_StartTime, S_EndTime, (CASE S_Active WHEN 0 THEN \'Inactive\' ELSE \'Active\' END) as S_Active, S_Break1Out, S_Break1In, S_Break2Out, S_Break2In, S_Break3Out, S_Break3In');

		// INDIVIDUAL COLUMN SEARCH

		if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
			$this->db->like('S_ID',$_POST['columns'][2]["search"]["value"]);
		}

		if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('S_Name',$_POST['columns'][3]["search"]["value"]);
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $timeHH = substr($_POST['columns'][4]["search"]["value"], 0, 2);
            $timeMM = substr($_POST['columns'][4]["search"]["value"], 3, 2);
            $this->db->like('S_StartTime',$_POST['columns'][4]["search"]["value"]);
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $timeHH = substr($_POST['columns'][4]["search"]["value"], 0, 2);
            $timeMM = substr($_POST['columns'][4]["search"]["value"], 3, 2);
            $this->db->like('S_StartTime',$timeHH.$timeMM);
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $timeHH = substr($_POST['columns'][5]["search"]["value"], 0, 2);
            $timeMM = substr($_POST['columns'][5]["search"]["value"], 3, 2);
            $this->db->like('S_EndTime',$timeHH.$timeMM);
        }

        if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
            $timeHH = substr($_POST['columns'][6]["search"]["value"], 0, 2);
            $timeMM = substr($_POST['columns'][6]["search"]["value"], 3, 2);
            $this->db->like('S_Break1Out',$timeHH.$timeMM);
        }

        if(isset($_POST['columns'][7]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $timeHH = substr($_POST['columns'][7]["search"]["value"], 0, 2);
            $timeMM = substr($_POST['columns'][7]["search"]["value"], 3, 2);
            $this->db->like('S_Break1In',$timeHH.$timeMM);
        }

        if(isset($_POST['columns'][8]["search"]["value"]) && !empty($_POST['columns'][8]["search"]["value"])){
            $timeHH = substr($_POST['columns'][8]["search"]["value"], 0, 2);
            $timeMM = substr($_POST['columns'][8]["search"]["value"], 3, 2);
            $this->db->like('S_Break2Out',$timeHH.$timeMM);
        }

         if(isset($_POST['columns'][9]["search"]["value"]) && !empty($_POST['columns'][9]["search"]["value"])){
            $timeHH = substr($_POST['columns'][9]["search"]["value"], 0, 2);
            $timeMM = substr($_POST['columns'][9]["search"]["value"], 3, 2);
            $this->db->like('S_Break3In',$timeHH.$timeMM);
        }

        if(isset($_POST['columns'][10]["search"]["value"]) && !empty($_POST['columns'][10]["search"]["value"])){
            $timeHH = substr($_POST['columns'][10]["search"]["value"], 0, 2);
            $timeMM = substr($_POST['columns'][10]["search"]["value"], 3, 2);
            $this->db->like('S_Break3Out',$timeHH.$timeMM);
        }

        if(isset($_POST['columns'][11]["search"]["value"]) && !empty($_POST['columns'][11]["search"]["value"])){
            $timeHH = substr($_POST['columns'][11]["search"]["value"], 0, 2);
            $timeMM = substr($_POST['columns'][11]["search"]["value"], 3, 2);
            $this->db->like('S_Break3In',$timeHH.$timeMM);
        }

        if(isset($_POST['columns'][12]["search"]["value"]) && !empty($_POST['columns'][12]["search"]["value"])){
            $this->db->like('CASE S_Active WHEN 0 THEN \'Inactive\' ELSE \'Active\' END',$_POST['columns'][12]["search"]["value"], 'after');
        }

        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("S_ID", $_POST["search"]["value"]);  
                $this->db->or_like("S_Name", $_POST["search"]["value"]);  
                $this->db->or_like("S_StartTime", substr($_POST["search"]["value"], 0, 2).substr($_POST["search"]["value"], 3, 2));  
                $this->db->or_like("S_EndTime", substr($_POST["search"]["value"], 0, 2).substr($_POST["search"]["value"], 3, 2));  
                $this->db->or_like("S_Break1In", substr($_POST["search"]["value"], 0, 2).substr($_POST["search"]["value"], 3, 2));  
                $this->db->or_like("S_Break1Out", substr($_POST["search"]["value"], 0, 2).substr($_POST["search"]["value"], 3, 2));  
                $this->db->or_like("S_Break2In", substr($_POST["search"]["value"], 0, 2).substr($_POST["search"]["value"], 3, 2));  
                $this->db->or_like("S_Break2Out", substr($_POST["search"]["value"], 0, 2).substr($_POST["search"]["value"], 3, 2));  
                $this->db->or_like("S_Break3In", substr($_POST["search"]["value"], 0, 2).substr($_POST["search"]["value"], 3, 2));  
                $this->db->or_like("S_Break3Out", substr($_POST["search"]["value"], 0, 2).substr($_POST["search"]["value"], 3, 2));  
                $this->db->or_like("CASE S_Active WHEN 0 THEN 'Inactive' ELSE 'Active' END", $_POST["search"]["value"], 'after');  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('S_ID', 'DESC');  
        }
	}

	public function datatables($status = null){
		$this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblShift');  
        return $query->result_array();
	}

	 public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblShift');  
           return $query->num_rows();  
    }

    // END 

     public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */

        $this->db->delete('tblShift',array('S_ID'=>$docno));

    }

}