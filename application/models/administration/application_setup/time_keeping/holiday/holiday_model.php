<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Holiday_model extends MAIN_Model {

	protected $_table = 'tblHoliday';
	
	private $id = NULL;
	private $order_column = array(null,null,'H_ID','H_Desc','H_Date','H_Type','H_HolidayStart','H_HolidayEnd','H_Policy','H_Active');

	public function __construct() {
		parent::__construct();
	}

	// DATA TABLE STARTS HERE
	public function table_data($status = null){

		$this->db->select('H_ID, H_Desc, H_Date, H_Type, H_HolidayStart, H_HolidayEnd, H_Policy, H_Active');

        $this->db->from('tblHoliday');
		$result_count = $this->db->count_all_results();

		$output['data']					= $this->datatables($status);
		$output['num_rows']				= $this->get_filtered_data($status);
		$output['count_all_results']	= $result_count;

		return $output;
        
	}	

	public function query($status = null){
		$this->db->select('H_ID, H_Desc, H_Date, CASE H_Type WHEN \'R\' THEN \'Regular\' ELSE \'Special\' END as H_Type, H_HolidayStart, H_HolidayEnd, H_Policy, CASE H_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as H_Active');

		// INDIVIDUAL COLUMN SEARCH

		if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
			$this->db->like('H_ID',trim($_POST['columns'][2]["search"]["value"]));
		}

		if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('H_Desc',trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('CASE H_Type WHEN \'R\' THEN \'Regular\' ELSE \'Special\' END',trim($_POST['columns'][4]["search"]["value"]));
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $dtFrom = substr($_POST['columns'][5]["search"]["value"],0,10);
            $dtTo = substr($_POST['columns'][5]["search"]["value"],13,10);
            $this->db->where("H_HolidayStart BETWEEN '{$dtFrom} 00:00:00' AND  '{$dtTo} 23:59:59'");
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $dtFrom = substr($_POST['columns'][5]["search"]["value"],0,10);
            $dtTo = substr($_POST['columns'][5]["search"]["value"],13,10);
            $this->db->where("H_HolidayEnd BETWEEN '{$dtFrom} 00:00:00' AND  '{$dtTo} 23:59:59'");
        }


        if(isset($_POST['columns'][7]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $this->db->like('H_Policy',trim($_POST['columns'][7]["search"]["value"]));
        }

        if(isset($_POST['columns'][8]["search"]["value"]) && !empty($_POST['columns'][8]["search"]["value"])){
            $this->db->like('CASE H_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END ',$_POST['columns'][8]["search"]["value"], 'after');
        }

        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("H_ID", trim($_POST["search"]["value"]));  
                $this->db->or_like("H_Desc", trim($_POST["search"]["value"]));  
                $this->db->or_like("H_Date", trim($_POST["search"]["value"]));  
                $this->db->or_like("CASE H_Type WHEN 'R' THEN 'Regular' ELSE 'Special' END", trim($_POST["search"]["value"]));  
                $this->db->or_like("H_HolidayStart", trim($_POST["search"]["value"]));  
                $this->db->or_like("H_HolidayEnd", trim($_POST["search"]["value"]));  
                $this->db->or_like("H_Policy", trim($_POST["search"]["value"]));
                $this->db->or_like("CASE H_Active WHEN 1 THEN 'Active' ELSE 'Inactive' END", trim($_POST["search"]["value"]), 'after');  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('H_ID', 'DESC');  
        }
	}

	public function datatables($status = null){
		$this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblHoliday');  
        return $query->result_array();
	}

	 public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblHoliday');  
           return $query->num_rows();  
    }


}