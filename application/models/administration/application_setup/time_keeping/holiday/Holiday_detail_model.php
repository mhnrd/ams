<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Holiday_detail_model extends MAIN_Model{

	private $id = NULL;
	private $order_column = array('LH_FK_Location_id', 'SP_StoreName', 'LH_Selected');

	public function getActiveLocation(){

		$query_string = 'SELECT 		SP_ID, SP_StoreName
						 FROM 			tblStoreProfile
						 WHERE 			SP_Active = 1';

		return $this->db->query($query_string)->result_array();

	}

	// DATA TABLE STARTS HERE
	public function table_data($doc_no){

		$this->db->select('LH_FK_Location_id, SP_StoreName, LH_Selected');
		$this->db->join('tblStoreProfile', 'SP_ID = LH_FK_Location_id', 'left');
        $this->db->from('tblLocationHoliday');
        $this->db->where('LH_H_ID', $doc_no);

		$result_count = $this->db->count_all_results();

		$output['data']					= $this->datatables($doc_no);
		$output['num_rows']				= $this->get_filtered_data($doc_no);
		$output['count_all_results']	= $result_count;

		return $output;
        
	}	

	public function query($doc_no){
		$this->db->select('LH_FK_Location_id, SP_StoreName, LH_Selected');
		$this->db->join('tblStoreProfile', 'SP_ID = LH_FK_Location_id', 'left');
		$this->db->where('LH_H_ID', $doc_no);

		// INDIVIDUAL COLUMN SEARCH

		if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
			$this->db->like('LH_FK_Location_id',trim($_POST['columns'][2]["search"]["value"]));
		}

		if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('SP_StoreName',trim($_POST['columns'][3]["search"]["value"]));
        }

        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){ 
            	$this->db->group_start();
                $this->db->like("LH_FK_Location_id", trim($_POST["search"]["value"]));  
                $this->db->or_like("SP_StoreName", trim($_POST["search"]["value"])); 
                $this->db->group_end();
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('LH_FK_Location_id', 'ASC');  
        }
	}

	public function datatables($doc_no){
		$this->query($doc_no); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblLocationHoliday');  
        return $query->result_array();
	}

	 public function get_filtered_data($doc_no){  
           $this->query($doc_no);  
           $query = $this->db->get('tblLocationHoliday');  
           return $query->num_rows();  
    }
}