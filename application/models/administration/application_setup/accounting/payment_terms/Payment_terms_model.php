<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_terms_model extends MAIN_Model{

	private $id = NULL;
    private $order_column = array(null,null,'PT_ID','PT_Desc','PT_Days','PT_Active');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data(){

        $this->db->select('PT_ID, PT_Days ,PT_Desc, PT_Active');  
        $this->db->from('tblPaymentTerms'); 
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('PT_ID, PT_Days ,PT_Desc, CASE PT_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as PT_Active', false); 
        // Individual column search

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('PT_ID',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('PT_Desc',$_POST['columns'][3]["search"]["value"]);
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('CASE PT_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END',$_POST['columns'][4]["search"]["value"], 'after');
        }

        // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("PT_ID", $_POST["search"]["value"]);  
                $this->db->or_like("PT_Desc", $_POST["search"]["value"]);  
                $this->db->or_like('CASE PT_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END', $_POST["search"]["value"], 'after');  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('PT_ID', 'DESC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblPaymentTerms');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblPaymentTerms');  
           return $query->num_rows();  
    }

    /* datatable query ends here */ 

}