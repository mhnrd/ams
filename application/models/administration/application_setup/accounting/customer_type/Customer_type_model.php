<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer_type_model extends MAIN_Model {

    protected $_table = 'tblCustomerType';

    private $id = NULL;
    private $order_column = array(null,null,'CT_Id','CT_Description','CT_Active');

    public function __construct() {
        parent::__construct();
    }

  

    /* datatable query starts here */
    public function table_data($status = null){

        
        $this->db->select('CT_Id, CT_Description, CASE CT_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as CT_Active,');
        //$this->db->where('tblCPCenter.CreatedBy', getCurrentUser()['login-user']);
        $this->db->from('tblCustomerType');
        //$this->db->join('tblAttributeDetail', 'AD_Id = CPC_FK_Class', 'left');
        $result_count = $this->db->count_all_results(); 

        $result_item =  $this->datatables();
       foreach ($result_item as $key => $value) {
            
        }

        $output['data']              = $this->datatables($status);
        $output['num_rows']          = $this->get_filtered_data($status);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query($status = null){
        $this->db->select('CT_Id, CT_Description ,CASE CT_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as CT_Active,');
       // $this->db->join('tblAttributeDetail', 'AD_Id = CPC_FK_Class', 'left');
        //$this->db->where('tblCPCenter.CreatedBy', getCurrentUser()['login-user']);

        // Individual Column Search

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('CT_Id', trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('CT_Description', trim($_POST['columns'][3]["search"]["value"]));
        }

         if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('CASE CT_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END', trim($_POST['columns'][4]["search"]["value"]), false);
        }

        // End
        
        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){
                $this->db->like("CT_Id", trim($_POST["search"]["value"])); 
                $this->db->or_like("CT_Description", trim($_POST["search"]["value"]));
                $this->db->or_like("CASE CT_Active WHEN 1 THEN 'Active' ELSE 'Inactive' END", trim($_POST["search"]["value"]), false);           
            }
        }


        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('CT_Id', 'DESC');  
        }
    }

    public function datatables($status = null){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblCustomerType');  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblCustomerType');  
           return $query->num_rows();  
    }
    public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */
        $this->db->delete('tblCustomerType',array('CT_Id'=>$docno));
        
        if ($this->db->trans_status() === FALSE){
                return array('success'=>0);
        }
        else{
                return array('success'=>1);
        }

    }
    public function status_data($docno,$action){
  
        foreach ($docno as $row) {
            if ($action == 'active') {
                $this->db->where('CT_Id',$row)->update('tblCustomerType', array('CT_Active' => '1'));
            }else{
                $this->db->where('CT_Id',$row)->update('tblCustomerType', array('CT_Active' => '0'));
            }
        }
        if ($this->db->trans_status() === FALSE){
                return array('success'=>0);
        }
        else{
                return array('success'=>1);
        }
    }


}
