<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class BUsiness_unit_model extends MAIN_Model{

	private $id = NULL;
	private $order_column = array(null, 'BU_ID', 'BU_Description', 'BU_Status');

	public function __construct() {
		parent::__construct();
	}

	// DATA TABLE STARTS HERE
	public function table_data($status = null){

		$this->db->select('BU_ID, BU_Description, (CASE WHEN BU_Status = 1 THEN \'Active\' ELSE \'Inactive\' END) as BU_Status', false);
        $this->db->from('tblBusinessUnit');
		$result_count = $this->db->count_all_results();

		$output['data']					= $this->datatables($status);
		$output['num_rows']				= $this->get_filtered_data($status);
		$output['count_all_results']	= $result_count;

		return $output;
	}	

	public function query($status = null){
		$this->db->select('BU_ID, BU_Description, (CASE WHEN BU_Status = 1 THEN \'Active\' ELSE \'Inactive\' END) as BU_Status', false);

		// INDIVIDUAL COLUMN SEARCH

		if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('BU_ID',$_POST['columns'][1]["search"]["value"]);
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('BU_Description',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('(CASE BU_Status WHEN 1 THEN \'Active\' ELSE \'Inactive\' END)',$_POST['columns'][3]["search"]["value"], false);
        }
		
        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("BU_ID", $_POST["search"]["value"]); 
                $this->db->or_like("BU_Description", $_POST["search"]["value"]); 
                $this->db->or_like("(CASE BU_Status WHEN 1 THEN 'Active' ELSE 'Inactive' END)", $_POST["search"]["value"], false); 
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('BU_ID', 'DESC');  
        }
	}

	public function datatables($status = null){
		$this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblBusinessUnit');  
        return $query->result_array();
	}

	public function get_filtered_data(){ 
        $this->query();
        $this->db->from('tblBusinessUnit');
        $query = $this->db->count_all_results();  
        return $query;  

    }
	// END

}