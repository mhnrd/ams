<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class sub_location_setup_detail_model extends MAIN_Model {

    private $id = NULL;

    public function __construct() {
        parent::__construct();
    }

    public function getParent($level_id, $type){
        $query_string = 'SELECT        LOC_Id, LOC_FK_Level_id, LOC_Parent_id, LOC_FK_Type_id, LOC_Name
                         FROM          tblLocation
                         WHERE         LOC_FK_Level_id = '.($level_id - 1).' AND LOC_FK_Type_id = '.$type.'
                         ORDER BY LOC_FK_Level_id';

        $result = $this->db->query($query_string)->result_array();

        return $result;
    }

    public function getLocationID($loc_id){
        $query_string = 'SELECT        LOC_Id
                         FROM          tblLocation
                         WHERE         LOC_Id = \''.$loc_id.'\'';

        $result = $this->db->query($query_string)->row_array();

        return $result;
    }

    public function getAll($loc_id){
        $query_string = 'SELECT        LOC_Id, LOC_FK_Level_id, LOC_Parent_id, LOC_FK_Type_id, LOC_Name
                         FROM          tblLocation
                         WHERE         '.$this->sql_hash('LOC_Id').' = \''.$loc_id.'\'';

        $result = $this->db->query($query_string)->row_array();

        return $result;
    }

}