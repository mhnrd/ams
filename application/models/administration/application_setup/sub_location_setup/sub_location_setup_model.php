<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class sub_location_setup_model extends MAIN_Model {

    protected $_table = 'tblLocation';
    private $id = NULL;
    private $order_column = array(null,'LOC_Id','LOC_Name','LOC_FK_Level_id', 'LOC_FK_Type_id', 'LOC_Parent_id');

    public function __construct() {
        parent::__construct();
    }


    /* datatable query starts here */
    public function table_data(){

        
       $this->db->select('LOC_Id, CASE WHEN LOC_FK_Level_id = 1 THEN \'Level 1\' WHEN LOC_FK_Level_id = 2 THEN \'Level 2\' WHEN LOC_FK_Level_id = 3 THEN \'Level 3\' END as LOC_FK_Level_id, LOC_Name, CASE WHEN ISNULL(LOC_Parent_id, \'\') = \'\' THEN \'Parent\' WHEN LOC_Parent_id != \'\' THEN LOC_Parent_id END as LOC_Parent_id, CASE WHEN LOC_FK_Type_id = 1 THEN \'Warehouse\' WHEN LOC_FK_Type_id = 2 THEN \'Business Unit\' WHEN LOC_FK_Type_id = 3 THEN \'Department\' END as LOC_FK_Type_id', false);
        $this->db->from('tblLocation');  
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('LOC_Id, CASE WHEN LOC_FK_Level_id = 1 THEN \'Level 1\' WHEN LOC_FK_Level_id = 2 THEN \'Level 2\' WHEN LOC_FK_Level_id = 3 THEN \'Level 3\' END as LOC_FK_Level_id, LOC_Name, CASE WHEN ISNULL(LOC_Parent_id, \'\') = \'\' THEN \'Parent\' WHEN LOC_Parent_id != \'\' THEN LOC_Parent_id END as LOC_Parent_id, CASE WHEN LOC_FK_Type_id = 1 THEN \'Warehouse\' WHEN LOC_FK_Type_id = 2 THEN \'Business Unit\' WHEN LOC_FK_Type_id = 3 THEN \'Department\' WHEN LOC_FK_Type_id = 4 THEN \'Production Area\' WHEN LOC_FK_Type_id = 5 THEN \'Location\' END as LOC_FK_Type_id', false);

        // Individual Column Search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('LOC_Id',$_POST['columns'][1]["search"]["value"]);
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('LOC_Name',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('CASE WHEN LOC_FK_Level_id = 1 THEN \'Level 1\' WHEN LOC_FK_Level_id = 2 THEN \'Level 2\' WHEN LOC_FK_Level_id = 3 THEN \'Level 3\' END',$_POST['columns'][3]["search"]["value"]);
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('CASE LOC_FK_Type_id WHEN 1 THEN \'Warehouse\' WHEN 2 THEN \'Business Unit\' WHEN 3 THEN \'Department\' END',$_POST['columns'][4]["search"]["value"]);
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('CASE WHEN ISNULL(LOC_Parent_id, \'\') = \'\' THEN \'Parent\' WHEN LOC_Parent_id != \'\' THEN LOC_Parent_id END',$_POST['columns'][5]["search"]["value"]);
        }

        // End

        if(isset($_POST["search"]["value"])){
            if($_POST["search"]["value"] != ''){  
                $this->db->like("LOC_Id", $_POST["search"]["value"]);  
                $this->db->or_like("LOC_Name", $_POST["search"]["value"]);  
                $this->db->or_like("LOC_FK_Level_id", $_POST["search"]["value"]);
                $this->db->or_like("CASE LOC_FK_Type_id WHEN 1 THEN \'Warehouse\' WHEN 2 THEN \'Business Unit\' WHEN 3 THEN \'Department\' END", $_POST["search"]["value"]);
                $this->db->or_like("CASE LOC_Parent_id WHEN NULL THEN \'Parent\' WHEN NOT NULL THEN LOC_Parent_id END", $_POST["search"]["value"]);
            }  
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('LOC_FK_Type_id desc');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblLocation');  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblLocation');  
           return $query->num_rows();  
    }

    /* datatable query ends here */ 


}
