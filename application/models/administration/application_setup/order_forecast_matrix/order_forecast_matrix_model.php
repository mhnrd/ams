<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class order_forecast_matrix_model extends MAIN_Model {

    protected $_table = 'tblOFMatrix';
    private $id = NULL;
    private $order_column = array(null,'OFM_Code','OFM_Location','OFM_Remarks');

    public function __construct() {
        parent::__construct();
    }


    /* datatable query starts here */
    public function table_data(){

        
        $this->db->select("*, SP_StoreName");  
        $this->db->from('tblOFMatrix');
        $this->db->join('tblStoreProfile', 'OFM_Location = SP_ID', 'left');
        $this->db->where_in('OFM_Location', array_column(getUserLocationAccess(),'CA_FK_Location_id')); 
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select(array(
                                'OFM_Code','OFM_Location','OFM_Remarks', 'SP_StoreName'
                        ));
        $this->db->join('tblStoreProfile', 'OFM_Location = SP_ID', 'left');
        $this->db->where_in('OFM_Location', array_column(getUserLocationAccess(),'CA_FK_Location_id'));

        // Individual Column Search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('OFM_Code',$_POST['columns'][1]["search"]["value"]);
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('SP_StoreName',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('OFM_Remarks',$_POST['columns'][3]["search"]["value"]);
        }

        // End

        if(isset($_POST["search"]["value"])){
            if($_POST["search"]["value"] != ''){  
                $this->db->like("OFM_Code", $_POST["search"]["value"]);  
                $this->db->or_like("SP_StoreName", $_POST["search"]["value"]);  
                $this->db->or_like("OFM_Remarks", $_POST["search"]["value"]);
            }  
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('OFM_Code', 'OFM_Location', 'OFM_Remarks');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblOFMatrix');  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblOFMatrix');  
           return $query->num_rows();  
    }

    /* datatable query ends here */ 

    public function getAll($docno = null){
        
        $query_string = 'SELECT OFM_Code, OFM_Remarks, OFM_Location
                         FROM tblOFMatrix';
        if(!empty($docno)){
            $query_string .= ' WHERE '.$this->sql_hash('OFM_Code').' = \''.$docno.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();   
        }
        return $result;
    }

    public function getAllLocation($of_code){
        $query_string = 'SELECT        OFML.OFML_OFM_Code, OFML.OFML_Location, OFML.OFML_Category, SP.SP_StoreName, IG.IG_Desc, OFML.OFML_EntryNo
                         FROM            tblOFMatrix_Location AS OFML RIGHT OUTER JOIN
                                         tblItemGroup AS IG ON OFML.OFML_Category = IG.IG_Id RIGHT OUTER JOIN
                                         tblStoreProfile AS SP ON OFML.OFML_Location = SP.SP_ID
                         WHERE        '.$this->sql_hash('OFML.OFML_OFM_Code').' = \''.$of_code.'\'
                         ORDER BY       OFML.OFML_EntryNo';

        $result = $this->db->query($query_string)->result_array();

        return $result;
    }

    public function getAllItem($of_code){

        $query_string = 'SELECT        OFMI.OFMI_OFM_Code, OFMI.OFMI_ItemNo, OFMI.OFMI_UOM AS UOM_Id, I.IM_Sales_Desc, AD.AD_Desc, AD.AD_Id,
                                        IUC.IUC_Quantity AS BaseUOMQty, I.IM_FK_Attribute_UOM_id AS BaseUOM, OFMI_EntryNo
                        FROM            tblOFMatrix_Item AS OFMI LEFT OUTER JOIN
                                        tblItemUOMConv AS IUC ON OFMI.OFMI_ItemNo = IUC.IUC_FK_Item_id AND OFMI.OFMI_UOM = IUC.IUC_FK_UOM_id LEFT OUTER JOIN
                                        tblAttributeDetail AS AD ON OFMI.OFMI_UOM = AD.AD_Id LEFT OUTER JOIN
                                        tblItem AS I ON OFMI.OFMI_ItemNo = I.IM_Item_Id
                        WHERE        '.$this->sql_hash('OFMI.OFMI_OFM_Code').' = \''.$of_code.'\'
                        ORDER BY        OFMI.OFMI_EntryNo';

        $result = $this->db->query($query_string)->result_array();

        return $result;
    }

    public function delete_data($docno){
        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USE.
            --insert code here--
        */ 
        $this->db->trans_begin();
        $this->db->delete('tblOFMatrix_Item',array('OFMI_OFM_Code'=>$docno));
        $this->db->delete('tblOFMatrix_Location',array('OFML_OFM_Code'=>$docno));
        $this->db->delete('tblOFMatrix',array('OFM_Code'=>$docno));

        if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return array('success'=>0);
        }
        else{
                $this->db->trans_commit();
                return array('success'=>1);
        }

    }

}
