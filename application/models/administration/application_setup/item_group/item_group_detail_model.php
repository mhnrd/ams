<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class item_group_detail_model extends MAIN_Model {

    private $id = NULL;

    public function __construct() {
        parent::__construct();
    }

    public function table_detail_data($docno = null){
        
        $query_string = 'SELECT IGD_LineNo, IGD_FK_Item_Id, IGD_FK_IG_Id, IM_Sales_Desc
                         FROM tblItemGroupDetail 
                            LEFT OUTER JOIN tblItem ON IGD_FK_Item_Id = IM_Item_Id 
                            WHERE ('.$this->sql_hash('IGD_FK_IG_Id').' = \''.$docno.'\')
                            ORDER BY IGD_LineNo';
        $result = $this->db->query($query_string)->result_array();      
        return $result;
    }

}
