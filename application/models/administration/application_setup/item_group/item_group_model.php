<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class item_group_model extends MAIN_Model {

    private $id = NULL;
    private $order_column = array(null,null,'IG_Id','IG_Desc','IG_Active');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data(){

        
        $this->db->select("*");  
        $this->db->from('tblItemGroup');  
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('IG_Id, IG_Desc, IG_Active');

        // Individual column search

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('IG_Id',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('IG_Desc',$_POST['columns'][3]["search"]["value"]);
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('CASE IG_Active WHEN 1 THEN \'Active\' WHEN 0 THEN \'Inactive\' END',$_POST['columns'][4]["search"]["value"]);
        }

        // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("IG_Id", $_POST["search"]["value"]);  
                $this->db->or_like("IG_Desc", $_POST["search"]["value"]);  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('IG_Id', 'DESC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblItemGroup');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblItemGroup');  
           return $query->num_rows();  
    }

    /* datatable query ends here */ 

    public function getAll($docno = null){
        
        $query_string = 'SELECT IG_Id,IG_Desc,IG_Active
                         FROM tblItemGroup ';
        if(!empty($docno)){
            $query_string .= ' WHERE '.$this->sql_hash('IG_Id').' = \''.$docno.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();   
        }
        return $result;
    }

    public function getAll_Active(){        
        $query_string = 'SELECT IG_Id,IG_Desc,IG_Active
                         FROM tblItemGroup 
                         WHERE IG_Active = \'1\' 
                         ORDER BY IG_Desc';        
        return $this->db->query($query_string)->result_array();       
        
    }

    public function getAllActive(){
       $query_string = 'SELECT *
                        FROM tblItemGroup
                        WHERE [BOM_Status] = \'Active\'';
        $params = array();
        $result = $this->db->query($query_string, $params);
        return $result->result_array();
    }

    public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */
    
        $this->db->trans_begin();
        $this->db->delete('tblItemGroupDetail',array('IGD_FK_IG_Id'=>$docno));
        $this->db->delete('tblItemGroup',array('IG_Id'=>$docno));

        if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return array('success'=>0);
        }
        else{
                $this->db->trans_commit();
                return array('success'=>1);
        }

    }

    public function status_data($docno,$action){

        $this->db->trans_begin();
        foreach ($docno as $row) {
            if($action == 'active'){
                $this->db->where('IG_Id',$row)->update('tblItemGroup',array('IG_Active'=>'1'));
            }
            else{
                $this->db->where('IG_Id',$row)->update('tblItemGroup',array('IG_Active'=>'0'));
            }
        }
        if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return array('success'=>0);
        }
        else{
                $this->db->trans_commit();
                return array('success'=>1);
        }

    }

}
