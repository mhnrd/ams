<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payroll_items_model extends MAIN_Model {

    protected $_table = 'tblPAYItems';

    private $id = NULL;
    private $order_column = array(null,'PI_ID','PI_Desc','PI_Type', 'PI_Basis2Compute','PI_Sperate','PI_Regrate','PI_Taxable','AD_Desc');

    public function __construct() {
        parent::__construct();
    }

  

    /* datatable query starts here */
    public function table_data(){

        
        $this->db->select('PI_ID, PI_Desc, 
            CASE PI_Type WHEN \'E\' THEN \'Earnings\' 
                                     WHEN \'D\' THEN \'Deduction\'
                                     WHEN \'M\' THEN \'Monthly\'
                                     WHEN \'A\' THEN \'Amount\'
                                     ELSE \'\' END as PI_Type, 
             CASE PI_Basis2Compute WHEN \'D\' THEN \'Daily\' 
                                     WHEN \'H\' THEN \'Hourly\'
                                     WHEN \'M\' THEN \'Minute\'
                                     WHEN \'A\' THEN \'Amount\'
                                     ELSE \'\' END as PI_Basis2Compute, PI_Sperate,PI_Regrate,PI_Taxable,AD_Desc');
        //$this->db->where('tblCPCenter.CreatedBy', getCurrentUser()['login-user']);
        $this->db->from('tblPAYItems');
        $this->db->join('tblAttributeDetail', 'AD_Id = PI_AttAllowTypeId', 'left');
        $result_count = $this->db->count_all_results(); 

        $result_item =  $this->datatables();
       foreach ($result_item as $key => $value) {
            
        }

        $output['data']              = $this->datatables();
        $this->print_lastquery();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('PI_ID, PI_Desc, PI_Type, 
             CASE PI_Basis2Compute WHEN \'D\' THEN \'Daily\' 
                                     WHEN \'H\' THEN \'Hourly\'
                                     WHEN \'M\' THEN \'Minute\'
                                     WHEN \'A\' THEN \'Amount\'
                                     ELSE \'\' END as PI_Basis2Compute, PI_Sperate,PI_Regrate,PI_Taxable,AD_Desc');
        $this->db->join('tblAttributeDetail', 'AD_Id = PI_AttAllowTypeId', 'left');
        //$this->db->where('tblCPCenter.CreatedBy', getCurrentUser()['login-user']);

        // Individual Column Search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('PI_ID', trim($_POST['columns'][1]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('PI_Desc', trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('CASE PI_Type WHEN \'D\' THEN \'Deduction\'  WHEN \'E\' THEN \'Earnings\' WHEN \'M\' THEN \'Monthly\' WHEN \'A\' THEN \'Amount\' ELSE \'\' END ', trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('CASE PI_Basis2Compute WHEN \'D\' THEN \'Daily\'  WHEN \'H\' THEN \'Hourly\' WHEN \'M\' THEN \'Minute\' WHEN \'A\' THEN \'Amount\' ELSE \'\' END ',trim($_POST['columns'][4]["search"]["value"]));
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('PI_Sperate',trim($_POST['columns'][5]["search"]["value"]));
        }
        if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
            $this->db->like('PI_Regrate',trim($_POST['columns'][6]["search"]["value"]));
        }
        if(isset($_POST['columns'][7]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $this->db->like('PI_Taxable',trim($_POST['columns'][7]["search"]["value"]));
        }
        if(isset($_POST['columns'][8]["search"]["value"]) && !empty($_POST['columns'][8]["search"]["value"])){
            $this->db->like('AD_Desc',trim($_POST['columns'][8]["search"]["value"]));
        }
        // End
        
        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){
                $this->db->group_start();
                $this->db->like("PI_ID", trim($_POST["search"]["value"])); 
                $this->db->or_like("PI_Desc", trim($_POST["search"]["value"]));
                $this->db->or_like("CASE PI_Type WHEN 'D' THEN 'Deduction' WHEN 'E' THEN 'Earnings' WHEN 'M' THEN 'Monthly' WHEN 'A' THEN 'Amount' END", $_POST["search"]["value"]);
                 $this->db->or_like("CASE PI_Basis2Compute WHEN 'D' THEN 'Daily' WHEN 'H' THEN 'Hourly' WHEN 'M' THEN 'Monthly' WHEN 'A' THEN 'Amount' END", $_POST["search"]["value"]);
                $this->db->or_like("PI_Sperate", trim($_POST["search"]["value"])); 
                $this->db->or_like("PI_Regrate", trim($_POST["search"]["value"]));
                $this->db->or_like("PI_Taxable", trim($_POST["search"]["value"]));
                $this->db->or_like("AD_Desc", trim($_POST["search"]["value"])); 
                $this->db->group_end();                   
            }
        }


        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('PI_ID', 'DESC');  
        }
    }
     public function getAll($doc_no = null){
       $query_string = 'SELECT *
                        FROM tblPAYItems';
        if(!empty($doc_no)){
            $query_string .= ' WHERE ' . $this->sql_hash('PI_ID') . ' = \''.$doc_no.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();
        }
        return $result;
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblPAYItems');  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblPAYItems');  
           return $query->num_rows();  
    }
    public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */
    
        $this->db->trans_begin();
        $this->db->delete('tblPAYItems',array('PI_ID'=>$docno));
        
        if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return array('success'=>0);
        }
        else{
                $this->db->trans_commit();
                return array('success'=>1);
        }

    }

    public function getAllowanceType(){
         $query_string = 'SELECT AD_FK_Code, AD_Code, AD_Desc, AD_Id
                         FROM tblAttributeDetail AS AD 
                         WHERE AD_Active = \'1\' AND AD_FK_Code = \'102401\' ';
        return $this->db->query($query_string)->result_array();
    }
    public function getLastGroupCode(){
        $query_string = 'SELECT     TOP 1 ISNULL(CAST(PI_ID as int), 2000000) as PI_ID 
                         FROM       tblPAYItems
                         ORDER BY   CAST(PI_ID as int) DESC';

        return $this->db->query($query_string)->row_array()['PI_ID'] + 1;
    }
}
