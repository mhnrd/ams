<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tax_exemptions_model extends MAIN_Model {

	
    protected $_table = 'tblTaxMatrix';
    
    private $id = NULL;
    private $order_column = array(null,null,'TM_TaxType');
    private $order_column_TM = array(null,null,'TM_ID','TM_OverAmount','TM_NotOver','TM_TaxDue','TM_Percent');
    // private $order_column = array(null,'TM_TaxType');
    // private $order_column1 = array(null,'TM_ID','TM_OverAmount','TM_NotOver','TM_TaxDue','TM_Percent');

    public function __construct() {
        parent::__construct();
    }


    // DATA TABLE STARTS HERE
    public function table_data($status = null){

        $this->db->select('TM_TaxType');
        $this->db->from('tblTaxMatrix');

        $result_count = $this->db->count_all_results();

        $output['data']                 = $this->datatables($status);
        $output['num_rows']             = $this->get_filtered_data($status);
        $output['count_all_results']    = $result_count;

        return $output;
        
    }  

    public function query($status = null){
        $this->db->select('TM_TaxType');

        // INDIVIDUAL COLUMN SEARCH

        if(isset($_POST['columns'][0]["search"]["value"]) && !empty($_POST['columns'][0]["search"]["value"])){
            $this->db->like('TM_TaxType',trim($_POST['columns'][0]["search"]["value"]));
        }

        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("TM_TaxType", trim($_POST["search"]["value"]));  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('TM_TaxType', 'DESC');  
        }
    }

    public function datatables($status = null){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblTaxMatrix');  
        return $query->result_array();
    }

     public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblTaxMatrix');  
           return $query->num_rows();  
    }

    // END 

     // DATA TABLE FOR TAX EXEMPTION DETAIL STARTS HERE
    public function table_data_TM($docno = null){

        $this->db->select('TM_ID, TM_OverAmount, TM_NotOver, TM_TaxDue,TM_Percent');
        $this->db->from('tblTaxMatrix');
        $result_count = $this->db->count_all_results();

        $output['data']                 = $this->datatables_TM($docno);
        $output['num_rows']             = $this->get_filtered_data_TM($docno);
        $output['count_all_results']    = $result_count;

        return $output;
    }  

    public function query_TM($docno){
        $this->db->select('TM_ID, TM_OverAmount, TM_NotOver, TM_TaxDue,TM_Percent');

        // INDIVIDUAL COLUMN SEARCH

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('TM_ID',trim($_POST['columns'][1]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('TM_OverAmount',trim($_POST['columns'][2]["search"]["value"]));
        }

         if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('TM_NotOver',trim($_POST['columns'][3]["search"]["value"]));
        }
        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('TM_TaxDue',trim($_POST['columns'][4]["search"]["value"]));
        }
        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('TM_Percent',trim($_POST['columns'][5]["search"]["value"]));
        }


        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("TM_ID", trim($_POST["search"]["value"]));  
                $this->db->or_like("TM_OverAmount", trim($_POST["search"]["value"]));
                $this->db->or_like("TM_NotOver", trim($_POST["search"]["value"]));
                $this->db->or_like("TM_TaxDue", trim($_POST["search"]["value"]));
                $this->db->or_like("TM_Percent", trim($_POST["search"]["value"]));  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column_TM[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('TM_ID', 'DESC');  
        }
    }

    public function datatables_TM($docno){
        $this->query_TM($docno); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblTaxMatrix');  
        return $query->result_array();
    }

    public function get_filtered_data_TM($docno){  
           $this->query_TM($docno);  
           $query = $this->db->get('tblTaxMatrix'); 
           return $query->num_rows();  
    }

    public function getLastGroupCode(){
        $query_string = 'SELECT     * 
                         FROM       tblTaxMatrix';

        return $this->db->query($query_string)->num_rows() + 1;
    
}
    //END


     // DATA TABLE FOR TAX EXEMPTION DETAIL STARTS HERE
    public function table_data_detail($docno = null){

        $this->db->select('TM_ID, TM_OverAmount, TM_NotOver, TM_TaxDue,TM_Percent');
        $this->db->from('tblTaxMatrix');
        $result_count = $this->db->count_all_results();

        $output['data']                 = $this->datatables_detail($docno);
        $output['num_rows']             = $this->get_filtered_data_detail($docno);
        $output['count_all_results']    = $result_count;

        return $output;
    } 

    public function query_detail($docno){
        $this->db->select('TM_ID, TM_OverAmount, TM_NotOver, TM_TaxDue,TM_Percent');

        // INDIVIDUAL COLUMN SEARCH

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('TM_ID',trim($_POST['columns'][1]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('TM_OverAmount',trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('TM_NotOver',trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('TM_TaxDue',trim($_POST['columns'][4]["search"]["value"]));
        }

        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("TM_ID", trim($_POST["search"]["value"]));  
                $this->db->or_like("TM_OverAmount", trim($_POST["search"]["value"]));  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('TM_ID', 'DESC');  
        }
    }

    public function datatables_detail($docno){
        $this->query_detail($docno); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblTaxMatrix');  
        return $query->result_array();
    }

    public function get_filtered_data_detail($docno){  
           $this->query_detail($docno);  
           $query = $this->db->get('tblTaxMatrix'); 
           return $query->num_rows();  
    }

}