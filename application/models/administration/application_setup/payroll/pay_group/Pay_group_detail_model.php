<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pay_group_detail_model extends MAIN_Model {

    protected $_table = 'tblPAYGroupAccess';

    private $id = NULL;
    private $order_column = array(null,'GA_FK_User_Id','U_AssociatedEmployeeId','U_Username','GA_G_ID');

    public function __construct() {
        parent::__construct();
    }

    public function searchStyles($type, $filter){
        $query_string = 'SELECT     RTRIM(U_ID) as U_ID,RTRIM(U_Username) as U_Username,RTRIM(U_AssociatedEmployeeId) as U_AssociatedEmployeeId
                         FROM       tblUser
                         WHERE      '.$type.' LIKE \'%'.$filter.'%\'';

        return $this->db->query($query_string)->result_array();
    }
    


  

    /* datatable query starts here */
   public function table_data($doc_no){

        $this->db->select('GA_G_ID,GA_FK_User_Id, U_Username,U_AssociatedEmployeeId');
        $this->db->from('tblPAYGroupAccess');
        $this->db->join('tblUser', 'GA_FK_User_Id = U_ID', 'left');
        $this->db->where('GA_G_ID', $doc_no);
        $result_count = $this->db->count_all_results();

        $output['data']                 = $this->datatables($doc_no);
        $output['num_rows']             = $this->get_filtered_data($doc_no);
        $output['count_all_results']    = $result_count;

        return $output;
    }   

  public function query($doc_no){
        $this->db->select(',GA_G_ID,U_Username, GA_FK_User_Id,U_AssociatedEmployeeId');
        $this->db->where('GA_G_ID', $doc_no);

        // INDIVIDUAL COLUMN SEARCH
        
        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("GA_FK_User_Id", $_POST["search"]["value"]);
                $this->db->like("U_Username", $_POST["search"]["value"]);
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('GA_FK_User_Id', 'ASC');  
        }
    }

    public function datatables($doc_no){
        $this->query($doc_no); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $this->db->join('tblUser', 'GA_FK_User_Id = U_ID', 'left');
        $query = $this->db->get('tblPAYGroupAccess');  
        return $query->result_array();
    }

    public function get_filtered_data($doc_no){ 
        $this->query($doc_no);
        $this->db->from('tblPAYGroupAccess');
        $this->db->join('tblUser', 'GA_FK_User_Id = U_ID', 'left');
        $query = $this->db->count_all_results();  
        return $query;  

    }
    // END For Detail User ID,Employee ID, Username

    // Start function for table userid and username
    public function table_data_PGD($paygroup_id){
        $this->db->select('GA_G_ID,GA_FK_User_Id,U_ID,U_Username');
        $this->db->from('tblPAYGroupAccess');
        $this->db->join('tblUser', 'GA_FK_User_Id = U_ID', 'left');
        $this->db->where('GA_G_ID', $paygroup_id);
        $result_count = $this->db->count_all_results();

        $output['data']                 = $this->datatables_PGD($paygroup_id);
        $output['num_rows']             = $this->get_filtered_data_PGD($paygroup_id);
        $output['count_all_results']    = $result_count;

        return $output;
    }
    public function query_PGD($paygroup_id){
        $this->db->select('GA_G_ID,U_Username, GA_FK_User_Id,U_AssociatedEmployeeId');
        $this->db->where('GA_G_ID', $paygroup_id);

        // INDIVIDUAL COLUMN SEARCH

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('GA_G_ID',trim($_POST['columns'][1]["search"]["value"]));
        }
        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('U_Username',trim($_POST['columns'][2]["search"]["value"]));
        }

        
        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("GA_FK_User_Id", trim($_POST["search"]["value"]));
                $this->db->like("U_Username", trim($_POST["search"]["value"]));
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('GA_FK_User_Id', 'ASC');  
        }
    }
    public function datatables_PGD($paygroup_id){
        $this->query($paygroup_id); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $this->db->join('tblUser', 'GA_FK_User_Id = U_ID', 'left');
        $query = $this->db->get('tblPAYGroupAccess');  
        return $query->result_array();
    }

    public function get_filtered_data_PGD($paygroup_id){ 
        $this->query($paygroup_id);
        $this->db->from('tblPAYGroupAccess');
        $this->db->join('tblUser', 'GA_FK_User_Id = U_ID', 'left');
        $query = $this->db->count_all_results();  
        return $query;  

    }
}
