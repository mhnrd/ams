<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pay_group_model extends MAIN_Model {

    protected $_table = 'tblPAYGroup';

    private $id = NULL;
    private $order_column = array(null,'COM_Id','G_ID','G_Desc', 'CASE G_PAY_TaxCuttoff WHEN \'2\' THEN \'2nd Cutoff\' ELSE \'Split\' END', 'CASE G_PAY_SSSDeduct WHEN \'W\' THEN \'Weekly\' 
                                     WHEN \'S\' THEN \'Semi-Monthly\'
                                     WHEN \'M\' THEN \'Monthly\'
                                     WHEN \'A\' THEN \'Amount\'
                                     ELSE \'\' END', 'CASE G_PAY_SSSCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END','CASE G_PAY_PagibigDeduct WHEN \'W\' THEN \'Weekly\' WHEN \'S\' THEN \'Semi-Monthly\' ELSE \'Monthly\' END','CASE G_PAY_PagibigCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END','CASE G_PAY_PhilhealthDeduct WHEN \'W\' THEN \'Weekly\' WHEN \'S\' THEN \'Semi-Monthly\' ELSE \'Monthly\' END','CASE G_PAY_PhilhealthCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END');
    private $user_order_column = array(null,'U_ID');

    public function __construct() {
        parent::__construct();
    }

  

    /* datatable query starts here */
    public function table_data($status = null){

        
        $this->db->select('COM_Id, G_ID, G_Desc, C.COM_PAY_TaxDeduct, G_PAY_TaxCuttoff, 
            CASE G_PAY_SSSDeduct WHEN \'W\' THEN \'Weekly\' 
                                     WHEN \'S\' THEN \'Semi-Monthly\'
                                     WHEN \'M\' THEN \'Monthly\'
                                     WHEN \'A\' THEN \'Amount\'
                                     ELSE \'\' END as G_PAY_SSSDeduct, G_PAY_SSSCutoff, G_PAY_PagibigDeduct, G_PAY_PagibigCutoff,G_PAY_PhilhealthDeduct,G_PAY_PhilhealthCutoff, COM_Name');
        //$this->db->where('tblPAY_Group.CreatedBy', getCurrentUser()['login-user']);
        
        $this->db->join('tblCompany as C','COM_Id = G_FK_CompanyId','left');
        $this->db->from('tblPAYGroup');
        $result_count = $this->db->count_all_results(); 

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('COM_Id, G_ID, G_Desc, CASE G_PAY_TaxCuttoff WHEN \'2\' THEN \'2nd Cutoff\' ELSE \'Split\' END as G_PAY_TaxCuttoff, 
            CASE G_PAY_SSSDeduct WHEN \'W\' THEN \'Weekly\' 
                                     WHEN \'S\' THEN \'Semi-Monthly\'
                                     WHEN \'M\' THEN \'Monthly\'
                                     WHEN \'A\' THEN \'Amount\'
                                     ELSE \'\' END as G_PAY_SSSDeduct, 
                                     CASE G_PAY_SSSCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END as G_PAY_SSSCutoff, 
                                     CASE G_PAY_PagibigDeduct WHEN \'W\' THEN \'Weekly\' WHEN \'S\' THEN \'Semi-Monthly\' ELSE \'Monthly\' END as G_PAY_PagibigDeduct, 
                                     CASE G_PAY_PagibigCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END as G_PAY_PagibigCutoff,
                                     CASE G_PAY_PhilhealthDeduct WHEN \'W\' THEN \'Weekly\' WHEN \'S\' THEN \'Semi-Monthly\' ELSE \'Monthly\' END as G_PAY_PhilhealthDeduct,
                                     CASE G_PAY_PhilhealthCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END as G_PAY_PhilhealthCutoff,COM_Name,C.COM_PAY_TaxDeduct');
         $this->db->join('tblCompany as C','COM_Id = G_FK_CompanyId','left');

        //$this->db->where('tblPAY_Group.CreatedBy', getCurrentUser()['login-user']);
        // Individual Column Search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('COM_Id',trim($_POST['columns'][1]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('G_ID',trim($_POST['columns'][2]["search"]["value"]));
        }

        if (isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])) {
            $this->db->like('G_Desc', trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('CASE G_PAY_TaxCuttoff WHEN \'2\' THEN \'2nd Cutoff\' ELSE \'Split\' END',trim($_POST['columns'][4]["search"]["value"]));
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('CASE G_PAY_SSSDeduct WHEN \'W\' THEN \'Weekly\'  WHEN \'S\' THEN \'Semi-Monthly\' WHEN \'M\' THEN \'Monthly\' WHEN \'A\' THEN \'Amount\' ELSE \'\' END ',trim($_POST['columns'][5]["search"]["value"]));
        }

        if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
            $this->db->like('CASE G_PAY_SSSCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END',trim($_POST['columns'][6]["search"]["value"]));
        }

        if(isset($_POST['columns'][7]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $this->db->like('CASE G_PAY_PagibigDeduct WHEN \'W\' THEN \'Weekly\' WHEN \'S\' THEN \'Semi-Monthly\' ELSE \'Monthly\' END',trim($_POST['columns'][7]["search"]["value"]));
        }
        if(isset($_POST['columns'][8]["search"]["value"]) && !empty($_POST['columns'][8]["search"]["value"])){
            $this->db->like('CASE G_PAY_PagibigCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END',trim($_POST['columns'][8]["search"]["value"]));
        }
        if(isset($_POST['columns'][9]["search"]["value"]) && !empty($_POST['columns'][9]["search"]["value"])){
            $this->db->like('CASE G_PAY_PhilhealthDeduct WHEN \'W\' THEN \'Weekly\' WHEN \'S\' THEN \'Semi-Monthly\' ELSE \'Monthly\' END',trim($_POST['columns'][9]["search"]["value"]));
        }
        if(isset($_POST['columns'][10]["search"]["value"]) && !empty($_POST['columns'][10]["search"]["value"])){
            $this->db->like('CASE G_PAY_PhilhealthCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END',trim($_POST['columns'][10]["search"]["value"]));
        }


        // End
        
        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){
                $this->db->like("COM_Id", trim($_POST["search"]["value"]));
                $this->db->or_like("G_ID", trim($_POST["search"]["value"]));
                $this->db->or_like("G_Desc", trim($_POST["search"]["value"]));
                $this->db->or_like('CASE G_PAY_TaxCuttoff WHEN \'2\' THEN \'2nd Cutoff\' ELSE \'Split\' END', trim($_POST["search"]["value"]));
               $this->db->or_like("CASE G_PAY_SSSDeduct WHEN 'W' THEN 'Weekly' WHEN 'S' THEN 'Semi-Monthly' WHEN 'M' THEN 'Monthly' WHEN 'A' THEN 'Amount' END", $_POST["search"]["value"]);
                $this->db->or_like('CASE G_PAY_SSSCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END', trim($_POST["search"]["value"]));
                $this->db->or_like("CASE G_PAY_PagibigDeduct WHEN 'W' THEN 'Weekly' WHEN 'S' THEN 'Semi-Monthly' WHEN 'M' THEN 'Monthly' WHEN 'A' THEN 'Amount' END", $_POST["search"]["value"]);
                $this->db->or_like('CASE G_PAY_PagibigCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END', trim($_POST["search"]["value"]));
                $this->db->or_like("CASE G_PAY_PhilhealthDeduct WHEN 'W' THEN 'Weekly' WHEN 'S' THEN 'Semi-Monthly' WHEN 'M' THEN 'Monthly' WHEN 'A' THEN 'Amount' END", $_POST["search"]["value"]);
                $this->db->or_like('CASE G_PAY_PhilhealthCutoff WHEN \'1\' THEN \'1st Cutoff\' WHEN \'2\' THEN \'2nd Cutoff\' WHEN \'3\' THEN \'3rd Cutoff\' WHEN \'4\' THEN \'4th Cutoff\' ELSE \'Split\' END', trim($_POST["search"]["value"]));


            }
        }

        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('G_ID', 'DESC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblPAYGroup');  
        return $query->result_array();
    }
    public function getAll($doc_no = null){
       $query_string = 'SELECT G.G_FK_CompanyId, G.G_ID, G.G_Desc, G.G_PAY_CutOffStart, G.G_PAY_CutOffEnd, G.G_PAY_Payperiod, G.G_PAY_PreparedBy, G.G_PAY_ApprovedBy, G.G_PAY_BankAccount, G.CreatedBy, G.DateCreated, 
                         G.ModifiedBy, G.DateModified, G.COM_PAY_TaxDeduct, G.G_PAY_SSSDeduct, G.G_PAY_PagibigDeduct, G.G_PAY_PhilhealthDeduct, G.G_PAY_TaxCuttoff, G.G_PAY_SSSCutoff, G.G_PAY_PagibigCutoff, 
                         G.G_PAY_PhilhealthCutoff, G.G_PAY_WorkDays, G.G_PAY_WorkDays_D, G.G_PAY_NoAttendance, C.COM_Name,C.COM_Id
            FROM tblPAYGroup AS G INNER JOIN
            tblCompany AS C ON G.G_FK_CompanyId = C.COM_Id';
        if(!empty($doc_no)){
            $query_string .= ' WHERE ' . $this->sql_hash('G_ID') . ' = \''.$doc_no.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();
        }
        return $result;
    }
     public function getAll_Active(){        
        $query_string = 'SELECT COM_Id,COM_Name
                         FROM tblCompany AS c 
                         WHERE COM_Active  = \'1\' ';        
        return $this->db->query($query_string)->result_array();       
        
    }
     public function getUser_ID(){
        $query_string = 'SELECT U_ID,U_Username
                         FROM tblUser';
        return $this->db->query($query_string)->result_array();

    }

    public function getAllActive(){
       $query_string = 'SELECT *
                        FROM tblPAYGroup
                        ';
        $params = array();
        $result = $this->db->query($query_string, $params);
        return $result->result_array();
    }
     public function getUserName(){
        $query_string = 'SELECT U_ID,U_Username
                         FROM tblUser';
        return $this->db->query($query_string)->result_array();

    }


    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblPAYGroup');  
           return $query->num_rows();  
    }

    public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */
    
       // $this->db->trans_begin();
        $this->db->delete('tblPAYGroup',array('G_ID'=>$docno));
        
        if ($this->db->trans_status() === FALSE){
                //$this->db->trans_rollback();
                return array('success'=>0);
        }
        else{
                //$this->db->trans_commit();
                return array('success'=>1);
        }

    }

   


}
