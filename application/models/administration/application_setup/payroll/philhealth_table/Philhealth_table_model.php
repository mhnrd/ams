<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Philhealth_table_model extends MAIN_Model {

    protected $_table = 'tblPhilHealthTable';

    private $id = NULL;
    private $order_column = array(null,'PT_ID','PT_SalaryRangeFrom','PT_SalaryRangeTo', 'PT_Salarybase','PT_Emprshare','PT_Empeshare','PT_MonthlyCont','PT_Percentage');

    public function __construct() {
        parent::__construct();
    }

  

    /* datatable query starts here */
    public function table_data(){

        
        $this->db->select('PT_ID, PT_SalaryRangeFrom, PT_SalaryRangeTo, PT_Salarybase, PT_Emprshare,PT_Empeshare,PT_MonthlyCont,PT_Percentage');
        //$this->db->where('tblCPCenter.CreatedBy', getCurrentUser()['login-user']);
        $this->db->from('tblPhilHealthTable');
        //$this->db->join('tblAttributeDetail', 'AD_Id = CPC_FK_Class', 'left');
        $result_count = $this->db->count_all_results(); 

        $result_item =  $this->datatables();
       foreach ($result_item as $key => $value) {
            
        }

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('PT_ID, PT_SalaryRangeFrom, PT_SalaryRangeTo, PT_Salarybase, PT_Emprshare,PT_Empeshare,PT_MonthlyCont,PT_Percentage');

        // Individual Column Search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('PT_ID', trim($_POST['columns'][1]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('PT_SalaryRangeFrom', trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('PT_SalaryRangeTo', trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('PT_Salarybase',trim($_POST['columns'][4]["search"]["value"]));
        }
        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('PT_Emprshare',trim($_POST['columns'][5]["search"]["value"]));
        }
        if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
            $this->db->like('PT_Empeshare',trim($_POST['columns'][6]["search"]["value"]));
        }
        if(isset($_POST['columns'][7]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $this->db->like('PT_MonthlyCont',trim($_POST['columns'][7]["search"]["value"]));
        }
        if(isset($_POST['columns'][8]["search"]["value"]) && !empty($_POST['columns'][8]["search"]["value"])){
            $this->db->like('PT_Percentage',trim($_POST['columns'][8]["search"]["value"]));
        }

        // End
        
        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){
                $this->db->like("PT_ID", trim($_POST["search"]["value"])); 
                $this->db->or_like("PT_SalaryRangeFrom", trim($_POST["search"]["value"]));
                $this->db->or_like("PT_SalaryRangeTo", trim($_POST["search"]["value"]));
                $this->db->or_like("PT_Salarybase", trim($_POST["search"]["value"])); 
                $this->db->or_like("PT_Emprshare", trim($_POST["search"]["value"]));
                $this->db->or_like("PT_Empeshare", trim($_POST["search"]["value"]));
                $this->db->or_like("PT_MonthlyCont", trim($_POST["search"]["value"])); 
                $this->db->or_like("PT_Percentage", trim($_POST["search"]["value"]));        
            }
        }


        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('PT_ID', 'DESC');  
        }
    }
     public function getAll($doc_no = null){
       $query_string = 'SELECT *
                        FROM tblPhilHealthTable';
        if(!empty($doc_no)){
            $query_string .= ' WHERE ' . $this->sql_hash('PT_ID') . ' = \''.$doc_no.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();
        }
        return $result;
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblPhilHealthTable');  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblPhilHealthTable');  
           return $query->num_rows();  
    }

    public function getLastGroupCode(){
        $query_string = 'SELECT     * 
                         FROM       tblPhilHealthTable';

        return $this->db->query($query_string)->num_rows() + 1;
    }


}
