<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class doc_approval_model extends MAIN_Model {

	protected $_table = 'tblDocTracking';

	private $order_column = array(null, 'dt.DT_DocNo', 'dt.DT_Sender', 'dt.DT_Approver', 'dt.DT_Location', 'dt.DT_EntryDate');

	public function __construct() {
        parent::__construct();
    }

    public function isUnlimited(){
    	$query_string = 'SELECT        TOP 1 AppSetup.AS_Unlimited, U.U_FK_Position_id, U.U_ID
						 FROM          tblApprovalSetup AS AppSetup RIGHT OUTER JOIN
						               tblUser AS U ON AppSetup.AS_FK_Position_id = U.U_FK_Position_id
						 WHERE         U.U_ID = \''.getCurrentUser()['login-user'].'\'';

		$result = $this->db->query($query_string);

		$blnUnlimited = false;
		if($result->num_rows() > 0){
			$output = array(	
						'user-position' => $result->row_array()['U_FK_Position_id'],
						'bln-unlimited' => $result->row_array()['AS_Unlimited']
					);
		}

		else{
			$output = array(	
						'user-position' => '',
						'bln-unlimited' => false
					);
		}

		return $output;
    }

    /* dataTable query goes here */

    public function table_data(){
    	$blnUnlimited = $this->isUnlimited();
    	if($blnUnlimited['bln-unlimited']):
    		$query_string = 'SELECT 0 AS Selected, dt.DT_FK_NSCode, dt.DT_DocNo, dt.DT_Sender, dt.DT_Approver, dt.DT_Location, dt.DT_EntryDate, dt.DT_EntryNo, dt.DT_TargetURL, P.P_Position AS Sender, P2.P_Position AS Approver,
    								 tblNoSeries.NS_FK_Module_id, SP.SP_StoreName
							 FROM 	 tblDocTracking AS dt LEFT OUTER JOIN 
                                     tblPosition AS P2 ON dt.DT_Approver = P2.P_ID LEFT OUTER JOIN
                                     tblPosition AS P ON dt.DT_Sender = P.P_ID LEFT OUTER JOIN
                                     tblStoreProfile AS SP ON dt.DT_Location = SP.SP_ID LEFT OUTER JOIN
                                     tblNoSeries ON dt.DT_FK_NSCode = tblNoSeries.NS_Id
							 WHERE   (ISNUMERIC(dt.DT_FK_NSCode) = 1) AND dt.DT_Approver = \''.$blnUnlimited['user-position'].'\' AND dt.DT_Status = \'Pending\'';
    	else:
    		$query_string = 'SELECT 0 AS Selected, dt.DT_FK_NSCode, dt.DT_DocNo, dt.DT_Sender, dt.DT_Approver, dt.DT_Location, dt.DT_EntryDate, dt.DT_EntryNo, dt.DT_TargetURL, P.P_Position AS Sender, P2.P_Position AS Approver,
    								ns.NS_FK_Module_id, SP.SP_StoreName
							FROM tblDocTracking dt LEFT OUTER JOIN
                                 tblPosition AS P2 ON dt.DT_Approver = P2.P_ID LEFT OUTER JOIN
                                 tblPosition AS P ON dt.DT_Sender = P.P_ID LEFT OUTER JOIN
                                 tblStoreProfile AS SP ON dt.DT_Location = SP.SP_ID LEFT OUTER JOIN
                                 tblNoSeries ns ON dt.DT_FK_NSCode = ns.NS_Id 
							WHERE (ISNUMERIC(dt.DT_FK_NSCode) = 1) AND dt.DT_Approver = '.$blnUnlimited['user-position'].' AND dt.DT_Status = \'Pending\' AND (NOT EXISTS(SELECT DT_FK_NSCode, DT_DocNo, DT_EntryNo, DT_Status 
							FROM tblDocTracking 
							WHERE (DT_FK_NSCode = dt.DT_FK_NSCode) AND (DT_DocNo = dt.DT_DocNo) AND (DT_Status = \'Pending\') AND (DT_EntryNo < DT_EntryNo)))';
    	endif;

    	$result_count = $this->db->query($query_string)->num_rows();

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
    	$blnUnlimited = $this->isUnlimited();

    	if($blnUnlimited['bln-unlimited']):
    		$query_string = 'SELECT 0 AS Selected, dt.DT_FK_NSCode, dt.DT_DocNo, dt.DT_Sender, dt.DT_Approver, dt.DT_Location, dt.DT_EntryDate, dt.DT_EntryNo, dt.DT_TargetURL, P.P_Position AS Sender, P2.P_Position AS Approver,
                                     tblNoSeries.NS_FK_Module_id, SP.SP_StoreName
                             FROM    tblDocTracking AS dt LEFT OUTER JOIN 
                                     tblPosition AS P2 ON dt.DT_Approver = P2.P_ID LEFT OUTER JOIN
                                     tblPosition AS P ON dt.DT_Sender = P.P_ID LEFT OUTER JOIN
                                     tblStoreProfile AS SP ON dt.DT_Location = SP.SP_ID LEFT OUTER JOIN
                                     tblNoSeries ON dt.DT_FK_NSCode = tblNoSeries.NS_Id
                             WHERE   (ISNUMERIC(dt.DT_FK_NSCode) = 1) AND dt.DT_Approver = \''.$blnUnlimited['user-position'].'\' AND dt.DT_Status = \'Pending\'';
    	else:
    		$query_string = 'SELECT 0 AS Selected, dt.DT_FK_NSCode, dt.DT_DocNo, dt.DT_Sender, dt.DT_Approver, dt.DT_Location, dt.DT_EntryDate, dt.DT_EntryNo, dt.DT_TargetURL, P.P_Position AS Sender, P2.P_Position AS Approver,
                                    ns.NS_FK_Module_id, SP.SP_StoreName
                            FROM tblDocTracking dt LEFT OUTER JOIN
                                 tblPosition AS P2 ON dt.DT_Approver = P2.P_ID LEFT OUTER JOIN
                                 tblPosition AS P ON dt.DT_Sender = P.P_ID LEFT OUTER JOIN
                                 tblStoreProfile AS SP ON dt.DT_Location = SP.SP_ID LEFT OUTER JOIN
                                 tblNoSeries ns ON dt.DT_FK_NSCode = ns.NS_Id 
                            WHERE (ISNUMERIC(dt.DT_FK_NSCode) = 1) AND dt.DT_Approver = '.$blnUnlimited['user-position'].' AND dt.DT_Status = \'Pending\' AND (NOT EXISTS(SELECT DT_FK_NSCode, DT_DocNo, DT_EntryNo, DT_Status 
                            FROM tblDocTracking 
                            WHERE (DT_FK_NSCode = dt.DT_FK_NSCode) AND (DT_DocNo = dt.DT_DocNo) AND (DT_Status = \'Pending\') AND (DT_EntryNo < DT_EntryNo)))';
    	endif;

        // Individual column search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $query_string .= ' AND DT_DocNo LIKE \'%'.$_POST['columns'][1]["search"]["value"].'%\' ESCAPE \'!\'';
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $query_string .= ' AND DT_Sender LIKE \'%'.$_POST['columns'][2]["search"]["value"].'%\' ESCAPE \'!\'';
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $query_string .= ' AND DT_Approver LIKE \'%'.$_POST['columns'][3]["search"]["value"].'%\' ESCAPE \'!\'';
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $query_string .= ' AND DT_Location LIKE \'%'.$_POST['columns'][4]["search"]["value"].'%\' ESCAPE \'!\'';
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $dtFrom = substr($_POST['columns'][5]["search"]["value"],0,10);
            $dtTo = substr($_POST['columns'][5]["search"]["value"],13,10);
            $query_string .= ' AND DT_EntryDate BETWEEN \''.$dtFrom.'\' AND \''.$dtTo.'\'';
        }

        // End

    	if(isset($_POST["search"]["value"])){  
            if($_POST["search"]["value"] != ''){
                $query_string .= ' AND dt.DT_DocNo LIKE %'.$_POST["search"]["value"].'% OR dt.DT_Sender LIKE %'.$_POST["search"]["value"].'% OR dt.DT_Approver LIKE %'.$_POST["search"]["value"].'% OR dt.DT_Location LIKE %'.$_POST["search"]["value"].'% OR dt.DT_EntryDate LIKE %'.$_POST["search"]["value"].'%'; 
            }
        }  

        if(isset($_POST["order"])){  
            $query_string .= ' ORDER BY '.$this->order_column[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].'';  
        }  
        else{  
            // $this->db->order_by('DT_DocNo', 'DESC'); 
            $query_string .= ' ORDER BY dt.DT_DocNo DESC'; 
        }

        return $query_string;

    }

    public function datatables(){
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->query($this->query());  
        return $query->result_array();
    }

    public function get_filtered_data(){ 
       	$query = $this->db->query($this->query());  
       	return $query->num_rows();
    }

    /* dataTable query ends here */

    public function count_approval(){
        $blnUnlimited = $this->isUnlimited();

        $query_string = 'SELECT 0 AS Selected, dt.DT_FK_NSCode, dt.DT_DocNo, dt.DT_Sender, dt.DT_Approver, dt.DT_Location, dt.DT_EntryDate, dt.DT_EntryNo, dt.DT_TargetURL, P.P_Position AS Sender, P2.P_Position AS Approver,
                                    ns.NS_FK_Module_id
                            FROM tblDocTracking dt LEFT OUTER JOIN
                                 tblPosition AS P2 ON dt.DT_Approver = P2.P_ID LEFT OUTER JOIN
                                 tblPosition AS P ON dt.DT_Sender = P.P_ID LEFT OUTER JOIN
                                 tblNoSeries ns ON dt.DT_FK_NSCode = ns.NS_Id 
                            WHERE (ISNUMERIC(dt.DT_FK_NSCode) = 1) AND dt.DT_Approver = '.$blnUnlimited['user-position'].' AND dt.DT_Status = \'Pending\' AND (NOT EXISTS(SELECT DT_FK_NSCode, DT_DocNo, DT_EntryNo, DT_Status 
                            FROM tblDocTracking 
                            WHERE (DT_FK_NSCode = dt.DT_FK_NSCode) AND (DT_DocNo = dt.DT_DocNo) AND (DT_Status = \'Pending\') AND (DT_EntryNo < DT_EntryNo))) ORDER BY dt.DT_DocNo desc';

        $result = $this->db->query($query_string)->num_rows();

        return $result;
    }

    public function notifications(){
        $blnUnlimited = $this->isUnlimited();

        $query_string = 'SELECT TOP 5 0 AS Selected, dt.DT_FK_NSCode, dt.DT_DocNo, dt.DT_Sender, dt.DT_Approver, dt.DT_Location, CONVERT(VARCHAR(10), dt.DT_EntryDate, 101) AS DT_EntryDate, dt.DT_EntryNo, dt.DT_TargetURL, P.P_Position AS Sender, P2.P_Position AS Approver,
                                    ns.NS_FK_Module_id
                            FROM tblDocTracking dt LEFT OUTER JOIN
                                 tblPosition AS P2 ON dt.DT_Approver = P2.P_ID LEFT OUTER JOIN
                                 tblPosition AS P ON dt.DT_Sender = P.P_ID LEFT OUTER JOIN
                                 tblNoSeries ns ON dt.DT_FK_NSCode = ns.NS_Id 
                            WHERE (ISNUMERIC(dt.DT_FK_NSCode) = 1) AND dt.DT_Approver = '.$blnUnlimited['user-position'].' AND dt.DT_Status = \'Pending\' AND (NOT EXISTS(SELECT DT_FK_NSCode, DT_DocNo, DT_EntryNo, DT_Status 
                            FROM tblDocTracking 
                            WHERE (DT_FK_NSCode = dt.DT_FK_NSCode) AND (DT_DocNo = dt.DT_DocNo) AND (DT_Status = \'Pending\') AND (DT_EntryNo < DT_EntryNo))) ORDER BY dt.DT_EntryDate desc';

        $result = $this->db->query($query_string)->result_array();
        return $result;
    }



}