<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Department_model extends MAIN_Model {

	protected $_table = 'tblDepartmentClass';
	
	private $id = NULL;
	private $order_column = array(null,null,'DEP_Id','DEP_Description','DEP_Active');

	public function __construct() {
		parent::__construct();
	}

	// DATA TABLE STARTS HERE
	public function table_data($status = null){

		$this->db->select('DEP_Id, DEP_Description, DEP_Active');
        $this->db->from('tblDepartmentClass');

		$result_count = $this->db->count_all_results();

		$output['data']					= $this->datatables($status);
		$output['num_rows']				= $this->get_filtered_data($status);
		$output['count_all_results']	= $result_count;

		return $output; 
	}	

	public function query($status = null){
		$this->db->select('DEP_Id, DEP_Description, DEP_Active');

		// INDIVIDUAL COLUMN SEARCH

		if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
			$this->db->like('DEP_Id',trim($_POST['columns'][2]["search"]["value"]));
		}

		if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('DEP_Description',trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('CASE DEP_Active WHEN 1 THEN \'Active\' WHEN 0 THEN \'Inactive\' END',$_POST['columns'][4]["search"]["value"]);
        }

        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("DEP_Id", trim($_POST["search"]["value"]));  
                $this->db->or_like("DEP_Description", trim($_POST["search"]["value"]));  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('DEP_Id', 'DESC');  
        }
	}

	public function datatables($status = null){
		$this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblDepartmentClass');  
        return $query->result_array();
	}

	 public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblDepartmentClass');  
           return $query->num_rows();  
    }

    // END

     // DATA TABLE FOR HOLIDAY DETAIL STARTS HERE
    public function table_data_DSD($docno){

        $this->db->select('DS_FK_Department_id, DS_FK_Position_id');
        $this->db->from('tblDepartmentSetup');

        $result_count = $this->db->count_all_results();

        $output['data']                 = $this->datatables_DSD($docno);
        $output['num_rows']             = $this->get_filtered_data_DSD($docno);
        $output['count_all_results']    = $result_count;

        return $output;
    }

    public function query_DSD($docno){
        $this->db->select('DS_FK_Department_id, DS_FK_Position_id');
        //$this->db->join('tblHoliday', 'H_ID = LH_FK_Holiday_id', 'left');

        // INDIVIDUAL COLUMN SEARCH

        if(isset($_POST['columns'][0]["search"]["value"]) && !empty($_POST['columns'][0]["search"]["value"])){
            $this->db->like('DS_FK_Department_id',trim($_POST['columns'][0]["search"]["value"]));
        }

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('DS_FK_Position_id',trim($_POST['columns'][1]["search"]["value"]));
        }

        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("DS_FK_Department_id", trim($_POST["search"]["value"]));  
                $this->db->or_like("DS_FK_Position_id", trim($_POST["search"]["value"]));  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('DS_FK_Department_id', 'DESC');  
        }
    }  

    public function datatables_DSD($docno){
        $this->query_DSD($docno); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblDepartmentSetup');  
        return $query->result_array();
    }

    public function get_filtered_data_DSD($docno){  
           $this->query_DSD($docno);  
           $query = $this->db->get('tblDepartmentSetup'); 
           return $query->num_rows();  
    }

    public function status_data($docno,$action){


        foreach ($docno as $row) {
            if($action == 'active'){
                $this->db->where('DEP_Id',$row)->update('tblDepartmentClass',array('DEP_Active'=>'1'));
            }
            else{
                $this->db->where('DEP_Id',$row)->update('tblDepartmentClass',array('DEP_Active'=>'0'));
            }
        }
        if ($this->db->trans_status() === FALSE){
            
                return array('success'=>0);
        }
        else{
          
                return array('success'=>1);
        }

    }

    public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */
    
        $this->db->delete('tblDepartmentClass',array('DEP_Id'=>$docno));

        if ($this->db->trans_status() === FALSE){
                return array('success'=>0);
        }
        else{
                return array('success'=>1);
        }

    }

}
