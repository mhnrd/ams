<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Business_unit_sku_model extends MAIN_Model{

	protected $_table = 'tblBusinessUnitSKU';
	
	private $id = NULL;
	private $order_column = array(null, 'BUSD_BUS_BU_ID', 'BUSD_BUS_ID', 'BUS_SKUCode', 'BUS_Desc', 'BUS_UPC', 'BUSD_ItemCategory', 'BUSD_ItemSeason', 'BUSD_ItemGender', 'BUSD_ItemDept', 'BUSD_ItemType', 'BUSD_RegPrice', 'BUSD_MDPrice', 'BUSD_PriceOffPerc');

	public function __construct() {
		parent::__construct();
	}

	// DATA TABLE STARTS HERE
	public function table_data($status = null){

		$this->db->select('BUS_BU_ID, BUSD_BUS_ID, BUS_SKUCode, BUS_Desc, BUS_UPC, BUSD_ItemCategory, BUSD_ItemSeason, BUSD_ItemGender, BUSD_ItemDept, BUSD_ItemType, BUSD_RegPrice, BUSD_MDPrice, BUSD_PriceOffPerc');
        $this->db->from('tblBusinessUnitSKUDetail');
        $this->db->join('tblBusinessUnitSKU', 'BUSD_BUS_BU_ID = BUS_BU_ID AND BUSD_BUS_ID = BUS_ID', 'right outer');
		$result_count = $this->db->count_all_results();

		$output['data']					= $this->datatables($status);
		$output['num_rows']				= $this->get_filtered_data($status);
		$output['count_all_results']	= $result_count;

		return $output;
	}	

	public function query($status = null){
		$this->db->select('BUS_BU_ID, BUS_ID, BUS_SKUCode, BUS_Desc, BUS_UPC, BUSD_ItemCategory, BUSD_ItemSeason, BUSD_ItemGender, BUSD_ItemDept, BUSD_ItemType, BUSD_RegPrice, BUSD_MDPrice, (BUSD_PriceOffPerc * 100) AS BUSD_PriceOffPerc');
        $this->db->join('tblBusinessUnitSKU', 'BUSD_BUS_BU_ID = BUS_BU_ID AND BUSD_BUS_ID = BUS_ID', 'right outer');

		// INDIVIDUAL COLUMN SEARCH

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('BUS_BU_ID',$_POST['columns'][1]["search"]["value"]);
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('BUS_ID',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('BUS_SKUCode',$_POST['columns'][3]["search"]["value"]);
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('BUS_Desc',$_POST['columns'][4]["search"]["value"]);
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('BUS_UPC',$_POST['columns'][5]["search"]["value"]);
        }

        if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
            $this->db->like('BUSD_ItemCategory',$_POST['columns'][6]["search"]["value"]);
        }

        if(isset($_POST['columns'][7]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $this->db->like('BUSD_ItemSeason',$_POST['columns'][7]["search"]["value"]);
        }

        if(isset($_POST['columns'][8]["search"]["value"]) && !empty($_POST['columns'][8]["search"]["value"])){
            $this->db->like('BUSD_ItemSeason',$_POST['columns'][8]["search"]["value"]);
        }

        if(isset($_POST['columns'][9]["search"]["value"]) && !empty($_POST['columns'][9]["search"]["value"])){
            $this->db->like('BUSD_ItemDept',$_POST['columns'][9]["search"]["value"]);
        }

        if(isset($_POST['columns'][10]["search"]["value"]) && !empty($_POST['columns'][10]["search"]["value"])){
            $this->db->like('BUSD_ItemType',$_POST['columns'][10]["search"]["value"]);
        }

        if(isset($_POST['columns'][11]["search"]["value"]) && !empty($_POST['columns'][11]["search"]["value"])){
            $this->db->like('CAST(BUSD_RegPrice AS VARCHAR)',$_POST['columns'][11]["search"]["value"], 'after');
        }

        if(isset($_POST['columns'][12]["search"]["value"]) && !empty($_POST['columns'][12]["search"]["value"])){
            $this->db->like('CAST(BUSD_MDPrice AS VARCHAR)',$_POST['columns'][12]["search"]["value"], 'after');
        }

        if(isset($_POST['columns'][13]["search"]["value"]) && !empty($_POST['columns'][13]["search"]["value"])){
            $this->db->like('CAST((BUSD_PriceOffPerc * 100) AS VARCHAR)',$_POST['columns'][13]["search"]["value"], 'after');
        }
		
        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("BUS_BU_ID", $_POST["search"]["value"]);  
                $this->db->or_like("BUS_UPC", $_POST["search"]["value"]);  
                $this->db->or_like("BUS_ID", $_POST["search"]["value"]);  
                $this->db->or_like("BUS_SKUCode", $_POST["search"]["value"]);  
                $this->db->or_like("BUS_Desc", $_POST["search"]["value"]); 
                $this->db->or_like("BUS_UPC", $_POST["search"]["value"]); 
                $this->db->or_like("BUSD_ItemCategory", $_POST["search"]["value"]); 
                $this->db->or_like("BUSD_ItemSeason", $_POST["search"]["value"]); 
                $this->db->or_like("BUSD_ItemGender", $_POST["search"]["value"]); 
                $this->db->or_like("BUSD_ItemDept", $_POST["search"]["value"]); 
                $this->db->or_like("BUSD_ItemType", $_POST["search"]["value"]); 
                $this->db->or_like("CAST(BUSD_RegPrice as VARCHAR)", $_POST["search"]["value"], 'after'); 
                $this->db->or_like("CAST(BUSD_MDPrice as VARCHAR)", $_POST["search"]["value"], 'after'); 
                $this->db->or_like("CAST(BUSD_PriceOffPerc * 100 as VARCHAR)", $_POST["search"]["value"], 'after');
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('BUS_ID', 'DESC');  
        }
	}

	public function datatables($status = null){
		$this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblBusinessUnitSKUDetail');  
        return $query->result_array();
	}

	public function get_filtered_data(){ 
        $this->query();
        $this->db->from('tblBusinessUnitSKUDetail');
        $query = $this->db->count_all_results();  
        return $query;  

    }
	// END

    public function getBSDropdown($bu_code){
        $query_string = 'SELECT         BS_ID, BS_Description
                         FROM           tblBarcodeSticker
                         WHERE          BS_BU_ID = \''.$bu_code.'\' AND BS_Active = 1';

        return $this->db->query($query_string)->result_array();
    }

    public function getBarcodeDetailsByCategory($bu_code, $category, $price_type){

        $query_string = 'SELECT        BUSD.BUSD_ItemCategory, BUS.BUS_UPC, BUS.BUS_SKUCode, BUS.BUS_Dept, BUS.BUS_SubDept, BUS.BUS_Class, BUS.BUS_ShortDesc, BUS.BUS_VendorCode, BUS.BUS_Desc,
                                       BUS.BUS_SubClass, BUS.BUS_BuyingUOMCode, ';
        if($price_type == 'MDP'){
            $query_string .= 'BUSD.BUSD_MDPrice as Price ';
        }
        else{
            $query_string .= 'BUSD.BUSD_RegPrice as Price ';
        }
        
        $query_string .= 'FROM          tblBusinessUnitSKUDetail AS BUSD LEFT OUTER JOIN
                                        tblBusinessUnitSKU AS BUS ON BUSD.BUSD_BUS_ID = BUS.BUS_ID AND BUSD.BUSD_BUS_BU_ID = BUS.BUS_BU_ID
                         WHERE          (BUSD.BUSD_ItemCategory = \''.$category.'\') AND (BUSD.BUSD_BUS_BU_ID = \''.$bu_code.'\')
                                        AND (BUS.BUS_Status = 1)';

        return $this->db->query($query_string)->row_array();

    }

    public function getPrintOutURL($bu_id){

        $query_string = 'SELECT         BS_PrintOutURL
                         FROM           tblBarcodeSticker
                         WHERE          BS_ID = \''.$bu_id.'\'';

        return $this->db->query($query_string)->row_array()['BS_PrintOutURL'];

    }

    public function getAllBUCode(){
        $query_string = 'SELECT     BU_ID, BU_Description
                         FROM       tblBusinessUnit
                         WHERE      BU_Status = 1';

        return  $this->db->query($query_string)->result_array();
    }


	public function generateBUS_ID($bu_code){
		$query_string = 'SELECT 	BUS_BU_ID
						 FROM 		tblBusinessUnitSKU
						 WHERE 		BUS_BU_ID = \''.$bu_code.'\'';

		$current_docno = $this->db->query($query_string)->num_rows() + 1;

		return $bu_code.'-'.$this->padLeftZero(10, $current_docno);

	}

	private function padLeftZero($num, $txt){

        $pcnt = $num - strlen($txt);
        $output = '';

        for ($i=0; $i < $pcnt; $i++) { 
            $output .= '0';
        }

        $output = $output.$txt;

        return $output;

    }

    public function getActiveUOM(){
        // For future use. Do not delete
        // $query_string = 'SELECT     AD_Code, AD_Desc
        //                  FROM       tblAttributeDetail
        //                  WHERE      AD_FK_Code = 102301 AND AD_Active = 1';

        $query_string = 'SELECT     AD_Code, AD_Desc
                         FROM       Retailflex.dbo.tblAttributeDetail
                         WHERE      AD_FK_Code = 1001 AND AD_Active = 1';

        return $this->db->query($query_string)->result_array();
    }

    public function getPricesByRow($data){

        $query_string = 'SELECT         BUSD_MDPrice, BUSD_RegPrice, BUSD_PriceOffPerc
                         FROM           tblBusinessUnitSKUDetail
                         WHERE          BUSD_BUS_BU_ID = \''.$data['bu_code'].'\' AND
                                        BUSD_BUS_ID = \''.$data['docno'].'\' AND
                                        BUSD_ItemCategory = \''.$data['category'].'\' AND
                                        BUSD_ItemSeason = \''.$data['season'].'\' AND
                                        BUSD_ItemGender = \''.$data['gender'].'\' AND
                                        BUSD_ItemDept = \''.$data['dept'].'\' AND
                                        BUSD_ItemType = \''.$data['type'].'\' AND
                                        BUSD_RegPrice = '.$data['prc_point'];

        return $this->db->query($query_string)->row_array();

    }

    public function getSKUDetailList($code, $table, $status){

        $query_string = 'SELECT     '.$code.' 
                         FROM       '.$table;

        if($status != ''){
            $query_string .= ' WHERE '.$status.' = 1';
        }
        else{
            $query_string .= ' WHERE IM.IM_UnitPrice > 0';   
        }

        return $this->db->query($query_string)->result_array();

    }

}