<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Business_unit_sku_detail_model extends MAIN_Model{

	protected $_table = 'tblBusinessUnitSKUDetail';
	
	private $id = NULL;


	public function __construct() {
		parent::__construct();
	}

	// DATA TABLE STARTS HERE
	public function table_data($docno, $type){

		$this->db->select('BUSD_ItemCategory, BUSD_ItemSeason, BUSD_ItemGender, BUSD_ItemDept, BUSD_ItemType, BUSD_MDPrice, BUSD_RegPrice, BUSD_PriceOffPerc, BUSD_PricePoint');
      $this->db->from('tblBusinessUnitSKUDetail');
      $this->db->where('BUSD_BUS_ID', $docno);
		$result_count = $this->db->count_all_results();

		$output['data']					= $this->datatables($docno, $type);
		$output['num_rows']				= $this->get_filtered_data($docno, $type);
		$output['count_all_results']	= $result_count;

		return $output;
	}	

	public function query($docno, $type){
		$this->db->select('BUSD_ItemCategory, BUSD_ItemSeason, BUSD_ItemGender, BUSD_ItemDept, BUSD_ItemType, BUSD_MDPrice, BUSD_RegPrice, BUSD_PriceOffPerc, BUSD_PricePoint');
    $this->db->where('BUSD_BUS_ID', $docno);

		// INDIVIDUAL COLUMN SEARCH
		
    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("BUSD_ItemCategory", $_POST["search"]["value"]);  
            $this->db->or_like("BUSD_ItemSeason", $_POST["search"]["value"]);  
            $this->db->or_like("BUSD_ItemGender", $_POST["search"]["value"]);  
            $this->db->or_like("BUSD_ItemDept", $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){ 

        $order_column = '';

        if($type == 'update'){
          $order_column = array(null, 'BUSD_ItemCategory', 'BUSD_ItemSeason', 'BUSD_ItemGender', 'BUSD_ItemDept', 'BUSD_ItemType' , 'BUSD_MDPrice', 'BUSD_RegPrice', 'BUSD_PriceOffPerc');

        }
        else{
          $order_column = array('BUSD_ItemCategory', 'BUSD_ItemSeason', 'BUSD_ItemGender', 'BUSD_ItemDept', 'BUSD_ItemType' , 'BUSD_MDPrice', 
            'BUSD_RegPrice', 'BUSD_PriceOffPerc');
        }

        $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('BUSD_ItemType', 'ASC');  
    }
	}

	public function datatables($docno, $type){
		$this->query($docno, $type); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblBusinessUnitSKUDetail');  
        return $query->result_array();
	}

	public function get_filtered_data($docno, $type){ 
        $this->query($docno, $type);
        $this->db->from('tblBusinessUnitSKUDetail');
        $query = $this->db->count_all_results();  
        return $query;  

    }
	// END

    public function getList($filter){
        $query_string = 'SELECT     '.$filter['filter-cols'].' 
                         FROM '.$filter['filter-table'].' 
                         WHERE '.$filter['filter-like'][0].' LIKE \'%'.$filter['filter-input'].'%\' '.(isset($filter['filter-like'][1]) ? ' OR '.$filter['filter-like'][1].' LIKE \'%'.$filter['filter-input'].'%\'' : '');

        return $this->db->query($query_string)->result_array();
    }

    public function generateSKUDetailandSubDetail($data){

        // if(!isset($data['typ_arr'])){
        //   $query_typ = 'SELECT      FK_ProdType
        //                 FROM        tblCategory
        //                 WHERE       (Category_Code IN (\''.implode('\', \'', $data['cat_arr']).'\')) AND 
        //                             (PS_Code IN (\''.implode('\', \'', $data['sea_arr']).'\')) AND 
        //                             (PG_Code IN (\''.implode('\', \'', $data['gen_arr']).'\')) AND 
        //                             (PDT_Code IN (\''.implode('\', \'', $data['dept_arr']).'\'))';

        //   $data['typ_arr'] = $this->db->query($query_typ)->result_array();
        // }

        // Insert Detail query
        if(isset($data['cat_arr'])){
          $query_BUSD = 'INSERT INTO          tblBusinessUnitSKUDetail(BUSD_BUS_BU_ID, BUSD_BUS_ID, BUSD_ItemCategory, 
                                                                      BUSD_ItemSeason, BUSD_ItemGender, BUSD_ItemDept, BUSD_ItemType, 
                                                                      BUSD_MDPrice, BUSD_RegPrice, CreatedBy, DateCreated)
                         SELECT DISTINCT       \''.$data['code'].'\' as BUSD_BUS_BU_ID, \''.$data['docno'].'\' as BUSD_BUS_ID, 
                                              RTRIM(Category_Code) as BUSD_ItemCategory, RTRIM(C.FK_ProdSeason) as BUSD_ItemSeason, 
                                              RTRIM(C.FK_ProdGender) as BUSD_ItemGender, RTRIM(C.FK_ProdDept) as BUSD_ItemDept,  
                                              RTRIM(C.FK_ProdType) as BUSD_ItemType, \''.$data['md_price'].'\' as BUSD_MDPrice, 
                                              ISNULL(IM_UnitPrice, 0) as BUSD_RegPrice, \''.getCurrentUser()['login-user'].'\' as CreatedBy, GETDATE() as DateCreated
                         FROM                 Retailflex.dbo.tblCategory as C LEFT OUTER JOIN 
                                              Retailflex.dbo.tblInvMaster as IM ON Category_Code = FK_Category_Code
                         WHERE                Category_Code IN (\''.implode('\', \'', $data['cat_arr']).'\')
                                              AND Category_Code NOT IN (SELECT BUSD_ItemCategory
                                                                        FROM    tblBusinessUnitSKUDetail
                                                                        WHERE   BUSD_BUS_BU_ID = \''.$data['code'].'\' AND
                                                                                BUSD_ItemCategory IN (\''.implode('\', \'', $data['cat_arr']).'\'))';
        }
        else{
          $query_BUSD = 'INSERT INTO          tblBusinessUnitSKUDetail(BUSD_BUS_BU_ID, BUSD_BUS_ID, BUSD_ItemCategory, 
                                                                      BUSD_ItemSeason, BUSD_ItemGender, BUSD_ItemDept, BUSD_ItemType, 
                                                                      BUSD_MDPrice, BUSD_RegPrice, CreatedBy, DateCreated)
                         SELECT DISTINCT       \''.$data['code'].'\' as BUSD_BUS_BU_ID, \''.$data['docno'].'\' as BUSD_BUS_ID, 
                                              RTRIM(Category_Code) as BUSD_ItemCategory, RTRIM(C.FK_ProdSeason) as BUSD_ItemSeason, 
                                              RTRIM(C.FK_ProdGender) as BUSD_ItemGender, RTRIM(C.FK_ProdDept) as BUSD_ItemDept,  
                                              RTRIM(C.FK_ProdType) as BUSD_ItemType, \''.$data['md_price'].'\' as BUSD_MDPrice, 
                                              ISNULL(IM_UnitPrice, 0) as BUSD_RegPrice, \''.getCurrentUser()['login-user'].'\' as CreatedBy, GETDATE() as DateCreated
                         FROM                 Retailflex.dbo.tblCategory as C LEFT OUTER JOIN 
                                              Retailflex.dbo.tblInvMaster as IM ON Category_Code = FK_Category_Code
                         WHERE                IM_UnitPrice IN (\''.implode('\', \'', $data['prc_point']).'\') AND
                                              C.FK_ProdSeason IN (\''.implode('\', \'', $data['sea_arr']).'\') AND
                                              C.FK_ProdGender IN (\''.implode('\', \'', $data['gen_arr']).'\') AND
                                              C.FK_ProdType IN (\''.implode('\', \'', $data['typ_arr']).'\') AND
                                              C.FK_ProdDept IN (\''.implode('\', \'', $data['dept_arr']).'\') AND
                                              Category_Code NOT IN (SELECT BUSD_ItemCategory 
                                                                    FROM tblBusinessUnitSKUDetail
                                                                    WHERE BUSD_BUS_BU_ID = \''.$data['code'].'\' AND
                                                                          BUSD_RegPrice IN (\''.implode('\', \'', $data['prc_point']).'\') AND
                                                                          BUSD_ItemSeason IN (\''.implode('\', \'', $data['sea_arr']).'\') AND
                                                                          BUSD_ItemGender IN (\''.implode('\', \'', $data['gen_arr']).'\') AND
                                                                          BUSD_ItemDept IN (\''.implode('\', \'', $data['dept_arr']).'\') AND
                                                                          BUSD_ItemType IN (\''.implode('\', \'', $data['typ_arr']).'\') )';
        }

        $this->db->query($query_BUSD);

        if(!isset($data['cat_arr'])){

          $query_CAT = 'SELECT DISTINCT   Category_Code
                        FROM              Retailflex.dbo.tblCategory as C LEFT OUTER JOIN
                                          Retailflex.dbo.tblInvMaster as IM ON Category_Code = FK_Category_Code
                        WHERE             IM_UnitPrice IN (\''.implode('\', \'', $data['prc_point']).'\') AND
                                          C.FK_ProdSeason IN (\''.implode('\', \'', $data['sea_arr']).'\') AND
                                          C.FK_ProdGender IN (\''.implode('\', \'', $data['gen_arr']).'\') AND
                                          C.FK_ProdType IN (\''.implode('\', \'', $data['typ_arr']).'\') AND
                                          C.FK_ProdDept IN (\''.implode('\', \'', $data['dept_arr']).'\')';

          $data['cat_arr'] = array_column($this->db->query($query_CAT)->result_array(), 'Category_Code');

        }

        $query_BUSSD =  'INSERT INTO tblBusinessUnitSKUSubDetail(BUSSD_BU_ID, BUSSD_BUS_ID, BUSSD_ItemCategory, 
                                     BUSSD_ItemCode, BUSSD_MDPrice, BUSSD_RegPrice, CreatedBy, DateCreated)
                         SELECT      DISTINCT \''.$data['code'].'\' as BUSSD_BU_ID, \''.$data['docno'].'\' as BUSSD_BUS_ID, 
                                     RTRIM(Category_Code) as BUSSD_ItemCategory, RTRIM(IM_Item_Code) as BUSSD_ItemCode, 
                                      \''.$data['md_price'].'\' as BUSSD_MDPrice, IM_UnitPrice as BUSSD_RegPrice, \''.getCurrentUser()['login-user'].'\' as CreatedBy, 
                                     GETDATE() as DateCreated
                         FROM        Retailflex.dbo.tblCategory RIGHT OUTER JOIN
                                     Retailflex.dbo.tblInvMaster ON Category_Code = FK_Category_Code
                         WHERE       '.(isset($data['cat_arr']) ? '(FK_Category_Code IN (\''.implode('\', \'', $data['cat_arr']).'\')) ' : 
                                     '(IM_UnitPrice IN (\''.implode('\', \'', $data['prc_point']).'\')) ').' 
                                     '.(isset($data['siz_arr']) ? ' AND (FK_Size_Code IN (\''.implode('\', \'', $data['siz_arr']).'\'))' : ' ').'
                                     AND IM_Item_Code NOT IN (SELECT BUSSD_ItemCode FROM tblBusinessUnitSKUSubDetail WHERE BUSSD_BU_ID = \''.$data['code'].'\')';

        // $this->print_r($query_BUSSD);
        // die();

        $this->db->query($query_BUSSD);

    }

    public function checkIfRowExist($data){
        $query_string = 'SELECT     BUSD_BUS_BU_ID, BUSD_BUS_ID, BUSD_ItemCategory, BUSD_ItemSeason, BUSD_ItemGender, BUSD_ItemDept, 
                                    BUSD_ItemType, BUSD_MDPrice, BUSD_RegPrice, BUSD_PriceOffPerc
                         FROM       tblBusinessUnitSKUDetail
                         WHERE      BUSD_BUS_ID = \''.$data['docno'].'\' AND 
                                    BUSD_BUS_BU_ID = \''.$data['bu_code'].'\' AND
                                    BUSD_ItemCategory = \''.$data['category'].'\' AND
                                    BUSD_ItemSeason = \''.$data['season'].'\' AND
                                    BUSD_ItemGender = \''.$data['gender'].'\' AND
                                    BUSD_ItemDept = \''.$data['dept'].'\' AND
                                    BUSD_ItemType = \''.$data['type'].'\'';

        return $this->db->query($query_string)->row_array();
    }

    public function getCategoryDetails($field, $category){
      $query_string = 'SELECT   '.$field.'
                       FROM     Retailflex.dbo.tblCategory
                       WHERE    Category_Code IN (\''.implode('\', \'', $category).'\')';

      return $this->db->query($query_string)->result_array();
    }

    public function countDetails($docno, $bu_code, $category){

      $query_string = 'SELECT     BUSD_ItemCategory
                       FROM       tblBusinessUnitSKUDetail
                       WHERE      BUSD_BUS_BU_ID = \''.$bu_code.'\' AND BUSD_BUS_ID = \''.$docno.'\' AND BUSD_ItemCategory = \''.$category.'\'';

      return $this->db->query($query_string)->num_rows();

    }

    public function seasonList($category){
      $query_string = 'SELECT     DISTINCT FK_ProdSeason
                       FROM       Retailflex.dbo.tblCategory
                       WHERE      Category_Code IN (\''.implode('\', \'', $category).'\') AND FK_ProdSeason IS NOT NULL';

        return $this->db->query($query_string)->result_array();
    }

    public function sizeList($data){

      $subquery_cat = 'SELECT DISTINCT   Category_Code
                        FROM    Retailflex.dbo.tblCategory as C 
                        WHERE   Category_Code IN (\''.implode('\', \'', $data['category']).'\')
                                '.(isset($data['sea_arr']) ? ' AND C.FK_ProdSeason IN (\''.implode('\', \'', $data['sea_arr']).'\') ' : '' ).(isset($data['gen_arr']) ? ' AND C.FK_ProdGender IN (\''.implode('\', \'', $data['gen_arr']).'\') ' : '' ).(isset($data['dept_arr']) ? ' AND C.FK_ProdDept IN (\''.implode('\', \'', $data['dept_arr']).'\') ' : '' ).(isset($data['typ_arr']) ? ' AND C.FK_ProdType IN (\''.implode('\', \'', $data['typ_arr']).'\') ' : '' );

      $query_string = 'SELECT     DISTINCT FK_Size_Code
                       FROM       Retailflex.dbo.tblInvMaster 
                       WHERE      FK_Category_Code IN ('.$subquery_cat.')';

      return $this->db->query($query_string)->result_array();
    }

    public function genderList($category, $season){
      $query_string = 'SELECT     DISTINCT FK_ProdGender
                       FROM       Retailflex.dbo.tblCategory
                       WHERE      Category_Code IN (\''.implode('\', \'', $category).'\') AND FK_ProdSeason IN (\''.implode('\', \'', $season).'\')';

        return $this->db->query($query_string)->result_array();
    }

    public function deptList($category, $season, $gender){
      $query_string = 'SELECT     DISTINCT FK_ProdDept
                       FROM       Retailflex.dbo.tblCategory
                       WHERE      Category_Code IN (\''.implode('\', \'', $category).'\') AND FK_ProdSeason IN (\''.implode('\', \'', $season).'\') AND FK_ProdGender IN (\''.implode('\', \'', $gender).'\')';

        return $this->db->query($query_string)->result_array();
    }

    public function typeList($category, $season, $gender, $dept){
      $query_string = 'SELECT     DISTINCT FK_ProdType
                       FROM       Retailflex.dbo.tblCategory
                       WHERE      Category_Code IN (\''.implode('\', \'', $category).'\') AND FK_ProdSeason IN (\''.implode('\', \'', $season).'\') AND FK_ProdGender IN (\''.implode('\', \'', $gender).'\') AND FK_ProdDept IN (\''.implode('\', \'', $dept).'\')';

        return $this->db->query($query_string)->result_array();
    }

    public function getCategoriesByPricePoint($price_point){
      $query_string = 'SELECT     DISTINCT FK_Category_Code
                       FROM       Retailflex.dbo.tblInvMaster
                       WHERE      IM_UnitPrice IN (\''.implode('\', \'', $price_point).'\')';

      return array_column($this->db->query($query_string)->result_array(), 'FK_Category_Code');
    }

}
