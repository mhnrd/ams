<?php 

class Business_unit_sku_sub_detail_model extends MAIN_Model
{
	
	protected $_table = 'tblBusinessUnitSKUSubDetail';
	
	private $id = NULL;
	private $order_column = array('BUSSD_ItemCategory', 'BUSSD_ItemCode', 'BUSSD_MDPrice', 'BUSSD_RegPrice', 'BUSSD_PriceOffPerc');

	public function __construct() {
		parent::__construct();
	}

	// DATA TABLE STARTS HERE
	public function table_data($docno){

		$this->db->select('BUSSD_ItemCategory, BUSSD_ItemCode, BUSSD_MDPrice, BUSSD_RegPrice, BUSSD_PriceOffPerc');
        $this->db->from('tblBusinessUnitSKUSubDetail');
        $this->db->where('BUSSD_BUS_ID', $docno);
		$result_count = $this->db->count_all_results();

		$output['data']					= $this->datatables($docno);
		$output['num_rows']				= $this->get_filtered_data($docno);
		$output['count_all_results']	= $result_count;

		return $output;
	}	

	public function query($docno){
		$this->db->select('BUSSD_ItemCategory, BUSSD_ItemCode, BUSSD_MDPrice, BUSSD_RegPrice, BUSSD_PriceOffPerc');
        $this->db->where('BUSSD_BUS_ID', $docno);

		// INDIVIDUAL COLUMN SEARCH
		
        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->group_start();
                $this->db->like("BUSSD_ItemCategory", $_POST["search"]["value"]);  
                $this->db->or_like("BUSSD_ItemCode", $_POST["search"]["value"]); 
                $this->db->group_end();   
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('BUSSD_BU_ID', 'DESC');  
        }
	}

	public function datatables($docno){
		$this->query($docno); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblBusinessUnitSKUSubDetail');  
        return $query->result_array();
	}

	public function get_filtered_data($docno){ 
        $this->query($docno);
        $this->db->from('tblBusinessUnitSKUSubDetail');
        $query = $this->db->count_all_results();  
        return $query;  

    }
	// END

	public function getPricesByRow($data){

        $query_string = 'SELECT         BUSSD_MDPrice, BUSSD_RegPrice, BUSSD_PriceOffPerc
                         FROM           tblBusinessUnitSKUSubDetail
                         WHERE          BUSSD_BU_ID = \''.$data['bu_code'].'\' AND
                                        BUSSD_BUS_ID = \''.$data['docno'].'\' AND
                                        BUSSD_ItemCategory = \''.$data['category'].'\' AND
                                        BUSSD_ItemCode = \''.$data['item'].'\'';

        return $this->db->query($query_string)->row_array();

    }
}