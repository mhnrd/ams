<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Company_model extends MAIN_Model {

    protected $_table = 'tblCompany';

    private $id = NULL;
    private $order_column = array(null,null,'COM_Id','COM_Name','COM_Address','COM_PhoneNo','COM_FaxNum','COM_Email', 'COM_Active');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data($status = null){

        $this->db->select('COM_Id, COM_Name, COM_Address, COM_PhoneNo, COM_FaxNum, COM_Email, COM_Active');  
        $this->db->from('tblCompany');
        $result_count = $this->db->count_all_results();

        $output['data']              = $this->datatables($status);
        $output['num_rows']          = $this->get_filtered_data($status);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query($status = null){
        $this->db->select('COM_Id, COM_Name, COM_Address, COM_PhoneNo, COM_FaxNum, COM_Email, CASE COM_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as COM_Active', false);

        // Individual Column Search

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('COM_Id',trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('COM_Name',trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('COM_Address',trim($_POST['columns'][4]["search"]["value"]));
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('COM_PhoneNo',trim($_POST['columns'][5]["search"]["value"]));
        }

        if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
            $this->db->like('COM_FaxNum',trim($_POST['columns'][6]["search"]["value"]));
        }

        if(isset($_POST['columns'][7]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $this->db->like('COM_Email',trim($_POST['columns'][7]["search"]["value"]));
        }

        if(isset($_POST['columns'][8]["search"]["value"]) && !empty($_POST['columns'][8]["search"]["value"])){
            $this->db->like('CASE COM_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END',($_POST['columns'][8]["search"]["value"]), false);
        }

        // End

        if(isset($_POST["search"]["value"])){
            if($_POST["search"]["value"] != ''){
                $this->db->group_start();
                $this->db->like("COM_Id", trim($_POST["search"]["value"]));  
                $this->db->or_like("COM_Name", trim($_POST["search"]["value"]));  
                $this->db->or_like("COM_Address", trim($_POST["search"]["value"]));
                $this->db->or_like("COM_PhoneNo", trim($_POST["search"]["value"]));
                $this->db->or_like("COM_FaxNum", trim($_POST["search"]["value"]));  
                $this->db->or_like("COM_Email", trim($_POST["search"]["value"]));
                $this->db->or_like("CASE COM_Active WHEN 1 THEN 'Active' ELSE 'Inactive' END", trim($_POST["search"]["value"]),false);
                $this->db->group_end();
            }  
        }  
        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir'], false);  
        }  
        else{  
            $this->db->order_by('COM_Id', 'DESC');  
        }
    }

    public function datatables($status = null){
        $this->query($status); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblCompany');  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblCompany');  
           return $query->num_rows();  
    }
    // DATA TABLE QUERY ENDS HERE 
   
      public function getAll($docno = null){
       $query_string = 'SELECT *
                        FROM tblCompany';
            
        if(!empty($docno)){
            $query_string .= ' WHERE ' . $this->sql_hash('COM_Id') . ' = \''.$docno.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();
        }
        return $result;
    }

    public function getActiveCurrency(){
        $query_string = 'SELECT AD_Desc, AD_FK_Code, AD_Id
                        FROM tblAttributeDetail as AD 
                        WHERE AD_Active = \'1\' AND  AD_FK_Code = \'102203\' ';

        return $this->db->query($query_string)->result_array();
    }
}   