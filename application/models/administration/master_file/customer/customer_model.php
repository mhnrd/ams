<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer_model extends MAIN_Model {

    protected $_table = 'tblCustomer';

    private $id = NULL;
    private $order_column = array(null,null,'C_DocNo','C_Name','C_CompanyName', 'C_Active');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data(){
        $this->db->select('C_DocNo, C_Name, C_CompanyName, C_Active'); 
        $this->db->from('tblCustomer');
         
        $result_count = $this->db->count_all_results();

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('C_DocNo, C_Name, C_CompanyName, CASE C_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as C_Active', false);

        // Individual Column Search

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('C_DocNo',trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('C_Name',trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            // $this->print_r('test');
            $this->db->like('C_CompanyName',trim($_POST['columns'][4]["search"]["value"]));
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like("(CASE C_Active WHEN 1 THEN 'Active' ELSE 'Inactive' END) ",$_POST['columns'][5]["search"]["value"]);
        }

        // End

        if(isset($_POST["search"]["value"])){
            if($_POST["search"]["value"] != ''){
                $this->db->like("C_DocNo", trim($_POST["search"]["value"]));  
                $this->db->or_like("C_Name", trim($_POST["search"]["value"]));  
                $this->db->or_like("C_CompanyName", trim($_POST["search"]["value"]));  
                $this->db->or_like("(CASE C_Active WHEN 1 THEN 'Active' ELSE 'Inactive' END)", trim($_POST["search"]["value"]));  
            }  
        }  
        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('C_DocNo', 'DESC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblCustomer');  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblCustomer');  
           return $query->num_rows();  
    }
      // END 

     public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */
    
        
        $this->db->delete('tblCustomer',array('C_DocNo'=>$docno));

        if ($this->db->trans_status() === FALSE){
             
                return array('success'=>0);
        }
        else{
               
                return array('success'=>1);
        }

    }

     // DATA TABLE QUERY ENDS HERE 

     public function getAll($docno = null){
       $query_string = 'SELECT *
                        FROM tblCustomer';
                 
         if(!empty($docno)){
            $query_string .= ' WHERE ' . $this->sql_hash('C_DocNo') . ' = \''.$docno.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();
        }
        return $result;
    }

    public function status_data($docno,$action){
        foreach ($docno as $row) {
            if($action == 'active'){
                $this->db->where('C_DocNo',$row)->update('tblCustomer',array('C_Active'=>'1'));
            }
            else{
                $this->db->where('C_DocNo',$row)->update('tblCustomer',array('C_Active'=>'0'));
            }
        }
        if ($this->db->trans_status() === FALSE){
            
                return array('success'=>0);
        }
        else{
          
                return array('success'=>1);
        }

    }

    public function getActiveCPG(){
        $query_string = 'SELECT CPG_Code
                        FROM tblCustomerPostingGroup as CPG
                        WHERE CPG_Active = \'1\'';

        return $this->db->query($query_string)->result_array();
    }

    public function getActiveWHT(){
        $query_string = 'SELECT WBPG_Code
                        FROM tblWHTBusPostingGroup as WHT
                        WHERE WBPG_Active = \'1\'';

        return $this->db->query($query_string)->result_array();
    }

    public function getActiveVATPostingGroup(){
        $query_string = 'SELECT VBPG_Code
                        FROM tblVATBusPostingGroup as VPG
                        WHERE VBPG_Active = \'1\'';

        return $this->db->query($query_string)->result_array();
    }

    public function getActivePaymentTerms(){
        $query_string = 'SELECT PT_id, PT_Desc
                        FROM tblPaymentTerms as PT
                        WHERE PT_Active = \'1\'';

        return $this->db->query($query_string)->result_array();
    }

    public function getActiveCustomerType(){
        $query_string = 'SELECT CT_Id, CT_Description
                        FROM tblCustomerType as CT
                        WHERE CT_Active = \'1\'';

        return $this->db->query($query_string)->result_array();
    }

     public function getActiveCurrency(){
        $query_string = 'SELECT AD_Desc, AD_FK_Code, AD_Id
                        FROM tblAttributeDetail as AD 
                        WHERE AD_Active = \'1\' AND  AD_FK_Code = \'102203\' ';

        return $this->db->query($query_string)->result_array();
    }

    public function getActiveBillTo(){
        $query_string = 'SELECT C_DocNo, C_Name
                        FROM tblCustomer as C 
                        WHERE C_Active = \'1\'';

        return $this->db->query($query_string)->result_array();
    }


}   