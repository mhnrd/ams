<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supplier_model extends MAIN_Model {

    protected $_table = 'tblSupplier';

    private $id = NULL;
    private $order_column = array(null,'S_DocNo','S_Name','S_Address','S_TelNum','S_TinNum', 'S_Active');

    public function __construct() {
        parent::__construct();
    }

  

    /* datatable query starts here */
    public function table_data($status = null){

        
        $this->db->select('S_DocNo, S_Name, S_Address, S_TelNum, S_TinNum, S_Active');
        //$this->db->from('tblSupplier'); 
        $result_count = $this->db->count_all_results(); 

        $result_item  =  $this->datatables();
       foreach ($result_item as $key => $value) {
            
        }
        if($status != ''){
            $this->db->where('S_Status',$status);  
        }

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('S_DocNo, S_Name, S_Address, S_TelNum, S_TinNum, S_Active');

        // Individual Column Search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('S_DocNo',trim($_POST['columns'][1]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('S_Name', trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('S_Address', trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('S_TelNum', trim($_POST['columns'][4]["search"]["value"]));
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('S_TinNum', trim($_POST['columns'][5]["search"]["value"]));
        }

        if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
            $this->db->like('S_Active', trim($_POST['columns'][6]["search"]["value"]));
        }

        // End
        
        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){
                $this->db->like("S_DocNo", trim($_POST["search"]["value"]));
                $this->db->or_like("S_Name", trim($_POST["search"]["value"])); 
                $this->db->or_like("S_Address", trim($_POST["search"]["value"]));
                $this->db->or_like("S_TelNum", trim($_POST["search"]["value"]));
                $this->db->or_like("S_TinNum", trim($_POST["search"]["value"]));         
            }
        }

        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('S_DocNo', 'DESC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblSupplier');  
        return $query->result_array();
    }
    public function getAll($docno = null){
       $query_string = 'SELECT S.S_DocNo, S.S_Name, S.S_BalanceAsOf, S.S_Address, S.S_Addr_City, S.S_Addr_PostalCode, S.S_Addr_Region, S.S_Country, S.S_TelNum, S.S_FaxNum, S.S_EmailAdd1, S.S_EmailAdd2, S.S_PrintCheckAs, 
                         S.S_BankAccountNo, S.S_FK_PayTerms, S.S_CreditLimit, S.S_TinNum, S.S_Contact, S.S_SupplierPostingGroup, S.S_WHT_PostingGroup, S.S_Vat_PostingGroup, S.S_Active, S.S_BankName, S.S_BankAddress, 
                         S.S_SwiftCode, S.S_FK_Attribute_Currency_id, S.S_SupplierType, S.S_Id, S.S_Status, S.CreatedBy, S.ModifiedBy, S.DateCreated, S.DateModified
         FROM            tblSupplier AS S LEFT OUTER JOIN
                         tblPaymentTerms AS PT ON S.S_FK_PayTerms = PT.PT_id LEFT OUTER JOIN
                         tblSupplierPostingGroup AS SPG ON S.S_SupplierPostingGroup = SPG.SPG_Code LEFT OUTER JOIN
                         tblWHTPostingGroupSetup AS WPS ON S.S_WHT_PostingGroup = WPS.WPS_WPPG_FK_Code LEFT OUTER JOIN
                         tblVATPostingSetup AS VPS ON S.S_Vat_PostingGroup = VPS.VPS_VBPG_FK_Code';
        if(!empty($docno)){
            $query_string .= ' WHERE ' . $this->sql_hash('S_DocNo') . ' = \''.$docno.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();
        }
        return $result;
    }

    public function getAllActive(){
       $query_string = 'SELECT *
                        FROM tblSupplier
                        WHERE [S_Status] = \'Active\'';
        $params = array();
        $result = $this->db->query($query_string, $params);
        return $result->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblSupplier');  
           return $query->num_rows();  
    }
    public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */
    
       // $this->db->trans_begin();
        $this->db->delete('tblSupplier',array('S_DocNo'=>$docno));
        
        if ($this->db->trans_status() === FALSE){
                //$this->db->trans_rollback();
                return array('success'=>0);
        }
        else{
                //$this->db->trans_commit();
                return array('success'=>1);
        }

    }
    public function status_data($docno,$action){

        //$this->db->trans_begin();
        foreach ($docno as $row) {
            if($action == 'active'){
                $this->db->where('S_DocNo',$row)->update('tblSupplier',array('S_Active'=>'1'));
            }
            else{
                $this->db->where('S_DocNo',$row)->update('tblSupplier',array('S_Active'=>'0'));
            }
        }
        if ($this->db->trans_status() === FALSE){
                //$this->db->trans_rollback();
                return array('success'=>0);
        }
        else{
                //$this->db->trans_commit();
                return array('success'=>1);
        }

    }
     public function getActiveSupplierPostingGroup(){
        $query_string = 'SELECT SPG_CODE
                         FROM tblSupplierPostingGroup as SPG
                         WHERE SPG_Active = \'1\' ';
        return $this->db->query($query_string)->result_array();
    }

    public function getActiveWhtPostingGroup(){
        $query_string = 'SELECT WBPG_Code
                         FROM tblWHTBusPostingGroup as WBPG
                         WHERE WBPG_Active = \'1\' ';
        return $this->db->query($query_string)->result_array();
    }

    public function getActiveVatPostingGroup(){
        $query_string = 'SELECT VBPG_Code
                         FROM tblVATBusPostingGroup as VBPG
                         WHERE VBPG_Active = \'1\' ';
        return $this->db->query($query_string)->result_array();
    }

    public function getActiveFkPaymentTerms(){
        $query_string = 'SELECT PT_id
                         FROM tblPaymentTerms as PT
                         WHERE PT_Active = \'1\' ';
        return $this->db->query($query_string)->result_array();
    }
    public function getActiveCurrency(){
        $query_string = 'SELECT AD_Code,AD_FK_Code, AD_Desc, AD_Id
                         FROM tblAttributeDetail as AD
                         WHERE AD_Active = \'1\' AND AD_FK_Code = \'1302\' ';
        return $this->db->query($query_string)->result_array();   
    }
    public function getSupplierType(){
        $query_string = 'SELECT AD_Code, AD_FK_Code, AD_Desc,AD_Id
                         FROM tblAttributeDetail as AD 
                         WHERE AD_Active = \'1\' AND AD_FK_Code =\'1305\' ';
        return $this->db->query($query_string)->result_array();
    }

    /* datatable query ends here */ 

   /* public function checkAllBOMSetup($itemNo){
        $blnValidate = true;    

        $where = array(
            'BOM_ItemNo' => $itemNo,
            'BOM_Status' => 'Active',
        );

        $result = $this->db->where($where)->get($this->_table)->num_rows();
        if($result == 0){
            $blnValidate = false;
        }


        return $blnValidate;
    }*/

    /*public function getBOMHeader($bomItemNo_List){
        $itemNo = '(\''.implode('\',\'', array_column($bomItemNo_List,'item-no')).'\')';
        $sql_query = 'SELECT        BOM.BOM_DocNo, BOM.BOM_BOMDescription, BOM.BOM_ItemNo, BOM.BOM_Version, BOM.BOM_YieldQty, BOM.BOM_YieldUomID, 
                                    BOM.BOM_BatchQty, BOM.BOM_BatchUomID, I.IM_FK_Attribute_UOM_id, ISNULL(IUC.IUC_Quantity, 0) AS BaseUOMQty
                     FROM            tblBOM AS BOM LEFT OUTER JOIN
                                     tblItemUOMConv AS IUC ON BOM.BOM_ItemNo = IUC.IUC_FK_Item_id AND BOM.BOM_YieldUomID = IUC.IUC_FK_UOM_id LEFT OUTER JOIN
                                     tblItem AS I ON BOM.BOM_ItemNo = I.IM_Item_Id
                     WHERE (BOM.BOM_ItemNo IN '.$itemNo.') AND (BOM.BOM_Status = \'Active\')
                     ORDER BY BOM.BOM_BOMDescription';
                     
        $result = $this->db->query($sql_query)->result_array();

        return $result;
    }


    public function getBOMDetail($bomNo){
        $sql_query = 'SELECT BOMD.BOMD_BOM_DocNo, BOMD.BOMD_LineNo, BOMD.BOMD_ItemNo, BOMD.BOMD_Barcode, BOMD.BOMD_Qty, BOMD.BOMD_UomID, 
                             BOMD.BOMD_AverageCost, BOMD.BOMD_UnitCost, BOMD.BOMD_TotalCost, BOMD.BOMD_Waste, BOMD.BOMD_RequiredQty, BOMD.BOMD_Comment, 
                             BOMD.BOMD_BaseUomID, BOMD.BOMD_BaseUomQty, I.IM_FK_Attribute_UOM_id
                      FROM tblBOMDetail AS BOMD LEFT OUTER JOIN tblItem AS I ON BOMD.BOMD_ItemNo = I.IM_Item_Id
                      WHERE (BOMD.BOMD_BOM_DocNo = \''.$bomNo.'\')';
        $result = $this->db->query($sql_query)->result_array();
        return $result;
    }

    public function getBOMHeaderSpices($bomNo){
        $sql_query = 'SELECT        BOM.BOM_DocNo, BOM.BOM_BOMDescription, BOM.BOM_ItemNo, BOM.BOM_Version, BOM.BOM_YieldQty, BOM.BOM_YieldUomID, 
                                    BOM.BOM_BatchQty, BOM.BOM_BatchUomID, I.IM_FK_Attribute_UOM_id, ISNULL(IUC.IUC_Quantity, 0) AS BaseUOMQty
                     FROM            tblBOM AS BOM LEFT OUTER JOIN
                                     tblItemUOMConv AS IUC ON BOM.BOM_ItemNo = IUC.IUC_FK_Item_id AND BOM.BOM_YieldUomID = IUC.IUC_FK_UOM_id LEFT OUTER JOIN
                                     tblItem AS I ON BOM.BOM_ItemNo = I.IM_Item_Id
                      WHERE (BOM.BOM_ItemNo = \''.$bomNo.'\') AND (BOM.BOM_Status = \'Active\')
                      ORDER BY BOM.BOM_BOMDescription';
        
        $result = $this->db->query($sql_query)->result_array();

        return $result;
    }

    public function checkIfUsedByMO($bom_docno){
        $query_string = 'SELECT        MOD.MOD_BOM_DocNo, MO.MO_Status
                        FROM            tblManufacturingOrderDetail AS MOD LEFT OUTER JOIN
                                                 tblManufacturingOrder AS MO ON MOD.MOD_MO_DocNo = MO.MO_DocNo
                        WHERE        (MO.MO_Status = \'Open\') AND (MOD.MOD_BOM_DocNo = \''.$bom_docno.'\')';

        return $this->db->query($query_string)->num_rows();
    }

    public function checkIfUsedByOF($itemNo){
        $query_string = 'SELECT        OFD.OFD_IM_ItemNo, OrdF.OF_Status
                        FROM            tblOrderForecastDetail AS OFD LEFT OUTER JOIN
                                                 tblOrderForecast AS OrdF ON OFD.OFD_OF_DocNo = OrdF.OF_DocNo
                        WHERE        (OrdF.OF_Status = \'Open\') AND (OFD.OFD_IM_ItemNo = \''.$itemNo.'\')';

        return $this->db->query($query_string)->num_rows();
    }

    public function checkIfUsedByPF($itemNo){
        $query_string = 'SELECT        PFDS.PFSDS_ItemNo, PF.PF_Status
                        FROM            tblProductionForecastSubDetailSummary AS PFDS LEFT OUTER JOIN
                                        tblProductionForecast AS PF ON PFDS.PFSDS_PFD_PF_DocNo = PF.PF_DocNo
                        WHERE        (PF.PF_Status = \'Open\') AND (PFDS.PFSDS_ItemNo= \''.$itemNo.'\')';

        return $this->db->query($query_string)->num_rows();
    }*/


}
