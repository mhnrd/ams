<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class employee_profile_model extends MAIN_Model{


	protected $_table = 'tblEmployee';
	
	private $id = NULL;
	private $order_column = array(null,'Emp_Name');
  private $order_column_id_history = array(null,'EID_LineNo', 'EID_NewId', 'EID_BarcodeId','EID_BiometrixNo', 'CreatedBy','DateCreated');
  private $order_column_allowance = array(null,'PI_Desc', 'Allow_Amount', 'PI_Taxable', 'AD_Desc');
  private $order_column_siblings = array(null,'R_Name', 'R_Birthdate', 'Status', 'WorkType','CompanyAdd','AD_Desc');
  private $order_column_beneficiaries = array(null,'B_Name', 'Status', 'B_WorkType', 'B_CompanyAdd','AD_Desc');
  private $order_column_education = array(null,'EDU_SchoolAttended', 'EDU_CourseTaken', 'EDU_SchoolYear', 'EDU_Levels', 'EDU_HonorReceived');

  private $order_column_leave = array('AD_Desc', 'LL_NumDays', 'LL_Applied_Date', 'LL_LeaveFrom', 'LL_LeaveTo', 'LL_WithApproval', 'LL_Remarks', 'LL_Initial');
  private $order_column_leavebalance = array('AD_Desc', 'LT_Balance');
  private $order_column_leavebalanceview = array('LL_DocType', 'LL_DocNo','LL_DocDate','LL_NumDays');
  private $order_column_workschedule = array('WS_DocDate', 'WS_WeekDay', 'WS_FK_Shift_id', 'S_Name', 'S_StartTime', 'S_EndTime');
  private $order_column_notes = array(NULL, 'EmpN_Date', 'EmpN_Remarks');
  private $order_column_employeemovement = array('EM_AppliedDate', 'C_Name', 'P_Position', 'AD_Desc', 'EM_BasicRate', 'EM_BaseRate', 'EM_CashBond');
  private $order_column_employeeMemo = array('DocNo', 'DocDate', 'AppliedDate', 'Status', 'Createdby', 'DateCreated');
  private $order_column_employeeMemoDocuments = array('PKFK_Emp_id', 'ED_DocName', 'ED_Remarks', 'ED_DocDestination', 'download', 'CreatedBy', 'DateCreated', 'ModifiedBy', 'DateModified');


	public function __construct() {
		parent::__construct();
	}

	// DATA TABLE STARTS HERE
	public function table_data($status = null){

		$this->db->select('LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1))) AS Emp_Name,Emp_Id',false);

    if(count($status) == 0){
      $this->db->where('isnull(Emp_Active,0)','1');
    }
    else{
      if(isset($status['chkInactive'])){
        if($status['chkInactive'] == 'true'){
          $this->db->where('isnull(Emp_Active,0)', '1');
        }
        else{
          $this->db->where('isnull(Emp_Active,0)', '0'); 
        }
      }

      if(isset($status['filterBy']) && isset($status['searchBy'])){
          switch ($status['filterBy']) {
            case 'Department':
              $this->db->where('Emp_DepartmentId',$status['searchBy']);
              break;
            case 'Location':
              $this->db->where('Emp_FK_StoreID',$status['searchBy']);
              break;
            case 'Position':
              $this->db->where('Emp_PositionId',$status['searchBy']);
              break;
            case 'Principal':
              $this->db->where('Emp_CustomerId',$status['searchBy']);
              break;
            case 'PayGroup':
              $this->db->where('Emp_FK_GroupId',$status['searchBy']);
              break;
            default:
              break;
          }
      }
    }

    $this->db->from('tblEmployee');
		$result_count = $this->db->count_all_results();
		$output['data']					= $this->datatables($status);
		$output['num_rows']				= $this->get_filtered_data($status);
		$output['count_all_results']	= $result_count;

		return $output;
	}	

	public function query($status = null){
		
		$this->db->select('LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1))) AS Emp_Name,Emp_Id',false);

    if(count($status) == 0){
      $this->db->where('isnull(Emp_Active,0)','1');
    }
    else{
      if(isset($status['chkInactive'])){
        if($status['chkInactive'] != 'true'){
          $this->db->where('isnull(Emp_Active,0)', '1');
        }
        else{
          $this->db->where('isnull(Emp_Active,0)', '0'); 
        }
      }

      if(isset($status['filterBy']) && isset($status['searchBy'])){
          switch ($status['filterBy']) {
            case 'Department':
              $this->db->where('Emp_DepartmentId',$status['searchBy']);
              break;
            case 'Location':
              $this->db->where('Emp_FK_StoreID',$status['searchBy']);
              break;
            case 'Position':
              $this->db->where('Emp_PositionId',$status['searchBy']);
              break;
            case 'Principal':
              $this->db->where('Emp_CustomerId',$status['searchBy']);
              break;
            case 'PayGroup':
              $this->db->where('Emp_FK_GroupId',$status['searchBy']);
              break;
            default:
              break;
          }
      }
    }
		// INDIVIDUAL COLUMN SEARCH

		if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
			$this->db->like('LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1)))',$_POST['columns'][1]["search"]["value"],false);
		}

        // END

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->like("Emp_ID", $_POST["search"]["value"]);  
                $this->db->or_like('LTRIM(RTRIM(Emp_LastName)) + \', \' + LTRIM(RTRIM(Emp_FirstName)) + \' \' + LTRIM(RTRIM(LEFT(Emp_MiddleName, 1)))', $_POST["search"]["value"],false);  
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('Emp_Name', 'DESC');  
        }
	}

	public function datatables($status = null){
		$this->query($status); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblEmployee');  
        return $query->result_array();
	}

  public function get_filtered_data($status = null){  
         $this->query($status);  
         $query = $this->db->get('tblEmployee');  
         return $query->num_rows();  
  }

  // END 

     public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */

        $this->db->delete('tblEmployee',array('S_ID'=>$docno));

    }

    public function getAllEmployeeData($employee_id){
        $output         = array();
        $output['general_info']     = $this->getGeneralInfo($employee_id);
        return $output;
    }

    public function getGeneralInfo($employee_id){

        $attributes_model     = $this->load_model('administration/application_setup/general/attributes/attributes_model');

        $sql_query = 'SELECT a.Emp_Id 
                           , a.Emp_Title 
                           , a.Emp_FirstName 
                           , a.Emp_MiddleName 
                           , a.Emp_LastName 
                           , a.Emp_NickName 
                           , b.P_Position 
                           , a.Emp_CompanyId
                           , c.COM_Name 
                           , d.TC_ExemtDesc 
                           --, e.G_Desc 
                           , f.SP_StoreName 
                           , ISNULL(a.Emp_TardyExemp, 0)        AS Emp_TardyExemp 
                           , a.Emp_PermanentAddress 
                           , a.Emp_PermanentAddressTelNo 
                           , a.Emp_CurrentAddress 
                           , a.Emp_CurrentAddressTelNo 
                           , a.Emp_CurrentAddressMobileNo 
                           , a.Emp_PhoneNo 
                           , a.Emp_MobileNo 
                           , a.Emp_EmailAdd 
                           , a.Emp_CivilStatus 
                           , a.Emp_Gender 
                           , a.Emp_Height 
                           , a.Emp_Weight 
                           , ISNULL(a.Emp_Religion,\'\') AS Emp_Religion
                           , a.Emp_Nationality 
                           , a.Emp_Birthdate 
                           , a.Emp_PlaceofBirth 
                           , a.Emp_SSSNum 
                           , a.Emp_TINNum 
                           , a.Emp_PagIbigNum 
                           , a.Emp_PhilHealthNum 
                           , a.Emp_DateHired 
                           , a.Emp_DateResign 
                           , a.Emp_BaseRate 
                           , \'0000\'                             as Emp_BasicRate 
                           , a.Emp_BankAccountNum 
                           , a.Emp_GracePeriod 
                           , ISNULL(a.Emp_CheckBreak, 0)        AS Emp_CheckBreak 
                           , ISNULL(a.Emp_AgencyEmployee, 0)    AS Emp_AgencyEmployee 
                           , a.Emp_BarcodeID 
                           , g.C_Name 
                           , ISNULL(a.Emp_CompressSched, 0)     AS Emp_CompressSched 
                           , a.Emp_BarcodeExpiry 
                           , a.Emp_ContactPerson 
                           , a.Emp_ContactNumber 
                           , a.Emp_ContactAddress 
                           , a.Emp_ContactRelationship 
                           , ISNULL(a.Emp_ComputeSSS, 0)        AS Emp_ComputeSSS 
                           , ISNULL(a.Emp_ComputeTax, 0)        AS Emp_ComputeTax 
                           , ISNULL(a.Emp_ComputePagIbig, 0)    AS Emp_ComputePagIbig 
                           , ISNULL(a.Emp_ComputePhilHealth, 0) AS Emp_ComputePhilHealth 
                           , ISNULL(a.Emp_HoldPayroll, 0)       AS Emp_HoldPayroll 
                           , ISNULL(a.Emp_PictureID, \'\')        AS Emp_PictureID 
                           , ISNULL(a.Emp_Active, 0)            AS Emp_Active 
                           , ISNULL(a.Emp_FixedSSSEEAmt, 0)     AS Emp_FixedSSSEEAmt 
                           , isnull(Emp_WithTimeAttendance, 0)  as Emp_WithTimeAttendance 
                           , ISNULL(a.Emp_FixedSSSERAmt, 0)     AS Emp_FixedSSSERAmt 
                           , ISNULL(a.Emp_FixedPagibigEEAmt, 0) AS Emp_FixedPagibigEEAmt 
                           , ISNULL(a.Emp_FixedPagibigERAmt, 0) AS Emp_FixedPagibigERAmt 
                           , ISNULL(a.Emp_FixedPHealthEEAmt, 0) AS Emp_FixedPHealthEEAmt 
                           , ISNULL(a.Emp_FixedPHealthERAmt, 0) AS Emp_FixedPHealthERAmt 
                           , a.Emp_DepartmentId 
                           , i.AD_Desc                          AS DEP_Description 
                           , a.Emp_PayrollStart 
                           , ISNULL(a.Emp_Manual, 0)            AS Emp_Manual 
                           , ISNULL(a.Emp_LastPay, 0)           AS Emp_LastPay 
                           , h.AD_Desc 
                           , a.Emp_FK_ApplicanId 
                           , a.Emp_IdCard 
                           , a.Emp_Status 
                           , ISNULL(a.Emp_WorkHours, 0)         as Emp_WorkHours 
                           , ISNULL(a.Emp_FixedECAmt, 0)        as Emp_FixedECAmt 
                           , a.Emp_FK_GroupId 
                           , ISNULL(a.Emp_SavingsFund, 0)       as Emp_SavingsFund 
                           , ISNULL(a.Emp_FixedTax, 0)          as Emp_FixedTax 
                           , ISNULL(a.Emp_Month13thBasis, 0)    as Emp_Month13thBasis 
                           , Emp_PayrollMode 
                           , Emp_AttrBankCode 
                           , Emp_ClearanceDate 
                           , ISNULL(Emp_UndertimeExempt, 0)     AS Emp_UndertimeExempt 
                           , ISNULL(a.Emp_MotherMaidenName, \'\') as Emp_MotherMaidenName 
                           , ISNULL(ad.AD_Desc, \'\')             as AttendanceType 
                      FROM tblEmployee AS a 
                           LEFT OUTER JOIN tblAttributeDetail AS ad 
                                        ON a.Emp_AttendanceType = ad.AD_Id 
                           LEFT OUTER JOIN tblAttributeDetail AS i 
                                        ON a.Emp_DepartmentId = i.AD_Id 
                           LEFT OUTER JOIN tblAttributeDetail AS h 
                                        ON a.Emp_FK_Status_Attribute_id = h.AD_Id 
                           LEFT OUTER JOIN tblCustomer AS g 
                                        ON a.Emp_CustomerId = g.C_Id 
                           LEFT OUTER JOIN tblStoreProfile AS f 
                                        ON a.Emp_FK_StoreID = f.SP_ID 
                                           AND a.Emp_CompanyId = f.SP_FK_CompanyID 
                           LEFT OUTER JOIN tblCompany AS c 
                                        ON a.Emp_CompanyId = c.COM_Id 
                           LEFT OUTER JOIN tblPayGroup AS e 
                                        ON a.Emp_CompanyId = e.G_FK_CompanyId 
                                           AND a.Emp_FK_GroupId = e.G_Id 
                           LEFT OUTER JOIN tblTaxCategory AS d 
                                        ON a.Emp_TaxExemptId = d.TC_ID 
                           LEFT OUTER JOIN tblPosition AS b 
                                        ON a.Emp_PositionId = b.P_id 
                     WHERE ( '.$this->sql_hash('a.Emp_Id').' = \''.$employee_id.'\' ) ';
        $result = $this->db->query($sql_query)->row_array();

        $sql_query = 'SELECT  E_FirstEval,E_SecondEval, E_ThirdEval  
                     From tblEvaluation  
                     WHERE ('.$this->sql_hash('FK_Emp_Id').' = \''.$employee_id.'\')';
        $data_evalution = $this->db->query($sql_query)->row_array();      

        $sql_query = 'SELECT LTRIM(RTRIM(AD_Desc)) as Description, LTRIM(RTRIM(AD_Id)) AS Id 
                         FROM tblAttributeDetail 
                         WHERE (AD_FK_Code = \'1308\') AND (ISNULL(AD_Active, 0) = 1) AND (AD_Id = \''.$result['Emp_AttrBankCode'].'\') 
                     ORDER BY AD_Desc ';
        $data_bank = $this->db->query($sql_query)->row_array();   

        // config data
        $result['Emp_Birthdate']            = date_format(date_create($result['Emp_Birthdate']),'m/d/Y');
        $result['Emp_BarcodeExpiry']        = ($result['Emp_BarcodeExpiry'] != '') ? date_format(date_create($result['Emp_BarcodeExpiry']),'Y') : "";
        $result['Emp_DateHired']            = ($result['Emp_DateHired'] != '') ? date_format(date_create($result['Emp_DateHired']),'m/d/Y') : "";
        $result['Emp_DateResign']           = ($result['Emp_DateResign'] != '') ? date_format(date_create($result['Emp_Birthdate']),'m/d/Y') : "";             
        $result['Emp_PayrollStart']         = ($result['Emp_PayrollStart'] != '') ? date_format(date_create($result['Emp_PayrollStart']),'m/d/Y') : "";             
        $result['Emp_ClearanceDate']        = ($result['Emp_ClearanceDate'] != '') ? date_format(date_create($result['Emp_ClearanceDate']),'m/d/Y') : ""; 

        $result['E_FirstEval']              = ($data_evalution['E_FirstEval'] != '') ? date_format(date_create($data_evalution['E_FirstEval']),'m/d/Y') : ""; 
        $result['E_SecondEval']             = ($data_evalution['E_SecondEval'] != '') ? date_format(date_create($data_evalution['E_SecondEval']),'m/d/Y') : ""; 
        $result['E_ThirdEval']              = ($data_evalution['E_ThirdEval'] != '') ? date_format(date_create($data_evalution['E_ThirdEval']),'m/d/Y') : ""; 

        $result['Emp_BankName']             = $data_bank['Description'];
        $result['Emp_Gender']               = $result['Emp_Gender'] == 'M' ? "Male" : "Female";
        $result['Emp_ContactRelationship']  = $attributes_model->getAttributeName(RELATION,$result['Emp_ContactRelationship']);
        $result['Emp_Religion']             = $attributes_model->getAttributeName(RELIGION,$result['Emp_Religion']);
        $result['Emp_AttrBankCode']         = $attributes_model->getAttributeName(BANK,$result['Emp_AttrBankCode']);

        $result['Emp_Name']                 = ucfirst(strtolower($result['Emp_LastName'] . ', ' . $result['Emp_FirstName'] . ' ' . substr($result['Emp_MiddleName'],1,1)));

        switch ($result['Emp_CivilStatus']) {
           case 'S':
             $result['Emp_CivilStatus'] = 'Single';
             break;
           case 'M':
             $result['Emp_CivilStatus'] = 'Married';
             break;
           case 'D':
             $result['Emp_CivilStatus'] = 'Divored';
             break;
           case 'S1':
             $result['Emp_CivilStatus'] = 'Seperated';
             break;    
           default:
             break;
         } 

        return $result;
    }

    // DATA ID HISTORY TABLE STARTS HERE
  public function table_data_id_history($status = null){

    $this->db->select('EID_LineNo, EID_NewId, EID_BarcodeId,EID_BiometrixNo, CreatedBy, DateCreated',false);
    $this->db->where('PKFK_EmpId',$status);
    $this->db->from('tblEmployeeId');
    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_id_history($status);
    $output['num_rows']       = $this->get_filtered_data_id_history($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_id_history($status = null){
    
    $this->db->select('EID_LineNo, EID_NewId, EID_BarcodeId,EID_BiometrixNo, CreatedBy, DateCreated',false);

    $this->db->where('PKFK_EmpId',$status);

    // INDIVIDUAL COLUMN SEARCH

    if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
      $this->db->like('EID_LineNo',$_POST['columns'][1]["search"]["value"]);
    }

    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("Emp_ID", $_POST["search"]["value"]);  
            $this->db->or_like('EID_LineNo', $_POST["search"]["value"]);  
            $this->db->or_like('EID_NewId', $_POST["search"]["value"]);  
            $this->db->or_like('EID_BarcodeId', $_POST["search"]["value"]);  
            $this->db->or_like('EID_BiometrixNo', $_POST["search"]["value"]);  
            $this->db->or_like('CreatedBy', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_id_history[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('Emp_Name', 'DESC');  
    }
  }

  public function datatables_id_history($status = null){
    $this->query_id_history($status); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblEmployeeId');  
        return $query->result_array();
  }

  public function get_filtered_data_id_history($status = null){  
         $this->query_id_history($status);  
         $query = $this->db->get('tblEmployeeId');  
         return $query->num_rows();  
  }

  // END 


   // DATA ALLOWANCE TABLE STARTS HERE
  public function table_data_allowance($status = null){

    $this->db->select('FK_Emp_Id, Allow_Id, PI_Desc, Allow_Amount, ISNULL(PI_Taxable, 0) AS PI_Taxable, AD_Desc',false);
        $this->db->join('tblAttributeDetail','AD_Id = PI_AttAllowTypeId','left');
        $this->db->join('tblAllowance','Allow_Id = PI_id','left');
        $this->db->where('FK_Emp_Id',$status);
        $this->db->from('tblPAYItems');

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_allowance($status);
    $output['num_rows']       = $this->get_filtered_data_allowance($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_allowance($status = null){
    
    $this->db->select('FK_Emp_Id, Allow_Id, PI_Desc, Allow_Amount, ISNULL(PI_Taxable, 0) AS PI_Taxable, AD_Desc',false);

    $this->db->where('FK_Emp_Id',$status);

    // INDIVIDUAL COLUMN SEARCH

    if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
      $this->db->like('EID_LineNo',$_POST['columns'][1]["search"]["value"]);
    }

    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("FK_Emp_Id", $_POST["search"]["value"]);  
            $this->db->or_like('Allow_Id', $_POST["search"]["value"]);  
            $this->db->or_like('PI_Desc', $_POST["search"]["value"]);  
            $this->db->or_like('Allow_Amount', $_POST["search"]["value"]);  
            $this->db->or_like('PI_Taxable', $_POST["search"]["value"]);  
            $this->db->or_like('AD_Desc', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_allowance[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('PI_Desc', 'DESC');  
    }
  }

  public function datatables_allowance($status = null){
      $this->query_allowance($status); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
      
      $this->db->join('tblAttributeDetail','AD_Id = PI_AttAllowTypeId','left');
      $this->db->join('tblAllowance','Allow_Id = PI_id','left');
      $query = $this->db->get('tblPAYItems');  
      return $query->result_array();
  }

  public function get_filtered_data_allowance($status = null){  
       $this->query_allowance($status);  
       $this->db->join('tblAttributeDetail','AD_Id = PI_AttAllowTypeId','left');
       $this->db->join('tblAllowance','Allow_Id = PI_id','left');
       $query = $this->db->get('tblPAYItems'); 
       return $query->num_rows();  
  }

  // END 

  public function getFilteredData($type, $filter, $addtl = null, $filter_input = null){

      $where = '';
      switch ($type) {
        case 'store':
          if($filter_input != ''){
            $where = "AND SP_StoreName LIKE '%". $filter_input ."%' ";
          }
          $query_string = 'SELECT LTRIM(RTRIM(SP_StoreName)) as Description,  LTRIM(RTRIM(SP_ID)) AS Id  
                           From tblStoreProfile  
                           WHERE (SP_FK_CompanyID = \''.$filter.'\') '.$where.'
                           ORDER BY Description';
          break;
        case 'department':
          if($filter_input != ''){
              // $where = "AND b.P_Position LIKE '%". $filter_input ."%' ";
              $where = "AND P_Position LIKE '%". $filter_input ."%' ";
          }
          
          $query_string = 'SELECT LTRIM(RTRIM(P_Position)) as Description,  LTRIM(RTRIM(P_ID)) AS Id   
                           FROM  tblPosition
                           WHERE (P_Dept = \''.$filter.'\') AND (ISNULL(P_Active, 0) = 1)   '.$where.'
                           ORDER BY Description';
          break;
        case 'paygroup':
          if($filter_input != ''){
            $where = "AND G_Desc LIKE '%". $filter_input ."%' ";
          }
          $query_string = 'SELECT LTRIM(RTRIM(G_Desc)) as Description,  LTRIM(RTRIM(G_Id)) AS Id
                           FROM tblPAYGroup 
                           WHERE (G_FK_CompanyId = \''.$filter.'\')
                           ORDER BY Description';
          break;
        case 'attendancegroup':
          if($filter_input != ''){
            $where = "AND a.AD_Desc LIKE '%". $filter_input ."%' ";
          }
          $query_string  = " SELECT LTRIM(RTRIM(a.AD_Desc)) as Description,  LTRIM(RTRIM(tr.TR_AttendanceType)) AS Id
                             FROM tblPAYTimekeepingRules AS tr LEFT OUTER JOIN tblAttributeDetail AS a ON tr.TR_AttendanceType = a.AD_Id 
                             WHERE  (tr.TR_PKFK_CompanyId = '".$filter."') AND (tr.TR_GroupId ='".$addtl."')  ".$where."
                             ORDER BY Description";
          break;
        case 'payitems':
          if($filter_input != '' && trim($addtl) == 'Description ASC'){
            $where = "AND PI_Desc LIKE '%". $filter_input ."%' ";
          }
          else if($filter_input != '' && trim($addtl) == 'Id ASC'){
            $where = "AND PI_id LIKE '%". $filter_input ."%' ";
          }
          $query_string = 'SELECT LTRIM(rTRIM(PI_id)) AS Id, LTRIM(RTRIM(PI_Desc)) AS Description  
                           From tblPAYItems  
                           WHERE (IsNull(PI_Allowance, 0) = 1) '.$where.' AND  
                                   (PI_id NOT IN (SELECT   Allow_Id  
                                                   From tblAllowance  
                                                   WHERE     (Allow_Id = tblPAYItems.PI_id) AND (FK_Emp_Id = \''.$filter.'\')))
                           ORDER BY '.$addtl.' ';
          break;
        case 'leavetype':
          if($filter_input != ''){
            $where = "AND LTRIM(RTRIM(AD_Desc)) LIKE '%". $filter_input ."%' ";
          }
          $query_string  = " SELECT LTRIM(RTRIM(AD_Desc)) as Description,  LTRIM(RTRIM(AD_Id)) AS Id
                             FROM tblAttributeDetail 
                             WHERE  AD_FK_Code = '".LEAVE_TYPE."' ".$where."
                              AND AD_Id NOT IN (SELECT PKFK_LeaveType FROM tblLeaveBalance WHERE PKFK_Empid = '".$filter."')
                             ORDER BY Description";
          break;
        default:
          return;
          break;
      }
      
      return $this->db->query($query_string)->result_array();
  }

  public function getFilteredByData($filter, $filter_input = null){

      $where = '';
      switch ($filter) {
        case 'Department':
          $where = " AND LTRIM(RTRIM(AD_Desc)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(AD_Desc)) as Description,  LTRIM(RTRIM(AD_Id)) AS Id   
                           FROM  tblAttributeDetail
                           WHERE (ISNULL(AD_Active, 0) = 1) AND (AD_FK_Code = \''.DEPARTMENT.'\') '.$where.'
                           ORDER BY Description';
          break;
        case 'Position':
          $where = " AND LTRIM(RTRIM(P_Position)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(P_Position)) as Description,  LTRIM(RTRIM(P_ID)) AS Id   
                           FROM  tblPosition
                           WHERE (ISNULL(P_Active, 0) = 1) '.$where.'
                           ORDER BY Description';
          break;
        case 'Location':
          $where = " AND LTRIM(RTRIM(SP_StoreName)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(SP_StoreName)) as Description,  LTRIM(RTRIM(SP_ID)) AS Id   
                           FROM  tblStoreProfile
                           WHERE (ISNULL(SP_Active, 0) = 1) '.$where.'
                           ORDER BY Description';
          break;
        case 'Principal':
          $where = " AND LTRIM(RTRIM(C_Name)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(C_Name)) as Description,  LTRIM(RTRIM(C_Id)) AS Id   
                           FROM  tblCustomer
                           WHERE (ISNULL(C_Active, 0) = 1) '.$where.'
                           ORDER BY Description';
          break;
        case 'PayGroup':
          $where = "WHERE LTRIM(RTRIM(G_Desc)) LIKE '%".$filter_input."%' ";
          $query_string = 'SELECT LTRIM(RTRIM(G_Desc)) as Description,  LTRIM(RTRIM(G_Id)) AS Id   
                           FROM  tblPayGroup '.$where.'
                           ORDER BY Description';
          break;
        default:
          return;
          break;
      }
      return $this->db->query($query_string)->result_array();
  }

  public function getAllFamilyData($employee_id){
    try {
      $sql_query = '
        SELECT a.Emp_FatherName, a.Emp_FatherOccupation, a.Emp_FatherCompany, a.Emp_FatherContact, 
               a.Emp_MotherName, a.Emp_MotherOccupation, a.Emp_MotherContact, a.Emp_MotherCompany, 
               a.Emp_SpouseName, a.Emp_SpouseOccupation, a.Emp_SpouseContact, a.Emp_SpouseCompany, 
               a.Emp_SpouseAge , b.ED_SpouseEmploymentStat 
           FROM tblEmployee AS a LEFT OUTER JOIN tblEmployeeDetail AS b ON a.Emp_Id = b.ED_Id 
           WHERE (a.Emp_Id = ?)
      ';
      return $this->db->query($sql_query,array($employee_id))->row_array();
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function getAllEducationData($employee_id){
    try {
      $sql_query = '
          SELECT  ISNULL(ED_ContinueStudy,0) AS ED_ContinueStudy, ED_Course, ED_ContinueStudyWhen, ISNULL(ED_Exam,0) AS ED_Exam, ED_ExamType, 
                  ED_ExamDesc, ED_ExamRating 
          FROM tblEmployeeDetail
          WHERE (ED_Id = ?)
      ';
      $result = $this->db->query($sql_query,array($employee_id));

      if($result->num_rows() > 0){
        $result = $result->row_array();
      }
      else{
        $result = array(
          'ED_ContinueStudy'      => 0, 
          'ED_Course'             => '', 
          'ED_ContinueStudyWhen'  => '', 
          'ED_Exam'               => 0, 
          'ED_ExamType'           => '', 
          'ED_ExamDesc'           => '', 
          'ED_ExamRating'         => ''
        );
      }

      return $result;
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function getAllAllowanceData($employee_id, $payitems_id){
    try {
      $sql_query = '
          SELECT LTRIM(rTRIM(PI_id)) AS Id, LTRIM(RTRIM(PI_Desc)) AS Description,  
             CASE WHEN PI_Type = \'E\' THEN \'Earnings\' ELSE \'Deduction\' END AS PI_Type,  
             PI_Basis2Compute, PI_Sperate, PI_Regrate, ISNULL(PI_Taxable,0) AS PI_Taxable, PI_Syscode, PI_Debit,  
             PI_NonTaxExclude, PI_Cutoff, PI_CutoffType  
         From tblPAYItems  
         WHERE (IsNull(PI_Allowance, 0) = 1) AND (PI_id = ?) 
      ';
      $result = $this->db->query($sql_query,array($payitems_id));

      if($result->num_rows() > 0){
        $result = $result->row_array();
      }
      else{
        $result = array(
          'Id'                    => '', 
          'Description'           => '', 
          'PI_Type'               => '', 
          'PI_Taxable'            => 0, 
          'PI_Cutoff'             => '', 
          'PI_CutoffType'         => '', 
        );
      }

      return $result;
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }


  // DATA SIBLINGS TABLE STARTS HERE
  public function table_data_siblings($status = null){

    $this->db->select('R_EmpId, R_RecordNum, R_Name, R_Birthdate,
                       CASE Status WHEN \'S\' THEN \'Single\' 
                                     WHEN \'M\' THEN \'Married\'
                                     WHEN \'D\' THEN \'Divorced\'
                                     WHEN \'S1\' THEN \'Seperated\'
                                     ELSE \'\' END as Status, 
                       WorkType, CompanyAdd, AD_Desc, AD_Id',false);
        $this->db->join('tblAttributeDetail','R_FK_RelationAtt = AD_Id','left');
        $this->db->where('R_EmpId',$status);
        $this->db->where('AD_FK_Code',RELATION);
        $this->db->from('tblEmployeeRelative');

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_siblings($status);
    $output['num_rows']       = $this->get_filtered_data_siblings($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_siblings($status = null){
    
    $this->db->select('R_EmpId, R_RecordNum, R_Name, R_Birthdate,
                       CASE Status WHEN \'S\' THEN \'Single\' 
                                     WHEN \'M\' THEN \'Married\'
                                     WHEN \'D\' THEN \'Divorced\'
                                     WHEN \'S1\' THEN \'Seperated\'
                                     ELSE \'\' END as Status, 
                       WorkType, CompanyAdd, AD_Desc, AD_Id',false);
    $this->db->where('R_EmpId',$status);
    $this->db->where('AD_FK_Code',RELATION);

    // INDIVIDUAL COLUMN SEARCH

    if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
      $this->db->like('EID_LineNo',$_POST['columns'][1]["search"]["value"]);
    }

    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("R_EmpId", $_POST["search"]["value"]);  
            $this->db->or_like('R_RecordNum', $_POST["search"]["value"]);  
            $this->db->or_like('R_Name', $_POST["search"]["value"]);  
            $this->db->or_like('R_Birthdate', $_POST["search"]["value"]);  
            $this->db->or_like('WorkType', $_POST["search"]["value"]);  
            $this->db->or_like('CompanyAdd', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_siblings[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('R_RecordNum', 'ASC');  
    }
  }

  public function datatables_siblings($status = null){
      $this->query_siblings($status); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
      
      $this->db->join('tblAttributeDetail','R_FK_RelationAtt = AD_Id','left');
      $query = $this->db->get('tblEmployeeRelative');  
      return $query->result_array();
  }

  public function get_filtered_data_siblings($status = null){  
       $this->query_siblings($status);  
       $this->db->join('tblAttributeDetail','R_FK_RelationAtt = AD_Id','left');
       $query = $this->db->get('tblEmployeeRelative'); 
       return $query->num_rows();  
  }

  // END 

   // DATA BENEFICIARIES TABLE STARTS HERE
  public function table_data_beneficiaries($status = null){

    $this->db->select('B_EmpId, B_RecordNum, B_Name,
                       CASE B_Status WHEN \'S\' THEN \'Single\' 
                                     WHEN \'M\' THEN \'Married\'
                                     WHEN \'D\' THEN \'Divorced\'
                                     WHEN \'S1\' THEN \'Seperated\'
                                     ELSE \'\' END as Status, 
                       B_WorkType, B_CompanyAdd, AD_Desc, AD_Id',false);
        $this->db->join('tblAttributeDetail','B_FK_RelationAtt = AD_Id','left');
        $this->db->where('B_EmpId',$status);
        $this->db->where('AD_FK_Code',RELATION);
        $this->db->from('tblEmployeeBeneficiaries');

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_beneficiaries($status);
    $output['num_rows']       = $this->get_filtered_data_beneficiaries($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_beneficiaries($status = null){
    
    $this->db->select('B_EmpId, B_RecordNum, B_Name,
                       CASE B_Status WHEN \'S\' THEN \'Single\' 
                                     WHEN \'M\' THEN \'Married\'
                                     WHEN \'D\' THEN \'Divorced\'
                                     WHEN \'S1\' THEN \'Seperated\'
                                     ELSE \'\' END as Status, 
                       B_WorkType, B_CompanyAdd, AD_Desc, AD_Id',false);
    $this->db->where('B_EmpId',$status);
    $this->db->where('AD_FK_Code',RELATION);

    // INDIVIDUAL COLUMN SEARCH

    if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
      $this->db->like('EID_LineNo',$_POST['columns'][1]["search"]["value"]);
    }

    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("B_EmpId", $_POST["search"]["value"]);  
            $this->db->or_like('B_RecordNum', $_POST["search"]["value"]);  
            $this->db->or_like('B_Name', $_POST["search"]["value"]);  
            $this->db->or_like('B_WorkType', $_POST["search"]["value"]);  
            $this->db->or_like('B_CompanyAdd', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_beneficiaries[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('B_RecordNum', 'ASC');  
    }
  }

  public function datatables_beneficiaries($status = null){
      $this->query_beneficiaries($status); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
      
      $this->db->join('tblAttributeDetail','B_FK_RelationAtt = AD_Id','left');
      $query = $this->db->get('tblEmployeeBeneficiaries');  
      return $query->result_array();
  }

  public function get_filtered_data_beneficiaries($status = null){  
       $this->query_beneficiaries($status);  
       $this->db->join('tblAttributeDetail','B_FK_RelationAtt = AD_Id','left');
       $query = $this->db->get('tblEmployeeBeneficiaries'); 
       return $query->num_rows();  
  }

  // END 

  public function saveSiblingsRecord($data){
    try {

      $result = $this->db->where('R_EmpId',$data['R_EmpId'])
                    ->order_by('R_RecordNum','DESC')
                    ->limit(1)
                    ->get('tblEmployeeRelative');

      $lineNo = 1;
      if($result->num_rows() > 0){
        $lineNo = $result->row_array()['R_RecordNum'] + 1;
      }

      if($data['mode'] == 'add'){
        // insert
        unset($data['mode']);
        $data['R_RecordNum']  = $lineNo;
        $data['CreatedBy']    = getCurrentUser()['login-user'];
        $data['DateCreated']  = date_format(date_create(),'m/d/Y');
        $this->db->insert('tblEmployeeRelative',$data);
      }
      else{
        // update
        $where = array('R_EmpId' => $data['R_EmpId'],'R_RecordNum' => $data['R_RecordNum']);

        unset($data['mode']);
        unset($data['R_EmpId']);
        unset($data['R_RecordNum']);
        $data['ModifiedBy']    = getCurrentUser()['login-user'];
        $data['DateModified']  = date_format(date_create(),'m/d/Y');
        $this->db->where($where)->update('tblEmployeeRelative',$data);
      }
    } catch (Exception $e) {
      throw new Exception($e->getMessage().", Error , error");
    }
  }

  public function saveBeneficiariesRecord($data){
     try {

      $result = $this->db->where('B_EmpId',$data['B_EmpId'])
                    ->order_by('B_RecordNum','DESC')
                    ->limit(1)
                    ->get('tblEmployeeBeneficiaries');

      $lineNo = 1;
      if($result->num_rows() > 0){
        $lineNo = $result->row_array()['B_RecordNum'] + 1;
      }

      if($data['mode'] == 'add'){
        // insert
        unset($data['mode']);
        $data['B_RecordNum']  = $lineNo;
        $data['CreatedBy']    = getCurrentUser()['login-user'];
        $data['DateCreated']  = date_format(date_create(),'m/d/Y');
        $this->db->insert('tblEmployeeBeneficiaries',$data);
      }
      else{
        // update
        $where = array('B_EmpId' => $data['B_EmpId'],'B_RecordNum' => $data['B_RecordNum']);

        unset($data['mode']);
        unset($data['B_EmpId']);
        unset($data['B_RecordNum']);
        $data['ModifiedBy']    = getCurrentUser()['login-user'];
        $data['DateModified']  = date_format(date_create(),'m/d/Y');
        $this->db->where($where)->update('tblEmployeeBeneficiaries',$data);
      }
    } catch (Exception $e) {
      throw new Exception($e->getMessage().", Error , error");
    }
  }

  // Save or Update Allowance Record
  public function saveAllowanceDataRecord($data){
     try {

      $data['Allow_Id'] = $data['Allow_Desc'];
      unset($data['Allow_Desc']);
      unset($data['DeductionType']);
      unset($data['Cutoff']);
      unset($data['Emp_CompanyId']);
      unset($data['Emp_Id']);
      if($data['mode'] == 'add'){
        // insert
        unset($data['mode']);
        $data['CreatedBy']    = getCurrentUser()['login-user'];
        $data['DateCreated']  = date_format(date_create(),'m/d/Y');
        $this->db->insert('tblAllowance',$data);
      }
      else{
        // update
        $where = array('FK_Emp_Id' => $data['FK_Emp_Id'],'Allow_Id' => $data['Allow_Id']);

        unset($data['mode']);
        unset($data['FK_Emp_Id']);
        unset($data['Allow_Id']);
        $data['ModifiedBy']    = getCurrentUser()['login-user'];
        $data['DateModified']  = date_format(date_create(),'m/d/Y');
        $this->db->where($where)->update('tblAllowance',$data);
      }
    } catch (Exception $e) {
      throw new Exception($e->getMessage().", Error , error");
    }
  }

  // Save or Update Leave Balance Record
  public function saveLeaveBalanceDataRecord($data){
     try {

      if($data['mode'] == 'add'){
        // insert
        unset($data['mode']);
        
        // save leave balance
        $this->db->insert('tblLeaveBalance',$data);

        // get latest EntryNo
        // $EntryNo = $this->db->get('tblLeaveBalance')->num_rows() + 1;
        $EntryNo = $this->db->select('top 1 LL_EntryNo',false)->order_by('LL_EntryNo','DESC')->get('tblLeaveLedger')->row_array()['LL_EntryNo'] + 1;

        // save leave ledger
        $data['LL_EntryNo']         = $EntryNo;
        $data['LL_DocType']         = 'BEGBAL';
        $data['LL_DocNo']           = 'BEGBAL';
        $data['LL_DocDate']         = date_format(date_create(),'m/d/Y');
        $data['LL_FK_Emp_id']       = $data['PKFK_Empid'];
        $data['LL_NumDays']         = $data['LT_Balance'];
        $data['LL_LeaveTypeAttr']   = $data['PKFK_LeaveType'];
        $data['CreatedBy']          = getCurrentUser()['login-user'];
        $data['DateCreated']        = date_format(date_create(),'m/d/Y');

        // unset data
        unset($data['PKFK_Empid']);
        unset($data['LT_Balance']);
        unset($data['PKFK_LeaveType']);
        $this->db->insert('tblLeaveLedger',$data);
      }
      else{
        // update

        // update leave balance
        $where = array('PKFK_Empid' => $data['PKFK_Empid'],'PKFK_LeaveType' => $data['PKFK_LeaveType']);

        unset($data['mode']);
        unset($data['PKFK_Empid']);
        unset($data['PKFK_LeaveType']);
        $this->db->where($where)->update('tblLeaveBalance',$data);

        $where = array(
          'LL_FK_Emp_id'          => $where['PKFK_Empid'],
          'LL_DocType'            => 'BEGBAL',
          'LL_DocNo'              => 'BEGBAL',
          'LL_LeaveTypeAttr'      => $where['PKFK_LeaveType'],
        );

        $data['LL_NumDays']         = $data['LT_Balance'];
        unset($data['LT_Balance']);
        $data['ModifiedBy']    = getCurrentUser()['login-user'];
        $data['DateModified']  = date_format(date_create(),'m/d/Y');
        $this->db->where($where)->update('tblLeaveLedger',$data);
      }
    } catch (Exception $e) {
      throw new Exception($e->getMessage().", Error , error");
    }
  }
  
   // DATA BENEFICIARIES TABLE STARTS HERE
  public function table_data_education($status = null){

    $this->db->select('EDU_Id, EDU_LineNo, EDU_SchoolAttended, EDU_CourseTaken, EDU_SchoolYear, EDU_Levels, EDU_HonorReceived',false);
    $this->db->where('EDU_Id',$status);
    $this->db->from('tblEmployeeEducation');

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_education($status);
    $output['num_rows']       = $this->get_filtered_data_education($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_education($status = null){
    
    $this->db->select('EDU_Id, EDU_LineNo, EDU_SchoolAttended, EDU_CourseTaken, EDU_SchoolYear, EDU_Levels, EDU_HonorReceived',false);
    $this->db->where('EDU_Id',$status);

    // INDIVIDUAL COLUMN SEARCH

    if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
      $this->db->like('EDU_LineNo',$_POST['columns'][1]["search"]["value"]);
    }

    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("EDU_SchoolAttended", $_POST["search"]["value"]);  
            $this->db->or_like('EDU_CourseTaken', $_POST["search"]["value"]);  
            $this->db->or_like('EDU_SchoolYear', $_POST["search"]["value"]);  
            $this->db->or_like('EDU_Levels', $_POST["search"]["value"]);  
            $this->db->or_like('EDU_HonorReceived', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_education[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('EDU_SchoolYear', 'ASC');  
    }
  }

  public function datatables_education($status = null){
      $this->query_education($status); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
       
      $query = $this->db->get('tblEmployeeEducation');  
      return $query->result_array();
  }

  public function get_filtered_data_education($status = null){  
       $this->query_education($status);   
       $query = $this->db->get('tblEmployeeEducation'); 
       return $query->num_rows();  
  }

  // END   

  // DATA LEAVE TABLE STARTS HERE
  public function table_data_leave($status = null){

    $this->db->select('AD_Desc, LL_NumDays, LL_Applied_Date,LL_LeaveFrom, LL_LeaveTo, ISNULL(LL_WithApproval, 0) AS LL_WithApproval, LL_Remarks, ISNULL(LL_Initial,0) AS LL_Initial ',false);
    $this->db->where('LL_FK_Emp_id',$status);
    $this->db->where('AD_FK_Code',LEAVE_TYPE);
    $this->db->join('tblAttributeDetail','AD_Id = LL_LeaveTypeAttr');
    $this->db->from('tblLeaveLedger');

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_leave($status);
    $output['num_rows']       = $this->get_filtered_data_leave($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_leave($status = null){
    
    $this->db->select('AD_Desc, LL_NumDays, LL_Applied_Date,LL_LeaveFrom, LL_LeaveTo, ISNULL(LL_WithApproval, 0) AS LL_WithApproval, LL_Remarks, ISNULL(LL_Initial,0) AS LL_Initial ',false);
    $this->db->where('LL_FK_Emp_id',$status);
    $this->db->where('AD_FK_Code',LEAVE_TYPE);
    $this->db->join('tblAttributeDetail','AD_Id = LL_LeaveTypeAttr');

    // INDIVIDUAL COLUMN SEARCH

    if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
      $this->db->like('EDU_LineNo',$_POST['columns'][1]["search"]["value"]);
    }

    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("AD_Desc", $_POST["search"]["value"]);  
            $this->db->or_like('LL_NumDays', $_POST["search"]["value"]);  
            $this->db->or_like('LL_Applied_Date', $_POST["search"]["value"]);  
            $this->db->or_like('LL_LeaveFrom', $_POST["search"]["value"]);  
            $this->db->or_like('LL_LeaveTo', $_POST["search"]["value"]);  
            $this->db->or_like('LL_Remarks', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_leave[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('LL_LineNo', 'ASC');  
    }
  }

  public function datatables_leave($status = null){
      $this->query_leave($status); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
      
      $query = $this->db->get('tblLeaveLedger');  
      return $query->result_array();
  }

  public function get_filtered_data_leave($status = null){  
       $this->query_leave($status);   
       $query = $this->db->get('tblLeaveLedger'); 
       return $query->num_rows();  
  }

  // END 

  // DATA LEAVE BALANCE TABLE STARTS HERE
  public function table_data_leavebalance($status = null){

    $this->db->select('PKFK_Empid,PKFK_LeaveType,AD_Desc, ISNULL(SUM(LT_Balance), 0) AS LT_Balance',false);
    $this->db->where('PKFK_Empid',$status);
    $this->db->where('AD_FK_Code',LEAVE_TYPE);
    $this->db->join('tblAttributeDetail','AD_Id = PKFK_LeaveType');
    $this->db->group_by('PKFK_Empid,PKFK_LeaveType,AD_Desc');
    $this->db->from('tblLeaveBalance');

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_leavebalance($status);
    $output['num_rows']       = $this->get_filtered_data_leavebalance($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_leavebalance($status = null){
    
    $this->db->select('PKFK_Empid,PKFK_LeaveType,AD_Desc, ISNULL(SUM(LT_Balance), 0) AS LT_Balance',false);
    $this->db->where('PKFK_Empid',$status);
    $this->db->where('AD_FK_Code',LEAVE_TYPE);
    $this->db->join('tblAttributeDetail','AD_Id = PKFK_LeaveType');
    $this->db->group_by('PKFK_Empid,PKFK_LeaveType,AD_Desc');

    // INDIVIDUAL COLUMN SEARCH

    if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
      $this->db->like('EDU_LineNo',$_POST['columns'][1]["search"]["value"]);
    }

    // END

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("AD_Desc", $_POST["search"]["value"]);  
            $this->db->or_like('LT_Balance', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_leavebalance[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('AD_Desc', 'ASC');  
    }
  }

  public function datatables_leavebalance($status = null){
      $this->query_leavebalance($status); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
       
      $query = $this->db->get('tblLeaveBalance');  
      return $query->result_array();
  }

  public function get_filtered_data_leavebalance($status = null){  
       $this->query_leavebalance($status);   
       $query = $this->db->get('tblLeaveBalance'); 
       return $query->num_rows();  
  }

  // END 

  
  // DATA LEAVE BALANCE VIEW TABLE STARTS HERE
  public function table_data_leavebalanceview($status = null, $leave_type = null){

    $this->db->select(' LL_DocType, LL_DocNo, LL_DocDate, LL_NumDays',false);
    $this->db->where('LL_FK_Emp_id',$status);
    $this->db->where('LL_LeaveTypeAttr',$leave_type);
    $this->db->from('tblLeaveLedger');

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_leavebalanceview($status,$leave_type);
    $output['num_rows']       = $this->get_filtered_data_leavebalanceview($status,$leave_type);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_leavebalanceview($status = null,$leave_type){
    
    $this->db->select(' LL_DocType, LL_DocNo, LL_DocDate, LL_NumDays',false);
    $this->db->where('LL_FK_Emp_id',$status);
    $this->db->where('LL_LeaveTypeAttr',$leave_type);


    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("LL_DocType", $_POST["search"]["value"]);  
            $this->db->or_like('LL_DocNo', $_POST["search"]["value"]);  
            $this->db->or_like('LL_DocDate', $_POST["search"]["value"]);  
            $this->db->or_like('LL_NumDays', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_leavebalanceview[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('LL_EntryNo', 'ASC');  
    }
  }

  public function datatables_leavebalanceview($status = null, $leave_type = null){
      $this->query_leavebalanceview($status,$leave_type); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
       
      $query = $this->db->get('tblLeaveLedger');  
      return $query->result_array();
  }

  public function get_filtered_data_leavebalanceview($status = null, $leave_type = null){  
       $this->query_leavebalanceview($status,$leave_type);   
       $query = $this->db->get('tblLeaveLedger'); 
       return $query->num_rows();  
  }

  // END 

  // DATA WORK SCHEDULE TABLE STARTS HERE
  public function table_data_workschedule($status = null){

    $this->db->select('WS_Emp_id ,  WS_DocDate ,  
                       CASE  WHEN WS_WeekDay = 1 then \'Sunday\'  
                        WHEN WS_WeekDay = 2 then \'Monday\'  
                        WHEN WS_WeekDay = 3 then \'Tuesday\'   
                        WHEN WS_WeekDay = 4 then \'Wednesday\'  
                        WHEN WS_WeekDay = 5 then \'Thursday\'  
                        WHEN WS_WeekDay = 6 then \'Friday\'  
                        WHEN WS_WeekDay = 7 then \'Saturday\' end  AS WS_WeekDay,   
                    WS_FK_Shift_id, S_Name , S_StartTime, S_EndTime ',false);
    $this->db->where('WS_Emp_id',$status);    
    $this->db->join('tblShift','WS_FK_Shift_id = S_ID');
    $this->db->from('tblWorkSchedule');

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_workschedule($status);
    $output['num_rows']       = $this->get_filtered_data_workschedule($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_workschedule($status = null){
    
    $this->db->select('WS_Emp_id ,  WS_DocDate ,  
                       CASE  WHEN WS_WeekDay = 1 then \'Sunday\'  
                        WHEN WS_WeekDay = 2 then \'Monday\'  
                        WHEN WS_WeekDay = 3 then \'Tuesday\'   
                        WHEN WS_WeekDay = 4 then \'Wednesday\'  
                        WHEN WS_WeekDay = 5 then \'Thursday\'  
                        WHEN WS_WeekDay = 6 then \'Friday\'  
                        WHEN WS_WeekDay = 7 then \'Saturday\' end  AS WS_WeekDay,   
                    WS_FK_Shift_id, S_Name , S_StartTime, S_EndTime ',false);
    $this->db->where('WS_Emp_id',$status);    
    $this->db->join('tblShift','WS_FK_Shift_id = S_ID');

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("WS_DocDate", $_POST["search"]["value"]);  
            $this->db->or_like('WS_FK_Shift_id', $_POST["search"]["value"]);  
            $this->db->or_like('S_Name', $_POST["search"]["value"]);  
            $this->db->or_like('S_StartTime', $_POST["search"]["value"]);  
            $this->db->or_like('S_EndTime', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_workschedule[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('WS_DocDate', 'ASC');  
    }
  }

  public function datatables_workschedule($status = null){
      $this->query_workschedule($status); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
       
      $query = $this->db->get('tblWorkSchedule');  
      return $query->result_array();
  }

  public function get_filtered_data_workschedule($status = null){  
       $this->query_workschedule($status);   
       $query = $this->db->get('tblWorkSchedule'); 
       return $query->num_rows();  
  }

  // END 

  // DATA NOTES TABLE STARTS HERE
  public function table_data_notes($status = null){

    $this->db->select('EmpN_Id, EmpN_RecNo, EmpN_Date,  REPLACE(EmpN_Remarks, \'\\s\', CHAR(13)) AS EmpN_Remarks, CreatedBy ',false);
    $this->db->where('EmpN_Id',$status);    
    $this->db->from('tblEmployeeNotes');

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_notes($status);
    $output['num_rows']       = $this->get_filtered_data_notes($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_notes($status = null){
    
    $this->db->select('EmpN_Id, EmpN_RecNo, EmpN_Date,  REPLACE(EmpN_Remarks, \'\\s\', CHAR(13)) AS EmpN_Remarks, CreatedBy ',false);
    $this->db->where('EmpN_Id',$status);    

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("EmpN_Date", $_POST["search"]["value"]);  
            $this->db->or_like('EmpN_Remarks', $_POST["search"]["value"]);  
            $this->db->or_like('S_Name', $_POST["search"]["value"]);  
            $this->db->or_like('S_StartTime', $_POST["search"]["value"]);  
            $this->db->or_like('S_EndTime', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_notes[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('EmpN_RecNo', 'ASC');  
    }
  }

  public function datatables_notes($status = null){
      $this->query_notes($status); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
       
      $query = $this->db->get('tblEmployeeNotes');  
      return $query->result_array();
  }

  public function get_filtered_data_notes($status = null){  
       $this->query_notes($status);   
       $query = $this->db->get('tblEmployeeNotes'); 
       return $query->num_rows();  
  }

  // END 

  // DATA MEMO HISTORY TABLE STARTS HERE
  public function table_data_employeeMemo($status = null, $memo_type = null){

    if($memo_type == 'Personnel Action Notice'){
      $this->db->select('PKFK_EmpId, PAN_DocNo AS DocNo, PAN_DocDate AS DocDate, PAN_AppliedDate AS AppliedDate, PAN_Status AS Status, CreatedBy, DateCreated ',false);
      $this->db->where('PKFK_EmpId',$status);
      $this->db->where('PAN_Status','Posted');
      $this->db->from('tblPANMemoHeader');

    }
    else if($memo_type == 'Payroll Inclusion Memo'){
      $this->db->select('PKFK_EmpId, PIM_DocNo AS DocNo, PIM_DocDate AS DocDate, PIM_AppliedDate AS AppliedDate, PIM_Status AS Status, CreatedBy, DateCreated ',false);
      $this->db->where('PKFK_EmpId',$status);
      $this->db->where('PIM_Status','Posted');
      $this->db->from('tblPIMMemoHeader');
    }
    else{
      throw new Exception("Error Processing Employee Memo History");
    }

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_employeeMemo($status,$memo_type);
    $output['num_rows']       = $this->get_filtered_data_employeeMemo($status,$memo_type);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_employeeMemo($status = null, $memo_type = null){
    
    if($memo_type == 'Personnel Action Notice'){
      $this->db->select('PKFK_EmpId, PAN_DocNo AS DocNo, PAN_DocDate AS DocDate, PAN_AppliedDate AS AppliedDate, PAN_Status AS Status, CreatedBy, DateCreated ',false);
      $this->db->where('PKFK_EmpId',$status);
      $this->db->where('PAN_Status','Posted');
    }
    else if($memo_type == 'Payroll Inclusion Memo'){
      $this->db->select('PKFK_EmpId, PIM_DocNo AS DocNo, PIM_DocDate AS DocDate, PIM_AppliedDate AS AppliedDate, PIM_Status AS Status, CreatedBy, DateCreated ',false);
      $this->db->where('PKFK_EmpId',$status);
      $this->db->where('PIM_Status','Posted');
    }
    else{
      throw new Exception("Error Processing Employee Memo History");
    }


    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            
            if($memo_type == 'Personnel Action Notice'){
              $this->db->like("PAN_DocNo", $_POST["search"]["value"]);  
              $this->db->or_like('PAN_DocDate', $_POST["search"]["value"]);  
              $this->db->or_like('PAN_Status', $_POST["search"]["value"]); 
            }
            else if($memo_type == 'Payroll Inclusion Memo'){
              $this->db->like("PIM_DocNo", $_POST["search"]["value"]);  
              $this->db->or_like('PIM_DocDate', $_POST["search"]["value"]);  
              $this->db->or_like('PIM_Status', $_POST["search"]["value"]);
            }
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_employeeMemo[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('DocNo', 'ASC');  
    }
  }

  public function datatables_employeeMemo($status = null, $memo_type = null){
      $this->query_employeeMemo($status,$memo_type); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
       
      if($memo_type == 'Personnel Action Notice'){
        $query = $this->db->get('tblPANMemoHeader'); 
      }
      else if($memo_type == 'Payroll Inclusion Memo'){
        $query = $this->db->get('tblPIMMemoHeader'); 
      }
      return $query->result_array();
  }

  public function get_filtered_data_employeeMemo($status = null, $memo_type = null){  
      $this->query_employeeMemo($status,$memo_type);   
      if($memo_type == 'Personnel Action Notice'){
        $query = $this->db->get('tblPANMemoHeader'); 
      }
      else if($memo_type == 'Payroll Inclusion Memo'){
        $query = $this->db->get('tblPIMMemoHeader'); 
      }
      return $query->num_rows();  
  }

  // END 

  // DATA EMPLOYEE MEMO DOCUMENTS TABLE STARTS HERE
  public function table_data_employeeMemoDocuments($status = null){

    $this->db->select('PKFK_Emp_id, ED_DocName, ED_Remarks, ED_DocDestination,\'Download\' as download, CreatedBy, DateCreated, ModifiedBy, DateModified',false);
    $this->db->where('PKFK_Emp_id',$status);    
    $this->db->from('tblEmployeeDocs');

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_employeeMemoDocuments($status);
    $output['num_rows']       = $this->get_filtered_data_employeeMemoDocuments($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_employeeMemoDocuments($status = null){
    
    $this->db->select('PKFK_Emp_id, ED_DocName, ED_Remarks, ED_DocDestination,\'Download\' as  download, CreatedBy, DateCreated, ModifiedBy, DateModified',false);
    $this->db->where('PKFK_Emp_id',$status);    

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("ED_DocName", $_POST["search"]["value"]);  
            $this->db->or_like('ED_Remarks', $_POST["search"]["value"]);  
            $this->db->or_like('ED_DocDestination', $_POST["search"]["value"]);  
            $this->db->or_like('CreatedBy', $_POST["search"]["value"]);  
            $this->db->or_like('ModifiedBy', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_employeeMemoDocuments[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('DateCreated', 'ASC');  
    }
  }

  public function datatables_employeeMemoDocuments($status = null){
      $this->query_employeeMemoDocuments($status); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
       
      $query = $this->db->get('tblEmployeeDocs');  
      return $query->result_array();
  }

  public function get_filtered_data_employeeMemoDocuments($status = null){  
       $this->query_employeeMemoDocuments($status);   
       $query = $this->db->get('tblEmployeeDocs'); 
       return $query->num_rows();  
  }

  // END 

  // DATA EMPLOYEE MOVEMENT TABLE STARTS HERE
  public function table_data_employeemovement($status = null){

    $this->db->select('EM_PKFK_EmpId, EM_AppliedDate, C_Name, P_Position, AD_Desc, EM_BasicRate, 
                 CASE EM_BaseRate WHEN \'D\' THEN \'Daily\' ELSE \'Monthly\' END as EM_BaseRate, ISNULL(EM_CashBond, 0) AS EM_CashBond',false);
    $this->db->join('tblAttributeDetail','AD_Id = EM_Department');
    $this->db->join('tblPosition','P_ID = EM_Position');
    $this->db->join('tblCustomer','C_Id = EM_Principal');
    $this->db->where('EM_PKFK_EmpId',$status);    
    $this->db->from('tblEmployeeMovement');

    $result_count = $this->db->count_all_results();

    $output['data']         = $this->datatables_employeemovement($status);
    $output['num_rows']       = $this->get_filtered_data_employeemovement($status);
    $output['count_all_results']  = $result_count;

    return $output;
  } 

  public function query_employeemovement($status = null){
    
    $this->db->select('EM_PKFK_EmpId, EM_AppliedDate, C_Name, P_Position, AD_Desc, EM_BasicRate, 
                 CASE EM_BaseRate WHEN \'D\' THEN \'Daily\' ELSE \'Monthly\' END as EM_BaseRate, ISNULL(EM_CashBond, 0) AS EM_CashBond',false);
    $this->db->join('tblAttributeDetail','AD_Id = EM_Department');
    $this->db->join('tblPosition','P_ID = EM_Position');
    $this->db->join('tblCustomer','C_Id = EM_Principal');
    $this->db->where('EM_PKFK_EmpId',$status);    

    if(isset($_POST["search"]["value"])){ 
        if($_POST["search"]["value"] != ''){  
            $this->db->like("C_Name", $_POST["search"]["value"]);  
            $this->db->or_like('P_Position', $_POST["search"]["value"]);  
            $this->db->or_like('AD_Desc', $_POST["search"]["value"]);  
            $this->db->or_like('EM_BasicRate', $_POST["search"]["value"]);  
            $this->db->or_like('EM_BaseRate', $_POST["search"]["value"]);  
            $this->db->or_like('EM_CashBond', $_POST["search"]["value"]);  
        }
    }  

    if(isset($_POST["order"])){  
        $this->db->order_by($this->order_column_employeemovement[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
    }  
    else{  
        $this->db->order_by('EM_RecNo', 'ASC');  
    }
  }

  public function datatables_employeemovement($status = null){
      $this->query_employeemovement($status); 
      if(isset($_POST["length"])){
          if($_POST["length"] != -1){  
              $this->db->limit($_POST['length'], $_POST['start']);  
          }  
      }
       
      $query = $this->db->get('tblEmployeeMovement');  
      return $query->result_array();
  }

  public function get_filtered_data_employeemovement($status = null){  
       $this->query_employeemovement($status);   
       $query = $this->db->get('tblEmployeeMovement'); 
       return $query->num_rows();  
  }

  // END 
  
  public function saveEducationChildRecord($data){
    try {

      $result = $this->db->where('EDU_Id',$data['EDU_Id'])
                    ->order_by('EDU_LineNo','DESC')
                    ->limit(1)
                    ->get('tblEmployeeEducation');

      $lineNo = 1;
      if($result->num_rows() > 0){
        $lineNo = $result->row_array()['EDU_LineNo'] + 1;
      }

      // $data['EDU_SchoolYear'] = format_year($data['EDU_SchoolYear']);

      if($data['mode'] == 'add'){
        // insert
        unset($data['mode']);
        $data['EDU_LineNo']  = $lineNo;
        $data['CreatedBy']    = getCurrentUser()['login-user'];
        $data['DateCreated']  = date_format(date_create(),'m/d/Y');
        $this->db->insert('tblEmployeeEducation',$data);
      }
      else{
        // update
        $where = array('EDU_Id' => $data['EDU_Id'],'EDU_LineNo' => $data['EDU_LineNo']);

        unset($data['mode']);
        unset($data['EDU_Id']);
        unset($data['EDU_LineNo']);
        $data['ModifiedBy']    = getCurrentUser()['login-user'];
        $data['DateModified']  = date_format(date_create(),'m/d/Y');
        $this->db->where($where)->update('tblEmployeeEducation',$data);
      }
    } catch (Exception $e) {
      throw new Exception($e->getMessage().", Error , error");
    }
  }

   public function saveNotesDataRecord($data){
     try {

      $result = $this->db->where('EmpN_Id',$data['EmpN_Id'])
                    ->order_by('EmpN_RecNo','DESC')
                    ->limit(1)
                    ->get('tblEmployeeNotes');

      $lineNo = 1;
      if($result->num_rows() > 0){
        $lineNo = $result->row_array()['EmpN_RecNo'] + 1;
      }

      if($data['mode'] == 'add'){
        // insert
        unset($data['mode']);
        $data['EmpN_RecNo']  = $lineNo;
        $data['CreatedBy']    = getCurrentUser()['login-user'];
        $data['DateCreated']  = date_format(date_create(),'m/d/Y');
        $this->db->insert('tblEmployeeNotes',$data);
      }
      else{
        // update
        $where = array('EmpN_Id' => $data['EmpN_Id'],'EmpN_RecNo' => $data['EmpN_RecNo']);

        unset($data['mode']);
        unset($data['EmpN_Id']);
        unset($data['EmpN_RecNo']);
        $data['ModifiedBy']    = getCurrentUser()['login-user'];
        $data['DateModified']  = date_format(date_create(),'m/d/Y');
        $this->db->where($where)->update('tblEmployeeNotes',$data);
      }
    } catch (Exception $e) {
      throw new Exception($e->getMessage().", Error , error");
    }
  }
}