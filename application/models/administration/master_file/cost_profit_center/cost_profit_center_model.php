<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cost_profit_center_model extends MAIN_Model {

    protected $_table = 'tblCPCenter';

    private $id = NULL;
    private $order_column = array(null,null,'CPC_Id','CPC_Desc','CPC_FK_Class','AD_Desc','CASE CPC_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as CPC_Active');

    public function __construct() {
        parent::__construct();
    }

  

    /* datatable query starts here */
    public function table_data($status = null){

        
        $this->db->select('CPC_Id, CPC_Desc, AD_Desc,CASE CPC_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as CPC_Active,');

        $this->db->from('tblCPCenter');
        $this->db->join('tblAttributeDetail', 'AD_Id = CPC_FK_Class', 'left');
        $result_count = $this->db->count_all_results(); 


        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('CPC_Id, CPC_Desc ,AD_Desc,CASE CPC_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as CPC_Active,');
        $this->db->join('tblAttributeDetail', 'AD_Id = CPC_FK_Class', 'left');

        // Individual Column Search

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('CPC_Id', trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('CPC_Desc', trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('AD_Desc', trim($_POST['columns'][4]["search"]["value"]));
        }
        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('CASE CPC_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END', trim($_POST['columns'][5]["search"]["value"]), false);
        }

       
        // End
        
        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){
                $this->db->like("CPC_Id", trim($_POST["search"]["value"])); 
                $this->db->or_like("CPC_Desc", trim($_POST["search"]["value"]));
                $this->db->or_like("AD_Desc", trim($_POST["search"]["value"]));
                $this->db->or_like("CASE CPC_Active WHEN 1 THEN 'Active' ELSE 'Inactive' END", trim($_POST["search"]["value"]), false);        
            }
        }


        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('CPC_Id', 'DESC');  
        }
    }
     public function getAll($doc_no = null){
       $query_string = 'SELECT *
                        FROM tblCPCenter';
        if(!empty($doc_no)){
            $query_string .= ' WHERE ' . $this->sql_hash('CPC_Id') . ' = \''.$doc_no.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();
        }
        return $result;
    }

    public function getAll_Active(){
        $query_string = 'SELECT CPC_Id, CPC_Active, CPC_Desc 
                         FROM tblCPCenter
                         WHERE CPC_Active = \'1\'
                         ORDER BY CPC_Desc';
        return $this->db->query($query_string)->result_array();
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblCPCenter');  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblCPCenter');  
           return $query->num_rows();  
    }
     

    public function getActiveCPCclass(){
        $query_string = 'SELECT AD_FK_Code, AD_Code, AD_Desc, AD_Id
                         FROM tblAttributeDetail AS AD 
                         WHERE AD_Active = \'1\' AND AD_FK_Code = \'102202\' ';
        return $this->db->query($query_string)->result_array();
    }


}
