<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class noseries_model extends MAIN_Model {

    private $id = NULL;

    public function __construct() {
        parent::__construct();
    }

    public function getCurrentNoSeries(){
        $query_string = 'SELECT *
                        FROM tblNoSeries';
        $params = array();
        $result = $this->db->query($query_string, $params);
        return $result->result_array();
    }


    /**
     * Get next number in the number series
     * Params: NSId
     *         isconsume = false 
     */
    public function getNextAvailableNumber($NSId, $isconsume = false, $cUserLoc, $isDash = false) {

        $sql_statement = 'SELECT [NS_Id], [NS_LastNoUsed], [NS_EndingNo]
                          FROM tblNoSeries 
                          WHERE NS_FK_Module_id = ? AND NS_Location '.($cUserLoc == '' ? 'IS NULL' : '= ?');
        $params = ($cUserLoc == '' ? array($NSId) : array($NSId,$cUserLoc));
        $result = $this->db->query($sql_statement, $params)->row_array();
        //return $result;
        if(empty($result)){
            // throw new Exception("Number series ".$NSId." is not found");
            return '';
        }

        $newSeries = $result['NS_LastNoUsed'] + 1;
        if($newSeries > $result["NS_EndingNo"]){
            // throw new Exception("Number series number (". $newSeries .") exceeded the ending number (".$result["NS_EndingNo"].")");
            return '';
        }
        
        if(!$isDash){
            $maskSeries = $result['NS_Id']."-".str_pad($newSeries, strlen((string)$result["NS_EndingNo"]), "0", STR_PAD_LEFT);
        }
        else{
            $maskSeries = $result['NS_Id']."".str_pad($newSeries, strlen((string)$result["NS_EndingNo"]), "0", STR_PAD_LEFT);
        }
        
        if($isconsume){


            $user_id = getCurrentUser()['login-user'];
            $sql_statement = "UPDATE tblNoSeries 
                                SET [NS_LastNoUsed] = ?,
                                [NS_LastDateUsed] = getdate(),
                                [ModifiedBy] = ?
                                WHERE NS_FK_Module_id = ? and NS_Location ".($cUserLoc == '' ? 'IS NULL' : '= ?');
            $params = ($cUserLoc == '' ? array($newSeries, $user_id, $NSId) : array($newSeries, $user_id, $NSId, $cUserLoc));
            $this->db->query($sql_statement, $params);
        }

        return $maskSeries;
    }

    public function getNextAvailableNumberbyEvent($NSId, $isconsume = false, $cUserLoc) {

        $sql_statement = 'SELECT [NS_Id], [NS_LastNoUsed], [NS_EndingNo]
                          FROM tblNoSeries 
                          WHERE NS_FK_Module_id = ? AND NS_Location = ? ';
        $params = array($NSId,$cUserLoc);
        $result = $this->db->query($sql_statement, $params)->row_array();
        
        //return $result;
        if(empty($result)){
            // throw new Exception("Number series ".$NSId." is not found");
            return array('success'=> '0', 'message'=>"No number series found!");
        }

        $newSeries = $result['NS_LastNoUsed'] + 1;
        if($newSeries > $result["NS_EndingNo"]){
            // throw new Exception("Number series number (". $newSeries .") exceeded the ending number (".$result["NS_EndingNo"].")");
            return array('success'=> '0', 'message'=>"Number series number (". $newSeries .") exceeded the ending number (".$result["NS_EndingNo"].")");
        }
        $maskSeries = $result['NS_Id']."-".str_pad($newSeries, strlen((string)$result["NS_EndingNo"]), "0", STR_PAD_LEFT);
        
        if($isconsume){
            
            $user_id = getCurrentUser()['login-user'];
            $sql_statement = "UPDATE tblNoSeries 
                                SET [NS_LastNoUsed] = ?,
                                [NS_LastDateUsed] = getdate(),
                                [ModifiedBy] = ?
                                WHERE NS_FK_Module_id = ? and NS_Location = ? ";
            $params = array($newSeries, $user_id, $NSId, $cUserLoc);
            $this->db->query($sql_statement, $params);
        }

        // return $maskSeries;
        return array('success'=> '1', 'message'=>$maskSeries);
    }

    /**
     * Get next number with consuming
     */
    public function claimNextAvailableNumber($NSId) {

        //  no need for validations as getNextAvailableNumber already validates
        $availableFormattedNumber = $this->getNextAvailableNumber($NSId);

        $NSRow = $this->db->where(array("NS_Id" => $NSId))->get($this->table)->row_array();

        $lastNumberUsed = $NSRow["NS_LastNoUsed"];
        $claimedNumber  = $lastNumberUsed + 1;

        $userID = $this->session->userdata("U_ID");
        $dateTime = date("Y-m-d H:i:s", time());

        //  update the number series row
        $NSUpdate = array(
            "NS_LastNoUsed"   => $claimedNumber,
            "NS_LastDateUsed" => date("Y-m-d"),
            "NS_Trigger"      => 'click-' . $claimedNumber . '-' . $userID . "-" . $dateTime
        );

        $this->update_data(array("NS_Id" => $NSId), $NSUpdate);

        return $availableFormattedNumber;
    }
}
