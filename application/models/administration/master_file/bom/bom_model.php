<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class bom_model extends MAIN_Model {

    protected $_table = 'tblBOM';

    private $id = NULL;
    private $order_column = array(null,'BOM_DocNo','BOM_ItemNo','BOM_BOMDescription','Yield','BOM_Status', 'BOM_Remarks');

    public function __construct() {
        parent::__construct();
    }

    public function getAll($doc_no){
       $query_string = 'SELECT BOM.BOM_DocNo, BOM.BOM_DocDate, BOM.BOM_BOMDescription, BOM.BOM_ItemNo, I.IM_Sales_Desc, BOM.BOM_Version, 
                        BOM.BOM_YieldQty, BOM.BOM_YieldUomID, AD2.AD_Code AS YieldUOM, 
                         BOM.BOM_BatchQty, BOM.BOM_BatchUomID, AD1.AD_Code AS BatchUOM, BOM.BOM_Remarks, BOM.BOM_Status, BOM.CreatedBy, BOM.DateCreated, BOM.ModifiedBy, BOM.DateModified, 
                         BOM.EntryNo, BOM.BOM_Location, SP.SP_StoreName
                        FROM  tblItem AS I RIGHT OUTER JOIN
                         tblBOM AS BOM 
                         LEFT OUTER JOIN tblAttributeDetail AS AD2 ON BOM.BOM_YieldUomID = AD2.AD_Id 
                         LEFT OUTER JOIN tblAttributeDetail AS AD1 ON BOM.BOM_BatchUomID = AD1.AD_Id ON I.IM_Item_Id = BOM.BOM_ItemNo
                         LEFT OUTER JOIN tblStoreProfile AS SP ON SP.SP_ID = BOM_Location
                        ';
        if(!empty($doc_no)){
            $query_string .= ' WHERE '.$this->sql_hash('BOM_DocNo').' = \''.$doc_no.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();   
        }
        return $result;
    }

    public function getAllActive(){
       $query_string = 'SELECT *
                        FROM tblBOM
                        WHERE BOM_Status = \'Active\'';
        $params = array();
        $result = $this->db->query($query_string, $params);
        return $result->result_array();
    }

    public function getBOMList(){
         $query_string = 'SELECT tblBOM.*, attrDetail.AD_Code, attrDetail.AD_Desc
                        FROM tblBOM
                        LEFT JOIN tblAttributeDetail attrDetail
                        ON BOM_BatchUomID = AD_Id';
        $params = array();
        $result = $this->db->query($query_string, $params);
        return $result->result_array();
    }

    public function getByBOMDocNo($DocNo){
       $query_string = 'SELECT *
                        FROM tblBOM
                        WHERE [BOM_DocNo] = ?';
        $params = array($DocNo);
        $result = $this->db->query($query_string, $params);
        return $result->row_array();
    }

    public function getCountByBOMDescriptionBomNo($Description, $bom = false){
        $additional_where = ($bom)? 'AND BOM_DocNo = \''.$bom.'\'' : '';
        $query_string = 'SELECT TOP 1 [BOM_DocNo]
                        FROM tblBOM
                        WHERE [BOM_BOMDescription] = ? '.$additional_where;
        $params = array($Description);
        $result = $this->db->query($query_string, $params);
        return $result->row_array();
    }
    
    public function getByBOMDocNoWithUOM($DocNo){
       $query_string = 'SELECT tblBOM.*, batch.AD_Code b_code, batch.AD_Desc b_desc, yield.AD_Code y_code, yield.AD_Desc y_desc
                        FROM tblBOM
                        LEFT JOIN tblAttributeDetail batch
                        ON BOM_BatchUomID = batch.AD_Id
                        LEFT JOIN tblAttributeDetail yield
                        ON BOM_YieldQty = yield.AD_Id
                        WHERE [BOM_DocNo] = ?';
        $params = array($DocNo);
        $result = $this->db->query($query_string, $params);
        return $result->row_array();
    }

    public function status_data($itemno, $action, $version){
        $this->db->trans_begin();
            
        if($action == 'active'){
            $status = array('Active','Approved','Inactive');
            $this->db->where(array('BOM_ItemNo'=>$itemno, 'BOM_Status' => 'Active'))->update('tblBOM',array('BOM_Status'=>'Inactive'));
            $this->db->where(array('BOM_ItemNo'=>$itemno,'BOM_Version'=>$version))->update('tblBOM',array('BOM_Status'=>'Active'));
        }
        else{
            $this->db->where(array('BOM_ItemNo'=>$itemno,'BOM_Version'=>$version))->update('tblBOM',array('BOM_Status'=>'Inactive'));
        }
       
        if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return array('success'=>0);
        }
        else{
                $this->db->trans_commit();
                return array('success'=>1);
        }

    }


    /* datatable query starts here */
    public function table_data($hasAccess = false){

        
        $this->db->select('BOM_DocNo, BOM_DocDate, BOM_BOMDescription, BOM_ItemNo, BOM_Version, BOM_YieldQty,
                            (CAST(CAST(ROUND(BOM_YieldQty,2) AS NUMERIC(30,2)) AS VARCHAR) + \' / \' + AD_Code) AS Yield, 
                            BOM_YieldUomID, BOM_BatchQty, BOM_BatchUomID, BOM_Remarks, BOM_Status, EntryNo, AD_Code');
        if(!$hasAccess){
            $this->db->where('"tblBOM"."CreatedBy"',getCurrentUser()['login-user']);
        }
        $this->db->join('tblAttributeDetail','AD_Id = BOM_YieldUomID','left');
        $this->db->from('tblBOM'); 
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables($hasAccess);
        $output['num_rows']          = $this->get_filtered_data($hasAccess);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query($hasAccess = false){
        $this->db->select('BOM_DocNo, BOM_DocDate, BOM_BOMDescription, BOM_ItemNo, BOM_Version, BOM_YieldQty,
                            (CAST(CAST(ROUND(BOM_YieldQty,2) AS NUMERIC(30,2)) AS VARCHAR) + \' / \' + AD_Code) AS Yield, 
                            BOM_YieldUomID, BOM_BatchQty, BOM_BatchUomID, BOM_Remarks, BOM_Status, EntryNo, AD_Code');

        // Individual Column Search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('BOM_DocNo',$_POST['columns'][1]["search"]["value"]);
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('BOM_ItemNo',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('BOM_BOMDescription',$_POST['columns'][3]["search"]["value"]);
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('AD_Code',$_POST['columns'][4]["search"]["value"]);
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('BOM_Status',$_POST['columns'][5]["search"]["value"]);
        }

        if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
            $this->db->like('BOM_Remarks',$_POST['columns'][6]["search"]["value"]);
        }

        // End
        
        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){
                $this->db->like("BOM_DocNo", $_POST["search"]["value"]);  
                $this->db->or_like("BOM_BOMDescription", $_POST["search"]["value"]); 
                $this->db->or_like("BOM_ItemNo", $_POST["search"]["value"]);  
                $this->db->or_like("BOM_Status", $_POST["search"]["value"]);  
                $this->db->or_like("BOM_Remarks", $_POST["search"]["value"]);              
            }
        }

        if(!$hasAccess){
            $this->db->where('"tblBOM"."CreatedBy"',getCurrentUser()['login-user']);
        }

        $this->db->join('tblAttributeDetail','AD_Id = BOM_YieldUomID','left');

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('BOM_DocNo', 'DESC');  
        }
    }

    public function datatables($hasAccess = false){
        $this->query($hasAccess); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblBOM');  
        return $query->result_array();
    }

    public function get_filtered_data($hasAccess = false){  
           $this->query($hasAccess);  
           $query = $this->db->get('tblBOM');  
           return $query->num_rows();  
    }

    /* datatable query ends here */ 

    public function checkAllBOMSetup($itemNo){
        $blnValidate = true;    

        $where = array(
            'BOM_ItemNo' => $itemNo,
            'BOM_Status' => 'Active',
        );

        $result = $this->db->where($where)->get($this->_table)->num_rows();
        if($result == 0){
            $blnValidate = false;
        }


        return $blnValidate;
    }

    public function getBOMHeader($bomItemNo_List){
        $itemNo = '(\''.implode('\',\'', array_column($bomItemNo_List,'item-no')).'\')';
        $sql_query = 'SELECT        BOM.BOM_DocNo, BOM.BOM_BOMDescription, BOM.BOM_ItemNo, BOM.BOM_Version, BOM.BOM_YieldQty, BOM.BOM_YieldUomID, 
                                    BOM.BOM_BatchQty, BOM.BOM_BatchUomID, I.IM_FK_Attribute_UOM_id, ISNULL(IUC.IUC_Quantity, 0) AS BaseUOMQty
                     FROM            tblBOM AS BOM LEFT OUTER JOIN
                                     tblItemUOMConv AS IUC ON BOM.BOM_ItemNo = IUC.IUC_FK_Item_id AND BOM.BOM_YieldUomID = IUC.IUC_FK_UOM_id LEFT OUTER JOIN
                                     tblItem AS I ON BOM.BOM_ItemNo = I.IM_Item_Id
                     WHERE (BOM.BOM_ItemNo IN '.$itemNo.') AND (BOM.BOM_Status = \'Active\')
                     ORDER BY BOM.BOM_BOMDescription';
                     
        $result = $this->db->query($sql_query)->result_array();

        return $result;
    }


    public function getBOMDetail($bomNo){
        $sql_query = 'SELECT BOMD.BOMD_BOM_DocNo, BOMD.BOMD_LineNo, BOMD.BOMD_ItemNo, BOMD.BOMD_Barcode, BOMD.BOMD_Qty, BOMD.BOMD_UomID, 
                             BOMD.BOMD_AverageCost, BOMD.BOMD_UnitCost, BOMD.BOMD_TotalCost, BOMD.BOMD_Waste, BOMD.BOMD_RequiredQty, BOMD.BOMD_Comment, 
                             BOMD.BOMD_BaseUomID, BOMD.BOMD_BaseUomQty, I.IM_FK_Attribute_UOM_id
                      FROM tblBOMDetail AS BOMD LEFT OUTER JOIN tblItem AS I ON BOMD.BOMD_ItemNo = I.IM_Item_Id
                      WHERE (BOMD.BOMD_BOM_DocNo = \''.$bomNo.'\')';
        $result = $this->db->query($sql_query)->result_array();
        return $result;
    }

    public function getBOMHeaderSpices($bomNo){
        $sql_query = 'SELECT        BOM.BOM_DocNo, BOM.BOM_BOMDescription, BOM.BOM_ItemNo, BOM.BOM_Version, BOM.BOM_YieldQty, BOM.BOM_YieldUomID, 
                                    BOM.BOM_BatchQty, BOM.BOM_BatchUomID, I.IM_FK_Attribute_UOM_id, ISNULL(IUC.IUC_Quantity, 0) AS BaseUOMQty
                     FROM            tblBOM AS BOM LEFT OUTER JOIN
                                     tblItemUOMConv AS IUC ON BOM.BOM_ItemNo = IUC.IUC_FK_Item_id AND BOM.BOM_YieldUomID = IUC.IUC_FK_UOM_id LEFT OUTER JOIN
                                     tblItem AS I ON BOM.BOM_ItemNo = I.IM_Item_Id
                      WHERE (BOM.BOM_ItemNo = \''.$bomNo.'\') AND (BOM.BOM_Status = \'Active\')
                      ORDER BY BOM.BOM_BOMDescription';
        
        $result = $this->db->query($sql_query)->result_array();

        return $result;
    }

    public function checkIfUsedByMO($bom_docno){
        $query_string = 'SELECT        MOD.MOD_BOM_DocNo, MO.MO_Status
                        FROM            tblManufacturingOrderDetail AS MOD LEFT OUTER JOIN
                                                 tblManufacturingOrder AS MO ON MOD.MOD_MO_DocNo = MO.MO_DocNo
                        WHERE        (MO.MO_Status = \'Open\') AND (MOD.MOD_BOM_DocNo = \''.$bom_docno.'\')';

        return $this->db->query($query_string)->num_rows();
    }

    public function checkIfUsedByOF($itemNo){
        $query_string = 'SELECT        OFD.OFD_IM_ItemNo, OrdF.OF_Status
                        FROM            tblOrderForecastDetail AS OFD LEFT OUTER JOIN
                                                 tblOrderForecast AS OrdF ON OFD.OFD_OF_DocNo = OrdF.OF_DocNo
                        WHERE        (OrdF.OF_Status = \'Open\') AND (OFD.OFD_IM_ItemNo = \''.$itemNo.'\')';

        return $this->db->query($query_string)->num_rows();
    }

    public function checkIfUsedByPF($itemNo){
        $query_string = 'SELECT        PFDS.PFSDS_ItemNo, PF.PF_Status
                        FROM            tblProductionForecastSubDetailSummary AS PFDS LEFT OUTER JOIN
                                        tblProductionForecast AS PF ON PFDS.PFSDS_PFD_PF_DocNo = PF.PF_DocNo
                        WHERE        (PF.PF_Status = \'Open\') AND (PFDS.PFSDS_ItemNo= \''.$itemNo.'\')';

        return $this->db->query($query_string)->num_rows();
    }


}
