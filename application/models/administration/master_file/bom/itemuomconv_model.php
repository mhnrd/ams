<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class itemuomconv_model extends MAIN_Model {

    private $id = NULL;

    public function __construct() {
        parent::__construct();
    }

    public function getAllByItemID($item_id){
       $query_string = 'SELECT [AD_Id], [AD_Code], [AD_Desc], [IUC_Quantity]
						FROM tblItemUOMConv
						INNER JOIN [tblAttributeDetail]
						ON [tblItemUOMConv].[IUC_FK_UOM_id] = [tblAttributeDetail].[AD_Id]
						WHERE [IUC_FK_Item_id] = ?';
        $params = array($item_id);
        $result = $this->db->query($query_string, $params);
        return $result->result_array();
    }

    public function getBaseUOMbyItemID($item_id){
       $query_string = 'SELECT [AD_Id], [AD_Code], [AD_Desc], [IUC_Quantity]
                        FROM tblItemUOMConv
                        INNER JOIN [tblAttributeDetail]
                        ON [tblItemUOMConv].[IUC_FK_UOM_id] = [tblAttributeDetail].[AD_Id]
                        WHERE [IUC_Quantity] = 1 AND [IUC_FK_Item_id] = ? ';
        $params = array($item_id);
        $result = $this->db->query($query_string, $params);
        return $result->row_array();
    }

}