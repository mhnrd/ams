<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class item_model extends MAIN_Model {

    protected $_table = 'tblItem';
    public function __construct() {
        parent::__construct();
    }

    public function getAll($itemNo = null, $type = null){
        $query_string = 'SELECT [IM_Item_Id], [IM_UPCCode], [IM_Sales_Desc], [IM_UnitCost], [IM_Active] , [AD_Code]
                        FROM tblItem
                            LEFT OUTER JOIN tblAttributeDetail ON IM_FK_Attribute_UOM_id = AD_Id ';
        if(!empty($itemNo)){

            $query_string .= ' WHERE IM_Item_Id = \''.$itemNo.'\' ';
            $query_string .= ' ORDER BY [IM_Sales_Desc]';
            if($type == null){
                $result = $this->db->query($query_string)->row_array();
            }
            else{
                $result = $this->db->query($query_string)->result_array();   
            }
        }
        else{
            $query_string .= ' ORDER BY [IM_Sales_Desc]';
            $result = $this->db->query($query_string)->result_array();   
        }
        return $result;
    }

    public function getAllbyID($item_id){
        $sql_query = 'SELECT I.*, I2.IM_Sales_Desc as ScrapParentDesc FROM tblItem as I LEFT OUTER JOIN tblItem as I2 ON I.IM_Scrap_Item_Id = I2.IM_Item_id WHERE '.$this->sql_hash('I.IM_Item_Id').' =  ? ';
        $params = array($item_id);
        $result = $this->db->query($sql_query,$params)->row_array();
        return $result;
    }

    public function getAllBySearch($type, $filter, $cat_id = null){
        $where_clause = '';
        $where_clause .= "WHERE ".$type." LIKE '%". $filter ."%' " ;

        if($cat_id != ''){
            $where_clause .= ' AND IM_FK_Category_id = \''.$cat_id.'\' ';
        }

        $query_string = 'SELECT [IM_Item_Id], [IM_UPCCode], [IM_Sales_Desc], [IM_UnitCost], [IM_Item_Waste], [IM_FK_ItemType_id]
                        FROM tblItem
                        '.$where_clause.'
                        ORDER BY [IM_Sales_Desc]';
        
        $result = $this->db->query($query_string)->result_array();

        foreach ($result as $key => $value) {
            $result[$key]['IM_Version'] = $this->db->where('BOM_ItemNo',$result[$key]['IM_Item_Id'])->get('tblBOM')->num_rows() + 1;
        }

        return $result;
    }

    public function getByFilter($type, $filter , $cat_id = null){

        $query_string = 'SELECT [IM_Item_Id], [IM_UPCCode], [IM_Sales_Desc], [IM_UnitCost], [IM_Item_Waste], [IM_FK_ItemType_id]
                        FROM tblItem
                        WHERE '.$type.' = ? ';
        
        if($cat_id != ''){
            $query_string .= ' AND IM_FK_Category_id = \''.$cat_id.'\' ';
        }

        $query_string .= ' ORDER BY [IM_Sales_Desc]';
        $params = array($filter);
        $result = $this->db->query($query_string, $params)->row_array();

        // Added by Jekzel Leonidas for getting the version # per selected item
        // $result['IM_Version'] = $this->db->where('BOM_ItemNo',$result['IM_Item_Id'])->get('tblBOM')->num_rows() + 1;

        return $result;
    }

    public function getAllBySearchNotScrap($type, $filter, $cat_id = null){
        $where_clause = '';
        $where_clause .= "WHERE ".$type." LIKE '%". $filter ."%' " ;

        if($cat_id != '' || $cat_id != null){
            $where_clause .= ' AND IM_FK_Category_id = \''.$cat_id.'\' ';
        }

        $where_clause .= ' AND IM_Scrap = 0';

        $query_string = 'SELECT [IM_Item_Id], [IM_UPCCode], [IM_Sales_Desc], [IM_UnitCost], [IM_Item_Waste], [IM_FK_ItemType_id]
                        FROM tblItem
                        '.$where_clause.'
                        ORDER BY [IM_Sales_Desc]';


        $result = $this->db->query($query_string)->result_array();

        foreach ($result as $key => $value) {
            $result[$key]['IM_Version'] = $this->db->where('BOM_ItemNo',$result[$key]['IM_Item_Id'])->get('tblBOM')->num_rows() + 1;
        }
        
        return $result;
    }

    public function getByFilterNotScrap($type, $filter , $cat_id = null){

        $query_string = 'SELECT [IM_Item_Id], [IM_UPCCode], [IM_Sales_Desc], [IM_UnitCost], [IM_Item_Waste], [IM_FK_ItemType_id]
                        FROM tblItem
                        WHERE '.$type.' = ? AND IM_Scrap = 0';
        
        if($cat_id != '' || $cat_id != null){
            $query_string .= ' AND IM_FK_Category_id = \''.$cat_id.'\' ';
        }

        $query_string .= ' ORDER BY [IM_Sales_Desc]';
        $params = array($filter);
        $result = $this->db->query($query_string, $params)->row_array();

        // Added by Jekzel Leonidas for getting the version # per selected item
        // $result['IM_Version'] = $this->db->where('BOM_ItemNo',$result['IM_Item_Id'])->get('tblBOM')->num_rows() + 1;

        return $result;
    }

    public function getByItemID($item_id){
       $query_string = 'SELECT *
                        FROM tblItem
                        WHERE [IM_Item_Id] = ?';
        $params = array($item_id);
        $result = $this->db->query($query_string, $params);
        return $result->row_array();
    }

    public function getLastPOCost($item_id){
        return $this->db->select('IL_UnitPrice')
                        ->where('IL_ItemNo',$item_id)
                        ->order_by('IL_EntryNo','DESC')
                        ->limit(1)
                        ->get('tblItemLedger')->row_array()['IL_UnitPrice'];
    }

    public function getSOH($itemNo){
        // $sql_query = 'SELECT ISNULL(SUM(IL_Qty),0) AS "IL_Qty" FROM tblItemLedger WHERE IL_ItemNo = ? AND IL_Location = ? ';
        // $params = array($itemNo,getDefaultLocation());
        $sql_query = 'SELECT ISNULL(SUM(IL_Qty),0) AS "IL_Qty" FROM tblItemLedger WHERE IL_ItemNo = ? ';
        $params = array($itemNo);
        return $this->db->query($sql_query,$params)->row_array()['IL_Qty'];
    }
}