<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class bomdetail_model extends MAIN_Model {

    private $id = NULL;
    protected $_table = 'tblBOMDetail';

    private $order_column_bom = array('BOMD_ItemNo', 'IM_Sales_Desc', 'BOMD_Qty', 'AD_Code', 'BOMD_UnitCost', 'BOMD_TotalCost', 'BOMD_Waste', 'BOMD_RequiredQty', 'BOMD_Comment');


    public function __construct() {
        parent::__construct();
    }

    public function getCurrentLineByDocNo($DocNo){
       $query_string = 'SELECT ISNULL(MAX([BOMD_LineNo]), 0) as currentNo
                        FROM tblBOMDetail
                        WHERE [BOMD_BOM_DocNo] = ?';
        $params = array($DocNo);
        $result = $this->db->query($query_string, $params);
        return $result->row_array();
    }

    public function getAllByDocNo($DocNo){
       $query_string = 'SELECT tblBOMDetail.*, tblItem.IM_Sales_Desc, tblAttributeDetail.AD_Code AS BOMD_UOM, tblAttributeDetail.AD_Desc
                        FROM            tblBOMDetail LEFT OUTER JOIN
                                        tblAttributeDetail ON tblBOMDetail.BOMD_UomID = tblAttributeDetail.AD_Id LEFT OUTER JOIN
                                        tblItem ON tblBOMDetail.BOMD_ItemNo = tblItem.IM_Item_Id 
                        WHERE ' .$this->sql_hash('BOMD_BOM_DocNo'). '= ?
                        ORDER BY tblBOMDetail.BOMD_LineNo';
        // $query_string = 'SELECT tblBOMDetail.*, attrDetail.AD_Code, attrDetail.AD_Desc
        //                 FROM tblBOMDetail
        //                 LEFT JOIN tblAttributeDetail attrDetail
        //                 ON BOMD_UOM = AD_Id 
        //                 WHERE AD_FK_Code = \'1401\' AND [BOMD_BOM_DocNo] = ?';
        $params = array($DocNo);
        $result = $this->db->query($query_string, $params);
        return $result->result_array();

    }

    public function getAllByDocNoWithItemCat($DocNo){
       $query_string = 'SELECT tblBOMDetail.*, attrDetail.AD_Code, attrDetail.AD_Desc, tblItemCat.IC_Type
                        FROM tblBOMDetail
                        LEFT JOIN tblAttributeDetail attrDetail
                        ON BOMD_UOM = AD_Id 
                        LEFT JOIN tblItemCat
                        ON IC_ItemNo = BOMD_ItemNo
                        WHERE [BOMD_BOM_DocNo] = ?';
        // $query_string = 'SELECT tblBOMDetail.*, attrDetail.AD_Code, attrDetail.AD_Desc
        //                 FROM tblBOMDetail
        //                 LEFT JOIN tblAttributeDetail attrDetail
        //                 ON BOMD_UOM = AD_Id 
        //                 WHERE AD_FK_Code = \'1401\' AND [BOMD_BOM_DocNo] = ?';
        $params = array($DocNo);
        $result = $this->db->query($query_string, $params);
        return $result->result_array();
    }

    public function getByDocNoLineNo($DocNo, $LineNo){
        $query_string = 'SELECT tblBOMDetail.*
                        FROM tblBOMDetail 
                        WHERE [BOMD_BOM_DocNo] = ? AND [BOMD_LineNo] = ?';
        // $query_string = 'SELECT tblBOMDetail.*, attrDetail.AD_Code, attrDetail.AD_Desc
        //                 FROM tblBOMDetail
        //                 LEFT JOIN tblAttributeDetail attrDetail
        //                 ON BOMD_UOM = AD_Id 
        //                 WHERE AD_FK_Code = \'1401\' AND [BOMD_BOM_DocNo] = ?';
        $params = array($DocNo, $LineNo);
        $result = $this->db->query($query_string, $params);
        return $result->row_array();
    }

    public function getTotalByDocNo($DocNo){
        $query_string = 'SELECT SUM(BOMD_TotalCost) as \'total\'
                        FROM tblBOMDetail
                        WHERE BOMD_BOM_DocNo = ?';
        $params = array($DocNo);
        $result = $this->db->query($query_string, $params);
        return $result->row_array();
    }

    public function insert($data){
       return $this->db->insert('tblBOMDetail', $data);
    }

    public function update($data, $filter){
        $this->db->where($filter);

        return $this->db->update('tblBOMDetail', $data);
    }

    public function delete($filter){
        $this->db->where($filter);
        return $this->db->delete('tblBOMDetail');
    }

    // Datatable BOM Details query starts here

    public function table_data_bomdetail($doc_no){

        $this->db->select('BOMD_ItemNo, IM_Sales_Desc, BOMD_Qty, BOMD_UomID, AD_Code, BOMD_UnitCost, BOMD_TotalCost, BOMD_Waste, BOMD_RequiredQty, BOMD_Comment');
        $this->db->from('tblBOMDetail');
        $this->db->join('tblAttributeDetail', 'BOMD_UomID = AD_Id' ,'left');
        $this->db->join('tblItem', 'BOMD_ItemNo = IM_Item_Id', 'left');
        $this->db->where('BOMD_BOM_DocNo', $doc_no); 

        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables_bomdetail($doc_no);
        $output['num_rows']          = $this->get_filtered_data_bomdetail($doc_no);
        $output['count_all_results'] = $result_count;

        

        return $output;
    }

    public function query_bomdetail($doc_no){

        $this->db->select('BOMD_ItemNo, BOMD_LineNo, IM_Sales_Desc, BOMD_Qty, BOMD_UomID, AD_Code, BOMD_UnitCost, BOMD_TotalCost, BOMD_Waste, BOMD_RequiredQty, BOMD_Comment');
        $this->db->join('tblAttributeDetail', 'BOMD_UomID = AD_Id' ,'left');
        $this->db->join('tblItem', 'BOMD_ItemNo = IM_Item_Id', 'left');
        $this->db->where('BOMD_BOM_DocNo', $doc_no); 

        // Individual Column Search

        if(isset($_POST['columns'][0]["search"]["value"]) && !empty($_POST['columns'][0]["search"]["value"])){
            $this->db->like('BOMD_ItemNo',$_POST['columns'][0]["search"]["value"]);
        }

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('IM_Sales_Desc',$_POST['columns'][1]["search"]["value"]);
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('BOMD_Qty',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('AD_Code',$_POST['columns'][3]["search"]["value"]);
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('BOMD_UnitCost',$_POST['columns'][4]["search"]["value"]);
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('BOMD_TotalCost',$_POST['columns'][5]["search"]["value"]);
        }

        if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
            $this->db->like('BOMD_Waste',$_POST['columns'][6]["search"]["value"]);
        }

        if(isset($_POST['columns'][7]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $this->db->like('BOMD_RequiredQty',$_POST['columns'][7]["search"]["value"]);
        }

        if(isset($_POST['columns'][8]["search"]["value"]) && !empty($_POST['columns'][8]["search"]["value"])){
            $this->db->like('BOMD_Comment',$_POST['columns'][8]["search"]["value"]);
        }

        // End

        if(isset($_POST["search"]["value"])){  
            if($_POST["search"]["value"] != ''){
                $this->db->group_start();
                $this->db->like("BOMD_ItemNo", $_POST["search"]["value"]);  
                $this->db->or_like("IM_Sales_Desc", $_POST["search"]["value"]);              
                $this->db->or_like("BOMD_Qty", $_POST["search"]["value"]);              
                $this->db->or_like("AD_Code", $_POST["search"]["value"]);              
                $this->db->or_like("BOMD_UnitCost", $_POST["search"]["value"]);              
                $this->db->or_like("BOMD_TotalCost", $_POST["search"]["value"]);              
                $this->db->or_like("BOMD_Waste", $_POST["search"]["value"]);              
                $this->db->or_like("BOMD_RequiredQty", $_POST["search"]["value"]);              
                $this->db->or_like("BOMD_Comment", $_POST["search"]["value"]);                            
                $this->db->group_end();
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column_bom[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('BOMD_LineNo', 'DESC');  
        }
    }

    public function datatables_bomdetail($doc_no){
        $this->query_bomdetail($doc_no); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblBOMDetail');  
        return $query->result_array();
    }

    public function get_filtered_data_bomdetail($doc_no){
        $this->query_bomdetail($doc_no);  
        $query = $this->db->get('tblBOMDetail');  
        return $query->num_rows(); 
    }
    public function getBOMDetails($doc_no){
        $query_string = 'SELECT     BOMD.BOMD_BOM_DocNo, BOMD.BOMD_ItemNo, I.IM_Sales_Desc, BOMD.BOMD_Qty, AD.AD_Desc, BOMD.BOMD_UnitCost,
                                    BOMD.BOMD_TotalCost, BOMD.BOMD_Waste, BOMD.BOMD_RequiredQty, BOMD.BOMD_Comment
                        FROM        tblBOMDetail AS BOMD LEFT OUTER JOIN
                                    tblAttributeDetail AS AD ON BOMD.BOMD_UomID = AD.AD_Id LEFT OUTER JOIN
                                    tblItem AS I ON BOMD.BOMD_ItemNo = I.IM_Item_Id
                         WHERE      '.$this->sql_hash('BOMD.BOMD_BOM_DocNo').' = \''.$doc_no.'\'';
        return $this->db->query($query_string)->result_array();
    }


    // Datatable BOM Details query ends here
    


}
