<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class user_model extends MAIN_Model {

    private $id = NULL;

    public function __construct() {
        parent::__construct();
    }

    public function getUserByUsernameAndPassword($username, $password){
       $query_string = 'SELECT  U_ID, U_Username, U_Password, U_FK_Position_id, U_AssociatedEmployeeId, Emp_FK_StoreID, U_ParentUserID, P_Position
                        FROM    tblUser LEFT OUTER JOIN
                                tblEmployee ON U_AssociatedEmployeeId = Emp_Id LEFT OUTER JOIN 
                                tblPosition ON U_FK_Position_id = P_ID
                        WHERE   U_ID = \''.$username.'\' AND U_Password = \''.$password.'\'';
        $query = $this->db->query($query_string);
        return $query->row_array();
    }

    public function validateOldPassword($username, $password){
      $result = $this->db->where('U_ID',$username)
                          ->where('U_Password',$password)
                          ->get('tblUser')->num_rows();
      return ($result > 0) ? true : false ;
    }

    public function updatePassword($username, $password){
        $this->db->where('U_ID',$username)->set('U_Password',$password)->update('tblUser');
    }

    public function getAllBySearch($filter){
        $where_clause = '';
        $where_clause .= "[U_ID] LIKE '%". $filter ."%' OR [U_Username] LIKE '%" . $filter . "%' OR [CA_FK_Location_id] LIKE '%" . $filter . "%' ";

        $query_string = 'SELECT [U_ID], [U_Username], [CA_FK_Location_id]
                       FROM tblUser
                       INNER JOIN tblCompanyAccess
                       ON [CA_U_ID] = [U_ID] 
                       WHERE CA_DefaultLocation = \'1\'
                        AND CA_Active = \'1\' AND (
                        '.$where_clause.' )
                        ORDER BY [U_Username]';
        $params = array();
        $result = $this->db->query($query_string, $params);
        return $result->result_array();
    }

    public function getByFilter($filter){

        $query_string = 'SELECT [U_ID], [U_Username], [CA_FK_Location_id]
                       FROM tblUser
                       INNER JOIN tblCompanyAccess
                       ON [CA_U_ID] = [U_ID] 
                       WHERE CA_DefaultLocation = \'1\'
                        AND CA_Active = \'1\' AND [U_ID] = ?
                        ORDER BY [IM_Sales_Desc]';
        $params = array($filter);
        $result = $this->db->query($query_string, $params);
        return $result->row_array();
    }

    public function getAllModules($username){
      $sql_query = 'SELECT RA.RA_FK_Modules_Code AS UP_FK_Module_id, LTRIM(RTRIM(M.M_DisplayName)) AS M_Description, M.M_Menu, M.M_Icon, M.M_Trigger, M.M_ControllerName
                    FROM tblModule AS M 
                      RIGHT OUTER JOIN tblRoleAccess AS RA ON M.M_Module_id = RA.RA_FK_Modules_Code 
                      RIGHT OUTER JOIN tblUserRole AS R ON RA.RA_ID = R.UR_FK_RoleID
                    WHERE  (R.UR_FK_UserID = ?) AND (RA.RA_Menu = 1)
                    ORDER BY M.M_Sequence';       
      $params = array($username);
      $result = $this->db->query($sql_query, $params);
      return $result->result_array(); 
    }

    public function getModulesPerParent($username,$parentModuleID){
      $sql_query = 'SELECT RA.RA_FK_Modules_Code AS UP_FK_Module_id, LTRIM(RTRIM(M.M_DisplayName)) AS M_Description, M.M_Menu, M.M_Icon, M.M_Trigger, M.M_ControllerName
                    FROM tblModule AS M 
                      RIGHT OUTER JOIN tblRoleAccess AS RA ON M.M_Module_id = RA.RA_FK_Modules_Code 
                      RIGHT OUTER JOIN tblUserRole AS R ON RA.RA_ID = R.UR_FK_RoleID
                    WHERE  (R.UR_FK_UserID = ?) AND (M.M_WebParent = ?) AND (RA.RA_Menu = 1)
                    ORDER BY M.M_Sequence';                     
      $params = array($username,$parentModuleID);
      $result = $this->db->query($sql_query, $params);
      return $result->result_array();
    }

    public function getModulesPerParentESS($username,$parentModuleID){
      $sql_query = 'SELECT RA.RA_FK_Modules_Code AS UP_FK_Module_id, LTRIM(RTRIM(M.M_DisplayName)) AS M_Description, M.M_Menu, M.M_Icon, \'modules/\' + M.M_ControllerName AS M_Trigger, M.M_ControllerName
                    FROM tblModule AS M 
                      RIGHT OUTER JOIN tblRoleAccess AS RA ON M.M_Module_id = RA.RA_FK_Modules_Code 
                      RIGHT OUTER JOIN tblUserRole AS R ON RA.RA_ID = R.UR_FK_RoleID
                    WHERE  (R.UR_FK_UserID = ?) AND (M.M_WebParent = ?) AND (RA.RA_Menu = 1)
                    ORDER BY M.M_Sequence';                    
      $params = array($username,$parentModuleID);
      $result = $this->db->query($sql_query, $params);
      return $result->result_array();
    }

    public function getHeaderAccess($moduleId){

      // Added by Jekzel for Correction for disrupt Session
      if(!isset($_SESSION['current_user'])){
        $this->redirect(DOMAIN.'user/login');
      }

      $sql_query = 'SELECT        CASE WHEN SUM(CAST(RA.RA_Add AS INT)) > 0 THEN 1 ELSE 0 END AS RA_Add,
                                  CASE WHEN SUM(CAST(RA.RA_Edit AS INT)) > 0 THEN 1 ELSE 0 END AS RA_Edit,
                                  CASE WHEN SUM(CAST(RA.RA_Edit AS INT)) > 0 THEN 1 ELSE 0 END AS RA_Print,
                                  CASE WHEN SUM(CAST(RA.RA_View AS INT)) > 0 THEN 1 ELSE 0 END AS RA_View,
                                  CASE WHEN SUM(CAST(RA.RA_Delete AS INT)) > 0 THEN 1 ELSE 0 END AS RA_Delete
                    FROM          tblRoleAccess AS RA
                    WHERE         (RA.RA_ID IN
                                               (SELECT        UR_FK_RoleID
                                                 FROM            tblUserRole
                                                 WHERE        (UR_FK_UserID = \''.getCurrentUser()['login-user'].'\'))) AND 
                                  (RA.RA_FK_Modules_Code = '.$moduleId.')
                    GROUP BY    RA.RA_FK_Modules_Code';
      $params = array(getCurrentUser()['login-user']);
      return $this->db->query($sql_query,$params)->row_array();
    }

    public function getDetailAccess($moduleId){
      if(!isset($_SESSION['current_user'])){
        $this->redirect(DOMAIN.'user/login');
      }
      
      $sql_query = 'SELECT DISTINCT   F_FunctionName
                    FROM              tblFunction LEFT OUTER JOIN 
                                      tblRoleFunction ON RF_FK_FunctionID = F_Function_id AND
                                                         RF_FK_ModuleID = F_FK_Module_id
                    WHERE             RF_FK_ModuleID = '.$moduleId.' AND
                                      RF_Active = 1 AND
                                      F_Trigger = 1 AND
                                      RF_FK_RoleID IN (SELECT UR_FK_RoleID FROM tblUserRole WHERE UR_FK_UserID = \''.getCurrentUser()['login-user'].'\')';

      return array_column($this->db->query($sql_query)->result_array(), 'F_FunctionName');
    }

    private function redirect($url, $permanent = false){ 
      header('Location: ' . $url, true, $permanent ? 301 : 302);    
      exit();
    }

}
