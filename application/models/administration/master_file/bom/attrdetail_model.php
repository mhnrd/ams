<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class attrdetail_model extends MAIN_Model {

    private $id = NULL;

    public function __construct() {
        parent::__construct();
    }

    public function getAllByAttrCode($code){

       $query_string = 'SELECT AD_Code, AD_Desc, AD_Id
                        FROM tblAttributeDetail 
                        WHERE AD_FK_Code = ?
                        ORDER BY AD_Desc ASC';
        $params = array($code);
        $result = $this->db->query($query_string, $params);
        return $result->result_array();
    }
}
