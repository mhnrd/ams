<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class item_master_model extends MAIN_Model {

    protected $_table = 'tblItem';

    private $id = NULL;
    private $order_column = array(null,null,'IM_Item_Id','IM_UPCCode','IM_Sales_Desc','IM_SOH','AD_Code','IM_Active');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data(){

        
        $this->db->select("*");  
        $this->db->from('tblItem');  
        $result_count = $this->db->count_all_results();

        $result_item =  $this->datatables();

        foreach ($result_item as $key => $value) {
            $result_item[$key]['IM_SOH'] = $this->getSOH($result_item[$key]['IM_Item_Id']);
        }

        $output['data']              = $result_item;
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('IM_Item_Id, IM_UPCCode, IM_Sales_Desc, IM_UnitCost, IM_Active, AD_Code, 0 AS "IM_SOH"');

        // Individual Column Search

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('IM_Item_Id',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('IM_UPCCode',$_POST['columns'][3]["search"]["value"]);
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            // $this->print_r('test');
            $this->db->like('IM_Sales_Desc',$_POST['columns'][4]["search"]["value"]);
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like("(SELECT ISNULL(SUM(IL_Qty),0) FROM tblItemLedger WHERE IL_ItemNo = IM_Item_Id)",$_POST['columns'][5]["search"]["value"]);
        }

        if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
            $this->db->like('AD_Code',$_POST['columns'][6]["search"]["value"]);
        }

        if(isset($_POST['columns'][7]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $this->db->like("(CASE WHEN IM_Active = 1 THEN 'Active' ELSE 'Inactive' END) ",$_POST['columns'][7]["search"]["value"]);
        }

        // End

        if(isset($_POST["search"]["value"])){
            if($_POST["search"]["value"] != ''){
                $this->db->like("IM_Item_Id", $_POST["search"]["value"]);  
                $this->db->or_like("IM_UPCCode", $_POST["search"]["value"]);  
                $this->db->or_like("IM_Sales_Desc", $_POST["search"]["value"]);  
                $this->db->or_like("IM_UnitCost", $_POST["search"]["value"]);  
                $this->db->or_like("AD_Code", $_POST["search"]["value"]);
            }  
        }  


        $this->db->join('tblAttributeDetail','IM_FK_Attribute_UOM_id = AD_Id', 'left');
        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('IM_Item_Id', 'DESC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblItem');  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblItem');  
           return $query->num_rows();  
    }

    /* datatable query ends here */

    public function getSOH($itemNo){
       
        return $this->db->select('ISNULL(SUM("IL_Qty"),0) AS "IL_Qty"')
                ->where('IL_ItemNo',$itemNo)
                ->where('IL_SubLocation <>', 'INTRANSIT')
                ->where_in('IL_Location',array_column(getUserLocationAccess(), 'CA_FK_Location_id'))
                ->get('tblItemLedger')->row_array()['IL_Qty'];

    }

    public function getAllItemType(){
        return $this->db->select(array('IT_id','IT_Description'))
                        ->where(array(
                                'IT_Active'=>'1',
                                'IT_id <>'=>'GA',
                        ))
                        ->order_by('IT_Description')
                        ->get('tblItemType')->result_array();
    }

    public function getAllProductInventoryPostingGroup(){
        return $this->db->select(array('IPG_Code'))
                        ->where(array(
                                'IPG_Active'=>'1'
                        ))
                        ->order_by('IPG_Code')
                        ->get('tblACC_InvPostingGroup')->result_array();
    }

    public function getAllProductVATPostingGroup(){
        return $this->db->select(array('VPPG_Code','VPPG_Description'))
                        ->where(array(
                                'VPPG_Active'=>'1'
                        ))
                        ->order_by('VPPG_Description')
                        ->get('tblACC_VATProdPostingGroup')->result_array();
    }

    public function getAllProductWHTPostingGroup(){
        return $this->db->select(array('WPPG_Code','WPPG_Description'))
                        ->where(array(
                                'WPPG_Active'=>'1'
                        ))
                        ->order_by('WPPG_Description')
                        ->get('tblACC_WHTProdPostingGroup')->result_array();
    }

    public function getCategoryList($itemType){
        return $this->db->select(array('CAT_id','CAT_Desc'))
                        ->where(array(
                                'CAT_Active'         =>'1',
                                'CAT_FK_Itemtype_id' => $itemType
                        ))
                        ->order_by('CAT_Desc')
                        ->get('tblCategory')->result_array();
    }

    public function getSubCategoryList($cat_id){
        return $this->db->select(array('SC_Id','SC_Description'))
                        ->where(array(
                                'SC_Active'         =>'1',
                                'SC_FK_Category_id' => $cat_id
                        ))
                        ->order_by('SC_Description')
                        ->get('tblSubCategory')->result_array();
    }

    public function getItemNumberbyItemType($Type){
        $sql_query = 'SELECT TOP 1 IM_Item_Id, 
                                   RIGHT(IM_Item_Id, 6) AS ItemSeries
                        FROM tblItem WHERE (IM_Item_Id LIKE \'%'.$Type.'%\') 
                        ORDER BY  ItemSeries ASC';
        $result = $this->db->query($sql_query);

        if($result->num_rows() > 0){
            $ItemSeries = str_replace("-", "", $result->row_array()['ItemSeries']);

            if($ItemSeries == null || $ItemSeries == ''){
                $ItemSeries = $Type.'-000001';
            }
            else{
                if(is_numeric($ItemSeries)){
                    $y = 1;
                    for ($x=1; $x <= $y ; $x++) { 
                        $ItemSeries = $Type.'-'.$this->padLeftZero(6,strval($x));
                        if(!$this->checkItemCodeExist($ItemSeries)){
                            break;
                        }
                        else{
                            $y++;
                        }
                    }
                }
                else{
                    $ItemSeries = $Type.'-000001';
                }
            }
        }
        else{
            $ItemSeries = $Type.'-000001';
        }

        return $ItemSeries;
    }

    private function checkItemCodeExist($ItemSeries){
        $query_string = 'SELECT IM_Item_Id FROM tblItem WHERE IM_Item_Id = \''.$ItemSeries.'\'';

        $result = $this->db->query($query_string)->row_array()['IM_Item_Id'];

        if($result == '' || $result == null){
            $result_bln = false;
        }
        else{
            $result_bln = true;
        }

        return $result_bln;
    }

    private function padLeftZero($num, $txt){

        $pcnt = $num - strlen($txt);
        $output = '';

        for ($i=0; $i < $pcnt; $i++) { 
            $output .= '0';
        }

        $output = $output.$txt;

        return $output;

    }

    public function getAllSupplierBySearch($type, $filter,$supplierlist){

        $this->db->select(array('S_Id','S_Name'))
                    ->like($type,$filter);

        if(count($supplierlist)>0){
            $this->db->where_not_in('S_Id',$supplierlist);
        }
                    
        $result = $this->db->order_by('S_Name')
                    ->get('tblSupplier')->result_array();

        return $result;
    }

    public function getSupplierByFilter($type, $filter, $supplierlist){

        $this->db->select(array('S_Id','S_Name'))
                    ->where($type,$filter);

        if(count($supplierlist)>0){
            $this->db->where_not_in('S_Id',$supplierlist);
        }
         
        $result = $this->db->order_by('S_Name')
                    ->get('tblSupplier')->row_array();

        return $result;
    }

    // FOR UOM FILTER
    public function getAllUOMBySearch($type, $filter, $uomlist){
    
        $this->db->select(array('AD_Id','AD_Code'))
                    ->like($type,$filter)
                    ->where('AD_FK_Code', '1401');

        if(count($uomlist)>0){
            $this->db->where_not_in('AD_Id',$uomlist);
        }
                    
        $result = $this->db->order_by('AD_Code')
                    ->get('tblAttributeDetail')->result_array();

        return $result;
    }

    public function getUOMByFilter($type, $filter , $uomlist){

        $this->db->select(array('AD_Id','AD_Code'))
                    ->where($type,$filter)
                    ->where('AD_FK_Code', '1401');

        if(count($uomlist)>0){
            $this->db->where_not_in('AD_Id',$uomlist);
        }
         
        $result = $this->db->order_by('AD_Code')
                    ->get('tblAttributeDetail')->row_array();

        return $result;
    }

    public function getCountByItemNo($itemID){
        return $this->db->where('IM_Item_Id',$itemID)->get('tblItem')->row_array();
    }

    public function on_save($data, $item_series, $current_user){

        $this->db->trans_begin();
        // SAVING HEADER
        $header_data = array(
                        'IM_Item_Id'                 => $item_series
                        ,'IM_UPCCode'                => $data['IM_UPCCode']
                        ,'IM_Sales_Desc'             => $data['IM_Sales_Desc']
                        ,'IM_Purchased_Desc'         => $data['IM_Purchased_Desc']
                        ,'IM_ProductionArea'         => $data['IM_ProductionArea']
                        // ,'IM_CostOfGoods'            => $data['IM_CostOfGoods']
                        ,'IM_UnitCost'               => $data['IM_UnitCost']
                        ,'IM_UnitPrice'              => $data['IM_UnitPrice']
                        ,'IM_SalePrice'              => $data['IM_SalePrice']
                        ,'IM_VATProductPostingGroup' => $data['IM_VATProductPostingGroup']
                        ,'IM_INVPosting_Group'       => $data['IM_INVPosting_Group']
                        ,'IM_WHTProductPostingGroup' => $data['IM_WHTProductPostingGroup']
                        ,'IM_ManufacturerPartNo'     => $data['IM_ManufacturerPartNo']
                        ,'IM_Weight'                 => $data['IM_Weight']
                        ,'IM_FK_Category_id'         => $data['IM_FK_Category_id']
                        ,'IM_Model'                  => $data['IM_Model']
                        ,'IM_SalePrice'              => $data['IM_SalePrice']
                        ,'IM_FK_ItemType_id'         => $data['IM_FK_ItemType_id']
                        ,'IM_Imported'               => ($data['IM_Imported'] == 'on') ? "1" : "0"
                        ,'IM_Short_Desc'             => $data['IM_Short_Desc']
                        ,'IM_Serialize'              => ($data['IM_Serialize'] == 'on') ? "1" : "0"
                        ,'IM_FK_SubCategory_id'      => $data['IM_FK_SubCategory_id']
                        ,'IM_PAR_Item'               => ($data['IM_PAR_Item'] == 'on') ? "1" : "0"
                        ,'IM_FK_Attribute_UOM_id'    => $data['IM_FK_Attribute_UOM_id']
                        ,'IM_Production_UOM_id'      => $data['IM_Production_UOM_id']
                        ,'IM_Transfer_UOM_id'        => $data['IM_Transfer_UOM_id']
                        ,'IM_OF_UOM_id'              => $data['IM_OF_UOM_id']
                        ,'IM_Status'                 => 'Posted'
                        ,'IM_Active'                 => '1'
                        ,'IM_SalesUOM'               => $data['IM_SalesUOM']
                        ,'IM_PurchaseUOM'            => $data['IM_PurchaseUOM']
                        ,'IM_Item_Waste'             => $data['IM_Item_Waste']
                        ,'IM_ExpiryDays'             => $data['IM_ExpiryDays']
                        ,'IM_Scrap'                  => $data['IM_Scrap']
                        ,'IM_Scrap_Item_Id'          => $data['IM_Scrap_Item_Id']
                        ,'IM_Scrap_Perc'             => $data['IM_Scrap_Perc']
                    );

        $this->db->insert('tblItem',$header_data);

        // SAVING SUPPLIER DETAIL
        if(isset($data['supplier-list'])):
            if(count($data['supplier-list']) > 0){
                $reindexed_supplier_array = array_values($data['supplier-list']);
                foreach ($reindexed_supplier_array as $key => $supplier_detail):
                    $_supplier_detail = array(
                        'IS_FK_Item_id'         => $item_series,
                        'IS_FK_Suppllier_id'    => $supplier_detail['supplier-id'],
                        'IS_SupplierItemCode'   => $supplier_detail['supplier-item-code'],
                        'IS_OldItemCode'        => $supplier_detail['old-item-code'],
                        'IS_LastPOCost'         => $supplier_detail['unit-cost'],
                        'IS_Active'             => ($supplier_detail['status']) ? "1" : "0" ,
                        'CreatedBy'             => $current_user,
                        'DateCreated'           => date('m/d/Y h:i:s')
                    );

                    $this->db->insert('tblItemSupplier',$_supplier_detail);
                    
                endforeach;
            }
        endif;

        // SAVING UOM
        $getLatestNo = $this->db->order_by('IUC_Id','DESC')->limit(1)->get('tblItemUOMConv')->row_array()['IUC_Id'];

        $uom_array = array(
            'IUC_Id'         => $getLatestNo + 1,
            'IUC_FK_Item_id' => $item_series,
            'IUC_Quantity'   => 1,
            'IUC_FK_UOM_id'  => $data['IM_FK_Attribute_UOM_id'],
        );
        
        $this->db->insert('tblItemUOMConv',$uom_array);
        $getLatestNo += 1;
        
        // SAVING UOM CONVERSION
        if(isset($data['uom-list'])):
            if(count($data['uom-list']) > 0){
                $reindexed_uom_array = array_values($data['uom-list']);
                foreach ($reindexed_uom_array as $key => $uom_detail):
                    $_uom_detail = array(
                        'IUC_Id'         => $getLatestNo + 1,
                        'IUC_FK_Item_id' => $item_series,
                        'IUC_Quantity'   => $uom_detail['quantity'],
                        'IUC_FK_UOM_id'  => $uom_detail['uom'],
                    );
                    $getLatestNo += 1;

                    $this->db->insert('tblItemUOMConv',$_uom_detail);
                    
                endforeach;
            }
        endif;    

        //validation of saving
        if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $output = array(
                                    'success'=>false
                                ,   'message'=>$this->db->error()
                            );
        }
        else{
                $this->db->trans_commit();
                $output = array(
                                    'success'=>true
                                ,   'message'=>''
                            );
        }

        return $output;
    }

    public function checkIfScrap($itemNo){
        $query_string = 'SELECT IM_Scrap FROM tblItem WHERE IM_Item_id = \''.$itemNo.'\'';

        return $this->db->query($query_string)->row_array()['IM_Scrap'];

    }

    public function checkFGhasScrap($itemNo){
        $query_string = 'SELECT IM_Item_Id FROM tblItem WHERE IM_Scrap_Item_Id = \''.$itemNo.'\'';

        return $this->db->query($query_string)->row_array()['IM_Item_Id'];
    }

    public function on_update($data, $item_series, $current_user){

        $scrap = $this->checkIfScrap($data['IM_Item_Id']);

        $fg_scrap = $this->checkFGhasScrap($data['IM_Item_Id']);

        if($scrap == 0){

            $this->db->trans_begin();
            // SAVING HEADER
            $header_data = array(
                            'IM_UPCCode'                => $data['IM_UPCCode']
                            ,'IM_Sales_Desc'             => $data['IM_Sales_Desc']
                            ,'IM_Purchased_Desc'         => $data['IM_Purchased_Desc']
                            ,'IM_ProductionArea'         => $data['IM_ProductionArea']
                            // ,'IM_CostOfGoods'            => $data['IM_CostOfGoods']
                            ,'IM_UnitCost'               => $data['IM_UnitCost']
                            ,'IM_UnitPrice'              => $data['IM_UnitPrice']
                            ,'IM_SalePrice'              => $data['IM_SalePrice']
                            ,'IM_VATProductPostingGroup' => $data['IM_VATProductPostingGroup']
                            ,'IM_INVPosting_Group'       => $data['IM_INVPosting_Group']
                            ,'IM_WHTProductPostingGroup' => $data['IM_WHTProductPostingGroup']
                            ,'IM_ManufacturerPartNo'     => $data['IM_ManufacturerPartNo']
                            ,'IM_Weight'                 => $data['IM_Weight']
                            ,'IM_FK_Category_id'         => $data['IM_FK_Category_id']
                            ,'IM_Model'                  => $data['IM_Model']
                            ,'IM_SalePrice'              => $data['IM_SalePrice']
                            ,'IM_FK_ItemType_id'         => $data['IM_FK_ItemType_id']
                            ,'IM_Imported'               => ($data['IM_Imported'] == 'on') ? "1" : "0"
                            ,'IM_Short_Desc'             => $data['IM_Short_Desc']
                            ,'IM_Serialize'              => ($data['IM_Serialize'] == 'on') ? "1" : "0"
                            ,'IM_FK_SubCategory_id'      => $data['IM_FK_SubCategory_id']
                            ,'IM_PAR_Item'               => ($data['IM_PAR_Item'] == 'on') ? "1" : "0"
                            ,'IM_FK_Attribute_UOM_id'    => $data['IM_FK_Attribute_UOM_id']
                            ,'IM_Production_UOM_id'      => $data['IM_Production_UOM_id']
                            ,'IM_Transfer_UOM_id'        => $data['IM_Transfer_UOM_id']
                            ,'IM_OF_UOM_id'              => $data['IM_OF_UOM_id']
                            ,'IM_Status'                 => 'Posted'
                            ,'IM_Active'                 => '1'
                            ,'IM_SalesUOM'               => $data['IM_SalesUOM']
                            ,'IM_PurchaseUOM'            => $data['IM_PurchaseUOM']
                            ,'IM_Item_Waste'             => $data['IM_Item_Waste']
                            ,'IM_ExpiryDays'             => $data['IM_ExpiryDays']
                        );

            $this->db->where('IM_Item_Id',$item_series)->update('tblItem',$header_data);

            // SAVING SUPPLIER DETAIL
            $this->db->delete('tblItemSupplier',array('IS_FK_Item_id'=>$item_series));
            if(isset($data['supplier-list'])):
                if(count($data['supplier-list']) > 0){
                    $reindexed_supplier_array = array_values($data['supplier-list']);
                    foreach ($reindexed_supplier_array as $key => $supplier_detail):
                        $_supplier_detail = array(
                            'IS_FK_Item_id'         => $item_series,
                            'IS_FK_Suppllier_id'    => $supplier_detail['supplier-id'],
                            'IS_SupplierItemCode'   => $supplier_detail['supplier-item-code'],
                            'IS_OldItemCode'        => $supplier_detail['old-item-code'],
                            'IS_LastPOCost'         => $supplier_detail['unit-cost'],
                            'IS_Active'             => ($supplier_detail['status']) ? "1" : "0" ,
                            'CreatedBy'             => $current_user,
                            'DateCreated'           => date('m/d/Y h:i:s')
                        );

                        $this->db->insert('tblItemSupplier',$_supplier_detail);
                        
                    endforeach;
                }
            endif;
            // SAVING UOM
            $getLatestNo = $this->db->order_by('IUC_Id','DESC')->limit(1)->get('tblItemUOMConv')->row_array()['IUC_Id'];
            $this->db->delete('tblItemUOMConv',array('IUC_FK_Item_id'=>$item_series));
            $uom_array = array(
                'IUC_Id'         => $getLatestNo + 1,
                'IUC_FK_Item_id' => $item_series,
                'IUC_Quantity'   => 1,
                'IUC_FK_UOM_id'  => $data['IM_FK_Attribute_UOM_id'],
            );
            
            $this->db->insert('tblItemUOMConv',$uom_array);
            $getLatestNo += 1;
            
            // SAVING UOM CONVERSION
            if(isset($data['uom-list'])):
                if(count($data['uom-list']) > 0){
                    $reindexed_uom_array = array_values($data['uom-list']);
                    foreach ($reindexed_uom_array as $key => $uom_detail):
                        $_uom_detail = array(
                            'IUC_Id'         => $getLatestNo + 1,
                            'IUC_FK_Item_id' => $item_series,
                            'IUC_Quantity'   => $uom_detail['quantity'],
                            'IUC_FK_UOM_id'  => $uom_detail['uom'],
                        );
                        $getLatestNo += 1;

                        $this->db->insert('tblItemUOMConv',$_uom_detail);
                        
                    endforeach;
                }
            endif;

            //validation of saving
            if($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $output = array(
                                        'success'=>false
                                    ,   'message'=>$this->db->error()
                                );
            }
            else{
                    $this->db->trans_commit();
                    $output = array(
                                        'success'=>true
                                    ,   'message'=>''
                                );
            }

            if($fg_scrap != '' || $fg_scrap != null){
                 $this->db->trans_begin();
                // SAVING HEADER
                $header_data = array(
                                'IM_UPCCode'                => $data['IM_UPCCode']
                                ,'IM_Sales_Desc'             => $data['IM_Sales_Desc']
                                ,'IM_Purchased_Desc'         => $data['IM_Purchased_Desc']
                                ,'IM_ProductionArea'         => $data['IM_ProductionArea']
                                // ,'IM_CostOfGoods'            => $data['IM_CostOfGoods']
                                ,'IM_UnitCost'               => $data['IM_UnitCost']
                                ,'IM_UnitPrice'              => $data['IM_UnitPrice']
                                ,'IM_SalePrice'              => $data['IM_SalePrice']
                                ,'IM_VATProductPostingGroup' => $data['IM_VATProductPostingGroup']
                                ,'IM_INVPosting_Group'       => $data['IM_INVPosting_Group']
                                ,'IM_WHTProductPostingGroup' => $data['IM_WHTProductPostingGroup']
                                ,'IM_ManufacturerPartNo'     => $data['IM_ManufacturerPartNo']
                                ,'IM_Weight'                 => $data['IM_Weight']
                                ,'IM_FK_Category_id'         => $data['IM_FK_Category_id']
                                ,'IM_Model'                  => $data['IM_Model']
                                ,'IM_SalePrice'              => $data['IM_SalePrice']
                                ,'IM_FK_ItemType_id'         => $data['IM_FK_ItemType_id']
                                ,'IM_Imported'               => ($data['IM_Imported'] == 'on') ? "1" : "0"
                                ,'IM_Short_Desc'             => $data['IM_Short_Desc']
                                ,'IM_Serialize'              => ($data['IM_Serialize'] == 'on') ? "1" : "0"
                                ,'IM_FK_SubCategory_id'      => $data['IM_FK_SubCategory_id']
                                ,'IM_PAR_Item'               => ($data['IM_PAR_Item'] == 'on') ? "1" : "0"
                                ,'IM_FK_Attribute_UOM_id'    => $data['IM_FK_Attribute_UOM_id']
                                ,'IM_Production_UOM_id'      => $data['IM_Production_UOM_id']
                                ,'IM_Transfer_UOM_id'        => $data['IM_Transfer_UOM_id']
                                ,'IM_OF_UOM_id'              => $data['IM_OF_UOM_id']
                                ,'IM_Status'                 => 'Posted'
                                ,'IM_Active'                 => '1'
                                ,'IM_SalesUOM'               => $data['IM_SalesUOM']
                                ,'IM_PurchaseUOM'            => $data['IM_PurchaseUOM']
                                ,'IM_Item_Waste'             => $data['IM_Item_Waste']
                                ,'IM_ExpiryDays'             => $data['IM_ExpiryDays']
                            );

                $this->db->where('IM_Item_Id',$fg_scrap)->update('tblItem',$header_data);

                // SAVING SUPPLIER DETAIL
                $this->db->delete('tblItemSupplier',array('IS_FK_Item_id'=>$fg_scrap));
                if(isset($data['supplier-list'])):
                    if(count($data['supplier-list']) > 0){
                        $reindexed_supplier_array = array_values($data['supplier-list']);
                        foreach ($reindexed_supplier_array as $key => $supplier_detail):
                            $_supplier_detail = array(
                                'IS_FK_Item_id'         => $fg_scrap,
                                'IS_FK_Suppllier_id'    => $supplier_detail['supplier-id'],
                                'IS_SupplierItemCode'   => $supplier_detail['supplier-item-code'],
                                'IS_OldItemCode'        => $supplier_detail['old-item-code'],
                                'IS_LastPOCost'         => $supplier_detail['unit-cost'],
                                'IS_Active'             => ($supplier_detail['status']) ? "1" : "0" ,
                                'CreatedBy'             => $current_user,
                                'DateCreated'           => date('m/d/Y h:i:s')
                            );

                            $this->db->insert('tblItemSupplier',$_supplier_detail);
                            
                        endforeach;
                    }
                endif;
                // SAVING UOM
                $getLatestNo = $this->db->order_by('IUC_Id','DESC')->limit(1)->get('tblItemUOMConv')->row_array()['IUC_Id'];
                $this->db->delete('tblItemUOMConv',array('IUC_FK_Item_id'=>$fg_scrap));
                $uom_array = array(
                    'IUC_Id'         => $getLatestNo + 1,
                    'IUC_FK_Item_id' => $fg_scrap,
                    'IUC_Quantity'   => 1,
                    'IUC_FK_UOM_id'  => $data['IM_FK_Attribute_UOM_id'],
                );
                
                $this->db->insert('tblItemUOMConv',$uom_array);
                $getLatestNo += 1;
                
                // SAVING UOM CONVERSION
                if(isset($data['uom-list'])):
                    if(count($data['uom-list']) > 0){
                        $reindexed_uom_array = array_values($data['uom-list']);
                        foreach ($reindexed_uom_array as $key => $uom_detail):
                            $_uom_detail = array(
                                'IUC_Id'         => $getLatestNo + 1,
                                'IUC_FK_Item_id' => $fg_scrap,
                                'IUC_Quantity'   => $uom_detail['quantity'],
                                'IUC_FK_UOM_id'  => $uom_detail['uom'],
                            );
                            $getLatestNo += 1;

                            $this->db->insert('tblItemUOMConv',$_uom_detail);
                            
                        endforeach;
                    }
                endif;

                //validation of saving
                if($this->db->trans_status() === FALSE){
                        $this->db->trans_rollback();
                        $output = array(
                                            'success'=>false
                                        ,   'message'=>$this->db->error()
                                    );
                }
                else{
                        $this->db->trans_commit();
                        $output = array(
                                            'success'=>true
                                        ,   'message'=>''
                                    );
                }
            }
        }
        else{
            $this->db->trans_begin();
            //UPDATE HEADER ONLY FOR SCRAP
            $header_data = array(
                            'IM_UPCCode'                => $data['IM_UPCCode']
                            ,'IM_Sales_Desc'             => $data['IM_Sales_Desc']
                            ,'IM_Purchased_Desc'         => $data['IM_Purchased_Desc']
                            ,'IM_ProductionArea'         => $data['IM_ProductionArea']
                            // ,'IM_CostOfGoods'            => $data['IM_CostOfGoods']
                            ,'IM_UnitCost'               => $data['IM_UnitCost']
                            ,'IM_UnitPrice'              => $data['IM_UnitPrice']
                            ,'IM_SalePrice'              => $data['IM_SalePrice']
                            ,'IM_Status'                 => 'Posted'
                            ,'IM_Active'                 => '1'
                        );

            $this->db->where('IM_Item_Id',$item_series)->update('tblItem',$header_data);

            //validation of saving
            if($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $output = array(
                                        'success'=>false
                                    ,   'message'=>$this->db->error()
                                );
            }
            else{
                    $this->db->trans_commit();
                    $output = array(
                                        'success'=>true
                                    ,   'message'=>''
                                );
            }
        }


        

        return $output;
    }

    public function getIdentifierData($item_id){
        return $this->db->select(array(
                                    'IM_FK_ItemType_id',
                                    'IT_Description',
                                    'IM_FK_Category_id',
                                    'CAT_Desc',
                                    'IM_FK_SubCategory_id',
                                    'SC_Description',
                                    'IM_ProductionArea',
                                    'SP_StoreName',
                            ))
                    ->where('IM_Item_Id', $item_id)
                    ->join('tblItemType','IT_id = IM_FK_ItemType_id', 'left')
                    ->join('tblCategory','CAT_id = IM_FK_Category_id', 'left')
                    ->join('tblSubCategory','SC_Id = IM_FK_SubCategory_id', 'left')
                    ->join('tblStoreProfile','SP_ID = IM_ProductionArea', 'left')
                    ->get('tblItem')->row_array();
    }

    public function getItemClassificationGroupingData($item_id){
        return $this->db->select(array(
                                    'IM_INVPosting_Group',
                                    'IM_VATProductPostingGroup',
                                    'VPPG_Description',
                                    'IM_WHTProductPostingGroup',
                                    'WPPG_Description',
                            ))
                    ->where('IM_Item_Id', $item_id)
                    ->join('tblACC_VATProdPostingGroup','VPPG_Code = IM_VATProductPostingGroup')
                    ->join('tblACC_WHTProdPostingGroup','WPPG_Code = IM_WHTProductPostingGroup')
                    ->get('tblItem')->row_array();
    }

    public function getItemSupplierData($item_id){
        return $this->db->select(array(
                                        'IS_FK_Suppllier_id',
                                        'S_Name',
                                        'IS_SupplierItemCode',
                                        'IS_OldItemCode',
                                        'IS_LastPOCost',
                                        'IS_Active'
                          ))
                        ->where('IS_FK_Item_id', $item_id)
                        ->join('tblSupplier','S_Id = IS_FK_Suppllier_id')
                        ->get('tblItemSupplier')->result_array();
    }

    public function getItemUOMData($item_id){
        return $this->db->select(array(
                                        'IUC_FK_Item_id',
                                        'IUC_Quantity',
                                        'IUC_FK_UOM_id',
                                        'AD_Code'
                          ))
                        ->where('IUC_FK_Item_id', $item_id)
                        ->where('IUC_Quantity !=', 1)
                        ->join('tblAttributeDetail','AD_Id = IUC_FK_UOM_id')
                        ->get('tblItemUOMConv')->result_array();
    }

    public function getUOMInformationData($item_id){
        $sql_query = 'SELECT I.IM_FK_Attribute_UOM_id, AD3.AD_Code, I.IM_SalesUOM, 
                             AD1.AD_Code AS SalesUOM_Name, I.IM_PurchaseUOM, AD2.AD_Code AS PurchUOM_Name
                      FROM tblItem AS I LEFT OUTER JOIN
                              tblAttributeDetail AS AD2 ON I.IM_PurchaseUOM = AD2.AD_Id LEFT OUTER JOIN
                              tblAttributeDetail AS AD1 ON I.IM_SalesUOM = AD1.AD_Id LEFT OUTER JOIN
                              tblAttributeDetail AS AD3 ON I.IM_FK_Attribute_UOM_id = AD3.AD_Id
                      WHERE (I.IM_Item_Id = ?)';
        $params = array($item_id);
        return $this->db->query($sql_query,$params)->row_array();
    }

    public function getUOMInformationDataProdTransfer($item_id){
        $sql_query = 'SELECT I.IM_Production_UOM_id, AD1.AD_Code AS ProductionUOM_Name, I.IM_Transfer_UOM_id, AD2.AD_Code AS TransferUOM_Name, 
                        I.IM_OF_UOM_id, AD3.AD_Code AS OF_UOM_Name
                      FROM tblItem AS I LEFT OUTER JOIN
                              tblAttributeDetail AS AD1 ON I.IM_Production_UOM_id = AD1.AD_Id LEFT OUTER JOIN
                              tblAttributeDetail AS AD2 ON I.IM_Transfer_UOM_id = AD2.AD_Id LEFT OUTER JOIN
                              tblAttributeDetail AS AD3 ON I.IM_OF_UOM_id = AD3.AD_Id
                      WHERE '.$this->sql_hash('I.IM_Item_Id').' = ?';
        $params = array($item_id);
        return $this->db->query($sql_query,$params)->row_array();
    }

    public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */
    
        $this->db->trans_begin();
        $this->db->delete('tblItemSupplier',array('IS_FK_Suppllier_id'=>$docno));
        $this->db->delete('tblItemUOMConv',array('IUC_FK_Item_id'=>$docno));
        $this->db->delete('tblItem',array('IM_Item_Id'=>$docno));

        if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return array('success'=>0);
        }
        else{
                $this->db->trans_commit();
                return array('success'=>1);
        }

    }

    public function status_data($docno,$action){

        $this->db->trans_begin();
        foreach ($docno as $row) {
            if($action == 'active'){
                $this->db->where('IM_Item_Id',$row)->update('tblItem',array('IM_Active'=>'1'));
            }
            else{
                $this->db->where('IM_Item_Id',$row)->update('tblItem',array('IM_Active'=>'0'));
            }
        }
        if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return array('success'=>0);
        }
        else{
                $this->db->trans_commit();
                return array('success'=>1);
        }

    }

    public function getAllBySearch($type, $filter, $cat_id = null){
        $where_clause = '';
        $where_clause .= "WHERE ".$type." LIKE '%". $filter ."%' " ;

        if($cat_id != ''){
            $where_clause .= ' AND IM_FK_Category_id = \''.$cat_id.'\' ';
        }

        $where_clause .= ' AND IM_Scrap = 0';

        $query_string = 'SELECT [IM_Item_Id], [IM_UPCCode], [IM_Sales_Desc], [IM_UnitCost], [IM_Item_Waste]
                        FROM tblItem
                        '.$where_clause.'
                        ORDER BY [IM_Sales_Desc]';
        
        $result = $this->db->query($query_string)->result_array();

        foreach ($result as $key => $value) {
            $result[$key]['IM_Version'] = $this->db->where('BOM_ItemNo',$result[$key]['IM_Item_Id'])->get('tblBOM')->num_rows() + 1;
        }

        return $result;
    }

    public function getByFilter($type, $filter , $cat_id = null){

        $query_string = 'SELECT [IM_Item_Id], [IM_UPCCode], [IM_Sales_Desc], [IM_UnitCost], [IM_Item_Waste]
                        FROM tblItem
                        WHERE '.$type.' = ? ';
        
        if($cat_id != ''){
            $query_string .= ' AND IM_FK_Category_id = \''.$cat_id.'\' ';
        }

        $query_string .= ' AND IM_Scrap = 0';

        $query_string .= ' ORDER BY [IM_Sales_Desc]';
        $params = array($filter);
        $result = $this->db->query($query_string, $params)->row_array();

        return $result;
    }

    public function getItemBySearch($type, $filter, $item_type = null){
        $where_clause = '';
        $where_clause .= "WHERE ".$type." LIKE '%". $filter ."%' " ;

        if($item_type != ''){
            $where_clause .= ' AND IM_Type = \''.$item_type.'\' ';
        }

        $uom_id = UOM;

        $where_clause .= ' AND ISNULL(IM_Scrap, 0) = 0';

        $query_string = 'SELECT [IM_Item_Id], [IM_UPCCode], [IM_Sales_Desc], [IM_UnitCost], [IM_Item_Waste], [IM_FK_ItemType_id], [IM_FK_Attribute_UOM_id], [AD_Desc]
                        FROM tblItem 
                            LEFT OUTER JOIN tblAttributeDetail ON AD_Id = IM_FK_Attribute_UOM_id AND AD_FK_Code = '.$uom_id.'
                        '.$where_clause.'
                        ORDER BY [IM_Sales_Desc]';
        
        $result = $this->db->query($query_string)->result_array();

        // $this->print_r($result);
        // $this->print_lastquery();
        // die();

        foreach ($result as $key => $value) {
            $result[$key]['IM_Version'] = $this->db->where('BOM_ItemNo',$result[$key]['IM_Item_Id'])->get('tblBOM')->num_rows() + 1;
        }

        return $result;
    }

    public function getItemByFilter($type, $filter , $item_type = null){

        $query_string = 'SELECT [IM_Item_Id], [IM_UPCCode], [IM_Sales_Desc], [IM_UnitCost], [IM_Item_Waste], [IM_FK_ItemType_id], [IM_FK_Attribute_UOM_id], [AD_Desc]
                        FROM tblItem LEFT OUTER JOIN tblAttributeDetail ON AD_Id = IM_FK_Attribute_UOM_id
                        WHERE '.$type.' = ?';
        
        if($item_type != ''){
            $query_string .= ' AND IM_Type = \''.$item_type.'\' ';
        }

        $query_string .= ' AND ISNULL(IM_Scrap, 0) = 0';

        $query_string .= ' ORDER BY [IM_Sales_Desc]';
        $params = array($filter);
        $result = $this->db->query($query_string, $params)->row_array();

        return $result;
    }

    public function getScrapParentBySearch($type, $filter, $cat_id = null){
        $where_clause = '';
        $where_clause .= "WHERE ".$type." LIKE '%". $filter ."%' " ;

        if($cat_id != ''){
            $where_clause .= ' AND IM_FK_Category_id = \''.$cat_id.'\' ';
        }

        $where_clause .= ' AND ISNULL(IM_Scrap, 0) = 0 AND IM_Item_Id NOT IN (SELECT IM_Scrap_Item_Id FROM tblItem as tblItem1 WHERE (IM_Scrap_Item_Id <> NULL OR IM_Scrap_Item_Id <> \'\'))';

        $query_string = 'SELECT [IM_Item_Id], [IM_UPCCode], [IM_Sales_Desc], [IM_UnitCost], [IM_Item_Waste]
                        FROM tblItem
                        '.$where_clause.'
                        ORDER BY [IM_Sales_Desc]';
        
        $result = $this->db->query($query_string)->result_array();

        foreach ($result as $key => $value) {
            $result[$key]['IM_Version'] = $this->db->where('BOM_ItemNo',$result[$key]['IM_Item_Id'])->get('tblBOM')->num_rows() + 1;
        }

        return $result;
    }

    public function getScrapParentByFilter($type, $filter , $cat_id = null){

        $query_string = 'SELECT [IM_Item_Id], [IM_UPCCode], [IM_Sales_Desc], [IM_UnitCost], [IM_Item_Waste]
                        FROM tblItem
                        WHERE '.$type.' = ? AND IM_Item_Id NOT IN (SELECT IM_Scrap_Item_Id FROM tblItem as tblItem1 WHERE (IM_Scrap_Item_Id <> NULL OR IM_Scrap_Item_Id <> \'\'))';
        
        if($cat_id != ''){
            $query_string .= ' AND IM_FK_Category_id = \''.$cat_id.'\' ';
        }

        $query_string .= ' AND ISNULL(IM_Scrap, 0) = 0';

        $query_string .= ' ORDER BY [IM_Sales_Desc]';
        $params = array($filter);
        $result = $this->db->query($query_string, $params)->row_array();

        return $result;
    }

    public function getAllSOHByLocation($item_no){
        
        return $this->db->select('IL_Location, SP_StoreName, IL_SubLocation, IL_Qty')
                        ->where('IL_ItemNo', $item_no)
                        ->where('IL_SubLocation <>', 'INTRANSIT')
                        ->where_in('IL_Location', array_column(getUserLocationAccess(),'CA_FK_Location_id'))
                        ->join('tblStoreProfile','SP_ID = IL_Location')
                        ->get('tblItemLedger')->result_array();
    }

    public function copyDetails($itemNo){
        $query_string = 'SELECT        I.IM_INVPosting_Group, I.IM_WHTProductPostingGroup, I.IM_ExpDate, I.IM_FK_Category_id, I.IM_FK_ItemType_id, 
                                       I.IM_FK_SubCategory_id, I.IM_SalesUOM, I.IM_PurchaseUOM, I.IM_Item_Waste, I.IM_ExpiryDays, I.IM_ProductionArea, 
                                       I.IM_Production_UOM_id, I.IM_Transfer_UOM_id, I.IM_LastPOCost, I.IM_Scrap, I.IM_Scrap_Item_Id, I.IM_Scrap_Perc, 
                                       I.IM_VATProductPostingGroup, C.CAT_Desc, 
                                       SC.SC_Description, IT.IT_Description, SP.SP_StoreName, I.IM_FK_Attribute_UOM_id, bUOM.AD_Desc AS BaseUOM,
                                       sUOM.AD_Desc AS SalesUOM, puUOM.AD_Desc AS PurchaseUOM, prUOM.AD_Desc AS ProdUOM, tUOM.AD_Desc AS TransferUOM
                        FROM            tblAttributeDetail AS puUOM RIGHT OUTER JOIN
                                        tblItem AS I LEFT OUTER JOIN
                                        tblAttributeDetail AS tUOM ON I.IM_Transfer_UOM_id = tUOM.AD_Id LEFT OUTER JOIN
                                        tblAttributeDetail AS prUOM ON I.IM_Production_UOM_id = prUOM.AD_Id ON puUOM.AD_Id = I.IM_PurchaseUOM LEFT OUTER JOIN
                                        tblAttributeDetail AS sUOM ON I.IM_SalesUOM = sUOM.AD_Id LEFT OUTER JOIN
                                        tblAttributeDetail AS bUOM ON I.IM_FK_Attribute_UOM_id = bUOM.AD_Id LEFT OUTER JOIN
                                        tblStoreProfile AS SP ON I.IM_ProductionArea = SP.SP_ID LEFT OUTER JOIN
                                        tblItemType AS IT ON I.IM_FK_ItemType_id = IT.IT_id LEFT OUTER JOIN
                                        tblSubCategory AS SC ON I.IM_FK_SubCategory_id = SC.SC_Id LEFT OUTER JOIN
                                        tblCategory AS C ON I.IM_FK_Category_id = C.CAT_id
                        WHERE        (I.IM_Item_Id = \''.$itemNo.'\')';

        return $this->db->query($query_string)->row_array(); 
    }

    public function copySupplierList($itemNo){
        $query_string = 'SELECT        S.S_Name, ItemSup.IS_SupplierItemCode, ItemSup.IS_LastPOCost, ItemSup.IS_OldItemCode, ItemSup.IS_Active
                        FROM            tblItemSupplier AS ItemSup LEFT OUTER JOIN
                                                 tblSupplier AS S ON ItemSup.IS_FK_Suppllier_id = S.S_Id
                        WHERE        (ItemSup.IS_FK_Item_id = \''.$itemNo.'\')';

        return $this->db->query($query_string)->result_array(); 
    }

    public function copyUOMList($itemNo){
        $query_string = 'SELECT        IUC.IUC_FK_Item_id, IUC.IUC_Quantity, IUC.IUC_FK_UOM_id, AD.AD_Desc
                        FROM            tblItemUOMConv AS IUC LEFT OUTER JOIN
                                        tblAttributeDetail AS AD ON IUC.IUC_FK_UOM_id = AD.AD_Id
                        WHERE        (IUC.IUC_FK_Item_id = \''.$itemNo.'\') AND (IUC.IUC_Quantity > 1)';

        return $this->db->query($query_string)->result_array();
    }

    public function getProdTransOFUom($itemNo){
        $query_string = 'SELECT        I.IM_Item_Id, I.IM_Production_UOM_id, I.IM_Transfer_UOM_id, I.IM_OF_UOM_id, AD1.AD_Desc AS ProdUOM, AD2.AD_Desc 
                                       AS TransUOM, AD3.AD_Desc AS OFUom
                        FROM            tblAttributeDetail AS AD3 RIGHT OUTER JOIN
                                                 tblItem AS I ON AD3.AD_Id = I.IM_OF_UOM_id LEFT OUTER JOIN
                                                 tblAttributeDetail AS AD2 ON I.IM_Transfer_UOM_id = AD2.AD_Id LEFT OUTER JOIN
                                                 tblAttributeDetail AS AD1 ON I.IM_Production_UOM_id = AD1.AD_Id
                        WHERE        (I.IM_Item_Id = \''.$itemNo.'\')';

        return $this->db->query($query_string)->row_array(); 
    }



}
