<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bank_account_model extends MAIN_Model{

	protected $_table = 'tblBankAccount';

    private $id = NULL;
    private $order_column = array(null,null,'BA_BankID','BA_BankName','BA_BankAccountType','BA_Currency','BA_Active','AD_Code');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data($status = null){

        $this->db->select('BA_BankID, BA_BankName, BA_BankAccountType, BA_Currency, BA_Active, AD_Code'); 
        $this->db->join('tblAttributeDetail', 'AD_Id = BA_Currency', 'left');
        

        $this->db->from('tblBankAccount');
        $result_count = $this->db->count_all_results();

        $output['data']              = $this->datatables($status);
        $output['num_rows']          = $this->get_filtered_data($status);
        $output['count_all_results'] = $result_count;

        return $output;
    }

     public function query($status = null){
        $this->db->select('BA_BankID, BA_BankName, BA_BankAccountType, BA_Currency, BA_Active, AD_Code');
        $this->db->join('tblAttributeDetail', 'AD_Id = BA_Currency', 'left');

        // Individual Column Search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('BA_BankID',trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('BA_BankName',trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            // $this->print_r('test');
            $this->db->like('BA_BankAccountType',trim($_POST['columns'][4]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $this->db->like('BA_Currency',trim($_POST['columns'][7]["search"]["value"]));
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $this->db->like("(CASE WHEN BA_Active = 1 THEN 'Active' ELSE 'Inactive' END) ",$_POST['columns'][7]["search"]["value"]);
        }

        // End

        if(isset($_POST["search"]["value"])){
            if($_POST["search"]["value"] != ''){
                $this->db->like("BA_BankID", trim($_POST["search"]["value"]));  
                $this->db->or_like("BA_BankName", trim($_POST["search"]["value"]));  
                $this->db->or_like("BA_BankAccountType", trim($_POST["search"]["value"]));  
                $this->db->or_like("BA_Currency", trim($_POST["search"]["value"]));
            }  
        }  
        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('BA_BankID', 'DESC');  
        }
    }

     public function datatables($status = null){
        $this->query($status); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblBankAccount');  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblBankAccount');  
           return $query->num_rows();  
    }
      // END 

    public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */
        $this->db->delete('tblBankAccount',array('BA_BankID'=>$docno));

        if ($this->db->trans_status() === FALSE){
                return array('success'=>0);
        }
        else{
           
                return array('success'=>1);
        }

    }

    public function getActiveBPG(){
        $query_string = 'SELECT BPG_Code
                         FROM tblBankPostingGroup as BPG
                         WHERE BPG_Active = \'1\'';

        return $this->db->query($query_string)->result_array();
    }

    public function getActiveCurrency(){
        $query_string = 'SELECT AD_Code, AD_FK_Code, AD_Id
                        FROM tblAttributeDetail as AD 
                        WHERE AD_Active = \'1\' AND  AD_FK_Code = \'1302\' ';

        return $this->db->query($query_string)->result_array();
    }

     public function getActiveCPC(){
        $query_string = 'SELECT CPC_Id, CPC_Desc
                         FROM tblCPCenter as CPC
                         WHERE CPC_Active = \'1\'';

        return $this->db->query($query_string)->result_array();
    }

}