<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Location_model extends MAIN_Model {

    protected $_table = 'tblLocation';

    private $id = NULL;
    private $order_column = array(null,null,'SP_ID','LOC_Name','SP_Address', 'COM_Name', 'CPC_Desc', 'CASE SP_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as SP_Active');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data($status = null){

        $this->db->select('SP_ID, SP_StoreName, SP_Address, COM_Name, CPC_Desc, 
            CASE SP_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as SP_Active'); 
        $this->db->join('tblCPCenter', ' LOC_FK_CPC_id = CPC_Id', 'left');
        $this->db->join('tblCompany', 'LOC_FK_Company_id = COM_Id', 'left');
        $this->db->from('tblLocation');
        $result_count = $this->db->count_all_results();

        $output['data']              = $this->datatables($status);
        $output['num_rows']          = $this->get_filtered_data($status);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query($status = null){
        $this->db->select('SP_ID, SP_StoreName, SP_Address, COM_Name, CPC_Desc, CASE SP_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as SP_Active', false);
        $this->db->join('tblCPCenter', ' SP_FK_CPC_id = CPC_Id', 'left');
        $this->db->join('tblCompany', 'SP_FK_CompanyID = COM_Id', 'left');

        // Individual Column Search

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('SP_ID',trim($_POST['columns'][2]["search"]["value"]));
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('SP_StoreName',trim($_POST['columns'][3]["search"]["value"]));
        }

        if(isset($_POST['columns'][4]["search"]["value"]) && !empty($_POST['columns'][4]["search"]["value"])){
            $this->db->like('SP_Address',trim($_POST['columns'][4]["search"]["value"]));
        }

        if(isset($_POST['columns'][5]["search"]["value"]) && !empty($_POST['columns'][5]["search"]["value"])){
            $this->db->like('COM_Name',trim($_POST['columns'][5]["search"]["value"]));
        }

        if(isset($_POST['columns'][6]["search"]["value"]) && !empty($_POST['columns'][6]["search"]["value"])){
            $this->db->like('CPC_Desc',trim($_POST['columns'][6]["search"]["value"]));
        }

        if(isset($_POST['columns'][7]["search"]["value"]) && !empty($_POST['columns'][7]["search"]["value"])){
            $this->db->like('CASE SP_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END',$_POST['columns'][7]["search"]["value"]);
        }

        // End

        if(isset($_POST["search"]["value"])){
            if($_POST["search"]["value"] != ''){
                $this->db->like("SP_ID", trim($_POST["search"]["value"]));  
                $this->db->or_like("SP_StoreName", trim($_POST["search"]["value"]));  
                $this->db->or_like("SP_Address", trim($_POST["search"]["value"]));  
                $this->db->or_like("COM_Name", trim($_POST["search"]["value"]));  
                $this->db->or_like("CPC_Desc", trim($_POST["search"]["value"]));
                $this->db->or_like("CASE SP_Active WHEN 1 THEN 'Active' ELSE 'Inactive' END", trim($_POST["search"]["value"]), false);
            }  
        }  
        
        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('SP_ID', 'DESC');  
        }
    }

    public function datatables($status = null){
        $this->query($status); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblStoreProfile');
        return $query->result_array();
    }

    public function get_filtered_data($status = null){  
        $this->query();  
        $query = $this->db->get('tblStoreProfile');
           return $query->num_rows();  
    }
      // END 

    public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */
    
         $this->db->delete('tblLocation',array('LOC_Id'=>$docno));

    }

     // DATA TABLE QUERY ENDS HERE 

     public function getAll($docno = null){
       $query_string = 'SELECT L.LOC_Id, L.LOC_FK_Company_id, L.LOC_Addrs, L.LOC_TelNum, L.LOC_FaxNum, L.LOC_Tin, L.LOC_Name, L.LOC_Active, L.LOC_FK_Type_id, C.COM_Name, CPC.CPC_Desc
                        FROM tblLocation AS L LEFT OUTER JOIN
                         tblCPCenter AS CPC ON L.LOC_FK_CPC_id = CPC.CPC_Id LEFT OUTER JOIN
                         tblCompany AS C ON L.LOC_FK_Company_id = C.COM_Id';
        if(!empty($docno)){
            $query_string .= ' WHERE ' . $this->sql_hash('LOC_Id') . ' = \''.$docno.'\' ';
            $result = $this->db->query($query_string)->row_array();
        }
        else{
            $result = $this->db->query($query_string)->result_array();
        }
        return $result;
    }

    public function getActiveCPC(){
        $query_string = 'SELECT CPC_Id, CPC_Desc
                         FROM tblCPCenter as CPC
                         WHERE CPC_Active = \'1\'';

        return $this->db->query($query_string)->result_array();
    }

    public function getActiveCompanyName(){
        $query_string = 'SELECT COM_Name , COM_Id
                        FROM tblCompany as C
                        WHERE COM_Active = \'1\'';

        return $this->db->query($query_string)->result_array();
    }


}