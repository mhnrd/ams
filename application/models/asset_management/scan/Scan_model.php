<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Scan_model extends MAIN_Model {

    private $id = NULL;
    private $order_column = array(null,'FA_ID','FA_Description','FA_DepartmentDesc');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data(){

        $this->db->select('FA_ID, FA_Description, AD_Desc as FA_DepartmentDesc');
        $this->db->join('tblAttributeDetail', 'AD_Id = FA_DepartmentID', 'left');
        $this->db->from('tblFA');  
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('FA_ID, FA_Description, AD_Desc as FA_DepartmentDesc');
        $this->db->join('tblAttributeDetail', 'AD_Id = FA_DepartmentID', 'left');
        // Individual column search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('FA_ID',$_POST['columns'][1]["search"]["value"]);
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('FA_Description',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('AD_Desc',$_POST['columns'][3]["search"]["value"]);
        }

        // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->group_start();
                $this->db->like("FA_ID", $_POST["search"]["value"]);  
                $this->db->or_like("FA_Description", $_POST["search"]["value"]);  
                $this->db->or_like('AD_Desc', $_POST["search"]["value"]);  
                $this->db->group_end();
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('FA_ID', 'DESC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblFA');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblFA');  
           return $query->num_rows();  
    }

    public function checkExistingIDandDesc($docno, $desc, $action){
        $output = array(
            'success'  => true,
            'message'  => ''
        );

        if ($action == 'add') {
            $sql = 'SELECT  FA_ID
                    FROM    tblFA
                    WHERE   FA_ID = \''.$docno.'\'';

            $FA_ID = $this->db->query($sql)->result_array();


            if (!empty($facs_id)) {
                $output = array(
                    'success'  => false,
                    'message'  => 'Asset ID already exists.'
                );

                return $output;
            }
        }


        $sql = 'SELECT  FA_Description
                FROM    tblFA
                WHERE   RTRIM(LTRIM(FA_Description)) = \''.trim($desc).'\' AND 
                        FA_ID <> \''.$docno.'\'';

        $facs_desc = $this->db->query($sql)->result_array();

        if (!empty($facs_desc)) {
            $output = array(
                'success'  => false,
                'message'  => 'Description already exists.'
            );

            return $output;
        }

        return $output;

    }

    public function searchItem($filter){

        $sql = 'SELECT      AD_Id, AD_Code + \' - \' + AD_Desc as Description
                FROM        tblAttributeDetail
                WHERE       AD_FK_Code = 400001 AND 
                            AD_Code + \' - \' + AD_Desc LIKE \'%'.$filter.'%\'
                ORDER BY    Description';

        return  $this->db->query($sql)->result_array();

    }
}