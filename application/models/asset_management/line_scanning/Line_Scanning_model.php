<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Line_scanning_model extends MAIN_Model {

	public function getLineScanningDetail($filter){

		$query_string = 'SELECT	FA_Description as [Equipment Name],
								[FA_SerialNo] as [Serial No.],
								[FA_ID] as [Asset ID],
								[FA_UsefulLifeNo] as [Life in Years],
								[FA_AcquisitionCost] as [Unit Cost],
								1 as [Qty],
								[FA_AcquisitionCost] as [Amount],
								[FA_DateUsed] as [Date Used in Production],
								0 as [Depreciation Cost/Year],
								0 as [Depreciation Cost/Month],
								0 as [Net Book Value],
								[FA_Status] as [Status],
								dept.AD_Desc as [Dept Name],
								bay.[SL_Name] as [Bay Name],
								line.[SL_Name] as [Line Name]
						FROM	[dbo].[tblFA]
								join [dbo].[tblAttributeDetail] as dept on dept.[AD_ID] = [FA_DepartmentID]
								join [dbo].[tblSubLocation] as bay on bay.SL_Id = FA_Bay
								join [dbo].[tblSubLocation] as line on line.SL_Id = FA_Bay
						WHERE	[FA_DepartmentID] = \''.$filter['FA_DepartmentID'].'\' AND
								[FA_AffiliatesID] = \''.$filter['FA_AffiliatesID'].'\' AND
								((select MONTH([FA_DateUsed])) = \''.$filter['Month'].'\' AND (select YEAR([FA_DateUsed])) = \''.$filter['Year'].'\')	
										
								';

								// [FA_ModelYear] = \''.$filter['FA_ModelYear'].'\' AND
								// [FA_Line] = \''.$filter['FA_Line'].'\' AND
								
		return $this->db->query($query_string)->result_array();

	}
}