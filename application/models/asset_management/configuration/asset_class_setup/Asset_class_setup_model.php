<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Asset_class_setup_model extends MAIN_Model {

    private $id = NULL;
    private $order_column = array(null,'FACS_ID','FACS_Description','FACS_UsefulLife');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data(){

        $this->db->select('FACS_ID, FACS_Description, FACS_UsefulLifeNo, CAST(CAST(FACS_UsefulLifeNo as int) as varchar) + \' \' + (CASE FACS_UsefulLifeNoUOM WHEN \'M\' THEN \'Month(s)\' WHEN \'Y\' THEN \'Year(s)\' ELSE \'\' END) as FACS_UsefulLife, FACS_UsefulLifeNoUOM');  
        $this->db->from('tblFAClassSetup');  
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables();
        // $this->print_lastquery();
        // die();
        $output['num_rows']          = $this->get_filtered_data();
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query(){
        $this->db->select('FACS_ID, FACS_Description, FACS_UsefulLifeNo, CAST(CAST(FACS_UsefulLifeNo as int) as varchar) + \' \' + (CASE FACS_UsefulLifeNoUOM WHEN \'M\' THEN \'Month(s)\' WHEN \'Y\' THEN \'Year(s)\' ELSE \'\' END) as FACS_UsefulLife, FACS_UsefulLifeNoUOM');  
        // Individual column search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('FACS_ID',$_POST['columns'][1]["search"]["value"]);
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('FACS_Description',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('CAST(FACS_UsefulLifeNo as varchar) + \'\' + (CASE FACS_UsefulLifeNoUOM WHEN \'M\' THEN \'Month(s)\' WHEN \'Y\' THEN \'Year(s)\' ELSE \'\' END)',$_POST['columns'][3]["search"]["value"]);
        }

        // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->group_start();
                $this->db->like("FACS_ID", $_POST["search"]["value"]);  
                $this->db->or_like("FACS_Description", $_POST["search"]["value"]);  
                $this->db->or_like('CAST(FACS_UsefulLifeNo as varchar) + \' \' + (CASE FACS_UsefulLifeNoUOM WHEN \'M\' THEN \'Month(s)\' WHEN \'Y\' THEN \'Year(s)\' ELSE \'\' END)', $_POST["search"]["value"]);  
                $this->db->group_end();
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('FACS_ID', 'DESC');  
        }
    }

    public function datatables(){
        $this->query(); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblFAClassSetup');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data(){  
           $this->query();  
           $query = $this->db->get('tblFAClassSetup');  
           return $query->num_rows();  
    }

    public function checkExistingIDandDesc($docno, $desc, $action){
        $output = array(
            'success'  => true,
            'message'  => ''
        );

        if ($action == 'add') {
            $sql = 'SELECT  FACS_ID
                    FROM    tblFAClassSetup
                    WHERE   FACS_ID = \''.$docno.'\'';

            $facs_id = $this->db->query($sql)->result_array();


            if (!empty($facs_id)) {
                $output = array(
                    'success'  => false,
                    'message'  => 'Asset Class ID already exists.'
                );

                return $output;
            }
        }


        $sql = 'SELECT  FACS_Description
                FROM    tblFAClassSetup
                WHERE   RTRIM(LTRIM(FACS_Description)) = \''.trim($desc).'\' AND 
                        FACS_ID <> \''.$docno.'\'';

        $facs_desc = $this->db->query($sql)->result_array();

        if (!empty($facs_desc)) {
            $output = array(
                'success'  => false,
                'message'  => 'Description already exists.'
            );

            return $output;
        }

        return $output;

    }
}