<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lapsing_schedule_model extends MAIN_Model {

	public function getLapsingScheduleDetail($filter){

		$query_string = 'SELECT	[FA_AssetCode] as [Code],
								[FA_ID] as [Asset ID],
								[FA_SupplierID] as [Supplier],
								[FA_Description] as [Asset Description],
								[FA_RefNo] as [Ref. No.],
								[FA_DateUsed] as [Date Placed in Services],
								[FA_DepartmentID] as [Department],
								[FA_AffiliatesID] as [Affiliates],
								[FA_UsefulLifeNo] as [Life in Months],
								[FA_UsefulLifeNo] as [Life in Years],
								[FA_ExchangeRate] as [Exchange Rate],
								[FA_AcquisitionCost] as [Acquisition Cost (USD)],
								0 as [Accu. Depre. Beginning (USD)],
								0 as [Monthly Depreciation (USD)],
								0 as [YTD Depreciation (USD)],
								0 as [Accu. Depre. Ending (USD)],
								0 as [Net Book Value (USD)],
								0 as [Accu. Depre. Beginning (PHP)],
								0 as [Monthly Depreciation (PHP)],
								0 as [YTD Depreciation (PHP)],
								0 as [Accu. Depre. Ending (PHP)],
								0 as [Net Book Value (PHP)]
						FROM 	[dbo].[tblFA]
						WHERE 	[FA_DateUsed] BETWEEN \''.$filter['dtFrom'].'\' AND \''.$filter['dtTo'].'\'';

		return $this->db->query($query_string)->result_array();

	}

}