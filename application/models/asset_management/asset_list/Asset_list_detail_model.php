<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Asset_list_detail_model extends MAIN_Model {

    private $id = NULL;
    private $order_column_movement = array('FAMH_Date', 'FAMH_RefNo', 'DepartmentDesc', 'AffiliatesDesc', 'FAMH_Remarks');
    private $order_column_mainte = array(null,'FAML_Date', 'FAML_Particular', 'FAML_Cost', 'FAML_Remarks');
    private $order_column_compo = array(null,'FAC_ItemID', 'FAC_Description', 'FAC_Qty', 'FAC_UnitCost', 'FAC_TotalCost');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data_movement($id){

        $this->db->select('FAMH_Date, FAMH_RefNo, (CASE FAMH_LocationID WHEN 0 THEN \'Disposed\' ELSE AD1.AD_Desc END) as DepartmentDesc, (CASE FAMH_AffiliatesID WHEN 0 THEN \'Disposed\' ELSE AD2.AD_Desc END) as AffiliatesDesc, FAMH_Remarks');
        $this->db->join('tblAttributeDetail as AD1', 'AD1.AD_Id = FAMH_LocationID', 'left');
        $this->db->join('tblAttributeDetail as AD2', 'AD2.AD_Id = FAMH_AffiliatesID', 'left');
        $this->db->from('tblFAMovementHistory');  
        $this->db->where('FAMH_FA_AssetID', $id);

        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables_movement($id);
        $output['num_rows']          = $this->get_filtered_data_movement($id);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query_movement($id){
        $this->db->select('FAMH_Date, FAMH_RefNo, (CASE FAMH_LocationID WHEN 0 THEN \'Disposed\' ELSE AD1.AD_Desc END) as DepartmentDesc, (CASE FAMH_AffiliatesID WHEN 0 THEN \'Disposed\' ELSE AD2.AD_Desc END) as AffiliatesDesc, FAMH_Remarks');
        $this->db->join('tblAttributeDetail as AD1', 'AD1.AD_Id = FAMH_LocationID', 'left');
        $this->db->join('tblAttributeDetail as AD2', 'AD2.AD_Id = FAMH_AffiliatesID', 'left');
        $this->db->where('FAMH_FA_AssetID', $id);
        // // Individual column search

        // // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->group_start();
                $this->db->like("FAMH_Date", $_POST["search"]["value"]);  
                $this->db->or_like("FAMH_RefNo", $_POST["search"]["value"]);  
                $this->db->or_like('(CASE FAMH_LocationID WHEN 0 THEN \'Disposed\' ELSE AD1.AD_Desc END)', $_POST["search"]["value"]);  
                $this->db->or_like('(CASE FAMH_AffiliatesID WHEN 0 THEN \'Disposed\' ELSE AD2.AD_Desc END)', $_POST["search"]["value"]);
                $this->db->or_like('FAMH_Remarks', $_POST["search"]["value"]);  
                $this->db->group_end();
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column_movement[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('FAMH_Date', 'DESC');  
        }
    }

    public function datatables_movement($id){
        $this->query_movement($id); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblFAMovementHistory');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data_movement($id){  
           $this->query_movement($id);  
           $query = $this->db->get('tblFAMovementHistory');  
           return $query->num_rows();  
    }

    /* datatable query starts here */
    public function table_data_maintenance($id){

        $this->db->select('FAML_FA_AssetID, FAML_FA_LineNo, FAML_Date, FAML_Particular, FAML_Cost, FAML_Remarks');
        $this->db->where('FAML_FA_AssetID', $id);
        $this->db->from('tblFAMaintenanceLog');  
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables_maintenance($id);
        $output['num_rows']          = $this->get_filtered_data_maintenance($id);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query_maintenance($id){
        $this->db->select('FAML_FA_AssetID, FAML_FA_LineNo, FAML_Date, FAML_Particular, FAML_Cost, FAML_Remarks');
        $this->db->where('FAML_FA_AssetID', $id);
        // // Individual column search

        // // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->group_start();
                $this->db->like("FAML_Date", $_POST["search"]["value"]);  
                $this->db->or_like("FAML_Particular", $_POST["search"]["value"]);  
                $this->db->or_like('FAML_Cost', $_POST["search"]["value"]);
                $this->db->or_like('FAML_Remarks', $_POST["search"]["value"]);  
                $this->db->group_end();
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column_mainte[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('FAML_FA_LineNo', 'DESC');  
        }
    }

    public function datatables_maintenance($id){
        $this->query_maintenance($id); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblFAMaintenanceLog');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data_maintenance($id){  
           $this->query_maintenance($id);  
           $query = $this->db->get('tblFAMaintenanceLog');  
           return $query->num_rows();  
    }

    /* datatable query starts here */
    public function table_data_components($id){

        $this->db->select('FAC_FA_AssetID, FAC_FA_LineNo, AD_Code as FAC_ItemCode, FAC_Description, FAC_Qty, FAC_UnitCost, FAC_TotalCost');
        $this->db->join('tblAttributeDetail', 'AD_Id = FAC_ItemID', 'left');
        $this->db->where('FAC_FA_AssetID', $id);
        $this->db->from('tblFAComponent');  
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables_components($id);
        $output['num_rows']          = $this->get_filtered_data_components($id);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query_components($id){
        $this->db->select('FAC_FA_AssetID, FAC_FA_LineNo, FAC_ItemID, AD_Code as FAC_ItemCode, FAC_Description, FAC_Qty, FAC_UnitCost, FAC_TotalCost');
        $this->db->join('tblAttributeDetail', 'AD_Id = FAC_ItemID', 'left');
        $this->db->where('FAC_FA_AssetID', $id);
        // // Individual column search

        // // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->group_start();
                $this->db->like("FAC_ItemID", $_POST["search"]["value"]);  
                $this->db->or_like("FAC_Description", $_POST["search"]["value"]);  
                $this->db->or_like('FAC_Qty', $_POST["search"]["value"]);
                $this->db->or_like('FAC_UnitCost', $_POST["search"]["value"]);
                $this->db->or_like('FAC_TotalCost', $_POST["search"]["value"]); 
                $this->db->group_end();
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column_compo[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('FAC_FA_LineNo', 'DESC');  
        }
    }

    public function datatables_components($id){
        $this->query_components($id); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblFAComponent');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data_components($id){  
           $this->query_components($id);  
           $query = $this->db->get('tblFAComponent');  
           return $query->num_rows();  
    }

    public function getLastLineNo($docno, $docno_field, $line_no_field, $table){

        $sql = 'SELECT      TOP (1) '.$line_no_field.'
                FROM        '.$table.'
                WHERE       '.$docno_field.' = \''.$docno.'\'
                ORDER BY    '.$line_no_field.' DESC';

        return $this->db->query($sql)->row_array()[$line_no_field] + 1;

    }
}