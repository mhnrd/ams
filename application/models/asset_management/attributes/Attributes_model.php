<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Attributes_model extends MAIN_Model {

    private $id = NULL;
    private $order_column = array(null,null,'AD_Code','AD_Desc','AD_Active');

    public function __construct() {
        parent::__construct();
    }

    /* datatable query starts here */
    public function table_data($attr_code){

        $this->db->select('AD_ID, AD_Code ,AD_Desc, AD_Active');  
        $this->db->from('tblAttributeDetail');
        $this->db->where('AD_FK_Code', $attr_code);  
        $result_count = $this->db->count_all_results();          

        $output['data']              = $this->datatables($attr_code);
        $output['num_rows']          = $this->get_filtered_data($attr_code);
        $output['count_all_results'] = $result_count;

        return $output;
    }

    public function query($attr_code){
        $this->db->select('AD_ID, AD_Code ,AD_Desc, CASE AD_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END as AD_Active', false); 
        $this->db->where('AD_FK_Code', $attr_code);
        // Individual column search

        if(isset($_POST['columns'][1]["search"]["value"]) && !empty($_POST['columns'][1]["search"]["value"])){
            $this->db->like('AD_Code',$_POST['columns'][1]["search"]["value"]);
        }

        if(isset($_POST['columns'][2]["search"]["value"]) && !empty($_POST['columns'][2]["search"]["value"])){
            $this->db->like('AD_Desc',$_POST['columns'][2]["search"]["value"]);
        }

        if(isset($_POST['columns'][3]["search"]["value"]) && !empty($_POST['columns'][3]["search"]["value"])){
            $this->db->like('CASE AD_Active WHEN 1 THEN \'Active\' ELSE \'Inactive\' END',$_POST['columns'][3]["search"]["value"], 'after');
        }

        // End

        if(isset($_POST["search"]["value"])){ 
            if($_POST["search"]["value"] != ''){  
                $this->db->group_start();
                $this->db->like("AD_Code", $_POST["search"]["value"]);  
                $this->db->or_like("AD_Desc", $_POST["search"]["value"]);  
                $this->db->or_like("CASE AD_Active WHEN 1 THEN 'Active' ELSE 'Inactive' END", $_POST["search"]["value"], 'after');  
                $this->db->group_end();
            }
        }  

        if(isset($_POST["order"])){  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
        }  
        else{  
            $this->db->order_by('AD_Code', 'DESC');  
        }
    }

    public function datatables($attr_code){
        $this->query($attr_code); 
        if(isset($_POST["length"])){
            if($_POST["length"] != -1){  
                $this->db->limit($_POST['length'], $_POST['start']);  
            }  
        }
        $query = $this->db->get('tblAttributeDetail');
        // $this->print_lastquery();  
        return $query->result_array();
    }

    public function get_filtered_data($attr_code){  
           $this->query($attr_code);  
           $query = $this->db->get('tblAttributeDetail');  
           return $query->num_rows();  
    }

    /* datatable query ends here */ 

    public function delete_data($docno){

        /* FOR FUTURE CODE INSERT SECURITY TO CHECK IF THE SELECTED DATA IS IN USED.
            --insert code here--
        */
    
        $this->db->trans_begin();
        $this->db->delete('tblAttribute',array('A_Code'=>$docno));

        if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return array('success'=>0);
        }
        else{
                $this->db->trans_commit();
                return array('success'=>1);
        }

    }

    public function getLastADCode(){

        $query_string = 'SELECT     TOP (1) ISNULL(AD_ID, 0) as AD_ID
                         FROM       tblAttributeDetail
                         ORDER BY   AD_ID DESC';

        return $this->db->query($query_string)->row_array()['AD_ID'] + 1;

    }

    public function getAttributesData($id){
        return $this->db->select('LOWER(LTRIM(RTRIM(AD_Desc))) AS Description, LTRIM(RTRIM(AD_Id)) AS Id',false)
                        ->where('AD_FK_Code',$id)
                        ->where('ISNULL(AD_Active, 0) = 1',null,false)
                        ->order_by('Description')
                        ->get('tblAttributeDetail')
                        ->result_array()
        ;

    }

    public function getAttributeName($module_id, $id){
        return $this->db->select('LOWER(LTRIM(RTRIM(AD_Desc))) AS Description, LTRIM(RTRIM(AD_Id)) AS Id',false)
                        ->where('AD_FK_Code',$module_id)
                        ->where('AD_Id',$id)
                        ->where('ISNULL(AD_Active, 0) = 1',null,false)
                        ->order_by('Description')
                        ->get('tblAttributeDetail')
                        ->row_array()['Description']
        ;

    }

    public function checkAttrExists($module_id, $ad_code){
        $query_string = 'SELECT     COUNT(*) as rows
                         FROM       tblAttributeDetail
                         WHERE      AD_FK_Code = \''.$module_id.'\' AND AD_Code = \''.$ad_code.'\'';

        $numrows = $this->db->query($query_string)->row_array()['rows'];

        if($numrows > 0){
            $output = array(
                            'success'=> false
                        ,   'message'=> implode( getErrorMessage(7), ' , ' ));

            return $output;

        }
        else{
            $output = array(
                            'success'=>true
                        ,   'message'=>'');

            return $output;
        }
    }       

}
