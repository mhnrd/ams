<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventory_report_model extends MAIN_Model {

	public function getInventoryReportDetail($filter){

		$query_string = 'SELECT	[FA_AssetCode] as [Asset Code],
								[FA_Description] as [Asset Description],
								0 as [Month 1 Count],
								0 as [Month 2 Count],
								0 as [Changes],
								0 as [Receive],
								0 as [Transfer],
								0 as [Retired]
						FROM	[dbo].[tblFA]
						WHERE	[FA_DepartmentID] = \''.$filter['FA_DepartmentID'].'\' AND
								
								((select MONTH([FA_DateUsed])) = \''.$filter['Month'].'\' AND (select YEAR([FA_DateUsed])) = \''.$filter['Year'].'\')';

								// [FA_Model] = \''.$filter['FA_Model'].'\' AND
								// [FA_Family] = \''.$filter['FA_Family'].'\'
								// [FA_Line] = \''.$filter['FA_Line'].'\' AND
								
								// [FA_ModelYear] = \''.$filter['FA_ModelYear'].'\' AND

		return $this->db->query($query_string)->result_array();

	}
}