<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class Line_scanning extends MAIN_Controller {
	protected $number_series		= 500002;
	protected $header_table 		= '';
	protected $header_doc_no_field	= '';
	protected $header_status_field	= '';

	public function __construct(){
		parent::__construct();		
		$this->is_secure = true;
		$this->data['header_table'] 		= $this->header_table;
		$this->data['header_doc_no_field'] 	= $this->header_doc_no_field;
		$this->data['header_status_field'] 	= $this->header_status_field;
		$this->data['number_series'] 		= $this->number_series;
		$this->data['access_header']		= $this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		= $this->user_model->getDetailAccess($this->number_series);
		$this->cols 						= array('FA_DepartmentID', 'FA_AffiliatesID','FA_ModelYear','FA_Line');
	}

	public function index(){

		// $this->data['Company'] = getCompanyDetails('tblCompany', array('COM_Id', 'COM_Name'));
		$this->data['department'] = getAttributeList(400003);
		$this->data['affiliates'] = getAttributeList(400004);
		$this->data['FA_Line'] = getLineNo('FA_Line');
		$this->data['header'] = $this->getData($this->data['department'], $this->data['affiliates'], $this->data['FA_Line']);
		$this->load_page('asset_management/reports/line_scanning');
	}

	public function getData($filter){
        $noseries_model = $this->load_model('administration/master_file/bom/noseries_model');
        $asset_list_model = $this->load_model('asset_management/asset_list/asset_list_model');

            $output = array(
                'FA_DepartmentID'	=> '',
                'FA_AffiliatesID'	=> '',
                'FA_ModelYear'		=> '',
                'FA_Line'			=> ''
            );

			return $output;
    }

	public function print_document(){
		// $data['company'] = getCompanyByPayGroup($filters['PayGrp']);
		// $this->data['departmentName'] = getDepartmentName('102');

		
		$line_scanning_model = $this->load_model('asset_management/line_scanning/Line_scanning_model');
		$filter = $this->input->post();
		$data = array();
		$data['detail'] = $line_scanning_model->getLineScanningDetail($filter);
	
		$xlsObj = array();

		$xlsObj = $this->cellDetails($xlsObj, $data['detail'], $filter['Month'], $filter['Year'], $filter['FA_DepartmentID'], $filter['FA_AffiliatesID'], $filter['FA_ModelYear'], $filter['FA_Line']);

		$spreadsheet = $this->spreadsheetSetup($xlsObj);

		echo json_encode(array("success" => true, "file_name"=>$this->generateExcelPrintout($spreadsheet, 'Line Scanning')));
		// echo getDepartmentName('102');

	}

	public function cellDetails($xlsObj, $data, $month, $year, $department, $affiliates, $FA_ModelYear, $FA_Line){
		

		//Main Header
		array_push($xlsObj,
			array(
				"cell"  	=> 'A1',
				"value"  	=> 'LINE SCANNING REPORT (per month)'
			),
			array(
				"cell"  	=> 'A3',
				"value"  	=> 'Division: '.getAttributeName($department)
			),
			array(
				"cell"		=> 'B3',
				"value"		=> 'Model / Carline: '.getAttributeName($affiliates)
			),
			array(
				"cell"		=> 'C3',
				"value"		=> 'Model Year: '.$FA_ModelYear
			),
			array(
				"cell"		=> 'D3',
				"value"		=> 'Line No. : '.$FA_Line
			),
			array(
				"cell"		=> 'I3',
				"value"		=> 'Month / Year: '.$month.' '.$year.''
			),
			array(
				"cell"		=> 'K1',
				"value"		=> 'Run Date: '.date('m/d/Y').''
			),
			array(
				"cell"		=> 'K2',
				"value"		=> 'Run Time: '.date('g:i:sa').''
			),
			array(
				"cell"		=> 'K3',
				"value"		=> 'Run By: '.getCurrentUser()['login-user'].''
			)
		);

		array_push($xlsObj,
			// Detail Header
			array(
				'cell' 		=> 'A7',
				'value'  	=> 'Equipment Name'
			),
			array(
				'cell' 		=> 'B7',
				'value'  	=> 'Serial No.'
			),
			array(
				'cell' 		=> 'C7',
				'value'  	=> 'Asset ID'
			),
			array(
				'cell' 		=> 'D7',
				'value'  	=> 'Life in Years'
			),
			array(
				'cell' 		=> 'E7',
				'value'  	=> 'Unit Cost'
			),
			array(
				'cell' 		=> 'F7',
				'value'  	=> 'Qty'
			),
			array(
				'cell' 		=> 'G7',
				'value'  	=> 'Amount'
			),
			array(
				'cell' 		=> 'H7',
				'value'  	=> 'Date Used in Production'
			),
			array(
				'cell' 		=> 'I7',
				'value'  	=> 'Depreciation Cost/Year'
			),
			array(
				'cell' 		=> 'J7',
				'value'  	=> 'Depreciation Cost/Month'
			),
			array(
				'cell' 		=> 'K7',
				'value'  	=> 'Net Book Value'
			),
			array(
				'cell' 		=> 'L7',
				'value'  	=> 'Status'
			)
		);

		foreach ($data as $key => $value) {
		 	$nextDCol = 8;

			array_push($xlsObj,
				// Details
				array(
					'cell' 		=> 'A'.($key+$nextDCol),
					'value'  	=> strtoupper($value['Equipment Name'])
				),
				array(
					'cell' 		=> 'B'.($key+$nextDCol),
					'value'  	=> $value['Serial No.']
				),
				array(
					'cell' 		=> 'C'.($key+$nextDCol),
					'value'  	=> strtoupper($value['Asset ID'])
				),
				array(
					'cell' 		=> 'D'.($key+$nextDCol),
					'value'  	=> $value['Life in Years']
				),
				array(
					'cell' 		=> 'E'.($key+$nextDCol),
					'value'  	=> $value['Unit Cost']
				),
				array(
					'cell' 		=> 'F'.($key+$nextDCol),
					'value'  	=> $value['Qty']
				),
				array(
					'cell' 		=> 'G'.($key+$nextDCol),
					'value'  	=> number_format($value['Amount'], 2)
				),
				array(
					'cell' 		=> 'H'.($key+$nextDCol),
					'value'  	=> date_format(date_create($value['Date Used in Production']), 'm/d/Y')
				),
				array(
					'cell' 		=> 'I'.($key+$nextDCol),
					'value'  	=> number_format($value['Depreciation Cost/Year'], 2)
				),
				array(
					'cell' 		=> 'J'.($key+$nextDCol),
					'value'  	=> number_format($value['Depreciation Cost/Month'], 2)
				),
				array(
					'cell' 		=> 'K'.($key+$nextDCol),
					'value'  	=> number_format($value['Net Book Value'], 2)
				),
				array(
					'cell' 		=> 'L'.($key+$nextDCol),
					'value'  	=> $value['Status']
				)
			);
			
		}

	 	return $xlsObj;
	}

	public function spreadsheetSetup($xlsObj){
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet()->setTitle("Line Scanning");

			$styleArray = array(
	 		'borders' => array(
	 		'allBorders' => array(
  			'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
  			'color' => array('argb' => '000000'),
			),
			),
			);

  			$sheet ->getStyle('A7:L7')->applyFromArray($styleArray);

		// Color Fill
		foreach (range('A', 'L') as $key => $value) {
			$spreadsheet->getActiveSheet()->getStyle($value.'1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_NONE)->getStartColor()->setARGB('FFFFFF');
			$spreadsheet->getActiveSheet()->getStyle($value.'7')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('C0C0C0');

			$spreadsheet->getActiveSheet()->getColumnDimension($value)->setAutoSize(true);
			$sheet->getStyle($value.'7')->getAlignment()->setHorizontal('center');
			$spreadsheet->getActiveSheet()->getStyle($value)->getAlignment()->setHorizontal('center');
			$spreadsheet->getActiveSheet()->mergeCells('A1:B1');
			$spreadsheet->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setHorizontal('left');
			$spreadsheet->getActiveSheet()->getStyle('A2:C2')->getAlignment()->setHorizontal('left');
			$spreadsheet->getActiveSheet()->getStyle('A3:D3')->getAlignment()->setHorizontal('left');
			$spreadsheet->getActiveSheet()->getStyle('I3')->getAlignment()->setHorizontal('left');
			$spreadsheet->getActiveSheet()->mergeCells('K1:L1');
			$spreadsheet->getActiveSheet()->mergeCells('K2:L2');
			$spreadsheet->getActiveSheet()->mergeCells('K3:L3');
			$spreadsheet->getActiveSheet()->getStyle('K1:L1')->getAlignment()->setHorizontal('left');
			$spreadsheet->getActiveSheet()->getStyle('K2:L2')->getAlignment()->setHorizontal('left');
			$spreadsheet->getActiveSheet()->getStyle('K3:L3')->getAlignment()->setHorizontal('left');
		}

		foreach ($xlsObj as $key => $value) {
			$sheet->setCellValue($value['cell'], $value['value']);
		}

		// Number Format
		// $spreadsheet->getActiveSheet()->getStyle('B6:B'.(count($xlsObj) + 5))->getNumberFormat()->setFormatCode('#,##0.00');

		return $spreadsheet;
	}
}