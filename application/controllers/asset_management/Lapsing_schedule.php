<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class Lapsing_schedule extends MAIN_Controller {
	protected $number_series		= 500001;
	protected $header_table 		= '';
	protected $header_doc_no_field	= '';
	protected $header_status_field	= '';

	public function __construct(){
		parent::__construct();		
		$this->is_secure = true;
		$this->data['header_table'] 		= $this->header_table;
		$this->data['header_doc_no_field'] 	= $this->header_doc_no_field;
		$this->data['header_status_field'] 	= $this->header_status_field;
		$this->data['number_series'] 		= $this->number_series;
	}

	public function index(){

		$this->data['Company'] = getCompanyDetails('tblCompany', array('COM_Id', 'COM_Name'));
		
		$this->load_page('asset_management/reports/lapsing_schedule');
	}

	public function print_document(){
		$lapsing_schedule_model = $this->load_model('asset_management/lapsing_schedule/Lapsing_schedule_model');
		$filter = $this->input->post();
		$data = array();
		$data['detail'] = $lapsing_schedule_model->getLapsingScheduleDetail($filter);

		$xlsObj = array();

		$xlsObj = $this->cellDetails($xlsObj, $data['detail'], $filter['dtFrom'], $filter['dtTo']);

		$spreadsheet = $this->spreadsheetSetup($xlsObj);

		echo json_encode(array("success" => true, "file_name"=>$this->generateExcelPrintout($spreadsheet, 'Lapsing Schedule')));

	}

	public function cellDetails($xlsObj, $data, $dtFrom, $dtTo){

		//Hardcoded Header
		array_push($xlsObj,
			array(
				"cell"  	=> 'A1',
				"value"  	=> 'Company: YAZAKI-TORRES MANUFACTURING, INC.'
			),
			array(
				"cell"  	=> 'A2',
				"value"  	=> 'Book: YTMI ASSET BOOK'
			),
			array(
				"cell"  	=> 'A3',
				"value"  	=> 'Deprecation Method: STL'
			),
			array(
				"cell"		=> 'L6',
				"value"		=> 'USD'
			),
			array(
				"cell"		=> 'R6',
				"value"		=> 'PHP'
			),
			array(
				"cell"		=> 'M1',
				"value"		=> 'YTMI LAPSING SCHEDULE'
			),
			array(
				"cell"		=> 'M2',
				"value"		=> 'Date From '.$dtFrom.' to '.$dtTo.''
			),
			array(
				"cell"		=> 'V1',
				"value"		=> 'Run Date: '.date('m/d/Y').''
			),
			array(
				"cell"		=> 'V2',
				"value"		=> 'Run Time: '.date('g:i:sa').''
			),
			array(
				"cell"		=> 'V3',
				"value"		=> 'Run By: '.getCurrentUser()['login-user'].''
			)
		);

		array_push($xlsObj,
			// Header
			array(
				'cell' 		=> 'A7',
				'value'  	=> 'Code'
			),
			array(
				'cell' 		=> 'B7',
				'value'  	=> 'Asset ID'
			),
			array(
				'cell' 		=> 'C7',
				'value'  	=> 'Supplier'
			),
			array(
				'cell' 		=> 'D7',
				'value'  	=> 'Asset Description'
			),
			array(
				'cell' 		=> 'E7',
				'value'  	=> 'Ref. No.'
			),
			array(
				'cell' 		=> 'F7',
				'value'  	=> 'Date Placed in Services'
			),
			array(
				'cell' 		=> 'G7',
				'value'  	=> 'Department'
			),
			array(
				'cell' 		=> 'H7',
				'value'  	=> 'Affiliates'
			),
			array(
				'cell' 		=> 'I7',
				'value'  	=> 'Life in Months'
			),
			array(
				'cell' 		=> 'J7',
				'value'  	=> 'Life in Years'
			),
			array(
				'cell' 		=> 'K7',
				'value'  	=> 'Exchange Rate'
			),
			array(
				'cell' 		=> 'L7',
				'value'  	=> 'Acquisition Cost'
			),
			array(
				'cell' 		=> 'M7',
				'value'  	=> 'Accu. Depre. Beginning'
			),
			array(
				'cell' 		=> 'N7',
				'value'  	=> 'Monthly Depreciation'
			),
			array(
				'cell' 		=> 'O7',
				'value'  	=> 'YTD Depreciation'
			),
			array(
				'cell' 		=> 'P7',
				'value'  	=> 'Accu. Depre. Ending'
			),
			array(
				'cell' 		=> 'Q7',
				'value'  	=> 'Net Book Value'
			),
			array(
				'cell' 		=> 'R7',
				'value'  	=> 'Accu. Depre. Beginning'
			),
			array(
				'cell' 		=> 'S7',
				'value'  	=> 'Monthly Depreciation'
			),
			array(
				'cell' 		=> 'T7',
				'value'  	=> 'YTD Depreciation'
			),
			array(
				'cell' 		=> 'U7',
				'value'  	=> 'Accu. Depre. Ending'
			),
			array(
				'cell' 		=> 'V7',
				'value'  	=> 'Net Book Value'
			)
		);

		foreach ($data as $key => $value) {
		 	$nextDCol = 8;

			array_push($xlsObj,
				// Details
				array(
					'cell' 		=> 'A'.($key+$nextDCol),
					'value'  	=> $value['Code']
				),
				array(
					'cell' 		=> 'B'.($key+$nextDCol),
					'value'  	=> $value['Asset ID']
				),
				array(
					'cell' 		=> 'C'.($key+$nextDCol),
					'value'  	=> strtoupper($value['Supplier'])
				),
				array(
					'cell' 		=> 'D'.($key+$nextDCol),
					'value'  	=> strtoupper($value['Asset Description'])
				),
				array(
					'cell' 		=> 'E'.($key+$nextDCol),
					'value'  	=> strtoupper($value['Ref. No.'])
				),
				array(
					'cell' 		=> 'F'.($key+$nextDCol),
					'value'  	=> date_format(date_create($value['Date Placed in Services']), 'm/d/Y')
				),
				array(
					'cell' 		=> 'G'.($key+$nextDCol),
					'value'  	=> $value['Department']
				),
				array(
					'cell' 		=> 'H'.($key+$nextDCol),
					'value'  	=> $value['Affiliates']
				),
				array(
					'cell' 		=> 'I'.($key+$nextDCol),
					'value'  	=> $value['Life in Months']
				),
				array(
					'cell' 		=> 'J'.($key+$nextDCol),
					'value'  	=> $value['Life in Years']
				),
				array(
					'cell' 		=> 'K'.($key+$nextDCol),
					'value'  	=> number_format($value['Exchange Rate'], 2)
				),
				array(
					'cell' 		=> 'L'.($key+$nextDCol),
					'value'  	=> number_format($value['Acquisition Cost (USD)'], 2)
				),
				array(
					'cell' 		=> 'M'.($key+$nextDCol),
					'value'  	=> number_format($value['Accu. Depre. Beginning (USD)'], 2)
				),
				array(
					'cell' 		=> 'N'.($key+$nextDCol),
					'value'  	=> number_format($value['Monthly Depreciation (USD)'], 2)
				),
				array(
					'cell' 		=> 'O'.($key+$nextDCol),
					'value'  	=> number_format($value['YTD Depreciation (USD)'], 2)
				),
				array(
					'cell' 		=> 'P'.($key+$nextDCol),
					'value'  	=> number_format($value['Accu. Depre. Ending (USD)'], 2)
				),
				array(
					'cell' 		=> 'Q'.($key+$nextDCol),
					'value'  	=> number_format($value['Net Book Value (USD)'], 2)
				),
				array(
					'cell' 		=> 'R'.($key+$nextDCol),
					'value'  	=> number_format($value['Accu. Depre. Beginning (PHP)'], 2)
				),
				array(
					'cell' 		=> 'S'.($key+$nextDCol),
					'value'  	=> number_format($value['Monthly Depreciation (PHP)'], 2)
				),
				array(
					'cell' 		=> 'T'.($key+$nextDCol),
					'value'  	=> number_format($value['YTD Depreciation (PHP)'], 2)
				),
				array(
					'cell' 		=> 'U'.($key+$nextDCol),
					'value'  	=> number_format($value['Accu. Depre. Ending (PHP)'], 2)
				),
				array(
					'cell' 		=> 'V'.($key+$nextDCol),
					'value'  	=> number_format($value['Net Book Value (PHP)'], 2)
				)
			);
			
		}

	 	return $xlsObj;
	}

	public function spreadsheetSetup($xlsObj){
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet()->setTitle("Lapsing Schedule");

			$styleArray = array(
	 		'borders' => array(
	 		'allBorders' => array(
  			'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
  			'color' => array('argb' => '000000'),
			),
			),
			);

  			$sheet ->getStyle('A7:V7')->applyFromArray($styleArray);
  			$sheet ->getStyle('L6:V6')->applyFromArray($styleArray);

		// Color Fill
		foreach (range('A', 'V') as $key => $value) {
			$spreadsheet->getActiveSheet()->getStyle($value.'1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_NONE)->getStartColor()->setARGB('FFFFFF');
			$spreadsheet->getActiveSheet()->getStyle($value.'7')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('C0C0C0');

			$spreadsheet->getActiveSheet()->getColumnDimension($value)->setAutoSize(true);
			$sheet->getStyle($value.'7')->getAlignment()->setHorizontal('center');
			$spreadsheet->getActiveSheet()->getStyle($value)->getAlignment()->setHorizontal('center');
			$spreadsheet->getActiveSheet()->mergeCells('A1:D1');
			$spreadsheet->getActiveSheet()->mergeCells('A2:D2');
			$spreadsheet->getActiveSheet()->mergeCells('A3:D3');
			$spreadsheet->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal('left');
			$spreadsheet->getActiveSheet()->getStyle('A2:D2')->getAlignment()->setHorizontal('left');
			$spreadsheet->getActiveSheet()->getStyle('A3:D3')->getAlignment()->setHorizontal('left');
			$spreadsheet->getActiveSheet()->mergeCells('L6:Q6');
			$spreadsheet->getActiveSheet()->mergeCells('R6:V6');
			$spreadsheet->getActiveSheet()->getStyle('L6:Q6')->getAlignment()->setHorizontal('center');
			$spreadsheet->getActiveSheet()->getStyle('R6:V6')->getAlignment()->setHorizontal('center');
			$spreadsheet->getActiveSheet()->mergeCells('M1:N1');
			$spreadsheet->getActiveSheet()->mergeCells('M2:N2');
			$spreadsheet->getActiveSheet()->getStyle('M1:N1')->getAlignment()->setHorizontal('center');
			$spreadsheet->getActiveSheet()->getStyle('M2:N2')->getAlignment()->setHorizontal('center');
			$spreadsheet->getActiveSheet()->getStyle('V1')->getAlignment()->setHorizontal('left');
			$spreadsheet->getActiveSheet()->getStyle('V2')->getAlignment()->setHorizontal('left');
			$spreadsheet->getActiveSheet()->getStyle('V3')->getAlignment()->setHorizontal('left');
		}

		foreach ($xlsObj as $key => $value) {
			$sheet->setCellValue($value['cell'], $value['value']);
		}

		// Number Format
		// $spreadsheet->getActiveSheet()->getStyle('B6:B'.(count($xlsObj) + 5))->getNumberFormat()->setFormatCode('#,##0.00');

		return $spreadsheet;
		
	}
}