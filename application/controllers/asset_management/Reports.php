<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends MAIN_Controller{

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
	}

	public function index(){

		$this->load_page($this->uri->segment(1).'/reports/index');

	}
	
}