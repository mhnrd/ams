<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Asset_list extends MAIN_Controller {

	protected $number_series		=	300000;
	protected $header_table			=	'tblFA';
	protected $header_doc_no_field	=	'FA_ID';
	protected $header_status_field	=	'';
	protected $module_name			=	'Asset List';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->data['header_table']					= 	$this->header_table;
		$this->data['header_doc_no_field']			=	$this->header_doc_no_field;
		$this->data['header_status_field']			=	$this->header_status_field;
		$this->data['number_series']				=	$this->number_series;
		$this->data['access_header']				=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_header_maintenance']	=	$this->user_model->getHeaderAccess(300003);
		$this->data['access_header_component']		=	$this->user_model->getHeaderAccess(300004);
		$this->data['access_detail']				=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						= 	array('FA_ID', 'FA_RFID', 'FA_ItemID', 'FA_Description','FA_AssetClass','FA_RefNo','FA_DateUsed', 'FA_SupplierID','FA_DepartmentID','FA_AffiliatesID','FA_Status','FA_AcquisitionDate','FA_SerialNo','FA_AcquisitionCost', 'FA_ExchangeRate', 'FA_WarrantyPeriodNo' ,'FA_WarrantyPeriodUOM','FA_UsefulLifeNo','FA_UsefulLifeNoUOM','FA_DepreMonthly', 'AD_Desc as FA_ItemDesc', 'AD_Code as FA_ItemCode', 'FA_AcquisitionCostCY', 'FA_AcquisitionNetBookCY', 'FA_AssetCode', 'FA_AssetType', 'FA_Model', 'FA_ModelYear', 'FA_Bay', 'FA_Line', 'FA_Family', 'FA_Process');
	}

	public function index(){

		$this->data['table_hdr'] = array('asset_list'	=> array(
													'buttons' 				=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline add-header" 
																									data-toggle="tooltip" data-placement="top" 
																									title="Add" type="button" href="'.DOMAIN.'asset_management/asset_list/add"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5px; vertical-align: middle' 
																				), 
													'asset_id' 				=>	array(
																					'title' 	=>	'Asset ID',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 25%; vertical-align: middle'
																				),
													'description' 			=>	array(
																					'title' 	=>	'Description',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
													'division' 				=>	array(
																					'title' 	=>	'Division',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
													'date_placed_in_services' =>	array(
																					'title' 	=>	'Date Placed In Services',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
													'car_line' 				=>	array(
																					'title' 	=>	'Car Line',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
													'net_book_value'		=>	array(
																					'title' 	=>	'Net Book Value',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
													'status' 				=>	array(
																					'title' 	=>	'Status',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				)
												)

									);

		$this->data['filter'] = array(

			array(
				"name"  => "Asset ID",
				"value"  => 1
			),
			array(
				"name"  => "Description",
				"value"  => 2
			),
			array(
				"name"  => "Division",
				"value"  => 3
			),
			array(
				"name"  => "Date Placed In Services",
				"value"  => 4
			),
			array(
				"name"  => "Car Line",
				"value"  => 5
			),
			array(
				"name"  => "Net Book Value",
				"value"  => 6
			),
			array(
				"name"  => "Status",
				"value"  => 7
			)

		);
		
		$this->data['dtl_center'] = array(0 , 1 , 3 , 4 , 5 , 7);

		$this->load_page('asset_management/asset_list/index');
	}

	public function add(){
		$this->data['type'] = 'add';
		$this->data['doc_no'] = '';
		$this->data['item'] = getAttributeList(400003);
		$this->data['supplier'] = getAttributeList(400002);
		$this->data['department'] = getAttributeList(400003);
		$this->data['affiliates'] = getAttributeList(400004);
		$this->data['currency'] = getAttributeList(400005);
		$this->data['asset_class'] = getAssetClassList();
		$this->data['asset_code'] = getAttributeList(400007);
		$this->data['asset_type'] = getAttributeList(400008);
		$this->data['process_type'] = getAttributeList(400009);
		$this->data['header'] = $this->getData($this->data['type']);

		// Revised/Added by JDC 071720241
		$this->data['asset_bay'] = getBayList();
		$this->data['asset_line'] = getLineList();

		$this->data['table_movement'] = array('table_id_movement'	=> array(
													
													'Date' 					=>	array(
																					'title' 	=>	'Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Reference' 			=>	array(
																					'title' 	=>	'Reference',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Location' 				=>	array(
																					'title' 	=>	'Location',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Affiliates' 			=>	array(
																					'title' 	=>	'Affiliates',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Remarks' 				=>	array(
																					'title' 	=>	'Remarks',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
												)

									);
		
		$this->data['col_center_movement'] = array(0);

		$this->data['table_maintenance'] = array('table_id_maintenance'	=> array(
			
													'Date' 					=>	array(
																					'title' 	=>	'Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'Particular' 			=>	array(
																					'title' 	=>	'Particular',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'Cost' 					=>	array(
																					'title' 	=>	'Cost',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Remarks' 				=>	array(
																					'title' 	=>	'Remarks',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
												)

									);
		
		$this->data['col_center_maintenance'] = array(0);

		$this->data['table_components'] = array('table_id_components'	=> array(
													'Item Ref. ID' 			=>	array(
																					'title' 	=>	'Item Ref. ID',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Description' 			=>	array(
																					'title' 	=>	'Description',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'Qty' 					=>	array(
																					'title' 	=>	'Qty',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'Cost' 					=>	array(
																					'title' 	=>	'Cost',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Total Cost' 			=>	array(
																					'title' 	=>	'Total Cost',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				)
												)

									);
		
		$this->data['col_center_components'] = array(0,1);
		

		$this->load_page('asset_management/asset_list/form');
	}

	public function update(){
		if (!isset($_GET['id'])) {
			$this->redirect(DOMAIN.'asset_management/asset_list');
		}

		$this->data['type'] = 'update';
		$this->data['doc_no'] = $_GET['id'];
		$this->data['item'] = getAttributeList(400003);
		$this->data['supplier'] = getAttributeList(400002);
		$this->data['department'] = getAttributeList(400003);
		$this->data['affiliates'] = getAttributeList(400004);
		$this->data['currency'] = getAttributeList(400005);
		$this->data['asset_class'] = getAssetClassList();
		$this->data['asset_code'] = getAttributeList(400007);
		$this->data['asset_type'] = getAttributeList(400008);
		$this->data['process_type'] = getAttributeList(400009);
		$this->data['header'] = $this->getData($this->data['type'], $this->data['doc_no']);

		// Revised/Added by JDC 071720241
		$this->data['asset_bay'] = getBayList();
		$this->data['asset_line'] = getLineList();

		$this->data['table_movement'] = array('table_id_movement'	=> array(
													
													'Date' 					=>	array(
																					'title' 	=>	'Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Reference' 			=>	array(
																					'title' 	=>	'Reference',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Location' 				=>	array(
																					'title' 	=>	'Location',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Affiliates' 			=>	array(
																					'title' 	=>	'Affiliates',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Remarks' 				=>	array(
																					'title' 	=>	'Remarks',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
												)

									);
		
		$this->data['col_center_movement'] = array(0);

		$add_button_maintenance = '';

		if ($this->data['access_header_maintenance']['RA_Add'] && $this->data['header']['FA_Status'] != 'Disposed') {
			$add_button_maintenance = '<a class="btn btn-success btn-xs 
												btn-outline add-maintenance" 
												data-toggle="tooltip" id="MaintenanceLog" data-placement="top" 
												title="Add" type="button" href="javascript:void(0)"><i
												class="fa
												fa-plus"></i></a>';
		}

		$this->data['table_maintenance'] = array('table_id_maintenance'	=> array(
													'buttons' 				=> 	array(
																					'title' 	=>	$add_button_maintenance,
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5px; vertical-align: middle' 
																				),
			
													'Date' 					=>	array(
																					'title' 	=>	'Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'Particular' 			=>	array(
																					'title' 	=>	'Particular',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'Cost' 					=>	array(
																					'title' 	=>	'Cost',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Remarks' 				=>	array(
																					'title' 	=>	'Remarks',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
												)

									);
		
		$this->data['col_center_maintenance'] = array(0);

		$add_button_components = '';

		if ($this->data['access_header_component']['RA_Add'] && $this->data['header']['FA_Status'] != 'Disposed') {
			$add_button_components = '<a class="btn btn-success btn-xs 
												btn-outline add-header" 
												data-toggle="tooltip" data-placement="top" 
												title="Add" type="button" href="javascript:void(0)"><i
												class="fa
												fa-plus"></i></a>';
		}

		$this->data['table_components'] = array('table_id_components'	=> array(

													'buttons' 				=> 	array(
																					'title' 	=>	$add_button_components,
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5px; vertical-align: middle' 
																				),
													'Item Ref. ID' 			=>	array(
																					'title' 	=>	'Item Ref. ID',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Description' 			=>	array(
																					'title' 	=>	'Description',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'Qty' 					=>	array(
																					'title' 	=>	'Qty',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'Cost' 					=>	array(
																					'title' 	=>	'Cost',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Total Cost' 			=>	array(
																					'title' 	=>	'Total Cost',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				)
												)

									);
		
		$this->data['col_center_components'] = array(0,1);
		

		$this->load_page('asset_management/asset_list/form');
	}


	public function view(){
		if (!isset($_GET['id'])) {
			$this->redirect(DOMAIN.'asset_management/asset_list');
		}

		$this->data['type'] = 'view';
		$this->data['doc_no'] = $_GET['id'];
		$this->data['item'] = getAttributeList(400003);
		$this->data['supplier'] = getAttributeList(400002);
		$this->data['department'] = getAttributeList(400003);
		$this->data['affiliates'] = getAttributeList(400004);
		$this->data['currency'] = getAttributeList(400005);
		$this->data['asset_class'] = getAssetClassList();
		$this->data['asset_bay'] = getBayList();
		$this->data['asset_line'] = getLineList();
		$this->data['asset_code'] = getAttributeList(400007);
		$this->data['asset_type'] = getAttributeList(400008);
		$this->data['process_type'] = getAttributeList(400009);
		$this->data['bay_no'] = getBayNoTest('101');
		$this->data['header'] = $this->getData($this->data['type'], $this->data['doc_no']);

		// Revised/Added by JDC 071720241
		$this->data['asset_bay'] = getBayList();
		$this->data['asset_line'] = getLineList();

		$this->data['table_movement'] = array('table_id_movement'	=> array(
													
													'Date' 					=>	array(
																					'title' 	=>	'Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Reference' 			=>	array(
																					'title' 	=>	'Reference',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Location' 				=>	array(
																					'title' 	=>	'Location',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Affiliates' 			=>	array(
																					'title' 	=>	'Affiliates',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Remarks' 				=>	array(
																					'title' 	=>	'Remarks',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
												)

									);
		
		$this->data['col_center_movement'] = array(0);

		$this->data['table_maintenance'] = array('table_id_maintenance'	=> array(
			
													'Date' 					=>	array(
																					'title' 	=>	'Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'Particular' 			=>	array(
																					'title' 	=>	'Particular',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'Cost' 					=>	array(
																					'title' 	=>	'Cost',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Remarks' 				=>	array(
																					'title' 	=>	'Remarks',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
												)

									);
		
		$this->data['col_center_maintenance'] = array(0);

		$this->data['table_components'] = array('table_id_components'	=> array(

													'Item Ref. ID' 			=>	array(
																					'title' 	=>	'Item Ref. ID',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Description' 			=>	array(
																					'title' 	=>	'Description',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'Qty' 					=>	array(
																					'title' 	=>	'Qty',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'Cost' 					=>	array(
																					'title' 	=>	'Cost',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle'
																				),
													'Total Cost' 			=>	array(
																					'title' 	=>	'Total Cost',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				)
												)

									);
		
		$this->data['col_center_components'] = array(0,1);
		


		$this->load_page('asset_management/asset_list/form');
	}

	public function getData($type, $docno = ''){
        $noseries_model = $this->load_model('administration/master_file/bom/noseries_model');
        $asset_list_model = $this->load_model('asset_management/asset_list/asset_list_model');

        if($type == 'add'){
            $doc_no  = $noseries_model->getNextAvailableNumber($this->data['number_series'], false, '');
            $output = array(
                'FA_ID'                         => $doc_no,
                'FA_RFID'          				=> '',
                'FA_ItemID'          			=> '',
                'FA_ItemDesc'          			=> '',
                'FA_ItemCode'     				=> '',
                'FA_Description'         		=> '',
                'FA_AssetClass'                 => '',
                'FA_Model'                 		=> '',
                'FA_ModelYear'                 	=> '',
                'FA_RefNo'                 		=> '',
                'FA_DateUsed'                   => '',
                'FA_SupplierID'                 => '',
                'FA_DepartmentID'               => '',
                'FA_Bay'               			=> '',
                'FA_Line'               		=> '',
                'FA_AffiliatesID'              	=> '',
                'FA_Status'                		=> '',
                'FA_AcquisitionDate'            => '',
                'FA_SerialNo'               	=> '',
                'FA_AcquisitionCost'            => 0,
                'FA_AcquisitionCostCY'          => '',
                'FA_AcquisitionNetBookCY'       => '',
                'FA_ExchangeRate'              	=> 1,
                'FA_WarrantyPeriodNo'           => 0,
                'FA_WarrantyPeriodUOM'          => '',
                'FA_UsefulLifeNo'        		=> 0,
                'FA_UsefulLifeNoUOM'   			=> '',
                'FA_DepreMonthly'     			=> '',
                'FA_AssetCode'   				=> '',
                'FA_AssetType'   				=> '',
                'FA_Family'						=> '',
                'FA_Process'					=> ''
            );

        }
        else{
            $where = array(
				'FA_ID' 	=> $docno
			);

			$join_table = array(
				array(
					'tbl_name'  => 'tblAttributeDetail as AD1',
					'tbl_on'  	=> 'AD1.AD_Id = FA_ItemID',
					'tbl_join'  => 'left'
				)
			);

			$header = $asset_list_model->getHeaderByDocNo($this->cols, $this->header_table, $where, $join_table);

            $array_header = array();
            foreach ($header as $key => $value) {
                $array_header[key($header)] = $value;
                next($header);
            }
            $output = $array_header;
        }
        return $output;
    }
	
	public function data(){

		$asset_list_model = $this->load_model('asset_management/asset_list/asset_list_model');		
		$table_data = $asset_list_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');

			// if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'asset_management/asset_list/view?id='.md5($value['FA_ID']).'"><i class="fa fa-eye"></i></a> ';
			// }

			if ($this->data['access_header']['RA_Edit'] && $value['FA_Status'] != 'Disposed') {
				$button .= '<a class="btn btn-primary btn-xs btn-outline update-header" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['FA_ID'].'" href="'.DOMAIN.'asset_management/asset_list/update?id='.md5($value['FA_ID']).'" role="button">
									<i class="fa fa-pencil"></i>
								</a> ';
			}

			if ($this->data['access_header']['RA_Print'] && $value['FA_Status'] != 'Disposed') {
				$button .= '<a class="btn btn-primary btn-xs btn-outline print-header" data-toggle="tooltip" data-placement="bottom" title="Print" data-doc-no="'.$value['FA_ID'].'" href="'.DOMAIN.'asset_management/asset_list/printout" role="button">
									<i class="fa fa-print"></i>
								</a> ';
			}

			$sub_array[] = $button;
			$sub_array[] = $value['FA_ID'];
			$sub_array[] = $value['FA_Description'];
			$sub_array[] = $value['FA_DepartmentDesc'];
			$sub_array[] = $value['FA_DateUsed'];
			$sub_array[] = $value['FA_AffiliatesID'];
			$sub_array[] = number_format($value['FA_AcquisitionCost'], 2);
			$sub_array[] = $value['FA_Status'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_movement(){

		$id = $this->input->get('id');

		$asset_list_detail_model = $this->load_model('asset_management/asset_list/asset_list_detail_model');		
		$table_data = $asset_list_detail_model->table_data_movement($id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');

			// if(in_array('View', $access_detail)){
			// }

			$sub_array[] = date_format(date_create($value['FAMH_Date']), "m/d/Y");
			$sub_array[] = $value['FAMH_RefNo'];
			$sub_array[] = $value['DepartmentDesc'];
			$sub_array[] = $value['AffiliatesDesc'];
			$sub_array[] = $value['FAMH_Remarks'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_maintenance(){

		$id = $this->input->get('id');
		$type = $this->input->get('type');
		$status = $this->getData($type, md5(trim($id)))['FA_Status'];

		$asset_list_detail_model = $this->load_model('asset_management/asset_list/asset_list_detail_model');		
		$table_data = $asset_list_detail_model->table_data_maintenance($id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
			$value['FAML_Date'] = date_format(date_create($value['FAML_Date']), "m/d/Y");
			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');

			

			if ($type == 'update' && $status != 'Disposed') {
				if($this->data['access_header_maintenance']['RA_Edit']){
					$button .= '<button class="btn btn-primary btn-xs btn-outline edit-maintenance" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['FAML_FA_AssetID'].'" data-line-no="'.$value['FAML_FA_LineNo'].'" data-record="'.$json_data.'">
										<i class="fa fa-pencil"></i>
									</button> ';
				}

				if($this->data['access_header_maintenance']['RA_Delete']){
					$button .= '<button class="btn btn-danger btn-xs btn-outline delete-maintenance" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['FAML_FA_AssetID'].'" data-line-no="'.$value['FAML_FA_LineNo'].'"><i class="fa fa-trash"></i></button> ';

				}

			}

			if ($type == 'update') {
				$sub_array[] = $button;
			}
			$sub_array[] = $value['FAML_Date'];
			$sub_array[] = $value['FAML_Particular'];
			$sub_array[] = number_format($value['FAML_Cost'], 2);
			$sub_array[] = $value['FAML_Remarks'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_components(){

		$id = $this->input->get('id');
		$type = $this->input->get('type');
		$status = $this->getData($type, md5(trim($id)))['FA_Status'];

		$asset_list_detail_model = $this->load_model('asset_management/asset_list/asset_list_detail_model');		
		$table_data = $asset_list_detail_model->table_data_components($id);
		$data = array();

		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');

			if ($type == 'update' && $status != 'Disposed') {
				if($this->data['access_header_component']['RA_Edit']){
					$button .= '<button class="btn btn-primary btn-xs btn-outline edit-components" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['FAC_FA_AssetID'].'" data-line-no="'.$value['FAC_FA_LineNo'].'" data-record="'.$json_data.'">
										<i class="fa fa-pencil"></i>
									</button> ';
				}

				if($this->data['access_header_component']['RA_Delete']){
					$button .= '<button class="btn btn-danger btn-xs btn-outline delete-components" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['FAC_FA_AssetID'].'" data-line-no="'.$value['FAC_FA_LineNo'].'"><i class="fa fa-trash"></i></button> ';

				}

			}
			
			if ($type == 'update') {
				$sub_array[] = $button;
			}
			$sub_array[] = $value['FAC_ItemCode'];
			$sub_array[] = $value['FAC_Description'];
			$sub_array[] = number_format($value['FAC_Qty'], 0);
			$sub_array[] = number_format($value['FAC_UnitCost'], 2);
			$sub_array[] = number_format($value['FAC_TotalCost'], 2);

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;

		if(!empty($_POST)):

			// GET ALL POST data
			$data = $this->input->post();

			// LOAD ALL NEEDED MODEL
			$asset_list_model = $this->load_model('asset_management/asset_list/asset_list_model');

			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];

			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){

					$output = $this->error_message(true, false, false, false, $data, $data['FA_ID'], $this->number_series, '', 'FA', 'tblFA', '_ID');

					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table
						);

						$this->db->trans_begin();

						$asset_list_model->on_save_module(true, $data, $table, 'FA', $this->number_series, '', '', '', '_ID');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}
					/* 
							END OF SAVING - ADD FUNCTION
					*/
			
				// UPDATE HEADER
				}
				else if($this->input->post('todo') == 'update'){
					
					$output = $this->error_message(true, false, false, false, $data, $data['FA_ID'], $this->number_series, '', 'FA', 'tblFA', '_ID');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,
						);

						$this->db->trans_begin();

						$asset_list_model->on_update_module($data, $table, 'FA', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['FA_ID']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
				}
				
				echo json_encode($output); //ENDING QUERY
			} 
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function save_maintenance_log(){

		$data = $this->input->post();

		$asset_list_detail_model = $this->load_model('asset_management/asset_list/asset_list_detail_model');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		try {

			$table = array(
				'header'  => 'tblFAMaintenanceLog'
			);

			if ($data['action'] == 'add') {

				$data['FAML_FA_LineNo'] = $asset_list_detail_model->getLastLineNo($data['FAML_FA_AssetID'], 'FAML_FA_AssetID', 'FAML_FA_LineNo', 'tblFAMaintenanceLog');
				$asset_list_detail_model->on_save_module(false, $data, $table, 'FAML', 300003, '');
				
			}
			else{

				$asset_list_detail_model->on_update_module($data, $table, 'FAML', 300003, '');
				
			}

			echo json_encode($output);
			
		} catch (Exception $e) {
			$output = array(
				'success'  => false,
				'message'  => $e->getMessage()
			);

			echo json_encode($output);
		}

	}

	public function save_dispose(){

		$data = $this->input->post();

		$asset_list_detail_model = $this->load_model('asset_management/asset_list/asset_list_detail_model');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		try {

			$this->db->trans_begin();

			$data['FAMH_RefNo'] = "Disposed";
			$data['FAMH_LocationID'] = '0';
			$data['FAMH_AffiliatesID'] = '0';
			$data['CreatedBy'] = getCurrentUser()['login-user'];
			$data['FAMH_FA_LineNo'] = $asset_list_detail_model->getLastLineNo($data['FAMH_FA_AssetID'], 'FAMh_FA_AssetID', 'FAMh_FA_LineNo', 'tblFAMovementHistory');
			
			$this->db->set('DateCreated', 'GETDATE()', false);
			$this->db->insert('tblFAMovementHistory', $data);

			$this->db->where('FA_ID', $data['FAMH_FA_AssetID']);
			$this->db->set('FA_Status', 'Disposed');
			$this->db->set('ModifiedBy', getCurrentUser()['login-user']);
			$this->db->set('DateModified', 'GETDATE()', false);
			$this->db->update('tblFA');
			

			if($this->db->trans_status() !== False){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}	

			echo json_encode($output);
			
		} catch (Exception $e) {
			$output = array(
				'success'  => false,
				'message'  => $e->getMessage()
			);

			echo json_encode($output);
		}

	}

	public function save_change_location(){

		$data = $this->input->post();

		$asset_list_detail_model = $this->load_model('asset_management/asset_list/asset_list_detail_model');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		try {

			$this->db->trans_begin();

			$data['CreatedBy'] = getCurrentUser()['login-user'];
			$data['FAMH_FA_LineNo'] = $asset_list_detail_model->getLastLineNo($data['FAMH_FA_AssetID'], 'FAMh_FA_AssetID', 'FAMh_FA_LineNo', 'tblFAMovementHistory');
			
			$this->db->insert('tblFAMovementHistory', $data);

			$this->db->where('FA_ID', $data['FAMH_FA_AssetID']);
			$this->db->set('FA_DepartmentID', $data['FAMH_LocationID']);
			$this->db->set('FA_AffiliatesID', $data['FAMH_AffiliatesID']);
			$this->db->set('ModifiedBy', getCurrentUser()['login-user']);
			$this->db->set('DateModified', 'GETDATE()', false);
			$this->db->update('tblFA');
			

			if($this->db->trans_status() !== False){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}	

			echo json_encode($output);
			
		} catch (Exception $e) {
			$output = array(
				'success'  => false,
				'message'  => $e->getMessage()
			);

			echo json_encode($output);
		}

	}

	public function save_components(){

		$data = $this->input->post();

		$asset_list_detail_model = $this->load_model('asset_management/asset_list/asset_list_detail_model');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		try {

			$this->db->trans_begin();
			
			$table = array(
				'header'  => 'tblFAComponent'
			);

			if ($data['action'] == 'add') {

				$data['FAC_FA_LineNo'] = $asset_list_detail_model->getLastLineNo($data['FAC_FA_AssetID'], 'FAC_FA_AssetID', 'FAC_FA_LineNo', 'tblFAComponent');
				$asset_list_detail_model->on_save_module(false, $data, $table, 'FAC', 300004, '');
				
			}
			else{

				$asset_list_detail_model->on_update_module($data, $table, 'FAC', 300004, '');
				
			}


			if($this->db->trans_status() !== False){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}	

			echo json_encode($output);
			
		} catch (Exception $e) {
			$output = array(
				'success'  => false,
				'message'  => $e->getMessage()
			);

			echo json_encode($output);
		}

	}

	public function delete_components(){
		$doc_no = $this->input->get('id');
		$line_no = $this->input->get('line_no');

		$check_exist_docno = $this->checkIfDetailExist($doc_no, 'FAC_FA_AssetID', $line_no, 'FAC_FA_LineNo', 'tblFAComponent');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This Component has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where('FAC_FA_AssetID', $doc_no)
					 ->where('FAC_FA_LineNo', $line_no)
					 ->delete('tblFAComponent');

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function getItem(){

		$filter = $this->input->get('filter-input');
		$asset_list_model = $this->load_model('asset_management/asset_list/asset_list_model');

		$output = $asset_list_model->searchItem($filter);

		echo json_encode($output);

	}

	public function assetCode(){

		$filter = $this->input->get('filter-input');
		$asset_list_model = $this->load_model('asset_management/asset_list/asset_list_model');

		$output = $asset_list_model->getAssetCode($filter);

		echo json_encode($output);

	}

	public function assetType(){

		$filter = $this->input->get('filter-input');
		$asset_list_model = $this->load_model('asset_management/asset_list/asset_list_model');

		$output = $asset_list_model->getAssetType($filter);

		echo json_encode($output);

	}

	// public function test(){

	// 	$filter = $this->input->get('filter-input');
	// 	$asset_list_model = $this->load_model('asset_management/asset_list/asset_list_model');

	// 	$output = $asset_list_model->getTest($filter);

	// 	echo json_encode($output);
	// }

	public function getGeneralInfoFilteredData(){
		$data = $this->input->post();

		$asset_list_model = $this->load_model('asset_management/asset_list/asset_list_model');

		$result = '';

		if (isset($data['divisionType'])) {
			$result = $asset_list_model->getBayNo($data['divisionType']);
		}

		if(isset($data['bayNo'])){
			$result = $asset_list_model->getLineNo($data['bayNo']);
		}

		echo json_encode($result);
	}

	public function getAssetClassDetail(){
		$asset_class = $this->input->post('FA_AssetClass');

		$sql = 'SELECT 	* 
				FROM 	tblFAClassSetup
				WHERE 	FACS_ID = \''.$asset_class.'\'';

		echo json_encode($this->db->query($sql)->row_array());
	}

	public function getLastMovementHistory(){

		$asset_id = $this->input->post('FA_ID');

		$sql = 'SELECT 	TOP (1) FAMH_Date 
				FROM 	tblFAMovementHistory
				WHERE 	FAMH_FA_AssetID = \''.$asset_id.'\'
				ORDER BY FAMH_FA_LineNo DESC';

		$result = $this->db->query($sql)->row_array()['FAMH_Date'];

		if ($result != '') {
			$result = date_format(date_create($result), 'm/d/Y');
		}

		echo json_encode($result);

	}

	public function getLastMaintenanceLog(){

		$asset_id = $this->input->post('FA_ID');

		$sql = 'SELECT 	TOP (1) FAML_Date 
				FROM 	tblFAMaintenanceLog
				WHERE 	FAML_FA_AssetID = \''.$asset_id.'\'
				ORDER BY FAML_FA_LineNo DESC';

		$result = $this->db->query($sql)->row_array()['FAML_Date'];

		if ($result != '') {
			$result = date_format(date_create($result), 'm/d/Y');
		}

		echo json_encode($result);

	}

	public function printout(){

		$asset_list_model = $this->load_model('asset_management/asset_list/asset_list_model');

		$this->data['doc_no'] = $this->input->post('doc_no');

		$data = array();
		$data['header'] = $this->db->where('FA_ID',$this->data['doc_no'])->get('tblFA')->row_array();

		$qr_data = array(
							'size' => 172,
							'text' => $data['header']['FA_RFID'] == '' ? '' :  $data['header']['FA_RFID']
		);

		$data['details'] = $asset_list_model->table_data($this->data['doc_no']);
		$data['qr_code'] = $this->createQR($qr_data);
		$html = $this->load->view('asset_management/asset_list/printout', $data, true);
		$this->generate(
    			array(	'format'=>'Letter',
    					'orientation'=>'P',
    					'title' => 'Print Asset',
    					'html'=>$html,
    					'auto_top_margin' => 'pad',
  		));
	}


	// Revised/Added by JDC 071720241
	public function getBayDetail(){
		$asset_bay = $this->input->post('FA_Bay');

		$sql = 'select	*
				from	[dbo].[tblSubLocation]
				WHERE 	[SL_Id] = \''.$asset_bay.'\'';

		echo json_encode($this->db->query($sql)->row_array());
	}

	// Revised/Added by JDC 071720241
	public function getLineDetail(){
		$asset_line = $this->input->post('FA_Line');

		$sql = 'select	*
				from	[dbo].[tblSubLocation]
				WHERE 	[SL_Id] = \''.$asset_line.'\'';

		echo json_encode($this->db->query($sql)->row_array());
	}

	

}


