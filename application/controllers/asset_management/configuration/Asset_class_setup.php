<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Asset_class_setup extends MAIN_Controller {

	protected $number_series		= 	400006;
	protected $header_table			=	'tblFAClassSetup';
	protected $header_doc_no_field	=	'FACS_ID';
	protected $header_status_field	=	'';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;

		$this->data['header_table']			=	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		= 	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->data['module'] 				= 	getDisplayName($this->uri->segment(3));
		$this->data['module_icon'] 			= 	getModuleIcon($this->uri->segment(3));
		$this->cols 						= 	array('AD_ID', 'AD_Code', 'AD_Desc');
	}

	public function index(){
		//$this->user_model->deleteUserRecordLock(getCurrentUser()['login-user']); // Delete User Record Lock

		$this->data['table_hdr'] = array('asset_class_setup' =>	array(
												'buttons' 			=>	array(
																				'title' 	=>	'<button class="btn btn-success btn-xs 
																									btn-outline add-header" 
																									data-toggle="tooltip" data-placement="top" 
																									title="Add" type="button" ><i
																									class="fa
																									fa-plus"></i></button>',
																				'class' 	=>	'text-center',
																				'style' 	=>	'width: 15px; vertical-align:
																								middle' 
																			),
												'asset_class' 		=>	array(
																				'title' 	=>	'Asset Class',
																				'class' 	=>	'text-left',
																				'style' 	=>	'width: 25%; 
																					vertical-align: middle' 
																			),
												'description' 		=>	array(
																				'title' 	=>	'Description',
																				'class' 	=>	'text-left',
																				'style' 	=>	'width: 40%; 
																					vertical-align: middle' 
																			),
												'useful_life' 		=>	array(
																				'title' 	=>	'Useful Life',
																				'class' 	=>	'text-left',
																				'style' 	=>	'width: 20%; 
																					vertical-align: middle' 
																			)
										)
							);

		$this->data['filter'] = array(

			array(
				"name"  => "Asset Class",
				"value"  => 1
			),
			array(
				"name"  => "Description",
				"value"  => 2
			),
			array(
				"name"  => "Useful Life",
				"value"  => 3
			)

		);

		$this->data['hdr_center']	= array(0,1);

		$this->load_page('asset_management/configuration/asset_class_setup/index');
	}

	public function data(){

		$acs_model = $this->load_model('asset_management/configuration/asset_class_setup/asset_class_setup_model');		
		$table_data = $acs_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');

			// if(in_array('View', $access_detail)){
			// }

			// if(in_array('Edit', $access_detail)){
				$button .= '<button class="btn btn-primary btn-xs btn-outline update-header" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['FACS_ID'].'" data-record="'.$json_data.'">
									<i class="fa fa-pencil"></i>
								</button> ';
			// }

			// if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['FACS_ID'].'"><i class="fa fa-trash"></i></button> ';
			// }

			$sub_array[] = $button;
			$sub_array[] = $value['FACS_ID'];
			$sub_array[] = $value['FACS_Description'];
			$sub_array[] = $value['FACS_UsefulLife'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save_asset_class(){

		$data = $this->input->post();

		$acs_model = $this->load_model('asset_management/configuration/asset_class_setup/asset_class_setup_model');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		try {

			$this->db->trans_begin();
			
			$table = array(
				'header'  => $this->header_table
			);

			if ($data['action'] == 'add') {

				$checkExistingDesc = $acs_model->checkExistingIDandDesc($data['FACS_ID'], $data['FACS_Description'], $data['action']);


				if ($checkExistingDesc['success']) {

					$acs_model->on_save_module(false, $data, $table, 'FACS', $this->number_series, '');
				}
				else{
					throw new Exception($checkExistingDesc['message']);
				}
			}
			else{
				$checkExistingDesc = $acs_model->checkExistingIDandDesc($data['FACS_ID'], $data['FACS_Description'], $data['action']);


				if ($checkExistingDesc['success']) {
					$acs_model->on_update_module($data, $table, 'FACS', $this->number_series, '');
				}
				else{
					throw new Exception($checkExistingDesc['message']);
				}
			}


			if($this->db->trans_status() !== False){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}	

			echo json_encode($output);
			
		} catch (Exception $e) {
			$output = array(
				'success'  => false,
				'message'  => $e->getMessage()
			);

			echo json_encode($output);
		}

	}

	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}


}