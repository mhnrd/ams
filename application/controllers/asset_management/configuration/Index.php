<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Index extends MAIN_Controller {

	protected $number_series		=	400000;
	protected $header_table			=	'tblFA';
	protected $header_doc_no_field	=	'FA_AssetID';
	protected $header_status_field	=	'FA_Status';
	protected $module_name			=	'Scan RFID';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						= 	array('R_ID', 'R_Name', 'R_Description');
	}

	public function index(){

		$this->load_page('asset_management/configuration/index');
	}


}