<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class Inventory_report extends MAIN_Controller {
	protected $number_series		= 500003;
	protected $header_table 		= '';
	protected $header_doc_no_field	= '';
	protected $header_status_field	= '';

	public function __construct(){
		parent::__construct();		
		$this->is_secure = true;
		$this->data['header_table'] 		= $this->header_table;
		$this->data['header_doc_no_field'] 	= $this->header_doc_no_field;
		$this->data['header_status_field'] 	= $this->header_status_field;
		$this->data['number_series'] 		= $this->number_series;
		$this->data['access_header']		= $this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		= $this->user_model->getDetailAccess($this->number_series);
		$this->cols 						= array('FA_DepartmentID','FA_AffiliatesID','FA_ModelYear','FA_Line');
	}

	public function index(){

		// $this->data['Company'] = getCompanyDetails('tblCompany', array('COM_Id', 'COM_Name'));
		$this->data['department'] = getAttributeList(400003);
		$this->data['affiliates'] = getAttributeList(400004);
		$this->data['FA_Line'] = getLineNo('FA_Line');
		// $this->data['FA_Model'] = getLineNo('FA_Model');
		// $this->data['FA_Family'] = getLineNo('FA_Family');
		$this->data['header'] = $this->getData($this->data['department'], $this->data['affiliates'], $this->data['FA_Line']);
		$this->load_page('asset_management/reports/inventory_report');
	}

	public function getData($filter){
        $noseries_model = $this->load_model('administration/master_file/bom/noseries_model');
        $asset_list_model = $this->load_model('asset_management/asset_list/asset_list_model');

            $output = array(
                'FA_DepartmentID'	=> '',
                'FA_AffiliatesID'	=> '',
                'FA_ModelYear'		=> '',
                'FA_Line'			=> ''
            );

			return $output;

    }

	public function print_document(){
		$inventory_report_model = $this->load_model('asset_management/inventory_report/Inventory_report_model');
		$filter = $this->input->post();
		$data = array();
		$data['detail'] = $inventory_report_model->getInventoryReportDetail($filter);

		$xlsObj = array();

		//$xlsObj = $this->cellDetails($xlsObj, $data['detail'], $filter['FA_DepartmentID'], $filter['FA_Line'], $filter['FA_Model'], $filter['FA_ModelYear'], $filter['FA_Family']);
		$xlsObj = $this->cellDetails($xlsObj, $data['detail'], $filter['Month'], $filter['Year'], $filter['FA_DepartmentID'], $filter['FA_Line'], $filter['FA_ModelYear']);

		$spreadsheet = $this->spreadsheetSetup($xlsObj);

		echo json_encode(array("success" => true, "file_name"=>$this->generateExcelPrintout($spreadsheet, 'Inventory Report')));

	}

	// public function cellDetails($xlsObj, $data, $department, $FA_Line, $FA_Model, $FA_ModelYear, $FA_Family){
	public function cellDetails($xlsObj, $data, $month, $year, $department, $FA_Line, $FA_ModelYear){

		//Hardcoded Header
		array_push($xlsObj,
			array(
				"cell"  	=> 'A1',
				"value"  	=> 'INVENTORY REPORT for the MONTH YEAR: '.$month.' '.$year.''
			),
			array(
				"cell"  	=> 'A3',
				"value"  	=> 'Division: '.$department
			),
			array(
				"cell"  	=> 'A4',
				"value"  	=> 'Line No. : '.$FA_Line
			),
			array(
				"cell"		=> 'A5',
				"value"		=> 'Model: '
			),
			array(
				"cell"		=> 'A6',
				"value"		=> 'Model Year: '.$FA_ModelYear
			),
			array(
				"cell"		=> 'A7',
				"value"		=> 'Family: '
			)
		);

		array_push($xlsObj,
			// Header
			array(
				'cell' 		=> 'A9',
				'value'  	=> 'Asset Code'
			),
			array(
				'cell' 		=> 'B9',
				'value'  	=> 'Asset Description'
			),
			array(
				'cell' 		=> 'C9',
				'value'  	=> 'Month 1 Count'
			),
			array(
				'cell' 		=> 'D9',
				'value'  	=> 'Month 2 Count'
			),
			array(
				'cell' 		=> 'E9',
				'value'  	=> 'Changes'
			),
			array(
				'cell' 		=> 'F9',
				'value'  	=> 'Receive'
			),
			array(
				'cell' 		=> 'G9',
				'value'  	=> 'Transfer'
			),
			array(
				'cell' 		=> 'H9',
				'value'  	=> 'Retired'
			)
		);

		foreach ($data as $key => $value) {
		 	$nextDCol = 10;

			array_push($xlsObj,
				// Details
				array(
					'cell' 		=> 'A'.($key+$nextDCol),
					'value'  	=> strtoupper($value['Asset Code'])
				),
				array(
					'cell' 		=> 'B'.($key+$nextDCol),
					'value'  	=> strtoupper($value['Asset Description'])
				),
				array(
					'cell' 		=> 'C'.($key+$nextDCol),
					'value'  	=> $value['Month 1 Count']
				),
				array(
					'cell' 		=> 'D'.($key+$nextDCol),
					'value'  	=> $value['Month 2 Count']
				),
				array(
					'cell' 		=> 'E'.($key+$nextDCol),
					'value'  	=> $value['Changes']
				),
				array(
					'cell' 		=> 'F'.($key+$nextDCol),
					'value'  	=> $value['Receive']
				),
				array(
					'cell' 		=> 'G'.($key+$nextDCol),
					'value'  	=> $value['Transfer']
				),
				array(
					'cell' 		=> 'H'.($key+$nextDCol),
					'value'  	=> $value['Retired']
				)
			);
			
		}

	 	return $xlsObj;
	}

	public function spreadsheetSetup($xlsObj){
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet()->setTitle("Inventory Report");

			$styleArray = array(
	 		'borders' => array(
	 		'allBorders' => array(
  			'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
  			'color' => array('argb' => '000000'),
			),
			),
			);

  			$sheet ->getStyle('A9:H9')->applyFromArray($styleArray);

		// Color Fill
		foreach (range('A', 'H') as $key => $value) {
			$spreadsheet->getActiveSheet()->getStyle($value.'1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_NONE)->getStartColor()->setARGB('FFFFFF');
			$spreadsheet->getActiveSheet()->getStyle($value.'9')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('C0C0C0');

			$spreadsheet->getActiveSheet()->getColumnDimension($value)->setAutoSize(true);
			$sheet->getStyle($value.'9')->getAlignment()->setHorizontal('center');
			$spreadsheet->getActiveSheet()->getStyle($value)->getAlignment()->setHorizontal('center');
			$spreadsheet->getActiveSheet()->getStyle('A1:A7')->getAlignment()->setHorizontal('left');

		}

		foreach ($xlsObj as $key => $value) {
			$sheet->setCellValue($value['cell'], $value['value']);
		}

		// Number Format
		// $spreadsheet->getActiveSheet()->getStyle('B6:B'.(count($xlsObj) + 5))->getNumberFormat()->setFormatCode('#,##0.00');

		return $spreadsheet;
	}
}