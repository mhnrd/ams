<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Scan extends MAIN_Controller {

	protected $number_series		=	200000;
	protected $header_table			=	'tblFA';
	protected $header_doc_no_field	=	'FA_ID';
	protected $header_status_field	=	'';
	protected $module_name			=	'Scan';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						= 	array('R_ID', 'R_Name', 'R_Description');
	}

	public function index(){

		$this->data['table_hdr'] = array('scan_rfid'	=> array(
													'buttons' 				=> 	array(
																					'title' 	=>	'',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5px; vertical-align: middle' 
																				),
													'asset_id' 				=>	array(
																					'title' 	=>	'Asset ID',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'description' 			=>	array(
																					'title' 	=>	'Description',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
													'location' 				=>	array(
																					'title' 	=>	'Location',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				)
												)

									);

		$this->data['filter'] = array(

			array(
				"name"  => "Asset ID",
				"value"  => 1
			),
			array(
				"name"  => "Description",
				"value"  => 2
			),
			array(
				"name"  => "Location",
				"value"  => 3
			)

		);
		
		$this->data['dtl_center'] = array(0);

		$this->load_page('asset_management/scan/index');
	}

	

		public function data(){

		$asset_list_model = $this->load_model('asset_management/scan/scan_model');		
		$table_data = $asset_list_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');

			// if(in_array('View', $access_detail)){
			// }

			// if(in_array('Edit', $access_detail)){
				// $button .= '<button class="btn btn-primary btn-xs btn-outline update-header" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['FA_ID'].'" data-record="'.$json_data.'">
				// 					<i class="fa fa-pencil"></i>
				// 				</button> ';
			// }

			// if(in_array('Delete', $access_detail)){
				// $button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['FA_ID'].'"><i class="fa fa-trash"></i></button> ';
			// }

			$sub_array[] = $button;
			$sub_array[] = $value['FA_ID'];
			$sub_array[] = $value['FA_Description'];
			$sub_array[] = $value['FA_DepartmentDesc'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function getItem(){

		$filter = $this->input->get('filter-input');
		$scan_model = $this->load_model('asset_management/scan/scan_model');

		$output = $scan_model->searchItem($filter);

		echo json_encode($output);

	}
}