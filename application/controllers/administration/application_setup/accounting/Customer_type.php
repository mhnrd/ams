<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Customer_type extends MAIN_Controller{

	protected $number_series 		= 102204;
	protected $header_table 		= 'tblCustomerType';
	protected $header_doc_no_field 	= 'CT_Id';
	protected $header_status_field 	= 'CT_Active';
	protected $module_name 			= 'Customer Type';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> 'administration');
		$this->breadcrumb[] = array( 	'html'	=> 'Application Setup',
									 	'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=>	'Accounting',
										'url'	=>	'');

		$this->data['header_table']			= $this->header_table;
		$this->data['header_doc_no_field']	= $this->header_doc_no_field;
		$this->data['header_status_field']	= $this->header_status_field;
		$this->data['number_series']		= $this->number_series;
		$this->data['access_header']		= $this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		= $this->user_model->getDetailAccess($this->number_series);

		$this->cols = array('CT_Id','CT_Description','CT_Active');

	}

	// Table Header
	public function index(){
		$this->breadcrumb[]	= array( 'html'	=> 'Principal Type',
									 'url'	=> 'administration/application_setup/accounting'); 

		//Header Table
		$this->data['table_hdr'] = array('customer-type-details' => array(
											'button'		 	=> array(
																			'title' => '<a class="btn btn-success btn-xs btn-outline add-large" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="'.DOMAIN.'administration/application_setup/accounting/customer_type/add" role="button"><i class="fa fa-plus"></i></a>',
																			'class' => 'text-center',
																			'style' => 'width: 100px; vertical-align:middle'
																		),
											'checkbox'  		=> array(
																			'title' => '<input type="checkbox" 
																						id="chkSelectAll" 
																						class="icheckbox_square-green"',
																			'class' => 'text-center',
																			'style' => 'width: 10px; vertical-align:middle'
																		),
											'customer-type-id' 	=> array(
																			'title' => 'Principal Type ID',
																			'class' => 'text-left',
																			'style' => 'width: 40%; vertical-align:middle'
																		),
											'customer-type' 	=> array(
																			'title' => 'Principal Type',
																			'class' => 'text-left',
																			'style' => 'width: 40%; vertical-align:middle'
																		),
											'active' 			=> array(
																			'title' => 'Status',
																			'class' => 'text-left',
																			'style' => 'width: 10%; vertical-align:middle'
																		), 
			)
		);
		$this->data['hdr_center'] 	= array(0,1);
		$this->load_page('administration/application_setup/accounting/customer_type/index');
	}


	// Adding of User
	public function add(){
		$this->breadcrumb[] 			= array (	'html'	=>	'Principal Type',
													'url'	=>	'administration/application_setup/accounting/customer_type');
		$this->breadcrumb[]				= array (	'html'	=>	'Add',
													'url'	=>	'');
		
		$this->data['type']		 		= 'add';											
		$this->data['header']	 		= $this->getData($this->data['type'], '');
		$this->load_page('administration/application_setup/accounting/customer_type/form');

	}
	public function update(){
		$this->breadcrumb[]	= array(	'html'	=>	'Principal Type',
										'url'	=>	'administration/application_setup/accounting/customer_type');
		$this->breadcrumb[] = array(	'html'	=>	'Update',
										'url'	=>	'');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type']	= 'update';
		$this->data['id'] = $_GET['id'];
		
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);

		// LOAD NEEDED DATA
		$this->load_page('administration/application_setup/accounting/customer_type/form');

		
	}

	public function view(){
		$this->breadcrumb[] = array(	'html'	=> 'Principal Type',
										'url'	=> 'administration/application_setup/accounting/customer_type');
		$this->breadcrumb[] = array(	'html'	=> 'View',
										'url'	=> '');
		// STRUCTURE FOR VIEW PAGE
		$this->data['type'] = 'view';
		$this->data['id']	= $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);

		//LOAD NEEDED DATA
		$this->load_page('administration/application_setup/accounting/customer_type/form');
	}



	// Getting of data from the Form
	public function getData($type, $docno = ''){

		$customer_type_model = $this->load_model('administration/application_setup/accounting/customer_type/customer_type_model');

		if ($type == 'add') {
			$output = array(
				'CT_Id' 				=> '',
				'CT_Description' 		=> '',
				'CT_Active'  			=> '1',		
			);
		}
		else{
			$where = array(
				'CT_Id' => $docno
			);

			$header = $customer_type_model->getHeaderByDocNo($this->cols, $this->header_table, $where);

			$output = array(
				'CT_Id' 			=> $header['CT_Id'],
				'CT_Description' 	=> $header['CT_Description'],
				'CT_Active' 		=> $header['CT_Active'], 
				'functions'			=> $this->getApprovalButtons($header['CT_Id']
																,$header['CT_Active']
																,getCurrentUser()['login-user']
																,''
																,$this->header_table
																,$this->header_status_field
																,$this->header_doc_no_field
															), 

			);
			if($output['CT_Active'] == 'Approved'){
				unset($output['functions'][1]);
			}
		}
		return $output;
	}


	// Data to be Display in the Table
	public function data(){
		$customer_type_model = $this->load_model('administration/application_setup/accounting/customer_type/customer_type_model');
		$table_data 		 = $customer_type_model->table_data();
		$access_detail 		 = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button	   = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/application_setup/accounting/customer_type/view?id='.md5($value['CT_Id']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/application_setup/accounting/customer_type/update?id='.md5($value['CT_Id']).'">
					<i class="fa fa-pencil"></i>
					</a> ';
			//}
			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['CT_Id'].'" "><i class="fa fa-trash"></i></a> ';
			//}  	
				$sub_array[] = $button;
				$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['CT_Id'].'">';
				$sub_array[] = $value['CT_Id'];
				$sub_array[] = $value['CT_Description'];
				$sub_array[] = $value['CT_Active'];
				
				$data[] = $sub_array;
			}

			$output = array(  
					"draw"            =>     intval($_POST["draw"]),  
					"recordsTotal"    =>     $table_data['count_all_results'],  
					"recordsFiltered" =>     $table_data['num_rows'],  
					"data"            =>     $data  
	           );  

			echo json_encode($output);
		}

		public function save(){
			if(!$this->input->is_ajax_request()) return;

			if(!empty($_POST)):

				// Get All POST Data
				$data = $this->input->post();

				// Load All needed model
				$customer_type_model = $this->load_model('administration/application_setup/accounting/customer_type/customer_type_model');
				// GET CURRENT USER
				$current_user 		 = getCurrentUser()['login-user'];

				$output = array(
								'success'	=> true
							,	'message'	=> '');
				try{

					// Start Of Saving - Add Function
					if($this->input->post('todo') == 'add'){
						// GET THE LATEST NO SERIES
						
						$output = $this->error_message(false, true, false, false, $data, $data['CT_Id'], $this->number_series,'','CT','tblCustomerType');
						
					
					if($output['success']){
				
							$table = array(
								'header'  	=> $this->header_table,
							);

							$this->db->trans_begin();

							$customer_type_model->on_save_module(false, $data, $table, 'CT', $this->number_series,'');

							if($this->db->trans_status() !== False){
								$this->db->trans_commit();
							}
							else{
								$this->db->trans_rollback();
							}
						}
						else{
							throw new Exception($output['message']);
						}

						/* 
								END OF SAVING - ADD FUNCTION
						*/

					// UPDATE HEADER
					}
					else if($this->input->post('todo') == 'update'){

						// $this->print_r($data);

					$output = $this->error_message(false, true, false, false, $data, $data['CT_Id'], $this->number_series,'','CT','tblCustomerType');

						if($output['success']):

							$table = array(
								'header'  	=> $this->header_table,
								
							);

							$this->db->trans_begin();

							$customer_type_model->on_update_module($data, $table, 'CT', $this->number_series,'');

							if($this->db->trans_status() !== False){
								$this->db->trans_commit();
								$this->deleteUserRecordLock_php($data['CT_Id']);
							}
							else{
								$this->db->trans_rollback();
							}
							
						else:
							throw new Exception($output['message']);
						endif;

							/* 
								END OF SAVING - UPDATE FUNCTION
							*/
						
						}
					
					echo json_encode($output); //ENDING QUERY
				}

				catch(Exception $e){

					$str = $e->getMessage();
					$array_msg = explode(" , ", $str);

					$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
					);

					echo json_encode($output);
				}
			endif;

		}



		public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}
}
?>