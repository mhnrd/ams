<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payment_terms extends MAIN_Controller{

	protected $number_series 		= 102206;
	protected $header_table 		= 'tblPaymentTerms';
	protected $header_doc_no_field 	= 'PT_ID';
	protected $header_status_field 	= 'PT_Active';
	protected $module_name 			= 'Payment Terms';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> '');
		$this->breadcrumb[] = array( 	'html'	=> 'Application Setup',
									 	'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=>	'Accounting',
										'url'	=>	'');

		$this->data['header_table']			= $this->header_table;
		$this->data['header_doc_no_field']	= $this->header_doc_no_field;
		$this->data['header_status_field']	= $this->header_status_field;
		$this->data['number_series']		= $this->number_series;
		$this->data['access_header']		= $this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		= $this->user_model->getDetailAccess($this->number_series);

		$this->cols = array('PT_ID','PT_Desc','PT_Days');

	}

	// Table Header
	public function index(){
		$this->breadcrumb[]	= array( 'html'	=> 'Payment Terms',
									 'url'	=> 'administration/application_setup/accounting'); 

		//Header Table
		$this->data['table_hdr'] = array('payment-terms' => array(
											'button'		 	=> array(
																			'title' => '<a class="btn btn-success btn-xs btn-outline add-large" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="'.DOMAIN.'administration/application_setup/accounting/payment_terms/add" role="button"><i class="fa fa-plus"></i></a>',
																			'class' => 'text-center',
																			'style' => 'width: 100px; vertical-align:middle'
																		),
											'checkbox'  		=> array(
																			'title' => '<input type="checkbox" 
																						id="chkSelectAll" 
																						class="icheckbox_square-green"',
																			'class' => 'text-center',
																			'style' => 'width: 10px; vertical-align:middle'
																		),
											'pt_id' 			=> array(
																			'title' => 'Payment Terms ID',
																			'class' => 'text-left',
																			'style' => 'width: 25%; vertical-align:middle'
																		),
											'pt_description' 	=> array(
																			'title' => 'Payment Description',
																			'class' => 'text-left',
																			'style' => 'width: 45%; vertical-align:middle'
																		),
											'pt_no_of_days' 	=> array(
																			'title' => 'No. of Days',
																			'class' => 'text-right',
																			'style' => 'width: 10%; vertical-align:middle'
																		),
											'active' 			=> array(
																			'title' => 'Active',
																			'class' => 'text-left',
																			'style' => 'width: 10%; vertical-align:middle'
																		), 
			)
		);
		$this->data['hdr_center'] 	= array(0,1);
		$this->load_page('administration/application_setup/accounting/payment_terms/index');
	}


	// Adding of User
	public function add(){
		$this->breadcrumb[] 			= array (	'html'	=>	'Payment Terms',
													'url'	=>	'administration/application_setup/accounting/payment_terms');
		$this->breadcrumb[]				= array (	'html'	=>	'Add',
													'url'	=>	'');
		
		$this->data['type']		 		= 'add';
		$this->data['id'] 	 			= '';											
		$this->data['header']	 		= $this->getData($this->data['type']);
		$this->load_page('administration/application_setup/accounting/payment_terms/form');

	}
	public function update(){
		$this->breadcrumb[]	= array(	'html'	=>	'Payment Terms',
										'url'	=>	'administration/application_setup/accounting/payment_terms');
		$this->breadcrumb[] = array(	'html'	=>	'Update',
										'url'	=>	'');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type']	= 'update';
		$this->data['id'] = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);

		// LOAD NEEDED DATA
		$this->load_page('administration/application_setup/accounting/payment_terms/form');

		
	}

	public function view(){
		$this->breadcrumb[] = array(	'html'	=> 'Payment Terms',
										'url'	=> 'administration/application_setup/accounting/payment_terms');
		$this->breadcrumb[] = array(	'html'	=> 'View',
										'url'	=> '');
		// STRUCTURE FOR VIEW PAGE
		$this->data['type'] = 'view';
		$this->data['id']	= $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);

		//LOAD NEEDED DATA
		$this->load_page('administration/application_setup/accounting/payment_terms/form');
	}



	// Getting of data from the Form
	public function getData($type, $docno = ''){

		$payment_terms_model = $this->load_model('administration/application_setup/accounting/payment_terms/payment_terms_model');

		if ($type == 'add') {
			$output = array(
				'PT_ID' 				=> '',
				'PT_Desc' 				=> '',
				'PT_Days'  				=> 0,		
			);
		}
		else{
			$where = array(
				'PT_ID' => $docno
			);

			$header = $payment_terms_model->getHeaderByDocNo($this->cols, $this->header_table, $where);

			$output = array(
				'PT_ID' 				=> $header['PT_ID'],
				'PT_Desc' 				=> $header['PT_Desc'],
				'PT_Days'  				=> number_format($header['PT_Days'], 0, ".", ""),		
			);
		}
		return $output;
	}


	// Data to be Display in the Table
	public function data(){
		$payment_terms_model = $this->load_model('administration/application_setup/accounting/payment_terms/payment_terms_model');
		$table_data 		 = $payment_terms_model->table_data();
		// $access_detail 		 = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button	   = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/application_setup/accounting/payment_terms/view?id='.md5($value['PT_ID']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/application_setup/accounting/payment_terms/update?id='.md5($value['PT_ID']).'">
					<i class="fa fa-pencil"></i>
					</a> ';
			//}
			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['PT_ID'].'" "><i class="fa fa-trash"></i></a> ';
			//}  	
				$sub_array[] = $button;
				$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['PT_ID'].'">';
				$sub_array[] = $value['PT_ID'];
				$sub_array[] = $value['PT_Desc'];
				$sub_array[] = number_format($value['PT_Days'], 0);
				$sub_array[] = $value['PT_Active'];
				
				$data[] = $sub_array;
			}

			$output = array(  
					"draw"            =>     intval($_POST["draw"]),  
					"recordsTotal"    =>     $table_data['count_all_results'],  
					"recordsFiltered" =>     $table_data['num_rows'],  
					"data"            =>     $data  
	           );  

			echo json_encode($output);
		}

		public function save(){
			if(!$this->input->is_ajax_request()) return;

			if(!empty($_POST)):

				// Get All POST Data
				$data = $this->input->post();

				// Load All needed model
				$payment_terms_model = $this->load_model('administration/application_setup/accounting/payment_terms/payment_terms_model');
				// GET CURRENT USER
				$current_user 		 = getCurrentUser()['login-user'];

				$output = array(
								'success'	=> true
							,	'message'	=> '');
				try{

					// Start Of Saving - Add Function
					if($this->input->post('todo') == 'add'){
						// GET THE LATEST NO SERIES
						
						$output = $this->error_message(false, true, false, false, $data, $data['PT_ID'], $this->number_series,'','PT','tblPaymentTerms');
						
					
					if($output['success']){
				
							$table = array(
								'header'  	=> $this->header_table,
							);

							$this->db->trans_begin();

							$payment_terms_model->on_save_module(false, $data, $table, 'PT', $this->number_series,'');

							if($this->db->trans_status() !== False){
								$this->db->trans_commit();
							}
							else{
								$this->db->trans_rollback();
							}
						}
						else{
							throw new Exception($output['message']);
						}

						/* 
								END OF SAVING - ADD FUNCTION
						*/

					// UPDATE HEADER
					}
					else if($this->input->post('todo') == 'update'){

						// $this->print_r($data);

					$output = $this->error_message(false, true, false, false, $data, $data['PT_ID'], $this->number_series,'','PT','tblPaymentTerms');

						if($output['success']):

							$table = array(
								'header'  	=> $this->header_table,
								
							);

							$this->db->trans_begin();

							$payment_terms_model->on_update_module($data, $table, 'PT', $this->number_series,'');

							if($this->db->trans_status() !== False){
								$this->db->trans_commit();
								$this->deleteUserRecordLock_php($data['PT_ID']);
							}
							else{
								$this->db->trans_rollback();
							}
							
						else:
							throw new Exception($output['message']);
						endif;

							/* 
								END OF SAVING - UPDATE FUNCTION
							*/
						
						}
					
					echo json_encode($output); //ENDING QUERY
				}

				catch(Exception $e){

					$str = $e->getMessage();
					$array_msg = explode(" , ", $str);

					$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
					);

					echo json_encode($output);
				}
			endif;

		}



	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}
}
