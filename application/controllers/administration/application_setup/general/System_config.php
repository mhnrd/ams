<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');
class System_config extends MAIN_Controller{
	protected $number_series 		= 102109;
	protected $header_table 		= 'tblSystemConfig';
	protected $header_doc_no_field  = 'SC_ID';
	protected $header_status_field  = '';
	protected $module_name			= 'Configuration';

	public function __construct(){
		parent:: __construct();
		$this->is_secure 	= true;
		$this->breadcrumb[] = array(	'html'	=>	'Administration',
										'url'	=>	'administration');
		$this->breadcrumb[] = array(	'html'	=>	'Application Setup',
										'url'	=>	'');
		$this->breadcrumb[] = array(	'html'	=>	'General',
										'url'	=>	'');
		$this->data['header_table']				= $this->header_table;
		$this->data['header_doc_no_field']  	= $this->header_doc_no_field;
		$this->data['header_status_field']		= $this->header_status_field;
		$this->data['number_series']			= $this->number_series; 

		$this->cols  = array('SC_ID', 'SC_ModuleID', 'SC_Description', 'SC_Value', 'M_Description');
		$this->table = 'tblSystemConfig';
	}
	// Header Table
	public function index(){
		$this->breadcrumb[] 	 = array(	'html'	=> 'Configuration',
											'url'	=> 'administration/application_setup/general/system_config');

		$system_config_model 	= $this->load_model('administration/application_setup/general/system_config/system_config_model');
		$this->data['hdr_center']	 = array(0);
		$this->data['modules'] 		 = $system_config_model->getAllConfigModules();

		$this->load_page('/administration/application_setup/general/system_config/index');
	}


	// Adding Of User 
	public function add(){
		$this->breadcrumb[] 		 = array(	'html'	=> 'Configuration',
												'url'	=> 'administration/application_setup/general/system_config');
		$this->breadcrumb[] 		 = array(	'html'	=> 'Add',
												'url'	=> '');
		//Load Needed Data

		//structure for view page
		$this->data['type']			 = 'add';	
		$this->data['header']		 = $this->getData($this->data['type'], '');
		$this->data['details'] = array();
		$this->load_page('/administration/application_setup/general/system_config/form');
	}

	// Updating User Data
	public function update(){
		$this->breadcrumb[] 		 = array(	'html'	=> 'Configuration',
												'url'	=> 'administration/application_setup/general/system_config');
		$this->breadcrumb[] 		 = array(	'html'	=> 'Update',
												'url'	=> '');
		

		$this->data['type']			 = 'update';
		$this->data['doc_no'] 		 = $_GET['id'];
		$this->data['header']		 = $this->getData($this->data['type'], $this->data['doc_no']);

		$this->load_page('/administration/application_setup/general/system_config/form');
	}

	public function getData($type, $docno = ''){
		$system_config_model 	= $this->load_model('administration/application_setup/general/system_config/system_config_model');
		if ($type == 'add') {
			$output = array(
				'SC_ID' 				=> '',
				'SC_ModuleID' 			=> '',
				'SC_Description' 		=> '',
				'SC_Value' 				=> ''
			);
		}
		else{
			$where = array(
				'SC_ID'  => $docno
			);
			$join_table = array(
				array(
					'tbl_name'  => 'tblModule',
					'tbl_on'  	=> 'M_Module_id = SC_ModuleID',
					'tbl_join'  => 'left'
				)
			);

			$header = $system_config_model->getHeaderByDocNo($this->cols, $this->table, $where, $join_table);

			$output = array(
				'SC_ID' 				=> $header['SC_ID'],
				'SC_ModuleID' 			=> $header['SC_ModuleID'],
				'M_Description' 		=> $header['M_Description'],
				'SC_Description' 		=> $header['SC_Description'],
				'SC_Value' 				=> number_format($header['SC_Value'], 2, ".", "")
			);

		}
		return $output;
	}

	public function updateConfigValue(){
		$data = $this->input->post();

		$this->db->trans_begin();

		$this->db->where('SC_ID', $data['doc-no'])
				 ->set('SC_Value', $data['value'])
				 ->update('tblSystemConfig');

		if($this->db->trans_status() !== False){
			$this->db->trans_commit();
		}
		else{
			$this->db->trans_rollback();
		}
	}

	
	public function searchModules(){

		$filter = $this->input->get('filter-input');
		$system_config_model 	= $this->load_model('administration/application_setup/general/system_config/system_config_model');

		$output = $system_config_model->searchModulesByFilter($filter);

		echo json_encode($output);

	}

	public function getConfigID(){

		$module = $this->input->post('module-id');
		$system_config_model 	= $this->load_model('administration/application_setup/general/system_config/system_config_model');

		$output = $system_config_model->generateConfigID($module);

		echo json_encode($output);

	}

	public function save(){

		if(!$this->input->is_ajax_request()) return;
		

		if(!empty($_POST));

		//Get All POST Data
		$data 				= $this->input->post();


		//Load Needed Data
		
		$system_config_model 	= $this->load_model('administration/application_setup/general/system_config/system_config_model');

		$output = array(
						'success' => true
					,	'message' => '');
		
		try{
			//START OF SAVING - ADD FUNCTION
			if($this->input->post('todo') == 'add'){
					
				$output = $this->error_message(false, true, false, false, $data, $data['SC_ID'], $this->number_series,'','SC','tblSystemConfig');
					
				
				if($output['success']){


						$table = array(
							'header'  	=> $this->header_table,
						);

						$this->db->trans_begin();

						$system_config_model->on_save_module(false, $data, $table, 'SC', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);
					

				$output = $this->error_message(false, true, false, false, $data, $data['SC_ID'], $this->number_series,'','SC','tblSystemConfig');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table
						);

						$this->db->trans_begin();

						// $this->print_r($data);
						// die();

						$system_config_model->on_update_module($data, $table, 'SC', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['SC_ID']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				
				echo json_encode($output); //ENDING QUERY
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		
		
	}

}
?>