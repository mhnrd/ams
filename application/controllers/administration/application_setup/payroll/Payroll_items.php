<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payroll_items extends MAIN_Controller {

	protected $number_series 		= 102602;
	protected $header_table 		= 'tblPAYItems';
	protected $header_doc_no_field	= 'PI_ID';
	protected $header_status_field	= '';

	protected $module_name			= 'Payroll Items';
	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(		'html'	=>	'Administration',
											'url'	=>	'administration');
		$this->breadcrumb[] = array(		'html'	=>	'Application Setup',
											'url'	=>	'');
		$this->breadcrumb[] = array(		'html'	=>	'Payroll',
											'url'	=>	'');
		$this->data['header_table'] 			= $this->header_table;
		$this->data['header_doc_no_field'] 		= $this->header_doc_no_field;
		$this->data['header_status_field'] 		= $this->header_status_field;
		$this->data['number_series'] 			= $this->number_series;
		$this->data['access_header']			= $this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']			= $this->user_model->getDetailAccess($this->number_series);
		$this->cols  = array('PI_ID', 'PI_Desc', 'PI_Type', 'PI_Basis2Compute','PI_Sperate','PI_Regrate','PI_Taxable','PI_Allowance','PI_Cutoff','PI_CutoffType','PI_AttendanceBasis','PI_AttAllowTypeId','PI_Variability','PI_Divisor','AD_Id','AD_Desc');
		$this->table = 'tblPAYItems as PI';
	}

	public  function index(){
		$this->breadcrumb[]	= array(		'html'	=>	'Payroll Items',
											'url'	=>	'administration/application_setup/payroll/payroll_items');

		$this->data['table_hdr']  = array('payroll-items-details' => array(
															'button' 			=> array(
																						'title' => '<a class="btn btn-success btn-xs btn-outline add-large" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="'.DOMAIN.'administration/application_setup/payroll/payroll_items/add" role="button"><i class="fa fa-plus"></i></a>',
																						'class' => 'text-center',
																						'style' => 'width: 100px; vertical-align:middle'
																						),
															'code' 				=> array(
																						'title' => 'Code',
																						'class' => 'text-left',
																						'style' => 'width: 5%; vertical-align:middle'
																						),	
															'description' 		=> array(
																						'title' => 'Description',
																						'class' => 'text-left',
																						'style' => 'width: 30%; vertical-align:middle'
																						),	 
															'type' 				=> array(
																						'title' => 'Type',
																						'class' => 'text-left',
																						'style' => 'width: 10%; vertical-align:middle'
																						),
															'basis-to-compute' 	=> array(
																						'title' => 'Basis to Compute',
																						'class' => 'text-left',
																						'style' => 'width: 10%; vertical-align:middle'
																						),
															'special-rate' 		=> array(
																						'title' => 'Special Rate',
																						'class' => 'text-right',
																						'style' => 'width: 10%; vertical-align:middle'
																						), 
															'regular-rate' 		=> array(
																						'title' => 'Regular Rate',
																						'class' => 'text-right',
																						'style' => 'width: 10%; vertical-align:middle'
																						), 
															'taxable' 			=> array(
																						'title' => 'Taxable',
																						'class' => 'text-center',
																						'style' => 'width: 10px; vertical-align:middle'
																						), 
															'allowance-type' 	=> array(
																						'title' => 'Allowance Type',
																						'class' => 'text-left',
																						'style' => 'width: 15%; vertical-align:middle'
																						),

				)
			);
		$this->data['hdr_center'] = array(0,1,2,3);
		$this->load_page('administration/application_setup/payroll/payroll_items/index');
	}

	public function add(){
		$this->breadcrumb[]	= array(		'html'	=>	'Payroll Items',
											'url'	=>	'administration/application_setup/payroll/payroll_items');
		$this->breadcrumb[]	= array(		'html'	=>	'Add',
											'url'	=>	'');

		$allowance_type_list = $this->load_model('administration/application_setup/payroll/payroll_items/payroll_items_model');	



		$this->data['type']		 			= 'add';											
		$this->data['header']	 			= $this->getData($this->data['type'], '');
		$this->data['allowance_type_list']  = $allowance_type_list->getAllowanceType();
		$this->load_page('administration/application_setup/payroll/payroll_items/form');
	}

	public function update(){
		$this->breadcrumb[]	= array(		'html'	=>	'Payroll Items',
											'url'	=>	'administration/application_setup/payroll/payroll_items');
		$this->breadcrumb[]	= array(		'html'	=>	'Update',
											'url'	=>	'');
		$allowance_type_list = $this->load_model('administration/application_setup/payroll/payroll_items/payroll_items_model');	

		// STRUCTURE FOR VIEW PAGE
		$this->data['type']	= 'update';
		$this->data['id'] = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);
		$this->data['allowance_type_list']  = $allowance_type_list->getAllowanceType();
		$this->load_page('administration/application_setup/payroll/payroll_items/form');
	}

	public function view(){
		$this->breadcrumb[]	= array(		'html'	=>	'Payroll Items',
											'url'	=>	'administration/application_setup/payroll/payroll_items');
		$this->breadcrumb[]	= array(		'html'	=>	'View',
											'url'	=>	'');

		$allowance_type_list = $this->load_model('administration/application_setup/payroll/payroll_items/payroll_items_model');
		// STRUCTURE FOR VIEW PAGE
		$this->data['type'] = 'view';
		$this->data['id']	= $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);
		$this->data['allowance_type_list']  = $allowance_type_list->getAllowanceType();
		$this->load_page('administration/application_setup/payroll/payroll_items/form');
	}


	// Getting of data from the Form
	public function getData($type, $docno = ''){

		$payroll_items_model = $this->load_model('administration/application_setup/payroll/payroll_items/payroll_items_model');

		if ($type == 'add') {
			$id = $payroll_items_model->getLastGroupCode();
			$output = array(
				'PI_ID' 				=> $id,
				'PI_Desc' 				=> '',
				'PI_Type'  				=> '',
				'PI_Basis2Compute' 		=> '',
				'PI_Sperate' 			=> '0',
				'PI_Regrate'  			=> '0',
				'PI_Taxable' 			=> '',
				'PI_Allowance' 			=> '',
				'PI_Cutoff' 			=> '',
				'PI_CutoffType' 		=> '',
				'PI_AttendanceBasis' 	=> '',
				'PI_AttAllowTypeId' 	=> '',
				'PI_Variability' 		=> '',
				'PI_Divisor' 			=> '', 	 	
			);
		}
		else{
			$where = array(
				'PI_ID' => $docno
			);
			$join_table = array(
				array(
					'tbl_name'  => 'tblAttributeDetail',
					'tbl_on' 	=> 'AD_Id = PI_AttAllowTypeId',
					'tbl_join' 	=> 'left'
				),
			);

			$header = $payroll_items_model->getHeaderByDocNo($this->cols, $this->header_table, $where,$join_table);

			$output = array(
				'PI_ID' 				=> $header['PI_ID'],
				'PI_Desc' 				=> $header['PI_Desc'],
				'PI_Type'  				=> $header['PI_Type'],
				'PI_Basis2Compute' 		=> $header['PI_Basis2Compute'],
				'PI_Sperate' 			=> $header['PI_Sperate'],
				'PI_Regrate'  			=> $header['PI_Regrate'],
				'PI_Taxable' 			=> $header['PI_Taxable'],
				'PI_Allowance' 			=> $header['PI_Allowance'],
				'PI_Cutoff' 			=> $header['PI_Cutoff'],
				'PI_CutoffType' 		=> $header['PI_CutoffType'],
				'PI_AttendanceBasis' 	=> $header['PI_AttendanceBasis'],
				'PI_AttAllowTypeId' 	=> $header['AD_Desc'],
				'PI_Variability' 		=> $header['PI_Variability'],
				'PI_Divisor' 			=> $header['PI_Divisor'], 	 	
				

			);
		}
		return $output;
	}


	public function data(){

		$payroll_items_model = $this->load_model('administration/application_setup/payroll/payroll_items/payroll_items_model');		
		$table_data = $payroll_items_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/application_setup/payroll/payroll_items/view?id='.md5($value['PI_ID']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/application_setup/payroll/payroll_items/update?id='.md5($value['PI_ID']).'">
					<i class="fa fa-pencil"></i>
					</a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['PI_ID'].'" "><i class="fa fa-trash"></i></a> ';
			//}

			$sub_array[] = $button;
			$sub_array[] = $value['PI_ID'];
			$sub_array[] = $value['PI_Desc'];
			$sub_array[] = ($value['PI_Type'] == 'E' ? 'Earnings' : ($value['PI_Type'] == 'D' ? 'Deduction' : ''));
			$sub_array[] = $value['PI_Basis2Compute'];
			$sub_array[] = number_format($value['PI_Sperate'],2);
			$sub_array[] = number_format($value['PI_Regrate'],2);
			$sub_array[] = '<input type="checkbox" disabled '.($value['PI_Taxable'] ? 'checked' : '').'>';
			$sub_array[] = $value['AD_Desc'];
			
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
			if(!$this->input->is_ajax_request()) return;

			if(!empty($_POST)):

				// Get All POST Data
				$data = $this->input->post();

				// Load All needed model
				$payroll_items_model = $this->load_model('administration/application_setup/payroll/payroll_items/payroll_items_model');
				// GET CURRENT USER
				$current_user 		 = getCurrentUser()['login-user'];

				$output = array(
								'success'	=> true
							,	'message'	=> '');
				try{

					// Start Of Saving - Add Function
					if($this->input->post('todo') == 'add'){
						// GET THE LATEST NO SERIES
						
						$output = $this->error_message(false, true, false, false, $data, $data['PI_ID'], $this->number_series,'','PI','tblPAYItems');
						
					
					if($output['success']){
				
							$table = array(
								'header'  	=> $this->header_table,
							);

							$this->db->trans_begin();

							$payroll_items_model->on_save_module(false, $data, $table, 'PI', $this->number_series,'');

							if($this->db->trans_status() !== False){
								$this->db->trans_commit();
							}
							else{
								$this->db->trans_rollback();
							}
						}
						else{
							throw new Exception($output['message']);
						}

						/* 
								END OF SAVING - ADD FUNCTION
						*/

					// UPDATE HEADER
					}
					else if($this->input->post('todo') == 'update'){

						// $this->print_r($data);

					$output = $this->error_message(false, true, false, false, $data, $data['PI_ID'], $this->number_series,'','PI','tblPAYItems');

						if($output['success']):

							$table = array(
								'header'  	=> $this->header_table,
								
							);

							$this->db->trans_begin();

							$payroll_items_model->on_update_module($data, $table, 'PI', $this->number_series,'');

							if($this->db->trans_status() !== False){
								$this->db->trans_commit();
								$this->deleteUserRecordLock_php($data['PI_ID']);
							}
							else{
								$this->db->trans_rollback();
							}
							
						else:
							throw new Exception($output['message']);
						endif;

							/* 
								END OF SAVING - UPDATE FUNCTION
							*/
						
						}
					
					echo json_encode($output); //ENDING QUERY
				}

				catch(Exception $e){

					$str = $e->getMessage();
					$array_msg = explode(" , ", $str);

					$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
					);

					echo json_encode($output);
				}
			endif;

		}

	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}
}