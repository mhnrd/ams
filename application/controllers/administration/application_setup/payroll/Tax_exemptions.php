<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tax_exemptions extends MAIN_Controller {

	protected $number_series		= 	1756;
	protected $header_table			=	'tblTaxMatrix';
	protected $header_doc_no_field	=	'TM_ID';
	protected $header_status_field	=	'';
	protected $module_name			=	'Tax Exemptions';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(		'html'	=>	'Administration',
											'url'	=>	'');
		$this->breadcrumb[] = array(		'html'	=>	'Application Setup',
											'url'	=>	'');
		$this->breadcrumb[] = array(	'html'	=>	'Payroll',
										'url'	=>	'');

		$this->data['header_table']			=	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		= 	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		//$this->cols 						= 	array('');
		//$this->table 						=   'tblCustomer as C';
	}

	public  function index(){
		$this->breadcrumb[]	= array(		'html'	=>	'Tax Exemptions',
											'url'	=>	'administration/application_setup/payroll/tax_exemptions');

		$this->data['table_hdr'] = array('tax_exemptions'	=> array(
													'tax_type' 			=>	array(
																					'title' 	=>	'Tax Type',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
																	)

										);
		
		$this->data['hdr_center'] = array(0);

		$this->data['detail'] 	  = $this->generate_detail_table();
		$this->load_page('administration/application_setup/payroll/tax_exemptions/index');
	}

	public function generate_detail_table(){
		$dtl_data[] = '';

		$dtl_data['table_dtl'] = array('tax_exemptions_detail' => array(
												'buttons' 				=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" 
																									data-toggle="tooltip" data-placement="top" 
																									title="Add" type="button" href="'.
																									DOMAIN.'administration/application_setup/payroll/tax_exemptions/add"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 100px; vertical-align: middle' 
																				), 
												'tax_id' 				=>	array(
																					'title' 	=> 'Tax ID',
																					'class' 	=> 'text-left',
																					'style'  	=> 'width: 10%; vertical-align: middle' 
																				),
												'over_amount' 			=>	array(
																					'title'  	=> 'Over Amount',
																					'class'  	=> 'text-right',
																					'style'  	=> 'width: 20%; vertical-align: middle' 
																				),
												'not_over_amount' 			=>	array(
																					'title'  	=> 'Not Over Amount',
																					'class'  	=> 'text-right',
																					'style'  	=> 'width: 20%; vertical-align: middle' 
																				),
												'tax-due' 			=>	array(
																					'title'  	=> 'Tax Due',
																					'class'  	=> 'text-right',
																					'style'  	=> 'width: 25%; vertical-align: middle' 
																				),
												'percent' 			=>	array(
																					'title'  	=> 'Percent(%)',
																					'class'  	=> 'text-right',
																					'style'  	=> 'width: 25%; vertical-align: middle' 
																				),
																)

										);
		$dtl_data['dtl_center'] = array(0,1,2,3,4,5);

		return $dtl_data;
	}

	public function add(){
		$this->breadcrumb[]	= array(	'html'	=>	'Tax Exemptions',
										'url'	=>	'administration/application_setup/payroll/tax_exemptions');
		$this->breadcrumb[]	= array(	'html'	=>	'Add',
										'url'	=> 	'');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type'] 					= 'add';
		$this->data['header'] 					= $this->getData($this->data['type'], '');


		$this->load_page('administration/application_setup/payroll/tax_exemptions/form');
	}

	public function update(){
		$this->breadcrumb[]	= array(	'html'	=>	'Tax Exemptions',
										'url'	=>	'administration/application_setup/payroll/tax_exemptions');
		$this->breadcrumb[]	= array(	'html'	=>	'Update',
										'url'	=> 	'');


		// STRUCTURE FOR VIEW PAGE
		$this->data['type'] 	 = 'update';
		$this->data['docno'] 	 = $_GET['id'];
		$this->data['header']    = $this->getData($this->data['type'], $this->data['docno']);
		
		$this->load_page('administration/application_setup/payroll/tax_exemptions/form');
	}

	public function view(){
		$this->breadcrumb[]	= array(	'html'	=>	'Tax Exemptions',
										'url'	=>	'administration/application_setup/payroll/tax_exemptions');
		$this->breadcrumb[]	= array(	'html'	=>	'View',
										'url'	=> 	'');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type'] 	 = 'view';
		$this->data['docno'] 	 = $_GET['id'];
		$this->data['header']    = $this->getData($this->data['type'], $this->data['docno']);

		$this->load_page('administration/application_setup/payroll/tax_exemptions/form');
	}

	public function getData($type, $docno = ''){
	$tax_exemption_model = $this->load_model('administration/application_setup/payroll/tax_exemptions/tax_exemptions_model');

		
		if($type == 'add'){
			$id = $tax_exemption_model->getLastGroupCode();
			$output = array(
				'TM_ID' 					 => $id, 
				'TM_FK_ExemptID' 			 => '', 
				'TM_OverAmount'	             => '',
				'TM_NotOver'                 => '',
				'TM_TaxDue'		             => '',
				'TM_Percent'            	 => '',
				'TM_TaxType'         		 => '',

			);
		}
		else if($type == 'update' || $type == 'view'){

			$where = array(
				'CAST(TM_ID as varchar)' => $docno
			);

			
			$header = $tax_exemption_model->getHeaderByDocNo($this->cols, $this->header_table, $where);

			$output = array(
				'TM_ID'                 	 => $header['TM_ID'], 	
				'TM_FK_ExemptID'             => $header['TM_FK_ExemptID'],
				'TM_OverAmount'              => numeric($header['TM_OverAmount'],2),
				'TM_NotOver'        		 => numeric($header['TM_NotOver'],2),
				'TM_TaxDue'          		 => numeric($header['TM_TaxDue'],2),
				'TM_Percent'                 => numeric($header['TM_Percent'],2),
				'TM_TaxType'                 => $header['TM_TaxType'],
				// 'functions' 				 => $this->getApprovalButtons($header['TM_ID']
				// 												,$header['TM_OverAmount']
				// 												,$header['TM_NotOver']
				// 												,getCurrentUser()['login-user']
				// 												,$this->header_table
				// 												,''
				// 												,$this->header_doc_no_field
				// 											),
			);

		}

		return $output;
	}

	public function data(){

		$tax_exemption_model = $this->load_model('/administration/application_setup/payroll/tax_exemptions/tax_exemptions_model');		
		$table_data = $tax_exemption_model->table_data();

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			
			$sub_array[] = $value['TM_TaxType'];
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_TM(){
		$tax_exemption_model = $this->load_model('/administration/application_setup/payroll/tax_exemptions/tax_exemptions_model');		
		$table_data = $tax_exemption_model->table_data_TM();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/application_setup/payroll/tax_exemptions/view?id='.md5($value['TM_ID']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/application_setup/payroll/tax_exemptions/update?id='.md5($value['TM_ID']).'">
									<i class="fa fa-pencil"></i>
								</a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['TM_ID'].'"><i class="fa fa-trash"></i></a> ';
			//}

			$sub_array[] = $button;										
			$sub_array[] = $value['TM_ID'];
			$sub_array[] = number_format($value['TM_OverAmount'], 2);
			$sub_array[] = number_format($value['TM_NotOver'], 2);
			$sub_array[] = number_format($value['TM_TaxDue'], 2);
			$sub_array[] = number_format($value['TM_Percent'], 2);

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_detail(){
		$docno = $this->input->get('id');
		
		$tax_exemption_model = $this->load_model('/administration/application_setup/payroll/tax_exemptions/tax_exemptions_model');		
		$table_data = $tax_exemption_model->table_data_detail($docno);
		

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
											
			$sub_array[] = number_format($value['TM_OverAmount'], 2);
			$sub_array[] = number_format($value['TM_NotOver'], 2);
			$sub_array[] = number_format($value['TM_TaxDue'], 2);
			$sub_array[] = number_format($value['TM_Percent'], 2);
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;
		
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			
			// Load All needed Model
			$tax_exemption_model = $this->load_model('administration/application_setup/payroll/tax_exemptions/tax_exemptions_model');

			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];			


			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){
					// GET THE LATEST NO SERIES

					$output = $this->error_message(false, true, false, false, $data, $data['TM_ID'], $this->number_series, '', 'TM', $this->header_table);

					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table,
				
						);

						$this->db->trans_begin();

						$tax_exemption_model->on_save_module(false, $data, $table, 'TM', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					
					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);

					$output = $this->error_message(false, true, false, false, $data, $data['TM_ID'], $this->number_series, '', '', $this->header_table);

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,
			
						);

						$this->db->trans_begin();

						$tax_exemption_model->on_update_module($data, $table, 'TM', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
		
	}

	// END OF AJAX REQUEST
	
	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}
}