<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');
class Pay_group extends MAIN_Controller{
	protected $number_series 		= 1751;
	protected $header_table 		= 'tblPAYGroup';
	protected $header_doc_no_field  = 'G_ID';
	protected $header_status_field  = '';
	protected $module_name			= 'Pay Group';

	public function __construct(){
		parent:: __construct();
		$this->is_secure 	= true;
		$this->breadcrumb[] = array(	'html'	=>	'Administration',
										'url'	=>	'administration');
		$this->breadcrumb[] = array(	'html'	=>	'Application Setup',
										'url'	=>	'');
		$this->breadcrumb[] = array(	'html'	=>	'Payroll',
										'url'	=>	'');
		$this->data['header_table']				= $this->header_table;
		$this->data['header_doc_no_field']  	= $this->header_doc_no_field;
		$this->data['header_status_field']		= $this->header_status_field;
		$this->data['number_series']			= $this->number_series; 

		$this->cols  = array('G_ID', 'G_FK_CompanyId', 'G_Desc', 'COM_Id', 'G_PAY_SSSDeduct','G_PAY_CutOffEnd','G_PAY_Payperiod','G_PAY_PreparedBy', 'G_PAY_ApprovedBy','G_PAY_BankAccount','COM_Name','G_PAY_TaxCuttoff','G_PAY_CutOffStart', 'G_PAY_PagibigDeduct', 'G_PAY_PhilhealthDeduct', 'G_PAY_SSSCutoff','G_PAY_PagibigCutoff', 'G_PAY_PhilhealthCutoff', 'G_PAY_WorkDays', 'G_PAY_WorkDays_D', 'G_PAY_NoAttendance');
		$this->table = 'tblPAYGroup as G';
		$this->cols_detail 					=   array('GA_FK_User_Id','U_AssociatedEmployeeId','U_Username');
		$this->table_detail 				=   'tblPAYGroupAccess';
	}
	// Header Table
	public function index(){
		$this->breadcrumb[] 	 = array(	'html'	=> 'Pay Group',
											'url'	=> 'administration/application_setup/payroll/pay_group');
		$this->data['table_hdr'] = array('paygroup' => array(
										'button' 					=> array(
																				'title' => '<a class="btn btn-success btn-xs btn-outline add-large" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="'.DOMAIN.'administration/application_setup/payroll/pay_group/add" role="button"><i class="fa fa-plus"></i></a>',
																				'class' => 'text-center',
																				'style' => 'width: 5%; vertical-align: middle'
																			),
										'g-company' 				=> array(
																				'title' => 'Company',
																				'class' => 'text-left',
																				'style' => 'width: 5%; vertical-align: middle'
																			),
										'g-id' 						=> array(
																				'title' => 'Group ID',
																				'class' => 'text-left',
																				'style' => 'width: 5%; vertical-align:middle'
																			),
										'g-desc' 					=> array(
																				'title' => 'Group Description',
																				'class' => 'text-left',
																				'style' => 'width: 5%; vertical-align:middle'
																			), 
										'tax-cutoff'				=> array(
																				'title' => 'Tax CutOff',
																				'class' => 'text-center',
																				'style' => 'width: 5%; vertical-align:middle'
																			),
										'sss-deduction' 			=> array(
																				'title' => 'SSS Deduction',
																				'class' => 'text-left',
																				'style' => 'width: 5%; vertical-align:middle'
																			),
										'sss-cutoff' 				=> array(
																				'title' => 'SSS CutOff',
																				'class' => 'text-left',
																				'style' => 'width: 5%; vertical-align:middle'
																			), 
										'pag-ibig-deduction' 		=> array(
																				'title' => 'Pag-Ibig Deduction',
																				'class' => 'text-left',
																				'style' => 'width: 5%; vertical-align:middle'	
																			),
										'pag-ibig-cutoff' 			=> array(
																				'title' => 'Pag-Ibig CutOff',
																				'class' => 'text-left',
																				'style' => 'width: 5%; vertical-align:middle'
																			),
										'philhealth-deduction' 		=> array(
																				'title' => 'Philhealth Deduction',
																				'class' => 'text-left',
																				'style' => 'width: 5%; vertical-align:middle'
																			),
										'philhealth-cutoff' 		=> array(
																				'title' => 'Philhealth CutOff',
																				'class' => 'text-left',
																				'style' => 'width: 5%; vertical-align:middle'
																			),
		)
	);
		$this->data['hdr_center']	 = array(0,2,3,4,5,6,7,8,9,10,11,12);
		$this->data['type'] 		 = 'index';
		//$this->data['header']		 = $this->getData($this->data['type']);
		$this->data['detail_table']		 = $this->generate_detail_table();

		$this->load_page('/administration/application_setup/payroll/pay_group/index');
	}

	public function generate_detail_table(){
		
		$dtl_data[]= '';

		$dtl_data['table_dtl'] = array('pay_group_detail' => array(
														'user_id' 	=> array(
																				'title' => 'User ID',
																				'class' => 'text-left',
																				'style' => 'width: 5%; vertical-align: middle'
																			),
														'username'  => array(
																				'title' => 'Username',
																				'class' => 'text-left',
																				'style' => 'width: 5%; vertical-align:middle'
																			),
			)

		);
		$dtl_data['dtl_center'] = array(0,1);

		return $dtl_data;
	}


	// Adding Of User 
	public function add(){
		$this->breadcrumb[] 		 = array(	'html'	=> 'Pay Group',
												'url'	=> 'administration/application_setup/payroll/pay_group');
		$this->breadcrumb[] 		 = array(	'html'	=> 'Add',
												'url'	=> '');
		//Load Needed Data
		
		
		$category_list 				 = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_model');
		$pay_group_model			 = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_model');
		$user_id_list				 = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_model');

		//structure for view page
		$this->data['type']			 = 'add';		
		$this->data['category_list'] = $category_list->getAll_Active();
		$this->data['user_id_list']	 = $user_id_list->getUser_ID();	
		$this->data['header']		 = $this->getData($this->data['type'], '');
		$this->data['details'] = array();

		$this->data['table_detail'] = array('tbl-detail'	=> array(
									'button' 			=> 	array(
																	'title' 	=>	'<a class="btn btn-success btn-xs 
																					btn-outline add-detail" 
																					data-toggle="tooltip" data-placement="top"title="Add"><i class="fa fa-plus"></i></a>',
																	'class' 	=>	'text-center',
																	'style' 	=>	'width: 100px; vertical-align: middle' 
																),
									'user_id' 			=>	array(
																	'title' 	=>	'User ID',
																	'class' 	=>	'text-left',
																	'style' 	=>	'width: 20%; vertical-align: middle'
																),
									'emp-id' 			=>	array(
																	'title' 	=>	'Employee ID',
																	'class' 	=>	'text-left',
																	'style' 	=>	'width: 50%; vertical-align: middle'
																),
									'username' 			=>	array(
																	'title' 	=>	'Username',
																	'class' 	=>	'text-left',
																	'style' 	=>	'width: 30%; vertical-align: middle'
																)


										)

									);
		$this->load_page('/administration/application_setup/payroll/pay_group/form');
	}

	// Updating User Data
	public function update(){
		$this->breadcrumb[] 		 = array(	'html'	=> 'Pay Group',
												'url'	=> 'administration/application_setup/payroll/pay_group');
		$this->breadcrumb[] 		 = array(	'html'	=> 'Update',
												'url'	=> '');
		
		$pay_group_model 			 = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_detail_model');
		$category_list 				 = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_model');
		$user_id_list				 = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_model');

		$this->data['type']			 = 'update';
		$this->data['doc_no'] 		 = $_GET['id'];
		$this->data['category_list'] = $category_list->getAll_Active();
		$this->data['user_id_list']	 = $user_id_list->getUser_ID();
		//$this->data['details']		 = $pay_group_model->getAll_User();
		$this->data['header']		 = $this->getData($this->data['type'], $this->data['doc_no']);

		$where_docno = array('GA_G_ID' => $this->data['doc_no']);
		$join_table = array(
			array(
				'tbl_name'  	=> 'tblUser',
				'tbl_on'  		=> 'U_ID = GA_FK_User_Id',
				'tbl_join'  	=> 'left' 
			)
		);

		$this->data['details'] = $pay_group_model->getDetails($this->cols_detail, $this->table_detail, $where_docno, array(), $join_table, 'GA_LineNo');
		// $this->print_r($this->data['details']);
		// die();
		
		
		$this->data['table_detail'] = array('tbl-detail'	=> array(
									'button' 			=> 	array(
																	'title' 	=>	'<button class="btn btn-success btn-xs 
																					btn-outline add-detail" 
																					data-toggle="tooltip" data-placement="top"title="Add"><i class="fa fa-plus"></i></button>',
																	'class' 	=>	'text-center',
																	'style' 	=>	'width: 5%; vertical-align: middle' 
																),
									'user_id' 			=>	array(
																	'title' 	=>	'User ID',
																	'class' 	=>	'text-center',
																	'style' 	=>	'width: 10%; vertical-align: middle'
																),
									'emp-id' 			=>	array(
																	'title' 	=>	'Employee ID',
																	'class' 	=>	'text-center',
																	'style' 	=>	'width: 10%; vertical-align: middle'
																),
									'username' 			=>	array(
																	'title' 	=>	'Username',
																	'class' 	=>	'text-center',
																	'style' 	=>	'width: 10%; vertical-align: middle'
																)
												)

									);
		$this->load_page('/administration/application_setup/payroll/pay_group/form');
	}

	//Viewing Of Users Data
	public function view(){
		$this->breadcrumb[] 		 = array(	'html'	=> 'Pay Group',
												'url'	=> 'administration/application_setup/payroll/pay_group');
		$this->breadcrumb[]			 = array(	'html'	=> 'View',
												'url'	=> '');
		
		$pay_group_model 			 = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_model');
		$category_list 				 = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_model');
		$user_id_list				 = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_model');

		$this->data['type']			 = 'view';
		$this->data['doc_no'] 		 = $_GET['id'];
		$this->data['category_list'] = $category_list->getAll_Active();
		$this->data['user_id_list']	 = $user_id_list->getUser_ID();
		$this->data['header']		 = $this->getData($this->data['type'], $this->data['doc_no']);
		//$this->data['details']		 = $pay_group_model->getAll_User();

		$where_docno = array('GA_G_ID' => $this->data['doc_no']);

		$this->data['details'] = array();


		$this->data['table_detail'] = array('tbl-detail'	=> array(
													'user_id' 			=>	array(
																					'title' 	=>	'User ID',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'emp_id' 			=>	array(
																					'title' 	=>	'Employee ID',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 35%; vertical-align: middle'
																				),
													'username' 			=>	array(
																					'title' 	=>	'Username',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				)
												)

									);
		$this->load_page('/administration/application_setup/payroll/pay_group/form');
	}

	public function getData($type, $docno = ''){
		$pay_group_model 	= $this->load_model('administration/application_setup/payroll/pay_group/pay_group_model');
		if ($type == 'add') {
			$output = array(
				'G_FK_CompanyId' 				=> '',
				'G_ID' 						=> '',
				'G_Desc' 						=> '',
				'G_PAY_CutOffStart' 			=> '',
				'G_PAY_CutOffEnd' 				=> '',
				'G_PAY_Payperiod' 				=> '',
				'G_PAY_PreparedBy' 				=> '',
				'G_PAY_ApprovedBy' 				=> '',
				'G_PAY_BankAccount' 			=> '',
				'COM_Id' 						=> '',
				'COM_Name' 						=> '', 		
				'CreatedBy' 					=> '',
				'DateCreated' 					=> '',
				'ModifiedBy' 					=> '',
				'DateModified'  				=> '',
				'COM_PAY_TaxDeduct' 			=> '', 
				'G_PAY_SSSDeduct'				=> '',
				'G_PAY_PagibigDeduct' 			=> '',
				'G_PAY_PhilhealthDeduct' 		=> '',
				'G_PAY_TaxCuttoff' 				=> '',
				'G_PAY_SSSCutoff' 				=> '',
				'G_PAY_PagibigCutoff' 			=> '',
				'G_PAY_PhilhealthCutoff' 		=> '',
				'G_PAY_WorkDays' 				=> '',
				'G_PAY_WorkDays_D' 				=> '',
				'G_PAY_Tracking' 				=> '',
				'G_PAY_NoAttendance' 			=> '0',
				'G_PAY_NextPayperiod' 			=> '',
				'G_AttendanceStat' 				=> 0 	
			);
		}
		else{
			$where = array(
				'G_ID'  => $docno
			);
			$join_table = array(
				array(
					'tbl_name' => 'tblCompany',
					'tbl_on'   => 'COM_Id = G_FK_CompanyId',
					'tbl_join' => 'left'
				),
			);

			$header = $pay_group_model->getHeaderByDocNo($this->cols, $this->table, $where, $join_table);

			$output = array(
				'G_FK_CompanyId' 				=> $header['G_FK_CompanyId'],
				'G_ID' 							=> $header['G_ID'],
				'G_Desc' 						=> $header['G_Desc'],
				'G_PAY_CutOffStart' 			=> date_format(date_create($header['G_PAY_CutOffStart']), 'm/d/Y'),
				'G_PAY_CutOffEnd' 				=> date_format(date_create($header['G_PAY_CutOffEnd']), 'm/d/Y'),
				'G_PAY_Payperiod' 				=> date_format(date_create($header['G_PAY_Payperiod']), 'm/d/Y'),
				'G_PAY_PreparedBy' 				=> $header['G_PAY_PreparedBy'],
				'G_PAY_ApprovedBy' 				=> $header['G_PAY_ApprovedBy'],
				'G_PAY_BankAccount' 			=> $header['G_PAY_BankAccount'],
				'COM_Name' 						=> $header['COM_Name'],
				'COM_Id' 						=> $header['COM_Id'], 
				//'COM_PAY_TaxDeduct' 			=> $header['COM_PAY_TaxDeduct'],
				'G_PAY_SSSDeduct'				=> $header['G_PAY_SSSDeduct'],
				'G_PAY_PagibigDeduct' 			=> $header['G_PAY_PagibigDeduct'],
				'G_PAY_PhilhealthDeduct' 		=> $header['G_PAY_PhilhealthDeduct'],
				'G_PAY_TaxCuttoff' 				=> $header['G_PAY_TaxCuttoff'],
				'G_PAY_SSSCutoff' 				=> $header['G_PAY_SSSCutoff'],
				'G_PAY_PagibigCutoff' 			=> $header['G_PAY_PagibigCutoff'],
				'G_PAY_PhilhealthCutoff' 		=> $header['G_PAY_PhilhealthCutoff'],
				'G_PAY_WorkDays' 				=> number_format($header['G_PAY_WorkDays'],0),
				'G_PAY_WorkDays_D' 				=> $header['G_PAY_WorkDays_D'],
				'G_PAY_NoAttendance' 			=> $header['G_PAY_NoAttendance'],
				// 'functions' 					=> $this->getApprovalButtons($header['G_Id']
				// 															,getCurrentUser()['login-user']
				// 															,$header['G_Desc']
				// 															,''
				// 															,$this->header_table
				// 															,$this->header_status_field
				// 															,$this->header_doc_no_field
				// 														), 
			);

		}
		return $output;
	}

	
	// Data to be display in Table
	public function data(){
		$pay_group_model = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_model');
		$table_data 	 = $pay_group_model->table_data();
		//$access_detail 	 = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array  = array();
			$button 	= '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/application_setup/payroll/Pay_group/view?id='.md5($value['G_ID']).'"><i class="fa fa-eye"></i></a> ';
			//}
			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/application_setup/payroll/Pay_group/update?id='.md5($value['G_ID']).'">
									<i class="fa fa-pencil"></i>
								</a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['G_ID'].'" "><i class="fa fa-trash"></i></a> ';
			//}

			$sub_array[] = $button;
			$sub_array[] = $value['COM_Id'];
			$sub_array[] = $value['G_ID'];
			$sub_array[] = $value['G_Desc'];
			//$sub_array[] = ($value['COM_PAY_TaxDeduct'] == 'S' ? 'Semi-Monthly' :  '');
			$sub_array[] = $value['G_PAY_TaxCuttoff'];
			$sub_array[] = $value['G_PAY_SSSDeduct'];
			$sub_array[] = $value['G_PAY_SSSCutoff'];
			$sub_array[] = $value['G_PAY_PagibigDeduct'];
			$sub_array[] = $value['G_PAY_PagibigCutoff'];
			$sub_array[] = $value['G_PAY_PhilhealthDeduct'];
			$sub_array[] = $value['G_PAY_PhilhealthCutoff'];
			//$sub_array[] = $value['G_PAY_NoAttendance'];



			$data[]		 = $sub_array;
		}
		$output = array(
			"draw"  	 		=> intval($_POST['draw']),
			"recordsTotal"  	=> $table_data['count_all_results'],
			"recordsFiltered" 	=> $table_data['num_rows'],
			"data" 				=> $data 
		);
		echo json_encode($output);
	}

	public function data_detail(){
		$doc_no = $this->input->get('id');
		// $dr_docno 	= $this->input->get('id');
		// $bu_code 	= $this->input->get('bu_code');

		$pay_group_model = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_detail_model');		
		$table_data = $pay_group_model->table_data($doc_no);

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			
			$sub_array[] = $value['GA_FK_User_Id'];
			$sub_array[] = $value['U_AssociatedEmployeeId'];
			$sub_array[] = $value['U_Username'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_PGD(){

		$paygroup_id 	= $this->input->get('id');
		$pay_group_model = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_detail_model');
		$table_data = $pay_group_model->table_data_PGD($paygroup_id);;
		//$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array  = array();

			$sub_array[] = $value['GA_FK_User_Id'];
			$sub_array[] = $value['U_Username'];

			$data[] = $sub_array;
		}
		$output = array(
			"draw" 				=> intval($_POST["draw"]),
			"recordsTotal" 		=> $table_data['count_all_results'],
			"recordsFiltered" 	=> $table_data['num_rows'],
			"data" 				=> $data
		);
		echo json_encode($output);
	}

	public function getUser(){

		$filter = $this->input->get();

		$pay_group_detail_model = $this->load_model('administration/application_setup/payroll/pay_group/pay_group_detail_model');

		$output = $pay_group_detail_model->searchStyles($filter['filter-type'], $filter['filter-search']);

		echo json_encode($output); 

	}


	public function save(){

		if(!$this->input->is_ajax_request()) return;
		

		if(!empty($_POST));

		//Get All POST Data
		$data 				= $this->input->post();


		//Load Needed Data
		
		$pay_group_model 	= $this->load_model('administration/application_setup/payroll/pay_group/pay_group_model');

		//Get Current Login User
		$current_user 		= getCurrentUser()['login-user'];

		$output = array(
						'success' => true
					,	'message' => '');
		
		try{
			//START OF SAVING - ADD FUNCTION
			if($this->input->post('todo') == 'add'){
				// $this->print_r($data);
				// die();
					// GET THE LATEST NO SERIES
					
					$output = $this->error_message(false, true, true, false, $data, $data['G_ID'], $this->number_series,'','G','tblPAYGroup');
					
				
				if($output['success']){

					
					foreach($data['details'] as $key => $value){
						$data['details'][$key]['GA_G_ID'] 			= $data['G_ID'];
						$data['details'][$key]['GA_FK_CompanyId']  		= $data['G_FK_CompanyId'];
					}

						$table = array(
							'header'  	=> $this->header_table,
							'detail' 	=> 'tblPAYGroupAccess' 
						);

						$this->db->trans_begin();

						$pay_group_model->on_save_module(false, $data, $table, 'G', $this->number_series,'', 'GA');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);
					

				$output = $this->error_message(false, true, true, false, $data, $data['G_ID'], $this->number_series,'','G','tblPAYGroup');

					if($output['success']):
							foreach($data['details'] as $key => $value){
								$data['details'][$key]['GA_G_ID'] 				= $data['G_ID'];
								$data['details'][$key]['GA_FK_CompanyId']  		= $data['G_FK_CompanyId'];
								$data['details'][$key]['GA_LineNo'] 			= $key+1;
							}

						$table = array(
							'header'  	=> $this->header_table,
							'detail' 	=> 'tblPAYGroupAccess'
							
						);

						$this->db->trans_begin();

						// $this->print_r($data);
						// die();

						$pay_group_model->on_update_module($data, $table, 'G', $this->number_series,'','GA');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['G_ID']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				
				echo json_encode($output); //ENDING QUERY
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		
		
	}

	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}
}
?>