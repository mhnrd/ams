<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SSS_table extends MAIN_Controller {

	protected $number_series		= 	'';
	protected $header_table			=	'tblSSSTable';
	protected $header_doc_no_field	=	'ST_ID';
	protected $header_status_field	=	'';
	protected $module_name			=	'SSS Table';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(		'html'	=>	'Administration',
											'url'	=>	'');
		$this->breadcrumb[] = array(		'html'	=>	'Application Setup',
											'url'	=>	'');
		$this->breadcrumb[] = array(	'html'	=>	'Payroll',
										'url'	=>	'');

		$this->data['header_table']			=	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		= 	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						= 	array('ST_ID','ST_SalaryRangeFrom','ST_SalaryRangeTo','ST_SalaryCredit','ST_EC','ST_Emprshare','ST_Empeshare');
		$this->table 						=   'tblSSSTable as ST';
	}

	public  function index(){
		$this->breadcrumb[]	= array(		'html'	=>	'SSS Table',
											'url'	=>	'administration/application_setup/payroll/SSS_table');

		$this->data['table_hdr'] = array('paygroup' => array(
										'button' 					=> array(
																				'title' => '<a class="btn btn-success btn-xs btn-outline add-large" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="'.DOMAIN.'administration/application_setup/payroll/SSS_table/add" role="button"><i class="fa fa-plus"></i></a>',
																				'class' => 'text-center',
																				'style' => 'width: 100px; vertical-align: middle'
																			),
										'id' 						=> array(
																				'title' => 'ID',
																				'class' => 'text-center',
																				'style' => 'width: 5%; vertical-align: middle'
																			),
										'salary-range-form' 		=> array(
																				'title' => 'Salary Range (From)',
																				'class' => 'text-right',
																				'style' => 'width: 15%; vertical-align:middle'
																			),
										'salary-range-to' 			=> array(
																				'title' => 'Salary Range (To)',
																				'class' => 'text-right',
																				'style' => 'width: 15%; vertical-align:middle'
																			), 
										'salary-credit'				=> array(
																				'title' => 'Salary Credit',
																				'class' => 'text-cenrightter',
																				'style' => 'width: 15%; vertical-align:middle'
																			),
										'ec'			 			=> array(
																				'title' => 'EC',
																				'class' => 'text-right',
																				'style' => 'width: 15%; vertical-align:middle'
																			),
										'employer-share' 			=> array(
																				'title' => 'Employer Share',
																				'class' => 'text-right',
																				'style' => 'width: 15%; vertical-align:middle'
																			), 
										'employee-share' 		=> array(
																				'title' => 'Employee Share',
																				'class' => 'text-right',
																				'style' => 'width: 15%; vertical-align:middle'	
																			),
										'total-contribution' 			=> array(
																				'title' => 'Total Contibution',
																				'class' => 'text-right',
																				'style' => 'width: 5%; vertical-align:middle'
																			),
			)
		);
		$this->data['hdr_center'] = array(0,1,2,3);
		$this->load_page('administration/application_setup/payroll/SSS_table/index');
	}


	// Adding of User
	public function add(){
		$this->breadcrumb[] 			= array (	'html'	=>	'SSS Table',
													'url'	=>	'administration/application_setup/payroll/SSS_table');
		$this->breadcrumb[]				= array (	'html'	=>	'Add',
													'url'	=>	'');
		
		
		$this->data['type']		 		= 'add';
		$this->data['header']	 		= $this->getData($this->data['type'], '');
		$this->load_page('administration/application_setup/payroll/sss_table/form');

	}

	// Adding of User
	public function update(){
		$this->breadcrumb[] 			= array (	'html'	=>	'SSS Table',
													'url'	=>	'administration/application_setup/payroll/SSS_table');
		$this->breadcrumb[]				= array (	'html'	=>	'Update',
													'url'	=>	'');
		
		
		$this->data['type']		 		= 'update';
		$this->data['id'] 		 	= $_GET['id'];
		$this->data['header']	 		= $this->getData($this->data['type'], $this->data['id']);
		$this->load_page('administration/application_setup/payroll/sss_table/form');

	}

	//Viewing Of Users Data
	public function view(){
		$this->breadcrumb[] 		 = array(	'html'	=> 'Pay Group',
												'url'	=> 'administration/application_setup/payroll/pay_group');
		$this->breadcrumb[]			 = array(	'html'	=> 'View',
												'url'	=> '');
		
		$sss_table_model 			 = $this->load_model('administration/application_setup/payroll/sss_table/sss_table_model');

		$this->data['type']			 = 'view';
		$this->data['id'] 		 	 = $_GET['id'];
		$this->data['header']		 = $this->getData($this->data['type'], $this->data['id']);
		//$this->data['details']		 = $pay_group_model->getAll_User();
		$this->load_page('/administration/application_setup/payroll/sss_table/form');
	}

	// Getting of data from the Form
	public function getData($type, $docno = ''){
		$sss_table_model = $this->load_model('administration/application_setup/payroll/sss_table/sss_table_model');
		if ($type == 'add') {
			$id = $sss_table_model->getLastGroupCode();

			$output = array(
				'ST_ID' 					=> $id ,
				'ST_SalaryRangeFrom' 		=> '',
				'ST_SalaryRangeTo' 			=> '',
				'ST_SalaryCredit'  			=> '',
				'ST_EC' 					=> '',
				'ST_Emprshare' 				=> '',
				'ST_Empeshare' 				=> '',	
			);
		}else{
			$where = array(
				'CAST(ST_ID as varchar)' => $docno
			);

			$header = $sss_table_model->getHeaderByDocNo($this->cols, $this->table, $where, '');
			$output = array(
				'ST_ID' 					=> $header['ST_ID'],
				'ST_SalaryRangeFrom' 		=> numeric($header['ST_SalaryRangeFrom']),
				'ST_SalaryRangeTo' 			=> numeric($header['ST_SalaryRangeTo']),
				'ST_SalaryCredit' 			=> numeric($header['ST_SalaryCredit']),
				'ST_EC' 					=> numeric($header['ST_EC']), 
				'ST_Emprshare' 				=> numeric($header['ST_Emprshare']),
				'ST_Empeshare' 				=> numeric($header['ST_Empeshare']),

			);
		}
		return $output;
	}



	public function data(){

		$sss_table_model = $this->load_model('administration/application_setup/payroll/sss_table/sss_table_model');		
		$table_data = $sss_table_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/application_setup/payroll/sss_table/view?id='.md5($value['ST_ID']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/application_setup/payroll/sss_table/update?id='.md5($value['ST_ID']).'">
					<i class="fa fa-pencil"></i>
					</a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['ST_ID'].'" "><i class="fa fa-trash"></i></a> ';
			//}

			$sub_array[] = $button;
			$sub_array[] = $value['ST_ID'];
			$sub_array[] = number_format($value['ST_SalaryRangeFrom'],2);
			$sub_array[] = number_format($value['ST_SalaryRangeTo'],2);
			$sub_array[] = number_format($value['ST_SalaryCredit'],2);
			$sub_array[] = number_format($value['ST_EC'],2);
			$sub_array[] = number_format($value['ST_Emprshare'],2);
			$sub_array[] = number_format($value['ST_Empeshare'],2);
			$sub_array[] = number_format($value['ST_Emprshare'] + $value['ST_Empeshare'],2);
			
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}


	public function save(){
		if(!$this->input->is_ajax_request()) return;

		if(!empty($_POST)):

			// Get All POST Data
			$data 						= $this->input->post();

			// Load All needed model
			
			$sss_table_model	= $this->load_model('administration/application_setup/payroll/sss_table/sss_table_model');
			// GET CURRENT USER
			$current_user 				= getCurrentUser()['login-user'];


			$output = array(
							'success'	=> true
						,	'message'	=> '');
			try{

				// Start Of Saving - Add Function
				if($this->input->post('todo') == 'add'){
					// GET THE LATEST NO SERIES
				
					$output = $this->error_message(false, true, false, false, $data, $data['ST_ID'], $this->number_series,'','ST','tblSSSTable');
					
				
				if($output['success']){
						// UPDATE NO SERIES
						// $of_series = $noseries_model->getNextAvailableNumber($this->number_series, true, $data['of-location']);	
						
						// $output['doc_id'] = md5($of_series);

						$table = array(
							'header'  	=> $this->header_table,
						);

						$this->db->trans_begin();

						$sss_table_model->on_save_module(false, $data, $table, 'ST', $this->number_series, getDefaultLocation());

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);

				$output = $this->error_message(false, true, false, false, $data, $data['ST_ID'], $this->number_series,'','','tblSSSTable');
				

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,
							
						);

						$this->db->trans_begin();

						$sss_table_model->on_update_module($data, $table, 'ST', $this->number_series, getDefaultLocation());

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['ST_ID']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				echo json_encode($output); //ENDING QUERY
			}

			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;

	}
	
	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}
	

	
}