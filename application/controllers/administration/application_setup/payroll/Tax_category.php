<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tax_category extends MAIN_Controller {

	protected $number_series		=    1753;
	protected $header_table			=	'tblTaxCategory';
	protected $header_doc_no_field	=	'TC_ID';
	protected $header_status_field	=	'TC_Active';
	protected $module_name			=	'Tax Category';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> 'administration');
		$this->breadcrumb[] = array(	'html'	=> 'Application Setup',
										'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=>	'Payroll',
										'url'	=>	'');
		
		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						=   array('TC_ID','TC_ExemtDesc','TC_ExemptAmount','TC_Active');
		//$this->table 						=   'tblHoliday as H';
	}

	// Header Table
	public function index(){
		//$this->user_model->deleteUserRecordLock(getCurrentUser()['login-user']); // Delete User Record Lock
		$this->breadcrumb[]	= array(	'html' 	=> 'Tax Category',
										'url'	=> 'administration/application_setup/payroll/tax_category');

		$this->data['table_hdr'] = array('tax-category-details'	=> array(
													'buttons' 			=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" 
																									data-toggle="tooltip" data-placement="top" 
																									title="Add" type="button" href="'.
																									DOMAIN.'administration/application_setup/payroll/tax_category/add"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 100px; vertical-align: middle' 
																				), 
													'checkbox' 			=>	array(
																					'title' 	=>	'<input type="checkbox" 
																									id="chkSelectAll" 
																									class="icheckbox_square-green"',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10px; vertical-align: middle'
																				),
													'TC_ID' 		=>	array(
																					'title' 	=>	'Tax Category Code',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 25%; vertical-align: middle'
																				),
													'TC_ExemtDesc' 		=>	array(
																					'title' 	=>	'Tax Category Description',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 55%; vertical-align: middle' 
																				),
													'TC_ExemptAmount' 	=>	array(
																					'title' 	=>	'Tax Exemption Amount',
																					'class' 	=>	'text-right',
																					'style' 	=>	'width: 15%; vertical-align: middle' 
																				),
													'TC_Active' 	 	=>	array(
																					'title' 	=>	'Status',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				),
												)

									);
		
		$this->data['hdr_center'] = array(0,1,2,3);

		$this->load_page('/administration/application_setup/payroll/tax_category/index');
	}

	// Adding of User
	public function add(){
		$this->breadcrumb[] 			= array (	'html'	=>	'Tax Category',
													'url'	=>	'administration/application_setup/payroll/tax_category');
		$this->breadcrumb[]				= array (	'html'	=>	'Add',
													'url'	=>	'');
		
		$this->data['type']		 		= 'add';										
		$this->data['header']	 		= $this->getData($this->data['type'], '');
		$this->load_page('administration/application_setup/payroll/tax_category/form');

	}

	// For updating
	public function update(){
		$this->breadcrumb[]	= array(	'html'	=>	'Tax Category',
										'url'	=>	'administration/application_setup/payroll/tax_category');
		$this->breadcrumb[] = array(	'html'	=>	'Update',
										'url'	=>	'');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type']	= 'update';
		$this->data['id'] = $_GET['id'];
		
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);

		// LOAD NEEDED DATA
		$this->load_page('administration/application_setup/payroll/tax_category/form');

		
	}

	// For Viewing of user
	public function view(){
		$this->breadcrumb[] = array(		'html'	=>	'Tax Category',
											'url'	=>	'administration/application_setup/payroll/tax_category');
		$this->breadcrumb[] = array(		'html'	=>	'View',
											'url'	=>	'');

		

		// STRUCTURE FOR VIEW PAGE
		$this->data['type']	= 'view';
		$this->data['id'] = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);

		// LOAD NEEDED DATA
		$this->load_page('/administration/application_setup/payroll/tax_category/form');
	}

	// Data for adding
	public function getData($type, $docno = ''){

		$tax_category_model = $this->load_model('administration/application_setup/payroll/tax_category/tax_category_model');

		if($type == 'add'){
			$output = array(
				'TC_ID' 					=>	'',
				'TC_ExemtDesc' 				=>	'', 
				'TC_ExemptAmount' 			=>	'',
				'TC_Active' 				=>	1, 

			);
		}
		else if($type == 'update' || $type == 'view'){

			$where = array(
				'TC_ID' 	=> $docno 
			);

			$header = $tax_category_model->getHeaderByDocNo($this->cols, $this->header_table, $where);

			$output = array(
				'TC_ID' 				=> $header['TC_ID'],
				'TC_ExemtDesc' 				=> $header['TC_ExemtDesc'], 
				'TC_ExemptAmount' 			=> numeric($header['TC_ExemptAmount'],2),
				'TC_Active' 				=> $header['TC_Active'],
				'functions' 		=> $this->getApprovalButtons($header['TC_ID']
																,getCurrentUser()['login-user']
																,''
																,''
																,$this->header_table
																,$this->header_status_field
																,$this->header_doc_no_field
															),		
			);

			if($output['TC_Active'] == 'Approved'){
				unset($output['functions'][1]);
			}
		}

		return $output;
	}

	/*
		AJAX REQUEST GOES HERE
	*/

	// Displaying od Data to the table
	public function data(){

		$tax_category_model = $this->load_model('/administration/application_setup/payroll/tax_category/tax_category_model');		
		$table_data = $tax_category_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/application_setup/payroll/tax_category/view?id='.md5($value['TC_ID']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/application_setup/payroll/tax_category/update?id='.md5($value['TC_ID']).'">
									<i class="fa fa-pencil"></i>
								</a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['TC_ID'].'"><i class="fa fa-trash"></i></a> ';
			//}

			$sub_array[] = $button;										
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['TC_ID'].'">';
			$sub_array[] = $value['TC_ID'];
			$sub_array[] = $value['TC_ExemtDesc'];
			$sub_array[] = number_format($value['TC_ExemptAmount'], 2);
			$sub_array[] = $value['TC_Active'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;

		if(!empty($_POST)):

			// GET ALL POST data
			$data = $this->input->post();


			//$this->print_r($data);
			//die();

			// LOAD ALL NEEDED MODEL
			$tax_category_model = $this->load_model('administration/application_setup/payroll/tax_category/tax_category_model');

			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];

			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){

					$output = $this->error_message(false, true, false, false, $data, $data['TC_ID'], $this->number_series,'','TC','tblTaxCategory');
					//$this->print_r($data);
			//die();
					
					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table,
						);

						$this->db->trans_begin();

						$tax_category_model->on_save_module(false, $data, $table, 'TC', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}
					/* 
							END OF SAVING - ADD FUNCTION
					*/
			
				// UPDATE HEADER
				}
				else if($this->input->post('todo') == 'update'){
					
					$output = $this->error_message(false, true, false, false, $data, $data['TC_ID'], $this->number_series, '', 'TC', 'tblTaxCategory');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,

						);

						$this->db->trans_begin();

						$tax_category_model->on_update_module($data, $table, 'TC', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['TC_ID']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
				}
				
				echo json_encode($output); //ENDING QUERY
			} 
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}

}