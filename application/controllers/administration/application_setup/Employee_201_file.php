<?php  
defined('BASEPATH') OR exit ('No direct script access allowed');

class Employee_201_file extends MAIN_Controller{
	protected $number_series 		= '';
	protected $header_table	 		= '';
	protected $header_doc_no_field	= '';
	protected $header_status_field 	= '';

	public function __construct(){
		parent:: __construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=> 'Application Setup',
										'url'	=> '');
		$this->data['header_table']				= $this->header_table;
		$this->data['header_doc_no_field']		= $this->header_doc_no_field;
		$this->data['header_status_field']		= $this->header_status_field;
		$this->data['number_series']			= $this->number_series;
	}
	public function index(){
		$this->breadcrumb[] = array(	'html'	=> 'Employee 201 File',
										'url'	=> 'application_setup/employee_201_file');
		$this->load_page('administration/application_setup/employee_201_file/index');
	}

} 
?>