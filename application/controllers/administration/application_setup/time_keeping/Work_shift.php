<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Work_shift extends MAIN_Controller {

	protected $number_series		=	102502;
	protected $header_table			=	'tblShift';
	protected $header_doc_no_field	=	'S_ID';
	protected $header_status_field	=	'S_Active';
	protected $module_name			=	'Shift';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=>	'Administration',
										'url'	=>	'administration');
		$this->breadcrumb[] = array(		'html'	=> 'Application Setup',
											'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=>	'Time Keeping',
										'url'	=>	'');
		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						= array('S_ID', 'S_Name', 'S_StartTime', 'S_EndTime', 'S_Break1Out', 'S_Break1In', 'S_Break2Out', 'S_Break2In', 'S_Break3Out', 'S_Break3In', 'S_Active');
	}

	public function index(){
		$this->breadcrumb[] = array(	'html' 	=>	'Shift',
										'url'	=>	'administration/application_setup/time_keeping/work_shift');

		$this->data['table_hdr'] = array('work_shift_details'	=> array(
													'buttons' 			=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" 
																									data-toggle="tooltip" data-placement="top" 
																									title="Add" type="button" href="'.
																									DOMAIN.'administration/application_setup/time_keeping/work_shift/add"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 100px; vertical-align: middle' 
																				), 
													'checkbox' 			=>	array(
																					'title' 	=>	'<input type="checkbox" id="chkSelectAll" 
																									class="icheckbox_square-green"',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10px; vertical-align: middle'
																				),
													'shift_id' 			=>	array(
																					'title' 	=>	'Shift ID',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'shift_name' 		=>	array(
																					'title' 	=>	'Description',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'time_in' 			=>	array(
																					'title' 	=>	'Time In',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				),
													'time_out' 			=>	array(
																					'title' 	=>	'Time Out',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				),
													'break_1_out' 		=>	array(
																					'title' 	=>	'Break 1 Out',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				),
													'break_1_in' 		=>	array(
																					'title' 	=>	'Break 1 In',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				),
													'break_2_out' 		=>	array(
																					'title' 	=>	'Break 2 Out',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				),
													'break_2_in' 		=>	array(
																					'title' 	=>	'Break 2 In',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				),
													'break_3_out' 		=>	array(
																					'title' 	=>	'Break 3 Out',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				),
													'break_3_in' 		=>	array(
																					'title' 	=>	'Break 3 In',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				),
													'status' 			=>	array(
																					'title' 	=>	'Status',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
												)

									);
		
		$this->data['dtl_center'] = array(0,1,4,5,6,7,8,9,10,11,12);

		$this->load_page('administration/application_setup/time_keeping/work_shift/index');
	}

	public function add(){
		$this->breadcrumb[] = array(		'html'	=>	'Shift',
											'url'	=>	'administration/application_setup/time_keeping/work_shift');
		$this->breadcrumb[] = array(		'html'	=>	'Add',
											'url'	=>	'');
		$this->data['type']   = 'add';
		$this->data['header'] = $this->getData($this->data['type'], '');

		// LOAD NEEDED DATA
		
		$this->load_page('administration/application_setup/time_keeping/work_shift/form');
	}

	public function update(){
		$this->breadcrumb[] = array(		'html'	=>	'Shift',
											'url'	=>	'administration/application_setup/time_keeping/work_shift');
		$this->breadcrumb[] = array(		'html'	=>	'Update',
											'url'	=>	'');
		$this->data['type']   = 'update';
		$this->data['id'] 	  = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);

		// LOAD NEEDED DATA
		
		$this->load_page('administration/application_setup/time_keeping/work_shift/form');
	}

	public function view(){
		$this->breadcrumb[] = array(		'html'	=>	'Shift',
											'url'	=>	'administration/application_setup/time_keeping/work_shift');
		$this->breadcrumb[] = array(		'html'	=>	'View',
											'url'	=>	'');
		$this->data['type']   = 'view';
		$this->data['id'] 	  = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);

		// LOAD NEEDED DATA
		
		$this->load_page('administration/application_setup/time_keeping/work_shift/form');
	}

	public function getData($type, $docno = ''){
		
		$work_shift_model = $this->load_model('/administration/application_setup/time_keeping/work_shift/work_shift_model');

		if($type == 'add'){
			$output = array(
				'S_ID' 					=> '',
				'S_Name' 				=> '',
				'S_StartTime' 			=> '',
				'S_EndTime' 			=> '',
				'S_Break1Out' 			=> '',
				'S_Break1In' 			=> '',
				'S_Break2Out' 			=> '',
				'S_Break2In' 			=> '',
				'S_Break3Out' 			=> '',
				'S_Break3In' 			=> '',
				'S_Active' 				=> 1
			);
		}
		else{

			$where = array(
				'S_ID' 	=> $docno 
			);

			$header = $work_shift_model->getHeaderByDocNo($this->cols, $this->header_table, $where);

			$output = array(
				'S_ID' 					=> $header['S_ID'],
				'S_Name' 				=> $header['S_Name'],
				'S_StartTime' 			=> ($header['S_StartTime'] != '' || $header['S_StartTime'] !== null ? $this->convert12hrFormat($header['S_StartTime']) : ''),
				'S_EndTime' 			=> ($header['S_EndTime'] != '' || $header['S_EndTime'] !== null ? $this->convert12hrFormat($header['S_EndTime']) : ''),
				'S_Break1Out' 			=> ($header['S_Break1Out'] != '' || $header['S_Break1Out'] !== null ? $this->convert12hrFormat($header['S_Break1Out']) : ''),
				'S_Break1In' 			=> ($header['S_Break1In'] != '' || $header['S_Break1In'] !== null ? $this->convert12hrFormat($header['S_Break1In']) : ''),
				'S_Break2Out' 			=> ($header['S_Break2Out'] != '' || $header['S_Break2Out'] !== null ? $this->convert12hrFormat($header['S_Break2Out']) : ''),
				'S_Break2In' 			=> ($header['S_Break2In'] != '' || $header['S_Break2In'] !== null ? $this->convert12hrFormat($header['S_Break2In']) : ''),
				'S_Break3Out' 			=> ($header['S_Break3Out'] != '' || $header['S_Break3Out'] !== null ? $this->convert12hrFormat($header['S_Break3Out']) : ''),
				'S_Break3In' 			=> ($header['S_Break3In'] != '' || $header['S_Break3In'] !== null ? $this->convert12hrFormat($header['S_Break3In']) : ''),
				'S_Active'  			=> $header['S_Active']
			);
		}

		return $output;

	}

	public function data(){

		$work_shift_model = $this->load_model('/administration/application_setup/time_keeping/work_shift/work_shift_model');		
		$table_data = $work_shift_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			// if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/application_setup/time_keeping/work_shift/view?id='.md5($value['S_ID']).'"><i class="fa fa-eye"></i></a> ';
			// }

			// if(in_array('Edit', $access_detail)){
				$button .= '<button class="btn btn-primary btn-xs btn-outline update-header" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['S_ID'].'" data-link="'.DOMAIN.'administration/application_setup/time_keeping/work_shift/update?id='.md5($value['S_ID']).'">
									<i class="fa fa-pencil"></i>
								</button> ';
			// }

			// if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['S_ID'].'"><i class="fa fa-trash"></i></button> ';
			// }

			$sub_array[] = $button;										
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['S_ID'].'">';
			$sub_array[] = $value['S_ID'];
			$sub_array[] = $value['S_Name'];
			$sub_array[] = ($value['S_StartTime'] != '' || $value['S_StartTime'] !== null ? $this->convert12hrFormat($value['S_StartTime']) : '');
			$sub_array[] = ($value['S_EndTime'] != '' || $value['S_EndTime'] !== null ? $this->convert12hrFormat($value['S_EndTime']) : '');
			$sub_array[] = ($value['S_Break1Out'] != '' || $value['S_Break1Out'] !== null ? $this->convert12hrFormat($value['S_Break1Out']) : '');
			$sub_array[] = ($value['S_Break1In'] != '' || $value['S_Break1In'] !== null ? $this->convert12hrFormat($value['S_Break1In']) : '');
			$sub_array[] = ($value['S_Break2Out'] != '' || $value['S_Break2Out'] !== null ? $this->convert12hrFormat($value['S_Break2Out']) : '');
			$sub_array[] = ($value['S_Break2In'] != '' || $value['S_Break2In'] !== null ? $this->convert12hrFormat($value['S_Break2In']) : '');
			$sub_array[] = ($value['S_Break3Out'] != '' || $value['S_Break3Out'] !== null ? $this->convert12hrFormat($value['S_Break3Out']) : '');
			$sub_array[] = ($value['S_Break3In'] != '' || $value['S_Break3In'] !== null ? $this->convert12hrFormat($value['S_Break3In']) : '');
			$sub_array[] = $value['S_Active'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$work_shift_model = $this->load_model('/administration/application_setup/time_keeping/work_shift/work_shift_model');

			// Adjust time format for database saving 
			$data['S_StartTime'] = ($data['S_StartTime'] != '' ? $this->convert24hrFormat($data['S_StartTime']) : NULL);
			$data['S_EndTime'] = ($data['S_EndTime'] != '' ? $this->convert24hrFormat($data['S_Break1Out']) : NULL);
			$data['S_Break1Out'] = ($data['S_Break1Out'] != '' ? $this->convert24hrFormat($data['S_Break1Out']) : NULL);
			$data['S_Break1In'] = ($data['S_Break1In'] != '' ? $this->convert24hrFormat($data['S_Break1In']) : NULL);
			$data['S_Break2Out'] = ($data['S_Break2Out'] != '' ? $this->convert24hrFormat($data['S_Break2Out']) : NULL);
			$data['S_Break2In'] = ($data['S_Break2In'] != '' ? $this->convert24hrFormat($data['S_Break2In']) : NULL);
			$data['S_Break3Out'] = ($data['S_Break3Out'] != '' ? $this->convert24hrFormat($data['S_Break3Out']) : NULL);
			$data['S_Break3In'] = ($data['S_Break3In'] != '' ? $this->convert24hrFormat($data['S_Break3In']) : NULL);

			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){

					$output = $this->error_message(false, true, false, false, $data, $data['S_ID'], $this->number_series, '', 'S', 'tblShift');

					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table, 
						);

						$this->db->trans_begin();

						$work_shift_model->on_save_module(false, $data, $table, 'S', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					
					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);

					$output = $this->error_message(false, true, false, false, $data, $data['S_ID'], $this->number_series, '', 'S', 'tblShift');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table, 
						);

						$this->db->trans_begin();

						$work_shift_model->on_update_module($data, $table, 'S', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['S_ID']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
		
	}

	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}

	private function convert24hrFormat($time){
		if(strlen($time) == 7){
			$hours = intval(substr($time,0,2));
			$minutes = intval(substr($time,3,2));
			$AMPM = substr($time,5,2);
		}
		else{
			$hours = intval(substr($time,0,1));
			$minutes = intval(substr($time,2,2));
			$AMPM = substr($time,4,2);
		}

		if($AMPM == "PM" && $hours<12){$hours = $hours+12;}
		if($AMPM == "AM" && $hours==12){$hours = $hours-12;}

		$sHours = $hours;
		$sMinutes = $minutes;
		if($hours<10) {$sHours = "0".$sHours;}
		if($minutes<10) {$sMinutes = "0".$sMinutes;}

		return $sHours.$sMinutes;
	}

	private function convert12hrFormat($time){
		$hours = substr($time,0,2);
		$minutes = substr($time,2,2);
		$AMPM = '';

		if($hours == "00"){
			$hours = "12";
			$AMPM = "AM";
		}

		if($hours != "00" && intval($hours) < 12){
			$AMPM = "AM";
		}

		if(intval($hours) >= 12){
			if(intval($hours) != 12){
				$hours = intval($hours) - 12;
			}
			if(intval($hours) < 10){
				$hours = "0".$hours;
			}
			$AMPM = "PM";
		}

		return $hours.":".$minutes.$AMPM;
	}

}