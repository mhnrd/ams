<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');
class Time_capture_devices extends MAIN_Controller{
	protected $number_series 		= '';
	protected $header_table 		= '';
	protected $header_doc_no_field  = '';
	protected $header_status_field  = '';

	public function __construct(){
		parent:: __construct();
		$this->is_secure 	= true;
		$this->breadcrumb[] = array(	'html'	=>	'Administration',
										'url'	=>	'');
		$this->breadcrumb[] = array(	'html'	=>	'Application Setup',
										'url'	=>	'');
		$this->breadcrumb[] = array(	'html'	=>	'Time Keeping',
										'url'	=>	'');
		$this->data['header_table']			= $this->header_table;
		$this->data['header_doc_no_field']  = $this->header_doc_no_field;
		$this->data['header_status_field']	= $this->header_status_field;
		$this->data['number_series']		= $this->number_series; 
	}
	public function index(){
		$this->breadcrumb[] = array(	'html'	=> 'Time Capture Devices',
										'url'	=> 'administration/application_setup/time_keeping/time_capture_devices');
		$this->data['table_hdr'] = array('time-capture-devices' => array(
															'button'			=> array(
																							'title' => '<a class="btn btn-success btn-xs btn-outline add-large" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="'.DOMAIN.' administration/application_setup/time_keeping/time_capture_devices/add" role="button"><i class="fa fa-plus"></i></a>',
																							'class' => 'text-center',
																							'style' => 'width: 5%; vertical-align:middle'
																						),
															'device-id' 		=> array(
																							'title' => 'Device ID',
																							'class' => 'text-center',
																							'style' => 'width: 5%; vertical-align:middle',
																						),	
															'device-name' 		=> array(
																							'title' => 'Device Name',
																							'class' => 'text-center',
																							'style' => 'width: 5%; vertical-align:middle'
																						),
															'input-directory' 	=> array(
																							'title' => 'Input Directory',
																							'class' => 'text-center',
																							'style' => 'width: 5%; vertical-align:middle'
																						),
															'process-directory' => array(
																							'title' => 'Process Directory',
																							'class' => 'text-center',
																							'style' => 'width: 5%; vertical-align:middle'
																						),
															'output-directory' 	=> array(
																							'title' => 'Output Directory',
																							'class' => 'text-center',
																							'style' => 'width: 5%; vertical-align:middle'
																						),
															'active' 			=> array(
																							'title' => 'Active',
																							'class' => 'text-center',
																							'style' => 'width: 5%; vertical-align:middle'
																						)
			)
		);
		$this->data['hdr_center']	= array(0,1,2,3,4);
		$this->load_page('/administration/application_setup/time_keeping/time_capture_devices/index');
	}
}
?>