<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Holiday extends MAIN_Controller {

	protected $number_series		=    1701;
	protected $header_table			=	'tblHoliday';
	protected $header_doc_no_field	=	'H_ID';
	protected $header_status_field	=	'H_Active';
	protected $module_name			=	'Holiday';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> 'administration');
		$this->breadcrumb[] = array(	'html'	=> 'Application Setup',
										'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=>	'Time Keeping',
										'url'	=>	'');
		
		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						=   array('H_ID','H_Desc','H_Date','H_Type','H_HolidayStart','H_HolidayEnd','H_Policy','H_Active');
		$this->cols_detail 					=   array('LH_H_ID','LH_FK_Location_id','SP_StoreName','LH_Selected');
	
	}
	public function index(){
		$this->breadcrumb[]	= array(	'html' 	=> 'Holiday',
										'url'	=> 'administration/application_setup/time_keeping/holiday');

		$this->data['table_hdr'] = array('holiday'	=> array(
													'buttons' 			=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" 
																									data-toggle="tooltip" data-placement="top" 
																									title="Add" type="button" href="'.
																									DOMAIN.'administration/application_setup/time_keeping/holiday/add"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				), 
													'checkbox' 			=>	array(
																					'title' 	=>	'<input type="checkbox" 
																									id="chkSelectAll" 
																									class="icheckbox_square-green">',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'holiday_id' 		=>	array(
																					'title' 	=>	'Holiday ID',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'holiday_name' 		=>	array(
																					'title' 	=>	'Holiday Name',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 25%; vertical-align: middle' 
																				),
													'type' 				=>	array(
																					'title' 	=>	'Type',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'holiday_start' 	=>	array(
																					'title' 	=>	'Holiday Start',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'holiday_end' 		=>	array(
																					'title' 	=>	'Holiday End',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'payroll_policy' 	=>	array(
																					'title' 	=>	'Payroll Policy',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'status' 			=>	array(
																					'title' 	=>	'Status',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
												)

									);
		
		$holiday_model = $this->load_model('/administration/application_setup/time_keeping/holiday/holiday_model');
		$this->data['hdr_center'] = array(0,1,4,6,7);
		$this->load_page('/administration/application_setup/time_keeping/holiday/index');
	}

	public function generate_detail_table($type){

		$dtl_data = array('tbl-detail' => array(
												'location' 				=>	array(
																					'title' 	=> 'Location',
																					'class' 	=> 'text-center',
																					'style'  	=> 'width: 20%; vertical-align: middle' 
																				),
												'description' 			=>	array(
																					'title'  	=> 'Description',
																					'class'  	=> 'text-center',
																					'style'  	=> 'width: 70%; vertical-align: middle' 
																				),
												'checkbox' 				=>	array(
																					'title'  	=> ($type != 'view' ? '<input type="checkbox" 
																									id="chkSelectAll">' : ''),
																					'class'  	=> 'text-center',
																					'style'  	=> 'width: 10%; vertical-align: middle' 
																				),
																)

										);

		return $dtl_data;
	}

	public function add(){
		$this->breadcrumb[] = array( 	'html'	=>	'Holiday',
									 	'url'	=> 	'administration/application_setup/time_keeping/holiday');
		$this->breadcrumb[] = array( 	'html'	=>	'Add',
									 	'url'	=> 	'');

		$holiday_detail_model = $this->load_model('administration/application_setup/time_keeping/holiday/holiday_detail_model');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type'] = 'add';
		$this->data['doc_date'] = Date('m/d/y');
		$this->data['header'] = $this->getData($this->data['type'], '');
		$this->data['detail_table'] = $this->generate_detail_table($this->data['type']);
		$this->data['details'] = $holiday_detail_model->getActiveLocation();
		// LOAD NEEDED DATA
		$this->load_page('/administration/application_setup/time_keeping/holiday/form');
	}

	public function update(){
		$this->breadcrumb[]	= array(	'html'	=>	'Holiday',
										'url'	=>	'administration/application_setup/time_keeping/holiday');
		$this->breadcrumb[] = array(	'html'	=>	'Update',
										'url'	=>	'');

		$holiday_detail_model = $this->load_model('administration/application_setup/time_keeping/holiday/holiday_detail_model');
		// STRUCTURE FOR VIEW PAGE
		$this->data['type']	= 'update';
		$this->data['id'] = $_GET['id'];
		$this->data['detail_table'] = $this->generate_detail_table($this->data['type']);
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);

		$where_docno = array(
			'LH_H_ID'  => $this->data['id']
		);

		$join_table = array(
			array(
				'tbl_name'  => 'tblStoreProfile',
				'tbl_on'  	=> 'LH_FK_Location_id = SP_ID',
				'tbl_join'  => 'left'
			)
		);

		$this->data['details'] = $holiday_detail_model->getDetails($this->cols_detail, 'tblLocationHoliday', $where_docno, array(), $join_table);

		// LOAD NEEDED DATA
		$this->load_page('administration/application_setup/time_keeping/holiday/form');

		
	}

	public function view(){
		$this->breadcrumb[] = array(		'html'	=>	'Holiday',
											'url'	=>	'administration/application_setup/time_keeping/holiday');
		$this->breadcrumb[] = array(		'html'	=>	'View',
											'url'	=>	'');
		// STRUCTURE FOR VIEW PAGE
		$this->data['type']	= 'view';
		$this->data['id'] = $_GET['id'];
		$this->data['detail_table'] = $this->generate_detail_table($this->data['type']);
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);
		$this->data['details'] = array();

		// LOAD NEEDED DATA
		$this->load_page('/administration/application_setup/time_keeping/holiday/form');
	}

	public function getData($type, $docno = ''){

		$holiday_model = $this->load_model('administration/application_setup/time_keeping/holiday/holiday_model');

		if($type == 'add'){
			$output = array(
				'H_ID' 				=>	'',
				'H_Desc' 			=>	'', 
				'H_Date' 			=>	date('m/d/Y'),
				'H_Type' 			=>	'',
				'H_HolidayStart' 	=>	'',
				'H_HolidayEnd' 	 	=>	'',
				'H_Policy' 			=> 	'',
			);
		}
		else if($type == 'update' || $type == 'view'){

			$where = array(
				'H_ID' 	=> $docno 
			);

			$header = $holiday_model->getHeaderByDocNo($this->cols, $this->header_table, $where);

			$output = array(
				'H_ID' 				=> $header['H_ID'],
				'H_Desc' 			=> $header['H_Desc'], 
				'H_Date' 			=> date_format(date_create($header['H_Date']),'m/d/Y'),
				'H_Type' 			=> $header['H_Type'],
				'H_HolidayStart' 	=> date_format(date_create($header['H_HolidayStart']),'m/d/Y'),
				'H_HolidayEnd' 		=> date_format(date_create($header['H_HolidayEnd']),'m/d/Y'),
				'H_Policy' 			=> $header['H_Policy']	
			);
		}

		return $output;
	}

	/*
		AJAX REQUEST GOES HERE
	*/

	public function data(){

		$holiday_model = $this->load_model('/administration/application_setup/time_keeping/holiday/holiday_model');		
		$table_data = $holiday_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			// if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/application_setup/time_keeping/holiday/view?id='.md5($value['H_ID']).'"><i class="fa fa-eye"></i></a> ';
			// }

			// if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/application_setup/time_keeping/holiday/update?id='.md5($value['H_ID']).'">
									<i class="fa fa-pencil"></i>
								</a> ';
			// }

			// if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['H_ID'].'"><i class="fa fa-trash"></i></button> ';
			// }

			$sub_array[] = $button;										
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['H_ID'].'">';
			$sub_array[] = $value['H_ID'];
			$sub_array[] = $value['H_Desc'];
			$sub_array[] = $value['H_Type'];
			$sub_array[] = date_format(date_create($value['H_HolidayStart']), 'm/d/Y');
			$sub_array[] = date_format(date_create($value['H_HolidayEnd']), 'm/d/Y');
			$sub_array[] = ($value['H_Policy'] == 'Not absent before' ? 'Not absent before' : ($value['H_Policy'] == 'Not absent after' ? 'Not absent after' : ($value['H_Policy'] == 'Both' ? 'Both' : ($value['H_Policy'] == 'None' ? 'None' : ''))));
			$sub_array[] = $value['H_Active'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_detail(){

		$doc_no = $this->input->get('id');

		$holiday_detail_model = $this->load_model('/administration/application_setup/time_keeping/holiday/holiday_detail_model');		
		$table_data = $holiday_detail_model->table_data($doc_no);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();

			$sub_array[] = $value['LH_FK_Location_id'];
			$sub_array[] = $value['SP_StoreName'];
			$sub_array[] = '<input type="checkbox" name="chkLocation[]" disabled '.($value['LH_Selected'] ? 'checked' : '').'>';

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;

		if(!empty($_POST)):

			// GET ALL POST data
			$data = $this->input->post();

			foreach($data['details'] as $key => $value){
				$data['details'][$key]['LH_H_ID'] = $data['H_ID'];
			}

			// LOAD ALL NEEDED MODEL
			$holiday_model = $this->load_model('administration/application_setup/time_keeping/holiday/holiday_model');

			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];

			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){

					$output = $this->error_message(false, true, true, false, $data, $data['H_ID'], $this->number_series, '', 'H', 'tblHoliday');

					if($output['success']){
						$not_selected = 0;
						
						foreach($data['details'] as $key => $value){
							if($value['LH_Selected'] == 'false'){
								$not_selected++;
							}	
						}

						if($not_selected == count($data['details'])){
							$output['success'] = false;
							$output['message'] = 'No details to be saved , Error , error';

							throw new Exception($output['message']);
						}
					}
					
					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table,
							'detail'  	=> 'tblLocationHoliday',
						);

						$this->db->trans_begin();

						$holiday_model->on_save_module(false, $data, $table, 'H', $this->number_series, '', 'LH');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}
					/* 
							END OF SAVING - ADD FUNCTION
					*/
			
				// UPDATE HEADER
				}
				else if($this->input->post('todo') == 'update'){
					
					$output = $this->error_message(false, true, true, false, $data, $data['H_ID'], $this->number_series, '', 'H', 'tblHoliday');

					if($output['success']){
						$not_selected = 0;
						
						foreach($data['details'] as $key => $value){
							if($value['LH_Selected'] == 'false'){
								$not_selected++;
							}	
						}

						if($not_selected == count($data['details'])){
							$output['success'] = false;
							$output['message'] = 'No details to be saved , Error , error';

							throw new Exception($output['message']);
						}
					}

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,
							'detail'  	=> 'tblLocationHoliday',
						);

						foreach($data['details'] as $key => $value){
							$data['details'][$key]['LH_LineNo'] = $key + 1;
						}

						$this->db->trans_begin();

						$holiday_model->on_update_module($data, $table, 'H', $this->number_series, '', 'LH');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['H_ID']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
				}
				
				echo json_encode($output); //ENDING QUERY
			} 
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}

}