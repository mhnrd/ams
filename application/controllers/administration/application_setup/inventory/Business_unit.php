<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Business_unit extends MAIN_Controller{

	protected $number_series 		= 102310;
	protected $header_table			= 'tblBusinessUnit';
	protected $header_doc_no_field	= 'BU_ID';
	protected $header_status_field	= 'BU_Status';
	protected $module_name			= 'Business Unit';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=> 'Application Setup',
										'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=> 'Inventory',
										'url'	=> '');

		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->cols 						=   array('BU_ID', 'BU_Description', 'BU_Status');
		$this->table 						=   'tblBusinessUnit';
	}

	public function index(){
		$this->breadcrumb[]	= array(	'html' 	=> 'Business Unit',
										'url'	=> 'administration/application_setup/inventory/business_unit');

		$this->data['table_hdr']	= array('business_unit' => array(
												'buttons' 				=> 	array(
																					'title'     => '<a class="btn btn-success btn-xs 
																									btn-outline" 
																									data-toggle="tooltip" data-placement="top" 
																									title="Add" type="button" href="'.
																									DOMAIN.'administration/application_setup/inventory/business_unit/add"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
												'bu_ID' 				=>	array(
																					'title' 	=>	'BU Code',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 25%; vertical-align: middle'
																				),
												'description' 			=>	array(
																					'title' 	=>	'Description',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
												'status' 				=>	array(
																					'title' 	=>	'Status',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 25%; vertical-align: middle'
																				)								
											)
									);

		$this->data['hdr_center'] = array(0,1,3);

		
		// LOAD NEEDED DATA

		// STRUCTURE FOR VIEW PAGE
		$this->data['doc_date'] 		= Date('m/d/y');

		$this->load_page('/administration/application_setup/inventory/business_unit/index');
	}

	public function add(){
		$this->breadcrumb[] = array(		'html'	=>	'Business Unit',
											'url'	=>	'administration/application_setup/inventory/business_unit');
		$this->breadcrumb[] = array(		'html'	=>	'Add',
											'url'	=>	'');
		// LOAD NEEDED DATA

		$this->data['type']   = 'add';
		$this->data['header'] = $this->getData($this->data['type'], '');

		
		$this->load_page('administration/application_setup/inventory/business_unit/form');
	}

	public function update(){
		$this->breadcrumb[] = array(		'html'	=>	'Business Unit',
											'url'	=>	'administration/application_setup/inventory/business_unit');
		$this->breadcrumb[] = array(		'html'	=>	'Update',
											'url'	=>	'');
		// LOAD NEEDED DATA

		$this->data['type']   = 'update';
		$this->data['doc_no'] = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['doc_no']);

		
		$this->load_page('administration/application_setup/inventory/business_unit/form');
	}

	public function view(){
		$this->breadcrumb[] = array(		'html'	=>	'Business Unit',
											'url'	=>	'administration/application_setup/inventory/business_unit');
		$this->breadcrumb[] = array(		'html'	=>	'View',
											'url'	=>	'');
		// LOAD NEEDED DATA

		$this->data['type']   = 'view';
		$this->data['doc_no'] = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['doc_no']);

		
		$this->load_page('administration/application_setup/inventory/business_unit/form');
	}

	public function getData($type, $docno = ''){
		
		$pcbu_model = $this->load_model('/administration/application_setup/inventory/business_unit/business_unit_model');

		if($type == 'add'){

			$output = array(
				'BU_ID' 			=> '',
				'BU_Description' 	=> ''	
			);
		}
		else{

			$where = array(
				'BU_ID' 	=> $docno 
			);

			$header = $pcbu_model->getHeaderByDocNo($this->cols, $this->header_table, $where);

			$output = array(
				'BU_ID' 			=> $header['BU_ID'],
				'BU_Description' 	=> $header['BU_Description']		
			);
		}

		return $output;

	}

	public function data(){

		$bu_model = $this->load_model('/administration/application_setup/inventory/business_unit/business_unit_model');		
		$table_data = $bu_model->table_data();
		// $access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			// if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/application_setup/inventory/business_unit/view?id='.md5($value['BU_ID']).'"><i class="fa fa-eye"></i></a> ';
			// }

			// if(in_array('Edit', $access_detail)){
				$button .= '<button class="btn btn-primary btn-xs btn-outline update-header" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['BU_ID'].'" data-link="'.DOMAIN.'administration/application_setup/inventory/business_unit/update?id='.md5($value['BU_ID']).'">
									<i class="fa fa-pencil"></i>
								</button> ';
			// }

			// if(in_array('Delete', $access_detail)){
				// $button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['PCBU_DocNo'].'"><i class="fa fa-trash"></i></button> ';
			// }

			$sub_array[] = $button;
			$sub_array[] = $value['BU_ID'];
			$sub_array[] = $value['BU_Description'];
			$sub_array[] = $value['BU_Status'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;
		
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();

			// Load All needed Model
			$bu_model = $this->load_model('administration/application_setup/inventory/business_unit/business_unit_model');

			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];			


			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){
					// GET THE LATEST NO SERIES
					// $of_series = $noseries_model->getNextAvailableNumber($this->number_series, false, $data['OF_Location']);

					$output = $this->error_message(false, true, false, false, $data, $data['BU_ID'], $this->number_series, '', 'BU', $this->header_table);

					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table 
						);

						$this->db->trans_begin();

						$bu_model->on_save_module(false, $data, $table, 'BU', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					
					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					$output = $this->error_message(false, true, false, false, $data, $data['BU_ID'], $this->number_series, '', 'BU', $this->header_table);

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,
						);

						$this->db->trans_begin();

						$bu_model->on_update_module($data, $table, 'BU', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
		
	}
}