<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends MAIN_Controller{

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(		'html'	=> 'Administration',
											'url'	=> '');
	}

	public function index(){

		$this->load_page($this->uri->segment(1).'/index');

	}
	
	
}