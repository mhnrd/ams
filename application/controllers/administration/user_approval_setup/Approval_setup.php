<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Approval_setup extends MAIN_Controller {

	protected $number_series		=    101005;
	protected $header_table			=	'tblApprovalSetup';
	protected $header_doc_no_field	=	'AS_FK_NS_id';
	protected $module_name			=	'Approval Setup';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=> 'User Approval Setup',
										'url'	=> '');
		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						=   array('AS_FK_NS_id','AS_Sequence','AS_FK_Position_id','AS_Amount','AS_Unlimited','AS_Required','NS_Id');
		//$this->table 						=   'tblApprovalSetup as AS';
	}

	public function index(){
		$this->breadcrumb[]	= array(	'html' 	=> 'Approval Setup',
										'url'	=> 'administration/user_approval_setup/approval_setup');


		$this->data['table_hdr'] = array('approval_setup'	=> array(
													'number_series_id' 		=>	array(
																					'title' 	=>	'Number Series ID',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align:
																					 				 middle; height:25px'
																				),
													'module' 		=>	array(
																					'title' 	=>	'Module',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align:
																					 				 middle; height:25px' 
																				),
												)

									);

		$this->data['table_detail'] = array('tbl-detail' => array(
												'buttons' 			=> 	array(
																					'title' 	=>	'<a class="invisible btn btn-success
																									btn-xs btn-outline 
																									" id="btnAddEntry"
																									data-toggle="tooltip"
																									data-placement="top" 
																									title="Add" type="button">
																									<i class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				),
												'sequence' 				=>	array(
																					'title' 	=> 'Sequence',
																					'class' 	=> 'text-center',
																					'style'  	=> 'width: 10%; vertical-align: middle' 
																				),
												'position' 			=>	array(
																					'title'  	=> 'Position',
																					'class'  	=> 'text-left',
																					'style'  	=> 'width: 10%; vertical-align: middle' 
																				),
												'amount' 			=>	array(
																					'title'  	=> 'Amount',
																					'class'  	=> 'text-right',
																					'style'  	=> 'width: 10%; vertical-align: middle' 
																				),
												'unlimited' 			=>	array(
																					'title'  	=> 'Unlimited',
																					'class'  	=> 'text-center',
																					'style'  	=> 'width: 10%; vertical-align: middle' 
																				),
												'required' 			=>	array(
																					'title'  	=> 'Required',
																					'class'  	=> 'text-center',
																					'style'  	=> 'width: 10%; vertical-align: middle' 
																				),
																)

										);

		

		$this->data['hdr_center'] = array(0,1);
		$this->data['dtl_center'] = array(0);
		$this->load_page('/administration/user_approval_setup/approval_setup/index');
	}

	public function generate_detail_table(){
		$dtl_data[] = '';

		
		$dtl_data['dtl_center'] = array(0,1,3,4);

		return $dtl_data;
	}


	public function getData($type, $docno = ''){

		$approval_setup_model = $this->load_model('administration/user_approval_setup/approval_setup/approval_setup_model');

		if($type == 'index'){

			$id = $approval_setup_model->getLastGroupCode();

			$output = array(
				
				'AS_FK_User_id' 	=>	'', 
				'AS_FK_Position_id' => 	'',
				'P_Position' 		=>  '', 
				'AS_Amount' 		=>	'',
				'AS_Unlimited' 		=>  '', 
				'AS_Required'  		=>  '', 
				'P_Type'		    =>  '', 

			);
		}
		else if($type == 'update' || $type == 'view'){

			$where = array(
				'AS_FK_NS_id' 		=> $docno, 
			);

			$join_table = array(
				array(
					'tbl_name' => 'tblPosition',
					'tbl_on'   => 'AS_FK_Position_id = P_ID',
					'tbl_join' => 'left'
				),
			);

			$join_table = array(
				array(
					'tbl_name' => 'tblNoSeries',
					'tbl_on'   => 'AS_FK_NS_id = NS_Id',
					'tbl_join' => 'left'
				),
			);

			$header = $approval_setup_model->getHeaderByDocNo($this->cols, $this->header_table, $where, $join_table);

			$output = array(
				'AS_FK_NS_id' 			=> $header['AS_FK_NS_id'],
				'NS_Id' 				=> $header['NS_Id'], 
				//'AS_FK_User_id' 		=> $header['AS_FK_User_id'], 
				'AS_FK_Position_id' 	=> $header['AS_FK_Position_id'],
				'P_Position' 			=> $header['P_Position'], 
				'AS_Amount' 			=> $header['AS_Amount'],
				'AS_Unlimited' 			=> $header['AS_Unlimited'],
				'AS_Required' 			=> $header['AS_Required'],
				//'NS_Description' 		=> $header['NS_Description'],			
				'functions' 			=> $this->getApprovalButtons($header['AS_FK_NS_id']
																,$header['AS_FK_Position_id']
																,getCurrentUser()['login-user']
																,$header['AS_Amount']
																,$this->header_table
																,''
																,$this->header_doc_no_field
															),		
			);
		}

		return $output;
	}

	/*
		AJAX REQUEST GOES HERE
	*/

	public function data(){

		$get_data = $this->input->get();
		$ap_detail_model = $this->load_model('administration/user_approval_setup/approval_setup/approval_setup_detail_model');		
		$table_data = $ap_detail_model->table_data($get_data);
		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');
		
			$sub_array[] = '<span class="employee_data" data-employee-id="'.md5($value['NS_Id']).'" data-employee="'.$value['NS_Id'].'">'.$value['NS_Id'].'</span>';
			$sub_array[] = '<span class="employee_data" data-employee-name="'.md5($value['NS_Description']).'" data-employee="'.$value['NS_Description'].'">'.$value['NS_Description'].'</span>';
			
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_detail(){
		
		$employee_id = $this->input->get('id');
		$ap_detail_model = $this->load_model('administration/user_approval_setup/approval_setup/approval_setup_detail_model');		
		$table_data = $ap_detail_model->table_data_detail($employee_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');
			$button .= '<button class="btn btn-primary btn-xs btn-outline button-edit" data-toggle="tooltip" data-placement="bottom" title="Update" data-employee-id="'.$value['AS_FK_NS_id'].'" data-allow-id="'.$value['AS_Sequence'].'" data-record="'.$json_data.'"><i class="fa fa-pencil"></i></button> ';

				$button .= '<button class="btn btn-danger btn-xs btn-outline button-delete" data-toggle="tooltip" data-placement="bottom" title="Delete" data-employee-id="'.$value['AS_FK_NS_id'].'" data-id="'.$value['AS_Sequence'].'" data-record="'.$json_data.'"><i class="fa fa-trash"></i></button>';
			
			$sub_array[] = $button;
			$sub_array[] = $value['AS_Sequence'];
			$sub_array[] = $value['P_Position'];
			$sub_array[] = number_format($value['AS_Amount'], 2);
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" disabled '.($value['AS_Unlimited'] ? 'checked' : '').'>';
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" disabled '.($value['AS_Required'] ? 'checked' : '').'>';
			
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	// AJAX REQUEST GOES HERE

	public function getApprovalSetup(){

		$ns_id = $this->input->post('ns-id');

		$ns_id_model = $this->load_model('administration/user_approval_setup/approval_setup/approval_setup_detail_model');

		$output = $ns_id_model->generateApprovalSetup($ns_id);

		echo json_encode($output);

	}

	public function getCurrentPtype(){

		$p_type = $this->input->post('p-type');

		$position_id_list 	= $this->load_model('administration/user_approval_setup/approval_setup/approval_setup_detail_model');

		$output = $position_id_list->generatePtype($p_type);

		echo json_encode($output);

	}

	public function getCurrentSequence(){

		$sequence = $this->input->post('sequence-id');

		$sequence_model 	= $this->load_model('administration/user_approval_setup/approval_setup/approval_setup_detail_model');

		$output = $sequence_model->generateSequence($sequence);

		echo json_encode($output);

	}


	public function save(){
		if(!empty($_POST)):

			// Get all POST Data
			$data = $this->input->post();
			$ap_detail_model = $this->load_model('/administration/user_approval_setup/approval_setup/approval_setup_detail_model');
			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				

				if($output['success']){								 

					$this->db->trans_begin();


					$history_LineNo = $this->db->select('ISNULL(AS_Sequence,1) AS AS_Sequence', false)
											 ->where('AS_FK_NS_id')
											 ->order_by('AS_Sequence','DESC')
											 ->get('tblApprovalSetup')
											 ->row_array()['AS_Sequence'] + 1;
					
					$array_insert = array(
								'AS_FK_Position_id' 			=> $data['AS_FK_NS_id'], 
								'AS_Sequence' 			=> $history_LineNo, 
								'AS_FK_Position_id' 	=> $data['AS_FK_Position_id'],
								'AS_Unlimited' 			=> $data['AS_Unlimited'], 
								'AS_Amount' 			=> $data['AS_Amount'],
								'AS_Required' 			=>  $data['AS_Required'], 
					);											 

					$this->db->insert('tblApprovalSetup',$array_insert);

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function delete(){

		$data = $this->input->post();

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		$this->db->trans_begin();

		$this->db->where('AS_FK_NS_id', $data['ns-id'])
				 ->where('AS_FK_Position_id', $data['p-id'])
				 ->delete('tblApprovalSetup');

		if($this->db->trans_status() !== False){
			$this->db->trans_commit();
		}
		else{
			$this->db->trans_rollback();
		}

		echo json_encode($output);

	}

	public function getAllPositions(){

		$filter = $this->input->get();

		$position_id_model = $this->load_model('administration/user_approval_setup/approval_setup/approval_setup_detail_model');

		if(!isset($filter['filter-search'])){
			$filter['filter-search'] = '';
		}

		if(!isset($filter['filter-approvers'])){
			$filter['filter-approvers'] = array();
		}

		$output = $position_id_model->searchPositions($filter['filter-type'], $filter['filter-search'], $filter['filter-approvers']);

		echo json_encode($output); 

	}

	public function getNoseriesData(){
		$employee_id = $this->input->get('id');
		$ap_detail_model = $this->load_model('/administration/user_approval_setup/approval_setup_detail_model');		
		$output = $ap_detail_model->getAllEmployeeData($employee_id);
		echo json_encode($output);
	}


	// save or update siblings record
	public function saveTimeEntries(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$ap_detail_model = $this->load_model('/administration/user_approval_setup/approval_setup/approval_setup_detail_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{				

				if($output['success']){
										
					$this->db->trans_begin();

					$ap_detail_model->saveTimeEntryRecord($data);					

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}


}