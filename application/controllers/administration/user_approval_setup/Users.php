<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Encrypt\SGEncryption;
class Users extends MAIN_Controller {

	protected $number_series		=    101001;
	protected $header_table			=	'tblUser';
	protected $header_doc_no_field	=	'U_ID';
	protected $header_status_field	=	'U_Status';
	protected $module_name			=	'Users';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=> 'User Approval Setup',
										'url'	=> '');
		
		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						=   array('U_ID','U_UserEmailAddress','U_Username','U_Password','U_FK_Position_id', 'U_ParentUserID', 'U_AssociatedEmployeeId');
		$this->table 						=   'tblUser';
	}
	public function index(){
		//$this->user_model->deleteUserRecordLock(getCurrentUser()['login-user']); // Delete User Record Lock
		$this->breadcrumb[]	= array(	'html' 	=> 'Users',
										'url'	=> 'administration/user_approval_setup/users');

		$this->data['table_hdr'] = array('users'	=> array(
													'buttons' 			=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" 
																									data-toggle="tooltip" data-placement="top" 
																									title="Add" type="button" href="'.
																									DOMAIN.'administration/user_approval_setup/users/add"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 100px; vertical-align: middle' 
																				), 
													'checkbox' 			=>	array(
																					'title' 	=>	'<input type="checkbox" 
																									id="chkSelectAll" 
																									class="icheckbox_square-green"',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10px; vertical-align: middle'
																				),
													'U_ID' 				=>	array(
																					'title' 	=>	'User ID',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'U_Username' 		=>	array(
																					'title' 	=>	'Username',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 35%; vertical-align: middle' 
																				),
													'Position' 			=>	array(
																					'title' 	=>	'Position',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 35%; vertical-align: middle' 
																				),
													'Status' 			=>	array(
																					'title' 	=>	'Status',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
												)

									);
		
		$this->data['hdr_center'] = array(0,1,2,3,4,5,7);
		$this->load_page('/administration/user_approval_setup/users/index');
	}

	public function add(){
		$this->breadcrumb[] = array( 	'html'	=>	'Users',
									 	'url'	=> 	'administration/user_approval_setup/users');
		$this->breadcrumb[] = array( 	'html'	=>	'Add',
									 	'url'	=> 	'');

		$users_model = $this->load_model('administration/user_approval_setup/users/users_model');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type'] = 'add';
		$this->data['doc_date'] = Date('m/d/y');
		$this->data['header'] 	= $this->getData($this->data['type'], '');
		$this->data['position'] = $users_model->getAllActivePosition();
		$this->data['role'] 	= $users_model->getAllUserRoles();
		$this->data['location_list'] = loadAllLocations();
		$this->data['user_list'] = $users_model->loadAllUsers();
		$this->data['table_detail'] = array('tbl-detail'	=> array(
													'checkbox' 			=>	array(
																					'title' 	=>	'<input type="checkbox" 
																									id="chkSelectAll">',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'location' 		=>	array(
																					'title' 	=>	'Location',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 90%; vertical-align: middle'
																				)
												)

									);

		// LOAD NEEDED DATA
		$this->load_page('/administration/user_approval_setup/users/form');
	}

	public function update(){
		$this->breadcrumb[]	= array(	'html'	=>	'Users',
										'url'	=>	'administration/user_approval_setup/users');
		$this->breadcrumb[] = array(	'html'	=>	'Update',
										'url'	=>	'');

		$users_model = $this->load_model('administration/user_approval_setup/users/users_model');
		
		// STRUCTURE FOR VIEW PAGE
		$this->data['type']	= 'update';
		$this->data['id'] = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);
		$this->data['position'] = $users_model->getAllActivePosition();
		$this->data['role'] 	= $users_model->getAllUserRoles();
		$this->data['user_list'] = $users_model->loadAllUsers();
		$this->data['location_list'] = $users_model->loadLocAccess($this->data['header']['U_ID']);
		$this->data['table_detail'] = array('tbl-detail'	=> array(
													'checkbox' 			=>	array(
																					'title' 	=>	'<input type="checkbox" 
																									id="chkSelectAll" 
																									class="icheckbox_square-green"',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'location' 		=>	array(
																					'title' 	=>	'Location',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 90%; vertical-align: middle'
																				)
												)

									);

		// LOAD NEEDED DATA
		$this->load_page('administration/user_approval_setup/users/form');

		
	}

	public function view(){
		$this->breadcrumb[] = array(		'html'	=>	'Users',
											'url'	=>	'administration/user_approval_setup/users');
		$this->breadcrumb[] = array(		'html'	=>	'View',
											'url'	=>	'');

		$users_model = $this->load_model('administration/user_approval_setup/users/users_model');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type']	= 'view';
		$this->data['id'] = $_GET['id'];
		$this->data['position'] = $users_model->getAllActivePosition();
		$this->data['role'] 	= $users_model->getAllUserRoles();
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);
		$this->data['user_list'] = $users_model->loadAllUsers();
		$this->data['location_list'] = $users_model->loadLocAccess($this->data['header']['U_ID']);
		$this->data['table_detail'] = array('tbl-detail'	=> array(
													'checkbox' 			=>	array(
																					'title' 	=>	'<input type="checkbox" 
																									id="chkSelectAll" 
																									class="icheckbox_square-green"',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'location' 		=>	array(
																					'title' 	=>	'Location',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 90%; vertical-align: middle'
																				)
												)

									);
		// LOAD NEEDED DATA
		$this->load_page('administration/user_approval_setup/users/form');
	}

	public function getData($type, $docno = ''){

		$users_model = $this->load_model('administration/user_approval_setup/users/users_model');

		if($type == 'add'){
			$output = array(
				'U_ID' 						=>	'',
				'U_Username' 				=>	'', 
				'CA_DefaultLocation' 		=>	'', 
				'U_FK_Position_id' 			=>	'',
				'U_Emp_Location' 			=>	'',
				'U_UserEmailAddress' 		=>	'',
				'U_Password' 				=>	'',
				'U_ParentUserID' 			=>	'',
				'UR_FK_RoleID'  			=> 	array()
			);
		}
		else if($type == 'update' || $type == 'view'){

			// User
			$where_docno = array(
				'U_ID' 	=> $docno 
			);

			// User Role
			$cols_dtl = array('UR_FK_RoleID');

			$dtl_where_docno = array(
									'UR_FK_UserID'  => $docno
							   );

			$header = $users_model->getHeaderByDocNo($this->cols, $this->header_table, $where_docno);
			$UR_details = array_column($users_model->getDetails($cols_dtl, 'tblUserRole', $dtl_where_docno, array(), array(), 'UR_FK_RoleID'), 'UR_FK_RoleID');

			$output = array(
				'U_ID' 						=>	$header['U_ID'],
				'U_Username' 				=>	$header['U_Username'], 
				'CA_DefaultLocation' 		=>	$users_model->getUsersDefaultLocation($header['U_ID'], $header['U_AssociatedEmployeeId']), 
				'U_FK_Position_id' 			=>	$header['U_FK_Position_id'],
				'U_UserEmailAddress' 		=>	$header['U_UserEmailAddress'],
				'U_Password' 				=>	$header['U_Password'],
				'U_ParentUserID' 			=>	$header['U_ParentUserID'],
				'UR_FK_RoleID'  			=> 	$UR_details
			);
		}

		return $output;
	}

	/*
		AJAX REQUEST GOES HERE
	*/

	public function data(){

		$users_model = $this->load_model('/administration/user_approval_setup/users/users_model');		
		$table_data = $users_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			// if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/user_approval_setup/users/view?id='.md5($value['U_ID']).'"><i class="fa fa-eye"></i></a> ';
			// }

			// if(in_array('Edit', $access_detail)){
				$button .= '<button class="btn btn-primary btn-xs btn-outline update-header" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['U_ID'].'" data-link="'.DOMAIN.'administration/user_approval_setup/users/update?id='.md5($value['U_ID']).'">
									<i class="fa fa-pencil"></i>
								</button> ';
			// }

			// if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['U_ID'].'"><i class="fa fa-trash"></i></button> ';
			// }

			$sub_array[] = $button;										
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['U_ID'].'">';
			$sub_array[] = $value['U_ID'];
			$sub_array[] = $value['U_Username'];
			$sub_array[] = $value['U_FK_Position_id'];
			$sub_array[] = $value['U_Status'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;

		if(!empty($_POST)):

			// GET ALL POST data
			$data = $this->input->post();
			$encryptor = new SGEncryption();
			if($data['U_FK_Position_id'] == 0){
				$data['U_FK_Position_id'] = "0000";
			}

			// LOAD ALL NEEDED MODEL
			$users_model = $this->load_model('administration/user_approval_setup/users/users_model');

			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];

			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){

					$output = $this->error_message(false, true, false, false, $data, $data['U_ID'], $this->number_series, '', 'U', 'tblUser');
					
					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table,
							'detail'  	=> 'tblCompanyAccess'
						);

						foreach($data['details'] as $key => $value){
							$data['details'][$key]['CA_U_ID'] = $data['U_ID'];

							if($data['DefaultLocation'] == $value['CA_FK_Location_id']){
								$data['details'][$key]['CA_DefaultLocation'] = 1; 
							}
						}

						unset($data['DefaultLocation']);

						if(strlen($data['U_ID']) < 8 || strlen($data['U_ID']) > 16){
							throw new Exception("Username must be 8-16 characters long , Error , error");
						}

						if(strlen($data['U_Password']) < 8 || strlen($data['U_Password']) > 16){
							throw new Exception("Password must be 8-16 characters long , Error , error");
						}

						if(strlen($data['U_Username']) < 4){
							throw new Exception("Name must be at least 4 characters long , Error , error");
						}

						$this->db->trans_begin();

						$data['U_Password'] = $encryptor->Encrypt($data['U_Password']);
						$user_role = $data['UR_FK_RoleID'];
						unset($data['UR_FK_RoleID']);

						$users_model->on_save_module(false, $data, $table, 'U', $this->number_series, '', 'CA');					

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}
					/* 
							END OF SAVING - ADD FUNCTION
					*/
			
				// UPDATE HEADER
				}
				else if($this->input->post('todo') == 'update'){
					
					$output = $this->error_message(false, true, false, false, $data, $data['U_ID'], $this->number_series, '', 'U', 'tblUser');

					if($output['success']):
						if(strlen($data['U_ID']) < 8 || strlen($data['U_ID']) > 16){
							throw new Exception("Username must be 8-16 characters long , Error , error");
						}

						if(strlen($data['U_Password']) < 8 || strlen($data['U_Password']) > 16){
							throw new Exception("Password must be 8-16 characters long , Error , error");
						}

						if(strlen($data['U_Username']) < 4){
							throw new Exception("Name must be at least 4 characters long , Error , error");
						}

						$current_password = $users_model->getUserPass($data['U_ID']);
						if($current_password !== $data['U_Password']){
							$data['U_Password'] = $encryptor->Encrypt($data['U_Password']);
						}

						$table = array(
							'header'  	=> $this->header_table,
							'detail'  	=> 'tblCompanyAccess'
						);

						$this->db->trans_begin();

						$user_role = $data['UR_FK_RoleID'];
						unset($data['UR_FK_RoleID']);


						foreach($data['details'] as $key => $value){
							$data['details'][$key]['CA_U_ID'] = $data['U_ID'];

							if($data['DefaultLocation'] == $value['CA_FK_Location_id']){
								$data['details'][$key]['CA_DefaultLocation'] = 1; 
							}
						}

						$this->db->where('CA_U_ID', $data['U_ID'])
								 ->delete('tblCompanyAccess');

						$users_model->on_update_module($data, $table, 'U', $this->number_series, '', 'CA');

						$users_model->saveUserRoles($data['U_ID'], $user_role, 'update');	

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
				}
				
				echo json_encode($output); //ENDING QUERY
			} 
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function delete(){
		$doc_no = $this->input->get();
		$users_model = $this->load_model('administration/user_approval_setup/users/users_model');
		echo json_encode($users_model->delete_data($doc_no['id']));
	}

	public function action(){
		$type = $this->input->post('action');
		$checked_list = $this->input->post('id');
		$users_model   = $this->load_model('administration/user_approval_setup/users/users_model');
		echo json_encode($users_model->status_data($checked_list,$type));
	}

}