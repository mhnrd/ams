<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');
class Position extends  MAIN_Controller{
	protected $number_series 		= 101002;
	protected $header_table			= 'tblPosition';
	protected $header_doc_no_field  = 'P_ID';
	protected $header_status_field	= 'P_Active';
	protected $module_name 			= 'Position'; 

	public function __construct(){
		parent:: __construct();
		$this->is_secure 	= true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> 'administration');
		$this->breadcrumb[] = array(	'html'	=> 'User Approval Setup',
										'url'	=> '');
		$this->data['header_table']				= $this->header_table;
		$this->data['header_doc_no_field']		= $this->header_doc_no_field;
		$this->data['header_status_field']		= $this->header_status_field;
		$this->data['number_series']			= $this->number_series;
		$this->data['access_header']			= $this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']			= $this->user_model->getDetailAccess($this->number_series);


		$this->cols  = array('P_ID','P_Position','P_Active','P_Type','P_Parent','PT_PositionType','PT_PositionType_id', 'P_AllowHalfDay'); 
		$this->table = 'tblPosition as P'; 
	}
	// HEADER TABLE
	public function index(){
		//$this->user_model->deleteUserRecordLock(getCurrentUser()['login-user']); //Delete User Record Lock
		$this->breadcrumb[]		 = array(	'html'	=>	'Position',
											'url'	=>	'administration/user_approval_setup/position');
		
		$this->data['table_hdr'] = array('position'	=>	array(

													'button' 	 			 => array(
																						'title' => '<a class="btn btn-success btn-xs btn-outline add-large" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="'.DOMAIN.'administration/user_approval_setup/position/add" role="button"><i class="fa fa-plus"></i></a>',
																						'class' => 'text-center',
																						'style' => 'width: 100px; vertical-align:middle'
																					 ),	
													'checkbox' 			     => array(
																						'title' 	=>	'<input type="checkbox" 
																									id="chkSelectAll" 
																									class="icheckbox_square-green"',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10px; vertical-align: middle'
																					 ),	

													'position-id' 		 	 => array(
																						'title' => 'Position ID',
																						'class' => 'text-left',
																						'style' => 'width: 10%; vertical-align:middle'
																					 ),		

													'position' 	  	 	 	 => array(
																						'title' => 'Position',
																						'class' => 'text-left',
																						'style' => 'width: 35%; vertical-align:middle'
																					 ),	

													'position-type'		  	 => array(
																						'title' => 'Position Type',
																						'class' => 'text-left',
																						'style' => 'width: 35%; vertical-align:middle'
																					 ),	
													'active' 				=>  array(
																						'title' => 'Status',
																						'class' => 'text-left',
																						'style' => 'width: 100px; vertical-align:middle'
																					 ),

			)
		);
		$this->data['hdr_center']	=	array(0,1,2,3);
		$this->load_page('administration/user_approval_setup/position/index');
	}

	public function add(){
		$this->breadcrumb[] 		= array(	'html'	=> 'Position',
												'url' 	=> 'administration/user_approval_setup/position');
		$this->breadcrumb[]			= array(	'html'	=> 'Add',
												'url'	=> '');

		$position_model					= $this->load_model('administration/user_approval_setup/position/position_model');


		$this->data['type']					= 'add';
		$this->data['position_type_list']	= $position_model->getPositionType();
		$this->data['header']				= $this->getData($this->data['type'], '');	
		$this->load_page('administration/user_approval_setup/position/form');
	}

	// Updating of Users Data
	public function update(){
		$this->breadcrumb[] 			= array (	'html'	=>	'Position',
													'url'	=>	'administration/user_approval_setup/position');
		$this->breadcrumb[] 			= array (	'html'	=>	'Update',
													'url'	=>	'');

		$position_model 		 			= $this->load_model('administration/user_approval_setup/position/position_model');

		$this->data['type'] 	  			= 'update';
		$this->data['id'] 	  	  			= $_GET['id'];
		$this->data['position_type_list']	= $position_model->getPositionType();
		$this->data['header'] 	  			= $this->getData($this->data['type'], $this->data['id']);
		$this->data['parent_list'] 			= $position_model->getPositionParent($this->data['header']['P_Type']);
		$this->load_page('administration/user_approval_setup/position/form');
	}
	
	//Viewing Of Users Data
	public function view(){
		$this->breadcrumb[] 			= array (	'html'	=>	'Position',
													'url'	=>	'administration/user_approval_setup/position');
		$this->breadcrumb[] 			= array (	'html'	=>	'View',
													'url'	=>	'');

		$position_model 		 			= $this->load_model('administration/user_approval_setup/position/position_model');
		
		$this->data['type']		  			= 'view';
		$this->data['id'] 	  				= $_GET['id'];

		$this->data['position_type_list']	= $position_model->getPositionType();
		$this->data['header']	  			= $this->getData($this->data['type'], $this->data['id']);
		$this->data['parent_list'] 			= $position_model->getPositionParent($this->data['header']['P_Type']);
		$this->load_page('administration/user_approval_setup/position/form');
	}
	// Getting of data from the Form
	public	function getData($type, $docno	= ''){
		$position_model	= $this->load_model('administration/user_approval_setup/position/position_model');
		if( $type == 'add'){
			$output	= array(
				'P_ID' 					=> '',
				'P_Position' 	 		=> '',
				'P_Parent' 				=> '', 
				'P_Type' 				=> '', 		
				'P_Active' 				=> 1, 		
				'P_AllowHalfDay' 		=> 0, 		
			);
		}else{
			$where = array(
				'P_ID' => 	$docno
			);
			$join_table	= array(
				array(
					'tbl_name'  => 'tblPositionType',
					'tbl_on' 	=> 'PT_PositionType_id = P_Type',
					'tbl_join'  => 'left' 	
				),
			);

			$header	= $position_model->getHeaderByDocNo($this->cols, $this->table, $where, $join_table);

			$output	= array(
				'P_ID' 					=> 	$header['P_ID'],
				'P_Position' 			=>	$header['P_Position'],
				'P_Type' 				=>	$header['P_Type'],
				'P_Parent' 				=> 	$header['P_Parent'], 
				'P_Active' 				=>  $header['P_Active'], 
				'P_AllowHalfDay' 		=>  $header['P_AllowHalfDay'], 
				'PT_PositionType' 		=>	$header['PT_PositionType'],
				'PT_PositionType_id'	=>	$header['PT_PositionType_id'], 	 	
			);	
		}
		return $output;
	}

	public function getParentList(){

		$p_type = $this->input->post('p-id');
		$position_model = $this->load_model('administration/user_approval_setup/position/Position_model');

		$output = $position_model->getPositionParent($p_type);

		echo json_encode($output);

	}

	//Data to be Display in the table 
	public function data(){
		$position_model = $this->load_model('administration/user_approval_setup/position/Position_model');
		$table_data 	= $position_model->table_data();
		$access_detail 	= $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/user_approval_setup/position/view?id='.md5($value['P_ID']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/user_approval_setup/position/update?id='.md5($value['P_ID']).'">
					<i class="fa fa-pencil"></i>
					</a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['P_ID'].'" "><i class="fa fa-trash"></i></a> ';
			//}

			$sub_array[] = $button;
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['P_ID'].'">';
			$sub_array[] = $value['P_ID'];
			$sub_array[] = $value['P_Position'];
			$sub_array[] = $value['PT_PositionType'];
			// $sub_array[] = '';
			// $sub_array[] = '';
			// $sub_array[] = '';
			$sub_array[] = $value['P_Active'];
			// $sub_array[] = '';
			
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function getCurrentPID(){

		$p_id = $this->input->post('p-id');

		$position_model = $this->load_model('/administration/user_approval_setup/position/position_model');

		$output = $position_model->generatePID($p_id);

		echo json_encode($output);

	}

	public function getPType(){

		$filter = $this->input->get();

		$position_model = $this->load_model('administration/user_approval_setup/position/position_model');

		$output = $position_model->searchStyles($filter['filter-type'], $filter['filter-search']);

		echo json_encode($output); 

	}
	public function getParentID(){

		$filter = $this->input->get();

		$position_model = $this->load_model('administration/user_approval_setup/position/position_model');

		$output = $position_model->searchParentID($filter['filter-type'], $filter['filter-search']);

		echo json_encode($output); 

	}

	public function save(){
		if(!$this->input->is_ajax_request()) return;

		if(!empty($_POST)):


			// Get All POST Data
			$data 						= $this->input->post();

			// Load All needed model
			$position_model				= $this->load_model('administration/user_approval_setup/position/position_model');
			// GET CURRENT USER
			$current_user 				= getCurrentUser()['login-user'];


			$output = array(
							'success'	=> true
						,	'message'	=> '');
			try{

				// Start Of Saving - Add Function
				if($this->input->post('todo') == 'add'){
					// GET THE LATEST NO SERIES
					$output = $this->error_message(false, true, false, false, $data, $data['P_ID'], $this->number_series, '', 'P', 'tblPosition');
					
				
				if($output['success']){
						// UPDATE NO SERIES
						$table = array(
							'header'  	=> $this->header_table,
						);

						$this->db->trans_begin();

						$position_model->on_save_module(false, $data, $table, 'P', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);

				$output = $this->error_message(false, false, false, false, $data, $data['P_ID'], $this->number_series,'','P','tblPosition');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,
							
						);

						$this->db->trans_begin();

						$position_model->on_update_module($data, $table, 'P', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['P_ID']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				echo json_encode($output); //ENDING QUERY
			}

			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;

	}


	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}

	public function getParent (){
		$data = $this->input->get();

		$position_model = $this->load_model('/administration/user_approval_setup/position/position_model');
		$ptposition_type = $position_model->getParent($data['parent_type']);
		echo json_encode($ptposition_type);
	}
}
?>
