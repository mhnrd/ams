<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class User_role_setup extends MAIN_Controller {

	protected $number_series		=	101003;
	protected $header_table			=	'tblRole';
	protected $header_doc_no_field	=	'R_ID';
	protected $header_status_field	=	'';
	protected $module_name			=	'User Role Setup';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(		'html'	=> 'Administration',
											'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=> 'User Role Setup',
										'url'	=> '');
		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						= 	array('R_ID', 'R_Name', 'R_Description');
	}

	public function index(){
		$this->breadcrumb[] = array(	'html' 	=>	'User Role Setup',
										'url'	=>	'administration/user_approval_setup/user_role_setup');

		$this->data['table_hdr'] = array('user_role_setup'	=> array(
													'buttons' 			=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" 
																									data-toggle="tooltip" data-placement="top" 
																									title="Add" type="button" href="'.
																									DOMAIN.'administration/user_approval_setup/user_role_setup/add"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 00px; vertical-align: middle' 
																				), 
													'name' 				=>	array(
																					'title' 	=>	'Role Name',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'description' 		=>	array(
																					'title' 	=>	'Role Description',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 80%; vertical-align: middle'
																				)
												)

									);
		
		$this->data['dtl_center'] = array(0,1);

		$this->load_page('administration/user_approval_setup/user_role_setup/index');
	}

	public function add(){
		$this->breadcrumb[] = array(		'html'	=>	'User Role Setup',
											'url'	=>	'administration/user_approval_setup/user_role_setup');
		$this->breadcrumb[] = array(		'html'	=>	'Add',
											'url'	=>	'');
		// LOAD NEEDED DATA
		$urs_detail_model = $this->load_model('/administration/user_approval_setup/user_role_setup/user_role_setup_detail_model');
		$urs_subdetail_model = $this->load_model('/administration/user_approval_setup/user_role_setup/user_role_setup_sub_detail_model');
		
		$this->data['type']   = 'add';
		$this->data['module'] = $urs_detail_model->getAllActiveModules($this->data['type']);
		$this->data['header'] = $this->getData($this->data['type'], '');
		$this->data['functions'] = $urs_subdetail_model->getAllModuleFunctions();

		$this->data['table_detail'] = array('tbl-detail'	=> array(
													'mod_name' 			=> 	array(
																					'title' 	=>	'Module Name',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 50%; vertical-align: middle' 
																				), 
													'add' 				=>	array(
																					'title' 	=>	'Add',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'edit' 				=>	array(
																					'title' 	=>	'Edit',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'delete' 			=>	array(
																					'title' 	=>	'Delete',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'view' 				=>	array(
																					'title' 	=>	'View',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'menu' 				=>	array(
																					'title' 	=>	'Menu',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
												)

									);

		$this->data['table_sub_detail'] = array('tbl-sub-detail'	=> array( 
													'mod_name' 			=>	array(
																					'title' 	=>	'Module',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 60%; vertical-align: middle'
																				),
													'func_name' 		=>	array(
																					'title' 	=>	'Function',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'checkbox' 			=>	array(
																					'title' 	=>	'Access',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				)
												)

									);

		
		$this->load_page('administration/user_approval_setup/user_role_setup/form');
	}

	public function update(){
		$this->breadcrumb[] = array(		'html'	=>	'User Role Setup',
											'url'	=>	'administration/user_approval_setup/user_role_setup');
		$this->breadcrumb[] = array(		'html'	=>	'Update',
											'url'	=>	'');
		// LOAD NEEDED DATA
		$urs_detail_model = $this->load_model('/administration/user_approval_setup/user_role_setup/user_role_setup_detail_model');
		$urs_subdetail_model = $this->load_model('/administration/user_approval_setup/user_role_setup/user_role_setup_sub_detail_model');
		
		$this->data['type']   = 'update';
		$this->data['id'] 	  = $_GET['id'];
		$this->data['module'] = $urs_detail_model->getAllActiveModules($this->data['type'], $this->data['id']);
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);
		$this->data['functions'] = $urs_subdetail_model->getAllModuleFunctions($this->data['header']['R_ID'], 'update');
		$this->data['table_detail'] = array('tbl-detail'	=> array(
													'mod_name' 			=> 	array(
																					'title' 	=>	'Module Name',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 50%; vertical-align: middle' 
																				), 
													'add' 				=>	array(
																					'title' 	=>	'Add',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'edit' 				=>	array(
																					'title' 	=>	'Edit',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'delete' 			=>	array(
																					'title' 	=>	'Delete',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'view' 				=>	array(
																					'title' 	=>	'View',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'menu' 				=>	array(
																					'title' 	=>	'Menu',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
												)

									);

		$this->data['table_sub_detail'] = array('tbl-sub-detail'	=> array( 
													'mod_name' 			=>	array(
																					'title' 	=>	'Module',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 60%; vertical-align: middle'
																				),
													'func_name' 		=>	array(
																					'title' 	=>	'Function',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'checkbox' 			=>	array(
																					'title' 	=>	'Access',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				)
												)

									);

		
		$this->load_page('administration/user_approval_setup/user_role_setup/form');
	}

	public function view(){
		$this->breadcrumb[] = array(		'html'	=>	'User Role Setup',
											'url'	=>	'administration/user_approval_setup/user_role_setup');
		$this->breadcrumb[] = array(		'html'	=>	'View',
											'url'	=>	'');
		// LOAD NEEDED DATA
		$urs_detail_model = $this->load_model('/administration/user_approval_setup/user_role_setup/user_role_setup_detail_model');
		$urs_subdetail_model = $this->load_model('/administration/user_approval_setup/user_role_setup/user_role_setup_sub_detail_model');
		
		$this->data['type']   = 'view';
		$this->data['id'] 	  = $_GET['id'];
		$this->data['module'] = $urs_detail_model->getAllActiveModules($this->data['type'], $this->data['id']);
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);
		$this->data['functions'] = $urs_subdetail_model->getAllModuleFunctions($this->data['header']['R_ID'], 'view');
		$this->data['table_detail'] = array('tbl-detail'	=> array(
													'mod_name' 			=> 	array(
																					'title' 	=>	'Module Name',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 50%; vertical-align: middle' 
																				), 
													'add' 				=>	array(
																					'title' 	=>	'Add',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'edit' 				=>	array(
																					'title' 	=>	'Edit',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'delete' 			=>	array(
																					'title' 	=>	'Delete',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'view' 				=>	array(
																					'title' 	=>	'View',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'menu' 				=>	array(
																					'title' 	=>	'Menu',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
												)

									);

		$this->data['table_sub_detail'] = array('tbl-sub-detail'	=> array( 
													'mod_name' 			=>	array(
																					'title' 	=>	'Module',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 60%; vertical-align: middle'
																				),
													'func_name' 		=>	array(
																					'title' 	=>	'Function',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'checkbox' 			=>	array(
																					'title' 	=>	'Access',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				)
												)

									);

		
		$this->load_page('administration/user_approval_setup/user_role_setup/form');
	}

	public function getData($type, $docno = ''){
		
		$urs_model = $this->load_model('/administration/user_approval_setup/user_role_setup/user_role_setup_model');

		if($type == 'add'){

			$code = $urs_model->getLastGroupCode();

			$output = array(
				'R_ID' 				=> $code,
				'R_Name' 			=> '',
				'R_Description' 	=> ''
			);
		}
		else{

			$where = array(
				'R_ID' 	=> $docno 
			);

			$header = $urs_model->getHeaderByDocNo($this->cols, $this->header_table, $where);

			$output = array(
				'R_ID' 				=> $header['R_ID'],
				'R_Name' 			=> $header['R_Name'],
				'R_Description' 	=> $header['R_Description']
			);
		}

		return $output;

	}

	public function data(){

		$urs_model = $this->load_model('/administration/user_approval_setup/user_role_setup/user_role_setup_model');		
		$table_data = $urs_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			// if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/user_approval_setup/user_role_setup/view?id='.md5($value['R_ID']).'"><i class="fa fa-eye"></i></a> ';
			// }

			// if(in_array('Edit', $access_detail)){
				$button .= '<button class="btn btn-primary btn-xs btn-outline update-header" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['R_ID'].'" data-link="'.DOMAIN.'administration/user_approval_setup/user_role_setup/update?id='.md5($value['R_ID']).'">
									<i class="fa fa-pencil"></i>
								</button> ';
			// }

			// if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['R_ID'].'"><i class="fa fa-trash"></i></button> ';
			// }

			$sub_array[] = $button;
			$sub_array[] = $value['R_Name'];
			$sub_array[] = $value['R_Description'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$urs_model = $this->load_model('/administration/user_approval_setup/user_role_setup/user_role_setup_model');
			$urs_detail_model = $this->load_model('/administration/user_approval_setup/user_role_setup/user_role_setup_detail_model');
			$urs_subdetail_model = $this->load_model('/administration/user_approval_setup/user_role_setup/user_role_setup_sub_detail_model');

			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){

					$output = $this->error_message(false, true, false, false, $data, $data['R_ID'], $this->number_series, '', 'R', 'tblRole');

					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table, 
						);

						$rf_header = array();

						$RA_header = $data['details'];

						if(isset($data['sub-details'])){
							$rf_header = $data['sub-details'];
						}
						unset($data['details']);
						unset($data['sub-details']);

						$this->db->trans_begin();

						$urs_model->on_save_module(false, $data, $table, 'R', $this->number_series, '');


						foreach($RA_header as $key => $value){

							$RA_header[$key]['todo'] = $data['todo'];
							$RA_header[$key]['RA_ID'] = $data['R_ID'];
							$table_ugm = array(
								'header'  	=> 'tblRoleAccess', 
							);

							if($output['success']){
								$urs_model->on_save_module(false, $RA_header[$key], $table_ugm, 'RA', '', '');
							}
							else{
								throw new Exception($output['message']);
							}

						}

						if(!empty($rf_header)){
							foreach($rf_header as $key => $value){
								$rf_header[$key]['RF_FK_RoleID'] = $data['R_ID'];
								$urs_subdetail_model->saveRoleFunctions($rf_header[$key]);
							}
						}


						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					
					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);

					$output = $this->error_message(false, true, false, false, $data, $data['R_ID'], $this->number_series, '', 'R', 'tblRole');

					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table, 
						);

						$rf_header = array();

						$RA_header = $data['details'];

						if(isset($data['sub-details'])){
							$rf_header = $data['sub-details'];
						}

						unset($data['details']);
						unset($data['sub-details']);

						$this->db->trans_begin();

						$urs_model->on_update_module($data, $table, 'R', $this->number_series, '');


						// Delete Role Access first then re-insert new access
						$this->db->where('RA_ID', $data['R_ID']);
						$this->db->delete('tblRoleAccess');

						foreach($RA_header as $key => $value){

							$RA_header[$key]['todo'] = $data['todo'];
							$RA_header[$key]['RA_ID'] = $data['R_ID'];
							$table_ugm = array(
								'header'  	=> 'tblRoleAccess', 
							);

							if($output['success']){
								$urs_model->on_save_module(false, $RA_header[$key], $table_ugm, 'RA', '', '');
							}
							else{
								throw new Exception($output['message']);
							}

						}

						if(!empty($rf_header)){
							foreach($rf_header as $key => $value){
								$rf_header[$key]['RF_FK_RoleID'] = $data['R_ID'];
								$urs_subdetail_model->saveRoleFunctions($rf_header[$key], 'update');
							}
						}

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
		
	}

}