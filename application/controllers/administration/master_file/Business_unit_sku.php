<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Business_unit_sku extends MAIN_Controller{

	protected $number_series 		= 103013;
	protected $header_table			= 'tblBusinessUnitSKU';
	protected $header_doc_no_field	= 'BUS_ID';
	protected $header_status_field	= 'BUS_Status';
	protected $module_name			= 'Business Unit SKU';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> '');
		$this->breadcrumb[] = array(	'html'	=> 'Master File',
										'url'	=> '');

		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->cols 						=   array('BUS_BU_ID', 'BUS_UPC', 'BUS_ID', 'BUS_SKUCode', 'BUS_Desc', 'BUS_ShortDesc', 
													  'BUS_FullDesc', 'BUS_VendorCode', 'BUS_Hierarchy', 'BUS_Dept', 'BUS_SubDept', 'BUS_Class', 
													  'BUS_SubClass', 'BUS_Buyer', 'BUS_SKUType', 'BUS_SellUOMCode', 'BUS_SellUOMDesc', 
													  'BUS_BuyingUOMCode', 'BUS_BuyingUOMDesc', 'BUS_InnerPack', 'BUS_GrossCost', 'BUS_MDPrice', 
													  'BUS_GMPerc', 'BUS_UPCType', 'BUS_InvGroup', 'BUS_SMKT', 'BUS_E3', 'BUS_Status', 
													  'BU_Description');
		// $this->cols_detail 					=   array('PCBUD_CategoryCode', 'PCBUD_MarkdownPrice', 'Category_Desc');
		$this->table 						=   'tblBusinessUnitSKU';
		// $this->table_detail 				=   'tblBusinessUnitDetail';
	}

	public function index(){
		$this->breadcrumb[]	= array(	'html' 	=> 'Business Unit SKU',
										'url'	=> 'administration/master_file/business_unit_sku');
		// LOAD NEEDED DATA

		$this->data['table_hdr']	= array('business_unit_sku' => array(
												'buttons' 				=> 	array(
																					'title'     => '<a class="btn btn-success btn-xs 
																									btn-outline" 
																									data-toggle="tooltip" data-placement="top" 
																									title="Add" type="button" href="'.
																									DOMAIN.'administration/master_file/business_unit_sku/add"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle' 
																				),
												'bu_code' 				=>	array(
																					'title' 	=>	'BU Code',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
												'doc_no' 				=>	array(
																					'title' 	=>	'Document No.',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle'
																				),
												'sku_code' 				=>	array(
																					'title' 	=>	'SKU Code',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
												'description' 			=>	array(
																					'title' 	=>	'Description',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 25%; vertical-align: middle'
																				),
												'upc_code' 				=>	array(
																					'title' 	=>	'UPC',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle'
																				),
												'style' 				=>	array(
																					'title' 	=>	'Style',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle'
																				),
												'season' 				=>	array(
																				'title' 	=>	'Season',
																				'class' 	=>	'text-center',
																				'style' 	=>	'width: 5%; vertical-align: middle'
																			),
												'gender' 				=>	array(
																				'title' 	=>	'Gender',
																				'class' 	=>	'text-center',
																				'style' 	=>	'width: 5%; vertical-align: middle'
																			),
												'dept' 					=>	array(
																				'title' 	=>	'Department',
																				'class' 	=>	'text-center',
																				'style' 	=>	'width: 5%; vertical-align: middle'
																			),
												'type' 					=>	array(
																				'title' 	=>	'Type',
																				'class' 	=>	'text-center',
																				'style' 	=>	'width: 5%; vertical-align: middle'
																			),
												'md_price' 				=>	array(
																				'title' 	=>	'Reg Price',
																				'class' 	=>	'text-center',
																				'style' 	=>	'width: 5%; vertical-align: middle'
																			),
												'reg_price' 			=>	array(
																				'title' 	=>	'MD Price',
																				'class' 	=>	'text-center',
																				'style' 	=>	'width: 5%; vertical-align: middle'
																			),
												'price_perc' 			=>	array(
																				'title' 	=>	'Prc % Off',
																				'class' 	=>	'text-center',
																				'style' 	=>	'width: 5%; vertical-align: middle'
																			),					
											)
									);

		$this->data['hdr_center'] = array(0,1,5);

		

		$this->load_page('/administration/master_file/business_unit_sku/index');
	}

	public function add(){
		$this->breadcrumb[] = array(		'html'	=>	'Business Unit SKU',
											'url'	=>	'administration/master_file/business_unit_sku');
		$this->breadcrumb[] = array(		'html'	=>	'Add',
											'url'	=>	'');
		// LOAD NEEDED DATA
		
		$busd_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');
		$bus_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_model');

		$this->data['type']   = 'add';
		$this->data['header'] = $this->getData($this->data['type'], '');
		$this->data['bu_code'] = $bus_model->getAllBUCode();
		$this->data['uom_list'] = $bus_model->getActiveUOM();
		
		$this->load_page('administration/master_file/business_unit_sku/form');
	}

	public function update(){
		$this->breadcrumb[] = array(		'html'	=>	'Business Unit SKU',
											'url'	=>	'administration/master_file/business_unit_sku');
		$this->breadcrumb[] = array(		'html'	=>	'Update',
											'url'	=>	'');
		// LOAD NEEDED DATA
		$busd_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');
		$bus_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_model');

		$this->data['type']   = 'update';
		$this->data['doc_no'] = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['doc_no']);
		// $this->print_r($this->data['header']);
		// die();
		$this->data['uom_list'] = $bus_model->getActiveUOM();
		$this->data['price_point_list'] = $bus_model->getSKUDetailList('DISTINCT IM.IM_UnitPrice as ID', 'Retailflex.dbo.tblInvMaster as IM', '');


		$this->data['table_detail'] = array('tbl-detail'	=> array(
													'button' 			=>	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs btn-outline add-detail" 
																									data-toggle="tooltip" data-placement="right" 
																									title="Add" type="button" href="javascript:void(0)"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 5%; vertical-align: middle'
																				),
													'style' 			=>	array(
																					'title' 	=>	'Style',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'season' 			=>	array(
																					'title' 	=>	'Season',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle'
																				),
													'gender' 			=>	array(
																					'title' 	=>	'Gender',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'dept' 				=>	array(
																					'title' 	=>	'Department',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'type' 				=>	array(
																					'title' 	=>	'Type',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'md_price' 			=>	array(
																					'title' 	=>	'Marked Down Price',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'reg_price' 		=>	array(
																					'title' 	=>	'Regular Price',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'price_perc' 		=>	array(
																					'title' 	=>	'Price % Off',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
												)

									);

		$this->data['table_subdetail'] = array('tbl-sub-detail'	=> array(
													'style' 			=>	array(
																					'title' 	=>	'Style',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'season' 			=>	array(
																					'title' 	=>	'Item',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
													'md_price' 			=>	array(
																					'title' 	=>	'Marked Down Price',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'reg_price' 		=>	array(
																					'title' 	=>	'Regular Price',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'price_perc' 		=>	array(
																					'title' 	=>	'Price % Off',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
												)

									);

		
		$this->load_page('administration/master_file/business_unit_sku/form');
	}

	public function view(){
		$this->breadcrumb[] = array(		'html'	=>	'Business Unit SKU',
											'url'	=>	'administration/master_file/business_unit_sku');
		$this->breadcrumb[] = array(		'html'	=>	'View',
											'url'	=>	'');
		// LOAD NEEDED DATA
		$bus_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_model');

		$this->data['type']   = 'view';
		$this->data['doc_no'] = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['doc_no']);
		$this->data['uom_list'] = $bus_model->getActiveUOM();
		// $this->data['season_list'] = $bus_model->getSKUDetailList('PS_Code as ID', 'tblProdSeason', 'PS_Active');
		// $this->data['gender_list'] = $bus_model->getSKUDetailList('PG_Code as ID', 'tblProdGender', 'PG_Active');
		// $this->data['dept_list'] = $bus_model->getSKUDetailList('PDT_Code as ID', 'tblProdDept', 'PDT_Active');
		// $this->data['type_list'] = $bus_model->getSKUDetailList('PT_Code as ID', 'tblProdType', 'PT_Active');
		$this->data['price_point_list'] = $bus_model->getSKUDetailList('DISTINCT IM_UnitPrice as ID', 'Retailflex.dbo.tblInvMaster as IM', '');

		$this->data['table_detail'] = array('tbl-detail'	=> array(
													'style' 			=>	array(
																					'title' 	=>	'Style',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle'
																				),
													'season' 			=>	array(
																					'title' 	=>	'Season',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle'
																				),
													'gender' 			=>	array(
																					'title' 	=>	'Gender',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'dept' 				=>	array(
																					'title' 	=>	'Department',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle'
																				),
													'type' 				=>	array(
																					'title' 	=>	'Type',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'md_price' 			=>	array(
																					'title' 	=>	'Marked Down Price',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'reg_price' 		=>	array(
																					'title' 	=>	'Regular Price',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'price_perc' 		=>	array(
																					'title' 	=>	'Price % Off',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
												)

									);

		$this->data['table_subdetail'] = array('tbl-sub-detail'	=> array(
													'style' 			=>	array(
																					'title' 	=>	'Style',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 30%; vertical-align: middle'
																				),
													'season' 			=>	array(
																					'title' 	=>	'Item',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 40%; vertical-align: middle'
																				),
													'md_price' 			=>	array(
																					'title' 	=>	'Marked Down Price',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'reg_price' 		=>	array(
																					'title' 	=>	'Regular Price',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
													'price_perc' 		=>	array(
																					'title' 	=>	'Price % Off',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle'
																				),
												)

									);


		
		$this->load_page('administration/master_file/business_unit_sku/form');
	}


	public function getData($type, $docno = ''){
		
		$bu_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_model');

		if($type == 'add'){

			$output = array(
				'BUS_BU_ID' 		=> '',
				'BUS_ID' 			=> '',
				'BUS_SKUCode' 		=> '',
				'BUS_Desc' 			=> '',
				'BUS_UPC' 			=> '',
				'BUS_ShortDesc' 	=> '',
				'BUS_FullDesc' 		=> '',
				'BUS_VendorCode' 	=> '',
				'BUS_Hierarchy' 	=> '',
				'BUS_Dept' 			=> '',
				'BUS_SubDept' 		=> '',
				'BUS_Class' 		=> '',
				'BUS_SubClass' 		=> '',
				'BUS_Buyer' 		=> '',
				'BUS_SKUType' 		=> '',
				'BUS_SellUOMCode' 	=> '',
				'BUS_SellUOMDesc' 	=> '',
				'BUS_BuyingUOMCode' => '',
				'BUS_BuyingUOMDesc' => '',
				'BUS_InnerPack' 	=> '',
				'BUS_GrossCost' 	=> 0,
				'BUS_MDPrice' 		=> 0,
				'BUS_GMPerc' 		=> 0,
				'BUS_UPCType' 		=> '',
				'BUS_InvGroup' 		=> '',
				'BUS_SMKT' 			=> '',
				'BUS_E3' 			=> ''	
			);
		}
		else{

			$where = array(
				'BUS_ID' 	=> $docno 
			);

			$join_table = array(
				array(
					'tbl_name' 	=> 'tblBusinessUnit',
					'tbl_on'  	=> 'BUS_BU_ID = BU_ID',
					'tbl_join'  => 'LEFT OUTER JOIN'
				)
			);

			$header = $bu_model->getHeaderByDocNo($this->cols, $this->header_table, $where, $join_table);

			$output = array(
				'BUS_BU_ID' 		=> $header['BUS_BU_ID'],
				'BU_Description' 	=> $header['BU_Description'],
				'BUS_ID' 			=> $header['BUS_ID'],
				'BUS_SKUCode' 		=> $header['BUS_SKUCode'],
				'BUS_Desc' 			=> $header['BUS_Desc'],
				'BUS_UPC' 			=> $header['BUS_UPC'],
				'BUS_ShortDesc' 	=> $header['BUS_ShortDesc'],
				'BUS_FullDesc' 		=> $header['BUS_FullDesc'],
				'BUS_VendorCode' 	=> $header['BUS_VendorCode'],
				'BUS_Hierarchy' 	=> $header['BUS_Hierarchy'],
				'BUS_Dept' 			=> $header['BUS_Dept'],
				'BUS_SubDept' 		=> $header['BUS_SubDept'],
				'BUS_Class' 		=> $header['BUS_Class'],
				'BUS_SubClass' 		=> $header['BUS_SubClass'],
				'BUS_Buyer' 		=> $header['BUS_Buyer'],
				'BUS_SKUType' 		=> $header['BUS_SKUType'],
				'BUS_SellUOMCode' 	=> $header['BUS_SellUOMCode'],
				'BUS_SellUOMDesc' 	=> $header['BUS_SellUOMDesc'],
				'BUS_BuyingUOMCode' => $header['BUS_BuyingUOMCode'],
				'BUS_BuyingUOMDesc' => $header['BUS_BuyingUOMDesc'],
				'BUS_InnerPack' 	=> $header['BUS_InnerPack'],
				'BUS_GrossCost' 	=> $header['BUS_GrossCost'],
				'BUS_MDPrice' 		=> $header['BUS_MDPrice'],
				'BUS_GMPerc' 		=> $header['BUS_GMPerc'],
				'BUS_UPCType' 		=> $header['BUS_UPCType'],
				'BUS_InvGroup' 		=> $header['BUS_InvGroup'],
				'BUS_SMKT' 			=> $header['BUS_SMKT'],
				'BUS_E3' 			=> $header['BUS_E3']	
			);
		}

		return $output;

	}

	public function data(){

		$bus_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_model');		
		$table_data = $bus_model->table_data();
		// $access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			// if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/master_file/business_unit_sku/view?id='.md5($value['BUS_ID']).'"><i class="fa fa-eye"></i></a> ';
			// }

			// if(in_array('Edit', $access_detail)){
				$button .= '<button class="btn btn-primary btn-xs btn-outline update-header" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['BUS_ID'].'" data-link="'.DOMAIN.'administration/master_file/business_unit_sku/update?id='.md5($value['BUS_ID']).'">
									<i class="fa fa-pencil"></i>
								</button> ';
			// }

			if($value['BUSD_ItemCategory'] != "" || $value['BUSD_ItemCategory'] !== null){
				$button .= '<button id="print-barcode" class="btn btn-warning btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Print Barcode" data-doc-no="'.trim($value['BUS_ID']).'"  data-code="'.$value['BUS_BU_ID'].'" data-category="'.$value['BUSD_ItemCategory'].'"><i class="fa fa-barcode"></i></button> ';
			}

			$sub_array[] = $button;
			$sub_array[] = $value['BUS_BU_ID'];
			$sub_array[] = $value['BUS_ID'];
			$sub_array[] = $value['BUS_SKUCode'];
			$sub_array[] = $value['BUS_Desc'];
			$sub_array[] = $value['BUS_UPC'];
			$sub_array[] = $value['BUSD_ItemCategory'];
			$sub_array[] = $value['BUSD_ItemSeason'];
			$sub_array[] = $value['BUSD_ItemGender'];
			$sub_array[] = $value['BUSD_ItemDept'];
			$sub_array[] = $value['BUSD_ItemType'];
			$sub_array[] = number_format($value['BUSD_RegPrice'], 2);
			$sub_array[] = number_format($value['BUSD_MDPrice'], 2);
			$sub_array[] = number_format($value['BUSD_PriceOffPerc'], 2).'%';

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_detail(){

		$docno 		= $this->input->get('id');
		$output_type = $this->input->get('type');
		$busd_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');		
		$table_data = $busd_model->table_data($docno, $output_type);
		// $access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			if($output_type == 'update'){
				$button = '';

				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['BUSD_ItemCategory'].'"><i class="fa fa-trash"></i></button> ';

				$sub_array[] = $button;
			}


			$sub_array[] = $value['BUSD_ItemCategory'];
			$sub_array[] = $value['BUSD_ItemSeason'];
			$sub_array[] = $value['BUSD_ItemGender'];
			$sub_array[] = $value['BUSD_ItemDept'];
			$sub_array[] = $value['BUSD_ItemType'];
			if($output_type == 'view'){
				$sub_array[] = number_format($value['BUSD_MDPrice'], 2);
				$sub_array[] = number_format($value['BUSD_RegPrice'], 2);
				$sub_array[] = number_format($value['BUSD_PriceOffPerc'] * 100, 2, ".", "");
			}
			else{
				$sub_array[] = '<button class="btn btn-link md-price-btn">'.number_format($value['BUSD_MDPrice'], 2).'</button>';
				$sub_array[] = number_format($value['BUSD_RegPrice'], 2);
				$sub_array[] = '<button class="btn btn-link prc-off-btn">'.number_format($value['BUSD_PriceOffPerc'] * 100, 2, ".", "").'</button>';
			}

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_subdetail(){

		$docno 			= $this->input->get('id');
		$output_type 	= $this->input->get('type');
		$busd_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_sub_detail_model');		
		$table_data = $busd_model->table_data($docno);
		// $access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();

			$sub_array[] = $value['BUSSD_ItemCategory'];
			$sub_array[] = $value['BUSSD_ItemCode'];

			if($output_type == 'view'){
				$sub_array[] = number_format($value['BUSSD_MDPrice'], 2);
				$sub_array[] = number_format($value['BUSSD_RegPrice'], 2);
				$sub_array[] = number_format($value['BUSSD_PriceOffPerc'] * 100, 2, ".", "");
			}
			else{
				$sub_array[] = '<button class="btn btn-link md-price-btn">'.number_format($value['BUSSD_MDPrice'], 2).'</button>';
				$sub_array[] = number_format($value['BUSSD_RegPrice'], 2);
				$sub_array[] = '<button class="btn btn-link prc-off-btn">'.number_format($value['BUSSD_PriceOffPerc'] * 100, 2, ".", "").'</button>';
			}

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}


	// AJAX REQUESTS GOES HERE

	public function getBSList(){
		$data = $this->input->post();
		$bus_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_model');

		$output = $bus_model->getBSDropdown($data['bu-code']);

		echo json_encode($output);
	}

	public function getCategoryDetails(){
		$category = $this->input->post('category');

		$bus_dtl_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');

		$output = array(
			'season'  	=> $bus_dtl_model->getCategoryDetails('DISTINCT FK_ProdSeason as ID', $category), 
			'gender'  	=> $bus_dtl_model->getCategoryDetails('DISTINCT FK_ProdGender as ID', $category), 
			'dept'  	=> $bus_dtl_model->getCategoryDetails('DISTINCT FK_ProdDept as ID', $category), 
			'type'  	=> $bus_dtl_model->getCategoryDetails('DISTINCT FK_ProdType as ID', $category) 
		);

		echo json_encode($output);
	}

	public function getCurrentBUS_ID(){

		$bu_code = $this->input->post('bu-code');

		$bus_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_model');

		$output = $bus_model->generateBUS_ID($bu_code);

		echo json_encode($output);

	}

	public function getDropdownList(){

		$filter = $this->input->get();
		$bus_detail_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');

		$output = $bus_detail_model->getList($filter);

		echo json_encode($output);

	}

	public function saveSKUDetails(){
		$data = $this->input->post();

		$bus_detail_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if($output['success']){
			$this->db->trans_begin();

			$bus_detail_model->generateSKUDetailandSubDetail($data);

			if($this->db->trans_status() !== False){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
				$output = array(
					'success'  	=> false,
					'message'  	=> 'Database Error',
					'type'  	=> 'error',
					'title'  	=> 'Error'
				);
			}

		}
		
		echo json_encode($output);

	}

	public function getSeasonList(){

		$data = $this->input->post();
		$bus_detail_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');

		$output = $bus_detail_model->seasonList($data['category']);

		echo json_encode($output);
	}

	public function getSeasonListByPricePoint(){

		$data = $this->input->post();
		$bus_detail_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');

		$data['category'] = $bus_detail_model->getCategoriesByPricePoint($data['prc_point']);

		$output = $bus_detail_model->seasonList($data['category']);

		echo json_encode($output);
	}

	public function getSizeList(){

		$data = $this->input->post();
		$bus_detail_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');

		$output = $bus_detail_model->sizeList($data);

		echo json_encode($output);
	}

	public function getSizeListByPricePoint(){

		$data = $this->input->post();
		$bus_detail_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');

		$data['category'] = $bus_detail_model->getCategoriesByPricePoint($data['prc_point']);

		$output = $bus_detail_model->sizeList($data);

		echo json_encode($output);
	}

	public function getGenderList(){

		$data = $this->input->post();

		$bus_detail_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');

		if(!isset($data['category'])){
			$data['category'] = $bus_detail_model->getCategoriesByPricePoint($data['prc_point']);
		}

		$output = $bus_detail_model->genderList($data['category'], $data['season']);

		echo json_encode($output);
	}

	public function getDeptList(){

		$data = $this->input->post();
		$bus_detail_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');

		if(!isset($data['category'])){
			$data['category'] = $bus_detail_model->getCategoriesByPricePoint($data['prc_point']);
		}

		$output = $bus_detail_model->deptList($data['category'], $data['season'], $data['gender']);

		echo json_encode($output);
	}

	public function getTypeList(){

		$data = $this->input->post();
		$bus_detail_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');

		if(!isset($data['category'])){
			$data['category'] = $bus_detail_model->getCategoriesByPricePoint($data['prc_point']);
		}

		$output = $bus_detail_model->typeList($data['category'], $data['season'], $data['gender'], $data['dept']);

		echo json_encode($output);
	}

	public function delete(){
		$data = $this->input->post();

		$busd_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_detail_model');
		$output = array(
			'success'  => true,
			'message'  => ''
		);

		$rowExist = $busd_model->checkIfRowExist($data);


		if(empty($rowExist)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'Detail has already been deleted',
				'type'  	=> 'info',
				'title'  	=> 'Information' 
			);
		}

		if($output['success']){

			$this->db->trans_begin();

			$this->db->where('BUSD_BUS_ID', $data['docno']);
			$this->db->where('BUSD_BUS_BU_ID', $data['bu_code']);
			$this->db->where('BUSD_ItemCategory', $data['category']);
			$this->db->where('BUSD_ItemSeason', $data['season']);
			$this->db->where('BUSD_ItemGender', $data['gender']);
			$this->db->where('BUSD_ItemDept', $data['dept']);
			$this->db->where('BUSD_ItemType', $data['type']);
			$this->db->delete('tblBusinessUnitSKUDetail');

			$countCat = $busd_model->countDetails($data['docno'], $data['bu_code'], $data['category']);

			if($countCat == 0){
				$this->db->where('BUSSD_BUS_ID', $data['docno']);
				$this->db->where('BUSSD_BU_ID', $data['bu_code']);
				$this->db->where('BUSSD_ItemCategory', $data['category']);
				$this->db->delete('tblBusinessUnitSKUSubDetail');
			}

			if($this->db->trans_status() !== False){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
				$output = array(
					'success'  	=> false,
					'message'  	=> 'Database Error',
					'type'  	=> 'error',
					'title'  	=> 'Error'
				);
			}
		}

		echo json_encode($output);

	}

	public function updatePrice(){
		$data = $this->input->post();

		$bus_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_model');

		$output = array(
			'success'  	=> true,
			'message'  	=> ''
		);

		$price = $bus_model->getPricesByRow($data);

		if($data['priceType'] == 'MD'){
			if($data['price'] > $price['BUSD_RegPrice']){
				$output = array(
					'success'  	=> false,
					'message'  	=> 'Marked Down Price cannot be greater than the Regular Price',
					'title'  	=> 'Information',
					'type'  	=> 'info'
				);
			}
		}
		elseif($data['priceType'] == 'RP'){
			if($data['price'] < $price['BUSD_MDPrice']){
				$output = array(
					'success'  	=> false,
					'message'  	=> 'Marked Down Price cannot be greater than the Regular Price',
					'title'  	=> 'Information',
					'type'  	=> 'info'
				);
			}
		}
		elseif($data['priceType'] == 'POFF'){
			if(($data['price']/100) > 1){
				$output = array(
					'success'  	=> false,
					'message'  	=> 'Price Off Percentage cannot be greater than 100%',
					'title'  	=> 'Information',
					'type'  	=> 'info'
				);
			}
		}

		if($output['success']){
			$this->db->trans_begin();

			$this->db->where('BUSD_BUS_ID', $data['docno']);
			$this->db->where('BUSD_BUS_BU_ID', $data['bu_code']);
			$this->db->where('BUSD_ItemCategory', $data['category']);
			$this->db->where('BUSD_ItemSeason', $data['season']);
			$this->db->where('BUSD_ItemGender', $data['gender']);
			$this->db->where('BUSD_ItemDept', $data['dept']);
			$this->db->where('BUSD_ItemType', $data['type']);
			$this->db->where('BUSD_RegPrice', $data['prc_point']);
			if($data['priceType'] == 'MD'){
				$this->db->set('BUSD_MDPrice', $data['price']);
			}
			elseif($data['priceType'] == 'RP'){
				$this->db->set('BUSD_RegPrice', $data['price']);
			}
			elseif($data['priceType'] == 'POFF'){
				$this->db->set('BUSD_PriceOffPerc', ($data['price']/100));
			}

			$this->db->update('tblBusinessUnitSKUDetail');

			if($this->db->trans_status() !== False){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
				$output = array(
					'success'  	=> false,
					'message'  	=> 'Database Error',
					'type'  	=> 'error',
					'title'  	=> 'Error'
				);
			}
		}

		echo json_encode($output);

	}

	public function updatePriceItem(){
		$data = $this->input->post();

		$bussd_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_sub_detail_model');

		$output = array(
			'success'  	=> true,
			'message'  	=> ''
		);

		$price = $bussd_model->getPricesByRow($data);

		if($data['priceType'] == 'MD'){
			if($data['price'] > $price['BUSSD_RegPrice']){
				$output = array(
					'success'  	=> false,
					'message'  	=> 'Marked Down Price cannot be greater than the Regular Price',
					'title'  	=> 'Information',
					'type'  	=> 'info'
				);
			}
		}
		elseif($data['priceType'] == 'RP'){
			if($data['price'] < $price['BUSSD_MDPrice']){
				$output = array(
					'success'  	=> false,
					'message'  	=> 'Marked Down Price cannot be greater than the Regular Price',
					'title'  	=> 'Information',
					'type'  	=> 'info'
				);
			}
		}
		elseif($data['priceType'] == 'POFF'){
			if(($data['price']/100) > 1){
				$output = array(
					'success'  	=> false,
					'message'  	=> 'Price Off Percentage cannot be greater than 100%',
					'title'  	=> 'Information',
					'type'  	=> 'info'
				);
			}
		}

		if($output['success']){
			$this->db->trans_begin();

			$this->db->where('BUSSD_BUS_ID', $data['docno']);
			$this->db->where('BUSSD_BU_ID', $data['bu_code']);
			$this->db->where('BUSSD_ItemCategory', $data['category']);
			$this->db->where('BUSSD_ItemCode', $data['item']);
			if($data['priceType'] == 'MD'){
				$this->db->set('BUSSD_MDPrice', $data['price']);
			}
			elseif($data['priceType'] == 'RP'){
				$this->db->set('BUSSD_RegPrice', $data['price']);
			}
			elseif($data['priceType'] == 'POFF'){
				$this->db->set('BUSSD_PriceOffPerc', ($data['price']/100));
			}

			$this->db->update('tblBusinessUnitSKUSubDetail');

			if($this->db->trans_status() !== False){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
				$output = array(
					'success'  	=> false,
					'message'  	=> 'Database Error',
					'type'  	=> 'error',
					'title'  	=> 'Error'
				);
			}
		}

		echo json_encode($output);

	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;
		
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$data['BUS_SellUOMDesc'] = getUOMDesc($data['BUS_SellUOMCode']);
			$data['BUS_BuyingUOMDesc'] = getUOMDesc($data['BUS_BuyingUOMCode']);
			unset($data['details']);
			unset($data['sub-details']);

			// Load All needed Model
			$bus_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_model');

			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];			


			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){

					if(!isset($data['details'])){

						$output = $this->error_message(false, true, false, false, $data, $data['BUS_ID'], $this->number_series, '', 'BUS', $this->header_table);
					}
					else{

						$output = $this->error_message(false, true, true, false, $data, $data['BUS_ID'], $this->number_series, '', 'BUS', $this->header_table);
					}


					if($output['success']){

						if(!isset($data['details'])){

							$table = array(
								'header'  	=> $this->header_table,
							);
						}
						else{

							$table = array(
								'header'  	=> $this->header_table,
								'detail'  	=> 'tblBusinessUnitSKUDetail' 
							);
						}

						$this->db->trans_begin();

						if(!isset($data['details'])){

							$bus_model->on_save_module(false, $data, $table, 'BUS', $this->number_series, '');
						}
						else{

							$bus_model->on_save_module(false, $data, $table, 'BUS', $this->number_series, '', 'BUSD');
						}


						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					
					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);

					if(!isset($data['details'])){

						$output = $this->error_message(false, true, false, false, $data, $data['BUS_ID'], $this->number_series, '', 'BUS', $this->header_table);
					}
					else{

						$output = $this->error_message(false, true, true, false, $data, $data['BUS_ID'], $this->number_series, '', 'BUS', $this->header_table);
					}

					if($output['success']):

						if(!isset($data['details'])){

							$table = array(
								'header'  	=> $this->header_table,
							);
						}
						else{

							$table = array(
								'header'  	=> $this->header_table, 
							);
						}

						$this->db->trans_begin();

						unset($data['BUS_BU_ID']);

						if(!isset($data['details'])){

							$bus_model->on_update_module($data, $table, 'BUS', $this->number_series, '');
						}
						else{

							$bus_model->on_update_module($data, $table, 'BUS', $this->number_series, '');
						}

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
		
	}

	public function print_barcode(){

		$generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
		
		$data = array();
		$filters = $this->input->post();
		$bus_model = $this->load_model('/administration/master_file/business_unit_sku/business_unit_sku_model');

		$data['bu_id'] 			= $filters['bu-id'];
		$data['category'] 		= $filters['category'];
		$data['bu_code'] 		= $filters['bu-code'];
		$data['qty_to_print'] 	= $filters['qty-to-print'];


		$printout_html 			= $bus_model->getPrintOutURL($data['bu_id']);

		$html = '';

		
		if(strpos($data['bu_id'], 'MDP') !== false){
			$details = $bus_model->getBarcodeDetailsByCategory($data['bu_code'], $data['category'], 'MDP');
			if(!empty($details)){
					$details['barcode'] = '<img height="30" width="85" src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode("000000" + $details['BUS_SKUCode'], $generatorPNG::TYPE_CODE_128)) . '">';
			}
		}
		elseif(strpos($data['bu_id'], 'RP') !== false){
			$details = $bus_model->getBarcodeDetailsByCategory($data['bu_code'], $data['category'], 'RP');
			if(!empty($details)){
				// RDS
				$details['barcode'] = '<img height="30" width="85" src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode("000000" + $details['BUS_SKUCode'], $generatorPNG::TYPE_CODE_128)) . '">';
			}
		}


		if(!empty($details)){

			$details['qty_to_print'] = $data['qty_to_print'];

			$html .= $this->load->view($printout_html, $details, true);


			$this->generate(
				// 100, 30
	    			array(	'format'=> [100, 30],
	    					'html'=> $html,
	    					'margin_top' => 2,
	    					'margin_bottom' => 0.5,
	    					'margin_left' => 1,
	    					'title' => 'Barcode Printout',
	    					'margin_right' => 1,
	    					'font'  => 'Arial Black',
	    					'is_dr_barcode'  => true
	  		));
		}
		else{
			
			echo json_encode(array(
									'success'	=>	false, 
									'message'	=>	'No SKU setup for '.trim($data['item_code']).' in '.$data['bu_code'],
									'title'  	=> 	'Information',
									'type'  	=>  'info' 
								));
		}

	}

	// AJAX REQUESTS END

}