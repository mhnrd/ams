<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Item_master extends MAIN_Controller {

	protected $number_series		=  1604;
	protected $header_table 		= 'tblItem';
	protected $header_doc_no_field	= 'IM_Item_id';
	protected $header_status_field	= 'IM_Active';
	protected $module_name 			= 'Item Master';

	public function __construct(){
		parent::__construct();		
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=>'Master File',
						'url'	=> '');
		$this->data['header_table'] 		= $this->header_table;
		$this->data['header_doc_no_field'] 	= $this->header_doc_no_field;
		$this->data['header_status_field'] 	= $this->header_status_field;
		$this->data['number_series'] 		= $this->number_series;
		$this->data['access_header']		= $this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		= $this->user_model->getDetailAccess($this->number_series);
	}

	public function index(){
		$this->user_model->deleteUserRecordLock(getCurrentUser()['login-user']); // Delete User Record Lock
		$this->breadcrumb[] = array(	'html'	=>'Item Master',
						'url'	=> 'master_file/item_master');
		$item_model = $this->load_model('master_file/item_master/item_master_model');		
		$this->load_page('/master_file/item_master/index');
	}

	public function add(){
		$this->breadcrumb[] = array('html'	=>'Item Master',	
									'url'	=> 'master_file/item_master');
		$this->breadcrumb[] = array('html'	=>'Add',					
									'url'	=> '');
		$companyaccess_model = $this->load_model('com/companyaccess_model');
		$this->data['type']      = 'add';
		$this->data['header']    = $this->getData('add');
		$this->data['location_list'] = $companyaccess_model->getStoreTypebyUserID(getCurrentUser()['login-user']);
		$this->data['all_location'] = $companyaccess_model->getAllLocation();
		$this->getListData('add');
		$this->load_page('/master_file/item_master/form');
	}

	public function update(){
		$this->breadcrumb[] = array('html'	=>'Item Master',	
									'url'	=> 'master_file/item_master');
		$this->breadcrumb[] = array('html'	=>'Update',					
									'url'	=> '');

		$companyaccess_model = $this->load_model('com/companyaccess_model');
		$this->data['type']      = 'update';
		$this->data['header']    = $this->getData('update');
		$this->data['detail']    = $this->getDetailData($this->data['header']['IM_Item_Id']);
		$this->data['all_location'] = $companyaccess_model->getAllLocation();
		$this->getListData('update');


		// Validate Document if other user is editing it.
		$this->validateUserRecordLock(	 getCurrentUser()['login-user']
										,$this->data['header']['IM_Item_Id']
										,$this->module_name
										,$this->header_table
										,'master_file/item_master'
										,'master_file/item_master/form'
									);
	}

	public function view(){
		$this->breadcrumb[] = array('html'	=>'Item Master',	
									'url'	=> 'master_file/item_master');
		$this->breadcrumb[] = array('html'	=>'View',					
									'url'	=> '');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type']      = 'view';
		$this->data['header']    = $this->getData('view');
		$this->data['detail']    = $this->getDetailData($this->data['header']['IM_Item_Id']);
		$this->getListData('view');
		$this->load_page('/master_file/item_master/form');
	}

	public function getListData($type){

		// LOAD NEEDED DATA
		$attrdetail_model = $this->load_model('master_file/bom/attrdetail_model');		
		$item_model       = $this->load_model('master_file/item_master/item_master_model');

		$itemType = ($type == 'add') ? 'INV' : $this->data['header']['IM_FK_Category_id'] ;

		$this->data['uom_list']          = $attrdetail_model->getAllByAttrCode('1401');
		$this->data['item_type']         = $item_model->getAllItemType();
		$this->data['category_list']	 = $item_model->getCategoryList($itemType);
		$this->data['inv_posting_group'] = $item_model->getAllProductInventoryPostingGroup();		
		$this->data['vat_posting_group'] = $item_model->getAllProductVATPostingGroup();		
		$this->data['wht_posting_group'] = $item_model->getAllProductWHTPostingGroup();		

	}


	public function getData($type){

		if($type == 'add'){
			$output = array(
				'IM_Item_Id'                 => ''
				,'IM_UPCCode'                => ''
				,'IM_Sales_Desc'             => ''
				,'IM_Purchased_Desc'         => ''
				,'IM_CostOfGoods'            => 0
				,'IM_FK_Supplier_id'         => ''
				,'IM_UnitCost'               => 0
				,'IM_UnitPrice'              => 0
				,'IM_VATProductPostingGroup' => ''
				,'IM_INVPosting_Group'       => ''
				,'IM_WHTProductPostingGroup' => ''
				,'IM_OnHandQty'              => 0
				,'IM_TotalValue'             => 0
				,'IM_ManufacturerPartNo'     => ''
				,'IM_Weight'                 => 0
				,'IM_ExpDate'                => ''
				,'IM_FK_Category_id'         => ''
				,'IM_FK_SizeSet_id'          => ''
				,'IM_Model'                  => ''
				,'IM_MinStkLvl'              => 0
				,'IM_SalePrice'              => 0
				,'IM_SaleType'               => ''
				,'IM_Consign'                => ''
				,'IM_Promo'                  => ''
				,'IM_FK_ItemType_id'         => ''
				,'IM_Royalty'                => ''
				,'IM_PackageItem'            => ''
				,'IM_Imported'               => ''
				,'IM_Short_Desc'             => ''
				,'IM_NextSeries'             => ''
				,'IM_Serialize'              => ''
				,'IM_FK_SubCategory_id'      => ''
				,'IM_Points'                 => ''
				,'IM_PAR_Item'               => ''
				,'IM_OldItemCode'            => ''
				,'IM_SupplierItemCode'       => ''
				,'IM_FK_BuyerClass_id'       => ''
				,'IM_FK_Department_id'       => ''
				,'IM_ForTO'                  => ''
				,'IM_FK_Attribute_UOM_id'    => ''
				,'IM_Production_UOM_id'    	 => ''
				,'IM_Transfer_UOM_id'    	 => ''
				,'IM_OF_UOM_id'    	 		 => ''
				,'IM_DocNo'                  => ''
				,'IM_Status'                 => 'Posted'
				,'IM_Active'                 => '0'
				,'IM_SalesUOM'               => ''
				,'IM_PurchaseUOM'            => ''
				,'IM_Item_Waste'             => 0
				,'IM_ExpiryDays'             => 0
				,'IM_SOH'					 => 0
				,'IM_ProductionArea'	     => ''
				,'IM_Scrap'					 => false
				,'IM_Scrap_Item_Id'			 => ''
				,'IM_Scrap_Perc'			 => ''
			);
		}
		else{

			$item_model 					= $this->load_model('master_file/bom/item_model');
			$item_master_model              = $this->load_model('master_file/item_master/item_master_model');
			$item_series 					= $this->input->get('id');
			$data 							= $item_model->getAllbyID($item_series);
			$UOM_information_data			= $item_master_model->getUOMInformationDataProdTransfer($item_series);

			$output = array(
				'IM_Item_Id'                 => $data['IM_Item_Id']
				,'IM_UPCCode'                => $data['IM_UPCCode']
				,'IM_Sales_Desc'             => $data['IM_Sales_Desc']
				,'IM_Purchased_Desc'         => $data['IM_Purchased_Desc']
				,'IM_CostOfGoods'            => $data['IM_CostOfGoods']
				,'IM_FK_Supplier_id'         => $data['IM_FK_Supplier_id']
				,'IM_UnitCost'               => $data['IM_UnitCost']
				,'IM_UnitPrice'              => $data['IM_UnitPrice']
				,'IM_SalePrice'				 => $data['IM_SalePrice']
				,'IM_VATProductPostingGroup' => $data['IM_VATProductPostingGroup']
				,'IM_INVPosting_Group'       => $data['IM_INVPosting_Group']
				,'IM_WHTProductPostingGroup' => $data['IM_WHTProductPostingGroup']
				,'IM_OnHandQty'              => $data['IM_OnHandQty']
				,'IM_TotalValue'             => $data['IM_TotalValue']
				,'IM_ManufacturerPartNo'     => $data['IM_ManufacturerPartNo']
				,'IM_Weight'                 => $data['IM_Weight']
				,'IM_FK_Category_id'         => $data['IM_FK_Category_id']
				,'IM_Model'                  => $data['IM_Model']
				,'IM_FK_ItemType_id'         => $data['IM_FK_ItemType_id']
				,'IM_Imported'               => $data['IM_Imported']
				,'IM_Short_Desc'             => $data['IM_Short_Desc']
				,'IM_Serialize'              => $data['IM_Serialize']
				,'IM_FK_SubCategory_id'      => $data['IM_FK_SubCategory_id']
				,'IM_PAR_Item'               => $data['IM_PAR_Item']
				,'IM_FK_Attribute_UOM_id'    => $data['IM_FK_Attribute_UOM_id']
				,'IM_Production_UOM_id'    	 => $data['IM_Production_UOM_id']
				,'ProductionUOM_Name'    	 => $UOM_information_data['ProductionUOM_Name']
				,'IM_Transfer_UOM_id'    	 => $data['IM_Transfer_UOM_id']
				,'TransferUOM_Name'    	 	 => $UOM_information_data['TransferUOM_Name']
				,'IM_OF_UOM_id'    	 		 => $data['IM_OF_UOM_id']
				,'OF_UOM_Name'    	 	 	 => $UOM_information_data['OF_UOM_Name']
				,'IM_Active'                 => $data['IM_Active']
				,'IM_SalesUOM'               => $data['IM_SalesUOM']
				,'IM_PurchaseUOM'            => $data['IM_PurchaseUOM']
				,'IM_Item_Waste'             => $data['IM_Item_Waste']
				,'IM_ExpiryDays'             => $data['IM_ExpiryDays']
				,'IM_LastPOCost'             => $item_model->getLastPOCost($data['IM_Item_Id'])
				,'IM_SOH'					 => $item_model->getSOH($data['IM_Item_Id'])
				,'IM_ProductionArea'		 => $data['IM_ProductionArea']
				,'IM_Scrap'					 => $data['IM_Scrap']
				,'IM_Scrap_Item_Id'			 => $data['IM_Scrap_Item_Id']
				,'IM_Scrap_Perc'			 => $data['IM_Scrap_Perc']
				,'ScrapParentDesc'			 => $data['ScrapParentDesc']
			);
		}

		return $output;

	}


	public function getDetailData($item_id){

		$item_model                        = $this->load_model('master_file/item_master/item_master_model');
		$identifier_data                   = $item_model->getIdentifierData($item_id);
		$item_classification_grouping_data = $item_model->getItemClassificationGroupingData($item_id);
		$UOM_information_data			   = $item_model->getUOMInformationData($item_id);

		$output = array(
			'ItemType_id'       		=> $identifier_data['IM_FK_ItemType_id'],
			'ItemType_name'     		=> $identifier_data['IT_Description'],
			'Category_id'       		=> $identifier_data['IM_FK_Category_id'],
			'Category_name'     		=> $identifier_data['CAT_Desc'],
			'SubCategory_id'    		=> $identifier_data['IM_FK_SubCategory_id'],
			'SubCategory_name'  		=> $identifier_data['SC_Description'],
			'Production_area'  			=> $identifier_data['IM_ProductionArea'],
			'Production_area_name'  	=> $identifier_data['SP_StoreName'],
			'BaseUOM_id'        		=> $UOM_information_data['IM_FK_Attribute_UOM_id'],
			'BaseUOM_name'      		=> $UOM_information_data['AD_Code'],
			'SalesUOM_id'       		=> $UOM_information_data['IM_SalesUOM'],
			'SalesUOM_name'     		=> $UOM_information_data['SalesUOM_Name'],
			'PurchUOM_id'       		=> $UOM_information_data['IM_PurchaseUOM'],
			'PurchUOM_name'     		=> $UOM_information_data['PurchUOM_Name'],
			'Inventory_PG_id'   		=> $item_classification_grouping_data['IM_INVPosting_Group'],
			'Inventory_PG_name' 		=> $item_classification_grouping_data['IM_VATProductPostingGroup'],
			'VAT_id'            		=> $item_classification_grouping_data['VPPG_Description'],
			'VAT_name'          		=> $item_classification_grouping_data['VPPG_Description'],
			'WHT_id'            		=> $item_classification_grouping_data['IM_WHTProductPostingGroup'],
			'WHT_name'          		=> $item_classification_grouping_data['WPPG_Description'],
			'Supplier_list'     		=> $item_model->getItemSupplierData($item_id),
			'UOM_list'          		=> $item_model->getItemUOMData($item_id)
		);

		return $output;
	}


	public function data(){

		$item_model = $this->load_model('master_file/item_master/item_master_model');		
		// $this->print_r(getUserLocationAccess());
		$table_data = $item_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'master_file/item_master/view?id='.md5($value['IM_Item_Id']).'"><i class="fa fa-eye"></i></a> ';
			}

			if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'master_file/item_master/update?id='.md5($value['IM_Item_Id']).'">
									<i class="fa fa-pencil"></i>
								</a> ';
			}

			if(in_array('Delete', $access_detail)){
				$button .= '<a class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['IM_Item_Id'].'" data-desc="'.$value['IM_Sales_Desc'].'"><i class="fa fa-trash"></i></a> ';
			}

			$sub_array[] = $button;										
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['IM_Item_Id'].'">';
			$sub_array[] = $value['IM_Item_Id'];
			$sub_array[] = $value['IM_UPCCode'];
			$sub_array[] = $value['IM_Sales_Desc'];
			$sub_array[] = '<a class="soh-breakdown" data-uom="'.$value['AD_Code'].'" data-item-no="'.$value['IM_Item_Id'].'" data-item-desc="'.$value['IM_Sales_Desc'].'" href="#" role="button" style="text-decoration: underline" data-original-title="Click to see SOH Breakdown" data-toggle="tooltip" data-placement="bottom">'.number_format($value['IM_SOH'],4).'</a>';
			$sub_array[] = $value['AD_Code'];
			$sub_array[] = ($value['IM_Active'] == 1) ? "Active" : "Inactive";
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function getItem(){
		if (!$this->input->is_ajax_request()) return;
		$type = $_GET['filter-type'];
		$filter = $_GET['filter-search'];
		$action = $_GET['action'];
		$cat_id = '';
		if(isset($_GET['category-type'])){
			$cat_id = $_GET['category-type'];
		}

		$data = array();
		$item_model = $this->load_model('master_file/item_master/item_master_model');
		if($action=='search'){
			$data = $item_model->getAllBySearch($type, $filter,$cat_id);
		}
		else if($action=='Select'){
			$data = $item_model->getByFilter($type, $filter,$cat_id);
		}


		
		echo json_encode($data);
	}

	public function getScrapParent(){
		if (!$this->input->is_ajax_request()) return;
		$type = $_GET['filter-type'];
		$filter = $_GET['filter-search'];
		$action = $_GET['action'];
		$cat_id = '';
		if(isset($_GET['category-type'])){
			$cat_id = $_GET['category-type'];
		}

		$data = array();
		$item_model = $this->load_model('master_file/item_master/item_master_model');
		if($action=='search'){
			$data = $item_model->getScrapParentBySearch($type, $filter,$cat_id);
		}
		else if($action=='Select'){
			$data = $item_model->getScrapParentByFilter($type, $filter,$cat_id);
		}

		
		echo json_encode($data);
	}

	public function getItemwithBOM(){
		if (!$this->input->is_ajax_request()) return;
		$type = $_GET['filter-type'];
		$filter = $_GET['filter-search'];
		$action = $_GET['action'];
		$item_type = '';
		if(isset($_GET['item-type'])){
			$cat_id = $_GET['item-type'];
		}

		$data = array();
		$item_model = $this->load_model('administration/master_file/item_master/item_master_model');
		if($action=='search'){
			$data = $item_model->getItemBySearch($type, $filter,$item_type);
		}
		else if($action=='Select'){
			$data = $item_model->getItemByFilter($type, $filter,$item_type);
		}

		echo json_encode($data);
	}

	/*
		AJAX Request goes here
	*/

	public function save(){
		if (!$this->input->is_ajax_request()) return;
		
		if(!empty($_POST)):

			// Get all POST Data
			$data         = $this->input->post();
			
			// Load All needed Model
			$item_model   = $this->load_model('master_file/item_master/item_master_model');
			
			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];						

			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){
					
					// GET THE LATEST NO SERIES
					$item_series = $this->getItemNumberController($data['IM_FK_Category_id']);

					if(empty($data['IM_Item_Id'])){
						$output['success'] = false;
						$output['message'] = 'item-no-null';
					}
					if($item_model->getCountByItemNo($item_series)){
						$output['success'] = false;
						$output['message'] = 'item-no-exists';
					}
					if($data['IM_Scrap'] == true){
						if(!isset($data['IM_Scrap_Item_Id']) || empty($data['IM_Scrap_Item_Id']) || empty($data['IM_Scrap_Perc'])){
							$output['success'] = false;
							$output['message'] = 'item-scrap-inc';
						}
					}

					if($output['success']):

						// Saving function
						$saving_output = $item_model->on_save($data,$item_series,$current_user);
						$output['success'] = $saving_output['success'];
						$output['message'] = $saving_output['message'];
						
					endif; 

					
					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

						// Update function
						$update_output = $item_model->on_update($data,$data['IM_Item_Id'],$current_user);
						$output['success'] = $update_output['success'];
						$output['message'] = $update_output['message'];
						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
				}
				
				echo json_encode($output); //ENDING QUERY
			
			}
			catch(Exception $e){

				$output = array(
							'success'=>false
						,	'message'=>$e->getMessage());

				echo json_encode($output);
			}
		endif;
		
	}

	public function delete(){
		$doc_no     = $this->input->get();
		$item_model = $this->load_model('master_file/item_master/item_master_model');
		echo json_encode($item_model->delete_data($doc_no['id']));
	}

	public function action(){
		$type         = $this->input->post('action');
		$checked_list = $this->input->post('id');
		$item_model   = $this->load_model('master_file/item_master/item_master_model');
		echo json_encode($item_model->status_data($checked_list,$type));
	}

	public function getCategory(){
		$itemtype = $this->input->get('ItemType');
		$category = $this->load_model('master_file/item_master/item_master_model');
		echo json_encode($category->getCategoryList($itemtype));
	}

	public function getSubCategory(){
		$cat_id   = $this->input->get('CategoryID');
		$category = $this->load_model('master_file/item_master/item_master_model');
		echo json_encode($category->getSubCategoryList($cat_id));
	}

	public function getItemNumber(){
		$itemtype = $this->input->get('ItemType');
		$category = $this->load_model('master_file/item_master/item_master_model');
		echo json_encode($category->getItemNumberbyItemType($itemtype));
	}

	public function getItemNumberController($category_id){
		$category = $this->load_model('master_file/item_master/item_master_model');
		return $category->getItemNumberbyItemType($category_id);
	}

	public function getSupplier(){
		if (!$this->input->is_ajax_request()) return;
		$type = $_GET['filter-type'];
		$filter = $_GET['filter-search'];
		$action = $_GET['action'];

		if(!isset($_GET['supplier-list'])){
			$supplierlist = array();
		}
		else{
			$supplierlist = $_GET['supplier-list'];	
		}

		$data = array();
		$item_model = $this->load_model('master_file/item_master/item_master_model');
		if($action=='search'){
			$data = $item_model->getAllSupplierBySearch($type, $filter,$supplierlist);
		}
		else if($action=='Select'){
			$data = $item_model->getSupplierByFilter($type, $filter,$supplierlist);
		}
		
		echo json_encode($data);
	}

	public function getUOM(){
		if (!$this->input->is_ajax_request()) return;
		$type = $_GET['filter-type'];
		$filter = $_GET['filter-search'];
		$action = $_GET['action'];
		
		if(!isset($_GET['uom-list'])){
			$uom_list = array();
		}
		else{
			$uom_list = $_GET['uom-list'];
		}

		$data = array();
		$item_model = $this->load_model('master_file/item_master/item_master_model');
		if($action=='search'){
			$data = $item_model->getAllUOMBySearch($type, $filter, $uom_list);
		}
		else if($action=='Select'){
			$data = $item_model->getUOMByFilter($type, $filter, $uom_list);
		}
		
		echo json_encode($data);
	}

	public function getSOHBreakdown(){
		$data = $this->input->get('id');

		$im_model = $this->load_model('master_file/item_master/item_master_model');

		$output = $im_model->getAllSOHByLocation($data);

		echo json_encode ($output);
	}

	public function copyItemDetail(){
		$data = $this->input->post('item-no');

		$im_model = $this->load_model('master_file/item_master/item_master_model');

		echo json_encode($im_model->copyDetails($data));
	}
	
	public function SupplierList(){
		$data = $this->input->post('item-no');

		$im_model = $this->load_model('master_file/item_master/item_master_model');

		echo json_encode($im_model->copySupplierList($data));
	}

	public function UOMList(){
		$data = $this->input->post('item-no');

		$im_model = $this->load_model('master_file/item_master/item_master_model');

		echo json_encode($im_model->copyUOMList($data));
	}

	public function getProdTransOFUom(){
		$data = $this->input->post();

		$im_model = $this->load_model('master_file/item_master/item_master_model');

		$output = $im_model->getProdTransOFUom($data['item-no']);

		echo json_encode($output);

	}

	/*
		AJAX Request ends here
	*/
}
