<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Employee_profile extends MAIN_Controller {

	protected $number_series		=	103012;
	protected $header_table			=	'tblEmployee';
	protected $header_doc_no_field	=	'Emp_Id';
	protected $header_status_field	=	'Emp_Active';
	protected $module_name			=	'Employee Profile';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(		'html'	=> 'Administration',
											'url'	=> 'administration');
		$this->breadcrumb[] = array(		'html'	=> 'Master File',
											'url'	=> '');

		$this->data['config'] 				= 	ModuleSystemConfigs($this->number_series);
		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						= array('Emp_Name');
	}

	public function index(){

		$attributes_model 		= $this->load_model('administration/application_setup/general/attributes/attributes_model');

		$this->breadcrumb[] = array(	'html' 	=>	'Employee Profile',
										'url'	=>	'administration/master_file/employee_profile');

		$this->data['table_hdr'] = array('employee_profile_details'	=> array(
													'buttons' 			=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" 
																									data-toggle="tooltip" data-placement="right" 
																									title="Add" type="button" href="'.
																									DOMAIN.'administration/master_file/employee_profile/add"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle' 
																				),
													'Emp_Name' 			=>	array(
																					'title' 	=>	'Employee Name',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				)
										)

									);
		
		$this->data['table_id'] = array('employee_id_details'	=> array(
													'buttons' 			=> 	array(
																					'title' 	=>	'<a class="invisible btn btn-success btn-xs 
																									btn-outline" data-toggle="tooltip" data-placement="right" 
																									title="Add" type="button" href="#" id="btnIDHistoryAdd"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle' 
																				),
													'EID_LineNo' 			=>	array(
																					'title' 	=>	'Line No',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'EID_NewId' 			=>	array(
																					'title' 	=>	'ID Number',
																					'class' 	=>	'text-right',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'EID_BiometrixNo' 			=>	array(
																					'title' 	=>	'Biometrix',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Createdby' 			=>	array(
																					'title' 	=>	'Created by',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'DateCreated' 			=>	array(
																					'title' 	=>	'Date Created',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
										)

									);

		$this->data['table_allowance'] = array('employee_allowance_details'	=> array(
													'buttons' 			=> 	array(
																					'title' 	=>	'<a class="invisible btn btn-success btn-xs 
																									btn-outline" data-toggle="tooltip" data-placement="right" 
																									title="Add" type="button" href="#" id="btnAllowanceAdd"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle' 
																				),
													'Allowance' 			=>	array(
																					'title' 	=>	'Allowance',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Amount' 			=>	array(
																					'title' 	=>	'Amount',
																					'class' 	=>	'text-right',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Taxable' 			=>	array(
																					'title' 	=>	'Taxable',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Type' 			=>	array(
																					'title' 	=>	'Type',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
										)

									);

		$this->data['table_relatives_siblings'] = array('siblings_details'	=> array(
													'buttons' 				=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" data-toggle="tooltip" data-placement="right" 
																									title="Add" type="button" href="#" id="btnSiblingsAdd"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle' 
																				),
													'Name' 					=>	array(
																					'title' 	=>	'Name',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'BirthDate' 			=>	array(
																					'title' 	=>	'Birth Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Status' 				=>	array(
																					'title' 	=>	'Status',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Occupation' 			=>	array(
																					'title' 	=>	'Occupation',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'CompanyandAddress' 	=>	array(
																					'title' 	=>	'Company and Address',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Relation' 			=>	array(
																					'title' 	=>	'Relation',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
			)

		);

		$this->data['table_relatives_beneficiaries'] = array('beneficiaries_details'	=> array(
													'buttons' 				=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" data-toggle="tooltip" data-placement="right" 
																									title="Add" type="button" href="#" id="btnBeneficiariesAdd"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle' 
																				),
													'Name' 					=>	array(
																					'title' 	=>	'Name',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Status' 				=>	array(
																					'title' 	=>	'Status',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Occupation' 			=>	array(
																					'title' 	=>	'Occupation',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'CompanyandAddress' 	=>	array(
																					'title' 	=>	'Company and Address',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Relation' 			=>	array(
																					'title' 	=>	'Relation',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
			)

		);

		$this->data['table_education'] = array('education_details'	=> array(
													'buttons' 				=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" data-toggle="tooltip" data-placement="right" 
																									title="Add" type="button" href="#" id="btnEducationAdd"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle' 
																				),
													'School' 					=>	array(
																					'title' 	=>	'School',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Course' 				=>	array(
																					'title' 	=>	'Course',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'School Year' 			=>	array(
																					'title' 	=>	'School Year',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Levels' 	=>	array(
																					'title' 	=>	'Levels',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'HonorReceived' 			=>	array(
																					'title' 	=>	'Honor Received',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
			)

		);

		$this->data['table_leave'] = array('leave_details'	=> array(													
													'LeaveType' 					=>	array(
																					'title' 	=>	'Leave Type',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Days' 							=>	array(
																					'title' 	=>	'Day(s)',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'AppliedDate' 			=>	array(
																					'title' 	=>	'Applied Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'DateFrom' 	=>	array(
																					'title' 	=>	'Date (From)',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'DateTo' 	=>	array(
																					'title' 	=>	'Date (To)',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'WithApproval' 			=>	array(
																					'title' 	=>	'With<br>Approval',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Remarks' 			=>	array(
																					'title' 	=>	'Remarks',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Initial' 			=>	array(
																					'title' 	=>	'Initial',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
			)

		);

		$this->data['table_leave_bal'] = array('leave_bal_details'	=> array(
													'buttons' 				=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline invisible" data-toggle="tooltip" data-placement="right" 
																									title="Add" type="button" href="#" id="btnLeaveBalAdd"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),													
													'LeaveType' 					=>	array(
																					'title' 	=>	'Leave Type',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 70%; vertical-align: middle' 
																				),
													'LeaveBalance' 							=>	array(
																					'title' 	=>	'Leave Balance',
																					'class' 	=>	'text-right',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'LeaveRunBalance' 							=>	array(
																					'title' 	=>	'Running Balance',
																					'class' 	=>	'text-right',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),													
			)

		);

		$this->data['table_leave_view'] = array('leave_bal_view'	=> array(
													'LL_DocType' 					=>	array(
																					'title' 	=>	'Doc Type',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 25%; vertical-align: middle' 
																				),
													'LL_DocNo' 							=>	array(
																					'title' 	=>	'Doc No',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 25%; vertical-align: middle' 
																				),
													'LL_DocDate' 							=>	array(
																					'title' 	=>	'Date Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 25%; vertical-align: middle' 
																				),
													'LL_NumDays' 							=>	array(
																					'title' 	=>	'# Days',
																					'class' 	=>	'text-right',
																					'style' 	=>	'width: 25%; vertical-align: middle' 
																				),													
			)

		);

		$this->data['table_work_sched'] = array('work_sched_details'	=> array(													
													'AppliedDate' 					=>	array(
																					'title' 	=>	'Applied Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'WeekDay' 							=>	array(
																					'title' 	=>	'Week Day',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'ShiftCode' 							=>	array(
																					'title' 	=>	'Shift Code',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'ShiftDescription' 							=>	array(
																					'title' 	=>	'Shift Description',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 50%; vertical-align: middle' 
																				),
													'StartTime' 							=>	array(
																					'title' 	=>	'Start Time',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'EndTime' 							=>	array(
																					'title' 	=>	'End Time',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),																											
			)

		);

		$this->data['table_notes'] = array('notes_details'	=> array(													
													'buttons' 				=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" data-toggle="tooltip" data-placement="right" 
																									title="Add" type="button" href="#" id="btnNotesAdd"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle' 
																				),
													'Date' 					=>	array(
																					'title' 	=>	'Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Notes' 							=>	array(
																					'title' 	=>	'Notes',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 80%; vertical-align: middle' 
																				),													
			)

		);

		$this->data['table_notes'] = array('notes_details'	=> array(													
													'buttons' 				=> 	array(
																					'title' 	=>	'<a class="btn btn-success btn-xs 
																									btn-outline" data-toggle="tooltip" data-placement="right" 
																									title="Add" type="button" href="#" id="btnNotesAdd"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 15%; vertical-align: middle' 
																				),
													'Date' 					=>	array(
																					'title' 	=>	'Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Notes' 							=>	array(
																					'title' 	=>	'Notes',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 80%; vertical-align: middle' 
																				),													
			)

		);

		$this->data['table_employee_movement'] = array('employee_movement_details'	=> array(																										
													'AppliedDate' 					=>	array(
																					'title' 	=>	'Applied Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'Principal' 							=>	array(
																					'title' 	=>	'Principal',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'JobTitle' 							=>	array(
																					'title' 	=>	'Job Title',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'Department' 							=>	array(
																					'title' 	=>	'Department',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'BasicSalary' 							=>	array(
																					'title' 	=>	'Basic Salary',
																					'class' 	=>	'text-right',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'Category' 							=>	array(
																					'title' 	=>	'Category',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'CashBond' 							=>	array(
																					'title' 	=>	'Cash Bond',
																					'class' 	=>	'text-right',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),													
			)

		);

		$this->data['table_memo'] = array('memo_details'	=> array(																										
													'DocNo' 					=>	array(
																					'title' 	=>	'Doc No',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'DocDate' 							=>	array(
																					'title' 	=>	'Doc Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'AppliedDate' 					=>	array(
																					'title' 	=>	'Applied Date',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 10%; vertical-align: middle' 
																				),
													'Status' 					=>	array(
																					'title' 	=>	'Status',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'CreatedBy' 					=>	array(
																					'title' 	=>	'Created By',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'DateCreated' 					=>	array(
																					'title' 	=>	'Date Created',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
																									
			)

		);

		$this->data['table_memo_document'] = array('memo_document_details'	=> array(																										
													'Document' 					=>	array(
																					'title' 	=>	'Document',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Remarks' 					=>	array(
																					'title' 	=>	'Remarks',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'Download' 					=>	array(
																					'title' 	=>	'Download',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'CreatedBy' 					=>	array(
																					'title' 	=>	'CreatedBy',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'DateCreated' 					=>	array(
																					'title' 	=>	'Date Created',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'ModifiedBy' 					=>	array(
																					'title' 	=>	'Modified By',
																					'class' 	=>	'text-left',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),
													'DateModified' 					=>	array(
																					'title' 	=>	'Date Modified',
																					'class' 	=>	'text-center',
																					'style' 	=>	'width: 20%; vertical-align: middle' 
																				),

													
																									
			)

		);


		
		$this->data['dtl_center'] = array(0);
		$this->data['relationship_list'] 	= $attributes_model->getAttributesData(RELATION);
		$this->data['leavetype_list'] 		= $attributes_model->getAttributesData(LEAVE_TYPE);

		$this->load_page('administration/master_file/employee_profile/index');
	}

	public function add(){
		$this->breadcrumb[] = array(		'html'	=>	'Employee Profile',
											'url'	=>	'administration/master_file/employee_profile');
		$this->breadcrumb[] = array(		'html'	=>	'Add',
											'url'	=>	'');
		$this->data['type']   = 'add';
		$this->data['header'] = $this->getData($this->data['type'], '');

		// LOAD NEEDED DATA
		
		$this->load_page('administration/master_file/employee_profile/form');
	}

	public function update(){
		$this->breadcrumb[] = array(		'html'	=>	'Employee Profile',
											'url'	=>	'administration/master_file/employee_profile');
		$this->breadcrumb[] = array(		'html'	=>	'Update',
											'url'	=>	'');
		$this->data['type']   = 'update';
		$this->data['id'] 	  = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['id']);

		// LOAD NEEDED DATA
		
		$this->load_page('administration/master_file/employee_profile/form');
	}

	public function getData($type, $docno = ''){
		
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
		$attributes_model 		= $this->load_model('administration/application_setup/general/attributes/attributes_model');
		$noseries_model = $this->load_model('administration/master_file/bom/noseries_model');

		$output = array();

		if($type == 'add'){
			if($this->data['config']['103012-001'] == 1){
				$docno = $noseries_model->getNextAvailableNumber($this->number_series, false, '');
			}

			$output = array(

				'Emp_Status' 				=> 'Open',
			   	'Emp_Id' 					=> $docno, 
		       	'Emp_Title' 				=> '', 
		       	'Emp_FirstName' 			=> '', 
		       	'Emp_MiddleName' 			=> '', 
		       	'Emp_LastName' 				=> '', 
		       	'Emp_NickName' 				=> '', 
		       	'Emp_PositionId' 			=> '', 
		       	'Emp_CompanyId' 			=> '', 
		       	'Emp_TaxExemptId' 			=> '', 
		       	'Emp_FK_GroupId' 			=> '', 
		       	'Emp_FK_StoreID' 			=> '', 
		       	'Emp_TardyExemp' 			=> '', 
		       	'Emp_PermanentAddress' 		=> '', 
		       	'Emp_PermanentAddressTelNo' => '', 
		       	'Emp_CurrentAddress' => '', 
		       	'Emp_CurrentAddressTelNo' => '', 
		       	'Emp_CurrentAddressMobileNo' => '', 
			    'Emp_PhoneNo' => '', 
			    'Emp_MobileNo' => '', 
			    'Emp_EmailAdd' => '', 
			    'Emp_CivilStatus' => '', 
			    'Emp_Gender' => '', 
			    'Emp_Height' => 0, 
			    'Emp_Weight' => 0, 
			    'Emp_FatherName' => '', 
			    'Emp_FatherOccupation' => '', 
			    'Emp_FatherCompany' => '', 
			    'Emp_FatherContact' => '', 
			    'Emp_MotherName' => '', 
			    'Emp_MotherOccupation' => '', 
			    'Emp_MotherContact' => '', 
			    'Emp_MotherCompany' => '', 
			    'Emp_SpouseName' => '', 
			    'Emp_SpouseOccupation' => '', 
			    'Emp_SpouseContact' => '', 
			    'Emp_SpouseCompany' => '', 
			    'Emp_SpouseAge' => '', 
			    'Emp_Religion' => '', 
			    'Emp_Nationality' => '', 
			    'Emp_Birthdate' => '', 
			    'Emp_PlaceofBirth' => '', 
			    'Emp_SSSNum' => '', 
			    'Emp_TINNum' => '', 
			    'Emp_PagIbigNum' => '', 
			    'Emp_PhilHealthNum' => '', 
			    'Emp_DateHired' => '', 
			    'Emp_DateResign' => '', 
			    'Emp_BaseRate' => '', 
			    'Emp_BasicRate' => 0, 
			    'Emp_BankAccountNum' => '', 
			    'Emp_GracePeriod' => 0, 
			    'Emp_NonTaxAllowance' => '', 
			    'Emp_CheckBreak' => '', 
			    'Emp_AgencyEmployee' => '', 
			    'Emp_ECOLA' => '', 
			    'Emp_BarcodeID' => '', 
			    'Emp_CustomerId' => '', 
			    'Emp_CompressSched' => '', 
			    'Emp_BarcodeExpiry' => '', 
			    'Emp_AllowSpecialHoliday' => '', 
			    'Emp_ContactPerson' => '', 
			    'Emp_ContactNumber' => '', 
			    'Emp_ContactAddress' => '', 
			    'Emp_ContactRelationship' => '', 
			    'Emp_OwnedSavings' => '', 
			    'Emp_ComputeSSS' => '', 
			    'Emp_ComputeTax' => '', 
			    'Emp_ComputePagIbig' => '', 
			    'Emp_ComputePhilHealth' => '', 
			    'Emp_HoldPayroll' => '', 
			    'Emp_PictureID' => '', 
			    'Emp_Hobbies' => '', 
			    'Emp_ProfLicense' => '', 
			    'Emp_ProfLicenseDesc' => '', 
			    'Emp_Active' => '', 
			    'Emp_RecordChange' => '', 
			    'Emp_PersonalId' => '', 
			    'Emp_FixedSSSEEAmt' => 0, 
			    'Emp_FixedSSSERAmt' => 0, 
			    'Emp_FixedPagibigEEAmt' => 0, 
			    'Emp_FixedPagibigERAmt' => 0, 
			    'Emp_FixedPHealthEEAmt' => 0, 
			    'Emp_FixedPHealthERAmt' => 0, 
			    'Emp_FixedECAmt' => 0, 
			    'Emp_DepartmentId' => '', 
			    'Emp_PayrollStart' => '', 
			    'Emp_Manual' => '', 
			    'Emp_LastPay' => '', 
			    'Emp_FK_Status_Attribute_id' => '', 
			    'Emp_FK_ApplicanId' => '', 
			    'Emp_IdCard' => '', 
			   	'Emp_WithTimeAttendance' => '', 
			    'Emp_CapturedImage' => '', 
			    'Emp_WorkHours' => 0, 
			    'Emp_SavingsFund' => 0, 
			    'Emp_FixedTax' => 0, 
			    'Emp_Month13thBasis' => '', 
			    'CreatedBy' => '', 
			    'ModifiedBy' => '', 
			    'DateCreated' => '', 
			    'DateModified' => '', 
			    'Emp_PayrollMode' => '', 
			    'Emp_AttrBankCode' => '', 
			    'Emp_ClearanceDate' => '', 
			    'Emp_MotherMaidenName' => '', 
			    'Emp_UndertimeExempt' => '', 
			    'Emp_AttendanceType' => '',
			    'E_FirstEval' => '',
			    'E_SecondEval' => '',
			    'E_ThirdEval' => '',
			    
			);

			$output['work_location_list'] = array();
			$output['position_list'] = array();
			$output['paygroup_list'] = array();
			$output['attendance_list'] = array();
		}
		else{

			$where = array(
				'Emp_Id' 	=> $docno 
			);

			$output = $employee_profile_model->getHeaderByDocNo('*', 
																$this->header_table, 
																$where,
																array(
																	array('tbl_name' => 'tblEvaluation','tbl_on' => 'FK_Emp_Id = Emp_Id','tbl_join' => 'left')
																)
			);
			$output['Emp_Name'] = $output['Emp_LastName'] . ', ' . $output['Emp_FirstName'] . ' ' . substr($output['Emp_MiddleName'],1,1); 

			$sql_query = 'SELECT LTRIM(RTRIM(SP_StoreName)) as Description,  LTRIM(RTRIM(SP_ID)) AS Id  
                             From tblStoreProfile  
                             WHERE (SP_FK_CompanyID = \''.$output['Emp_CompanyId'].'\')
                             ORDER BY Description';
			$output['work_location_list'] = $this->db->query($sql_query)->result_array();

			$sql_query = 'SELECT LTRIM(RTRIM(P_Position)) as Description,  LTRIM(RTRIM(P_ID)) AS Id   
					      FROM tblPosition 
         				  WHERE (P_Dept = \''.$output['Emp_DepartmentId'].'\') AND (ISNULL(P_Active, 0) = 1)  
         				  ORDER BY P_Position';

			$output['position_list'] = $this->db->query($sql_query)->result_array();

			$sql_query = 'SELECT LTRIM(RTRIM(G_Desc)) as Description,  LTRIM(RTRIM(G_Id)) AS Id
				         FROM tblPAYGroup 
				         WHERE (G_FK_CompanyId = \''.$output['Emp_CompanyId'].'\')
				         ORDER BY Description';
			$output['paygroup_list'] = $this->db->query($sql_query)->result_array();


			$sql_query 	= " SELECT LTRIM(RTRIM(a.AD_Desc)) as Description,  LTRIM(RTRIM(tr.TR_AttendanceType)) AS Id  ";
			$sql_query .= " FROM tblPAYTimekeepingRules AS tr LEFT OUTER JOIN tblAttributeDetail AS a ON tr.TR_AttendanceType = a.AD_Id ";
			$sql_query .= " WHERE  (tr.TR_PKFK_CompanyId = '".$output['Emp_CompanyId']."') AND (tr.TR_GroupId ='".$output['Emp_FK_GroupId']."') ";
			$output['attendance_list'] = $this->db->query($sql_query)->result_array();

			// format data

			// date
			$output['Emp_Birthdate'] 			= format_fullyear($output['Emp_Birthdate']);
			$output['Emp_DateHired'] 			= format_fullyear($output['Emp_DateHired']);
			$output['Emp_DateResign'] 			= format_fullyear($output['Emp_DateResign']);
			$output['Emp_BarcodeExpiry'] 		= format_fullyear($output['Emp_BarcodeExpiry']);			
			$output['E_FirstEval'] 				= format_fullyear($output['E_FirstEval']);
			$output['E_SecondEval'] 			= format_fullyear($output['E_SecondEval']);
			$output['E_ThirdEval'] 				= format_fullyear($output['E_ThirdEval']);

			// amount
			$output['Emp_GracePeriod'] 			= numeric($output['Emp_GracePeriod']);
			$output['Emp_SavingsFund'] 			= numeric($output['Emp_SavingsFund']);			
			$output['Emp_FixedSSSERAmt'] 		= numeric($output['Emp_FixedSSSERAmt']);
			$output['Emp_FixedSSSEEAmt'] 		= numeric($output['Emp_FixedSSSEEAmt']);
			$output['Emp_FixedECAmt'] 			= numeric($output['Emp_FixedECAmt']);
			$output['Emp_FixedPagibigERAmt'] 	= numeric($output['Emp_FixedPagibigERAmt']);
			$output['Emp_FixedPagibigEEAmt'] 	= numeric($output['Emp_FixedPagibigEEAmt']);
			$output['Emp_FixedPHealthEEAmt'] 	= numeric($output['Emp_FixedPHealthEEAmt']);
			$output['Emp_FixedPHealthERAmt'] 	= numeric($output['Emp_FixedPHealthERAmt']);
			$output['Emp_FixedTax'] 			= numeric($output['Emp_FixedTax']);

			// checkbox
			$output['Emp_ComputeSSS'] 			= ($output['Emp_ComputeSSS'] == '1') ? "checked='checked'" : "";
			$output['Emp_ComputeTax'] 			= ($output['Emp_ComputeTax'] == '1') ? "checked='checked'" : "";
			$output['Emp_ComputePagIbig'] 		= ($output['Emp_ComputePagIbig'] == '1') ? "checked='checked'" : "";
			$output['Emp_ComputePhilHealth'] 	= ($output['Emp_ComputePhilHealth'] == '1') ? "checked='checked'" : "";
			$output['Emp_HoldPayroll'] 			= ($output['Emp_HoldPayroll'] == '1') ? "checked='checked'" : "";
			$output['Emp_WithTimeAttendance'] 	= ($output['Emp_WithTimeAttendance'] == '1') ? "checked='checked'" : "";
			$output['Emp_AgencyEmployee'] 		= ($output['Emp_AgencyEmployee'] == '1') ? "checked='checked'" : "";
			$output['Emp_LastPay'] 				= ($output['Emp_LastPay'] == '1') ? "checked='checked'" : "";
			$output['Emp_Active'] 				= ($output['Emp_Active'] == '1') ? "checked='checked'" : "";
			$output['Emp_TardyExemp'] 			= ($output['Emp_TardyExemp'] == '1') ? "checked='checked'" : "";
			$output['Emp_UndertimeExempt'] 		= ($output['Emp_UndertimeExempt'] == '1') ? "checked='checked'" : "";
			$output['Emp_Month13thBasis'] 		= ($output['Emp_Month13thBasis'] == '1') ? "checked='checked'" : "";
		}

		// combo box data
		$output['company_list'] = $this->db->select('COM_Id,LTRIM(RTRIM(COM_Name)) AS COM_Name',false)
											->where('COM_Active','1')
											->order_by('COM_Name')
											->get('tblCompany')
											->result_array();

		$output['principal_list'] = $this->db->select('C_Id,LOWER(LTRIM(RTRIM(C_Name))) AS C_Name',false)
											->where('C_Active','1')
											->order_by('C_Name')
											->get('tblCustomer')
											->result_array();	

		$output['taxExcempt_list'] = $this->db->select('TC_ID,LOWER(LTRIM(RTRIM(TC_ExemtDesc))) AS TC_ExemtDesc',false)
											->where('TC_Active','1')
											->order_by('TC_ExemtDesc')
											->get('tblTaxCategory')
											->result_array();


													

		$output['title_list'] = array(
										'Mr.' 	=> 'Mr.',
										'Ms.' 	=> 'Ms.',
										'Mrs.' 	=> 'Mrs.',
		);	
		
		$output['gender_list'] 	= array(
										'M'=>'Male',
										'F'=>'Female'
		);

		$output['baseRate_list'] 	= array(
										'D'=>'Daily',
										'M'=>'Monthly'
		);

		$output['marital_list'] 	= array(
										'S'=>'Single',
										'M'=>'Married',
										'W'=>'Widowed', 
										'D'=>'Divorced', 
										'S1'=>'Separated'
		);

		$output['payrollmode_list'] = array(
										'Bank' => 'Bank',
										'Cash' => 'Cash',
										'Check' => 'Check'
		);

		$output['religion_list'] 		= $attributes_model->getAttributesData(RELIGION);
		$output['relationship_list'] 	= $attributes_model->getAttributesData(RELATION);
		$output['employee_status_list'] = $attributes_model->getAttributesData(WORK_STATUS);
		$output['bank_list'] 			= $attributes_model->getAttributesData(BANK);
		$output['department_list'] 		= $attributes_model->getAttributesData(DEPARTMENT);

		return $output;

	}

	public function data(){

		$get_data = $this->input->get();
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data($get_data);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');
			$button .= '<button class="btn btn-primary btn-xs btn-outline update-header" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['Emp_Id'].'" data-link="'.DOMAIN.'administration/master_file/employee_profile/update?id='.md5($value['Emp_Id']).'" data-record="'.$json_data.'">
								<i class="fa fa-pencil"></i>
								</button> ';

			$sub_array[] = $button;										
			$sub_array[] = '<span class="employee_data" data-employee-id="'.md5($value['Emp_Id']).'" data-employee="'.$value['Emp_Id'].'">'.$value['Emp_Name'].'</span>';

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_IdHistory(){
		
		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_id_history($employee_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
			$sub_array[] = $button;										
			$sub_array[] = $value['EID_LineNo'];
			$sub_array[] = $value['EID_BarcodeId'];
			$sub_array[] = $value['EID_BiometrixNo'];
			$sub_array[] = $value['CreatedBy'];
			$sub_array[] = date_format(date_create($value['DateCreated']),'m/d/Y');

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_Allowance(){

		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_allowance($employee_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');
			$button .= '<button class="btn btn-primary btn-xs btn-outline allowance-edit" data-toggle="tooltip" data-placement="bottom" title="Update" data-employee-id="'.$value['FK_Emp_Id'].'" data-allow-id="'.$value['Allow_Id'].'" data-record="'.$json_data.'">
								<i class="fa fa-pencil"></i>
								</button> ';

			$button .= '<button class="btn btn-danger btn-xs btn-outline allowance-delete" data-toggle="tooltip" data-placement="bottom" title="Delete" data-employee-id="'.$value['FK_Emp_Id'].'" data-allow-id="'.$value['Allow_Id'].'" data-record="'.$json_data.'">
								<i class="fa fa-trash"></i>
								</button> ';

			$sub_array[] = $button;										
			$sub_array[] = $value['PI_Desc'];
			$sub_array[] = number_format($value['Allow_Amount'],2);
			$sub_array[] = ($value['PI_Taxable'] != '1') ? '<input type="checkbox" disabled>' : '<input type="checkbox" disabled checked>';
			$sub_array[] = $value['AD_Desc'];
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_Siblings(){

		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_siblings($employee_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');
			$button .= '<button class="btn btn-primary btn-xs btn-outline sibling-edit" data-toggle="tooltip" data-placement="bottom" title="Update" data-employee-id="'.$value['R_EmpId'].'" data-id="'.$value['R_RecordNum'].'" data-record="'.$json_data.'">
								<i class="fa fa-pencil"></i>
								</button> ';

			$button .= '<button class="btn btn-danger btn-xs btn-outline sibling-delete" data-toggle="tooltip" data-placement="bottom" title="Delete" data-employee-id="'.$value['R_EmpId'].'" data-id="'.$value['R_RecordNum'].'" data-record="'.$json_data.'">
								<i class="fa fa-trash"></i>
								</button> ';

			$sub_array[] = $button;										
			$sub_array[] = $value['R_Name'];
			$sub_array[] = format_fullyear($value['R_Birthdate']);
			$sub_array[] = $value['Status'];
			$sub_array[] = $value['WorkType'];
			$sub_array[] = $value['CompanyAdd'];			
			$sub_array[] = $value['AD_Desc'];			
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_Beneficiaries(){

		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_beneficiaries($employee_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');
			$button .= '<button class="btn btn-primary btn-xs btn-outline beneficiaries-edit" data-toggle="tooltip" data-placement="bottom" title="Update" data-employee-id="'.$value['B_EmpId'].'" data-id="'.$value['B_RecordNum'].'"  data-record="'.$json_data.'">
								<i class="fa fa-pencil"></i>
								</button> ';

			$button .= '<button class="btn btn-danger btn-xs btn-outline beneficiaries-delete" data-toggle="tooltip" data-placement="bottom" title="Delete" data-employee-id="'.$value['B_EmpId'].'" data-id="'.$value['B_RecordNum'].'" data-record="'.$json_data.'">
								<i class="fa fa-trash"></i>
								</button> ';

			$sub_array[] = $button;										
			$sub_array[] = $value['B_Name'];
			$sub_array[] = $value['Status'];
			$sub_array[] = $value['B_WorkType'];
			$sub_array[] = $value['B_CompanyAdd'];	
			$sub_array[] = $value['AD_Desc'];	
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_Education(){

		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_education($employee_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			// $value['EDU_SchoolYear'] = format_year($value['EDU_SchoolYear']);
			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');
			$button .= '<button class="btn btn-primary btn-xs btn-outline education-edit" data-toggle="tooltip" data-placement="bottom" title="Update" data-employee-id="'.$value['EDU_Id'].'" data-id="'.$value['EDU_LineNo'].'"  data-record="'.$json_data.'">
								<i class="fa fa-pencil"></i>
								</button> ';

			$button .= '<button class="btn btn-danger btn-xs btn-outline education-delete" data-toggle="tooltip" data-placement="bottom" title="Delete" data-employee-id="'.$value['EDU_Id'].'" data-id="'.$value['EDU_LineNo'].'" data-record="'.$json_data.'">
								<i class="fa fa-trash"></i>
								</button> ';

			$sub_array[] = $button;										
			$sub_array[] = $value['EDU_SchoolAttended'];
			$sub_array[] = $value['EDU_CourseTaken'];
			$sub_array[] = $value['EDU_SchoolYear'];
			$sub_array[] = $value['EDU_Levels'];	
			$sub_array[] = $value['EDU_HonorReceived'];	
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_Leave(){

		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_leave($employee_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
									
			$sub_array[] = $value['AD_Desc'];
			$sub_array[] = $value['LL_NumDays'];
			$sub_array[] = format_fullyear($value['LL_Applied_Date']);
			$sub_array[] = format_fullyear($value['LL_LeaveFrom']);
			$sub_array[] = format_fullyear($value['LL_LeaveTo']);
			$sub_array[] = ($value['LL_WithApproval'] == '1') ? '<input type="checkbox" checked>' : '<input type="checkbox">';
			$sub_array[] = $value['LL_Remarks'];	
			$sub_array[] = ($value['LL_Initial'] == '1') ? '<input type="checkbox" checked>' : '<input type="checkbox">';
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_LeaveBalance(){

		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_leavebalance($employee_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');
			$button .= '<button class="btn btn-primary btn-xs btn-outline leave-balance-edit" data-toggle="tooltip" data-placement="bottom" title="Update"  data-record="'.$json_data.'">
								<i class="fa fa-pencil"></i>
								</button> ';

			$button .= '<button class="btn btn-danger btn-xs btn-outline leave-balance-delete" data-toggle="tooltip" data-placement="bottom" title="Delete" data-record="'.$json_data.'">
								<i class="fa fa-trash"></i>
								</button> ';

			$runningBalance = $this->db->select('sum(LL_NumDays) AS LL_NumDays')
										->where(array('LL_FK_Emp_id'=> $value['PKFK_Empid'], 'LL_LeaveTypeAttr'=> $value['PKFK_LeaveType']))
										->get('tblLeaveLedger')
										->row_array()['LL_NumDays'];
									
			$sub_array[] = $button;
			$sub_array[] = $value['AD_Desc'];
			$sub_array[] = numeric($value['LT_Balance']);
			$sub_array[] = "<a href='#' class='leave-balance-view' data-record='".$json_data."'>".numeric($runningBalance)." <span class='glyphicon glyphicon-search'></span></a>";
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_LeaveBalanceView(){

		$employee_id = $this->input->get('id');
		$leave_type = $this->input->get('leave_type');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_leavebalanceview($employee_id,$leave_type);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$sub_array[] = $value['LL_DocType'];
			$sub_array[] = $value['LL_DocNo'];
			$sub_array[] = format_fullyear($value['LL_DocDate']);
			$sub_array[] = numeric($value['LL_NumDays']);
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_EmployeeMemo(){

		$employee_id = $this->input->get('id');
		$memo_type = $this->input->get('type');
		
		if($employee_id != '' && $memo_type != ''){

			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
			$table_data = $employee_profile_model->table_data_employeeMemo($employee_id,$memo_type);
			$access_detail = $this->data['access_detail'];

			$data = array();
			foreach ($table_data['data'] as $key => $value) {
				$sub_array = array();
				$sub_array[] = $value['DocNo'];
				$sub_array[] = format_fullyear($value['DocDate']);
				$sub_array[] = format_fullyear($value['AppliedDate']);
				$sub_array[] = $value['Status'];
				$sub_array[] = $value['CreatedBy'];
				$sub_array[] = format_fullyear($value['DateCreated']);
				$data[] = $sub_array;
			}

			$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
	        );  
		}
		else{
			$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     0,  
				"recordsFiltered" =>     0,  
				"data"            =>     array()
	        );  
		}
		

		echo json_encode($output);
	}

	public function data_EmployeeMemoDocuments(){

		$employee_id = $this->input->get('id');
		$leave_type = $this->input->get('leave_type');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_employeeMemoDocuments($employee_id,$leave_type);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$sub_array[] = $value['ED_DocName'];
			$sub_array[] = $value['ED_Remarks'];
			$sub_array[] = '<a href="'.DOMAIN.'employee_documents/'.$value['ED_DocDestination'].'" download data-toggle="tooltip" data-placement="right" title="" data-original-title="Download" class="btn btn-primary btn-xs btn-outline"><span class="glyphicon glyphicon-download"></span></a>';
			$sub_array[] = $value['CreatedBy'];
			$sub_array[] = format_fullyear($value['DateCreated']);
			$sub_array[] = $value['ModifiedBy'];
			$sub_array[] = ($value['DateModified'] != '') ? format_fullyear($value['DateModified']) : '';
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_WorkSchedule(){

		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_workschedule($employee_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
									
			$sub_array[] = $value['WS_DocDate'];
			$sub_array[] = $value['WS_WeekDay'];
			$sub_array[] = $value['WS_FK_Shift_id'];
			$sub_array[] = $value['S_Name'];
			$sub_array[] = $value['S_StartTime'];
			$sub_array[] = $value['S_EndTime'];
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_Notes(){

		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_notes($employee_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');
			$button .= '<button class="btn btn-primary btn-xs btn-outline notes-edit" data-toggle="tooltip" data-placement="bottom" title="Update" data-record="'.$json_data.'">
								<i class="fa fa-pencil"></i>
								</button> ';

			$button .= '<button class="btn btn-danger btn-xs btn-outline notes-delete" data-toggle="tooltip" data-placement="bottom" title="Delete" data-record="'.$json_data.'">
								<i class="fa fa-trash"></i>
								</button> ';
									
			$sub_array[] = $button;
			$sub_array[] = format_fullyear($value['EmpN_Date']);
			$sub_array[] = $value['EmpN_Remarks'];
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_EmployeeMovement(){

		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$table_data = $employee_profile_model->table_data_employeemovement($employee_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();

			$sub_array[] = format_fullyear($value['EM_AppliedDate']);
			$sub_array[] = $value['C_Name'];
			$sub_array[] = $value['P_Position'];
			$sub_array[] = $value['AD_Desc'];
			$sub_array[] = $value['EM_BasicRate'];
			$sub_array[] = $value['EM_BaseRate'];
			$sub_array[] = $value['EM_CashBond'];
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function getEmployeeData(){
		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$output = $employee_profile_model->getAllEmployeeData($employee_id);
		echo json_encode($output);
	}

	public function getEmployeeBasicAmnt(){
		$employee_id = $this->input->get('id');
		$employee_basic_rate = $this->db->select('Emp_BasicRate')->where('Emp_Id',$employee_id)->get('tblEmployee')->row_array()['Emp_BasicRate'];
		echo json_encode(array('Basic_rate' => $employee_basic_rate));
	}

	public function save(){
		
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$noseries_model = $this->load_model('administration/master_file/bom/noseries_model');
			$output = array(
							'success'=>true
						,	'message'=>'');
			
			// set value of data
			$data['Emp_Birthdate']            = ($data['Emp_Birthdate'] != '') ? $data['Emp_Birthdate'] : NULL;
        	$data['Emp_BarcodeExpiry']        = ($data['Emp_BarcodeExpiry'] != '') ? $data['Emp_BarcodeExpiry'] : NULL;
        	$data['Emp_DateHired']            = ($data['Emp_DateHired'] != '') ? $data['Emp_DateHired'] : NULL;
        	$data['Emp_ClearanceDate']        = ($data['Emp_ClearanceDate'] != '') ? $data['Emp_ClearanceDate'] : NULL; 
        	$data['E_FirstEval']              = ($data['E_FirstEval'] != '') ? $data['E_FirstEval'] : NULL; 
        	$data['E_SecondEval']             = ($data['E_SecondEval'] != '') ? $data['E_SecondEval'] : NULL; 
        	$data['E_ThirdEval']              = ($data['E_ThirdEval'] != '') ? $data['E_ThirdEval'] : NULL; 

			$employee_evalution_schedule = array(
				'E_FirstEval' 	=> $data['E_FirstEval'],
			    'E_SecondEval' 	=> $data['E_SecondEval'],
			    'E_ThirdEval' 	=> $data['E_ThirdEval'],
			    'FK_Emp_Id' 	=> $data['Emp_Id']
			);


			unset($output['E_FirstEval']);
			unset($output['E_SecondEval']);
			unset($output['E_ThirdEval']);

			try{
				if($this->input->post('todo') == 'add'){

					$output = $this->error_message(false, true, false, false, $data, $data['Emp_Id'], $this->number_series, '', 'S', 'tblEmployee');

					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table, 
						);

						if($this->data['config']['103012-001']){
							$data['Emp_Id'] = $noseries_model->getNextAvailableNumber($this->number_series, true, '');
						}

						$employee_profile_model->on_save_module(false, $data, $table, 'Emp', $this->number_series, '');

						// additional save for other table
						$this->db->where('FK_Emp_Id',$data['Emp_Id'])
								->delete('tblEvaluation');

						if($employee_evalution_schedule['E_FirstEval'] != ''){
							$employee_evalution_schedule['FK_Emp_Id'] = $data['Emp_Id'];
							$this->db->insert('tblEvaluation',$employee_evalution_schedule);	
						}



						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					
					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);

					$output = $this->error_message(false, true, false, false, $data, $data['Emp_Id'], $this->number_series, '', 'S', 'tblEmployee');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table, 
						);

						$this->db->trans_begin();

						$employee_profile_model->on_update_module($data, $table, 'Emp', $this->number_series, '');

						// additional save for other table
						$this->db->where('FK_Emp_Id',$data['Emp_Id'])
								->delete('tblEvaluation');
						$this->db->insert('tblEvaluation',$employee_evalution_schedule);	


						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['Emp_Id']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
		
	}

	public function saveImageURL(){
		try {
		

			$this->db->trans_begin();

			$data = $this->input->post();

			$new_filename = $data['Emp_Id_Photo'].'.jpg';
			move_uploaded_file($_FILES['flPhoto']['tmp_name'], "./employee_photo/$new_filename");

			// save employee photo path
			$where = array('Emp_Id' => $data['Emp_Id_Photo']);
			$this->db->where($where)
					 ->update('tblEmployee',array('Emp_PictureID'=>$new_filename));

			if($this->db->trans_status() !== False){
				$this->db->trans_commit();
				$this->deleteUserRecordLock_php($data['Emp_Id_Photo']);
			}
			else{
				$this->db->trans_rollback();
			}

			redirect(base_url().'administration/master_file/Employee_profile');


		} catch (Exception $e) {
			$str = $e->getMessage();
			$array_msg = explode(" , ", $str);

			$output = array(
				'success'  	=> false,
				'message'  	=> $array_msg[0],
				'title'  	=> $array_msg[1],
				'type'  	=> $array_msg[2] 
			);

			echo json_encode($output);
		}
	}

	public function saveEmployeeDocument(){
		try {

			$this->db->trans_begin();

			$data = $this->input->post();

			$new_filename = $data['memo_document_name'].'.'.pathinfo($_FILES['flDocument']['name'],PATHINFO_EXTENSION);
			move_uploaded_file($_FILES['flDocument']['tmp_name'], "./employee_documents/$new_filename");

			// save employee photo path
			$insert = array(
				'PKFK_Emp_id' 			=> $data['memo_employee_id'],
				'ED_DocName' 			=> $data['memo_document_name'],
				'ED_Remarks' 			=> $data['memo_document_remarks'],
				'ED_DocDestination' 	=> $new_filename,
				'Createdby' 			=> getCurrentUser()['login-user'],
				'DateCreated' 			=> date_format(date_create(),'m/d/Y'),
			);
			$this->db->insert('tblEmployeeDocs',$insert);

			if($this->db->trans_status() !== False){
				$this->db->trans_commit();
				$this->deleteUserRecordLock_php($data['memo_employee_id']);
			}
			else{
				$this->db->trans_rollback();
			}

			redirect(base_url().'administration/master_file/Employee_profile');
			
		} catch (Exception $e) {
			$str = $e->getMessage();
			$array_msg = explode(" , ", $str);

			$output = array(
				'success'  	=> false,
				'message'  	=> $array_msg[0],
				'title'  	=> $array_msg[1],
				'type'  	=> $array_msg[2] 
			);

			echo json_encode($output);
		}
	}

	public function getFilterData(){
		if (!$this->input->is_ajax_request()) return;
		$filter 		= $_GET['filter-search'];
		$filter_input 	= isset($_GET['filter-input']) ? $_GET['filter-input'] : null;
		$type 			= $_GET['filter-type'];
		$addtl 			= $_GET['filter-addtl'];
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
		$data = $employee_profile_model->getFilteredData($type, $filter, $addtl, $filter_input);
		echo json_encode($data);
	}

	public function getFilterByData(){
		if (!$this->input->is_ajax_request()) return;
		$filter 		= $_GET['filter-search'];
		$filter_input 	= isset($_GET['filter-input']) ? $_GET['filter-input'] : null;
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
		$data = $employee_profile_model->getFilteredByData($filter,$filter_input);
		echo json_encode($data);
	}

	public function getGeneratedBarcode(){
		if (!$this->input->is_ajax_request()) return;
		$employee_id = $this->input->get('id');
		// get last digit
		$lastDigit = $this->db->select('ISNULL(SMCD_LASTDOCREF, 0) AS SMCD_LASTDOCREF',false)
							  ->where('SMCD_CODE','EMP')
							  ->get('tblSysManCtrlDocument')
							  ->row_array()['SMCD_LASTDOCREF'];
		if($lastDigit == 0){
			$lastDigit = 1;
		}

		// update last digit
		$sql_query = 'UPDATE tblSysManCtrlDocument
                 	  SET SMCD_LASTDOCREF = isnull(SMCD_LASTDOCREF,0) + 1 
                 	  WHERE (SMCD_CODE = \'EMP\')';
		$this->db->query($sql_query);

		echo json_encode(date_format(date_create(),'my'). $this->ReturnNonAlpha($employee_id).$lastDigit);
	}
	
	// get only numbers function
	private function ReturnNonAlpha($cString){

		$result = '';
		for ($i=1; $i < strlen($cString); $i++) { 
			if(is_numeric(substr($cString,$i,1))){
				$result .= substr($cString, $i,1);
			}
		}
		return $result;
	}

	// save new ID
	public function saveID(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				

				if($output['success']){

					// validate if barcode id already exist;
					$barcode_validate = $this->db->where('PKFK_EmpId <>',$data['Emp_Id'])
												 ->where('EID_BarcodeId',$data['Emp_BarcodeID'])
												 ->get('tblEmployeeId')
												 ->num_rows();
					if($barcode_validate > 0){
						throw new Exception("Barcode already exist , Information , info");
					}												 

					$this->db->trans_begin();

					$sql_query = 'UPDATE tblEmployee 
				                  SET  Emp_IdCard = \''.$data['Emp_ID_Card'].'\', Emp_BarcodeID = \''.$data['Emp_BarcodeID'].'\' 
				                  WHERE  (Emp_Id = \''.$data['Emp_Id'].'\') AND (Emp_CompanyId= \''.$data['Emp_CompanyId'].'\')';
					$this->db->query($sql_query);

					$history_LineNo = $this->db->select('ISNULL(EID_LineNo,1) AS EID_LineNo', false)
											 ->where('PKFK_EmpId',$data['Emp_Id'])
											 ->order_by('EID_LineNo','DESC')
											 ->get('tblEmployeeId')
											 ->row_array()['EID_LineNo'] + 1;
					
					$array_insert = array(
								'PKFK_EmpId' 			=> $data['Emp_Id'], 
								'EID_LineNo' 			=> $history_LineNo, 
								'EID_NewId' 			=> $data['Emp_ID_Card'],
								'EID_BiometrixNo' 		=> $data['EID_BiometrixNo'], 
								'EID_BarcodeId' 		=> $data['Emp_BarcodeID'],
								'CreatedBy' 			=> getCurrentUser()['login-user'], 
								'DateCreated' 			=> date_format(date_create(),'m/d/Y')
					);											 

					$this->db->insert('tblEmployeeId',$array_insert);

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function saveFamily(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){

					$where = array(
						'Emp_Id' => $data['Emp_Id']
					);

					$spouseEmployementStatus = $data['ED_SpouseEmploymentStat'];

					unset($data['Emp_Id']);
					unset($data['ED_SpouseEmploymentStat']);
					
					$this->db->trans_begin();

					$this->db->where($where)->update('tblEmployee',$data);

					// check if existing;
					$validate = $this->db->where('ED_Id',$where['Emp_Id'])
										->get('tblEmployeeDetail')
										->num_rows();

					if($validate == 0){
						// save
						$this->db->insert('tblEmployeeDetail',array(
							'ED_SpouseEmploymentStat' => $spouseEmployementStatus,
							'ED_Id'					  => $where['Emp_Id']
						));
					}
					else{
						// update
						$this->db->where('ED_Id',$where['Emp_Id'])
								 ->update('tblEmployeeDetail',array('ED_SpouseEmploymentStat' => $spouseEmployementStatus));
					}

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	// update education record
	public function saveEducation(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){

					$where = array(
						'ED_Id' => $data['Emp_Id']
					);

					$data['ED_Id'] = $data['Emp_Id'];

					unset($data['Emp_Id']);
					unset($data['ED_SpouseEmploymentStat']);
					
					$this->db->trans_begin();

					// check if existing;
					$validate = $this->db->where($where)
										->get('tblEmployeeDetail')
										->num_rows();

					if($validate == 0){
						// save
						$this->db->insert('tblEmployeeDetail',$data);
					}
					else{
						unset($data['ED_Id']);
						// update
						$this->db->where($where)->update('tblEmployeeDetail',$data);
					}

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function getFamilyData(){
		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$output = $employee_profile_model->getAllFamilyData($employee_id);
		echo json_encode($output);		
	}

	public function getEducationData(){
		$employee_id = $this->input->get('id');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$output = $employee_profile_model->getAllEducationData($employee_id);
		echo json_encode($output);	
	}

	public function getAllowanceDetailData(){
		$employee_id = $this->input->get('id');
		$payitem_id = $this->input->get('paymentId');
		$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');		
		$output = $employee_profile_model->getAllAllowanceData($employee_id,$payitem_id);
		echo json_encode($output);
	}

	// save or update siblings record
	public function saveFamilySiblings(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{				

				if($output['success']){
										
					$this->db->trans_begin();

					$employee_profile_model->saveSiblingsRecord($data);					

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	// save or update beneficiaries record
	public function saveFamilyBeneficiaries(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{

				if($output['success']){
					
					$this->db->trans_begin();
					$employee_profile_model->saveBeneficiariesRecord($data);	

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	// save or update Allowance record
	public function saveAllowanceRecord(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){
					
					$this->db->trans_begin();
					$employee_profile_model->saveAllowanceDataRecord($data);	

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	// save or update Leave Balance record
	public function saveLeaveBalanceRecord(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){
					
					$this->db->trans_begin();
					$employee_profile_model->saveLeaveBalanceDataRecord($data);	

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	// delete record of leave balance and leave ledger
	public function deleteLeaveBalance(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){
					
					$this->db->trans_begin();

					$this->db->where("PKFK_Empid",$data['PKFK_Empid'])
							->where("PKFK_LeaveType",$data['PKFK_LeaveType'])
							->delete('tblLeaveBalance');

					$this->db->where("LL_FK_Emp_id",$data['PKFK_Empid'])
							->where("LL_LeaveTypeAttr",$data['PKFK_LeaveType'])
							->where("LL_DocType",'BEGBAL')
							->delete('tblLeaveLedger');

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function deleteSiblings(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){
					
					$this->db->trans_begin();

					$this->db->where('R_EmpId',$data['R_EmpId'])
							->where('R_RecordNum',$data['R_RecordNum'])
							->delete('tblEmployeeRelative');

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function deleteBeneficiaries(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){
					
					$this->db->trans_begin();

					$this->db->where('B_EmpId',$data['B_EmpId'])
							->where('B_RecordNum',$data['B_RecordNum'])
							->delete('tblEmployeeBeneficiaries');

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	// delete record of Education Details
	public function deleteEducationChild(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){
					
					$this->db->trans_begin();

					$this->db->where('EDU_Id',$data['EDU_Id'])
							->where('EDU_LineNo',$data['EDU_LineNo'])
							->delete('tblEmployeeEducation');

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}
	

	// save education child
	public function saveEducationChild(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{				

				if($output['success']){
										
					$this->db->trans_begin();

					$employee_profile_model->saveEducationChildRecord($data);					

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	// delete allowance record
	public function deleteAllowanceRecord(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){
					
					$this->db->trans_begin();

					$this->db->where('Allow_Id',$data['Allow_Id'])
							->where('FK_Emp_Id',$data['FK_Emp_Id'])
							->delete('tblAllowance');

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function updateHiredRecord(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){
					
					$this->db->trans_begin();

					$where 	= array('Emp_Id'=>$data['Emp_Id']);
					$mode 	= $data['mode'];

					unset($data['mode']);
	    			unset($data['Emp_Id']);

					if($mode === true || $mode === 1 || $mode === -1){
        				
        				$data['Emp_Active'] = '1';
        				$data['Emp_PayrollStart'] = ($data['Emp_PayrollStart'] == '') ? NULL : $data['Emp_PayrollStart'];
        				$this->db->where($where)->update('tblEmployee',$data);

					}
					else{
        				
        				$data['Emp_Active'] = '1';
        				$data['Emp_Status'] = 'Hired';
        				$data['Emp_DateResign'] = NULL;
        				$data['Emp_PayrollStart'] = ($data['Emp_PayrollStart'] == '') ? NULL : $data['Emp_PayrollStart'];
        				$this->db->where($where)->update('tblEmployee',$data);
					}

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function updateResignRecord(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){
					
					$this->db->trans_begin();

					$where 	= array('Emp_Id'=>$data['Emp_Id']);
					$data['Emp_DateHired'] 		= NULL;
					$data['Emp_PayrollStart'] 	= NULL;
					$data['Emp_RecordChange'] 	= 1;
					$data['Emp_Status'] 		= 'Resign';
					$data['ModifiedBy'] 		= getCurrentUser()['login-user'];
					$data['DateModified'] 		= date_format(date_create(),'m/d/Y');
					$this->db->where($where)->update('tblEmployee',$data);


					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	// save or update Notes record
	public function saveNotesRecord(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){
					
					$this->db->trans_begin();
					$employee_profile_model->saveNotesDataRecord($data);

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
						// throw new Exception("Error, Conctact your System Admin, error");
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function deleteNotesRecord(){
		if(!empty($_POST)):

			// Get all POST Data
			$data 				= $this->input->post();
			$employee_profile_model = $this->load_model('/administration/master_file/employee_profile/employee_profile_model');
			$output = array(
							'success'=>true
						,	'message'=>'');

			try{
				

				if($output['success']){
					
					$this->db->trans_begin();

					$this->db->where('EmpN_Id',$data['EmpN_Id'])
							->where('EmpN_RecNo',$data['EmpN_RecNo'])
							->delete('tblEmployeeNotes');

					if($this->db->trans_status() !== False){
						$this->db->trans_commit();
					}
					else{
						$this->db->trans_rollback();
					}
				}
				else{
					throw new Exception($output['message']);
				}

				
				/* 
						END OF SAVING - ADD FUNCTION
				*/
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

}