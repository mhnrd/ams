<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Department extends MAIN_Controller {

	protected $number_series		=	1217;
	protected $header_table			=	'tblDepartmentClass';
	protected $header_doc_no_field	=	'DEP_Id';
	protected $header_status_field	=	'DEP_Active';
	protected $module_name			=	'Department';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> '');
		$this->breadcrumb[] = array(		'html'	=>	'Master File',
											'url'	=>	'');

		$this->data['header_table']			=	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						=   array('DEP_Id','DEP_Description','DEP_Active');
		
	}

	public function index(){
		$this->breadcrumb[] = array(		'html'	=>	'Department',
											'url'	=>	'administration/master_file/department');

		//TABLE HEADER
		$this->data['table_hdr'] = array('department' => array(
												'buttons' 		=> array(
																			'title'  	=> '<a class="btn btn-success btn-xs
																			                btn-outline" data-toggle="tooltip"
																			                data-placement="top" 
																							title="Add" type="button" href="'.
																							DOMAIN.'administration/master_file/department/add"><i class="fa
																							fa-plus"></i></a>',
																			'class'  	=> 'text-center',
																			'style'  	=> 'width: 5%; vertical-align: middle' 
																		),
												'input' 		=> array(
																			'title' 	=> '<input type="checkbox"
																			 				id="chkSelectAll" 
																							class="icheckbox_square-green"',
																			'class'  	=> 'text-center',
																			'style' 	=> 'width: 10%; vertical-align: middle'
																		),
												'department_id' => array(
																			'title'  	=> 'Department ID',
																			'class'  	=> 'text-center',
																			'style'  	=> 'width: 10%; vertical-align: middle' 
																		),
												'description' 	=> array(
																			'title'  	=> 'Description',
																			'class' 	=> 'text-center',
																			'style' 	=> 'width: 10%; vertical-align: middle' 
																		),
												'status' 		=> array(
																			'title' 	=> 'Status',
																			'class'  	=> 'text-center',
																			'style' 	=> 'width: 10%; vertical-align: middle' 

																		), 
														)
										);

		$this->data['hdr_center'] = array(0,1,2,4);
		
		$this->data['detail'] 	  = $this->generate_detail_table();

		$this->load_page('administration/master_file/department/index');
	}

	public function generate_detail_table(){
		$dtl_data[] = '';

		// TABLE DETAIL
		$dtl_data['table_dtl'] = array('department_detail' => array(
												'buttons' 				=> array(
																			'title'  	=> '<a
																			 				class="btn btn-success btn-xs
																			                btn-outline add-position-setup" data-toggle="tooltip"
																			                data-placement="top" 
																							title="Add" type="button"><i class="fa
																							fa-plus"></i></a>',
																			'class'  	=> 'text-center',
																			'style'  	=> 'width: 5%; vertical-align: middle' 
																				),
												'position' 				=>	array(
																					'title' 	=> 'Position',
																					'class' 	=> 'text-center',
																					'style'  	=> 'width: 10%; vertical-align: middle' 
																				),
												'position_id' 			=>	array(
																					'title'  	=> 'Psition ID',
																					'class'  	=> 'text-center',
																					'style'  	=> 'width: 10%; vertical-align: middle' 
																				),
																)

										);
		$dtl_data['dtl_center'] = array(0,1,2);

		return $dtl_data;

		//$this->load_page('administration/master_file/department/position_setup_template');
	} 

	// public function detail_template(){

	// 	$this->data['type']		= 'detail_template';
	// 	$this->load_page('administration/master_file/department/position_setup_template');
	// }

	public function add(){
		$this->breadcrumb[] = array(		'html'	=>	'Department',
											'url'	=>	'administration/master_file/department');
		$this->breadcrumb[]	= array(		'html'	=>	'Add',
											'url'	=>	'');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type']		= 'add';
		$this->data['header'] 	= $this->getData($this->data['type'], '');

		$this->load_page('administration/master_file/department/form');
	}

	public function update(){
		$this->breadcrumb[] = array(		'html'	=>	'Department',
											'url'	=>	'administration/master_file/department');
		$this->breadcrumb[]	= array(		'html'	=>	'Update',
											'url'	=>	'');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type']		= 'update';
		$this->data['id'] 		= $_GET['id'];
		$this->data['header'] 	= $this->getData($this->data['type'], $this->data['id']);

		$this->load_page('administration/master_file/department/form');
	}

	public function view(){
		$this->breadcrumb[] = array(		'html'	=>	'Department',
											'url'	=>	'administration/master_file/department');
		$this->breadcrumb[]	= array(		'html'	=>	'View',
											'url'	=>	'');

		// STRUCTURE FOR VIEW PAGE
		$this->data['type']		= 'view';
		$this->data['id']		= $_GET['id'];
		$this->data['header'] 	= $this->getData($this->data['type'], $this->data['id']);

		$this->load_page('administration/master_file/department/form');
	}

	public function getData($type, $docno = ''){

		$department_model = $this->load_model('administration/master_file/department/department_model');

		if($type == 'add'){
			$output = array(
				'DEP_Id' 			=>	'',
				'DEP_Description' 	=>	'', 
				'DEP_Active' 		=>	1,
			);
		}
		else if($type == 'update' || $type == 'view'){

			$where = array(
				'DEP_Id' 	=> $docno 
			);

			$header = $department_model->getHeaderByDocNo($this->cols, $this->header_table, $where);

			$output = array(
				'DEP_Id' 				=> $header['DEP_Id'],
				'DEP_Description' 		=> $header['DEP_Description'], 
				'DEP_Active' 			=> $header['DEP_Active'],
				'functions' 			=> $this->getApprovalButtons($header['DEP_Id']
																,$header['DEP_Description'] 
																,$header['DEP_Active']

																,getCurrentUser()['login-user']
																,$this->header_table
																,$this->header_status_field
																,$this->header_doc_no_field
															),		
			);

			if($output['DEP_Active'] == 'Approved'){
				unset($output['functions'][1]);
			}
		}

		return $output;
	}

	public function data_D(){

		$department_class_model = $this->load_model('/administration/master_file/department/department_model');		
		$table_data = $department_class_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/master_file/department/view?id='.md5($value['DEP_Id']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/master_file/department/update?id='.md5($value['DEP_Id']).'">
									<i class="fa fa-pencil"></i>
								</a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<a class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['DEP_Id'].'"><i class="fa fa-trash"></i></a> ';
			//}

			$sub_array[] = $button;										
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['DEP_Id'].'">';
			$sub_array[] = $value['DEP_Id'];
			$sub_array[] = $value['DEP_Description'];
			$sub_array[] = ($value['DEP_Active'] == 1) ? "Active" : "Inactive";

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_DSD(){

		$ds_id = $this->input->get('id');
		$department_setup_model = $this->load_model('/administration/master_file/department/department_model');	

		$table_data = $department_setup_model->table_data_DSD($ds_id);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//$sub_array[] = $button;										
			$sub_array[] = $value['DS_FK_Department_id'];
			$sub_array[] = $value['DS_FK_Position_id'];
			
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;

		if(!empty($_POST)):

			// GET ALL POST data
			$data = $this->input->post();

			// LOAD ALL NEEDED MODEL
			$department_model = $this->load_model('administration/master_file/department/department_model');

			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];

			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){

					$output = $this->error_message(false, true, false, false, $data, $data['DEP_Id'], $this->number_series, '', 'DEP', 'tblDepartmentClass');
					
					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table,
							'details' 	=> 'tblDepartmentSetup' 
						);

						$this->db->trans_begin();

						$department_model->on_save_module(false, $data, $table, 'DEP', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}
					/* 
							END OF SAVING - ADD FUNCTION
					*/
			
				// UPDATE HEADER
				}
				else if($this->input->post('todo') == 'update'){
					
					$output = $this->error_message(false, true, false, false, $data, $data['DEP_Id'], $this->number_series, '', 'DEP', 'tblDepartmentClass');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,
							'details' 	=> 'tblDepartmentSetup' 

						);

						$this->db->trans_begin();

						$department_model->on_update_module($data, $table, 'DEP', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['DEP_Id']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
				}
				
				echo json_encode($output); //ENDING QUERY
			} 
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function delete(){
		$doc_no = $this->input->get();
		$department_class_model = $this->load_model('administration/master_file/department/department_model');
		echo json_encode($department_class_model->delete_data($doc_no['id']));
	}

	public function action(){
		$type = $this->input->post('action');
		$checked_list = $this->input->post('id');
		$department_class_model   = $this->load_model('administration/master_file/department/department_model');
		echo json_encode($department_class_model->status_data($checked_list,$type));
	}


}