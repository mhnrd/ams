<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Supplier extends MAIN_Controller {

	protected $number_series		=  1606;
	protected $header_table 		= 'tblSupplier';
	protected $header_doc_no_field	= 'S_DocNo';
	protected $header_status_field	= 'S_Active';
	protected $module_name 			= 'Supplier';

	public function __construct(){
		parent::__construct();		
		$this->is_secure 	= true;
		$this->breadcrumb[] = array(	'html'	=> 'Master Files',
										'url'	=> '');
		$this->data['header_table'] 			= $this->header_table;
		$this->data['header_doc_no_field'] 		= $this->header_doc_no_field;
		$this->data['header_status_field'] 		= $this->header_status_field;
		$this->data['number_series'] 			= $this->number_series;
		$this->data['access_header']			= $this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']			= $this->user_model->getDetailAccess($this->number_series);
		$this->cols = array('S_DocNo','S_Name','S_Address','S_Country','S_Addr_PostalCode','S_SupplierType','S_FK_PayTerms','S_FK_Attribute_Currency_id','S_BankName','S_BankAddress','S_PrintCheckAs','S_CreditLimit','S_BalanceAsOf','S_Contact','S_TelNum','S_FaxNUm','S_EmailAdd1','S_EmailAdd2','S_SwiftCode','S_SupplierPostingGroup','S_WHT_PostingGroup','S_Vat_PostingGroup','S_TinNum','S_BankAccountNo','S_Status','S_Active');
		$this->table = 'tblSupplier as G';	
		
	}

	//Header Of the Table
	public function index(){
		//$this->user_model->deleteUserRecordLock(getCurrentUser()['login-user']); // Delete User Record Lock
		$this->breadcrumb[] = array (	'html'	=>  'Supplier',
										'url'	=>	'');

		//$supplier_model = $this->load_model('master_file/supplier/supplier_model');
		$this->data['table_hdr'] = array('supplier-details'	=> array(
										'button' 					=> array(
												 								'title' => '<a class="btn btn-success btn-xs btn-outline add-large" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="'.DOMAIN.'master_file/supplier/add" role="button"><i class="fa fa-plus"></i></a>',
																				'class' => 'text-center',
																				'style' => 'width: 5%; vertical-align: middle' 
																			),
										 'checkbox' 			    => array(
																				'title' => '<input type="checkbox" id="chkSelectAll" class="icheckbox_square-green">',
																				'class' => 'text-center',
																				'style' => 'width: 5%; vertical-align: middle'
																			),
										 's-id' 					=> array(
										 										'title' => 'Supplier ID',
										 										'class' => 'text-center',
										 										'style' => 'width: 5%; vertical-align:middle'
										 									),
										 's-name' 					=> array(
										 										'title' => 'Name',
										 										'class' => 'text-center',
										 										'style' => 'width: 5%; vertical-align:middle'
										 									), 
										 's-address' 				=> array(
										 										'title' => 'Address',
										 										'class' => 'text-center',
										 										'style' => 'width: 5%; vertical-align:middle'
										 									), 
										 'tel-no' 					=> array(
										 										'title' => 'Telephone No.',
										 										'class' => 'text-center',
										 										'style' => 'width: 5%; vertical-align:middle'
										 									), 
										 'tin-no' 					=> array(
										 										'title' => 'TIN No',
										 										'class' => 'text-center',
										 										'style' => 'width: 5%; vertical-align:middle'
										 									),
										 'active' 					=> array(
										 										'title' => 'Active',
										 										'class' => 'text-center',
										 										'style' => 'width: 5%; vertical-align:middle'
										 									), 
		)
	);
		$this->data['hdr_center'] = array(0,1,2,3,4,5,6,7);
		$this->load_page('master_file/supplier/index');
	}


	// For Adding user
	public function add(){
		$this->breadcrumb[] 						= array (	'html'	=>	'Supplier',
																'url'	=>	'master_file/supplier');
		$this->breadcrumb[]							= array (	'html'	=>	'Add',
																'url'	=>	'');
		//Load Needed Data
		$noseries_model 		 	 				= $this->load_model('master_file/bom/noseries_model');
		$s_series 					 				= $noseries_model->getNextAvailableNumber($this->number_series, false,'');
		$s_supplierpostinggroup_list 				= $this->load_model('master_file/supplier/supplier_model');
		$s_wht_postinggroup_list 	 				= $this->load_model('master_file/supplier/supplier_model');
		$s_vat_postinggroup_list 	 				= $this->load_model('master_file/supplier/supplier_model');
		$s_fk_payterm_list			 				= $this->load_model('master_file/supplier/supplier_model');	
		$currency_list								= $this->load_model('master_file/supplier/supplier_model');
		$suppliertype_list							= $this->load_model('master_file/supplier/supplier_model');	

		//structure for view page
		$this->data['type']							= 'add';
		$this->data['docno']						= $s_series;
		$this->data['s_supplierpostinggroup_list']	= $s_supplierpostinggroup_list->getActiveSupplierPostingGroup();
		$this->data['s_wht_postinggroup_list']		= $s_wht_postinggroup_list->getActiveWhtPostingGroup();
		$this->data['s_vat_postinggroup_list']		= $s_vat_postinggroup_list->getActiveVatPostingGroup();
		$this->data['s_fk_payterm_list']			= $s_fk_payterm_list->getActiveFkPaymentTerms();	
		$this->data['currency_list']				= $currency_list->getActiveCurrency();
		$this->data['suppliertype_list']			= $suppliertype_list->getSupplierType();
		$this->data['header']						= $this->getData($this->data['type']);
		
		$this->load_page('/master_file/supplier/form');
	}

	// For Updating Users Data
	public function update(){
		$this->breadcrumb[] 						= array (	'html'	=>	'Supplier',
																'url'	=>	'master_file/supplier');
		$this->breadcrumb[] 						= array (	'html'	=>	'Update',
																'url'	=>	'');
		// Load Needed Data
		$noseries_model 		  	 				= $this->load_model('master_file/bom/noseries_model');
		$supplier_model 		 	 				= $this->load_model('master_file/supplier/supplier_model');
		$s_series 					 				= $noseries_model->getNextAvailableNumber($this->number_series, false,'');
		$s_supplierpostinggroup_list 				= $this->load_model('master_file/supplier/supplier_model');
		$s_wht_postinggroup_list 	 				= $this->load_model('master_file/supplier/supplier_model');
		$s_vat_postinggroup_list 	 				= $this->load_model('master_file/supplier/supplier_model');
		$s_fk_payterm_list			 				= $this->load_model('master_file/supplier/supplier_model');
		$currency_list								= $this->load_model('master_file/supplier/supplier_model');
		$suppliertype_list							= $this->load_model('master_file/supplier/supplier_model');	



		$this->data['type'] 	  					= 'update';
		$this->data['docno'] 	  					= $_GET['id'];
		//$this->data['item_list']  				= $item_model->getAll();
		$this->data['s_supplierpostinggroup_list']	= $s_supplierpostinggroup_list->getActiveSupplierPostingGroup();
		$this->data['s_wht_postinggroup_list']		= $s_wht_postinggroup_list->getActiveWhtPostingGroup();
		$this->data['s_vat_postinggroup_list']		= $s_vat_postinggroup_list->getActiveVatPostingGroup();
		$this->data['s_fk_payterm_list']			= $s_fk_payterm_list->getActiveFkPaymentTerms();
		$this->data['currency_list']				= $currency_list->getActiveCurrency();
		$this->data['suppliertype_list']			= $suppliertype_list->getSupplierType();
		$this->data['header'] 	  					= $this->getData($this->data['type'], $this->data['docno']);
		$this->load_page('/master_file/supplier/form');
	}

	// For Viewing Data of user
	public function	view(){
		$this->breadcrumb[] 						= array (	'html'	=>	'Supplier',
																'url'	=>	'master_file/supplier');
		$this->breadcrumb[] 						= array (	'html'	=>	'View',
																'url'	=>	'');
		$noseries_model 		  					= $this->load_model('master_file/bom/noseries_model');
		$supplier_model 		  					= $this->load_model('master_file/supplier/supplier_model');
		$s_supplierpostinggroup_list 				= $this->load_model('master_file/supplier/supplier_model');
		$s_wht_postinggroup_list 	 				= $this->load_model('master_file/supplier/supplier_model');
		$s_vat_postinggroup_list 	 				= $this->load_model('master_file/supplier/supplier_model');
		$s_fk_payterm_list			 				= $this->load_model('master_file/supplier/supplier_model');
		$currency_list								= $this->load_model('master_file/supplier/supplier_model');
		$suppliertype_list							= $this->load_model('master_file/supplier/supplier_model');

		$this->data['type'] 	  					= 'view';
		$this->data['docno'] 	  					= $_GET['id'];		
		$this->data['s_supplierpostinggroup_list']	= $s_supplierpostinggroup_list->getActiveSupplierPostingGroup();
		$this->data['s_wht_postinggroup_list']		= $s_wht_postinggroup_list->getActiveWhtPostingGroup();
		$this->data['s_vat_postinggroup_list']		= $s_vat_postinggroup_list->getActiveVatPostingGroup();
		$this->data['s_fk_payterm_list']			= $s_fk_payterm_list->getActiveFkPaymentTerms();
		$this->data['currency_list']				= $currency_list->getActiveCurrency();
		$this->data['suppliertype_list']			= $suppliertype_list->getSupplierType();
		$this->data['header'] 	  					= $this->getData($this->data['type'], $this->data['docno']);
		$this->load_page('/master_file/supplier/form');
	}

	public function getData($type, $docno = ''){
		$noseries_model 		  	 	  = $this->load_model('master_file/bom/noseries_model');
		
		$supplier_docno 			 	  = $noseries_model->getNextAvailableNumber($this->number_series, false,'');
		if ($type == 'add') {
			$output = array(
				'S_DocNo'							=> $supplier_docno,
				'S_Name'							=> '',
				'S_Address'							=> '',
				'S_Country'							=> '',
				'S_Addr_PostalCode'					=> '',
				'S_SupplierType'					=> '',
				'S_FK_PayTerms'						=> '',
				'S_FK_Attribute_Currency_id'		=> '',
				'S_BankName'						=> '',
				'S_BankAddress'						=> '',
				'S_PrintCheckAs'					=> '',
				'S_CreditLimit'						=> '',
				'S_BalanceAsOf'						=> '',
				'S_Contact'							=> '',
				'S_TelNum'							=> '',
				'S_FaxNUm'							=> '',
				'S_EmailAdd1'						=> '',
				'S_EmailAdd2'						=> '',
				'S_SwiftCode'						=> '',
				'S_SupplierPostingGroup'			=> '',
				'S_WHT_PostingGroup'				=> '',
				'S_Vat_PostingGroup'				=> '',
				'S_TinNum'							=> '',
				'S_BankAccountNo' 					=> '',
				'S_Status'                 			=> 'Posted',
				'S_Active'                 			=> '1'
			);
		}else  {
			$where = array(
				'S_DocNo'  => $docno
			);
			$supplier_model 	 = $this->load_model('master_file/supplier/supplier_model');
			$header = $supplier_model->getHeaderByDocNo($this->cols, $this->table, $where);
			//$supplier_model       = $this->load_model('master_file/supplier/supplier_model');
			//$header = $supplier_model->getAll($this->data['doc_no']);
			$output = array(
				'S_DocNo' 					=> $header['S_DocNo'],
				//'DateCreated' 		=> date_format(date_create($header['DateCreated']),'m/d/Y'),
				'S_Name' 					=> $header['S_Name'],
				'S_Address' 				=> $header['S_Address'],
				'S_Country' 				=> $header['S_Country'],
				'S_Addr_PostalCode' 		=> $header['S_Addr_PostalCode'],
				//'OF_TotalCost' 				=> (($header['OF_TotalCost'] == "" ? 0 :$header['OF_TotalCost'] )),
				'S_SupplierType' 			=> $header['S_SupplierType'],
				'S_FK_PayTerms' 			=> $header['S_FK_PayTerms'],
				'S_FK_Attribute_Currency_id'=> $header['S_FK_Attribute_Currency_id'],
				'S_BankName' 				=> $header['S_BankName'],
				'S_BankAddress' 			=> $header['S_BankAddress'],
				'S_Contact' 				=> $header['S_Contact'],
				'S_PrintCheckAs' 			=> $header['S_PrintCheckAs'],
				//'S_Country' 				=> $header['S_Country'],
				'S_CreditLimit' 			=> $header['S_CreditLimit'],
				'S_BalanceAsOf' 			=> $header['S_BalanceAsOf'],
				'S_TelNum' 					=> $header['S_TelNum'],
				'S_FaxNUm' 					=> $header['S_FaxNUm'],
				'S_EmailAdd1' 				=> $header['S_EmailAdd1'],
				'S_EmailAdd2' 				=> $header['S_EmailAdd2'],
				'S_SwiftCode' 				=> $header['S_SwiftCode'],
				'S_SupplierPostingGroup' 	=> $header['S_SupplierPostingGroup'],
				'S_WHT_PostingGroup' 		=> $header['S_WHT_PostingGroup'],
				'S_Vat_PostingGroup' 		=> $header['S_Vat_PostingGroup'],
				'S_TinNum' 					=> $header['S_TinNum'],
				'S_BankAccountNo' 			=> $header['S_BankAccountNo'],
				'S_Status' 					=> $header['S_Status'],
				'S_Active' 					=> $header['S_Active'],

				'functions' 				=> $this->getApprovalButtons($header['S_DocNo']
																,$header['S_Status']
																,getCurrentUser()['login-user']
																,$header['S_Name']
																,$this->header_table
																,$this->header_status_field
																,$this->header_doc_no_field
															),
			);

			
		}

		return $output;
	}

	// Datas to be Display in the table
	public function data(){

		$supplier_model = $this->load_model('master_file/supplier/supplier_model');		
		$table_data = $supplier_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" id="btnView" href="'.DOMAIN.'master_file/supplier/view?id='.md5($value['S_DocNo']).'"><i class="fa fa-eye"></i></a> ';
			}

			if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'master_file/supplier/update?id='.md5($value['S_DocNo']).'">
									<i class="fa fa-pencil"></i>
								</a> ';
			}

			if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['S_DocNo'].'" "><i class="fa fa-trash"></i></a> ';
			}

			$sub_array[] = $button;
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['S_DocNo'].'">';
			$sub_array[] = $value['S_DocNo'];
			$sub_array[] = $value['S_Name'];
			$sub_array[] = $value['S_Address'];
			$sub_array[] = $value['S_TelNum'];
			$sub_array[] = $value['S_TinNum'];
			$sub_array[] = ($value['S_Active'] == 1 ) ? "Active" : "Inactive";
			
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){

		if(!$this->input->is_ajax_request()) return;
		

		if(!empty($_POST));

		//Get All POST Data
		$data 				= $this->input->post();

		//Load Needed Data
		$item_model 		= $this->load_model('master_file/bom/item_model');
		$supplier_model 	= $this->load_model('master_file/supplier/supplier_model');

		//Get Current Login User
		$current_user = getCurrentUser()['login-user'];

		$output = array(
						'success' => true
					,	'message' => '');
		try{
			// Start of Saving - Add Function
			if($this->input->post('todo') == 'add'){
					// GET THE LATEST NO SERIES
					$noseries_model = $this->load_model('master_file/bom/noseries_model');
					//$supplier = $noseries_model->getNextAvailableNumber($this->number_series, false, '');
					$output = $this->error_message(true, false, false, false, $data, $data['S_DocNo'], $this->number_series,'','S','tblSupplier');
					
				
				if($output['success']){
						// UPDATE NO SERIES
						// $of_series = $noseries_model->getNextAvailableNumber($this->number_series, true, $data['of-location']);	
						
						// $output['doc_id'] = md5($of_series);

						$table = array(
							'header'  	=> $this->header_table
						);

						$this->db->trans_begin();

						$supplier_model->on_save_module(false, $data, $table, 'S', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);

				$output = $this->error_message(true, false, false, false, $data, $data['S_DocNo'], $this->number_series,'','','tblSupplier');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,
							
						);

						$this->db->trans_begin();

						$supplier_model->on_update_module($data, $table, 'S', $this->number_series,'' );

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['S_DocNo']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				
				echo json_encode($output); //ENDING QUERY
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		
		
	}

	public function delete(){
		$doc_no     	= $this->input->get();
		$supplier_model = $this->load_model('master_file/supplier/supplier_model');
		echo json_encode($supplier_model->delete_data($doc_no['id']));
	}
	public function action(){
		$type             = $this->input->post('action');
		$checked_list 	  = $this->input->post('id');
		$supplier_model   = $this->load_model('master_file/supplier/supplier_model');
		echo json_encode($supplier_model->status_data($checked_list,$type));
	}



}
?>