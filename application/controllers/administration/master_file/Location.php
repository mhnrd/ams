<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Location extends MAIN_Controller{

	protected $number_series		=	103008;
	protected $header_table			=	'tblStoreProfile';
	protected $header_doc_no_field	=	'SP_ID';
	protected $header_status_field	=	'SP_Active';
	protected $module_name 			=	'Location';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> 'administration');
		$this->breadcrumb[] = array(	'html'	=> 'Master File',
										'url'	=> '');

		$this->data['header_table']			=	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						=   array('SP_ID','SP_StoreName','SP_Address','SP_TelNo','SP_FaxNo', 'SP_FK_CompanyID', 'SP_TinNo','SP_Active', 'SP_FK_CPC_id', 'COM_Name', 'CPC_Desc');
		
	}

	public function index(){
		$this->breadcrumb[] = array(		'html'	=>	'Location',
											'url'	=>	'administration/master_file/location');

		$this->data['table_hdr'] = array('location_details'	=> array(
												'buttons' 	=> array(
																		'title' 	=>	'<a class="btn btn-success btn-xs btn-outline" 
																						data-toggle="tooltip" data-placement="top" title="Add" 
																						type="button" href="'.DOMAIN.'administration/master_file/location/
																						add"><i class="fa fa-plus"></i></a>',
																		'class' 	=>	'text-center',
																		'style' 	=>	'width: 100px; vertical-align: middle' 
																	),
												'checkbox' 		=>	array(	
																			'title' 	=>	'<input type="checkbox" id="chkSelectAll" 
																							class="icheckbox_square-green"',
																			'class' 	=>	'text-center',
																			'style' 	=>	'width: 10px; vertical-align: middle' 
																		),
												'location_id' 	=> 	array(
																			'title' 	=>	'Location ID',
																			'class' 	=>	'text-left',
																			'style' 	=>	'width: 10%; vertical-align: middle' 
																	), 
												'location_name' => 	array(
																			'title' 	=>	'Location Name',
																			'class' 	=>	'text-left',
																			'style' 	=>	'width: 20%; vertical-align: middle' 
																		),
												'location_address' => 	array(
																			'title' 	=>	'Location Address',
																			'class' 	=>	'text-left',
																			'style' 	=>	'width: 30%; vertical-align: middle' 
																		),
												'company_name' 	=>	array(
																			'title' 	=>	'Company Name',
																			'class' 	=> 	'text-left',
																			'style' 	=>	'width: 20%; vertical-align: middle' 
																		),
												// 'tel_no' 		=>	array(
												// 							'title' 	=>	'Tel No',
												// 							'class' 	=>	'text-center',
												// 							'style' 	=>	'width: 10%; vertical-align: middle' 
												// 						),
												// 'fax_no' 		=>	array(
												// 							'title' 	=>	'Fax No',
												// 							'class' 	=>	'text-center',
												// 							'style' 	=>	'width: 10%; vertical-align: middle' 
												// 						),
												// 'tin_no' 		=>	array(
												// 							'title' 	=>	'Tin No',
												// 							'class' 	=>	'text-center',
												// 							'style' 	=>	'width: 10%; vertical-align: middle' 
												// 						),
												'cpc' 			=>	array(
																			'title' 	=>	'Cost/Profit center',
																			'class' 	=>	'text-left',
																			'style' 	=>	'width: 10%; vertical-align: middle' 
																		),	
												'status' 		=> 	array(
																			'title' 	=>	'Status',
																			'class' 	=>	'text-left',
																			'style' 	=>	'width: 10%; vertical-align: middle' 
																		),
											)
									);

		$this->data['dtl_center'] = array(0,1,6,7);

		$this->load_page('/administration/master_file/location/index');

	}

	public function add(){

		$this->breadcrumb[] = array(	'html'	=>	'Location',
										'url'	=>	'administration/master_file/location');
		$this->breadcrumb[]	= array(	'html'	=>	'Add',
										'url'	=>	'');

		// LOAD NEEDED DATA
		$cpc_list 				= $this->load_model('administration/master_file/location/location_model');
		$company_name_list 		= $this->load_model('administration/master_file/location/location_model');

		// STRUCTURE FOR VIEW PAGE
		$this->data['cpc_list'] 		 = $cpc_list->getActiveCPC();
		$this->data['company_name_list'] = $company_name_list->getActiveCompanyName(); 

		$this->data['type']		=	'add';
		$this->data['doc_date'] = Date('m/d/y');
		$this->data['header']   = $this->getData($this->data['type'], '');

		$this->load_page('administration/master_file/location/form');
	}

	public function update(){

		$this->breadcrumb[]	= array(	'html'	=>	'Location',
										'url'	=>	'administration/master_file/location');
		$this->breadcrumb[] = array(	'html'	=>	'Update',
										'url'	=>	'');

		// LOAD NEEDED DATA
		$cpc_list				 = $this->load_model('administration/master_file/location/location_model');
		$company_name_list 		 = $this->load_model('administration/master_file/location/location_model');
		$location_model 		 = $this->load_model('administration/master_file/location/location_model');

		// STRUCTURE FOR VIEW PAGE
		$this->data['cpc_list']  		 = $cpc_list->getActiveCPC();
		$this->data['company_name_list'] = $company_name_list->getActiveCompanyName();
		
		$this->data['type']      = 'update';
		$this->data['id'] 	 	 = $_GET['id'];
		$this->data['header']    = $this->getData($this->data['type'], $this->data['id']);

		$this->load_page('administration/master_file/location/form');
	}

	public function view(){

		$this->breadcrumb[] = array(	'html' 	=>	'Location',
										'url'	=>	'administration/master_file/location');
		$this->breadcrumb[]	= array(	'html'	=>	'View',
										'url'	=>	'');

		// LOAD NEEDED DATA
		$cpc_list 				 = $this->load_model('administration/master_file/location/location_model');
		$company_name_list 		 = $this->load_model('administration/master_file/location/location_model');
		$location_model 		 = $this->load_model('administration/master_file/location/location_model');

		// STRUCTURE FOR VIEW PAGE
		$this->data['cpc_list']  			= $cpc_list->getActiveCPC();
		$this->data['company_name_list'] 	= $company_name_list->getActiveCompanyName();

		$this->data['type']      = 'view';
		$this->data['id'] 		 = 	$_GET['id'];	
		$this->data['header']    = $this->getData($this->data['type'], $this->data['id']);
		$this->load_page('administration/master_file/location/form');
	}

	public function getData($type, $docno = ''){

		$location_model = $this->load_model('administration/master_file/location/location_model');

		if($type == 'add'){
			$output = array(
				'SP_ID'		         			=> ''
				,'SP_StoreName'	             	=> ''
				,'SP_Address'             		=> ''
				,'SP_TelNo'		     			=> ''
				,'SP_FaxNo'     		 		=> ''
				,'SP_TinNo'         		 	=> ''
				,'SP_Active'			 		=>	1
				,'SP_FK_CompanyID'	 			=> ''
				,'SP_FK_CPC_id'		 			=> ''
			);
		}
		else if($type == 'update' || $type == 'view'){

			$where = array(
				'SP_ID' 	=> $docno 
			);

			$join_table = array(
				array(
					'tbl_name' => 'tblCompany',
					'tbl_on'   => 'COM_Name = SP_FK_CompanyID',
					'tbl_join' => 'left'
				),

				array(
					'tbl_name' => 'tblCPCenter',
					'tbl_on'   => 'CPC_Id = SP_FK_CPC_id',
					'tbl_join' => 'left'
				)
			);

			$header = $location_model->getHeaderByDocNo($this->cols, $this->header_table, $where, $join_table);

			$output = array(
				'SP_ID'                 	=> $header['SP_ID']
				,'SP_StoreName'                	=> $header['SP_StoreName']
				,'SP_Address'             		=> $header['SP_Address']
				,'SP_TelNo'        				=> $header['SP_TelNo']
				,'SP_FaxNo'          			=> $header['SP_FaxNo']
				,'SP_TinNo'                  	=> $header['SP_TinNo']
				,'SP_Active'		        	=> $header['SP_Active']
				,'SP_FK_CPC_id'					=> $header['SP_FK_CPC_id']
				,'SP_FK_CompanyID'					=> $header['SP_FK_CompanyID']
				,'CPC_Desc'						=> $header['CPC_Desc']
			);

		}

		return $output;
	}

	public function data(){

		$location_master_model = $this->load_model('administration/master_file/location/location_model');
		$table_data = $location_master_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/master_file/location/view?id='.md5($value['SP_ID']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/master_file/location/update?id='.md5($value['SP_ID']).'">
									<i class="fa fa-pencil"></i>
								</a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['SP_ID'].'"><i class="fa fa-trash"></i></button> ';
			//}

			$sub_array[] = $button;
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['SP_ID'].'">';
			$sub_array[] = $value['SP_ID'];
			$sub_array[] = $value['SP_StoreName'];
			$sub_array[] = $value['SP_Address'];
			$sub_array[] = $value['COM_Name'];
			// $sub_array[] = $value['SP_TelNo'];
			// $sub_array[] = $value['SP_FaxNo'];
			// $sub_array[] = $value['SP_TinNo'];
			$sub_array[] = $value['CPC_Desc']; 
			$sub_array[] = $value['SP_Active'];
			$data[] = $sub_array;

		}
			$output = array(  
					"draw"            =>     intval($_POST["draw"]),  
					"recordsTotal"    =>     $table_data['count_all_results'],  
					"recordsFiltered" =>     $table_data['num_rows'],  
					"data"            =>     $data  
	           );  

			echo json_encode($output);
	}


	public function save(){
		if (!$this->input->is_ajax_request()) return;
		
		if(!empty($_POST)):

			// Get all POST Data
			$data = $this->input->post();
			//$this->print_r($data);
			//die();
			// Load All needed Model
			$location_model = $this->load_model('administration/master_file/location/location_model');

			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];						


			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){
					
					$output = $this->error_message(false, true, false, false, $data, $data['SP_ID'], $this->number_series, '', 'SP', 'tblLocation');

					if($output['success']){
						// UPDATE NO SERIES
						
						$table = array(
							'header'  	=> $this->header_table
						);

						$this->db->trans_begin();

						$location_model->on_save_module(false, $data, $table, 'SP', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					/* 
							END OF SAVING - ADD FUNCTION
					*/

				// UPDATING HEADER
				}	
				else if($this->input->post('todo') == 'update'){
					//$this->print_r($data);
					$output = $this->error_message(false, true, false, false, $data, $data['SP_ID'], $this->number_series, '', 'SP', 'tblLocation');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,
						);

						$this->db->trans_begin();

						$location_model->on_update_module($data, $table, 'SP', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['SP_ID']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
				}
				echo json_encode($output); //ENDING QUERY
			} 
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}

}