<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cost_Profit_Center extends MAIN_Controller {

	protected $number_series		=  103009;
	protected $header_table 		= 'tblCPCenter';
	protected $header_doc_no_field	= 'CPC_Id';
	protected $header_status_field	= 'CPC_Active';
	
	protected $module_name 			= 'Cost/Profit Center';

	public function __construct(){
		parent::__construct();		
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=>'Administration',
										'url'	=> 'administration');
		$this->breadcrumb[] = array(	'html'	=>'Master Files',
										'url'	=> '');
		$this->data['header_table'] 			= $this->header_table;
		$this->data['header_doc_no_field'] 		= $this->header_doc_no_field;
		$this->data['header_status_field'] 		= $this->header_status_field;
		$this->data['number_series'] 			= $this->number_series;
		$this->data['access_header']			= $this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']			= $this->user_model->getDetailAccess($this->number_series);

		$this->cols  = array('CPC_Id', 'CPC_Desc', 'CPC_FK_Class', 'CPC_Active','AD_Id','AD_Desc');
		$this->table = 'tblCPCenter as CPC';
		
	}

	// Table Header
	public function index(){
		//$this->user_model->deleteUserRecordLock(getCurrentUser()['login-user']); // Delete User Record Lock
		$this->breadcrumb[] = array (	'html'	=> 'Cost/Profit Center',
										'url'	=>	'administration/master_file/cost_profit_center');
		//$cost_profit_center_model = $this->load_model('administration/master_file/cost_profit_center/cost_profit_center_model');

		//Header Table
		$this->data['table_hdr'] = array('cost-profit-center-details' => array(
										'button' 			    => array(
																			'title' => '<a class="btn btn-success btn-xs btn-outline add-large" data-toggle="tooltip" data-placement="top" title="Add" type="button" href="'.DOMAIN.'administration/master_file/cost_profit_center/add" role="button"><i class="fa fa-plus"></i></a>',
																			'class' => 'text-center',
																			'style' => 'width: 90px; vertical-align: middle'
																	), 
										'checkbox' 			    => array(
																			'title' => '<input type="checkbox" id="chkSelectAll" class="icheckbox_square-green">',
																			'class' => 'text-center',
																			'style' => 'width: 10px; vertical-align: middle'
																	),
										'cost-profit-center-id' => array(
																			'title' => 'Cost/Profit Center ID',
																			'class' => 'text-left',
																			'style' => 'width: 25%;vertical-align: middle'
																	),
										'description' 			=> array(
																			'title' => 'Description',
																			'class' => 'text-left',
																			'style' => 'width: 25%;vertical-align: middle'
																	), 
										'cost/profit class' 	=> array(
																			'title' => 'Cost/Profit Class',
																			'class' => 'text-left',
																			'style' => 'width: 25%;vertical-align: middle'
																	), 
										'active' 				=> array(
																			'title' => 'Status',
																			'class' => 'text-left',
																			'style' => 'width: 25%;vertical-align: middle'
																	), 
			) 
		);
		$this->data['hdr_center']	= array(0,1,2,3,4);
		$this->load_page('administration/master_file/cost_profit_center/index');
	}

	// Adding of User
	public function add(){
		$this->breadcrumb[] 			= array (	'html'	=>	'Cost/Profit Center',
													'url'	=>	'administration/master_file/cost-profit-center');
		$this->breadcrumb[]				= array (	'html'	=>	'Add',
													'url'	=>	'');
		$category_list 					= $this->load_model('administration/master_file/cost_profit_center/cost_profit_center_model');
		$cpc_class_list 				= $this->load_model('administration/master_file/cost_profit_center/cost_profit_center_model');
		
		$this->data['type']		 		= 'add';
		//$this->data['doc_no'] 	 		= $cpc_series;											
		//$this->data['item_list'] 		= $item_model->getAll();
		$this->data['category_list']	= $category_list->getAll_Active();
		$this->data['cpc_class_list']	= $cpc_class_list->getActiveCPCclass();
		$this->data['header']	 		= $this->getData($this->data['type'], '');
		$this->load_page('administration/master_file/cost_profit_center/form');

	}

	// Updating of Users Data
	public function update(){
		$this->breadcrumb[] 			= array (	'html'	=>	'Cost/Profit Center',
													'url'	=>	'administration/master_file/cost-profit-center');
		$this->breadcrumb[] 			= array (	'html'	=>	'Update',
													'url'	=>	'');
		
		$cost_profit_center_model 		= $this->load_model('administration/master_file/cost_profit_center/cost_profit_center_model');
		$cpc_class_list 				= $this->load_model('administration/master_file/cost_profit_center/cost_profit_center_model');

		$this->data['type'] 	  		= 'update';
		$this->data['id'] 	  	  		= $_GET['id'];
		$this->data['cpc_class_list']	= $cpc_class_list->getActiveCPCclass();
		//$this->data['item_list']  	= $item_model->getAll();
		$this->data['header'] 	  		= $this->getData($this->data['type'], $this->data['id']);
		$this->load_page('administration/master_file/cost_profit_center/form');
	}
	
	// Viewing Of Users Data
	public function view(){
		$this->breadcrumb[] 			= array (	'html'	=>	'Cost/Profit Center',
													'url'	=>	'administration/master_file/cost-profit-center');
		$this->breadcrumb[] 			= array (	'html'	=>	'View',
													'url'	=>	'');
		
		$cost_profit_center_model 		= $this->load_model('administration/master_file/cost_profit_center/cost_profit_center_model');
		$cpc_class_list 				= $this->load_model('administration/master_file/cost_profit_center/cost_profit_center_model');
		$this->data['type']		  		= 'view';
		$this->data['id'] 	  			= $_GET['id'];
		
		$this->data['cpc_class_list']	= $cpc_class_list->getActiveCPCclass();	
		$this->data['header']	  		= $this->getData($this->data['type'], $this->data['id']);
		$this->load_page('administration/master_file/cost_profit_center/form');
	}

	// Getting of data from the Form
	public function getData($type, $docno = ''){
		$cost_profit_center_model = $this->load_model('administration/master_file/cost_profit_center/cost_profit_center_model');
		if ($type == 'add') {
			$output = array(
				'CPC_Id' 				=> '',
				'CPC_Desc' 				=> '',
				'CPC_FK_Class' 			=> '',
				'CPC_Active'  			=> 1,		
			);
		}else{
			$where = array(
				'CPC_Id' => $docno
			);
			$join_table = array(
				array(
					'tbl_name'  => 'tblAttributeDetail',
					'tbl_on' 	=> 'AD_Id = CPC_FK_Class',
					'tbl_join' 	=> 'left'
				),
			);

			$header = $cost_profit_center_model->getHeaderByDocNo($this->cols, $this->table, $where, $join_table);
			$output = array(
				'CPC_Id' 			=> $header['CPC_Id'],
				'CPC_Desc' 			=> $header['CPC_Desc'],
				'CPC_FK_Class' 		=> $header['AD_Desc'],
				'CPC_Active' 		=> $header['CPC_Active'], 
				'functions'			=> $this->getApprovalButtons($header['CPC_Id']
																,$header['CPC_Active']
																,getCurrentUser()['login-user']
																,$header['CPC_Desc']
																,$this->table
																,$this->header_status_field
																,$this->header_doc_no_field
															), 

			);
		}
		return $output;
	}

	// Data to be Display in the Table
	public function data(){

		$cost_profit_center_model = $this->load_model('administration/master_file/cost_profit_center/cost_profit_center_model');		
		$table_data = $cost_profit_center_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/master_file/cost_profit_center/view?id='.md5($value['CPC_Id']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/master_file/cost_profit_center/update?id='.md5($value['CPC_Id']).'">
					<i class="fa fa-pencil"></i>
					</a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['CPC_Id'].'" "><i class="fa fa-trash"></i></a> ';
			//}

			$sub_array[] = $button;
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['CPC_Id'].'">';
			$sub_array[] = $value['CPC_Id'];
			$sub_array[] = $value['CPC_Desc'];
			$sub_array[] = $value['AD_Desc'];
			$sub_array[] = $value['CPC_Active'];
			
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		if(!$this->input->is_ajax_request()) return;

		if(!empty($_POST)):

			// Get All POST Data
			$data 						= $this->input->post();

			// Load All needed model
			
			$cost_profit_center_model	= $this->load_model('administration/master_file/cost_profit_center/cost_profit_center_model');
			// GET CURRENT USER
			$current_user 				= getCurrentUser()['login-user'];


			$output = array(
							'success'	=> true
						,	'message'	=> '');
			try{

				// Start Of Saving - Add Function
				if($this->input->post('todo') == 'add'){
					// GET THE LATEST NO SERIES
				
					$output = $this->error_message(false, true, false, false, $data, $data['CPC_Id'], $this->number_series,'','CPC','tblCPCenter');
					
				
				if($output['success']){
						// UPDATE NO SERIES
						// $of_series = $noseries_model->getNextAvailableNumber($this->number_series, true, $data['of-location']);	
						
						// $output['doc_id'] = md5($of_series);

						$table = array(
							'header'  	=> $this->header_table,
						);

						$this->db->trans_begin();

						$cost_profit_center_model->on_save_module(false, $data, $table, 'CPC', $this->number_series, getDefaultLocation());

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);

				$output = $this->error_message(false, true, false, false, $data, $data['CPC_Id'], $this->number_series,'','','tblCPCenter');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,
							
						);

						$this->db->trans_begin();

						$cost_profit_center_model->on_update_module($data, $table, 'CPC', $this->number_series, getDefaultLocation());

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['CPC_Id']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				echo json_encode($output); //ENDING QUERY
			}

			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;

	}

	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}
	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}

}
?>