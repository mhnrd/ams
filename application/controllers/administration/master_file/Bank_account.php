<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Bank_account extends MAIN_Controller {

	protected $number_series		= 1308;
	protected $header_table			= 'tblBankAccount';
	protected $header_doc_no_field	= 'BA_BankID';
	protected $header_status_field	= 'BA_Active';
	protected $module_name			= 'Bank Account';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=>	'Administration',
										'url'	=>	'');
		$this->breadcrumb[] = array(		'html'	=>	'Master File',
											'url'	=>	'');

		$this->data['header_table']			= 	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						=   array('BA_BankID', 'BA_BankName','BA_BankAccountType','BA_BankAccountNo','BA_BankAddress','BA_ContactName','BA_ContactNo','BA_FaxNo','BA_Currency','BA_BookBalance','BA_DateOpened','BA_DateClosed','BA_Active','BA_InUseCode','BA_InUseStartingNo','BA_InUseEndingNo','BA_InUseLastNoUsed','BA_InUseActive','BA_ReserveCode','BA_ReserveStartingNo','BA_ReserveEndingNo','BA_ReserveEndingNo','BA_ReserveLastNoUsed','BA_ReserveActive','BA_DMCode','BA_DMStartingNo','BA_DMEndingNo','BA_DMLastNoUsed','BA_DMActive','BA_WCode','BA_WStartingNo','BA_WEndingNo','BA_WLastNoUsed','BA_WActive','BA_TTCode','BA_TTStartingNo','BA_TTEndingNo','BA_TTLastNoUsed','BA_TTActive');
		$this->table 						=   'tblBankAccount as BA';
	}

	public function index(){
		$this->breadcrumb[] = array(		'html'	=>	'Bank Account',
											'url'	=>	'administration/master_file/bank_account');

		$this->data['table_hdr'] = array('bank_account' => array(
												'buttons' 				=> array(
																					'title' 	=> '<a class="btn btn-success
																									btn-xs 
																									btn-outline" 
																									data-toggle="tooltip" 
																									data-placement="top" 
																									title="Add" type="button"
																									href="'.
																									DOMAIN.'master_file/
																									bank_account/add"><i
																									class="fa
																									fa-plus"></i></a>',
																					'class' 	=> 'text-center',
																					'style' 	=> 'width: 20%; vertical-align:
																									middle' 
																				), 
												'bank_code' 			=> array(
																					'title' 	=> 'Bank Code',
																					'class' 	=> 'text-center',
																					'style' 	=> 'width: 20%; vertical-align:
																									middle'
																				),
												'bank_name' 			=> array(
																					'title' 	=> 'Bank Name',
																					'class' 	=> 'text-center',
																					'style' 	=> 'width: 20%; vertical-align:
																									middle' 
																				),
												'bank_account_type' 	=> array(
																					'title' 	=> 'Bank Account Type',
																					'class'  	=> 'text-center',
																					'style' 	=> 'width: 5%; vertical-align:
																									middle' 
																				),
												'currency' 				=> array(
																					'title'  	=> 'Currency',
																					'class' 	=> 'text-center',
																					'style' 	=> 'width: 20%; vertical-align:
																									middle'
																				),
												'status'  				=> array(
																					'title' 	=> 'Status',
																					'class' 	=> 'text-center',
																					'style' 	=> 'width: 20%; vertical-align:
																									middle' 
																				),		
											)
								);
		$this->data['hdr_center'] = array(0,1,2,4,5);

		$this->load_page('administration/master_file/bank_account/index');
	}

	public function add(){
		$this->breadcrumb[] = array(	'html'	=>	'Bank Account',
										'url'	=>	'administration/master_file/bank_account');
		$this->breadcrumb[] = array(	'html'	=>	'Add',
										'url'	=>	'');

		// LOAD NEEDED DATA
		$bank_posting_group_list 				= $this->load_model('administration/master_file/bank_account/bank_account_model');
		$currency_list							= $this->load_model('administration/master_file/bank_account/bank_account_model');
		$cpc_list 								= $this->load_model('master_file/location/location_model');

		//STRUCTURE FOR VIEW PAGE
		$this->data['bank_posting_group_list']	= $bank_posting_group_list->getActiveBPG();
		$this->data['currency_list']			= $currency_list->getActiveCurrency();
		$this->data['cpc_list'] 		 		= $cpc_list->getActiveCPC();

		$this->data['type']		 				= 'add';
		$this->data['doc_date'] 				= Date('m/d/y');
		$this->data['header']	 				= $this->getData($this->data['type'], '');

		$this->load_page('administration/master_file/bank_account/form');
	}

	public function update(){
		$this->breadcrumb[] = array(	'html'	=>	'Bank Account',
										'url'	=>	'administration/master_file/bank_account');
		$this->breadcrumb[] = array(	'html'	=>	'Update',
										'url'	=>	'');

		// LOAD NEEDED DATA
		$bank_posting_group_list 				= $this->load_model('administration/master_file/bank_account/bank_account_model');
		$currency_list							= $this->load_model('administration/master_file/bank_account/bank_account_model');
		$cpc_list 				= $this->load_model('master_file/location/location_model');

		//STRUCTURE FOR VIEW PAGE
		$this->data['bank_posting_group_list']	= $bank_posting_group_list->getActiveBPG();
		$this->data['currency_list']			= $currency_list->getActiveCurrency();
		$this->data['cpc_list'] 		 		= $cpc_list->getActiveCPC();


		$this->data['type']		= 'update';
		$this->data['id']		= $_GET['id'];
		$this->data['header'] 	= $this->getData($this->data['type'], $this->data['id']);

		$this->load_page('administration/master_file/bank_account/form');
	}

	public function getData($type, $docno = ''){

		$bank_account_model = $this->load_model('administration/master_file/bank_account/bank_account_model');

		if($type == 'add'){
			$output = array(
				'BA_BankID' 			=> '',
				'BA_BankName' 			=> '',
				'BA_BankAccountType'  	=> '',
				'BA_BankAccountNo' 		=> '',
				'BA_BankAddress'  		=> '',
				'BA_ContactName'  		=> '',
				'BA_ContactNo' 			=> '',
				'BA_FaxNo' 				=> '',
				'BA_Currency'  			=> '',
				'BA_BookBalance' 		=> '',
				//'maint. balance' 	 	=> '', 
				'BA_DateOpened' 		=> '',
				'BA_DateClosed' 		=> '',
				'BA_Active' 			=> '0', 
				'BA_InUseCode' 			=> '',
				'BA_InUseStartingNo' 	=> '',
				'BA_InUseEndingNo' 		=> '',
				'BA_InUseLastNoUsed' 	=> '',
				'BA_InUseActive' 		=> '',
				'BA_ReserveCode' 		=> '',
				'BA_ReserveStartingNo' 	=> '',
				'BA_ReserveEndingNo' 	=> '', 
				'BA_ReserveLastNoUsed' 	=> '',
				'BA_ReserveActive'  	=> '', 
				'BA_DMCode' 			=> '',
				'BA_DMStartingNo'  		=> '',
				'BA_DMEndingNo' 		=> '',
				'BA_DMLastNoUsed' 		=> '',
				'BA_DMActive' 			=> '',
				'BA_WCode' 				=> '',
				'BA_WStartingNo' 		=> '',
				'BA_WEndingNo'  		=> '',
				'BA_WLastNoUsed' 		=> '',
				'BA_WActive' 			=> '',
				'BA_TTCode'				=> '',
				'BA_TTStartingNo' 		=> '', 
				'BA_TTEndingNo' 		=> '', 
				'BA_TTLastNoUsed' 		=> '',
				'BA_TTActive'  			=> '', 
				'AD_Code' 				=> '', 	







			);
		}

		else if($type == 'update' || $type == 'view'){

			$where = array(
				'BA_BankID' => $docno
			);

			$join_table = array(
				array(
					'tbl_name' => 'tblAttributeDetail',
					'tbl_on'   => 'AD_Code = BA_Currency',
					'tbl_join' => 'left'
				),

			);

			$header = $bank_account_model->getHeaderByDocNo($this->cols, $this->header_table, $where);

			$output = array(
				'BA_BankID' 				=>	$header['BA_BankID'],
				'BA_BankName' 				=>	$header['BA_BankName'],
				'BA_Active' 				=>  $header['BA_Active'], 
				'BA_BankAccountType'  		=>	$header['BA_BankAccountType'],
				'BA_BankAccountNo' 			=>	$header['BA_BankAccountNo'],
				'BA_BankAddress' 			=>	$header['BA_BankAddress'],
				'BA_ContactName' 			=>	$header['BA_ContactName'],
				'BA_ContactNo'				=>	$header['BA_ContactNo'],
				'BA_FaxNo' 					=>	$header['BA_FaxNo'],
				'BA_BookBalance' 			=>	number_format($header['BA_BookBalance'],2), 
				'BA_DateOpened' 			=>	$header['BA_DateOpened'],
				'BA_DateClosed' 			=>	$header['BA_DateClosed'], 
				'BA_InUseCode' 				=>	$header['BA_InUseCode'],
				'BA_InUseStartingNo' 	 	=>	$header['BA_InUseStartingNo'],	
				'BA_InUseEndingNo' 			=>	$header['BA_InUseEndingNo'], 
				'BA_InUseLastNoUsed' 		=>	$header['BA_InUseLastNoUsed'],
				'BA_InUseActive'  			=>	$header['BA_InUseActive'], 
				'BA_ReserveCode' 			=>	$header['BA_ReserveCode'],
				'BA_ReserveStartingNo' 		=>	$header['BA_ReserveStartingNo'],
				'BA_ReserveEndingNo'  		=>	$header['BA_ReserveEndingNo'],
				'BA_ReserveLastNoUsed' 		=>	$header['BA_ReserveLastNoUsed'], 
				'BA_ReserveActive' 			=>	$header['BA_ReserveActive'],
				'BA_DMCode' 				=>	$header['BA_DMCode'],
				'BA_DMStartingNo' 			=>	$header['BA_DMStartingNo'],
				'BA_DMEndingNo'  			=>	$header['BA_DMEndingNo'],
				'BA_DMLastNoUsed' 			=>	$header['BA_DMLastNoUsed'],
				'BA_DMActive' 				=>	$header['BA_DMActive'],
				'BA_WCode' 					=>	$header['BA_WCode'],
				'BA_WStartingNo' 			=>  $header['BA_WStartingNo'],
				'BA_WEndingNo' 				=>  $header['BA_WEndingNo'],
				'BA_WLastNoUsed' 			=>  $header['BA_WLastNoUsed'], 
				'BA_WActive' 				=>	$header['BA_WActive'],
				'BA_TTCode' 				=>	$header['BA_TTCode'],
				'BA_TTStartingNo' 			=>	$header['BA_TTStartingNo'], 
				'BA_TTEndingNo' 			=>	$header['BA_TTEndingNo'],
				'BA_TTLastNoUsed' 			=>	$header['BA_TTLastNoUsed'], 
				'BA_TTActive'	 			=>	$header['BA_TTActive'],
				'AD_Code' 					=>	$header['BA_Currency'], 	



				//'BA_Currency' 				=>	$header['AD_Code'],
				
				'functions' 		    => $this->getApprovalButtons($header['BA_BankID']
																,$header['BA_Active']
																,$header['BA_BankName']
																,getCurrentUser()['login-user']
																,$this->header_table
																,$this->header_status_field
																,$this->header_doc_no_field
															),

			);

			if($output['BA_Active'] == 'Approved'){
				unset($output['functions'][1]);
			}
		}

		return $output;
	}

	public function data(){

		$bank_account_model = $this->load_model('administration/master_file/bank_account/bank_account_model');
		$table_data = $bank_account_model->table_data();
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/master_file/bank_account/update?id='.md5($value['BA_BankID']).'">
									<i class="fa fa-pencil"></i>
								</a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<a class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['BA_BankID'].'"><i class="fa fa-trash"></i></a> ';
			//}
		

			$sub_array[] = $button;
			$sub_array[] = $value['BA_BankID'];
			$sub_array[] = $value['BA_BankName'];
			$sub_array[] = ($value['BA_BankAccountType'] == 'CA' ? 'Current Account' : ($value['BA_BankAccountType'] == 'SA' ? 'Savings Account' : ''));
			$sub_array[] = $value['AD_Code'];
			$sub_array[] = ($value['BA_Active'] == 1) ? "Active" : "Inactive";

			$data[] = $sub_array;

		}
			$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;

		if(!empty($_POST)):

			// GET ALL POST data
			$data = $this->input->post();
			//$this->print_r($data);
			//die();
			// LOAD ALL NEEDED MODEL
			$bank_account_model = $this->load_model('administration/master_file/bank_account/bank_account_model');

			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];

			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){

					$output = $this->error_message(false, true, false, false, $data, $data['BA_BankID'], $this->number_series, '', 'BA', 'tblBankAccount');

					if($output['success']){
						// UPDATE NO SERIES

						$table = array(
							'header'  	=> $this->header_table,
							'detail' 	=> 'tblBankAccountLedger', 
						);

						$this->db->trans_begin();

						$bank_account_model->on_save_module(false, $data, $table, 'BA','BAL', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}
					
					/* 
							END OF SAVING - ADD FUNCTION
					*/
					
				// UPDATE HEADER
				}
				else if($this->input->post('todo') == 'update'){
				
					$output = $this->error_message(false, true, false, false, $data, $data['BA_BankID'], $this->number_series, '', 'BA', 'tblBankAccount');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table,
							'detail' 	=> 'tblBankAccountLedger', 
						);

						$this->db->trans_begin();

						$bank_account_model->on_update_module($data, $table, 'BA','BAL', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
				}
				echo json_encode($output); //ENDING QUERY
			} 
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function delete(){
		$doc_no = $this->input->get();
		$bank_account_model = $this->load_model('administration/master_file/bank_account/bank_account_model');
		echo json_encode($bank_account_model->delete_data($doc_no['id']));
	}
}