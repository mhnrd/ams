<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class NumberSeries extends MAIN_Controller {

	public function __construct(){
		parent::__construct();		
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=>'Bill Of Material',
						'url'	=> 'master_file/bom/bomlist');
	}

	public function getNextAvailableNumber(){
		$filter = $this->input->get();
		$noseries_model = $this->load_model('administration/master_file/bom/noseries_model');
		$no_series = $noseries_model->getNextAvailableNumberbyEvent($filter['ns_id'], false, $filter['location']);
		echo json_encode($no_series);
	}
}
