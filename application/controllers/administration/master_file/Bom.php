<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Bom extends MAIN_Controller {

	protected $number_series		=  1609;
	protected $header_table 		= 'tblBOM';
	protected $header_doc_no_field	= 'BOM_DocNo';
	protected $header_status_field	= 'BOM_Status';
	protected $module_name 			= 'Bill of Material';

	public function __construct(){
		parent::__construct();		
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=>'Master Files',
										'url'	=> '');

		$this->data['header_table'] 		= $this->header_table;
		$this->data['header_doc_no_field'] 	= $this->header_doc_no_field;
		$this->data['header_status_field'] 	= $this->header_status_field;
		$this->data['number_series'] 		= $this->number_series;
		$this->data['access_header']		= $this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		= $this->user_model->getDetailAccess($this->number_series);
	}

	public function index(){
		$this->user_model->deleteUserRecordLock(getCurrentUser()['login-user']); // Delete User Record Lock
		$this->breadcrumb[] = array(	'html'	=>'Bill Of Material',
										'url'	=> '');
		$this->load_page('administration/master_file/billofmaterial/index');
	}

	public function add(){
		$this->breadcrumb[] = array(	'html'	=>'Bill Of Material',
										'url'	=> 'master_file/bom');
		$this->breadcrumb[] = array(	'html'	=>'Add',
										'url'	=> '');
		$attrdetail_model = $this->load_model('administration/master_file/bom/attrdetail_model');
		$item_model = $this->load_model('administration/master_file/bom/item_model');
		$noseries_model = $this->load_model('administration/master_file/bom/noseries_model');
		$companyaccess_model = $this->load_model('com/companyaccess_model');

		$bom_series = $noseries_model->getNextAvailableNumber($this->number_series, false, getDefaultLocation());
		$this->data['type'] = 'add';
		$this->data['doc_no'] = $bom_series;
		$this->data['header'] = $this->getData('add', $this->data['doc_no']);
		$this->data['doc_date'] = Date('m/d/Y');
		$this->data['uom_list'] = $attrdetail_model->getAllByAttrCode('1401');
		$this->data['item_list'] = $item_model->getAll();
		$this->data['location_list'] = $companyaccess_model->getStoreTypebyUserID(getCurrentUser()['login-user']);
		$this->load_page('administration/master_file/billofmaterial/form');
	}

	public function update(){
		$this->breadcrumb[] = array(	'html'	=>'Bill Of Material',
										'url'	=> 'administration/master_file/bom');
		$this->breadcrumb[] = array(	'html'	=>'Update',
										'url'	=> '');
		$bom_model = $this->load_model('administration/master_file/bom/bom_model');		
		$attrdetail_model = $this->load_model('administration/master_file/bom/attrdetail_model');
		$item_model = $this->load_model('administration/master_file/bom/item_model');
		$itemuomconv_model = $this->load_model('administration/master_file/bom/itemuomconv_model');
		
		
		$companyaccess_model = $this->load_model('com/companyaccess_model');
		$bomdetail_model = $this->load_model('administration/master_file/bom/bomdetail_model');
		$this->data['type'] = 'update';
		$this->data['doc_no'] = $this->input->get('id');
		$this->data['header'] = $this->getData('update',$this->data['doc_no']);
		$this->data['bom'] = $bom_model->getByBOMDocNo($this->data['doc_no']);
		$this->data['bom_detail'] = $bomdetail_model->getAllByDocNo($this->data['doc_no']);
		$this->data['doc_date'] = format_date($this->data['bom']['BOM_DocDate'], 'm/d/Y');
		$this->data['uom_list'] = $attrdetail_model->getAllByAttrCode('1401');
		$this->data['item_list'] = $item_model->getAll($this->data['header']['BOM_ItemNo'],true);
		$this->data['location_list'] = $companyaccess_model->getStoreTypebyUserID(getCurrentUser()['login-user']);
		$this->data['uom'] = $itemuomconv_model->getAllByItemID($this->data['header']['BOM_ItemNo']);

		// $this->print_r($this->data['uom']);

		// Validate Document if other user is editing it.
		$this->validateUserRecordLock(	 getCurrentUser()['login-user']
										,$this->data['header']['BOM_DocNo']
										,$this->module_name
										,$this->header_table
										,'administration/master_file/bom'
										,'administration/master_file/billofmaterial/form'
									);		
	}

	public function view(){
		$this->breadcrumb[] = array(	'html'	=>'Bill Of Material',
										'url'	=> 'administration/master_file/bom');
		$this->breadcrumb[] = array(	'html'	=>'View',
										'url'	=> '');
		$bom_model = $this->load_model('administration/master_file/bom/bom_model');		
		$attrdetail_model = $this->load_model('administration/master_file/bom/attrdetail_model');
		$item_model = $this->load_model('administration/master_file/bom/item_model');

		$bomdetail_model = $this->load_model('administration/master_file/bom/bomdetail_model');
		$companyaccess_model = $this->load_model('com/companyaccess_model');

		$this->data['type'] = 'view';
		$this->data['doc_no'] = $this->input->get('id');
		$this->data['header'] = $this->getData('view',$this->data['doc_no']);
		$this->data['bom'] = $bom_model->getByBOMDocNo($this->data['doc_no']);
		$this->data['bom_detail'] = $bomdetail_model->getAllByDocNo($this->data['doc_no']);
		$this->data['doc_date'] = format_date($this->data['bom']['BOM_DocDate'], 'm/d/Y');
		$this->data['uom_list'] = $attrdetail_model->getAllByAttrCode('1401');
		$this->data['item_list'] = $item_model->getAll();
		$this->data['location_list'] = $companyaccess_model->getStoreTypebyUserID(getCurrentUser()['login-user']);

		// $this->print_r($this->data['header']['functions']);
		$this->load_page('administration/master_file/billofmaterial/form');
	}

	public function getData($type, $doc_no){

		if($type == 'add'){
			$output = array(
				'BOM_DocNo' 			=> $doc_no,
				'BOM_ItemNo' 			=> '',
				'BOM_ItemDescription'	=> '',
				'BOM_BOMDescription' 	=> '',
				'BOM_DocDate' 			=> date('m/d/Y'),
				'BOM_YieldQty' 			=> 0,
				'BOM_YieldUomID' 		=> '',
				'BOM_YieldUomName' 		=> '',
				'BOM_BatchQty' 			=> 0,
				'BOM_BatchUomID' 		=> '',
				'BOM_BatchUomName' 		=> '',
				'BOM_Status' 			=> 'Open',
				'BOM_Version' 			=> 1,
				'BOM_Remarks' 			=> '',
				'BOM_Location' 			=> getDefaultLocation()				
			);
		}
		else if($type == 'update' || $type == 'view'){
			$bom_model = $this->load_model('administration//master_file/bom/bom_model');
			$header = $bom_model->getAll($this->data['doc_no']);
			$output = array(
				'BOM_DocNo' 			=> $header['BOM_DocNo'],
				'BOM_ItemNo' 			=> $header['BOM_ItemNo'],
				'BOM_ItemDescription'	=> $header['IM_Sales_Desc'],
				'BOM_BOMDescription' 	=> $header['BOM_BOMDescription'], 
				'BOM_DocDate' 			=> date_format(date_create($header['BOM_DocDate']), 'm/d/Y'),
				'BOM_YieldQty' 			=> $header['BOM_YieldQty'],
				'BOM_YieldUomID' 		=> $header['BOM_YieldUomID'],
				'BOM_YieldUomName' 		=> $header['YieldUOM'],
				'BOM_BatchQty' 			=> $header['BOM_BatchQty'],
				'BOM_BatchUomID' 		=> $header['BOM_BatchUomID'],
				'BOM_BatchUomName' 		=> $header['BatchUOM'],
				'BOM_Status' 			=> $header['BOM_Status'],
				'BOM_Version' 			=> $header['BOM_Version'],
				'BOM_Remarks' 			=> $header['BOM_Remarks'],
				'BOM_Location' 			=> $header['BOM_Location'],
				'SP_StoreName' 			=> $header['SP_StoreName'],
				'functions' 			=> $this->getApprovalButtons($header['BOM_DocNo']
																	,$header['BOM_Status']
																	,getCurrentUser()['login-user']
																	,''
																	,$this->header_table
																	,$this->header_status_field
																	,$this->header_doc_no_field
																),
			);
		}

		return $output;
	}

	/*
		AJAX Request goes here
	*/
	
	public function action(){
	
		$type = $this->input->post('action');
		$bomno = $this->input->post('bom-no');
		$itemno = $this->input->post('id');
		$version = $this->input->post('version');
 		$bom_model = $this->load_model('administration/master_file/bom/bom_model');
		
		$check_mo = $bom_model->checkIfUsedByMO($bomno);
		$check_of = $bom_model->checkIfUsedByOF($itemno);
		$check_pf = $bom_model->checkIfUsedByPF($itemno);

 		if($type == 'active'){
 			if($check_mo == 0 && $check_of == 0 && $check_pf == 0){
 				$data = $bom_model->status_data($itemno,$type,$version);
 				if($data['success'] == 1){
	 				$output = array(
	 					'success' => true,
	 					'message' => ''
	 				);
 				}
 				else{
 					$output = array(
	 					'success' => false,
	 					'message' => 'Contact your System Administrator'
	 				);
 				}
 			}
 			else{
 				$output = array(
 					'success' => false,
 					'message' => 'This BOM is currently used in another module'
 				);
 			}
 		}
 		elseif($type == 'deactivate'){
 			if($check_mo == 0 && $check_of == 0){
 				$data = $bom_model->status_data($itemno,$type,$version);
 				if($data['success'] == 1){
	 				$output = array(
	 					'success' => true,
	 					'message' => ''
	 				);
 				}
 				else{
 					$output = array(
	 					'success' => false,
	 					'message' => 'Contact your System Administrator'
	 				);
 				}
 			}
 			else{
 				$output = array(
 					'success' => false,
 					'message' => 'This BOM is currently used in another module'
 				);
 			}
 		}
		echo json_encode($output);
	}

	public function getUOMConversion(){
		if (!$this->input->is_ajax_request()) return;
		$data = array();
		$itemuomconv_model = $this->load_model('administration/master_file/bom/itemuomconv_model');
		$data = $itemuomconv_model->getAllByItemID($_GET['item-no']);
		
		echo json_encode($data);
	}


	// Added by Jekzel Leonidas for Base UOM & Base UOM Qty
	public function getBaseUOMConversion(){
		if (!$this->input->is_ajax_request()) return;
		$data = array();
		$itemuomconv_model = $this->load_model('administration/master_file/bom/itemuomconv_model');
		$data = $itemuomconv_model->getBaseUOMbyItemID($_GET['item-no']);
		echo json_encode($data);
	}

	public function generateBOMReport(){		
		
		$rows = array();
		$bom_model = $this->load_model('administration/master_file/bom/bom_model');
		$this->data['bom'] = $bom_model->getByBOMDocNoWithUOM($this->input->get());

		$bomreport_model = $this->load_model('inv/bomreport_model');
		$this->data['bomreport'] = $bomreport_model->getAll();

		$bomdetail_model = $this->load_model('administration/master_file/bom/bomdetail_model');
		$bom_detail = $bomdetail_model->getAllByDocNoWithItemCat($this->input->get());

		$item_cat_list = array(
			array('type'=>	'A'
			,	'desc' => 'A. Meat Material'
			,	'total' => array(
					'quantity'=> 0,
					'cost'	=> 0
				)
			, 	'list'	=> array())
		,	array('type'=>	'B'
			,	'desc' => 'B. Dry Ingredients ( Spices, Seasonings, Additives, Food Color)'
			,	'total' => array(
					'quantity'=> 0,
					'cost'	=> 0
				)			
			,	'list'	=> array())
		,	array('type'=>	'C'
			,	'desc' => 'C. Liquid and Other Ingredients'
			,	'total' => array(
					'quantity'=> 0,
					'cost'	=> 0
				)
			,	'list'	=> array())
		,	array('type'=>	'D'
			,	'desc' => 'D. Added Water'
			,	'total' => array(
					'quantity'=> 0,
					'cost'	=> 0
				)
			,	'list'	=> array())
		,	array('type'=>	'E'
			,	'desc' => 'E. PACKAGING MATERIALS'
			,	'total' => array(
					'quantity'=> 0,
					'cost'	=> 0
				)
			,	'list'	=> array())
		);

		foreach($bom_detail as $bomd){
			if($bomd['IC_Type'] == 'A'){
				$item_cat_list[0]['total']['quantity'] += $bomd['BOMD_Qty'];
				$item_cat_list[0]['total']['cost'] += $bomd['BOMD_TotalCost'];
				array_push($item_cat_list[0]['list'], $bomd);
			}
			else if($bomd['IC_Type'] == 'B'){
				$item_cat_list[1]['total']['quantity'] += $bomd['BOMD_Qty'];
				$item_cat_list[1]['total']['cost'] += $bomd['BOMD_TotalCost'];
				array_push($item_cat_list[1]['list'], $bomd);
			}
			else if($bomd['IC_Type'] == 'C'){
				$item_cat_list[2]['total']['quantity'] += $bomd['BOMD_Qty'];
				$item_cat_list[2]['total']['cost'] += $bomd['BOMD_TotalCost'];
				array_push($item_cat_list[2]['list'], $bomd);
			}
			else if($bomd['IC_Type'] == 'D'){
				$item_cat_list[3]['total']['quantity'] += $bomd['BOMD_Qty'];
				$item_cat_list[3]['total']['cost'] += $bomd['BOMD_TotalCost'];
				array_push($item_cat_list[3]['list'], $bomd);
			}
			else if($bomd['IC_Type'] == 'E'){
				$item_cat_list[4]['total']['quantity'] += $bomd['BOMD_Qty'];
				$item_cat_list[4]['total']['cost'] += $bomd['BOMD_TotalCost'];
				array_push($item_cat_list[4]['list'], $bomd);
			}
		}
		$this->data['bom_detail_listed'] = $item_cat_list;
		$html = $this->load_report('/pdf_report/bom/bom');
		$this->load->library('Pdf_mpdf');
		$data = array(	'orientation'=>'P',
						'title'=>'Bill Of Material',
                        'html'=>$html,
                        'font' => 'ARIAL',
						'is_create'=>true);
        echo $this->pdf_mpdf->generate($data);
	}

	public function getBOMReportSignatory(){
		if (!$this->input->is_ajax_request()) return;

		$bomreport_model = $this->load_model('inv/bomreport_model');
		$bomreport = $bomreport_model->getAll();
		echo json_encode($bomreport);
	}

	public function setBOMReportSignatory(){
		if (!$this->input->is_ajax_request()) return;
		$bomreport_model = $this->load_model('inv/bomreport_model');
		echo json_encode(array('success'=>true));
	}

	public function getItem(){
		if (!$this->input->is_ajax_request()) return;
		$type = $_GET['filter-type'];
		$filter = $_GET['filter-search'];
		$action = $_GET['action'];
		$cat_id = '';
		if(isset($_GET['category-type'])){
			$cat_id = $_GET['category-type'];
		}
		$data = array();
		$item_model = $this->load_model('administration/master_file/bom/item_model');
		if($action=='search'){
			$data = $item_model->getAllBySearch($type, $filter, $cat_id);
		}
		else if($action=='Select'){
			$data = $item_model->getByFilter($type, $filter, $cat_id);
		}
		
		echo json_encode($data);
	}

	public function getItemNotScrap(){
		if (!$this->input->is_ajax_request()) return;
		$type = $_GET['filter-type'];
		$filter = $_GET['filter-search'];
		$action = $_GET['action'];
		$cat_id = '';
		if(isset($_GET['category-type'])){
			$cat_id = $_GET['category-type'];
		}
		$data = array();
		$item_model = $this->load_model('administration/master_file/bom/item_model');
		if($action=='search'){
			$data = $item_model->getAllBySearchNotScrap($type, $filter, $cat_id);
		}
		else if($action=='Select'){
			$data = $item_model->getByFilterNotScrap($type, $filter, $cat_id);
		}

		// $this->print_lastquery();
		
		echo json_encode($data);
	}

	public function manageApproval(){
		if (!$this->input->is_ajax_request()) return;
		$data = array();
		$bom_model = $this->load_model('administration/master_file/bom/bom_model');

		$current_user = getCurrentUser()['login-user'];
		$bom = array(
			'ModifiedBy'=>$current_user,
			'DateModified'=>date('Y-m-d H:i:s')
			);
		if($_GET['action'] == 'request-approval'){
			$bom['BOM_Status'] = 'Pending';
		}
		else if($_GET['action'] == 'approve'){
			$bom['BOM_Status'] = 'Active';
		}
		else if($_GET['action'] == 'reject'){
			$bom['BOM_Status'] = 'Open';
		}
		$where = array('BOM_DocNo = '=>	$_GET['doc-no']);
		$result = $bom_model->update($bom, $where);
		echo json_encode($data);
	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;
		if(!empty($_POST)){
			
			$data = $this->input->post();
			
			$item_model = $this->load_model('administration/master_file/bom/item_model');
			$bom_model = $this->load_model('administration/master_file/bom/bom_model');			
			$bomdetail_model = $this->load_model('administration/master_file/bom/bomdetail_model');

			$output = array(
							'success'=>true
						,	'message'=>'');
			
			if($data['doc-no'] == ''){
				$output = array(
							'success'=> false
						,	'message'=> 'doc-no-null'
				);
				echo json_encode($output);	
				return;
			}

			if(empty($data['bom-item-no'])){
				$output['success'] = false;
				$output['message'] = 'no-item';
				echo json_encode($output);	
				return;
			}
			
			if(!isset($_POST['bom-details'])){
				$output['success'] = false;
				$output['message'] = 'no-detail';
				echo json_encode($output);	
				return;
			}

			if(empty($data['bom-description'])){
				$output['success'] = false;
				$output['message'] = 'bom-desc-null';
				echo json_encode($output);	
				return;
			}


			if($this->input->post('todo') == 'add'){

				if($bom_model->getCountByBOMDescriptionBomNo($_POST['bom-description'])){
					$output['success'] = false;
					$output['message'] = 'bom-desc-exists';
				}
				
				if($output['success']){

					$this->db->trans_begin();

					$noseries_model = $this->load_model('administration/master_file/bom/noseries_model');
					$bom_series = $noseries_model->getNextAvailableNumber($this->number_series, true,$_POST['bom-location']);
					$current_user = getCurrentUser()['login-user'];
					$bom_item = $item_model->getByItemID($_POST['bom-item-no']);


					foreach($_POST['bom-details'] as $key => $bom_detail){

						$_bom_detail = array(
							'BOMD_BOM_DocNo'		=> $bom_series
						,	'BOMD_LineNo'			=> $key+1
						,	'BOMD_ItemNo'			=> $bom_detail['item-no']
						,	'BOMD_Qty'				=> $bom_detail['quantity']
						,	'BOMD_UomID'			=> $bom_detail['uom']
						,	'BOMD_AverageCost'		=> '0'
						,	'BOMD_UnitCost'			=> $bom_detail['unit-cost']
						,	'BOMD_TotalCost'		=> $bom_detail['total-cost']
						,	'BOMD_Waste'			=> $bom_detail['waste']
						,	'BOMD_RequiredQty'		=> $bom_detail['required-qty']
						,	'BOMD_Comment'			=> $bom_detail['comment']
						,	'BOMD_BaseUomID'		=> $bom_detail['base-uom-id']
						,	'BOMD_BaseUomQty'		=> $bom_detail['base-uom-qty']
						,	'BOMD_isBOM'			=> $bom_detail['is-bom']
						,	'BOMD_BOMNo'			=> $bom_detail['bom-no']
						,	'CreatedBy'				=> $current_user
						,	'DateCreated'			=> date('Y-m-d H:i:s')
						);

						if($bom_detail['todo'] == 'add'){
							$bomdetail_model->insert($_bom_detail);
						}
					}

					$bom = array(
						'BOM_DocNo'					=>	$bom_series,
						'BOM_Location'				=>	$_POST['bom-location'],
						'BOM_DocDate'				=>	date('Y-m-d H:i:s'),
						'BOM_BOMDescription'		=>	$_POST['bom-description'],
						'BOM_ItemNo'				=>	$_POST['bom-item-no'],
						'BOM_YieldQty'				=>	$_POST['bom-yield'],
						'BOM_YieldUomID'			=>	$_POST['bom-yield-uom'],
						'BOM_BatchQty'				=>	$_POST['bom-batch-size'],
						'BOM_BatchUomID'			=>	$_POST['bom-batch-size-uom'],
						'BOM_Status'				=>	'Open',
						'BOM_Version' 				=>  $this->getLatestVersion($_POST['bom-item-no']),
						'BOM_Remarks'				=>	$_POST['bom-remarks'],
						'CreatedBy'					=>	$current_user,
						'DateCreated'				=>	date('Y-m-d H:i:s'),
						'ModifiedBy'				=>	$current_user,
						'DateModified'				=>	date('Y-m-d H:i:s')
						);


					$result = $bom_model->insert($bom);

					//validation of saving
			        if($this->db->trans_status() === FALSE){
			                $this->db->trans_rollback();
			                $output = array(
			                                    'success'=>false
			                                ,   'message'=>$this->db->error()
			                            );
			        }
			        else{
			                $this->db->trans_commit();
			        }
				}
				
				echo json_encode($output);

			}
			else if($this->input->post('todo') == 'update'){

				$this->db->trans_begin();

				if($data['bom-yield'] == null || $data['bom-batch-size'] == null){
					$output['success'] = false;
					$output['message'] = 'bom-yield-batch-null';
				}

				if(empty($data['bom-description'])){
					$output['success'] = false;
					$output['message'] = 'bom-desc-null';
				}

				$current_user = getCurrentUser()['login-user'];
				$bom_item = $item_model->getByItemID($_POST['bom-item-no']);
				$bom_series = $_POST['doc-no'];

				$this->db->delete('tblBOMDetail',array('BOMD_BOM_DocNo'=>$bom_series));
				
				foreach($_POST['bom-details'] as $key => $bom_detail){					

					$_bom_detail = array(
						'BOMD_BOM_DocNo'		=> $bom_series
					,	'BOMD_LineNo'			=> $key + 1
					,	'BOMD_ItemNo'			=> $bom_detail['item-no']
					,	'BOMD_Qty'				=> $bom_detail['quantity']
					,	'BOMD_UomID'			=> $bom_detail['uom']
					,	'BOMD_AverageCost'		=> '0'
					,	'BOMD_UnitCost'			=> $bom_detail['unit-cost']
					,	'BOMD_TotalCost'		=> $bom_detail['total-cost']
					,	'BOMD_Waste'			=> $bom_detail['waste']
					,	'BOMD_RequiredQty'		=> $bom_detail['required-qty']
					,	'BOMD_Comment'			=> $bom_detail['comment']
					,	'BOMD_BaseUomID'		=> $bom_detail['base-uom-id']
					,	'BOMD_BaseUomQty'		=> $bom_detail['base-uom-qty']
					,	'BOMD_isBOM'			=> $bom_detail['is-bom']
					,	'BOMD_BOMNo'			=> $bom_detail['bom-no']
					,	'CreatedBy'				=> $current_user
					,	'DateCreated'			=> date('Y-m-d H:i:s')
					);

					$this->db->insert('tblBOMDetail',$_bom_detail);
				}

				$bom = array(
						'BOM_DocNo'					=>	$bom_series,
						'BOM_DocDate'				=>	date('Y-m-d H:i:s'),
						'BOM_Location'				=>	$_POST['bom-location'],
						'BOM_BOMDescription'		=>	$_POST['bom-description'],
						'BOM_ItemNo'				=>	$_POST['bom-item-no'],
						'BOM_YieldQty'				=>	$_POST['bom-yield'],
						'BOM_YieldUomID'			=>	$_POST['bom-yield-uom'],
						'BOM_BatchQty'				=>	$_POST['bom-batch-size'],
						'BOM_BatchUomID'			=>	$_POST['bom-batch-size-uom'],
						'BOM_Status'				=>	'Open',
						'BOM_Version' 				=>  $_POST['bom-version'],
						'BOM_Remarks'				=>	$_POST['bom-remarks'],
						'ModifiedBy'				=>	$current_user,
						'DateModified'				=>	date('Y-m-d H:i:s')
					);
				$where = array('BOM_DocNo = '=>	$bom_series);
				$result = $bom_model->update($bom, $where);

				//validation of saving
		        if($this->db->trans_status() === FALSE){
		                $this->db->trans_rollback();
		                $output = array(
		                                    'success'=>false
		                                ,   'message'=>$this->db->error()
		                            );
		        }
		        else{
		                $this->db->trans_commit();
		        }

				echo json_encode($output);
			}
		}
	}

	public function data(){

		$order_forecast_model = $this->load_model('administration/master_file/bom/bom_model');		
		$BOM_access = in_array('BOM',$this->data['access_detail']);
		$table_data = $order_forecast_model->table_data($BOM_access);
		$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
			
			if(in_array('View', $access_detail)){
				$button .= ' <a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" 
										title="View" type="button" href="'.DOMAIN.'administration/master_file/bom/view?id='.md5($value['BOM_DocNo']).'"><i class="fa fa-eye"></i>
							</a>';
			}
			
			if(($value['BOM_Status'] != 'Pending' && $value['BOM_Status'] != 'Active') && in_array('Edit', $access_detail)){
				$button .= ' <a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" 
									title="Update" type="button" href="'.DOMAIN.'administration/master_file/bom/update?id='.md5($value['BOM_DocNo']).'"><i class="fa fa-pencil"></i>
								</a>';
			}

			if($value['BOM_Status'] == 'Approved' || $value['BOM_Status'] == 'Inactive'){
				$button .= ' <a href="#" class="btn btn-warning btn-xs btn-outline btnActive" 
									data-version="'.$value['BOM_Version'].'" data-id="'.$value['BOM_ItemNo'].'" 
									data-bom-no="'.$value['BOM_DocNo'].'" data-toggle="tooltip" data-placement="bottom" title="Activate">
									<i class="fa fa-check"></i>					
								</a>';
			}

			if($value['BOM_Status'] == 'Active'){
				$button .= ' <a href="#" class="btn btn-danger btn-xs btn-outline btnDeactive" 
									data-version="'.$value['BOM_Version'].'" data-id="'.$value['BOM_ItemNo'].'" 
									data-bom-no="'.$value['BOM_DocNo'].'" data-toggle="tooltip" data-placement="bottom" title="Deactivate">
									<i class="fa fa-times"></i>					
								</a>';
			}


			$sub_array[] = $button;
			$sub_array[] = $value['BOM_DocNo'];
			$sub_array[] = $value['BOM_ItemNo'];
			$sub_array[] = $value['BOM_BOMDescription'];
			$sub_array[] = $value['Yield'];
			$sub_array[] = $value['BOM_Status'];
			$sub_array[] = $value['BOM_Remarks'];
			
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function data_view(){
		$doc_no = $this->input->get('id');

		$bom_detail_model = $this->load_model('administration/master_file/bom/bomdetail_model');		
		$table_data = $bom_detail_model->table_data_bomdetail($doc_no);		

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();

			$sub_array[] = $value['BOMD_ItemNo'];
			$sub_array[] = $value['IM_Sales_Desc'];
			$sub_array[] = number_format($value['BOMD_Qty'], 4);
			$sub_array[] = $value['AD_Code'];
			$sub_array[] = number_format($value['BOMD_UnitCost'], 2);
			$sub_array[] = number_format($value['BOMD_TotalCost'], 2);
			$sub_array[] = number_format($value['BOMD_Waste'], 2);
			$sub_array[] = number_format($value['BOMD_RequiredQty'], 4);
			$sub_array[] = $value['BOMD_Comment'];
			
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	private function getLatestVersion($item_no){
		$result = $this->db->where('BOM_ItemNo',$item_no)
							->get('tblBOM')->num_rows();
		if($result > 0){
			return $result + 1;
		}
		else{
			return 1;
		}
	}
public function print_document(){
		$bomdetail_model = $this->load_model('/administration/master_file/bom/bomdetail_model');

		$this->data['doc_no'] = md5($this->input->get('id'));
		$data['header'] = $this->getData('view' ,$this->data['doc_no']);
		$data['bom_detail'] = $bomdetail_model->getBOMDetails($this->data['doc_no']);
		$html = $this->load->view('administration/master_file/billofmaterial/printout', $data, true);

		$this->generate(
    			array(	'format'=>'Letter',
    					'orientation'=>'P',
    					'html'=>$html,
    					'auto_top_margin' => 'pad'
  		));
	}
	
	/*
		AJAX Request ends here
	*/
}
