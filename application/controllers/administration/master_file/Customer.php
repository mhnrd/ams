<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Customer extends MAIN_Controller{

	protected $number_series		= 	103002;
	protected $header_table			=	'tblCustomer';
	protected $header_doc_no_field	=	'C_DocNo';
	protected $header_status_field	=	'C_Active';
	protected $module_name			=	'Customer';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=> 'Administration',
										'url'	=> 'administration');
		$this->breadcrumb[] = array(	'html'	=> 'Master File',
										'url'	=> '');

		$this->data['header_table']			=	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		= 	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						= 	array('C_DocNo','C_Name','C_CompanyName','C_TIN_No','C_Active', 'C_Address1', 'C_City', 'C_FaxNum', 'C_PostalCode', 'C_Country', 'C_FK_CustomerType', 'C_FK_PayTerms', 'C_BillTo', 'C_Status', 'C_ContactName', 'C_ContactTitle', 'C_Email', 'C_BankName', 'C_BankAddress','C_WHTPostingGroup', 'C_VATPostingGroup', 'C_TelNum', 'C_CreditLimit','C_CustomerPostingGroup', 'C_CreditBalance', 'C_FK_Attribute_Currency_id','C_AccountNo','C_BalanceAsOf','C_BillDate','C_Status','C_ID');
		//$this->table 						=   'tblCustomer as C';
	}

	public function index(){
		$this->breadcrumb[]	= array(	'html'	=> 'Principal',
										'url'	=> 'administration/master_file/customer');

		$this->data['table_hdr'] = array('customer_details'	=> array(
												'buttons' 		=> array(
																			'title' 	=>	'<a class="btn btn-success btn-xs btn-outline" 
																						data-toggle="tooltip" data-placement="top" title="Add" type="button" href="'.DOMAIN.'administration/master_file/customer/add"><i class="fa fa-plus"></i></a>',
																			'class' 	=>	'text-center',
																			'style' 	=>	'width: 100px; vertical-align: middle' 
																		),
												'checkbox' 		=>	array(	
																			'title' 	=>	'<input type="checkbox" id="chkSelectAll" 
																							class="icheckbox_square-green"',
																			'class' 	=>	'text-center',
																			'style' 	=>	'width: 10px; vertical-align: middle' 
																		),
												'principal_id' 	=> 	array(
																			'title' 	=>	'Principal ID',
																			'class' 	=>	'text-text',
																			'style' 	=>	'width: 20%; vertical-align: middle' 
																	), 
												'principal_name' => 	array(
																			'title' 	=>	'Principal Name',
																			'class' 	=>	'text-text',
																			'style' 	=>	'width: 30%; vertical-align: middle' 
																		),
												'company_name' 	=>	array(
																			'title' 	=>	'Company Name',
																			'class' 	=> 	'text-text',
																			'style' 	=>	'width: 45%; vertical-align: middle' 
																		),	
												'status' 		=> 	array(
																			'title' 	=>	'Status',
																			'class' 	=>	'text-text',
																			'style' 	=>	'width: 5%; vertical-align: middle' 
																		),
											)
									);

		$this->data['dtl_center'] = array(0,1,5);

		$this->load_page('/administration/master_file/customer/index');

	}

	public function add(){
		$this->breadcrumb[]	= array(	'html'	=>	'Principal',
										'url'	=> 	'administration/master_file/customer');
		$this->breadcrumb[]	= array(	'html'	=>	'Add',
										'url'	=> 	'');

		// LOAD NEEDED DATA
		$noseries_model 	= $this->load_model('administration/master_file/bom/noseries_model');
		$customer_series 	= $noseries_model->getNextAvailableNumber($this->number_series, false, '');

		$customer_model		= $this->load_model('administration/master_file/customer/customer_model');

		// STRUCTURE FOR VIEW PAGE
		$this->data['bill_to_list'] 			= $customer_model->getActiveBillTo();
		$this->data['bill_to_list'] 			= $customer_model->getActiveBillTo();
		$this->data['cust_post_list'] 			= $customer_model->getActiveCPG();
		$this->data['cust_type_list']			= $customer_model->getActiveCustomerType();
		$this->data['currency_list']			= $customer_model->getActiveCurrency();
		$this->data['wht_list']					= $customer_model->getActiveWHT();
		$this->data['vat_list']					= $customer_model->getActiveVATPostingGroup();
		$this->data['pay_terms_list']			= $customer_model->getActivePaymentTerms();	
			

		$this->data['type'] 					= 'add';
		$this->data['docno'] 					= $customer_series;
		$this->data['doc_date'] 				= Date('m/d/y');
		$this->data['header'] 					= $this->getData($this->data['type']);

		$this->load_page('/administration/master_file/customer/form');
	}

	public function update(){
		$this->breadcrumb[] = array('html'	=>'Principal',	
									'url'	=> 'administration/master_file/customer');
		$this->breadcrumb[] = array('html'	=>'Update',					
									'url'	=> '');

		// LOAD NEEDED DATA
		$noseries_model = $this->load_model('administration/master_file/bom/noseries_model');
		$customer_series = $noseries_model->getNextAvailableNumber($this->number_series, false, '');
		$customer_model = $this->load_model('administration/master_file/customer/customer_model');
		
		// STRUCTURE FOR VIEW PAGE
		$this->data['bill_to_list'] 			= $customer_model->getActiveBillTo();
		$this->data['cust_post_list'] 			= $customer_model->getActiveCPG();
		$this->data['cust_type_list']			= $customer_model->getActiveCustomerType();
		$this->data['currency_list']			= $customer_model->getActiveCurrency();
		$this->data['wht_list']					= $customer_model->getActiveWHT();
		$this->data['vat_list']					= $customer_model->getActiveVATPostingGroup();
		$this->data['pay_terms_list']			= $customer_model->getActivePaymentTerms();	
		
		$this->data['type'] = 'update';
		$this->data['docno'] = $_GET['id'];
		$this->data['header']    = $this->getData($this->data['type'], $this->data['docno']);
		$this->load_page('administration/master_file/customer/form');
	}

	public function view(){
		$this->breadcrumb[] = array('html'	=>'Principal',	
									'url'	=> 'administration/master_file/customer');
		$this->breadcrumb[] = array('html'	=>'View',					
									'url'	=> '');

		// LOAD NEEDED DATA
		$noseries_model = $this->load_model('administration/master_file/bom/noseries_model');
		$customer_series = $noseries_model->getNextAvailableNumber($this->number_series, false,'');
		$customer_model = $this->load_model('administration/master_file/customer/customer_model');

		// STRUCTURE FOR VIEW PAGE
		$this->data['bill_to_list'] 			= $customer_model->getActiveBillTo();
		$this->data['cust_post_list'] 			= $customer_model->getActiveCPG();
		$this->data['cust_type_list']			= $customer_model->getActiveCustomerType();
		$this->data['currency_list']			= $customer_model->getActiveCurrency();
		$this->data['wht_list']					= $customer_model->getActiveWHT();
		$this->data['vat_list']					= $customer_model->getActiveVATPostingGroup();
		$this->data['pay_terms_list']			= $customer_model->getActivePaymentTerms();	
	
		$this->data['type'] = 'view';
		$this->data['docno'] = $_GET['id'];
		$this->data['header'] = $this->getData($this->data['type'], $this->data['docno']);
		$this->load_page('/administration/master_file/customer/form');
	}

	public function getData($type, $docno = ''){

		$noseries_model = $this->load_model('administration/master_file/bom/noseries_model');
		
		$cust_docno = $noseries_model->getNextAvailableNumber($this->number_series, false, '');

		if($type == 'add'){
			$output = array(
				'C_DocNo' 					 => $cust_docno, 
				'C_ID' 						 => '', 
				'C_Name'	               	 => '',
				'C_CompanyName'              => '',
				'C_Address1'		         => '',
				'C_City'            		 => '',
				'C_FaxNum'         		 	 => '',
				'C_PostalCode'               => '',
				'C_Country'              	 => '',
				'C_FK_CustomerType' 		 => '',
				'C_FK_PayTerms'       		 => '',
				'C_BillTo' 				     => '',
				'C_BillDate'				 => '',
				'C_FK_Attribute_Currency_id' => '',
				'C_Status'                   => 'Posted',
				'C_Active'                   => 1,
				'C_ContactName' 			 =>	'',
				'C_TelNum' 					 =>	'',
				'C_ContactTitle'  			 => '',
				'C_Email' 					 => '',
				'C_BankName' 				 =>	'',
				'C_BankAddress' 			 => '',
				'C_CreditLimit' 			 => '',
				'C_CreditBalance' 			 => '',
				'C_CustomerPostingGroup' 	 => '',
				'C_WHTPostingGroup' 		 => '',
				'C_VATPostingGroup' 		 => '', 
				'C_TIN_No'  				 => '',
				'C_AccountNo'				 => '',
				'C_BalanceAsOf' 			 => '0.00', 

			);
		}
		else if($type == 'update' || $type == 'view'){

			$where = array(
				'C_DocNo' => $docno
			);

			$join_table = array(
				array(
					'tbl_name' => 'tblAttributeDetail',
					'tbl_on'   => 'AD_ID = C_FK_Attribute_Currency_id',
					'tbl_join' => 'left'
				),
			);

			$customer_model = $this->load_model('administration/master_file/customer/customer_model');

			$header = $customer_model->getHeaderByDocNo($this->cols, $this->header_table, $where, $join_table);

			$output = array(
				'C_DocNo'                 	 => $header['C_DocNo'], 	
				'C_Name'                	 => $header['C_Name'],
				'C_CompanyName'              => $header['C_CompanyName'],
				'C_Address1'        		 => $header['C_Address1'],
				'C_City'          		     => $header['C_City'],
				'C_FaxNum'                   => $header['C_FaxNum'],
				'C_PostalCode'               => $header['C_PostalCode'],
				'C_Country'              	 => $header['C_Country'],
				'C_FK_CustomerType'		 	 => $header['C_FK_CustomerType'],
				'C_FK_PayTerms' 		     => $header['C_FK_PayTerms'],
				'C_BillTo'                   => $header['C_BillTo'],
				'C_BillDate' 				 => $header['C_BillDate'], 
				'C_FK_Attribute_Currency_id' => $header['C_FK_Attribute_Currency_id'],
				'C_Status'                   => $header['C_Status'],
				'C_Active'                   => $header['C_Active'],
				'C_ContactName'              => $header['C_ContactName'],
				'C_TelNum'                   => $header['C_TelNum'],
				'C_ContactTitle'             => $header['C_ContactTitle'],
				'C_Email'                    => $header['C_Email'],
				'C_BankName'                 => $header['C_BankName'],
				'C_BankAddress'              => $header['C_BankAddress'],
				'C_CreditLimit'              => number_format($header['C_CreditLimit'],2, ".", ""),
				'C_CreditBalance'            => number_format($header['C_CreditBalance'],2, ".", ""),
				'C_CustomerPostingGroup' 	 => $header['C_CustomerPostingGroup'],
				'C_WHTPostingGroup' 		 => $header['C_WHTPostingGroup'],
				'C_VATPostingGroup' 		 => $header['C_VATPostingGroup'], 
				'C_TIN_No'                   => $header['C_TIN_No'],
				'C_AccountNo'				 => $header['C_AccountNo'],
				'C_BalanceAsOf' 			 => number_format($header['C_BalanceAsOf'],2), 
				'functions' 				 => $this->getApprovalButtons($header['C_DocNo']
																,$header['C_Active']
																,$header['C_Name']
																,getCurrentUser()['login-user']
																,$this->header_table
																,$this->header_status_field
																,$this->header_doc_no_field
															),
			);

			if($output['C_Status'] == 'Approved'){
				unset($output['functions'][1]);
			}

		}

		return $output;
	}

	public function data(){

		$customer_model = $this->load_model('administration/master_file/customer/customer_model');		
		$table_data = $customer_model->table_data();
		//$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/master_file/customer/view?id='.md5($value['C_DocNo']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/master_file/customer/update?id='.md5($value['C_DocNo']).'">
									<i class="fa fa-pencil"></i>
								</a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['C_DocNo'].'"><i class="fa fa-trash"></i></button> ';
			//}

			$sub_array[] = $button;										
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['C_DocNo'].'">';
			$sub_array[] = $value['C_DocNo'];
			$sub_array[] = $value['C_Name'];
			$sub_array[] = $value['C_CompanyName'];
			$sub_array[] = $value['C_Active'];

			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;
		
		if(!empty($_POST)):

			// Get all POST Data
			$data = $this->input->post();
			$data['C_ID'] = $data['C_DocNo'];
			$data['C_CreditLimit']   = str_replace(",", "", $data['C_CreditLimit']);
			$data['C_CreditBalance'] = str_replace(",", "", $data['C_CreditBalance']);
			// $data['C_BillDate'] = str_replace("M", "1",$data['C_BillDate']);
			// $data['C_BillDate'] = str_replace("SM", "2",$data['C_BillDate']);

			$customer_model = $this->load_model('administration/master_file/customer/customer_model');

			// GET CURRENT LOGIN USER
			$current_user = getCurrentUser()['login-user'];						


			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){
					// GET THE LATEST NO SERIES
					
					$output = $this->error_message(true, false, false, false, $data, $data['C_DocNo'], $this->number_series, '', 'C', 'tblCustomer');

					if($output['success']){
						// UPDATE NO SERIES

						$table = array(
							'header'  	=> $this->header_table,
						);

						$this->db->trans_begin();

						$customer_model->on_save_module(true, $data, $table, 'C', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					
					}else{
						throw new Exception($output['message']);
					}
					
					/* 
							END OF SAVING - ADD FUNCTION
					*/

				
				// UPDATING HEADER	
				}
				else if($this->input->post('todo') == 'update'){

					$output = $this->error_message(true, false, false, false, $data, $data['C_DocNo'], $this->number_series, '', 'C', 'tblCustomer');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table
						);

						$this->db->trans_begin();

						$customer_model->on_update_module($data, $table, 'C', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['C_DocNo']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;			

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/			
				}
				echo json_encode($output); //ENDING QUERY
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}

	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}
}