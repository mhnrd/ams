<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Company extends MAIN_Controller{

	protected $number_series		=	103001;
	protected $header_table			=	'tblCompany';
	protected $header_doc_no_field	=	'COM_Id';
	protected $header_status_field	=	'COM_Active';
	protected $module_name 			=	'Company';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=>	'Administration',
										'url'	=>	'administration');
		$this->breadcrumb[] = array(	'html'	=> 'Master File',
										'url'	=> '');

		$this->data['header_table']			=	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		=	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->cols 						=   array('COM_Id','COM_Name','COM_Address','COM_PhoneNo','COM_FaxNum','COM_Email', 'COM_Active', 'COM_Website', 'COM_BusinessType', 'COM_Tin', 'COM_SSSNo', 'COM_PhilhealthNo', 'COM_PagibigNo', 'COM_FY_Start', 'COM_FY_End', 'COM_Currency');
	}

	// table header
	public function index(){
		$this->breadcrumb[] = array(	'html' => 'Company',
										'url'	=>	'administration/master_file/company');

		$this->data['table_hdr'] = array('company_details'	=> array(
												'buttons' 		=> array(
																			'title' 	=>	'<a class="btn btn-success btn-xs btn-outline" 
																							data-toggle="tooltip" data-placement="top" 
																							title="Add" type="button" href="'.
																							DOMAIN.'administration/master_file/company/add"><i class="fa
																							fa-plus"></i></a>',
																			'class' 	=>	'text-center',
																			'style' 	=>	'width: 100px; vertical-align: middle' 
																		),
												'checkbox' 		=>	array(	
																			'title' 	=>	'<input type="checkbox" 
																							id="chkSelectAll" 
																							class="icheckbox_square-green"',
																			'class' 	=>	'text-center',
																			'style' 	=>	'width: 10px; vertical-align: middle' 
																		),
												'company_id' 	=> 	array(
																			'title' 	=>	'Company ID',
																			'class' 	=>	'text-left',
																			'style' 	=>	'width: 10%; vertical-align: middle' 
																		), 
												'company_name' => 	array(
																			'title' 	=>	'Company Name',
																			'class' 	=>	'text-left',
																			'style' 	=>	'width: 20%; vertical-align: middle' 
																		),
												'address' 	=>	array(
																			'title' 	=>	'Address',
																			'class' 	=> 	'text-left',
																			'style' 	=>	'width: 20%; vertical-align: middle' 
																		),
												'phone_no' 		=>	array(
																			'title' 	=>	'Phone No',
																			'class' 	=>	'text-left',
																			'style' 	=>	'width: 10%; vertical-align: middle' 
																		),
												'fax_no' 		=>	array(
																			'title' 	=>	'Fax No',
																			'class' 	=>	'text-left',
																			'style' 	=>	'width: 10%; vertical-align: middle' 
																		),
												'email' 		=>	array(
																			'title' 	=>	'Email',
																			'class' 	=>	'text-left',
																			'style' 	=>	'width: 20%; vertical-align: middle' 
																		), 	
												'status' 		=> 	array(
																			'title' 	=>	'Status',
																			'class' 	=>	'text-left',
																			'style' 	=>	'width: 10%; vertical-align: middle' 
																		),
											)
									);

		$this->data['hdr_center'] = array(0,1);

		$this->load_page('/administration/master_file/company/index');
	}

	// Adding of user
	public function add(){
		$this->breadcrumb[] = array(	'html'	=>	'Company',
										'url'	=>	'administration/master_file/company');
		$this->breadcrumb[]	= array(	'html'	=>	'Add',
										'url'	=>	'');

		// Load needed data
		$currency_list 		= $this->load_model('administration/master_file/company/company_model');

		// Structure for add page
		$this->data['currency_list'] = $currency_list->getActiveCurrency();

		$this->data['type'] 		 = 'add';
		$this->data['header'] 	     = $this->getData($this->data['type'], '');

		$this->load_page('/administration/master_file/company/form');
	}

	// Updating of data
	public function update(){
		$this->breadcrumb[] = array('html'	=> 'Company',	
									'url'	=> 'administration/master_file/company');
		$this->breadcrumb[] = array('html'	=> 'Update',					
									'url'	=> '');

		// Load needed data
		$currency_list 		= $this->load_model('administration/master_file/company/company_model');

		// Structure for update page
		$this->data['currency_list'] = $currency_list->getActiveCurrency(); 

		$this->data['type'] 		 = 'update';
		$this->data['id']		     = $_GET['id'];
		$this->data['header']        = $this->getData($this->data['type'], $this->data['id']);

		
		$this->load_page('administration/master_file/company/form');
	}

	// Viewing of data
	public function view(){
		$this->breadcrumb[] = array('html'	=>'Company',	
									'url'	=> 'administration/master_file/company');
		$this->breadcrumb[] = array('html'	=>'View',					
									'url'	=> '');	

		// Load needed data
		$currency_list 		= $this->load_model('administration/master_file/company/company_model');

		// Structure for view page
		$this->data['currency_list'] = $currency_list->getActiveCurrency(); 

		$this->data['type'] 		 = 'view';
		$this->data['id'] 			 = $_GET['id'];
		$this->data['header']    	 = $this->getData($this->data['type'], $this->data['id']);

		$this->load_page('/administration/master_file/company/form');
	}

	// Getting of data from the Form
	public function getData($type, $docno = ''){

		$company_model = $this->load_model('administration/master_file/company/company_model');

		if($type == 'add'){
			$output = array(
				'COM_Id' 				=>	'',
				'COM_Name' 	 			=> 	'',
				'COM_Address' 			=> 	'',
				'COM_PhoneNo' 			=> 	'',
				'COM_FaxNum' 			=>	'',
				'COM_Email' 			=>	'',
				'COM_Active' 			=> 	1,
				'COM_Website' 			=>	'',
				'COM_BusinessType' 		=>	'',
				'COM_Tin' 				=>	'',
				'COM_SSSNo' 			=>	'',
				'COM_PhilhealthNo' 		=>	'',
				'COM_PagibigNo' 		=>	'',
				'COM_FY_Start' 			=>	'',
				'COM_FY_End' 			=>	'',
				'COM_Currency' 			=>	'',	
			);

		}
		else if($type == 'update' || $type == 'view'){

			$where = array(
				'COM_Id' => $docno
			);

			$header = $company_model->getHeaderByDocNo($this->cols, $this->header_table, $where);

			$output = array(
				'COM_Id' 				=>	$header['COM_Id'],
				'COM_Name' 				=>	$header['COM_Name'],
				'COM_Address' 			=>	$header['COM_Address'],
				'COM_PhoneNo'  			=> 	$header['COM_PhoneNo'],
				'COM_FaxNum' 			=>	$header['COM_FaxNum'],
				'COM_Email' 			=>	$header['COM_Email'],
				'COM_Active' 			=>	$header['COM_Active'],
				'COM_Website' 			=>	$header['COM_Website'],	
				'COM_BusinessType' 		=>	$header['COM_BusinessType'],	
				'COM_Tin' 				=>	$header['COM_Tin'],	
				'COM_SSSNo' 			=>	$header['COM_SSSNo'],	
				'COM_PhilhealthNo' 		=>	$header['COM_PhilhealthNo'],
				'COM_PagibigNo' 		=>	$header['COM_PagibigNo'],
				'COM_FY_Start' 			=>  date_format(date_create($header['COM_FY_Start']),'m/d/Y'),
				'COM_FY_End' 			=>  date_format(date_create($header['COM_FY_End']),'m/d/Y'),
				'COM_Currency' 			=>  $header['COM_Currency'],
				

			);

		}
		return $output;
	}

	// Data to be Display in the Table
	public function data(){

		$company_model = $this->load_model('administration/master_file/company/company_model');
		$table_data = $company_model->table_data();
		//$access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';

			//if(in_array('View', $access_detail)){
				$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="View" type="button" href="'.DOMAIN.'administration/master_file/company/view?id='.md5($value['COM_Id']).'"><i class="fa fa-eye"></i></a> ';
			//}

			//if(in_array('Edit', $access_detail)){
				$button .= '<a class="btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" href="'.DOMAIN.'administration/master_file/company/update?id='.md5($value['COM_Id']).'"><i class="fa fa-pencil"></i></a> ';
			//}

			//if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['COM_Id'].'"><i class="fa fa-trash"></i></button> ';
			//}
		

			$sub_array[] = $button;
			$sub_array[] = '<input type="checkbox" name="chkSelect[]" value="'.$value['COM_Id'].'">';
			$sub_array[] = $value['COM_Id'];
			$sub_array[] = $value['COM_Name'];
			$sub_array[] = $value['COM_Address'];
			$sub_array[] = $value['COM_PhoneNo'];
			$sub_array[] = $value['COM_FaxNum'];
			$sub_array[] = $value['COM_Email'];
			$sub_array[] = $value['COM_Active'];

			$data[] = $sub_array;

		}
			$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function save(){
		if (!$this->input->is_ajax_request()) return;

		if(!empty($_POST)):

			// Get all post data
			$data = $this->input->post();
			
			// Load all needed model
			$company_model = $this->load_model('administration/master_file/company/company_model');

			// Get current user
			$current_user = getCurrentUser()['login-user'];

			$output = array(
							'success'=>true
						,	'message'=>'');
			try{

				// Start Of Saving - Add Function
				if($this->input->post('todo') == 'add'){

					$output = $this->error_message(false, true, false, false, $data, $data['COM_Id'], $this->number_series, '', 'COM', 'tblCompany');

					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table,
						);

						$this->db->trans_begin();

						$company_model->on_save_module(false, $data, $table, 'COM', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}
					
					/* 
							END OF SAVING - ADD FUNCTION
					*/
					
				// Update header
				}
				else if($this->input->post('todo') == 'update'){
				
					$output = $this->error_message(false, true, false, false, $data, $data['COM_Id'], $this->number_series, '', 'COM', 'tblCompany');

					if($output['success']):

						$table = array(
							'header'  	=> $this->header_table
						);

						$this->db->trans_begin();

						$company_model->on_update_module($data, $table, 'COM', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
							$this->deleteUserRecordLock_php($data['COM_Id']);
						}
						else{
							$this->db->trans_rollback();
						}
						
					else:
						throw new Exception($output['message']);
					endif;

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
				}
				echo json_encode($output); //Ending query
			} 
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
						'success'  	=> false,
						'message'  	=> $array_msg[0],
						'title'  	=> $array_msg[1],
						'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
	}


	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}
}