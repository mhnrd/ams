<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Doc_approval extends MAIN_Controller {

	public function __construct(){
		parent::__construct();		
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=>'Approval Setup',
						'url'	=> '');
	}

	public function index(){
		$this->breadcrumb[] = array(	'html'	=>'Document Approval',
						'url'	=> 'approval_setup/doc_approval');
		// $this->data['barcode_printing_list'] = $itemcat_model->getAll();

		$this->load_page('/administration/doc_approval/index');
	}


	/*
		AJAX Request goes here
	*/

	public function data(){
		$doc_approval_model = $this->load_model('administration/doc_approval/doc_approval_model');
		$table_data = $doc_approval_model->table_data();
		$data = array();

		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$button = '';
			$button .= '<a class="btn btn-success btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" 
									title="View" type="button" href="'.$value['DT_TargetURL'].'view?id='.md5($value['DT_DocNo']).'&docapv=1"><i class="fa fa-eye"></i>
						</a>';

			$sub_array[] = $button;
			$sub_array[] = $value['DT_DocNo'];
			$sub_array[] = $value['Sender'];
			$sub_array[] = $value['Approver'];
			$sub_array[] = $value['SP_StoreName'];
			$sub_array[] = date_format(date_create($value['DT_EntryDate']), 'm/d/Y');
			
			$data[] = $sub_array;
		}
		
		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}

	public function getNotification(){

		header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

		$doc_approval_model = $this->load_model('administration/doc_approval/doc_approval_model');
		$table_data = $doc_approval_model->notifications();
		echo json_encode($table_data);

		ob_end_flush();
        flush();
        // sleep(1);
	}

	public function getCountNotif(){
		$doc_approval_model = $this->load_model('administration/doc_approval/doc_approval_model');
		$table_data = $doc_approval_model->count_approval();

		// $this->print_r($table_data);

		echo json_encode($table_data);
	}
	
	/*
		AJAX Request ends here
	*/
}
