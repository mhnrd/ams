<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Encrypt\SGEncryption;
class User extends MAIN_Controller {

	public function __construct(){
		parent::__construct();		
		$this->is_secure = true;
	}

	public function getBomInfo(){
		if (!$this->input->is_ajax_request()) return;
		$data = array();
		$bom_model = $this->load_model('master_file/bom/bom_model');
		$data = $bom_model->getByBOMDocNoWithUOM($this->input->get('bom-doc-no'));
		echo json_encode($data);
	}

	public function getUser(){
		if (!$this->input->is_ajax_request()) return;
		$filter = $_GET['filter-search'];
		$action = $_GET['action'];
		$data = array();
		$user_model = $this->load_model('administration/master_file/bom/user_model');
		if($action=='search'){
			$data = $user_model->getAllBySearch($filter);
		}
		else if($action=='Select'){
			$data = $user_model->getByFilter($filter);
		}
		
		echo json_encode($data);
	}

	public function login(){
		$this->is_secure = false;
		if(!empty($_POST)){			
			$encryptor = new SGEncryption();
			$user_model = $this->load_model('administration/master_file/bom/user_model');
			$user = $user_model->getUserByUsernameAndPassword($_POST['username'], $encryptor->Encrypt($_POST['password']));
			if(!empty($user)){
				$this->session->unset_userdata('alert');
				$access = array(
	                'all'                        	=> $user_model->getAllModules($user['U_ID']),
	                'document-approval' 			=> $user_model->getModulesPerParent($user['U_ID'],4000),
	                'production'             		=> $user_model->getModulesPerParent($user['U_ID'],7000),
	                'inventory'       				=> $user_model->getModulesPerParent($user['U_ID'],6000),
	                'master-files'     				=> $user_model->getModulesPerParent($user['U_ID'],1600),
	                'application-setup' 			=> $user_model->getModulesPerParent($user['U_ID'],1200),
	                'reports'                    	=> $user_model->getModulesPerParent($user['U_ID'],5000),
	                'ess'                    		=> $user_model->getModulesPerParentESS($user['U_ID'],602000),
	            );

				$data = array(	
									'user' 						=> $user['U_ID'],
									'username'					=> $user['U_Username'],
									'position' 					=> $user['U_FK_Position_id'],
									'position_name' 			=> $user['P_Position'],
									'employee_id' 				=> $user['U_AssociatedEmployeeId'],
									'location' 					=> $user['Emp_FK_StoreID'],	
									'parent_userId'				=> $user['U_ParentUserID'],
									'module_access' 			=> $access,
									'document_approval_url'		=> DOMAIN."approval_setup/doc-approval"	
							);			
				$this->sess->setLogin($data);
				$this->deleteUserRecordLock(getCurrentUser()['login-user']); // Delete User Record Lock
			}
			else{
				$this->data['error_msg'] = "Invalid username and password!";
			}
		}		
		if(!$this->sess->isLogin()){
			$this->load_page('/user/login');
		}
		else{
			$this->redirect(DOMAIN.'dashboard');
		}
	}
	
	public function dashboard(){
		$this->load_page('/user/dashboard');
	}

	public function logout(){
		$this->deleteUserRecordLock(getCurrentUser()['login-user']); // Delete User Record Lock
		$this->sess->clearLogin();
		$this->redirect(DOMAIN.'user/login');
	}

	public function changepassword(){
		$encryptor = new SGEncryption();
		$user_model = $this->load_model('administration/master_file/bom/user_model');

		$oldpassword = $this->input->post('oldpassword');
		$newpassword = $this->input->post('newpassword');
		$blnValidate = $user_model->validateOldPassword(getCurrentUser()['login-user'],$encryptor->Encrypt($oldpassword));
		if(!$blnValidate){
			echo json_encode(array('success' => 0
								 , 'message'=> 'Wrong Input of Old Password')
							);
			return;
		}
		// Update Password of the user
		$user_model->updatePassword(getCurrentUser()['login-user'],$encryptor->Encrypt($newpassword));		
		echo json_encode(array('success' => 1, 'message'=> ''));
	}
}
