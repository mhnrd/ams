<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TrackDocument extends MAIN_Controller {

	public function __construct(){
		parent::__construct();		
		$this->is_secure = true;
		$this->breadcrumb[] = array(	'html'	=>'Track Document',
						'url'	=> '');
	}

	/*
		AJAX Request goes here
	*/
	
	public function getData(){
		$docNo = $this->input->get('docNo');
		$trackdocument = $this->load_model('com/track_document_model');
		echo json_encode($trackdocument->getAll($docNo));
	}

	public function process(){
		$data = $this->input->post();
		if(!isset($data['remarks'])){
			$data['remarks'] = '';
		}

		$trackdocument = $this->load_model('com/track_document_model');
		$ns_id = $trackdocument->getNSId($data['numberSeriesID'], $data['ns_location']);
		echo json_encode($trackdocument->process(
													 $data['action']
													,$data['numberSeriesID']
													,$data['DocumentNo']
													,$data['DocumentDate']
													,$data['DocumentAmount']
													,$data['SenderID']
													,$data['SenderLocation']
													,$data['header_table']
													,$data['header_doc_no_field']
													,$data['header_status_field']
													,$ns_id
													,$data['module_url']
													,$data['remarks']
												)
						);
	}

	public function data(){
		$doc_no = $this->input->get('id');
		$trackdocument = $this->load_model('com/track_document_model');		
		$table_data = $trackdocument->table_data($doc_no);

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
			$sub_array = array();
			$sub_array[] = $value['DT_DocNo'];
			$sub_array[] = !empty($value['DT_EntryDate']) ? date_format(date_create($value['DT_EntryDate']),'m/d/Y h:m:s') : "";
			// $sub_array[] = $value['DT_Sender'];
			$sub_array[] = $value['SP_StoreName'];
			$sub_array[] = $value['P_Position'];
			$sub_array[] = $value['DT_ApprovedBy'];
			$sub_array[] = !empty($value['DT_DateApproved']) ? date_format(date_create($value['DT_DateApproved']),'m/d/Y h:m:s') : "";
			$sub_array[] = $value['DT_Status'];
			$sub_array[] = $value['DT_Remarks'];
			$data[] = $sub_array;
		}

		$output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output);
	}
	
	/*
		AJAX Request ends here
	*/


}
