<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Attributes extends MAIN_Controller {

	protected $number_series		= 	400000;
	protected $header_table			=	'tblAttributeDetail';
	protected $header_doc_no_field	=	'AD_ID';
	protected $header_status_field	=	'AD_Active';

	public function __construct(){
		parent::__construct();
		$this->is_secure = true;

		$this->data['header_table']			=	$this->header_table;
		$this->data['header_doc_no_field']	=	$this->header_doc_no_field;
		$this->data['header_status_field']	=	$this->header_status_field;
		$this->data['number_series']		= 	$this->number_series;
		$this->data['access_header']		=	$this->user_model->getHeaderAccess($this->number_series);
		$this->data['access_detail']		=	$this->user_model->getDetailAccess($this->number_series);
		$this->data['module'] 				= 	getDisplayName($this->uri->segment(3));
		$this->data['module_icon'] 			= 	getModuleIcon($this->uri->segment(3));
		$this->cols 						= 	array('AD_ID', 'AD_Code', 'AD_Desc');
	}

	public function index(){
		//$this->user_model->deleteUserRecordLock(getCurrentUser()['login-user']); // Delete User Record Lock

		$this->data['table_hdr'] = array('attribute' =>	array(
												'buttons' 			=>	array(
																				'title' 	=>	'<button class="btn btn-success btn-xs 
																									btn-outline add-header" 
																									data-toggle="tooltip" data-placement="top" 
																									title="Add" type="button" ><i
																									class="fa
																									fa-plus"></i></button>',
																				'class' 	=>	'text-center',
																				'style' 	=>	'width: 15px; vertical-align:
																								middle' 
																			),
												'code' 				=>	array(
																				'title' 	=>	'Code',
																				'class' 	=>	'text-left',
																				'style' 	=>	'width: 20%; 
																					vertical-align: middle' 
																			),
												'description' 		=>	array(
																				'title' 	=>	'Description',
																				'class' 	=>	'text-left',
																				'style' 	=>	'width: 60%; 
																					vertical-align: middle' 
																			),
												'status' 			=>	array(
																				'title' 	=>	'Status',
																				'class' 	=>	'text-left',
																				'style' 	=>	'width: 20%; 
																					vertical-align: middle' 
																			), 
										)
							);

		$this->data['filter'] = array(

			array(
				"name"  => "Code",
				"value"  => 1
			),
			array(
				"name"  => "Description",
				"value"  => 2
			),
			array(
				"name"  => "Status",
				"value"  => 3
			)

		);

		$this->data['hdr_center']	= array(0,1);

		$this->load_page('asset_management/attributes/index');
	}

	public function data(){

		$attributes_model = $this->load_model('asset_management/attributes/attributes_model');
		$attr_id = $this->input->get('id');
		$attr_mod = $this->input->get('attr');

		$table_data = $attributes_model->table_data($attr_id);

		// $access_detail = $this->data['access_detail'];

		$data = array();
		foreach ($table_data['data'] as $key => $value) {
		 	$sub_array = array();
			$button = '';
			$json = json_encode($value);
			$json_data = htmlentities($json, ENT_QUOTES, 'UTF-8');

			// if(in_array('Edit', $access_detail)){
				$button .= '<button class="btn btn-primary btn-xs btn-outline update-header" data-toggle="tooltip" data-placement="bottom" title="Update" data-doc-no="'.$value['AD_ID'].'" data-record="'.$json_data.'">
									<i class="fa fa-pencil"></i>
								</button> ';
			// }

			// if(in_array('Delete', $access_detail)){
				$button .= '<button class="btn btn-danger btn-xs btn-outline delete-button" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" data-id="'.$value['AD_ID'].'"><i class="fa fa-trash"></i></button> ';

			$sub_array[] = $button;
			$sub_array[] = $value['AD_Code'];
			$sub_array[] = $value['AD_Desc'];
			$sub_array[] = $value['AD_Active'];
			$data[] = $sub_array;
		 }

		 $output = array(  
				"draw"            =>     intval($_POST["draw"]),  
				"recordsTotal"    =>     $table_data['count_all_results'],  
				"recordsFiltered" =>     $table_data['num_rows'],  
				"data"            =>     $data  
           );  

		echo json_encode($output); 
	}

	public function save(){
		
		if(!empty($_POST)):

			// Get all POST Data
			$data 					= $this->input->post();
			$attr_model = $this->load_model('/asset_management/attributes/attributes_model');

			$output = array(
							'success'=>true
						,	'message'=>'');
			try{
				if($this->input->post('todo') == 'add'){

					// SPECIAL CHECKING FOR ATTRIBUTES

					$data['AD_ID'] = $attr_model->getLastADCode();

					$output = $attr_model->checkAttrExists($data['AD_FK_Code'], $data['AD_Code']);

					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table, 
						);

						$this->db->trans_begin();

						$attr_model->on_save_module(false, $data, $table, 'AD', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

					
					/* 
							END OF SAVING - ADD FUNCTION
					*/
				}
				else if($this->input->post('todo') == 'update'){

					// $this->print_r($data);

					$output = $this->error_message(false, true, false, false, $data, $data['AD_ID'], $this->number_series, '', 'AD', 'tblAttributeDetail');

					$output = $this->error_message(false, true, false, false, $data, $data['AD_Code'], $this->number_series, '', 'AD', 'tblAttributeDetail');

					if($output['success']){

						$table = array(
							'header'  	=> $this->header_table, 
						);

						$this->db->trans_begin();

						$attr_model->on_update_module($data, $table, 'AD', $this->number_series, '');

						if($this->db->trans_status() !== False){
							$this->db->trans_commit();
						}
						else{
							$this->db->trans_rollback();
						}
					}
					else{
						throw new Exception($output['message']);
					}

						/* 
							END OF SAVING - UPDATE FUNCTION
						*/
					
					}
				
				echo json_encode($output);
			
			}
			catch(Exception $e){

				$str = $e->getMessage();
				$array_msg = explode(" , ", $str);

				$output = array(
					'success'  	=> false,
					'message'  	=> $array_msg[0],
					'title'  	=> $array_msg[1],
					'type'  	=> $array_msg[2] 
				);

				echo json_encode($output);
			}
		endif;
		
	}

	public function delete(){
		$doc_no = $this->input->get('id');

		$check_exist_docno = $this->checkIfDocNoExist($doc_no, $this->data['header_doc_no_field'], $this->data['header_table']);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($check_exist_docno)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'This ID has already been deleted',
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			$this->db->trans_begin();

			$this->db->where($this->data['header_doc_no_field'], $doc_no)
					 ->delete($this->data['header_table']);

			if($this->db->trans_status() !== false){
				$this->db->trans_commit();
			}
			else{
				$this->db->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function status_update(){

		$doc_no = $this->input->post('id');
		$action = $this->input->post('action');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if(empty($doc_no)){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'No items to '.$action,
				'title'  	=> 'Error',
				'type'  	=> 'error'
			);
		}
		else{
			if($action == 'activate'){

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 1)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
			else{

				$this->db->trans_begin();

				$this->db->where_in($this->data['header_doc_no_field'], $doc_no)
						 ->set($this->data['header_status_field'], 0)
						 ->update($this->data['header_table']);

				if($this->db->trans_status() !== false){
					$this->db->trans_commit();
				}
				else{
					$this->db->trans_rollback();
				}

			}
		}

		echo json_encode($output);

	}
}