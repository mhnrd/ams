<?php
namespace App\Mapper;
use Sys\Mapper\MCB_Mapper;
class BOMMapper extends MCB_Mapper{

	public function selectAll(){
		$sql_statement = "SELECT * 
						FROM tblBOM";	
		$stmt = $this->prepare($sql_statement);
		$stmt->execute(array(":BOM_DocNo"=>'asdasdasd'));
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $result;
	}

	public function selectByDocNo($DocNo){
		$sql_statement = "SELECT *
						FROM tblBOM 
						WHERE BOM_DocNo = :BOM_DocNo";	
		$stmt = $this->prepare($sql_statement);
		$stmt->execute(array(":BOM_DocNo"=>$DocNo));
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $result;
	}

	public function insert($data){
		$sql_statement = "INSERT INTO tblBOM( 
	 								BOM_DocNo, 
	 								BOM_DocDate,
	 								BOM_BOMDescription,
	 								BOM_ItemNo,
	 								BOM_Barcode,

	 								BOM_ItemDescription,
	 								BOM_YieldQty,
	 								BOM_YieldUOM,
	 								BOM_BatchQty,
	 								BOM_BatchUOM,

	 								BOM_Type,
	 								BOM_Remarks,
	 								BOM_Status,
	 								BOM_Location,
	 								CreatedBy,
	 								
	 								DateCreated,
	 								ModifiedBy,
	 								DateModified)
	 						VALUES 
	 							(  	:BOM_DocNo,
	 								:BOM_DocDate,
	 								:BOM_BOMDescription,
	 								:BOM_ItemNo,
	 								:BOM_Barcode,

	 								:BOM_ItemDescription,
	 								:BOM_YieldQty,
	 								:BOM_YieldUOM,
	 								:BOM_BatchQty,
	 								:BOM_BatchUOM,

	 								:BOM_Type,
	 								:BOM_Remarks,
	 								:BOM_Status,
	 								:BOM_Location,
	 								:CreatedBy,

	 								:DateCreated,
	 								:ModifiedBy,
	 								:DateModified) ";	

	 	$stmt = $this->prepare($sql_statement);
	 	$stmt->execute(array(
	 					':BOM_DocNo' => $data['BOM_DocNo'],
	 					':BOM_DocDate' => $data['BOM_DocDate'],
	 					':BOM_BOMDescription' => $data['BOM_BOMDescription'],
	 					':BOM_ItemNo' => $data['BOM_ItemNo'],
	 					':BOM_Barcode' => $data['BOM_Barcode'],

	 					':BOM_ItemDescription' => $data['BOM_ItemDescription'],
	 					':BOM_YieldQty' => $data['BOM_YieldQty'],
	 					':BOM_YieldUOM' => $data['BOM_YieldUOM'],
	 					':BOM_BatchQty' => $data['BOM_BatchQty'],
	 					':BOM_BatchUOM' => $data['BOM_BatchUOM'],

	 					':BOM_Type' => $data['BOM_Type'],
	 					':BOM_Remarks' => $data['BOM_Remarks'],
	 					':BOM_Status' => $data['BOM_Status'],
	 					':BOM_Location' => $data['BOM_Location'],
	 					':CreatedBy' => $data['CreatedBy'],

	 					':DateCreated' => $data['DateCreated'],
	 					':ModifiedBy' => $data['ModifiedBy'],
	 					':DateModified' => $data['DateModified'],
	 					
	 					));	
	 	return $this->lastInsertId();
	}


	// public function selectAll(){
	// 	$sql_statement = "SELECT * FROM dms_doc_movement";	
	// 	$stmt = $this->prepare($sql_statement);
	// 	$stmt->execute();
	// 	$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
	// 	return $result;
	// }
	
	// public function selectByID($id){	
	// 	$sql_statement = "SELECT * FROM dms_doc_movement WHERE dm_id = ?";	
	// 	$stmt = $this->prepare($sql_statement);
	// 	$stmt->execute(array($id));		
	// 	$result = $stmt->fetch(\PDO::FETCH_ASSOC);
	// 	return $this->loadDocMovement($result);
	// }	

	// public function insert($data){
	// 	$sql_statement = "INSERT INTO dms_doc_movement( 
	// 								dm_from_emp_id, 
	// 								dm_to_emp_id,
	// 								dm_date,
	// 								dm_read_date,
	// 								dm_file_id)
	// 						VALUES 
	// 							(  	:dm_from_emp_id,
	// 								:dm_to_emp_id,
	// 								:dm_date,
	// 								:dm_read_date,
	// 								:dm_file_id) ";	

	// 	$stmt = $this->prepare($sql_statement);
	// 	$stmt->execute(array(
	// 					':dm_from_emp_id' => $data->_from_emp_id,
	// 					':dm_to_emp_id' => $data->_to_emp_id,
	// 					':dm_date' => $data->_date,
	// 					':dm_read_date' => $data->_read_date,
	// 					':dm_file_id' => $data->_file_id
	// 					));	
	// 	return $this->lastInsertId();
	// }

	// public function updateByID($data){
	// 	$sql_statement = "UPDATE dms_doc_movement SET
	// 								dm_from_emp_id = :dm_from_emp_id,
	// 								dm_to_emp_id = :dm_to_emp_id,
	// 								dm_date = :dm_date,
	// 								dm_read_date = :dm_read_date,
	// 								dm_file_id = :dm_file_id
	// 								WHERE dm_id = :dm_id";
	// 	$stmt = $this->prepare($sql_statement);
	// 	$stmt->execute(array(
	// 					':dm_from_emp_id' => $data->_from_emp_id,
	// 					':dm_to_emp_id' => $data->_to_emp_id,
	// 					':dm_date' => $data->_date,
	// 					':dm_read_date' => $data->_read_date,
	// 					':dm_file_id' => $data->_file_id,
	// 					':dm_id' => $data->_id
	// 					));	
	// 	if( !$stmt->rowCount() ) {
	// 		return false;
	// 	}
	// 	return true;
	// }	

	// public function delete($id){
	// 	$sql_statement = "DELETE FROM dms_doc_movement WHERE dm_id = ?";
	// 	$stmt = $this->prepare($sql_statement);
	// 	$stmt->execute(array($id));
	// 	if( !$stmt->rowCount() ) {
	// 		return false;
	// 	}
	// 	return true;
	// }

	// private function loadDocMovement($row){
	// 	if(is_array($row)){
	// 		return new DocMovement( $row['dm_id'],
	// 						$row['dm_from_emp_id'],
	// 						$row['dm_to_emp_id'],
	// 						$row['dm_date'],
	// 						$row['dm_read_date'],
	// 						$row['dm_file_id']);	
	// 	} else return null;
	// }
	
}