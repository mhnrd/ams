<?php 
namespace App\Encrypt;
/*
Author Brent Macatangay
February 19, 2018
Based from Philip Corpuz Encryption

Origin Below
*/

class SGEncryption{
	private $_gkey = 456789;
	public function Encrypt($password){
		$total = 0;
		foreach(str_split($password) as $key=>$char){
			$total += ord($char) * ($key+1) * $this->_gkey;
		}
		return $total;
	}
}

/*

Private Const Gkey = 456789 'Or any other postive integer

Public Function Encrypt(ByVal Password As String) As String
    
    Dim nPassCtr As Integer
    Dim nPassdbl As Double
    
    nPassdbl = 0
    For nPassCtr = 1 To Len(Password)
        nPassdbl = nPassdbl + (Asc(Mid(Password, nPassCtr, 1)) * nPassCtr * Gkey)
    Next nPassCtr
    
    Encrypt = Trim(Str(nPassdbl))

End Function


*/