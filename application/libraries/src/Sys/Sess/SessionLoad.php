<?php
namespace Sys\Sess;
class SessionLoad{
	public function __construct(){
		if (session_status() == PHP_SESSION_NONE) session_start();
	}
	public function isLogin(){
		if(isset($_SESSION['current_user'])){
			return true;
		}
		else{
			return false;
		}
	}
	public function setLogin($data/*, $displayname*/){
		$_SESSION['current_user']['login-user'] 			= $data['user'];
		$_SESSION['current_user']['login-name'] 			= $data['username'];
		$_SESSION['current_user']['login-position'] 		= $data['position'];
		$_SESSION['current_user']['login-position-name'] 	= $data['position_name'];
		$_SESSION['current_user']['login-employee-id'] 		= $data['employee_id'];
		$_SESSION['current_user']['login-employee'] 		= $data['employee'];
		$_SESSION['module-access'] 							= $data['module_access'];
		$_SESSION['document-approval-url'] 					= $data['document_approval_url'];
	}	
	public function clearLogin(){
		unset($_SESSION['current_user']);
	}
}