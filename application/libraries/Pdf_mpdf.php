<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!defined('BASEPATH')) exit('No direct script access allowed');
 
include_once APPPATH.'/third_party/mpdf/mpdf.php';
 
class Pdf_mpdf {
/*
	
	November 7, 2017 
	MPDF API Helper By: Brent

*/
public $pdf;
private $limit_count_file = 5;

	public function generate($data){
		//Filling up unset optional data
		/*
		Format list
		- Letter
		- Legal
		- A3
		- A4
		*/


		$data['mode'] = (array_key_exists('mode', $data))? $data['mode'] : '';
		$data['format'] = (array_key_exists('format', $data))? $data['format'] : 'Letter';
		$data['font_size'] = (array_key_exists('font_size', $data))? $data['font_size'] : 10;
		$data['font'] = (array_key_exists('font', $data))? $data['font'] : 'helvetica';
		$data['margin_left'] = (array_key_exists('margin_left', $data))? $data['margin_left'] : 5;
		$data['margin_right'] = (array_key_exists('margin_right', $data))? $data['margin_right'] : 5;
		$data['margin_top'] = (array_key_exists('margin_top', $data))? $data['margin_top'] : 5;
		$data['margin_bottom'] = (array_key_exists('margin_bottom', $data))? $data['margin_bottom'] : 3;
		$data['margin_head'] = (array_key_exists('margin_head', $data))? $data['margin_head'] : 9;
		$data['margin_foot'] = (array_key_exists('margin_foot', $data))? $data['margin_foot'] : 9;
		$data['orientation'] = (array_key_exists('orientation', $data))? $data['orientation'] : 'P';
		$data['title'] = (array_key_exists('title', $data))? $data['title'] : 'Bill Of Material';
        $data['is_create'] = (array_key_exists('is_create', $data))? $data['is_create'] : true;


		$this->pdf = new mPDF(
							$data['mode'],
							($data['orientation'] == 'P')? $data['format'] : $data['format'].'-'.$data['orientation'],
							$data['font_size'],
							$data['font'],
							$data['margin_left'],
							$data['margin_right'],
							$data['margin_top'],
							$data['margin_bottom'],
							$data['margin_head'],
							$data['margin_foot'],
							$data['orientation']);
		$this->pdf->SetTitle($data['title']);
		$this->pdf->SetAuthor('SG ERP Reports');
		$this->pdf->SetCreator('SG ERP Reports');
		$this->pdf->WriteHTML($data['html']);
		
                
        if($data['is_create']){
            echo json_encode(array("pdf_name"=>$this->generateFile($this->pdf)));
        }
        else{
            $this->pdf->Output();
        }

	}

	private function generateFile($pdfObj){
		$dir = FCPATH.'/pdf/';

		$files = glob($dir . "*");
		$filecount = ($files)? count($files) : 0 ;
		if($filecount>=$this->limit_count_file){//Remove the oldest file
			
			$excess_count = $filecount - ($this->limit_count_file-1);
			array_multisort(
				array_map( 'filemtime', $files ),
				SORT_NUMERIC,
				SORT_ASC,
				$files
			);
			for($i=0; $i<$excess_count; $i++){
			 	unlink($files[$i]);
			}
		}

        //Close and output PDF document
        $pdf_name = 'jo_'.bin2hex(openssl_random_pseudo_bytes(4)).'.pdf';
        $pdfObj->Output($dir.$pdf_name, 'F');
        return $pdf_name;
    }
}