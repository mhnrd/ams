<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('numeric')) {
    function numeric($num,$decimal = 2) {
       return number_format($num,$decimal,'.','');
    }
}

if (! function_exists('whole_number')) {
    function whole_number($num) {
       return number_format($num,0,'.',',');
    }
}

if (! function_exists('format')) {
    function format($date) {

		if($date){
	    	$date = date_create($date);	
	       	return date_format($date,'m/d/y');
    	}
    }
}

if (! function_exists('format_fullyear')) {
    function format_fullyear($date) {

        if($date){
            $date = date_create($date); 
            return date_format($date,'m/d/Y');
        }
    }
}


if (! function_exists('format_month')) {
    function format_month($date) {

        if($date){
            $date = date_create($date); 
            return date_format($date,'M-d-Y');
        }
    }
}

if (! function_exists('format_datetime')) {
    function format_datetime($date) {

        if($date){
        	$date = date_create($date);	
            return date_format($date,'m/d/y H:i');
        }
        
    }
}

if (! function_exists('format_datetime_fullyear_dash')) {
    function format_datetime_fullyear_dash($date) {

        if($date){
            $date = date_create($date); 
            return date_format($date,'Y-m-d H:i:s');
        }
        
    }
}


