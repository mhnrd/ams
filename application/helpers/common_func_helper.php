<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function loadAllLocations(){
	$CI = get_instance();

	$query = 'SELECT	AD_ID,
						AD_Desc
				FROM	tblAttributeDetail
				WHERE	AD_FK_Code = 400003 AND
						AD_Active = 1';

	return $CI->db->query($query)->result_array();

}

function format_date($date, $string_format = "M. d, Y g:i A"){
	return date_format(date_create($date), $string_format);
}

function format_numeric($data){
	return preg_replace('/\D/', '', $data);
}

function getErrorMessage($err_code){

	$CI = get_instance();

	$err_query = 'SELECT 		ERR_Desc, ERR_Title, ERR_Type
				  FROM 			tblError
				  WHERE 		ERR_Code = '.$err_code;

	return $CI->db->query($err_query)->row_array();
}

function checkNoSeries($module_id, $location){

	$CI = get_instance();

	$query = 'SELECT 	NS_Id
			  FROM 		tblNoSeries
			  WHERE 	NS_FK_Module_id = \''.$module_id.'\' AND ISNULL(NS_Location,\'\')= \''.$location.'\'';

	return $CI->db->query($query)->row_array();

}

function checkExisitingDocument($header_str, $header_table, $doc_no, $type = '_DocNo'){

	$CI = get_instance();

	$query = 'SELECT 	'.$header_str.$type.'
			  FROM 		'.$header_table.' 
			  WHERE 	'.$header_str.$type.' = \''.$doc_no.'\'';

	return $CI->db->query($query)->row_array();
}

function checkExisitingID($header_str, $header_table, $id){

	$CI = get_instance();

	$query = 'SELECT 	'.$header_str.'_ID
			  FROM 		'.$header_table.'
			  WHERE 	'.$header_str.'_ID = \''.$id.'\'';

	return $CI->db->query($query)->row_array();
}

function get_mother_modules(){

	$CI = get_instance();

	$query = 'SELECT        RA.RA_FK_Modules_Code, CASE WHEN SUM(CAST(RA.RA_Menu AS INT)) > 0 THEN 1 ELSE 0 END AS RA_Menu, 
							M.M_DisplayName, M.M_Trigger, M.M_Icon, M.M_ControllerName
			  FROM          tblRoleAccess AS RA LEFT OUTER JOIN
			                tblModule AS M ON RA.RA_FK_Modules_Code = M.M_Module_id
			  WHERE         (RA.RA_ID IN
			                             (SELECT        UR_FK_RoleID
			                               FROM            tblUserRole
			                               WHERE        (UR_FK_UserID = \''.getCurrentUser()['login-user'].'\'))) AND (M.M_Active = 1) AND (M.M_WebLevel = 1) AND (M.M_isWebModule <> 0)
			  GROUP BY 		RA.RA_FK_Modules_Code, M.M_DisplayName, M.M_Sequence, M.M_DisplayName, M.M_Trigger, M.M_Icon, 
			  				M.M_ControllerName
			  HAVING        (SUM(CAST(RA.RA_Menu AS INT)) > 0)
			  ORDER BY 		M.M_Sequence';

	return $CI->db->query($query)->result_array();  
}

function get_sub_modules($mother_mod){

	$CI = get_instance();

	$query = 'SELECT        RA.RA_FK_Modules_Code, CASE WHEN SUM(CAST(RA.RA_Menu AS INT)) > 0 THEN 1 ELSE 0 END AS RA_Menu, 
							M.M_DisplayName, M.M_Trigger, M.M_Icon, M.M_ControllerName
			  FROM          tblRoleAccess AS RA LEFT OUTER JOIN
			                tblModule AS M ON RA.RA_FK_Modules_Code = M.M_Module_id
			  WHERE         (RA.RA_ID IN
			                             (SELECT        UR_FK_RoleID
			                               FROM            tblUserRole
			                               WHERE        (UR_FK_UserID = \''.getCurrentUser()['login-user'].'\'))) AND (M.M_Active = 1) AND (M.M_WebLevel = 2) AND (M.M_isWebModule <> 0) AND (M_WebParent = '.$mother_mod.')
			  GROUP BY 		RA.RA_FK_Modules_Code, M.M_DisplayName, M.M_Sequence, M.M_DisplayName, M.M_Trigger, M.M_Icon, 
			  				M.M_ControllerName
			  HAVING        (SUM(CAST(RA.RA_Menu AS INT)) > 0)
			  ORDER BY 		M.M_Sequence';

	return $CI->db->query($query)->result_array();  
}

function getModuleIDbyController($module_cont){
	
	$CI = get_instance();

	$query = 'SELECT 		M_Module_id
			  FROM 			tblModule
			  WHERE 		M_ControllerName = \''.$module_cont.'\'';

	return $CI->db->query($query)->row_array()['M_Module_id'];
}

function getAllModulesList(){

	$CI = get_instance();

	$mother_mod = getModuleIDbyController($CI->uri->segment(1));

	// LEVEL 2

	$query_lv2 = 'SELECT        RA.RA_FK_Modules_Code, CASE WHEN SUM(CAST(RA.RA_Menu AS INT)) > 0 THEN 1 ELSE 0 END AS RA_Menu, 
								M.M_DisplayName, M.M_Trigger, M.M_Icon, M.M_ControllerName, M.M_WebParent
				  FROM          tblRoleAccess AS RA LEFT OUTER JOIN
				                tblModule AS M ON RA.RA_FK_Modules_Code = M.M_Module_id
				  WHERE         (RA.RA_ID IN
				                             (SELECT        UR_FK_RoleID
				                               FROM            tblUserRole
				                               WHERE        (UR_FK_UserID = \''.getCurrentUser()['login-user'].'\'))) AND (M.M_Active = 1) AND (M.M_WebLevel = 2) AND M.M_WebParent = \''.$mother_mod.'\' AND (M.M_isWebModule <> 0)
				  GROUP BY 		RA.RA_FK_Modules_Code, M.M_DisplayName, M.M_Sequence, M.M_DisplayName, M.M_Trigger, M.M_Icon, 
				  				M.M_ControllerName, M.M_WebParent
				  HAVING        (SUM(CAST(RA.RA_Menu AS INT)) > 0)
				  ORDER BY 		M.M_Sequence';

	$result_lv2 = $CI->db->query($query_lv2)->result_array();

	if(!empty($result_lv2)){

		// LEVEL 3 and 4

		$query_lvlower = 'SELECT        RA.RA_FK_Modules_Code, 
										CASE WHEN SUM(CAST(RA.RA_Menu AS INT)) > 0 THEN 1 ELSE 0 END AS RA_Menu, 
										M.M_DisplayName, M.M_Trigger, M.M_Icon, M.M_ControllerName, M.M_WebParent
						  FROM          tblRoleAccess AS RA LEFT OUTER JOIN
						                tblModule AS M ON RA.RA_FK_Modules_Code = M.M_Module_id
						  WHERE         (RA.RA_ID IN
						                             (SELECT        UR_FK_RoleID
						                               FROM            tblUserRole
						                               WHERE        (UR_FK_UserID = \''.getCurrentUser()['login-user'].'\'))) AND (M.M_Active = 1) AND (M.M_WebLevel > 2) AND (M.M_isWebModule <> 0)
						  GROUP BY 		RA.RA_FK_Modules_Code, M.M_DisplayName, M.M_Sequence, M.M_DisplayName, M.M_Trigger, 
						  				M.M_Icon, M.M_ControllerName, M.M_WebParent
						  HAVING        (SUM(CAST(RA.RA_Menu AS INT)) > 0)
						  ORDER BY 		M.M_Sequence';

		$result_lvlower = $CI->db->query($query_lvlower)->result_array();

		foreach($result_lv2 as $key => $lv2):

			$result_lv2[$key]['lvl3'] = array();

			// LEVEL 3 ARRAY
			foreach($result_lvlower as $index => $lv3):
				if($lv2['RA_FK_Modules_Code'] == $lv3['M_WebParent']):
					array_push($result_lv2[$key]['lvl3'], $result_lvlower[$index]);
				endif;
			endforeach;	

			if(!empty($result_lv2[$key]['lvl3']) ):

				// LEVEL 4 ARRAY

				foreach($result_lv2[$key]['lvl3'] as $index_no => $lv3){

					$result_lv2[$key]['lvl3'][$index_no]['lvl4'] = array();

					foreach($result_lvlower as $index => $lv4):
						if($lv3['RA_FK_Modules_Code'] == $lv4['M_WebParent']):
							array_push($result_lv2[$key]['lvl3'][$index_no]['lvl4'] , $result_lvlower[$index]);
						endif;
					endforeach;	
				}

			endif;		

		endforeach;

	}

	return $result_lv2;
}

function getDisplayName($controller_name){
	$CI = get_instance();

	$query = 'SELECT 	M_DisplayName
			  FROM 		tblModule
			  WHERE 	M_ControllerName = \''.$controller_name.'\'';

	return $CI->db->query($query)->row_array()['M_DisplayName'];
}

function getModuleIcon($controller_name){
	$CI = get_instance();

	$query = 'SELECT 	M_Icon
			  FROM 		tblModule
			  WHERE 	M_ControllerName = \''.$controller_name.'\'';

	return $CI->db->query($query)->row_array()['M_Icon'];
}

function getConfigs($module_id){
	$CI = get_instance();

	$query = 'SELECT 	SC_ID, SC_Description, CAST(SC_ModuleID as int) as SC_ModuleID, SC_DataType, SC_Value
			  FROM 		tblSystemConfig
			  WHERE 	SC_ModuleID = \''.$module_id.'\'
			  ORDER BY 	CAST(SC_ModuleID as int)';

	return $CI->db->query($query)->result_array();
}

function getDefaultLocation(){
	$CI = get_instance();

	$query = 'SELECT 	CA_FK_Location_id
			  FROM 		tblCompanyAccess
			  WHERE 	CA_U_ID = \''.$CI->session->userdata('current_user')['login-user'].'\' AND CA_DefaultLocation = 1';


	$location = $CI->db->query($query)->row_array()['CA_FK_Location_id'];

	if($location == '' || $location === null){
		$query = 'SELECT 	Emp_FK_StoreID
				  FROM 		tblEmployee
				  WHERE 	Emp_Id = \''.$CI->session->userdata('current_user')['login-employee-id'].'\'';

		$location = $CI->db->query($query)->row_array()['Emp_FK_StoreID'];
	}


	return $location;
}

function ModuleSystemConfigs($module_id){
    $CI = get_instance();

    $query_config = 'SELECT     SC_ID, SC_Description, SC_Value
                         FROM       tblSystemConfig
                         WHERE      SC_ModuleID = \''.$module_id.'\'';

    $result_config = $CI->db->query($query_config)->result_array();

    $config = [];

    foreach ($result_config as $key => $configs) {
        $config[$configs['SC_ID']] = $configs['SC_Value'];
    }

    return $config;
}

function getCategoryByItemCode($item_code){
	$CI = get_instance();

    $query_string = 'SELECT             IM.FK_Category_Code, CAT.Category_Desc
                     FROM               Retailflex.dbo.tblInvMaster AS IM LEFT OUTER JOIN
                                        Retailflex.dbo.tblCategory AS CAT ON IM.FK_Category_Code = CAT.Category_Code
                     WHERE              (IM.IM_Item_Code = \''.$item_code.'\')';

    return $CI->db->query($query_string)->row_array();
}

function getCategoryDesc($cat_code){
	$CI = get_instance();

    $query_string = 'SELECT             Category_Desc
                     FROM               Retailflex.dbo.tblCategory 
                     WHERE              (Category_Code = \''.$cat_code.'\')';

    return $CI->db->query($query_string)->row_array()['Category_Desc'];
}

function getUOMDesc($attr_code){
	$CI = get_instance();

	$query_string = 'SELECT             AD_Desc
                     FROM               tblAttributeDetail 
                     WHERE              (AD_Code = \''.$attr_code.'\') AND (AD_FK_Code = 1001)';

    return $CI->db->query($query_string)->row_array()['AD_Desc'];
}

function getAttributeList($attr_code){
	$CI = get_instance();

	$query_string = 'SELECT 	AD_ID, AD_Desc
					 FROM 		tblAttributeDetail
					 WHERE 		AD_FK_Code = \''.$attr_code.'\' AND 
					 			AD_Active = 1
					 ORDER BY 	AD_ID';

	return $CI->db->query($query_string)->result_array();

}


function getUserCompanyAccess(){
	$CI = get_instance();

	$query_string = 'SELECT 	CA_FK_Location_id, SP_StoreName
					 FROM 		tblCompanyAccess LEFT OUTER JOIN
					 			tblStoreProfile ON CA_FK_Location_id = SP_ID
					 WHERE 		CA_U_ID = \''.$CI->session->userdata('current_user')['login-user'].'\'';

	return $CI->db->query($query_string)->result_array();
}

function getLocationCompany($location){
	$CI = get_instance();

	$query_string = 'SELECT 		SP_FK_CompanyID
					 FROM 			tblStoreProfile
					 WHERE 			SP_ID = \''.$location.'\'';

	return $CI->db->query($query_string)->row_array()['SP_FK_CompanyID'];
}

function getItemCodebyUPC($upc_code){
	$CI = get_instance();

	$query_string = 'SELECT 		IM_Item_Id
					 FROM 			tblItem
					 WHERE 			IM_UPCCode = \''.$upc_code.'\'';

	return $CI->db->query($query_string)->row_array()['IM_Item_Id'];
}

function validate_stocks($item_code, $location, $sublocation){
	$CI = get_instance();

	// Total Stocks
	$query_IL = 'SELECT 	(IL_Qty * ISNULL(
											(SELECT IUC_Quantity 
											 FROM tblItemUOMConv 
											 WHERE IUC_FK_Item_id = \''.$item_code.'\' AND
												   IUC_FK_UOM_id = IL_UOM),0)) AS Stocks
				 FROM		tblItemLedger
				 WHERE 		IL_Location = \''.$location.'\' AND IL_SubLocation = \''.$sublocation.'\' AND
				 			IL_ItemNo = \''.$item_code.'\'';

	$SOH_result = array_column($CI->db->query($query_IL)->result_array(), 'Stocks');

	$SOH = 0;

	foreach ($SOH_result as $value) {
		$SOH += $value;
	}

	//TO Used Stocks (For future use: add another query for modules that consumes stocks)
	$query_TO = 'SELECT 	ISNULL(SUM(TOD_QtyToShip * TOD_BaseUOMQty), 0) AS TO_Stocks
				 FROM 		tblTransferOrderDetail LEFT OUTER JOIN
				 			tblTransferOrder ON TO_DocNo = TOD_TO_DocNo
				 WHERE 		TO_Status IN (\'Open\', \'Pending\', \'Approved\', \'Partial Shipped\')';

	$TOUsed = $CI->db->query($query_TO)->row_array()['TO_Stocks'];

	//IRJ Used Stocks (For Future Dev)

	//Debit/Credit memo Used Stocks (For Future Dev)

	//Sales Invoice Used Stocks (For Future Dev)

	$result = $SOH - $TOUsed;

	return $result;
}

function ILEntryNo(){
	$CI = get_instance();

	// get latest Entry No
    $IL_EntryNo = $CI->db->order_by('IL_EntryNo DESC')->limit(1)->get('tblItemLedger')->row_array()['IL_EntryNo'];

    return $IL_EntryNo; 
}

function AvailableSubLocationList($location){
	$CI = get_instance();

	$query_loc_type = 'SELECT    SP_StoreType
                       FROM      tblStoreProfile
                       WHERE     SP_ID = \''.$location.'\'';

    $loc_type = $CI->db->query($query_loc_type)->row_array()['SP_StoreType'];

    $query_string = 'SELECT     LOC_Id, LOC_Name
                     FROM       tblLocation
                     WHERE      LOC_Active = 1 AND LOC_FK_Type_id = \''.$loc_type.'\' AND
                                LOC_FK_Level_id = (SELECT   TOP (1) LOC_FK_Level_id
                                                   FROM     tblLocation
                                                   WHERE    LOC_FK_Type_id = \''.$loc_type.'\'
                                                   ORDER BY LOC_FK_Level_id DESC)
                     ORDER BY 	LOC_Name ASC';

    return $CI->db->query($query_string)->result_array();
}

// Get Base UOM Qty
function BaseUOMQty($item_code, $UOM){
	$CI = get_instance();

	$query_string = 'SELECT     TOP (1) IUC_Quantity
                     FROM       tblItemUOMConv
                     WHERE      IUC_FK_Item_id = \''.$item_code.'\' AND 
                     			IUC_FK_UOM_id = \''.$UOM.'\'';

    return $CI->db->query($query_string)->row_array()['IUC_Quantity'];

}

function getEmployeeDefaultLoc($emp_id){
	$CI = get_instance();

	$query_string = 'SELECT     Emp_FK_StoreID
                     FROM       tblEmployee
                     WHERE      Emp_Id = \''.$emp_id.'\'';

    return $CI->db->query($query_string)->row_array()['Emp_FK_StoreID'];
}

function getAssetClassList(){
	$CI = get_instance();

	$query_string = 'SELECT 	FACS_ID, FACS_Description
					 FROM 		tblFAClassSetup
					 ORDER BY 	FACS_Description';

	return $CI->db->query($query_string)->result_array();

}

function getCompanyDetails($table, $cols, $where_clause = array()){
	$CI = get_instance();

	$query_string = 'SELECT '.implode(', ', $cols).' 
                     FROM '.$table;

    if (!empty($where_clause)) {
    	$query_string .= ' WHERE ';

    	foreach ($where_clause as $value) {
    		$query_string .= $value;
    	}
    }

    return $CI->db->query($query_string)->result_array();
}



 // function getBayID($faID){

	// $CI = get_instance();
 //        $query_string =     'select [FA_DepartmentID] 
 //                            from    [dbo].[tblFA]
 //                            where   [FA_ID] = \''.$faID.'\'';

 //        // return $this->db->query($query_string, array($faID))->result_array();
 //        return $CI->db->query($query_string)->result_array();
 //    }



function getBayNoTest($attr_code){
	$CI = get_instance();

    $query_string = 'SELECT  SL_Id,
                             SL_Name
                     FROM    tblSubLocation
                     WHERE   SL_Parent_id = \''.$attr_code.'\' AND 
                     		 SL_FK_Type_id = 1 AND
                             SL_Active = 1
                             ORDER BY 	SL_Name';
                             
    // return $this->db->query($query_string, array($department))->result_array();
    return $CI->db->query($query_string)->result_array();
}


// function getDivision($FA_DepartmentID){
// 		$CI = get_instance();

// 		$query_string = 'SELECT	AD_Desc AS Division
// 						FROM	tblAttributeDetail
// 						WHERE	AD_FK_Code = 400003';

// 		return $CI->db->query($query_string)->result_array();
// }

// function getCarline($FA_AffiliatesID){
// 		$CI = get_instance();

// 		$query_string = 'SELECT	AD_Desc AS CarLine
// 						FROM	tblAttributeDetail
// 						WHERE	AD_FK_Code = 400004';

// 		return $CI->db->query($query_string)->result_array();
// }

function getLineNo($FA_Line){
		$CI = get_instance();

		$query_string = 'SELECT	SL_Name
						FROM 	tblSubLocation
						WHERE	SL_FK_Type_id = 2 AND
								SL_Active = 1';

		return $CI->db->query($query_string)->result_array();
}



// Revised/Added by JDC 071720241
function getBayList(){
	$CI = get_instance();

	$query_string = 'select	[SL_Id],
							[SL_Name]
					from	[dbo].[tblSubLocation]
					where	[SL_FK_Type_id] = 1';

	return $CI->db->query($query_string)->result_array();

}

// Revised/Added by JDC 071720241
function getLineList(){
	$CI = get_instance();

	$query_string = 'select	[SL_Id],
							[SL_Name]
					from	[dbo].[tblSubLocation]
					where	[SL_FK_Type_id] = 2';

	return $CI->db->query($query_string)->result_array();

}


// Revised/Added by JDC 071720241
function getAttributeName($code){ //DepartmentID
	$CI = get_instance();

	$query_string = 'SELECT [AD_Code] 
					from	[dbo].[tblAttributeDetail] 
					WHERE 	[AD_ID] = \''.$code.'\'
					 ';

	return $CI->db->query($query_string)->row_array()['AD_Code'];

}


// Revised/Added by JDC 071720241
function getSubLocationName($code){ //AffiliateID
	$CI = get_instance();

	$query_string = 'select	[SL_Name]
					from	[dbo].[tblSubLocation]
					WHERE 	[SL_Id] = \''.$code.'\'
					 ';

	return $CI->db->query($query_string)->row_array()['SL_Name'];

}

// function getPrincipalName($principal){
// 	$CI = get_instance();

// 	$query_string = 'SELECT     C_Name
//                    	 FROM       tblCustomer
//                    	 WHERE      C_DocNo = \''.$principal.'\'';


//     return $CI->db->query($query_string)->row_array()['C_Name'];
// }





