<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function generateModelName($model_name){
	$model_name = str_replace('/', '_', $model_name);
	return '_'.strtolower($model_name);
}   
