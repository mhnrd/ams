<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function getCurrentUser(){
	return $_SESSION['current_user'];
}

// function getDefaultLocation(){
// 	$CI = get_instance();
//     $CI->load->model('com/companyaccess_model');
//     return $CI->companyaccess_model->getLocationByUserId($_SESSION['current_user']['login-user'])['CA_FK_Location_id'];
// }

function getDefaultStoreType(){
	$CI = get_instance();
	$CI->load->model('com/companyaccess_model');
	return $CI->companyaccess_model->getDefaultStoreTypebyUserID($_SESSION['current_user']['login-user'])['SP_StoreType'];
}

function getUserLocationAccess(){
	$CI = get_instance();
	$CI->load->model('com/companyaccess_model');
	return $CI->companyaccess_model->getAllUserLocationAccess($_SESSION['current_user']['login-user']);
}

function getDefaultStoreName(){
	$CI = get_instance();
	$CI->load->model('com/companyaccess_model');
	return $CI->companyaccess_model->getDefaultStoreNamebyUserID($_SESSION['current_user']['login-user'])['SP_StoreName'];
}