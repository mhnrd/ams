<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('generate_table')) {

    function generate_table($data = array()) {
        $table = '<div class="table-responsive datatable">';
        $table .= '<table class="table table-hover table-bordered dataTables-example nowrap" id="' . key($data) . '" style="width: 100%"><thead><tr>';
        foreach ($data[key($data)] as $key => $value) {
            $style  = (isset($value['style']) ? 'style="'.$value['style'].'"' : ''); 
            $table .= "<th ".(isset($value['class']) ? "class='".$value['class']."' " : "" ).$style.">" . $value['title'] . "</th>";
        }

        $table .= "</tr></thead><tfoot style=\"display: none\"></tfoot><tbody></tbody></table>";
        $table .= "</div>";
        return $table;
    }

}

if (!function_exists('generate_table_standard')) {

    function generate_table_standard($data = array()) {
        $table = '<div class="table-responsive datatable">';
        $table .= '<table class="table table-bordered table-hover dataTables-example nowrap" id="' . key($data) . '" style="width: 100%"><thead><tr>';
        foreach ($data[key($data)] as $key => $value) {
            $style  = (isset($value['style']) ? 'style="'.$value['style'].'"' : ''); 
            $table .= "<th ".(isset($value['class']) ? "class='".$value['class']."' " : "" ).$style.">" . $value['title'] . "</th>";
        }

        $table .= "</tr></thead><tfoot style=\"display: none\"></tfoot><tbody></tbody></table>";
        $table .= "</div>";
        return $table;
    }

}

if (!function_exists('generate_table_ur')) {

    function generate_table_ur($data = array()) {
        $table = '<div class="table-responsive datatable col-md-7 col-12">';
        $table .= '<table class="table table-bordered table-hover dataTables-example nowrap" id="' . key($data) . '" style="width: 100%"><thead><tr>';
        foreach ($data[key($data)] as $key => $value) {
            $style  = (isset($value['style']) ? 'style="'.$value['style'].'"' : ''); 
            $table .= "<th ".(isset($value['class']) ? "class='".$value['class']."' " : "" ).$style.">" . $value['title'] . "</th>";
        }

        $table .= "</tr></thead><tfoot style=\"display: none\"></tfoot><tbody></tbody></table>";
        $table .= "</div>";
        return $table;
    }

}

if (!function_exists('generate_table_rf')) {

    function generate_table_rf($data = array()) {
        $table = '<div class="table-responsive datatable col-md-5 col-12">';
        $table .= '<table class="table table-bordered table-hover dataTables-example nowrap" id="' . key($data) . '" style="width: 100%"><thead><tr>';
        foreach ($data[key($data)] as $key => $value) {
            $style  = (isset($value['style']) ? 'style="'.$value['style'].'"' : ''); 
            $table .= "<th ".(isset($value['class']) ? "class='".$value['class']."' " : "" ).$style.">" . $value['title'] . "</th>";
        }

        $table .= "</tr></thead><tfoot style=\"display: none\"></tfoot><tbody></tbody></table>";
        $table .= "</div>";
        return $table;
    }

}

if (!function_exists('generate_filter')) {
    function generate_filter($fields){
        $filter = '<div class="form-group filter row">';

        $filter .= '<label class="control-label col-2 filter-tag">Filter: </label>';

        //Filter Dropdown
        $filter .= '<div class="col-5" style="padding: 5px!important">';
        $filter .= '<select class="form-control js-select2" style="width: 100%">';
        $filter .= '<option value=""></option>';
        foreach ($fields as $key => $value) {
            $filter .= '<option value="'.$value['value'].'">'.$value['name'].'</option>';
        }

        $filter .= '</select>';
        $filter .= '</div>';

        //Input field
        $filter .= '<div class="col-5 input-tag">';
        $filter .= '<input type="text" class="form-control search-field">';
        $filter .= '</div>';

        $filter .= '</div>';

        return $filter;
    }
}