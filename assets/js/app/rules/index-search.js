$(document).ready(function($) {

	function initFields(){
		$('.js-select2').select2({
			placeholder : '',
			allowClear : true
		});

		$('.dataTables_length label select').select2({
			placeholder : '',
			allowClear : false
		});
	}

	function init_search(){
		$('input.search-field').on('keyup change', function() {
			if ($('.js-select2').val() != '') {
				tableindex.search('').columns().search('').draw();
				var input = $(this).val();
				var column = $('.js-select2').val();

				tableindex.columns(column).map(function() {
					var that = this;
					if (that.search() !== input){
						that.search(input).draw();
					}
				});
			}
			else{
				var input = $(this).val();
				if ($(this).val() != '') {
					tableindex.search(input).draw();
				}
				else{
					tableindex.search('').columns().search('').draw();
				}
			}
		});

		$('.js-select2').change(function(event) {
			if ($(this).val() == '') {
				$('input.search-field').val('');
				tableindex.search('').columns().search('').draw();
			}
			else{
				tableindex.search('').columns().search('').draw();
				var input = $('input.search-field').val();
				var column = $(this).val();

				tableindex.columns(column).map(function() {
					var that = this;
					if (that.search() !== input){
						that.search(input).draw();
					}
				});
			}
		});

		initFields();
	}

	init_search();

});