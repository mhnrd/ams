$('.table').ready(function() {
	
	var toolbar = '<div class="pull-right table-tools" style="margin-top: 5px!important"><button class="btn btn-primary btn-outline btn-sm advance-search" id="advance-search" data-toggle="tooltip" title="Advance Search"><i class="fa fa-search-plus">&nbsp</i></button>';

	if(typeof mod_name != 'undefined') {
		
		if(mod_name == 'Barcode Printing'){
			
			toolbar += '<a class="btn btn-success btn-sm print-all" role="button" id="print-all" style="margin-left: 5px;"><i class="fa fa-qrcode">&nbsp</i> Print All</a>'	

		}

	}

	var create_sticker = {
						    state : function(current_state){
						        if(current_state == 'loading'){
						            $('#print-all').empty();
						            $('#print-all').append('<i class="fa fa-refresh fa-spin"></i> Printing');
						            $('#print-all').attr('disabled', true);
						        }
						        if(current_state == 'click'){
						            $('#print-all').attr('disabled', false);
						            $('#print-all').empty();
						            $('#print-all').append('<i class="fa fa-qrcode"></i> Print All');
						        }
						    }
						};
	


	toolbar +='</div>'

	$("div.datatable div.dt-toolbar").html(toolbar);

	var header = [];
	var col_num = [];

	$('#'+table_id+' thead th').each(function(){
		var title = $(this).text().trim();
		if(title == ''){
			title = null;
		}
		header.push(title);  
	});
	
	// if(header.indexOf(null) == -1){
	var clear = ' <button class="btn btn-success btn-outline btn-sm" id="clear-search" style="display: none" data-toggle="tooltip" title="Clear"><span class="fa fa-eraser"></span></button>';
	$("div.datatable div.dt-toolbar div.pull-right").append(clear);
	// }

	var refresh = ' <button class="btn btn-success btn-outline btn-sm" id="refresh" data-toggle="tooltip" title="Clear"><span class="fa fa-refresh"></span></button>';
	$("div.datatable div.dt-toolbar div.pull-right").append(refresh);

	var html = '<tr class="column_search" style="background-color: #e6e6e6">';
	var isClearbtn = false;
	$(header).each(function(index){
		html += '<td class="text-center">';

		if(header[index] !== null){
			html += '<input type="text" class="form-control col-search ' + ((header[index].includes('Time') || header[index].includes('Break')) ? 'clockpicker' : '') + '" '+ 
				((header[index].includes('Date') || header[index].includes('Period')  || header[index].includes('Holiday Start') || header[index].includes('Holiday End')) ? 'name="daterange"' : '') +
				' id="input-data'+ index +
				'" data-column="'+ index +
				'" style="width: 100%; '+ 
				((header[index].includes('Date') || header[index].includes('Period') || header[index].includes('Time') || header[index].includes('Break') || header[index].includes('Holiday Start') || header[index].includes('Holiday End')) ? 'background-color: #FFFFFF' : '') +'" ' + 
				((header[index].includes('Date') || header[index].includes('Period') || header[index].includes('Time') || header[index].includes('Break') || header[index].includes('Holiday Start') || header[index].includes('Holiday End')) ? 'readonly' : '')+'>';
		}
		html += '</td>';
	});
	html += '</tr>';
	$('#'+ table_id +' tfoot').append(html);

	var table_data = tableindex;

	$('input.col-search').on('keyup change', function() {
		var input = $(this).val();
		var column = $(this).data('column');
		table_data.columns(column).map(function() {
			var that = this;
			if (that.search() !== input){
				that.search(input).draw();
			}
		});
	});

  	$('input[name="daterange"]').daterangepicker({
	    opens: 'left'
	  });

  	if($('input.col-search').hasClass('clockpicker')){
	  	$('.clockpicker').clockpicker({
	  		autoclose: true
	  	});		
  	}
	

  	$('#'+table_id).on('keydown', '.clockpicker, [name="daterange"]', function(e){
  		let input = $(this);
  		if(e.keyCode == 46 || e.keyCode == 8){
  			input.val('');
  		}
  	});

	$('#advance-search').click(function(){
		
		if($('a.advance-search').hasClass('btn-outline')){
			$('#advance-search').removeClass('btn-outline');
		}
		else{
			$('a.advance-search').addClass('btn-outline');
		}

		$('.datatable .dataTables_filter label').toggle();
		$('#'+table_id).find('tfoot').toggle();
		$('#clear-search').toggle();
		$('#refresh').toggle();

	});


	$('#clear-search').click(function(){
		$('input.col-search').val('');
		table_data.search('').columns().search('').draw();
	});

	$('#refresh').click(function(){
		$('input').val('');
		table_data.search('').columns().search('').draw();
	});

	$('#print-all').click(function() {

		create_sticker.state('loading');

		$.ajax({
			url: global.site_name + 'inventory/barcode-printing/print-all',
			type: 'POST',
			dataType: 'json',
			data:{
				'doc-no'	: $('[name="BT_BatchID"]').val()
			},
			success : function (result){
    			printJS(global.site_name + 'pdf/'+result.pdf_name);
				create_sticker.state('click');
			}
		});
	});



});

