$(document).ready(function(){

	function initTableButtons(){
		delete_row();
		initStatusBtn();
		checkboxTrigger();
	}

	initTableButtons();

})

function delete_row(){
		
	$('#'+table_id).on('click', '.delete-button',function(e){
		e.preventDefault();
		var doc_no = $(this).data('id');
		swal({
				title: "Are you sure?",
				text:  "This data will be deleted",
				type:  "warning",
				showCancelButton:  true,
				confirmButtonColor: "#1AB394",
				confirmButtonText:  "Yes",
				cancelButtonText: "No",
				closeOnConfirm: false
		}, function(isConfirm){
			if(isConfirm){
				$.ajax({
					type: 'GET',
					dataType: 'json',
					url: global.site_name + controller_url + 'delete',
					data: {	'id'	: doc_no,
							'action': 'delete'},
					success: function(data){
						if(data.success == 1){
							swal({
								title: "Delete Success",
								type:  "success"
							});
							setInterval(function(){
								location.reload();
							}, 1500);
						}else{
							swal({
								title: data.title,
								text:  data.message,
								type:  data.type
							});
						}
					},
					error : function(){
						swal("Error", "Please contact your system administrator", "error")
					}
				});
			}
		});
	});

}

function initStatusBtn(){
	let intCheck = 0;

	// CHECK ALL CHECKBOX
	$('#chkSelectAll').click(function(){
		if($(this).is(':checked')){
			$('[name="chkSelect[]"]').prop('checked', true);
			$('#'+table_id+' tbody tr input[type="checkbox"]').each(function(){
				if($(this).is(':checked')){
					intCheck++;
				}
			});

			InitializeButtonEvent(intCheck);

			intCheck = 0;
		}
		else{
			$('[name="chkSelect[]"]').prop('checked', false);
			$('#'+table_id+' tbody tr input[type="checkbox"]').each(function(){
				if($(this).is(':checked')){
					intCheck++;
				}
			});

			InitializeButtonEvent(intCheck);

			intCheck = 0;
		}
	})

	//ACTIVATE SELECTED DATA
	$('#btnActive').on('click',function(e){
		e.preventDefault();
		status_update('activate','Activate');
	});

	//DEACTIVATE SELECTED DATA
	$('#btnDeactive').on('click',function(e){
		e.preventDefault();
		status_update('deactivate','Deactivate');
	});
}

function status_update(action,msg){
	swal({
        title: "Are you sure?",
        text: "This will " + msg + " all selected data",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#1AB394",
        confirmButtonText:  "Yes",
		cancelButtonText: "No",
        closeOnConfirm: false
    }, function (isConfirm){
    	if(isConfirm){
    		// Looping get all array checkbox
    		var id = [];
    		$.each($('input[type="checkbox"]:checked'),function(){
    			if($(this).val() != 'on'){
    				id.push($(this).val());
    			}
    		});

    		// Request
    		$.ajax({
				type: 'POST',
				dataType: 'json',
				url: global.site_name + controller_url + 'status_update',
				data : {	'id'		: id,
							'action'	: action},
				success : function(data){
					if(data.success){
						swal({
							title: msg + " Successful",
							type:  "success"
						});
						setInterval(function(){
							location.reload();	
						}, 1500);
						
					}
					else{
						swal({
							title: data.title,
							text:  data.message,
							type:  data.type
						});
					}
				}
			});
    	}            	
    });
}

function checkboxTrigger(){

	let intCheck = 0;

	$('#'+table_id).on('click', '[name="chkSelect[]"]', function(){

		$('#'+table_id+' tbody tr td input[type="checkbox"]').each(function(){
			if($(this).is(':checked')){
				intCheck++;
			}
		});

		InitializeButtonEvent(intCheck);

		intCheck = 0;

	})


}

function InitializeButtonEvent(intCheck){

	if(intCheck > 0){
		$('#btnActive').addClass('btn-outline').removeAttr('disabled');
		$('#btnDeactive').addClass('btn-outline').removeAttr('disabled');
	}
	else if(intCheck == 0){
		$('#btnActive').removeClass('btn-outline').attr('disabled', true);
		$('#btnDeactive').removeClass('btn-outline').attr('disabled', true);	
	}

}