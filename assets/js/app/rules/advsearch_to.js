$('.dataTables-incoming').ready(function() {

	var toolbar = '<div class="pull-right"><a class="btn btn-primary btn-outline btn-sm advance-search2" role="button" id="advance-search2"><i class="fa fa-search">&nbsp</i> Advance Search</a></a></div>';
	$("div.datatable2 div.dt-toolbar").html(toolbar);

	var header = [];
	var col_num = [];

	$('.dataTables-incoming thead th').each(function(){
		var title = $(this).text().trim();
		if(title == ''){
			title = null;
		}
		header.push(title);  
	});

	var clear = ' <button class="btn btn-success btn-outline btn-sm" id="clear-search2" style="display: none"><span class="fa fa-eraser"></span> Clear</button>';
	$("div.datatable2 div.dt-toolbar div.pull-right").append(clear);
	
	var html = '<tr class="column_search" style="background-color: #e6e6e6">';
	var isClearbtn = false;
	$(header).each(function(index){
		html += '<td class="text-center">';

		if(header[index] !== null){
			html += '<input type="text" class="form-control col-search2" '+ 
				((header[index].includes('Date') || header[index].includes('Period')) ? 'name="daterange" readonly' : '') +
				' id="input2-data'+ index +
				'" placeholder="Search '+ (header[index]) +'" data-column="'+ index +'" style="width: 100%; '+ ((header[index].includes('Date') || header[index].includes('Period')) ? 'background-color: #FFFFFF' : '') +'">';
		}
		html += '</td>';
	});
	html += '</tr>';
	$('.dataTables-incoming tfoot').append(html);

	var table_data = tableindex2;

	$('input.col-search2').on('keyup change', function() {
		var input = $(this).val();
		var column = $(this).data('column');
		table_data.columns(column).map(function() {
			var that = this;
			if (that.search() !== input){
				that.search(input).draw();
			}
		});
	});

 		$('input[name="daterange"]').daterangepicker({
	    opens: 'left'
	  });
	


	$('#advance-search2').click(function(){

		if($('a.advance-search2').hasClass('btn-outline')){
			$('#advance-search2').removeClass('btn-outline');
		}
		else{
			$('a.advance-search2').addClass('btn-outline');
		}

		$('.datatable2 .dataTables_filter label').toggle();
		$('.dataTables-incoming').find('tfoot').toggle();
		$('#clear-search2').toggle();

	});

	$('#clear-search2').click(function(){
		$('input.col-search2').val('');
		table_data.search('').columns().search('').draw();
	});



});

