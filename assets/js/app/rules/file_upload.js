$(document).ready(function() {
	function extractFile(file_name){

		return file_name.split('.');

	}

	function file_upload(){
		// Add attachments
		$('#add-file').click(function(e){
			e.preventDefault();
			let html_upload = '<div class="form-group file-detail" name="file-rows" data-row-state="created">' +
									'<input type="file" id="file-upload" name="file[]" style="font-size: 8px">' +
									'<button class="btn btn-link remove-file pull-right" style="font-size: 9px">Remove</button>' +
								'</div>';

			$('.form-file-attachments').append(html_upload);

			let file_length = $('.form-file-attachments div[name="file-rows"]').length;
			
			if(file_length > 0){
				$('#add-file').hide();
				$('#add-another-file').show();
			}

		});

		$('#add-another-file').click(function(e){
			e.preventDefault();
			let html_another = '<div class="form-group file-detail" name="file-rows" data-row-state="created">' +
									'<input type="file" id="file-upload" name="file[]" style="font-size: 8px">' +
									'<button class="btn btn-link remove-file pull-right" style="font-size: 9px">Remove</button>' +
								'</div>';

			$('.form-file-attachments').append(html_another);

			let file_length = $('.form-file-attachments div.file-detail').length;
			

		});

		$('body').on('click', '.remove-file', function(e){

			var action = $('[name="mode"]').val();

			e.preventDefault();

			var current_row = $(this).closest('.file-detail');

			if(action == 'Add'){
				current_row.remove();
			}
			else{
				if($('.form-file-attachments div.file-detail').data('row-state') == 'created'){
					current_row.remove();
				}
				else{
					current_row.removeAttr('data-row-state');
					current_row.attr('data-row-state', 'deleted');
					current_row.hide();
				}
			}


			let file_length = $('.form-file-attachments div.file-detail:visible').length;
			console.log(file_length);
			
			if(file_length == 0){
				$('#add-file').show();
				$('#add-another-file').hide();
			}
		});

		$('.form-file-attachments').on('change', '[name="file[]"]', function(){

			var current_row = $(this).closest('.file-detail');

			console.log(current_row.find('[name="file[]"]').prop('files')[0])

			let file_detail = current_row.find('[name="file[]"]').prop('files')[0];

			if(file_detail === undefined){
				current_row.remove();

				let file_length = $('.form-file-attachments div.file-detail:visible').length;
				console.log(file_length);
				
				if(file_length == 0){
					$('#add-file').show();
					$('#add-another-file').hide();
				}
			}
			else{
				let filename = file_detail["name"]
				let file_size = parseFloat(file_detail["size"]) / 1000000;
				let filename_arr = extractFile(filename);
				let file_ext = filename_arr.slice(-1)[0];

				if(file_ext == 'exe'){
					swal('Error', 'Invalid File type', 'error');
					current_row.find('[name="file[]"]').val(undefined).trigger('change');

					let file_length = $('.form-file-attachments div.file-detail:visible').length;
					console.log(file_length);
					
					if(file_length == 0){
						$('#add-file').show();
						$('#add-another-file').hide();
					}

					return false;
				}

				if(file_size > 5){
					swal('File Size exceeded', 'Only files up to 5MB is allowed', 'error');
					current_row.find('[name="file[]"]').val(undefined).trigger('change');

					let file_length = $('.form-file-attachments div.file-detail:visible').length;
					console.log(file_length);
					
					if(file_length == 0){
						$('#add-file').show();
						$('#add-another-file').hide();
					}

					return false;
				}
			}

		});

		// End of Add Attachments
	}

	file_upload();
});