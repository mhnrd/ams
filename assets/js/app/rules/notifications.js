
/* global base_url, session, notification */

//  exposed APIs
var notifications = {
    onReady: function () {

    }
};

(function () {

    $(document).ready(function () {
        updateNotifications();

        // added by jekzel leonidas - 02/20/2020 8:44PM
        $('body').on('click','.not-available',function(e){
            e.preventDefault();
            console.log("test");
            swal('Under construction!', 'please try again later', 'warning');
        })

        // if($('body select').data('select2').length > 0){
        //     var select2Instance = $('body select').data('select2');
        //     select2Instance.on('results:message',function(params){
        //         this.dropdown._resizeDropdown();
        //         this.dropdown._positionDropdown();
        //     });
        // }
    });

    function updateNotifications() {

        var noti = global.site_name + "administration/doc-approval/getCountNotif";
        $.get(noti, function (response) {
            var count = JSON.parse(response);
            if(count != 0){
                count_notif = '<span class="label label-danger noti-count">'+ count + '</span>';
                $('.fa-bell').html(count_notif);
            }
            
        });

        $('li.noti-num').click(function (){
            $('.notif').html('<li><div class="spiner-example"><div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div></div></li>');
            setTimeout(function(){
                var url = global.site_name + "administration/doc-approval/getNotification";
                $.get(url, function (response) {
                    var data = JSON.parse(response);
                    displayNotifications(data);
                });
            },100);
        });
    }

    function displayNotifications(data) {
        
        var html = '';
        if (data.length > 0) {
            $.each(data, function (index) {                
                html += '<li>'
                            + '<a href="' + data[index].DT_TargetURL + "view?id="+$.md5(data[index].DT_DocNo).trim()+'" class="notifs" style="cursor:pointer">'
                                + '<div>'
                                    + '<i class="fa fa-envelope fa-fw"></i>'                                              
                                    + ' Pending approval for Document No. ' + data[index].DT_DocNo + ''  
                                    + '<span class="small pull-right">Date: ' + data[index].DT_EntryDate + '</span>'
                                + '</div>'
                            + '</a>'
                        +'</li>';

            });
            
            if(data.length >= 5){
                html += "<li class='divider'></li>";
                html += '<li><a href="'+ global.site_name +'administration/doc-approval" class="see-all"><div class="text-center"><i class="fa fa-search"></i> See all notifications</div></a></li>';
            }
        } else {
            html += "<li><a href='javascript:void(0)' style='cursor:none'>No pending!</li>";
        }
        $('.notif').html(html);
    }


})();

$('body').on('click','#changepass',function(e){
    e.preventDefault();
    $("#change-pass-modal").modal('show');
    $('body').find('input[type="password"][name="oldpassword"]').focus();
});

$('body').on('click','#btnChangePass',function(e){
    e.preventDefault();

    if($('[name="oldpassword"]').val() == ''){
        swal({title: "Enter Old Password",type: "error"});
        return;
    }

    if($('[name="newpassword"]').val() == ''){
        swal({title: "Enter New Password",type: "error"});
        return;
    }

    if($('[name="newpassword"]').val() != $('[name="confirmpassword"]').val()){
        swal({title: "Confirm/New Password are not the same",type: "error"});
        return;
    }

    $.ajax({
        url: global.site_name + 'User/changepassword',
        type: 'POST',
        dataType: 'json',
        data: {
                oldpassword : $('[name="oldpassword"]').val(),
                newpassword : $('[name="newpassword"]').val()
        },
        success : function(result){
            if(result.success){
                swal({                    
                    title: "Change Password Successful",
                    type: "success"
                });
                setTimeout(function(){
                    $("#change-pass-modal").modal('hide');
                    $('[name="oldpassword"]').val('');
                    $('[name="newpassword"]').val('');
                },1000);
            }
            else{
                swal({                    
                    title: result.message,
                    type: "error"
                });
            }
        }
    });
    
});