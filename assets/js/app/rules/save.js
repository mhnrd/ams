$(document).ready(function(){

	function arrayKeys(input) {
	    var output = new Array();
	    var counter = 0;
	    for (i in input) {
	        output[counter++] = i;
	    } 
	    return output; 
	}

	function validate(){
		let isComplete = true;
		let requiredField = $('#form-header :input[required]:visible');
		let requiredFields = [];

		$(requiredField).each(function(index, el) {
			if($(this).val() == ""){
				isComplete = false;
			}		
			
			// let name = $(el).attr('name');
			// requiredFields.push({name:'required'});
		});

		// TODO 1/20/2020
		// isComplete = $("#form-header").validate({
		// 	requiredFields
		// });

		// console.log(isComplete);
		// return;

		return isComplete;

	}

	function on_save_module(){

		$('.save').click(function(){

			var action_page = $(this).attr('data-action');

			swal({
	            title: "Are you sure?",
	            text: (output_type == 'add')? "You will add the current " + module_name + "." : "You will update the current "+ module_name +".",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
	            closeOnConfirm: false
	        }, function () {

	        	swal({
	        		title 				: "Please wait",
	            	text 				: (output_type == 'add')? "Saving " + module_name + " details." : "Updating "+ module_name +" details.",
	            	type 				: "info",
	            	showConfirmButton 	: false 
	        	});

	        	var tbl_dtl_lngt 		= $('#tbl-detail tbody tr:visible').length;
	        	var tbl_subdtl_lngt 	= $('#tbl-sub-detail tbody tr:visible').length;
				var data = {};


				let complete = validate();

				if(!complete){
					swal("Information", "Complete all required fields", "info");
					$('.required-label').show();
				}
				else{
					$('#form-header :input[disabled]').prop('disabled', '');
					let header = $('#form-header :input').serializeArray();
					$('#form-header :input[disabled]').prop('disabled', true);
					if(module_controller == 'Users'){
						data["UR_FK_RoleID"] = [];
					}
					$(header).each(function(index){
						if(!isNaN(header[index].value) && header[index].value != ''){
							console.log('yay')
							switch(header[index].name){
								case "Emp_BankAccountNum" :
									data[header[index].name] = header[index].value;
									break;
								case "UR_FK_RoleID[]" :
									data["UR_FK_RoleID"].push(header[index].value);
									break;
								default:
									data[header[index].name] = Number(header[index].value);
									break;
							}
						}
						else{
							data[header[index].name] = header[index].value;
						}
					});


					// console.log(data);



					// get all checkbox value in form
					$('#form-header input[type="checkbox"]').each(function(e){
				        let chkName = $(this).attr('name');
				        if(module_controller == 'employee_profile' && chkName == 'chkSameCurrentAddress'){
				        	return;
				        }
				        data[chkName] = $(this).is(":checked") ? '1' : '0';
				    });

				    // console.log(data,"Header Data");

					data['todo'] 	= output_type; 

					if(isRequiredDetails){
						// Redraw tables before saving it to DB when searching was used
						if(typeof tabledetail != "undefined"){
							tabledetail.search('').columns().search('').draw();
						}

						if(typeof table_subdetail != "undefined"){
							table_subdetail.search('').columns().search('').draw();
						}

						if($('#tbl-detail').length > 0){
							if(tbl_dtl_lngt <= 0){
								swal("Information", "No Details to be saved", "info");
								return;
							}
							else{

								let detail 			= [];
								let sub_detail 		= [];
								$('#tbl-detail tbody tr').each(function(index){
									let current_tr 	= $(this);
									let detail_arr 	= {};
									let tr_data 	= current_tr.data();
									let row_state	= current_tr.attr('data-row-state');
									
									detail_arr['rowState'] = row_state;

									if(typeof module_name != "undefined"){
										if(module_name == 'User Role Setup'){
											if(output_type == 'update'){
												detail_arr['RA_ID'] = current_tr.attr('data-ugm-id');
											} 
										}
									}

									$(current_tr.find('td')).each(function(index){
										let current_td 	= $(this);
										let td_name 	= current_td.attr('data-name');
										let td_value 	= current_td.attr('data-value');

										console.log(td_name);
										console.log(td_value);
										
										detail_arr[td_name] = td_value;
									});
									
									delete detail_arr['undefined'];

									detail.push(detail_arr);

								});
							
								data['details'] = detail;

								if(tbl_subdtl_lngt > 0){
									$('#tbl-sub-detail tbody tr').each(function(){
										let current_tr 		= $(this);
										let subdetail_arr 	= {};
										let tr_data 	= current_tr.data();
										let row_state	= current_tr.attr('data-row-state');

										
										subdetail_arr['rowState'] = row_state;

										$(current_tr.find('td')).each(function(index){
											let current_td 	= $(this);
											let td_name 	= current_td.attr('data-name');
											let td_value 	= current_td.attr('data-value');
											
											subdetail_arr[td_name] = td_value;
										});
										
										delete subdetail_arr['undefined'];

										sub_detail.push(subdetail_arr);

									});
									
									data['sub-details'] = sub_detail;
								}


								console.log(data);


							} // End of else condition for Detail table length

						}
					} // End of isRequired condition

					
					// SAVING FUNCTION OF WHOLE HEADER AND DETAIL

					$.ajax({
						type: 'POST',
						dataType: 'json',
						url: global.site_name + module_folder + '/' + module_controller + '/save',
						data : data,
						success : function(result){
							if(result.success){
								swal((output_type == 'add') ? "Saving Success!" : "Update Success!",(output_type == 'add') ? module_name + " saving complete!" : module_name + " updating complete!", "success")
								setTimeout(function(){
									if(output_type == 'add'){
										if(typeof attr_url != "undefined"){
											if(action_page == 'save-new'){
				        						window.location.href = global.site_name + attr_folder + '/' + attr_controller + '/add';
											}
											else{
												window.location.href = global.site_name + attr_folder + '/' + attr_controller;
											}
				        				}
				        				else{
				        					if(action_page == 'save-new'){
				        						window.location.href = global.site_name + module_folder + '/' + module_controller + '/add';
											}
											else{
												if(module_name == "Physical Count"){
													window.location.href = global.site_name + module_folder + '/' + module_controller + update_link;
												}
												else{
													window.location.href = global.site_name + module_folder + '/' + module_controller;
												}
											}
				        				}
									}
									else if(output_type == 'update'){
										if(typeof attr_url != "undefined"){
				        					window.location.href = global.site_name + attr_folder + '/' + attr_controller;
				        				}
				        				else{
				        					window.location.href = global.site_name + module_folder + '/' + module_controller;
				        				}
									}
									else{
										swal("Error!", "Please contact your system administrator", "error")
									}
									
								},1500);
							}
							else{
								swal(result.title, result.message, result.type);
							}
						},
						error : function(){
							// swal('Error', 'Contact your system administrator', 'error');
							swal("Error!", "Please contact your system administrator", "error")

						}
					});

				} // End  of header length condition
				
				
	        }); // End of swal function

		}); // End of save function

		
	}

	on_save_module();


});


// Added by Jekzel Leonidas 03242020
function getFormData($form){
	var unindexed_array = $form.serializeArray();
	var indexed_array = {};

	$.map(unindexed_array, function(n, i){
	    indexed_array[n['name']] = n['value'];
	});

	return indexed_array;
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}