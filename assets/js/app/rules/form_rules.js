$(document).ready(function() {
	// FUNCTION FOR ASKING THE USER IF THEY WANT TO LEAVE THE PAGE SAVING / UPDATING.
	$('#back ').on('click', function(e){
		if(typeof(output_type) != 'undefined' && output_type !== null) {
			if(output_type == 'update'){
				if(confirm("Are you sure you want to leave?")){
					$.ajax({
		        		url 		: module_url + '/deleteUserRecordLock_ajax',
		        		type 		: 'POST',
		        		dataType 	: 'json',
		        		data 		: {
		        						'UR_PrimaryKey' 	: doc_no
		        					  },
		        		success 	: function(result){
		        			if(result){
		        				if(typeof attr_url != "undefined"){
		        					window.location.href = attr_url;
		        				}
		        				else{
		        					window.location.href = module_url;
		        				}
		        			}
		        		}
		        	});
				}
			}
			else{
				if(typeof attr_url != "undefined"){
					window.location.href = attr_url;
				}
				else{
					window.location.href = module_url;
				}
			}
		}
		// END OF URL-CHANGE CLICK
	});

	$('.url-change').on('click', function(e){
		if(typeof(output_type) != 'undefined' && output_type !== null) {
			let href_val = $(this).attr('href');
			if(output_type == 'update'){
				if(confirm("Are you sure you want to leave?")){
					$.ajax({
		        		url 		: module_url + '/deleteUserRecordLock_ajax',
		        		type 		: 'POST',
		        		dataType 	: 'json',
		        		data 		: {
		        						'UR_PrimaryKey' 	: doc_no
		        					  },
		        		success 	: function(result){
		        			if(result){
		        				if(typeof attr_url != "undefined"){
		        					window.location.href = href_val;
		        				}
		        				else{
		        					window.location.href = href_val;
		        				}
		        			}
		        		}
		        	});
				}
			}
		}
		else{
			if(typeof attr_url != "undefined"){
				window.location.href = href_val;
			}
			else{
				window.location.href = href_val;
			}
		}
		// END OF URL-CHANGE CLICK
	});

	if(typeof(output_type) != "undefined" && output_type !== null) {
		// $('body').removeClass('fixed-sidebar no-skin-config full-height-layout pace-done').addClass('fixed-sidebar no-skin-config full-height-layout pace-done mini-navbar');
		if($(window).width() >= 1024) {    

			if(typeof(table_movement) != "undefined"){
				table_movement.columns.adjust().draw();
			}

			if(typeof(table_maintenance) != "undefined"){
				table_maintenance.columns.adjust().draw();
			}

			if(typeof(table_components) != "undefined"){
				table_components.columns.adjust().draw();
			}
    	}		
	}
	// END OF FORM RULES JS

	// Event when adding focus event on tabindex
	setTimeout(function(){
		// Set Focus on tabindex = 1
		if($('body [tabindex="1"]').length > 0 && (typeof(output_type) != "undefined" && output_type !== null) ){
			$('body [tabindex="1"]').focus();
		}
	},1000);
});

function validateFields(){
	var blnValidate = true;
	if(typeof(output_type) != "undefined" && output_type !== null) {
		// Use for Validation of Required Fields.
		if(output_type == 'add' || output_type == 'update'){
			var fields = $('.required-field');
			//remove all required fields text first
			$('.validate-style').remove();

			for(var i = 0; i < fields.length; i++){
				if($(fields[i]).val() == '' || $(fields[i]).val() == null || $(fields[i]).val() == undefined){	
					let html = `<span class="validate-style">
									This field is required.
   								</span>`;
					$(fields[i]).before(html);
					blnValidate = false;
				}
			}
		}
	}
	return blnValidate;
}

// dont allow negative value or any other character besides numeric
$("body").delegate('input[type="number"]', 'focusout', function(){
    if($(this).val() < 0){
        $(this).val(0);
    }
});

$("body").delegate('input[type="number"]', 'click focusin', function(){        
      $(this).select();
});

$("body").delegate('input[name="qty-input"]', 'click focusin', function(){        
      $(this).select();
});

$('.dataTables-example').on('click', '.update-header', function(){
	let current_row = $(this).closest('.table-header');

	$.ajax({
		url 		: global.site_name + controller_url + 'checkUserRecordLock',
		type 		: 'POST',
		dataType 	: 'json',
		data 		: {
						'UR_PrimaryKey' 	: current_row.find('[data-doc-no]').data('doc-no')
					  },
		success		: function(result){
			if(result > 0){
				swal('Error!', 'This document is currently being updated by other user. Try again later.', 'error');
			}
			else{
				$.ajax({
					url 		: global.site_name + controller_url + 'insertUserRecordLock',
					type 		: 'POST',
					dataType 	: 'json',
					data 		: {
									'UR_PrimaryKey' 	: current_row.find('[data-doc-no]').data('doc-no'),
									'UR_TableName' 		: table_name,
									'UR_ProcessAction' 	: process_action
								  },
					success 	: function(result){
						if(result){
							window.location.href = current_row.find('[data-link]').data('link');
						}
					}
				})

			}
		}
	})
})

if(typeof(output_type) != 'undefined'){
	if(output_type == 'update'){

		if (window.history && window.history.pushState) {

		    $(window).on('popstate', function() {
		      var hashLocation = location.hash;
		      console.log(hashLocation);
		      var hashSplit = hashLocation.split("!/");
		      console.log(hashSplit);
		      var hashName = hashSplit[1];
		      console.log(hashName)
		      if (hashName !== '' && hashName !== undefined) {
		      	console.log('test')
		        var hash = window.location.hash;
		        if (hash === '') {
		        	$.ajax({
		        		url 		: module_url + '/deleteUserRecordLock_ajax',
		        		type 		: 'POST',
		        		dataType 	: 'json',
		        		data 		: {
		        						'UR_PrimaryKey' 	: doc_no
		        					  },
		        		success 	: function(result){
		        			if(result){
		        				window.location.href = module_url;
		        			}
		        		}
		        	});
		        }
		      }
		    });

		    window.history.pushState('forward', null, update_link);
		}
	}
}
