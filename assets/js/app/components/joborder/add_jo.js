function report_print_option(){
	print_cb = print;
	return show_print_option();
}
var print_cb = function(){};
var show_signatory = function(){};

$(document).ready(function(){
	var param = $('#print').data('param');
	show_print_option = function(){
		var dialog = bootbox.dialog({
			title: 'Which type of report you\'ll like?',
			message: "<h2>Select which type of report you've wanted to!</h2>",
			buttons: {
			    
			    list: {
			        label: "List",
			        className: 'btn-warning',
			        callback: function(){
			            param['type'] = 'list';
			            print_cb();
			        }
			    },
			    categorized: {
			        label: "Categorized",
			        className: 'btn-info',
			        callback: function(){
			            param['type'] = 'categorized';
			            print_cb();
			        }
			    },
			    cancel: {
			        label: '<i class="fa fa-times"></i> Cancel',
			        className: 'btn-danger',
			        callback: function(){
			        }
			    }
			}
			});
		// swal({
		// 	title: "Which type of report you'll like?",
		// 	text: "Select which type of report you've wanted to!",
  // 			icon: "info",
		//   	buttons: {			    
		// 	    list: {
		// 	    	text : "List",
		// 	    	value: "list",
		// 	    },
		// 	    categorized: {
		// 	    	text : "Categorized",
		// 	    	value: "categorized",
		// 	    },
		//     	cancel: "Cancel",
		//   },
		// })
		// .then((value) => {
		// 	var isPrint = false;
		// 	switch (value) {	 
		// 		case "categorized":
		// 		isPrint = true;
		// 		param['type'] = 'categorized';
		// 	break;

		// 	case "list":
		// 		isPrint = true;
		// 		param['type'] = 'list';
		// 	break;

		// 	default:

		// 	}
		// 	if(isPrint){
		// 		print_cb();
		// 	}
		// });
	}

	function init(){
		$.fn.modal.Constructor.prototype.enforceFocus = function() {};
		$('.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "mm/dd/yyyy"
        });

        $(".js-select2").select2();

		$('.all').click(function(){
			$('.single input').prop("checked", $(this).is(':checked'));
		});

		$("#add-jo-detail").click(function(){
			jo_detail.show_jo_modal('add');
		});

		$("#add-jo-sub-detail").prop("disabled", true).click(function(){

			jo_detail.show_jo_sub_detail_modal('add');
		});

		$("#add-from-jo").click(function(){
			if($(this).val() == 'true'){
				$(this).val('false')
				$("#jo-transfer-request").hide();
			}
			else{
				$(this).val('true')
				$("#jo-transfer-request").show();
			}
		});

		$('.chosen-select').chosen({width: "100%"});

		$('#jo-detail #add-close').click(function(){
			if($(this).val() == 'add'){
				jo_detail.add_detail();
				$("#jo-detail .close").click();
			}
			else if($(this).val() == 'edit'){
				jo_detail.edit_detail();
				$("#jo-detail .close").click();
			}
			else if($(this).val() == 'readd' ){
				jo_detail.edit_detail($(this).val());
				$("#jo-detail .close").click();
			}
		});

		$('#jo-sub-detail #add-close').click(function(){
			if($(this).val() == 'add'){
				jo_detail.add_sub_detail();
				$("#jo-sub-detail .close").click();
			}
			else if($(this).val() == 'edit'){
				jo_detail.edit_sub_detail();
				$("#jo-sub-detail .close").click();
			}
			else if($(this).val() == 'readd' ){
				jo_detail.edit_sub_detail();
				$("#jo-sub-detail .close").click();
			}
		});

		$('#jo-transfer-request #add-close').click(function(){			
			
			$("#add-from-jo").val('false');
			$('#jo-transfer-request').hide();
			jo_detail.add_detail_from_tr();

			
			$('.single input').prop("checked", false);
		});
		
		$('#jo-sub-detail #quantity').change(function(){
			jo_detail.process_total();
		});

		$('#jo-sub-detail #unit-cost').change(function(){
			jo_detail.process_total();
		});
		
		
		// $('.bom-item').change(function(){
		// 	$('.bom-item').val($(this).val());
		// 	$('.bom-item').trigger("chosen:updated");
		// 	console.log($('#bom-item-description').val());
		// 	$('#bom-description').val($('#bom-item-description option:selected').text());
		// });
		
		$('#jo-detail .bom').change(function(){
			$('.bom').val($(this).val());
			$('.bom').trigger("chosen:updated");
			//console.log($('#jo-detail #bom-cost').data('bom-cost'));
			$('#jo-detail #bom-cost').data('bom-cost', $('#bom-no.bom option:selected').data("cost"));
			$('#jo-detail #bom-cost').val(number_format($('#bom-no.bom option:selected').data("cost"), 2, '.', ''));
			$('#jo-detail #bom-remarks').val($('#bom-no.bom option:selected').data("remarks"));
		});
		
		$('#ongoing').click(function(){
			var doc_no = $(this).data('doc-no');

			swal({
                title: "Are you sure?",
                text: "You will be marking this Job Order as ongoing",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, mark it!",
                closeOnConfirm: true
            }, function (){
            	$.ajax({
					type: 'POST',
					dataType: 'json',
					url: global.site_name + 'joborder/markOnGoing',
					data : {
						'doc-no'	: doc_no
					},
					success : function(result){
						if(result.success){
							window.location.reload();
						}
					}
				});
            });
		});


		$('#post').click(function(){
			var doc_no = $(this).data('doc-no');

			swal({
                title: "Are you sure?",
                text: "You will be posting this job order",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, post this job order!",
                closeOnConfirm: true
            }, function (){
            	$.ajax({
					type: 'POST',
					dataType: 'json',
					url: global.site_name + 'joborder/markPosted',
					data : {
						'doc-no'	: doc_no
					},
					success : function(result){
						if(result.success){
							window.location.reload();
						}
					}
				});
            });
		});

		$('#save').click(function(){
			var action = $('#save').data('todo');
            swal({
                title: "Are you sure?",
                text: (action == 'add')? "You will add this current job order." : "You will save updated changes in this current job order",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false
            }, function () {
            	var jo_details = [];

				$('#jo-detail-list > tbody  > tr').each(function(){
					var line_no = $(this).data("line-no")
					var jo_sub_detail = [];
					$('#jo-sub-detail-list > tbody  > tr[data-jo-line-no="' + line_no + '"]').each(function(){
						jo_sub_detail.push({
							'todo'	: $(this).data("action")
						,	'doc-no'	: '0'
						,	'line-no'	: line_no
						,	'entry-no'	: $(this).data("line-no")
						,	'item-no'	: $(this).find("[data-item-no]").data("item-no")
						,	'item-description'	: $(this).find("[data-item-description]").data("item-description")
						,	'barcode'	: $(this).find("[data-barcode]").data("barcode")
						,	'quantity'	: $(this).find("[data-quantity]").data("quantity")
						,	'uom'	: $(this).find("[data-uom-id]").data("uom-id")
						,	'unit-cost'	: $(this).find("[data-unit-cost]").data("unit-cost")
						,	'total-cost'	: $(this).find("[data-total-cost]").data("total-cost")
						,	'waste'	: $(this).find("[data-waste]").data("waste")
						,	'required-qty'	: $(this).find("[data-required-quantity]").data("required-quantity")
						,	'actual'	: $(this).find("[data-actual]").data("actual")
						,	'comment'	: $(this).find("[data-comment]").data("comment")
						,	'is-original'	: $(this).data("is-original")
							//JOSD_JOD_JO_DocNo
							//JOSD_JOD_LineNo
							//JOSD_EntryNo
							//JOSD_ItemNo
							//JOSD_Barcode
							//JOSD_ItemDescription
							//JOSD_Qty
							//JOSD_UOM
							//JOSD_UnitCost
							//JOSD_TotalCost
							//JOSD_Waste
							//JOSD_RequiredQty
							//JOSD_Actual
							//JOSD_Comment
							//JOSD_BaseUOM
							//JOSD_isBOM
							//JOSD_BOMNo
							//JOSD_BOMDescription
							//CreatedBy
							//DateCreated
							//ModifiedBy
							//DateModified
						});
					});

					jo_details.push({
						'todo' 		: $(this).data("action")
					,	'doc-no'	: '0'
					,	'tr-doc-no'		: $(this).find("[data-tr-doc-no]").data("tr-doc-no")
					,	'line-no' 		: $(this).data("line-no")
					,	'bom-no' 		: $(this).find("[data-bom-doc-no]").data("bom-doc-no")
					,	'bom-description' 		: $(this).find("[data-bom-description]").data("bom-description")
					,	'item-description' 		: $(this).find("[data-item-description]").data("item-description")
					,	'item-no' 		: $(this).find("[data-item-no]").data("item-no")
					,	'barcode' 		: $(this).find("[data-barcode]").data("barcode")
					,	'yield-qty' 		: $(this).find("[data-yield]").data("yield")
					,	'yield-uom' 		: $(this).find("[data-yield-uom-id]").data("yield-uom-id")
					,	'batch-qty' 		: $(this).find("[data-batch-size]").data("batch-size")
					,	'batch-uom' 		: $(this).find("[data-batch-size-uom-id]").data("batch-size-uom-id")
					,	'location'			: $(this).find("[data-location]").data("location")
					,	'remarks'			: $(this).find("[data-remarks]").data("remarks")
					,	'base-qty'			: $(this).find("[data-reqstd]").data("reqstd")
					,	'base-uom'			: '0'
					,	'actual'			: $(this).find("[data-actual]").data("actual")
					,	'cost'				: $(this).find("[data-unit-cost]").data("unit-cost")
					,	'sub-detail'		: jo_sub_detail
					});
				});
				console.log(jo_details)
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: global.site_name + 'joborder/save',
					data : {
						'todo'		: action,
						'doc-date'		: $('#form-jo #doc-date').val(),
						'date-needed'		: $('#form-jo #date-needed').val(),
						'status'		: $('#form-jo #status').val(),
						'remarks'		: $('#form-jo #remarks').val(),
						'jo-details'	: jo_details,
						'jo-doc-no'		: $('#doc-no').val()

					},
					success : function(result){
						//console.log(result);
						if(result.success){
							window.location.href = global.site_name + "joborder/jolist";
						}
					}
				});
            });			
		});

		$('#jo-detail').on('hidden.bs.modal', function (e) {
			jo_detail.clear_form();
		});

		$('#jo-sub-detail').on('hidden.bs.modal', function (e) {
			jo_detail.clear_sub_form();
		});

		$('#jo-sub-detail .item').change(function(){
			$('#jo-sub-detail .item').val($(this).val());
			$('#jo-sub-detail .item').trigger("chosen:updated");
			$('#jo-sub-detail #unit-cost').data('unit-cost', $('#jo-sub-detail #item-no.item option:selected').data("price"));
			$('#jo-sub-detail #unit-cost').val(number_format($('#jo-sub-detail #item-no.item option:selected').data("price"), 2));
			$('#jo-sub-detail #average-cost').val(number_format($('#jo-sub-detail #item-no.item option:selected').data("price"), 2));
			jo_detail.process_total();
			$.ajax({
				type: 'GET',
				dataType: 'json',
				url: global.site_name + 'bom/getUOMConversion',
				data : {'item-no' : $(this).val()},
				success : function(result){					
					var list = {
						'html'		: ''
					,	'format'	: function(data){
							return '<option value="' + data.AD_Id + '">' + data.AD_Desc + '</option>';
						}
					,	'refresh'	: function(){
							$("#jo-sub-detail #uom-list").empty();
							result.forEach(function(item, index){
								list.html+=list.format(item);							
							});
							$("#jo-sub-detail #uom-list").append(list.html);
							$("#jo-sub-detail #uom-list").trigger("chosen:updated");													
						}
					}
					list.refresh();
				}
			});
		});		


		jo_detail.reload_row_events();
		jo_detail.reload_row_sub_events();
	}
	
	var jo_detail = {
		show_jo_modal : function(method){
			if(method=='add'){
				$("#jo-detail").modal('show');
				//$("#jo-detail #add-new").show();
				$("#jo-detail #add-close").text('ADD & CLOSE');
				//$("#jo-detail #add-new").val(method)
				$("#jo-detail #add-close").val(method)
			}
			else if(method=='readd' || method=='edit'){
				$("#jo-detail").modal('show');
				//$("#jo-detail #add-new").hide();
				$("#jo-detail #add-close").text('SAVE');
				//$("#jo-detail #add-new").val(method)
				$("#jo-detail #add-close").val(method)
			}
		},
		show_jo_sub_detail_modal : function(method){
			if(method=='add'){
				$("#jo-sub-detail").modal('show');
				var selected_jo_detail = $('#jo-sub-detail-list tbody > tr:visible');

				$('#jo-sub-detail #bom-doc-no').val(selected_jo_detail.data('bom-doc-no'));
				$('#jo-sub-detail #line-no').val(selected_jo_detail.data('line-no'));
				$('#jo-sub-detail #jo-line-no').val(selected_jo_detail.data('jo-line-no'));
				
				$("#jo-sub-detail #add-close").text('ADD & CLOSE');
				$("#jo-sub-detail #add-close").val(method)
			}
			else if(method=='readd' || method=='edit'){
				$("#jo-sub-detail").modal('show');
				$("#jo-sub-detail #add-close").text('SAVE');
				$("#jo-sub-detail #add-close").val(method);
			}
		},

		process_total : function(){
			var total = Number($("#jo-sub-detail #quantity").val()) * Number($("#jo-sub-detail #unit-cost").val())
			$("#jo-sub-detail #total-cost").val(number_format(total, 2));	
			$("#jo-sub-detail #total-cost").data('total-cost', total);			
		},
		update_bom_total : function(jo_line_no){
			var total = 0;
			$('#jo-sub-detail-list > tbody > tr.jo-sub-detail[data-jo-line-no="' + jo_line_no + '"]').each(function(){
				total +=Number($(this).find('[data-total-cost]').data('total-cost'));
			});
			$('#jo-detail-list .jo-detail[data-line-no="' + jo_line_no + '"]').find('[data-unit-cost]').data('unit-cost', total);
		},
		row_detail_format	:	function(row){
			console.log(row)
			return 	'<tr class="' + row['color'] + ' jo-detail" style="cursor: pointer" data-action="' + row['action'] + '"  data-line-no="' + row['line-no'] + '">' +
						'<td data-tr-doc-no="' + row['tr-no'] + '">' + row['tr-no'] + '</td>' + 
						'<td data-bom-doc-no="' + row['bom-doc-no'] + '">' + row['bom-doc-no'] + '</td>' + 
						'<td>' + row['doc-date'] + '</td>' + 
						'<td data-barcode="' + row['barcode'] + '" data-item-no="' + row['item-no'] + '" data-bom-description="' + row['bom-description'] + '" data-unit-cost="' + row['cost'] + '">' + row['bom-description'] + '</td>' + 
						'<td data-item-description="' + row['item-description'] + '">' + row['item-description'] + '</td>' + 
						'<td class="text-right" data-yield="' + row['yield'] + '" data-batch-size="' + row['batch-size'] + '" data-yield-uom-id="' + row['yield-uom-id'] + '" data-batch-size-uom-id="' + row['batch-size-uom-id']+ '">' + number_format(row['yield'], 2) + ' / ' + row['yield-uom'] + '</td>' + 
						'<td class="text-right" data-reqstd="' + row['base-qty'] + '">' + number_format(row['base-qty'], 4) + '</td>' + 
						'<td data-actual="' + row['actual'] + '" class="text-right">' + number_format(row['actual'], 2) + '</td>' + 
						'<td data-location="' + row['location'] + '">' + row['location'] + '</td>' + 
						'<td data-remarks="' + row['remarks'] + '">' + row['remarks'] + '</td>' + 
						'<td class="text-center">' +
							'<button class="edit-jo-detail btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Edit" type="button"><i class="fa fa-pencil"></i></button>' + 
							'<button class="delete-jo-detail btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button"><i class="fa fa-trash-o"></i></button>' +
						'</td>' + 
					'</tr>';
		},

		row_sub_detail_format	: function(row, docNo, is_original){
			return '<tr class="' + row['color'] + ' jo-sub-detail ' + '" data-bom-doc-no="' + docNo + '" data-jo-line-no="' + row['jo-line-no'] + '" data-line-no="' + row['line-no'] + '" data-action="' + row['action'] + '" data-is-original="' + is_original + '">' +
						'<td data-item-no="' + row['item-no'] + '" data-barcode="' + row['barcode'] + '">' + row['item-no'] + '</td>' +
						'<td data-item-description="' + row['item-description'] + '">' + row['item-description'] + '</td>' +
						'<td class="text-right" data-quantity="' + row['quantity'] + '" data-required-quantity="' + row['required-quantity']+ '">' + number_format(row['quantity'], 4) + '</td>' +
						'<td data-uom-id="' + row['uom-id'] + '">' + row['uom'] + '</td>' +
						'<td class="text-right" data-unit-cost="' + row['unit-cost'] + '">' + number_format(row['unit-cost'], 2) + '</td>' +
						'<td class="text-right" data-total-cost="' + row['total-cost'] + '">' + number_format(row['total-cost'], 2) + '</td>' +
						'<td class="text-right" data-waste="' + row['waste'] + '">' + number_format(row['waste'], 2) + '</td>' +
						'<td data-actual="' + row['actual'] + '" class="text-right">' + number_format(row['actual'], 2, ".", "") + '</td>' +
						'<td data-comment="' + row['comment'] + '">' + row['comment'] + '</td>' +
						'<td class="text-center">' +
							'<button class="edit-jo-sub-detail btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Edit" type="button" value="edit"><i class="fa fa-pencil"></i></button> ' + 
							((!is_original)? '<button class="delete-jo-sub-detail btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button"><i class="fa fa-trash-o"></i></button>' : '')
						'</td>' + 
					'</tr>';
		},
		reload_row_events	:	function(){
			$('.edit-jo-detail').unbind();
			$('.delete-jo-detail').unbind();
			$('.jo-detail').unbind();

			$('.edit-jo-detail').click(function(){
				var row = $(this).closest('.jo-detail');
				var todo = row.data('action');
				if(todo!='delete'){
					//For Update
					jo_detail.show_jo_modal((todo =='add')? 'readd' : 'edit');
					jo_detail.set_jo_form_data(row);					
				}
			});

			$('.delete-jo-detail').click(function(){
				var current_row = $(this).closest('.jo-detail');
				if(current_row.data('action')=='add'){
					//Remove entire rOw
					current_row.remove();
					$('#jo-sub-detail-list .jo-sub-detail[data-jo-line-no="' + current_row.data('line-no') + '"]').remove();
				}
				else if(current_row.data('action')=='none'){
					//To be delete on 
					current_row.addClass('danger');
					current_row.data('action', 'delete');

					$('#jo-sub-detail-list .jo-sub-detail[data-jo-line-no="' + current_row.data('line-no') + '"]').addClass('danger');					
					$('#jo-sub-detail-list .jo-sub-detail[data-jo-line-no="' + current_row.data('line-no') + '"]').data('action', 'delete');
					console.log($('#jo-sub-detail-list .jo-sub-detail[data-jo-line-no="' + current_row.data('line-no') + '"]').data('action'));
					//$('#jo-sub-detail-list .jo-sub-detail[data-jo-line-no="' + current_row.data('line-no') + '"] .').
				}
				$('#add-jo-sub-detail').prop('disabled', true);
			});

			$(".jo-detail").click(function(){
				var line_no = $(this).data('line-no');
				$('#jo-detail-list tbody tr').removeClass('selected');
				$(this).addClass('selected');				
				$('.jo-sub-detail').hide();
				$('.jo-sub-detail[data-jo-line-no="' + line_no + '"]').show();

				$('#add-jo-sub-detail').prop('disabled', false);
			});
		},
		reload_row_sub_events	:	function(){
			$('.edit-jo-sub-detail').unbind();
			$('.delete-jo-sub-detail').unbind();

			$('.edit-jo-sub-detail').click(function(){
				var row = $(this).closest('.jo-sub-detail');
				var todo = row.data('action');
				if(todo!='delete'){
					//For Update
					jo_detail.show_jo_sub_detail_modal((todo =='add')? 'readd' : 'edit');
					jo_detail.set_jo_detail_form_data(row);			
				}				

			});
			$('.delete-jo-sub-detail').click(function(){
				$(this).closest('.jo-sub-detail').remove();
			});
		},
		add_detail_from_tr	: function(){
			var onList = [];
			var alreadySelected = false;
			$('#jo-detail-list .jo-detail').each(function(){
				var trno = $(this).find('[data-tr-doc-no]').data('tr-doc-no');
				onList.push(trno);
			});

			$('#jo-transfer-request .jo-tr').each(function(){
				if($(this).find('.single input[type="checkbox"]').is(":checked")){
					var trno = $(this).find('[data-tr-doc-no]').data('tr-doc-no');
					var reqstd = $(this).find('[data-quantity]').data('quantity');
					if(onList.indexOf(trno) < 0 ){
						$.ajax({
							type: 'GET',
							dataType: 'json',
							url: global.site_name + 'joborder/getBomInfo',
							data : {'bom-doc-no' : $(this).find('[data-bom-doc-no]').data('bom-doc-no')},
							success : function(result){
								var row = {		'action'	: ''
									,	'tr-no'	: trno
									,	'bom-doc-no'	:	result.BOM_DocNo
									,	'doc-date'	:	result.BOM_DocDate
									,	'bom-description'	:	result.BOM_BOMDescription
									,	'barcode'	:	result.BOM_Barcode
									,	'item-no'	:	result.BOM_ItemNo
									,	'item-description'	:	result.BOM_ItemDescription
									,	'yield'	:	result.BOM_YieldQty
									,	'yield-uom-id'	:	result.BOM_YieldUOM
									,	'yield-uom'	:	result.y_desc
									,	'batch-size'	:	result.BOM_BatchQty
									,	'batch-size-uom-id'	:	result.BOM_BatchUOM
									,	'batch-size-uom'	:	result.b_desc
									,	'base-qty'	:	reqstd
									,	'cost'	:	result.BOM_Cost
									,	'actual'	:	'0.00'
									,	'location'	:	result.BOM_Location
									,	'remarks'	:	result.BOM_Remarks
								};

								row['action'] = 'add';
								row['color'] = 'success';
								row['line-no'] = jo_detail.get_temp_line_no();
								var rowdetail = jo_detail.row_detail_format(row);			
								$('#jo-detail-list tbody').append(rowdetail);
								var jo_line_no = row['line-no'];
								var bom_doc_no = row['bom-doc-no'];
								$.ajax({
									type: 'GET',
									dataType: 'json',
									url: global.site_name + 'joborder/getBomDetail',
									data : {'bom-doc-no' : bom_doc_no},
									success : function(result){
										$('.jo-sub-detail').hide();
										var rowdetail = '';
										for(var key in result){
											var raw = result[key];
											var row_data = [];
											row_data['jo-line-no'] = jo_line_no;
											row_data['line-no'] = raw['BOMD_LineNo'];
											row_data['item-no'] = raw['BOMD_ItemNo'];
											row_data['barcode'] = raw['BOMD_Barcode'];
											row_data['item-description'] = raw['BOMD_ItemDescription'];
											row_data['quantity'] = raw['BOMD_Qty'];
											row_data['required-quantity'] = raw['BOMD_RequiredQty'];
											row_data['uom-id'] = raw['BOMD_UOM'];
											row_data['uom'] = getString(raw['AD_Desc']);
											row_data['unit-cost'] = raw['BOMD_UnitCost'];
											row_data['total-cost'] = raw['BOMD_TotalCost'];
											row_data['waste'] = raw['BOMD_Waste'];
											row_data['comment'] = raw['BOMD_Comment'];
											row_data['actual']	=	'0.00';
											row_data['action'] = 'add';
											row_data['color'] = 'success';
											rowdetail += jo_detail.row_sub_detail_format(row_data, bom_doc_no, true);						
										}
										$('#jo-sub-detail-list tbody').append(rowdetail);
										jo_detail.reload_row_sub_events();		
										jo_detail.reload_row_events();
										$('#add-jo-sub-detail').prop('disabled', false);
									}
								});
							}
						});
					}
					else{
						alreadySelected = true;
					}				
				}
			});

			if(alreadySelected){
				swal("Failed to add Job Order!", "The selected/s Transfer Request is already on the Job Order Details!", "error");
			}	
			
		},
		add_detail	:	function(){
			var row = {		'action'	: ''
						,	'tr-no'	: ''
						,	'bom-doc-no'	:	$('#jo-detail #bom-no').val()
						,	'doc-date'	:	$('#jo-detail #bom-no option:selected').data('doc-date')
						,	'bom-description'	:	$('#jo-detail #bom-description option:selected').text()
						,	'barcode'	:	$('#jo-detail #bom-barcode option:selected').text()
						,	'item-no'	:	$('#jo-detail #bom-item-no option:selected').text()
						,	'item-description'	:	$('#jo-detail #bom-item-description option:selected').text()
						,	'yield'	:	$('#jo-detail #bom-yield').val()
						,	'yield-uom-id'	:	$('#jo-detail #bom-yield-uom').val()
						,	'yield-uom'	:	$('#jo-detail #bom-yield-uom option:selected').text()
						,	'batch-size'	:	$('#jo-detail #bom-batch-size').val()
						,	'batch-size-uom-id'	:	$('#jo-detail #bom-batch-size-uom').val()
						,	'batch-size-uom'	:	$('#jo-detail #bom-batch-size-uom option:selected').text()
						,	'base-qty'	:	$('#jo-detail #bom-reqstd-qty').val()
						,	'cost'	:	$('#jo-detail #bom-cost').val()
						,	'actual'	:	$('#jo-detail #bom-actual').val()
						,	'location'	:	$('#jo-detail #bom-no option:selected').data('location')
						,	'remarks'	:	$('#jo-detail #bom-remarks').val()
						};

			row['action'] = 'add';
			row['color'] = 'success';
			row['line-no'] = jo_detail.get_temp_line_no();
			var rowdetail = jo_detail.row_detail_format(row);			
			$('#jo-detail-list tbody').append(rowdetail);
			var jo_line_no = row['line-no'];
			var bom_doc_no = row['bom-doc-no'];
			$.ajax({
				type: 'GET',
				dataType: 'json',
				url: global.site_name + 'joborder/getBomDetail',
				data : {'bom-doc-no' : bom_doc_no},
				success : function(result){
					$('.jo-sub-detail').hide();
					var rowdetail = '';
					for(var key in result){
						var raw = result[key];
						var row_data = [];
						row_data['jo-line-no'] = jo_line_no;
						row_data['line-no'] = raw['BOMD_LineNo'];
						row_data['item-no'] = raw['BOMD_ItemNo'];
						row_data['barcode'] = raw['BOMD_Barcode'];
						row_data['item-description'] = raw['BOMD_ItemDescription'];
						row_data['required-quantity'] = raw['BOMD_RequiredQty'];
						row_data['quantity'] = raw['BOMD_Qty'];
						row_data['uom-id'] = raw['BOMD_UOM'];
						row_data['uom'] = getString(raw['AD_Desc']);
						row_data['unit-cost'] = raw['BOMD_UnitCost'];
						row_data['total-cost'] = raw['BOMD_TotalCost'];
						row_data['waste'] = raw['BOMD_Waste'];
						row_data['comment'] = raw['BOMD_Comment'];
						row_data['actual']	= '0'
						row_data['action'] = 'add';
						row_data['color'] = 'success';
						rowdetail += jo_detail.row_sub_detail_format(row_data, bom_doc_no, true);						
					}
					$('#jo-sub-detail-list tbody').append(rowdetail);
					jo_detail.reload_row_sub_events();
					$('#add-jo-sub-detail').prop('disabled', false);
				}
			});
			jo_detail.reload_row_events();
		},
		edit_detail	: function(isReadd = undefined){
			var row = 	{	'action'	: ''
						,	'tr-no'	: ''
						,	'bom-doc-no'	:	$('#jo-detail #bom-no').val()
						,	'doc-date'	:	$('#jo-detail #bom-no option:selected').data('doc-date')
						,	'bom-description'	:	$('#jo-detail #bom-description option:selected').text()
						,	'barcode'	:	$('#jo-detail #bom-barcode option:selected').text()
						,	'item-no'	:	$('#jo-detail #bom-item-no option:selected').text()
						,	'item-description'	:	$('#jo-detail #bom-item-description option:selected').text()
						,	'yield'	:	$('#jo-detail #bom-yield').val()
						,	'yield-uom-id'	:	$('#jo-detail #bom-yield-uom').val()
						,	'yield-uom'	:	$('#jo-detail #bom-yield-uom option:selected').text()
						,	'batch-size'	:	$('#jo-detail #bom-batch-size').val()
						,	'batch-size-uom-id'	:	$('#jo-detail #bom-batch-size-uom').val()
						,	'batch-size-uom'	:	$('#jo-detail #bom-batch-size-uom option:selected').text()
						,	'base-qty'	:	$('#jo-detail #bom-reqstd-qty').val()
						,	'cost'	:	$('#jo-detail #bom-cost').val()
						,	'actual'	:	$('#jo-detail #bom-actual').val()
						,	'location'	:	$('#jo-detail #bom-no option:selected').data('location')
						,	'remarks'	:	$('#jo-detail #bom-remarks').val()
						};

			row['action'] = (isReadd === undefined)? 'edit': 'add';
			row['color'] = (isReadd === undefined)? 'info': 'success';
			row['line-no'] = $('#jo-detail #line-no').val();

			var jo_line_no = row['line-no'];
			var previous_row = $('.jo-detail[data-line-no="' + jo_line_no + '"]');

			$('.jo-detail[data-line-no="' + jo_line_no + '"]').replaceWith(jo_detail.row_detail_format(row));

			if(previous_row.find('[data-bom-doc-no]').data('bom-doc-no') == $('#jo-detail #bom-no').val()){
				//Means no Change in selected BOM

			}
			else{
				$('.jo-sub-detail[data-jo-line-no="' + jo_line_no + '"]').remove();
				$.ajax({
					type: 'GET',
					dataType: 'json',
					url: global.site_name + 'joborder/getBomDetail',
					data : {'bom-doc-no' : row['bom-doc-no']},
					success : function(result){
						$('.jo-sub-detail').hide();
						var rowdetail = '';
						for(var key in result){
							var raw = result[key];
							var row_data = [];

							row_data['jo-line-no'] = jo_line_no;
							row_data['line-no'] = raw['BOMD_LineNo'];
							row_data['item-no'] = raw['BOMD_ItemNo'];
							row_data['barcode'] = raw['BOMD_Barcode'];
							row_data['item-description'] = raw['BOMD_ItemDescription'];
							row_data['quantity'] = raw['BOMD_Qty'];
							row_data['uom-id'] = raw['BOMD_UOM'];
							row_data['uom'] = getString(raw['AD_Desc']);
							row_data['unit-cost'] = raw['BOMD_UnitCost'];
							row_data['total-cost'] = raw['BOMD_TotalCost'];
							row_data['waste'] = raw['BOMD_Waste'];
							row_data['comment'] = raw['BOMD_Comment'];
							row_data['action'] = 'add';
							row_data['color'] = 'success';
							rowdetail += jo_detail.row_sub_detail_format(row_data, row['bom-doc-no'], true);						
						}
						$('#jo-sub-detail-list tbody').append(rowdetail);
						jo_detail.reload_row_sub_events();		
					}
				});
			}


			

			jo_detail.reload_row_events();
		},
		add_sub_detail	: function(){

			var row = {
						'action'	: ''
					,	'jo-line-no': $('#jo-sub-detail #jo-line-no').val()
					,	'bom-doc-no': $('#jo-sub-detail #bom-doc-no').val()
					,	'line-no'	: jo_detail.get_temp_line_no_sub()
					,	'item-no'	: $('#jo-sub-detail #item-no').val()
					,	'barcode'	: $('#jo-sub-detail #barcode option:selected').text()
					,	'item-description'	: $('#jo-sub-detail #item-description option:selected').text()
					,	'quantity'	: $('#jo-sub-detail #quantity').val()
					,	'uom-id'	: $('#jo-sub-detail #uom-list').val()
					,	'uom'	: $('#jo-sub-detail #uom-list option:selected').text()
					,	'unit-cost'	: $('#jo-sub-detail #unit-cost').val()
					,	'required-quantity' : $('#jo-sub-detail #required-qty').val() 
					,	'total-cost'	: $('#jo-sub-detail #total-cost').data('total-cost')
					,	'actual'	:	$('#jo-sub-detail #actual').val()
					,	'waste'	: $('#jo-sub-detail #waste').val()
					,	'comment'	: $('#jo-sub-detail #comment').val()
			};

			row['action'] = 'add';
			row['color'] = 'success';
			//var is_original = $('.jo-sub-detail[data-line-no="' + row['line-no'] + '"][data-jo-line-no="' + row['jo-line-no'] +'"]').data('is-original');
			$('#jo-sub-detail-list').append(jo_detail.row_sub_detail_format(row, row['bom-doc-no'], false));
			jo_detail.reload_row_sub_events();
			jo_detail.update_bom_total(row['jo-line-no']);

		},
		edit_sub_detail	: function(){
			console.log("Edit Subdetail");
			var row = {
						'action'	: ''
					,	'jo-line-no': $('#jo-sub-detail #jo-line-no').val()
					,	'bom-doc-no': $('#jo-sub-detail #bom-doc-no').val()
					,	'line-no'	: $('#jo-sub-detail #line-no').val()
					,	'item-no'	: $('#jo-sub-detail #item-no').val()
					,	'barcode'	: $('#jo-sub-detail #barcode option:selected').text()
					,	'item-description'	: $('#jo-sub-detail #item-description option:selected').text()
					,	'quantity'	: $('#jo-sub-detail #quantity').val()
					,	'uom-id'	: $('#jo-sub-detail #uom-list').val()
					,	'uom'	: $('#jo-sub-detail #uom-list option:selected').text()
					,	'unit-cost'	: $('#jo-sub-detail #unit-cost').val()
					,	'required-quantity' : $('#jo-sub-detail #required-qty').val() 
					,	'total-cost'	: $('#jo-sub-detail #total-cost').data('total-cost')
					,	'actual'	:	$('#jo-sub-detail #actual').val()
					,	'waste'	: $('#jo-sub-detail #waste').val()
					,	'comment'	: $('#jo-sub-detail #comment').val()

			};
			row['action'] = ($('#jo-sub-detail #add-close').val() != 'edit')? 'add': 'edit';
			row['color'] = ($('#jo-sub-detail #add-close').val() != 'edit')? 'success': 'info';
			var is_original = $('.jo-sub-detail[data-line-no="' + row['line-no'] + '"][data-jo-line-no="' + row['jo-line-no'] +'"]').data('is-original');
			$('.jo-sub-detail[data-line-no="' + row['line-no'] + '"][data-jo-line-no="' + row['jo-line-no'] +'"]').replaceWith(jo_detail.row_sub_detail_format(row, row['bom-doc-no'], is_original));
			jo_detail.reload_row_sub_events();
			jo_detail.update_bom_total(row['jo-line-no']);
		},
		set_jo_form_data	: function(row){
			$('#jo-detail #line-no').val(row.data('line-no'));
			$('#jo-detail #bom-description').val(row.find('[data-bom-doc-no]').data('bom-doc-no'));
			$('#jo-detail .bom:first').trigger('change'); //Use only the event once #SuckIt
			$('#jo-detail #bom-remarks').val(row.find('[data-remarks]').data('remarks'));
			$('#jo-detail #bom-cost').val(number_format(row.find('[data-unit-cost]').data('unit-cost'), 2, ".", ""));
			$('#jo-detail #bom-yield').val(number_format(row.find('[data-yield]').data('yield'), 2, ".", ""));
			$('#jo-detail #bom-yield-uom').val(row.find('[data-yield-uom-id]').data('yield-uom-id'));
			$('#jo-detail #bom-reqstd-qty').val(number_format(row.find('[data-reqstd]').data('reqstd'), 4, ".", ""));
			$('#jo-detail #bom-batch-size').val(number_format(row.find('[data-batch-size]').data('batch-size'), 2, ".", ""));
			$('#jo-detail #bom-batch-size-uom').val(row.find('[data-batch-size-uom-id]').data('batch-size-uom-id'));
		},
		set_jo_detail_form_data : function(row){
			var is_original = row.data('is-original');
			if(is_original){
				$('#jo-sub-detail .item').prop('disabled', true);
				$('#jo-sub-detail #unit-cost').prop('disabled', true);
				//Qty, UOM, Waste, Actual, Comment
			}
			else{
				$('#jo-sub-detail .item').prop('disabled', false);
				$('#jo-sub-detail #unit-cost').prop('disabled', false);
			}

			var bom_doc_no = row.data('bom-doc-no');
			var line_no = row.data('line-no');
			var jo_line_no = row.data('jo-line-no');
			$('#jo-sub-detail #item-no').val(row.find('[data-item-no]').data('item-no'));
			$('#jo-sub-detail #item-no').trigger("change");

			$('#jo-sub-detail #bom-doc-no').val(bom_doc_no);
			$('#jo-sub-detail #line-no').val(line_no);
			$('#jo-sub-detail #jo-line-no').val(jo_line_no);
			$('#jo-sub-detail #quantity').val(number_format(row.find('[data-quantity]').data('quantity'), 4, '.', ''));
			$('#jo-sub-detail #actual').val(number_format(row.find('[data-actual]').data('actual'), 0, '.', ''));
			$('#jo-sub-detail #uom-list').val(row.find('[data-uom-id]').data('uom-id'));
			$('#jo-sub-detail #unit-cost').val(number_format(row.find('[data-unit-cost]').data('unit-cost'), 2, '.', ''));
			$('#jo-sub-detail #required-qty').val(number_format(row.find('[data-required-quantity]').data('required-quantity'), 4, ".", ""));
			$('#jo-sub-detail #waste').val(number_format(row.find('[data-waste]').data('waste'), 4, '.', ''));
			$('#jo-sub-detail #comment').val(row.find('[data-comment]').data('comment'));
			$('#jo-sub-detail #quantity').trigger("change");
		},
		clear_form	: function(){
			$('#jo-detail .bom').val('').trigger('chosen:updated');
			$('#bom-cost').val('');
			$('#bom-remarks').val('');
		},
		clear_sub_form	: function(){
			$('#jo-sub-detail .item').prop('disabled', false);
			$('#jo-sub-detail #unit-cost').prop('disabled', false);
			$('#jo-sub-detail .item').val('').trigger('chosen:updated');
			$('#jo-sub-detail #quantity').val('1');
			$('#jo-sub-detail #actual').val('1');
			$('#jo-sub-detail #uom-list').val('').trigger('chosen:updated');
			
		},
		get_temp_line_no	: function(){
			var start = 0;
			while($("#jo-detail-list tbody").find('[data-line-no="' + ++start + '"]').length > 0);
			return start;
		},
		get_temp_line_no_sub	: function(){
			var start = 0;
			while($("#jo-sub-detail-list tbody").find('[data-line-no="' + ++start + '"]').length > 0);
			return start;
		}
	}	
	init();
});