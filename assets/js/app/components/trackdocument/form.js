// Global Variable DocNo , SenderID , DefaultLoc

InitializeTrackDocumentFormEvent();

function InitializeTrackDocumentFormEvent(){
	
	// $('#track-document').on('click',function(e){
	// 	e.preventDefault();
	// 	$('#track-document-modal').modal('show');

 //        table_doctracking = $('#track-document-list').DataTable();
 //        table_doctracking.destroy();

 //        // Datatable Setup
 //        table_doctracking = $('#track-document-list').DataTable( {
 //            "processing":true,  
 //            "serverSide":true,
 //            "searching": false,  
 //            "order":[],  
 //            "ajax":{  
 //                url: global.site_name + 'TrackDocument/data?id='+doc_no,  
 //                type:"POST"
 //            },  
 //            "deferRender": true,
 //             "columnDefs": [{
 //                  "targets":[0, 1],  
 //                  "orderable": false,
 //            },{
 //                  "targets": [1,4,5,6],
 //                  "className": 'text-center'
 //            }],
 //            pageLength: 10,
 //            responsive: true,
 //            "dom": 'lT<"dt-toolbar">fgtip'
 //        });
	// });

	// $('#cancelled').on('click',function(e){
	// 	e.preventDefault();
	// 	swal({
 //                title: "Are you sure?",
 //                text: "You will cancel this document",
 //                type: "warning",
 //                showCancelButton: true,
 //                confirmButtonColor: "#EB4757",
 //                confirmButtonText: "Ok",
 //                closeOnConfirm: true
 //            }, function () {
 //            	$.ajax({
 //            		url: global.site_name + 'TrackDocument/process',
 //            		type: 'post',
 //            		dataType: 'json',
 //            		data: {		
 //            					action 					: "cancel"
 //            				,	DocumentNo 				: doc_no
 //            				,	DocumentDate 			: moment().format('M/D/Y')
 //            				, 	DocumentAmount			: 0
 //            				,   numberSeriesID 			: number_series
 //            				, 	SenderID 				: cUserid 
 //            				, 	SenderLocation 			: cDefaultLoc
 //                            ,   ns_location             : ns_location
 //            				, 	header_table 			: header_table
 //            				, 	header_doc_no_field 	: header_doc_no_field
 //            				, 	header_status_field 	: header_status_field
 //                            ,   module_url              : module_url
 //            			},
 //            		success : function(data){
 //            			if(data.success == '1'){
 //            				swal("Document succesfully cancelled!", "", "success")
	// 						setTimeout(function(){
	// 							window.location.href = module_url;
	// 						},1500);
 //            			}
 //            			else{
 //            				swal("Server Error!","please contact your administrator.","error");
 //            			}
 //            		}
 //            	});
            	
 //            });
	// });

	// $('#send-approval').on('click',function(e){
	// 	e.preventDefault();
	// 	swal({
 //                title: "Are you sure?",
 //                text: "You will send this document for approval",
 //                type: "warning",
 //                showCancelButton: true,
 //                confirmButtonColor: "#15A589",
 //                confirmButtonText: "Send",
 //                closeOnConfirm: true
 //            }, function () {
 //            	$.ajax({
 //                    url: global.site_name + 'TrackDocument/process',
 //                    type: 'post',
 //                    dataType: 'json',
 //                    data: {     
 //                                action                  : "sendrequest"
 //                            ,   DocumentNo              : doc_no
 //                            ,   DocumentDate            : moment().format('M/D/Y')
 //                            ,   DocumentAmount          : 0
 //                            ,   numberSeriesID          : number_series
 //                            ,   ns_location             : ns_location
 //                            ,   SenderID                : cUserid 
 //                            ,   SenderLocation          : cDefaultLoc
 //                            ,   header_table            : header_table
 //                            ,   header_doc_no_field     : header_doc_no_field
 //                            ,   header_status_field     : header_status_field
 //                            ,   module_url              : module_url
 //                        },
 //                    success : function(data){
 //                        if(data.success == '1'){
 //                            swal("Document successfuly sent for approval!", "", "success")
 //                            setTimeout(function(){
 //                                window.location.href = module_url;
 //                            },1500);
 //                        }
 //                        else{
 //                            swal("Server Error!","please contact your administrator.","error");
 //                        }
 //                    }
 //                });
 //            });
	// });

 //    $('#approve').on('click',function(e){
 //        e.preventDefault();
 //        swal({
 //                title: "Are you sure?",
 //                text: "You will approve this document",
 //                type: "warning",
 //                showCancelButton: true,
 //                confirmButtonColor: "#18a689",
 //                confirmButtonText: "Approve",
 //                closeOnConfirm: false
 //            }, function () {
 //                $.ajax({
 //                    url: global.site_name + 'TrackDocument/process',
 //                    type: 'post',
 //                    dataType: 'json',
 //                    data: {     
 //                                action                  : "approve"
 //                            ,   DocumentNo              : doc_no
 //                            ,   DocumentDate            : moment().format('M/D/Y')
 //                            ,   DocumentAmount          : 0
 //                            ,   numberSeriesID          : number_series
 //                            ,   ns_location             : ns_location
 //                            ,   SenderID                : cUserid 
 //                            ,   SenderLocation          : cDefaultLoc
 //                            ,   header_table            : header_table
 //                            ,   header_doc_no_field     : header_doc_no_field
 //                            ,   header_status_field     : header_status_field
 //                            ,   module_url              : module_url
 //                        },
 //                    success : function(data){
 //                        if(data.success == '1'){
 //                            swal("Document successfully approved!", "", "success")
 //                            setTimeout(function(){
 //                                window.location.href = module_url;
 //                            },1500);
 //                        }
 //                        else{
 //                            swal("Server Error!","please contact your administrator.","error");
 //                        }
 //                    }
 //                });
 //            });
 //    });

 //    $('#reject').on('click',function(e){
 //        e.preventDefault();
 //        swal({
 //                title: "Are you sure?",
 //                text: "You will reject this document",
	// 			type: "warning",
 //                text: '<label class="control-label">Remarks</label><input type="text" class="form-control" id="remarks" placeholder="Enter Remarks">',
 //                html: true,
 //                showCancelButton: true,
 //                confirmButtonColor: "#ec4758",
 //                confirmButtonText: "Reject",
 //                closeOnConfirm: false
 //            }, function (inputValue) {
 //                if(inputValue != ""){

 //                    if($('#remarks').val() == ''){
 //                        swal("Remarks Required!","","error");
 //                        return;
 //                    }

 //                    $.ajax({
 //                        url: global.site_name + 'TrackDocument/process',
 //                        type: 'post',
 //                        dataType: 'json',
 //                        data: {     
 //                                    action                  : "reject"
 //                                ,   DocumentNo              : doc_no
 //                                ,   DocumentDate            : moment().format('M/D/Y')
 //                                ,   DocumentAmount          : 0
 //                                ,   numberSeriesID          : number_series
 //                                ,   ns_location             : ns_location
 //                                ,   SenderID                : cUserid 
 //                                ,   SenderLocation          : cDefaultLoc
 //                                ,   header_table            : header_table
 //                                ,   header_doc_no_field     : header_doc_no_field
 //                                ,   header_status_field     : header_status_field
 //                                ,   module_url              : module_url
 //                                ,   remarks                 : $('#remarks').val()
 //                            },
 //                        success : function(data){
 //                            if(data.success == '1'){
 //                                swal("Document successfully rejected!", "", "success")
 //                                setTimeout(function(){
 //                                    window.location.href = module_url;
 //                                },1500);
 //                            }
 //                            else{
 //                                swal("Server Error!","please contact your administrator.","error");
 //                            }
 //                        }
 //                    });
 //                }
 //            });
 //    });

 //    $('#reopen').on('click',function(e){
 //        e.preventDefault();
 //        swal({
 //                title: "Reopen this document?",
 //                // text: "This will Re Open this document",
 //                type: "warning",
 //                text: '<label class="control-label">Remarks</label><input type="text" class="form-control" id="remarks" placeholder="Enter Remarks">',
 //                html: true,
 //                showCancelButton: true,
 //                confirmButtonColor: "#18a689",
 //                confirmButtonText: "Reopen",
 //                closeOnConfirm: false
 //            }, function (inputValue) {
 //                if(inputValue != ""){

 //                    if($('#remarks').val() == ''){
 //                        swal("Remarks required!","","error");
 //                        return;
 //                    }

 //                    $.ajax({
 //                        url: global.site_name + 'TrackDocument/process',
 //                        type: 'post',
 //                        dataType: 'json',
 //                        data: {     
 //                                    action                  : "reopen"
 //                                ,   DocumentNo              : doc_no
 //                                ,   DocumentDate            : moment().format('M/D/Y')
 //                                ,   DocumentAmount          : 0
 //                                ,   numberSeriesID          : number_series
 //                                ,   ns_location             : ns_location
 //                                ,   SenderID                : cUserid 
 //                                ,   SenderLocation          : cDefaultLoc
 //                                ,   header_table            : header_table
 //                                ,   header_doc_no_field     : header_doc_no_field
 //                                ,   header_status_field     : header_status_field
 //                                ,   module_url              : module_url
 //                                ,   remarks                 : $('#remarks').val()
 //                            },
 //                        success : function(data){
 //                            if(data.success == '1'){
 //                                swal("Document Successfully Reopened!", "", "success")
 //                                setTimeout(function(){
 //                                    window.location.href = module_url;
 //                                },1500);
 //                            }
 //                            else{
 //                                swal("Server Error!","please contact your administrator.","error");
 //                            }
 //                        }
 //                    });
 //                }
 //            });
 //    });

     $('#cancelled').on('click',function(e){
        e.preventDefault();
        swal({
                title: "Are you sure?",
                text: "You will cancel this document",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EB4757",
                confirmButtonText: "Ok",
                closeOnConfirm: false
            }, function (isConfirm) {

                if(isConfirm){
                    swal({
                        title               : "Please wait!",
                        text                : "Processing...",
                        type                : "info",
                        showConfirmButton   : false
                    });

                    $.ajax({
                        url: global.site_name + 'TrackDocument/process',
                        type: 'post',
                        dataType: 'json',
                        data: {     
                                    action                  : "cancel"
                                ,   DocumentNo              : doc_no
                                ,   DocumentType            : module_name
                                ,   DocumentDate            : moment().format('YYYY-MM-DD')
                                ,   DocumentAmount          : 0
                                ,   numberSeriesID          : number_series
                                ,   SenderID                : cUserid 
                                ,   SenderLocation          : cDefaultLoc
                                ,   ns_location             : ns_location
                                ,   header_table            : header_table
                                ,   header_doc_no_field     : header_doc_no_field
                                ,   header_status_field     : header_status_field
                                ,   module_url              : module_url
                            },
                        success : function(data){
                            if(data.success == '1'){
                                swal("Document succesfully cancelled!", "", "success")
                                setTimeout(function(){
                                    window.location.href = module_url;
                                },1500);
                            }
                            else{
                                swal(data.error_msg,"please contact your administrator.","error");
                            }
                        },
                        error : function(jqXHR){
                            swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
                        }
                    });
                }
                
            });
    });

    $('#send-approval').on('click',function(e){
        e.preventDefault();
        swal({
                title: "Are you sure?",
                text: "Send document for approval?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#15A589",
                confirmButtonText: "Send",
                closeOnConfirm: false
            }, function (isConfirm) {

                if(isConfirm){
                    swal({
                        title               : "Please wait!",
                        text                : "Processing...",
                        type                : "info",
                        showConfirmButton   : false
                    });

                    $.ajax({
                        url: global.site_name + 'TrackDocument/process',
                        type: 'post',
                        dataType: 'json',
                        data: {     
                                    action                  : "sendrequest"
                                ,   DocumentNo              : doc_no
                                ,   DocumentType            : module_name
                                ,   DocumentDate            : moment().format('YYYY-MM-DD')
                                ,   DocumentAmount          : 0
                                ,   numberSeriesID          : number_series
                                ,   ns_location             : ns_location
                                ,   SenderID                : cUserid 
                                ,   SenderLocation          : cDefaultLoc
                                ,   header_table            : header_table
                                ,   header_doc_no_field     : header_doc_no_field
                                ,   header_status_field     : header_status_field
                                ,   module_url              : module_url
                                // ,   aUT_tbl                : aUT_tbl
                                // ,   aUT_detail             : aUT_detail
                            },
                        success : function(data){
                            if(data.success == '1'){
                                swal("Document successfuly sent for approval!", "", "success")
                                setTimeout(function(){
                                    window.location.href = module_url;
                                },1500);
                            }
                            else{
                                swal(data.error_msg,"please contact your administrator.","error");
                            }
                        },
                        error : function(jqXHR){
                            swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
                        }
                    });
                }

            });
    });

    $('#track-document').on('click',function(e){
        e.preventDefault();
        $('#track-document-modal').modal('show');

        table_doctracking = $('#track-document-list').DataTable();
        table_doctracking.destroy();

        // Datatable Setup
        table_doctracking = $('#track-document-list').DataTable( {
            "processing":true,  
            "serverSide":true,
            "searching": false,  
            "order":[],  
            "ajax":{  
                url: global.site_name + 'TrackDocument/data?id='+doc_no,  
                type:"POST"
            },  
            "deferRender": true,
             "columnDefs": [{
                  "targets":[0, 1],  
                  "orderable": false,
            },{
                  "targets": [1,4,5,6],
                  "className": 'text-center'
            }],
            pageLength: 10,
            responsive: true,
            "dom": 'lT<"dt-toolbar">fgtip'
        });
    });

    $('#approve').on('click',function(e){
        e.preventDefault();
        swal({
                title: "Are you sure?",
                text: "You will approve this document",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#18a689",
                confirmButtonText: "Approve",
                closeOnConfirm: false
            }, function (isConfirm) {

                if(isConfirm){

                    swal({
                        title               : "Please wait!",
                        text                : "Processing...",
                        type                : "info",
                        showConfirmButton   : false
                    });

                    if(module_name == 'Leave'){
                        let no_lvtype_count = 0;
                        $('#leave-details tbody tr').each(function(index, el) {
                            if($(this).find('.checkbox').is(':checked')){
                                if($(this).find('[name="LVD_FK_LeaveId"]').val() == '' || $(this).find('[name="LVD_FK_LeaveId"]').val() === null){
                                    no_lvtype_count++;
                                }
                            }
                        });

                        if(no_lvtype_count > 0){
                            swal("Error", "No Leave Type set on dates \"with pay\"", "error");
                            return false;
                        }
                    }

                    $.ajax({
                        url: global.site_name + 'TrackDocument/process',
                        type: 'post',
                        dataType: 'json',
                        data: {     
                                    action                  : "approve"
                                ,   DocumentNo              : doc_no
                                ,   DocumentType            : module_name
                                ,   DocumentDate            : moment().format('YYYY-MM-DD')
                                ,   DocumentAmount          : 0
                                ,   numberSeriesID          : number_series
                                ,   ns_location             : ns_location
                                ,   SenderID                : cUserid 
                                ,   SenderLocation          : cDefaultLoc
                                ,   header_table            : header_table
                                ,   header_doc_no_field     : header_doc_no_field
                                ,   header_status_field     : header_status_field
                                ,   module_url              : module_url
                                // ,   aUT_tbl                : aUT_tbl
                                // ,   aUT_detail             : aUT_detail
                                ,   mod_details             : mod_details
                            },
                        success : function(data){
                            if(data.success == '1'){
                                swal("Document successfully approved!", "", "success")
                                setTimeout(function(){
                                    if(doc_apv != ''){
                                        window.location.href = global.site_name + 'modules/doc_tracking'
                                    }
                                    else{
                                        window.location.href = module_url;
                                    }
                                },1500);
                            }
                            else{
                                swal(data.error_msg,"please contact your administrator.","error");
                            }
                        },
                        error : function(jqXHR){
                            swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
                        }
                    });
                }

            });
    });

    $('#reject').on('click',function(e){
        e.preventDefault();
        swal({
                title: "Are you sure?",
                text: "You will reject this document",
                type: "warning",
                text: '<label class="control-label">Remarks</label><input type="text" class="form-control" id="remarks" placeholder="Enter Remarks">',
                html: true,
                showCancelButton: true,
                confirmButtonColor: "#ec4758",
                confirmButtonText: "Reject",
                closeOnConfirm: false
            }, function (inputValue) {
                if(inputValue != ""){

                    if($('#remarks').val() == ''){
                        swal("Remarks Required!","","error");
                    }
                    else{

                        let txtRemarks = $('#remarks').val();

                        swal({
                            title               : "Please wait!",
                            text                : "Processing...",
                            type                : "info",
                            showConfirmButton   : false
                        });

                        $.ajax({
                            url: global.site_name + 'TrackDocument/process',
                            type: 'post',
                            dataType: 'json',
                            data: {     
                                        action                  : "reject"
                                    ,   DocumentNo              : doc_no
                                    ,   DocumentType            : module_name
                                    ,   DocumentDate            : moment().format('YYYY-MM-DD')
                                    ,   DocumentAmount          : 0
                                    ,   numberSeriesID          : number_series
                                    ,   ns_location             : ns_location
                                    ,   SenderID                : cUserid 
                                    ,   SenderLocation          : cDefaultLoc
                                    ,   header_table            : header_table
                                    ,   header_doc_no_field     : header_doc_no_field
                                    ,   header_status_field     : header_status_field
                                    ,   header_remarks_field    : header_remarks_field
                                    ,   module_url              : module_url
                                    ,   remarks                 : txtRemarks
                                },
                            success : function(data){
                                if(data.success == '1'){
                                    swal("Document successfully rejected!", "", "success")
                                    setTimeout(function(){
                                        if(doc_apv != ''){
                                            window.location.href = global.site_name + 'modules/doc_tracking'
                                        }
                                        else{
                                            window.location.href = module_url;
                                        }
                                    },1500);
                                }
                                else{
                                    swal(data.error_msg,"please contact your administrator.","error");
                                }
                            },
                            error : function(jqXHR){
                                swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
                            }

                        });
                    }

                }
            });
    });

    $('#revise').on('click',function(e){
        e.preventDefault();
        swal({
                title: "Revise this document?",
                // text: "This will Re Open this document",
                type: "warning",
                text: '<label class="control-label">Remarks</label><input type="text" class="form-control" id="remarks" placeholder="Enter Remarks">',
                html: true,
                showCancelButton: true,
                confirmButtonColor: "#18a689",
                confirmButtonText: "Revise",
                closeOnConfirm: false
            }, function (inputValue) {
                if(inputValue != ""){

                    if($('#remarks').val() == ''){
                        swal("Remarks required!","","error");
                    }
                    else{

                        let txtRemarks = $('#remarks').val();

                        swal({
                            title               : "Please wait!",
                            text                : "Processing...",
                            type                : "info",
                            showConfirmButton   : false
                        });

                        $.ajax({
                            url: global.site_name + 'TrackDocument/process',
                            type: 'post',
                            dataType: 'json',
                            data: {     
                                        action                  : "revise"
                                    ,   DocumentNo              : doc_no
                                    ,   DocumentType            : module_name
                                    ,   DocumentDate            : moment().format('YYYY-MM-DD')
                                    ,   DocumentAmount          : 0
                                    ,   numberSeriesID          : number_series
                                    ,   ns_location             : ns_location
                                    ,   SenderID                : cUserid 
                                    ,   SenderLocation          : cDefaultLoc
                                    ,   header_table            : header_table
                                    ,   header_doc_no_field     : header_doc_no_field
                                    ,   header_status_field     : header_status_field
                                    ,   header_remarks_field    : header_remarks_field
                                    ,   module_url              : module_url
                                    ,   remarks                 : txtRemarks
                                },
                            success : function(data){
                                if(data.success == '1'){
                                    swal("Document Successfully opened for revision!", "", "success")
                                    setTimeout(function(){
                                        if(doc_apv != ''){
                                            window.location.href = global.site_name + 'modules/doc_tracking'
                                        }
                                        else{
                                            window.location.href = module_url;
                                        }
                                    },1500);
                                }
                                else{
                                    swal(data.error_msg,"please contact your administrator.","error");
                                }
                            },
                            error : function(jqXHR){
                                swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
                            }
                        });
                    }

                }
            });
    });

    $('#reopen').on('click',function(e){
        e.preventDefault();
        swal({
                title: "Re-open this document?",
                // text: "This will Re Open this document",
                type: "warning",
                text: '<label class="control-label">Remarks</label><input type="text" class="form-control" id="remarks" placeholder="Enter Remarks">',
                html: true,
                showCancelButton: true,
                confirmButtonColor: "#18a689",
                confirmButtonText: "Re-open",
                closeOnConfirm: false
            }, function (inputValue) {
                if(inputValue != ""){

                    if($('#remarks').val() == ''){
                        swal("Remarks required!","","error");
                    }
                    else{

                        let txtRemarks = $('#remarks').val();

                        swal({
                            title               : "Please wait!",
                            text                : "Processing...",
                            type                : "info",
                            showConfirmButton   : false
                        });

                        $.ajax({
                            url: global.site_name + 'TrackDocument/process',
                            type: 'post',
                            dataType: 'json',
                            data: {     
                                        action                  : "reopen"
                                    ,   DocumentNo              : doc_no
                                    ,   DocumentType            : module_name
                                    ,   DocumentDate            : moment().format('YYYY-MM-DD')
                                    ,   DocumentAmount          : 0
                                    ,   numberSeriesID          : number_series
                                    ,   ns_location             : ns_location
                                    ,   SenderID                : cUserid 
                                    ,   SenderLocation          : cDefaultLoc
                                    ,   header_table            : header_table
                                    ,   header_doc_no_field     : header_doc_no_field
                                    ,   header_status_field     : header_status_field
                                    ,   header_remarks_field    : header_remarks_field
                                    ,   module_url              : module_url
                                    ,   remarks                 : txtRemarks
                                },
                            success : function(data){
                                if(data.success == '1'){
                                    swal("Document Successfully Re-opened!", "", "success")
                                    setTimeout(function(){
                                        window.location.href = module_url;
                                    },1500);
                                }
                                else{
                                    swal(data.error_msg,"please contact your administrator.","error");
                                }
                            },
                            error : function(jqXHR){
                                swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
                            }
                        });
                    }

                }
            });
    });

    $('#post').on('click',function(e){
        e.preventDefault();
        swal({
                title: "Post this Document?",
                // text: "This will Re Open this document",
                type: "warning",
                html: true,
                showCancelButton: true,
                confirmButtonColor: "#18a689",
                confirmButtonText: "Ok",
                closeOnConfirm: false
            }, function (confirmed) {
            
                    if(confirmed){
                        swal({
                            title               : "Please wait!",
                            text                : "Processing...",
                            type                : "info",
                            showConfirmButton   : false
                        });

                        $.ajax({
                            url: global.site_name +'/'+  module_folder +'/'+ module_controller + '/on_post' ,
                            type: 'post',
                            dataType: 'json',
                            data: {     
                                        action                  : "post"
                                    ,   DocumentNo              : doc_no
                                    ,   DocumentType            : module_name
                                    ,   DocumentDate            : moment().format('YYYY-MM-DD')
                                    ,   DocumentAmount          : 0
                                    ,   numberSeriesID          : number_series
                                    ,   ns_location             : ns_location
                                    ,   SenderID                : cUserid 
                                    ,   SenderLocation          : cDefaultLoc
                                    ,   header_table            : header_table
                                    // ,   header_doc_no_field     : header_doc_no_field
                                    // ,   header_status_field     : header_status_field
                                    // ,   header_remarks_field    : header_remarks_field
                                    ,   module_url              : module_url
                                    // ,   remarks                 : txtRemarks
                                },
                            success : function(data){
                                if(data.success){
                                    swal("Document Successfully Posted!", "", "success")
                                    setTimeout(function(){
                                        window.location.href = module_url;
                                    },1500);
                                }
                                else{
                                    swal(data.title, data.message, data.type);
                                }
                            },
                            error : function(jqXHR){
                                swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
                            }
                        });
                    }
            });
    });

}
