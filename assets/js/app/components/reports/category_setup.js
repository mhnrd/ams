$(document).ready(function(){
	function init(){
		$.fn.modal.Constructor.prototype.enforceFocus = function() {};
		var toolbar = '<div class="pull-right"><button id="add-category" class="btn btn-default" type="button"><i class="fa fa-file">&nbsp</i> ADD</a></a></div>';
		$('.dataTables').DataTable( {
			  "columnDefs": [{
			  "targets": 'no-sort',
			  "orderable": false,
			}],
			"order": [[ 1, "asc" ]],
			pageLength: 10,
	        responsive: true,
			"dom": 'lT<"dt-toolbar">fgtip',
			"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
				category_setup.table_events();
			    return iStart +" to "+ iEnd;
			}
		});

		$("div.dt-toolbar").html(toolbar);

		$('#add-category').click(function(){
			category_setup.show_modal('add');
			category_setup.clear_form();
		});

		

		$('#type').select2();
		$(".bom-item").select2();

		$("#bom-item-description").select2(category_setup.item_search('IM_Sales_Desc', "Enter Item Description Here"));
		$("#bom-item-no").select2(category_setup.item_search('IM_Item_Id', "Enter Item No Here"));
		$("#bom-barcode").select2(category_setup.item_search('IM_UPCCode', "Enter Item Barcode Here"));

		$('#save').click(function(){
			category_setup.save($(this).val())
		});

		$('.bom-item').on("select2:select", function(e) { 
			var type = $(this).data('type');
			var data_att = $(this).select2('data')[0];

			if(type=="item-description"){
				$("#bom-item-no").html('<option value="' + data_att['id']+ '" selected ="selected">' + data_att['data-id'] + ' </option>');
				$("#bom-item-no").select2("val", data_att['id']);
				$("#bom-barcode").html('<option value="' + data_att['id']+ '" selected ="selected">' + data_att['data-barcode'] + ' </option>');
				$("#bom-barcode").select2("val", data_att['id']);
			}
			else if(type=="item-no"){
				$("#bom-item-description").html('<option value="' + data_att['id']+ '" selected ="selected">' + data_att['data-text'] + ' </option>');
				$("#bom-item-description").select2("val", data_att['id']);
				$("#bom-barcode").html('<option value="' + data_att['id']+ '" selected ="selected">' + data_att['data-barcode'] + ' </option>');
				$("#bom-barcode").select2("val", data_att['id']);
			}
			else if(type=="barcode"){
				$("#bom-item-no").html('<option value="' + data_att['id']+ '" selected ="selected">' + data_att['data-id'] + ' </option>');
				$("#bom-item-no").select2("val", data_att['id']);
				$("#bom-item-description").html('<option value="' + data_att['id']+ '" selected ="selected">' + data_att['data-text'] + ' </option>');
				$("#bom-item-description").select2("val", data_att['id']);
			}
			$('#bom-description').val(data_att['data-text'] );
		});	

		category_setup.table_events();
	}

	var category_setup = {
		table_events : function(){
			$('.edit-item').click(function(){
				category_setup.show_modal('edit');
				$("#category-setup #type").select2("val", $(this).data('type'));

				var item_id = $(this).val();
				$.ajax({
					type: 'GET',
					dataType: 'json',
					url: global.site_name + 'bom/getItem',
					data : {
								'filter-type'	: 'IM_Item_Id'
							,	'filter-search'	: item_id
							,	'action'		: 'Select'
					},
					success : function(result){
						if(result){
							$('#bom-item-no').html('').select2({
								data :[{ 
										id: result.IM_Item_Id,
						                text:result.IM_UPCCode,
						                'data-price':result.IM_UnitCost,
						                'data-barcode':result.IM_UPCCode,
						                'data-text':result.IM_Sales_Desc,
						                'data-id':result.IM_Item_Id}]
							});
							$('#bom-item-no').select2('val', result.IM_Item_Id, true);
							$('#bom-item-no').trigger("select2:select");
							$("#add-close").val("add");
							$("#add-close").trigger("click");

							//Clear Form
							//bom_detail.clear_form();
							$("#bom-item-no").select2(category_setup.item_search('IM_Item_Id', "Enter Item No Here"));
						}
						

					}
				});
			});

			$('.delete-item').click(function(){
				var itemno = $(this).val();
				swal({
	                title: "Are you sure?",
	                text: "You want to delete this record?",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Yes, save it!",
	                closeOnConfirm: false
	            }, function () {
					category_setup.delete(itemno)
	            });
			});
		},
		show_modal : function(method){
			if(method=='add'){
				$("#category-setup").modal('show');
				$("#category-setup #save").val(method);
				// $("#bom-detail #add-close").text('ADD & CLOSE');
				// $("#bom-detail #add-new").val(method)
				// $("#bom-detail #add-close").val(method)
			}
			else if(method=='edit'){
				$("#category-setup").modal('show');
				$("#category-setup #save").val(method);
				// $("#bom-detail #add-close").text('SAVE');
				// $("#bom-detail #add-new").val(method)
				// $("#bom-detail #add-close").val(method)
			}
		},
		save : function(method){
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: global.site_name + 'report/save-category-setup',
				data : {
					'item-no' : $("#category-setup #bom-item-no").val()
				,	'type'	: $("#category-setup #type").val()
				,	'action'	: method
				},
				success : function(result){	
					if(result.success){
						location.reload();
					}
					else{
						if(result.message == 'already-exist'){
							swal("Failed to add Category Setup!", "The selected Item already exist!", "error");
						}
					}
				}
			});
		},
		delete : function(itemno){
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: global.site_name + 'report/save-category-setup',
				data : {
					'item-no' : itemno
				,	'action'	: 'delete'
				},
				success : function(result){	
					if(result.success){
						location.reload();
					}	
				}
			});
		},
		clear_form : function(){
			$('#item-no').select2("val", "");
			$('#barcode').select2("val", "");
			$('#item-description').select2("val", "");

			$("#category-setup #type").select2("val", "A");
		},
		item_search: function(filter, placeholder='Enter Item Here'){
			return {
				placeholder: placeholder,
				minimumInputLength: 3,
				ajax: {
					url: global.site_name + 'bom/getItem',
					dataType: 'json',
					delay: 250,
					data: function (params) {
					  var queryParameters = {
								'filter-type'	: filter
							,	'filter-search'	: params.term
							,	'action'		: 'search'
					    }
					    return queryParameters;
					},
					processResults: function (data, params) {
						return {
							results: $.map(data, function(item) {
					            return {
					                id: item.IM_Item_Id,
					                text:item[filter],
					                'data-price':item.IM_UnitCost,
					                'data-barcode':item.IM_UPCCode,
					                'data-text':item.IM_Sales_Desc,
					                'data-id':item.IM_Item_Id
					            }
					        })
						};
					}
				}
			}
		}
	};



	init();
});