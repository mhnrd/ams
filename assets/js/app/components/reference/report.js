var print = function(){};

$(document).ready(function(){
	var print_element = undefined;
	var before_print_event = undefined;
	var after_print_event = undefined;
	var create_report = {
		state : function(current_state){
			if(current_state == 'loading'){
				print_element.empty();
				print_element.append('<i class="fa fa-refresh fa-spin"></i>');
				print_element.prop('disabled', true);
				//this_element.attr('disabled', true);
			}
			if(current_state == 'click'){
				print_element.prop('disabled', false);
				print_element.empty();
				print_element.append('<i class="fa fa-print"></i>');
			}
		}
	};

	print = function(){
		create_report.state('loading');
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: print_element.data('url'),
			data : print_element.data('param'),
			success : function(result){
				var win = window.open(global.site_name + 'pdf/'+result.pdf_name);
				if (win) {
					//Browser has allowed it to be opened
					win.focus();
				} else {
					//Browser has blocked it
					alert('Please allow popups for this website');
				} 
				create_report.state('click');
				eval(after_print_event);				
			},
			error: function (request, status, error) {
				create_report.state('click');
			}
		});
	};

	$('#print').click(function(){
		print_element = $(this);

		before_print_event = (print_element.data('before-print') != undefined)? print_element.data('before-print') : function(){ return true};
		after_print_event = (print_element.data('after-print') != undefined)? print_element.data('after-print') : function(){};
		
		//e.preventDefault();
		if(eval(before_print_event)){
			print();
		}
	});
});