$(document).ready(function(){

	function initTable(){
		// Datatable Setup
		tableindex = $('#'+table_id).DataTable( {
			"processing":true,
			"serverSide":true,
			"scrollY": "350px",
			"scrollX": true,
  			"scrollCollapse": true,
  			"paging": true,
			"order":[],
			"ajax":{
				url: global.site_name + 'asset_management/scan/data',
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[0],
				"orderable": false,
			},{
				"targets": col_center,
				"className": 'text-center'
			}],
			pageLength: 10,
	        responsive: true,
	        "order": [[ 2, "asc" ]],
			"dom": 'lT<"dt-toolbar">gtip'
		});

		$('#'+table_id+'_paginate').addClass('pull-right');

		$('[name=FACS_UsefulLifeNoUOM]').select2({
			placeholder: '',
			allowClear: true
		}).on('change', function() {
            $(this).valid();
        });

	}

	initTable();

});