$(document).ready(function(){

	id = $('[name="FA_ID"]').val();

	function init(){

		compute_exchange_rate();

		if (output_type == 'view') {
			$('#form-header input, #form-header select, #form-header textarea').prop('disabled', true);
		}

		if (output_type == 'update') {
			if (status == 'Disposed') {
				$(':input').prop('disabled', true);
				$('button, a:not(#back, .nav-link, #home)').hide();
			}
		}

		if (output_type == 'print') {
			if (status == 'Disposed') {
				$(':input').prop('disabled', true);
				$('button, a:not(#back, .nav-link, #home)').hide();
			}
		}

		if (output_type == 'add') {

		}

		$('[name="FA_AcquisitionCostCY"], [name="FA_AcquisitionNetBookCY"]').change(function(event) {
			compute_exchange_rate();

			$('#FA_AccuDepreBegCY').val($('[name="FA_AcquisitionNetBookCY"]').val()).trigger('change');
			$('#FA_AccuDepreEndCY').val($('[name="FA_AcquisitionNetBookCY"]').val()).trigger('change');
			$('#FA_DepreMonthlyCY').val($('[name="FA_AcquisitionNetBookCY"]').val()).trigger('change');
			$('#FA_DepreYTDCY').val($('[name="FA_AcquisitionNetBookCY"]').val()).trigger('change');
		})
		
		$('[name="FAC_ItemID"]').on("select2:select", function(e) {

			if($(this).select2('data')[0] === undefined && e['params']['data'].id == '') return;
			var type = $(this).data('type');
			var data_att = $(this).select2('data')[0];
			
			// Data from Row Field
			var select_param = e['params'];
			// Array for Adding / Editing
			var item_data = [];
			item_data = {
				 'data-id' 		: data_att['data-description']
			};
			
			$('[name="FAC_Description"]').val(item_data['data-id'])
		});

		$('[name="FA_ItemID"]').on("select2:select", function(e) {

			if($(this).select2('data')[0] === undefined && e['params']['data'].id == '') return;
			var type = $(this).data('type');
			var data_att = $(this).select2('data')[0];
			
			// Data from Row Field
			var select_param = e['params'];
			// Array for Adding / Editing
			var item_data = [];
			item_data = {
				 'data-id' 		: data_att['data-description']
			};
			
			$('[name="FA_Description"]').val(item_data['data-id'])
		});

		$('[name="FAC_Qty"], [name="FAC_UnitCost"]').change(function(event) {
			compute_total_cost();
		});

		$('[name="FA_AcquisitionDate"], [name="FA_WarrantyPeriodUOM"], [name="FA_WarrantyPeriodNo"]').change(function(event) {
			if ($('[name="FA_AcquisitionDate"]').val() == '' || $('[name="FA_WarrantyPeriodUOM"]').val() == '' || $('[name="FA_WarrantyPeriodNo"]').val() == '') {
				$('#FA_WarrantyDate').val('');
			}
			else{
				compute_warranty_date();
			}
		});

		$('[name="FA_UsefulLifeNo"], [name="FA_WarrantyPeriodNo"]').inputFilter(function(value) {
  			return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 999); 
  		});

		$('[name=FA_AssetClass]').change(function(event) {
			if ($(this).val() != '') {
				$.ajax({
					url: global.site_name + 'asset_management/asset_list/getAssetClassDetail',
					type: 'POST',
					dataType: 'json',
					data: {
						FA_AssetClass 	: $(this).val(),
					},
					success 	: function(result){
						$('[name="FA_UsefulLifeNo"]').val(number_format(result.FACS_UsefulLifeNo, 0, ".", ""));
						$('[name="FA_UsefulLifeNoUOM"]').val(result.FACS_UsefulLifeNoUOM).trigger('change');
					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator.', 'error');
					}
				});
			}
			else{
				$('[name="FA_UsefulLifeNo"]').val('');
				$('[name="FA_UsefulLifeNoUOM"]').val('').trigger('change');
			}
			
		});

		initFields();
		initTable();
		initButtons();
		initAddUpdate();
		save_maintenance_log();
		save_dispose();
		save_change_location();
		save_components();
		compute_values();
		compute_warranty_date();
		dropdownEvents();
	}

	function compute_warranty_date(){
		var parts = $('[name="FA_AcquisitionDate"]').val().split("/");

        var month = parts[0] && parseInt( parts[0], 10 );
		var day = parts[1] && parseInt( parts[1], 10 );
        var year = parts[2] && parseInt( parts[2], 10 );
        var duration = parseInt( $('[name="FA_WarrantyPeriodNo"]').val(), 10);

		if ($('[name="FA_WarrantyPeriodUOM"]').val() == 'Y') {

            if( day <= 31 && day >= 1 && month <= 12 && month >= 1 ) {

                var expiryDate = new Date( year, month - 1, day );
                expiryDate.setFullYear( expiryDate.getFullYear() + duration );

                day = ( '0' + expiryDate.getDate() ).slice( -2 );
                month = ( '0' + ( expiryDate.getMonth() + 1 ) ).slice( -2 );
                year = expiryDate.getFullYear();

                $("#FA_WarrantyDate").val( month + "/" + day + "/" + year );
			}
        }
        else if ($('[name="FA_WarrantyPeriodUOM"]').val() == 'M') {
        	if( day <= 31 && day >= 1 && month <= 12 && month >= 1 ) {

        		let duration_year = Math.floor((month + duration)/12)
        		let duration_month = (month + duration % 12 == 0 ? 12 : month + duration % 12);

                var expiryDate = new Date( year, duration_month - 1, day );
            	expiryDate.setFullYear( expiryDate.getFullYear() + duration_year );

                day = ( '0' + expiryDate.getDate() ).slice( -2 );
                month = ( '0' + ( expiryDate.getMonth() +  1 ) ).slice( -2 );

                if (month + duration > 12) {
                	year = expiryDate.getFullYear();
                }

				$("#FA_WarrantyDate").val( month + "/" + day + "/" + year );
			}
        }
		

	}

	function compute_exchange_rate(){
		
		if ($('[name="FA_AcquisitionCostCY"]').val() == $('[name="FA_AcquisitionNetBookCY"]').val()) {
			$('[name="FA_ExchangeRate"]').val('1.00');
			$('[name="FA_ExchangeRate"]').attr('readonly', true);

		}
		else{
			$('[name="FA_ExchangeRate"]').attr('readonly', false);
		}
		
		let exchange_rate = parseFloat($('[name="FA_ExchangeRate"]').val());
		let acquisition_cost = parseFloat($('[name="FA_AcquisitionCost"]').val());

		$('#FA_NetBookValue').val(number_format(exchange_rate * acquisition_cost, 2));
	}

	function initFields(){
		$('.js-select2').select2({
			placeholder 	: '',
			allowClear 		: true	
		});

		$('[name="FA_ItemID"], [name="FAC_ItemID"]').select2({
			placeholder 		: '',
			allowClear 			: true,
			minimumInputLength	: 3,
			ajax : {
				url: global.site_name + 'asset_management/asset_list/getItem',
				dataType: 'json',
				delay: 250,
				data: function (params) {
				  var queryParameters = {
						'filter-input'		: params.term
				  }
				  return queryParameters;
				},
				processResults: function (data, params) {
					return {
						results: $.map(data, function(item) {
				            return {
				                id: item.AD_Id,
				                text: item.Description,
				                'data-description': item.AD_Desc
				            }
				        })
					};
				}
			}
		});

		$('.datepick').datepicker({
			todayBtn: "linked",
	        keyboardNavigation: false,
	        forceParse: false,
	        calendarWeeks: true,
	        autoclose: true,
		});

		$('.dropdown').click(function(event) {
			$('.dropdown-content').toggle();
		});

		$(document).click(function (e) {
		    e.stopPropagation();
		    var container = $(".dropdown");

		    //check if the clicked area is dropDown or not
		    if (container.has(e.target).length === 0) {
		        $('.dropdown-content').hide();
		    }
		})
	}

	function dropdownEvents(){
        // dropdown event
        $('[name="FA_DepartmentID"]').change(function (e) {

            $('[name="FA_Bay"]').select2("val", "");
            $('[name="FA_Line"]').select2("val", "");
            
            if ($(this).val() == "" || $(this).val() == null) {
                $('[name="FA_Bay"]').select2('enable',false);
                $('[name="FA_Line"]').select2('enable',false);
                return false;
            }

            $.ajax({
                url: global.site_name + 'asset_management/asset_list/getGeneralInfoFilteredData',
                dataType: 'json',
                type: 'POST',
                async: false,
                data: {
                    divisionType: $(this).val()
                },
                success : function(result){
                    $('[name="FA_Bay"]').empty();
                    $('[name="FA_Bay"]').select2('enable',true);

                    let bay_No_list = '';

                    bay_No_list += '<option value=""></option>';

                    $(result).each(function(index){
                        bay_No_list += '<option value="'+result[index].SL_Id+'">'+result[index].SL_Name+'</option>'
                    });

                    $('[name="FA_Bay"]').append(bay_No_list);

                }
            });
        });

        // --End

        $('[name="FA_Bay"]').change(function (e) {

            $('[name="FA_Line"]').select2("val", "");
            
            if ($(this).val() == "" || $(this).val() == null) {
                $('[name="FA_Line"]').select2('enable',false);
                return false;
            }

            $.ajax({
                url: global.site_name + 'asset_management/asset_list/getGeneralInfoFilteredData',
                dataType: 'json',
                type: 'POST',
                async: false,
                data: {
                    bayNo: $(this).val()
                },
                success : function(result){
                    $('[name="FA_Line"]').empty();
                    $('[name="FA_Line"]').select2('enable',true);

                    let bay_No_list = '';

                    bay_No_list += '<option value=""></option>';

                    $(result).each(function(index){
                        bay_No_list += '<option value="'+result[index].SL_Id+'">'+result[index].SL_Name+'</option>'
                    });

                    $('[name="FA_Line"]').append(bay_No_list);

                }
            });
        });

        // --End

        // $('[name="FA_DepartmentID"]').on('select2:unselecting', function (e) {
        //     $('[name="Bay_No"]').select2("val", "");
        //     $('[name="Bay_No"]').select2('enable',false);
        // });
    }

	function initTable(){
		// Datatable Setup
		table_movement = $('#'+table_id_movement).DataTable( {
			"processing":true,
			"serverSide":true,
			"scrollY": "100px",
			"scrollX": true,
  			"scrollCollapse": true,
  			"paging": true,
			"order":[],
			"ajax":{
				url: global.site_name + 'asset_management/asset_list/data_movement?id='+id,
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets": [0],
				"className": 'text-center'
			}],
			pageLength: 5,
			lengthMenu: [5, 10, 25, 50, 100],
	        responsive: true,
	        "order": [[ 0, "asc" ]],
	        createdRow:function(row){
				$(row).addClass('table-movement');
				$('td', row).css('vertical-align', 'middle')
			},
			"dom": 'lT<"dt-toolbar">fgtip'
		});

		$('#'+table_id_movement+'_paginate').addClass('pull-right');

		table_maintenance = $('#'+table_id_maintenance).DataTable( {
			"processing":true,
			"serverSide":true,
			"scrollY": "100px",
			"scrollX": true,
  			"scrollCollapse": true,
  			"paging": true,
			"order":[],
			"ajax":{
				url: global.site_name + 'asset_management/asset_list/data_maintenance?id='+id+'&type='+output_type,
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets": (output_type == 'update' ? [0] : []),
				"orderable": false
			},{
				"targets": (output_type == 'update' ? [0,1] : [0]),
				"className": 'text-center'
			},{
				"targets": (output_type == 'update' ? [3] : [2]),
				"className": 'text-right'
			}],
			pageLength: 5,
			lengthMenu: [5, 10, 25, 50, 100],
	        responsive: true,
	        "order": [[ (output_type == 'update' ? 1 : 0), "asc" ]],
	        createdRow:function(row){
				$(row).addClass('table-maintenance');
				$('td', row).css('vertical-align', 'middle')
			},
			"dom": 'lT<"dt-toolbar">fgtip'
		});

		$('#'+table_id_maintenance+'_paginate').addClass('pull-right');

		table_components = $('#'+table_id_components).DataTable( {
			"processing":true,
			"serverSide":true,
			"scrollY": "100px",
			"scrollX": true,
  			"scrollCollapse": true,
  			"paging": true,
			"order":[],
			"ajax":{
				url: global.site_name + 'asset_management/asset_list/data_components?id='+id+'&type='+output_type,
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets": (output_type == 'update' ? [0] : []),
				"orderable": false
			},{
				"targets": (output_type == 'update' ? [0,1,3] : [0,2]),
				"className": 'text-center'
			},{
				"targets": (output_type == 'update' ? [4,5] : [3,4]),
				"className": 'text-right'
			}],
			pageLength: 5,
			lengthMenu: [5, 10, 25, 50, 100],
	        responsive: true,
	        "order": [[ (output_type == 'update' ? 1 : 0), "asc" ]],
	        createdRow:function(row){
				$(row).addClass('table-components');
				$('td', row).css('vertical-align', 'middle')
			},
			"dom": 'lT<"dt-toolbar">fgtip'
		});

		$('#'+table_id_components+'_paginate').addClass('pull-right');

		$('.dataTables_length label select').select2({
			placeholder : '',
			allowClear : false
		});


	}

	function initButtons(){
		$('#ChangeLocation').click(function(){
			let location = $('[name="FA_DepartmentID"]').val();
			let affiliates = $('[name="FA_AffiliatesID"]').val();

			$('[name="FAMHCL_DepartmentID_To"], [name="FAMHCL_AffiliatesID_To"]').select2('destroy');

			$('[name="FAMHCL_DepartmentID_To"] option').removeAttr('disabled');
			$('[name="FAMHCL_AffiliatesID_To"] option').removeAttr('disabled');

			$('[name="FAMHCL_DepartmentID_To"] option[value="'+location+'"]').prop('disabled', true);
			$('[name="FAMHCL_AffiliatesID_To"] option[value="'+affiliates+'"]').prop('disabled', true);

			$('[name="FAMHCL_DepartmentID_To"], [name="FAMHCL_AffiliatesID_To"]').select2({
				placeholder 	: '',
				allowClear 		: true	
			});

			$.ajax({
				url: global.site_name + 'asset_management/asset_list/getLastMovementHistory',
				type: 'POST',
				dataType: 'json',
				async: false,
				data: {FA_ID: $('[name="FA_ID"]').val()},
				success 	: function(result){
					if (result != '') {
						$('[name="FAMHCL_Date"]').datepicker('setStartDate', result);
					}
				},
				error 		: function(jqXHR){
					swal('Error', 'Contact your System Administrator.', 'error');
				}
			});
			

			$('[name="FAMHCL_Description"]').val($('[name="FA_Description"]').val())
			$('[name="FAMHCL_FA_AssetID"]').val($('[name="FA_ID"]').val())
			$('[name="FAMHCL_DepartmentID_From"]').val($('[name="FA_DepartmentID"] option:selected').text())
			$('[name="FAMHCL_AffiliatesID_From"]').val($('[name="FA_AffiliatesID"] option:selected').text())
			$('#change-location').modal('show');
		});

		$('#Dispose').click(function(){
			$('[name="FAMH_Description"]').val($('[name="FA_Description"]').val())
			$('[name="FAMH_FA_AssetID"]').val($('[name="FA_ID"]').val())

			$.ajax({
				url: global.site_name + 'asset_management/asset_list/getLastMovementHistory',
				type: 'POST',
				dataType: 'json',
				async: false,
				data: {FA_ID: $('[name="FA_ID"]').val()},
				success 	: function(result){
					if (result != '') {
						$('[name="FAMH_Date"]').datepicker('setStartDate', result);
					}
				},
				error 		: function(jqXHR){
					swal('Error', 'Contact your System Administrator.', 'error');
				}
			});

			$('#dispose-template').modal('show');
		});

		$('#MaintenanceLog').click(function(){
			$('#maintenance-log input, #maintenance-log select, #maintenance-log textarea').val('').trigger('change');
			$('[name="FAML_Description"]').val($('[name="FA_Description"]').val())
			$('[name="FAML_FA_AssetID"]').val($('[name="FA_ID"]').val())

			lastMaintenanceLogDate();

			$('.save-ml').removeAttr('data-todo')
			$('.save-ml').attr('data-todo', 'add')
			$('#save-new-ml').show()
			$('#save-close-ml').html('<span class="glyphicon glyphicon-floppy-saved"></span> Add and Close');
			$('#maintenance-log').modal('show');
		});
	}

	function initAddUpdate(){
		// Components table button functions

		$('.add-header').click(function(event){
			$('[name="FAC_Qty"]').val(0)
			$('[name="FAC_UnitCost"]').val(0)
			$('[name="FAC_ItemID"], [name="FAC_Description"]').val('').trigger('change')
			compute_total_cost();
			$('.save-components').removeAttr('data-todo')
			$('.save-components').attr('data-todo', 'add')
			$('#save-new-components').show()
			$('#save-close-components').html('<span class="glyphicon glyphicon-floppy-saved"></span> Add and Close');
			$('#components-template').modal('show');
		});

		$('#'+table_id_components).on('click', '.edit-components', function(e){
			e.preventDefault();
			let data = $(this).data('record');

			$('[name="FAC_ItemID"]').append('<option value="'+data['FAC_ItemID']+'">'+ data['FAC_ItemCode'] + ' - ' + data['FAC_Description'] +'</option>').trigger('change');
			$('[name="FAC_ItemID"]').val(data['FAC_ItemID']).trigger('change');
			$('[name="FAC_FA_LineNo"]').val(data['FAC_FA_LineNo']);
			$('[name="FAC_Description"]').val(data['FAC_Description']);
			$('[name="FAC_Qty"]').val(number_format(parseFloat(data['FAC_Qty']), 0, ".", ""))
			$('[name="FAC_UnitCost"]').val(number_format(data['FAC_UnitCost'], 2, ".", ""))
			$('[name="FAC_TotalCost"]').val(number_format(data['FAC_TotalCost'], 2))
			compute_total_cost();
			$('#save-close-components').html('<span class="glyphicon glyphicon-floppy-saved"></span> Update');
			$('.save-components').removeAttr('data-todo')
			$('.save-components').attr('data-todo', 'update')
			$('#save-new-components').hide()
			$('#components-template').modal('show');
		});

		$('#'+table_id_components).on('click', '.delete-components',function(e){
			e.preventDefault();
			var doc_no = $(this).data('id');
			var line_no = $(this).data('line-no');

			swal({
					title: "Are you sure?",
					text:  "This data will be deleted",
					type:  "warning",
					showCancelButton:  true,
					confirmButtonColor: "#1AB394",
					confirmButtonText:  "Yes",
					cancelButtonText: "No",
					closeOnConfirm: false
			}, function(isConfirm){
				if(isConfirm){
					$.ajax({
						type: 'GET',
						dataType: 'json',
						url: global.site_name + controller_url + 'delete_components',
						data: {	'id'		: doc_no,
								'line_no'	: line_no,
								'action'	: 'delete'},
						success: function(data){
							if(data.success){
								swal({
									title: "Delete Success",
									type:  "success"
								});
								table_components.ajax.reload();
							}else{
								swal({
									title: data.title,
									text:  data.message,
									type:  data.type
								});
								table_components.ajax.reload();
							}
						},
						error : function(){
							swal("Error", "Please contact your system administrator", "error")
							table_components.ajax.reload();
						}
					});
				}
			});
		});

		// End

		// Maintenance table button function

		$('#'+table_id_maintenance).on('click', '.edit-maintenance', function(e){
			e.preventDefault();
			let data = $(this).data('record');

			$('[name="FAML_Description"]').val($('[name="FA_Description"]').val())
			$('[name="FAML_FA_LineNo"]').val(data['FAML_FA_LineNo']);
			$('[name="FAML_Date"]').val(data['FAML_Date']);
			$('[name="FAML_Particular"]').val(data['FAML_Particular'])
			$('[name="FAML_Cost"]').val(number_format(data['FAML_Cost'], 2, ".", ""))
			$('[name="FAML_Remarks"]').val(data['FAML_Remarks'])
			
			$('[name="FAML_Date"]').datepicker('setStartDate', data['FAML_Date']);
			compute_total_cost();
			$('#save-close-ml').html('<span class="glyphicon glyphicon-floppy-saved"></span> Update');
			$('.save-ml').removeAttr('data-todo')
			$('.save-ml').attr('data-todo', 'update')
			$('#save-new-ml').hide()
			$('#maintenance-log').modal('show');
		});

		$('#'+table_id_maintenance).on('click', '.delete-maintenance',function(e){
			e.preventDefault();
			var doc_no = $(this).data('id');
			var line_no = $(this).data('line-no');

			swal({
					title: "Are you sure?",
					text:  "This data will be deleted",
					type:  "warning",
					showCancelButton:  true,
					confirmButtonColor: "#1AB394",
					confirmButtonText:  "Yes",
					cancelButtonText: "No",
					closeOnConfirm: false
			}, function(isConfirm){
				if(isConfirm){
					$.ajax({
						type: 'GET',
						dataType: 'json',
						url: global.site_name + controller_url + 'delete_maintenance',
						data: {	'id'		: doc_no,
								'line_no'	: line_no,
								'action'	: 'delete'},
						success: function(data){
							if(data.success){
								swal({
									title: "Delete Success",
									type:  "success"
								});
								table_components.ajax.reload();
							}else{
								swal({
									title: data.title,
									text:  data.message,
									type:  data.type
								});
								table_components.ajax.reload();
							}
						},
						error : function(){
							swal("Error", "Please contact your system administrator", "error")
							table_components.ajax.reload();
						}
					});
				}
			});
		});

		// End
	}

	function compute_values(){

		$('[name="FA_AcquisitionCost"], [name="FA_ExchangeRate"]').change(function(event) {
			let exchange_rate = parseFloat($('[name="FA_ExchangeRate"]').val());
			let acquisition_cost = parseFloat($('[name="FA_AcquisitionCost"]').val());

			$('#FA_NetBookValue').val(number_format(exchange_rate * acquisition_cost, 2));
		});
	}

	function compute_total_cost(){
		let qty = parseFloat($('[name="FAC_Qty"]').val());
		let cost = parseFloat($('[name="FAC_UnitCost"]').val());

		$('[name="FAC_TotalCost"]').val(number_format(qty * cost, 2))
	}

	function save_maintenance_log(){
		$('.save-ml').click(function(event) {

			let isValid = validate_fields('#maintenance-log');
			let save_type = $(this).attr('data-action');
			let action = $(this).attr('data-todo');

			if (isValid) {
				

				$.ajax({
					url: global.site_name + 'asset_management/asset_list/save_maintenance_log',
					type: 'POST',
					dataType: 'json',
					data: {
						FAML_FA_AssetID 	: $('[name="FAML_FA_AssetID"]').val(),
						FAML_FA_LineNo 		: $('[name="FAML_FA_LineNo"]').val(),
						FAML_Date 			: $('[name="FAML_Date"]').val(),
						FAML_Particular 	: $('[name="FAML_Particular"]').val(),
						FAML_Cost 			: $('[name="FAML_Cost"]').val(),
						FAML_Remarks 		: $('[name="FAML_Remarks"]').val(),
						action 				: action
					},
					success 	: function(result){
						if (result.success) {
							$('#maintenance-log input:not([name="FAML_FA_AssetID"]), #maintenance-log select, #maintenance-log textarea').val('').trigger('change');
							$('[name="FAML_Cost"]').val('0.00');
							table_maintenance.ajax.reload();

							if (save_type == 'save-close') {
								$('#maintenance-log').modal('hide');
							}
						}
						else{
							swal('Error', result.message, 'error');
							table_maintenance.ajax.reload();
						}
					},
					error 		: function(jqXHR){
						swal('Error ' + jqXHR.status, 'Contact your System Administrator.', 'error');
						table_maintenance.ajax.reload();
					}
				});
				
			}
		});
	}

	function save_dispose(){
		$('#save-dispose').click(function(event) {

			let isValid = validate_fields('#dispose-template');

			if (isValid) {
				

				$.ajax({
					url: global.site_name + 'asset_management/asset_list/save_dispose',
					type: 'POST',
					dataType: 'json',
					data: {
						FAMH_FA_AssetID 	: $('[name="FAMH_FA_AssetID"]').val(),
						FAMH_Date 			: $('[name="FAMH_Date"]').val(),
						FAMH_Remarks 		: $('[name="FAMH_Remarks"]').val(),
					},
					success 	: function(result){
						if (result.success) {
							$('#dispose-template input, #dispose-template select, #dispose-template textarea').val('').trigger('change');
							status = 'Disposed';
							table_movement.ajax.reload();
							table_maintenance.ajax.reload();
							table_components.ajax.reload();
							$(':input').prop('disabled', true);
							$('button, a:not(#back, .nav-link, #home)').hide();
							$('#dispose-template').modal('hide');
							swal("Success", "Asset Disposed.", "success");
						}
						else{
							swal('Error', result.message, 'error');
							table_movement.ajax.reload();
						}
					},
					error 		: function(jqXHR){
						swal('Error ' + jqXHR.status, 'Contact your System Administrator.', 'error');
						table_movement.ajax.reload();
					}
				});
				
			}
		});
	}

	function save_change_location(){
		$('#save-cl').click(function(event) {

			let isValid = validate_fields('#change-location');

			if (isValid) {
				

				$.ajax({
					url: global.site_name + 'asset_management/asset_list/save_change_location',
					type: 'POST',
					dataType: 'json',
					data: {
						FAMH_FA_AssetID 	: $('[name="FAMHCL_FA_AssetID"]').val(),
						FAMH_Date 			: $('[name="FAMHCL_Date"]').val(),
						FAMH_LocationID 	: $('[name="FAMHCL_DepartmentID_To"]').val(),
						FAMH_AffiliatesID 	: $('[name="FAMHCL_AffiliatesID_To"]').val(),
						FAMH_RefNo 			: $('[name="FA_RefNo"]').val(),
					},
					success 	: function(result){
						if (result.success) {
							$('[name="FA_DepartmentID"]').val($('[name="FAMHCL_DepartmentID_To"]').val());
							$('[name="FA_AffiliatesID"]').val($('[name="FAMHCL_AffiliatesID_To"]').val());

							last_location = $('[name="FAMHCL_DepartmentID_To"]').val();
							last_affiliates = $('[name="FAMHCL_AffiliatesID_To"]').val();
							$('#change-location input, #change-location select, #change-location textarea').val('').trigger('change');
							table_movement.ajax.reload();
							$('#change-location').modal('hide');
						}
						else{
							swal('Error', result.message, 'error');
							table_movement.ajax.reload();
						}
					},
					error 		: function(jqXHR){
						swal('Error ' + jqXHR.status, 'Contact your System Administrator.', 'error');
						table_movement.ajax.reload();
					}
				});
				
			}
		});
	}

	function save_components(){
		$('.save-components').click(function(event) {

			let isValid = validate_fields('#components-template');
			let save_type = $(this).attr('data-action');
			let action = $(this).attr('data-todo');

			if (isValid) {
				

				$.ajax({
					url: global.site_name + 'asset_management/asset_list/save_components',
					type: 'POST',
					dataType: 'json',
					data: {
						FAC_FA_AssetID 			: $('[name="FA_ID"]').val(),
						FAC_FA_LineNo 			: $('[name="FAC_FA_LineNo"]').val(),
						FAC_ItemID 				: $('[name="FAC_ItemID"]').val(),
						FAC_Description 		: $('[name="FAC_Description"]').val(),
						FAC_Qty 				: $('[name="FAC_Qty"]').val(),
						FAC_UnitCost 			: $('[name="FAC_UnitCost"]').val(),
						FAC_TotalCost 			: number_format($('[name="FAC_TotalCost"]').val(), 12, ".", ""),
						action 					: action
					},
					success 	: function(result){
						if (result.success) {
							$('#components-template input, #components-template select, #components-template textarea').val('').trigger('change');
							$('[name="FAC_Qty"]').val(0)
							$('[name="FAC_UnitCost"]').val(0)
							compute_total_cost();
							table_components.ajax.reload();

							if (save_type == 'save-close') {
								$('#components-template').modal('hide');
							}
						}
						else{
							swal('Error', result.message, 'error');
							table_components.ajax.reload();
						}
					},
					error 		: function(jqXHR){
						swal('Error ' + jqXHR.status, 'Contact your System Administrator.', 'error');
						table_components.ajax.reload();
					}
				});
				
			}
		});
	}

	function validate_fields(form_modal){
		let isValid = true;

		$(form_modal+' input:required, '+form_modal+' select:required').each(function(index, el) {
			if ($(this).val() == '') {
				isValid = false;
			}		
		});

		return isValid;

	}

	function lastMaintenanceLogDate(){

		$.ajax({
			url: global.site_name + 'asset_management/asset_list/getLastMaintenanceLog',
			type: 'POST',
			dataType: 'json',
			async: false,
			data: {FA_ID: $('[name="FA_ID"]').val()},
			success 	: function(result){
				if (result != '') {
					$('[name="FAML_Date"]').datepicker('setStartDate', result);
				}
			},
			error 		: function(jqXHR){
				swal('Error', 'Contact your System Administrator.', 'error');
			}
		});

	}

	init();

});