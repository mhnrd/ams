$(document).ready(function(){

	function initTable(){
		// Datatable Setup
		tableindex = $('#'+table_id).DataTable( {
			"processing":true,
			"serverSide":true,
			"scrollY": "350px",
			"scrollX": true,
  			"scrollCollapse": true,
  			"paging": true,
			"order":[],
			"ajax":{
				url: global.site_name + 'asset_management/asset_list/data',
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[0],
				"orderable": false,
			},{
				"targets": [0,1,3,4,5,7],
				"className": 'text-center'
			},{
				"targets": [6],
				"className": 'text-right'
			}
			],
			pageLength: 10,
	        responsive: true,
	        "order": [[ 1, "asc" ]],
	        createdRow:function(row, data){
				$(row).addClass('table-header');
				if ( data[7] == 'Disposed' ) {
	                $('td', row).attr('style', 'color:#808080!important');
	            }
	            else if ( data[7] == 'Inactive' ) {
	                $('td', row).attr('style', 'color:red!important');
	            }
			},
			"dom": 'lT<"dt-toolbar">gtip'
		});

		$('#'+table_id+'_paginate').addClass('pull-right');

		$('[name=FACS_UsefulLifeNoUOM]').select2({
			placeholder: '',
			allowClear: true
		}).on('change', function() {
            $(this).valid();
        });

	}
	
	$('.dropdown').click(function(event) {
			$('.dropdown-content').toggle();
		});

	initTable();
});