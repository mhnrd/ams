$(document).ready(function(){

	// $('#btnActive').prop('disabled', true);
	// $('#btnDeactive').prop('disabled', true);
	function initTable(){
		// Datatable Setup
		tableindex = $('#'+table_id).DataTable( {
			"processing":true,
			"serverSide":true,
			"scrollY": "350px",
			"scrollX": true,
  			"scrollCollapse": true,
  			"paging": true,
			"order":[],
			"ajax":{
				url: global.site_name + 'attributes/data?id='+attr+'&attr='+attr_module ,
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[0,1],
				"orderable": false,
			},{
				"targets": [0,1],
				"className": 'text-center'
			}],
			pageLength: 10,
	        responsive: true,
	        "order": [[ 2, "asc" ]],
			"dom": 'lT<"dt-toolbar">gtip'
		});

		$('#'+table_id+'_paginate').addClass('pull-right');
	}

	function initAddUpdate(){
		$('.add-header').click(function(event) {
			$('#form-header input, #form-header select').val('').trigger('change');
			$('[name="AD_Code"]').attr('readonly', false);
			$('[name="AD_Active"]').prop('checked', true);
			$('#save-new').show()
			$('#save-close').html('<span class="glyphicon glyphicon-floppy-disk"></span> Add and Close');
			$('.save-attribute').removeAttr('data-todo')
			$('.save-attribute').attr('data-todo', 'add')
			$('#detail-template').modal('show');
		});

		$('#'+table_id).on('click', '.update-header', function(){
			let data = $(this).data('record');

			$('[name="AD_Code"]').val(data['AD_Code']);
			$('[name="AD_Code"]').attr('readonly', true);
			$('[name="AD_Desc"]').val(data['AD_Desc']);
			$('[name="AD_Active"]').prop('checked', (data['AD_Active'] == 'Active' ? true : false));
			$('[name="AD_ID"]').val(data['AD_ID']);
			$('#save-close').html('<span class="glyphicon glyphicon-floppy-disk"></span> Update');
			$('.save-attribute').removeAttr('data-todo')
			$('.save-attribute').attr('data-todo', 'update')
			$('#save-new').hide()
			$('#detail-template').modal('show');
		});

	}

	function save_attribute(){
		$('.save-attribute').click(function(event) {

			let form = $('#form-header');
			let save_type = $(this).attr('data-action');
			let action = $(this).attr('data-todo');

			isValid = form.valid();

			if (isValid) {
				let data = form.serializeArray();

				data.push({
					name: 'todo',
					value: action
				});

				data.push({
					name: 'AD_ID',
					value: $('[name="AD_ID"]').val()
				});

				data.push({
					name: 'AD_FK_Code',
					value: attr_module
				});

				data.push({
					name: 'AD_Active',
					value: ($('[name="AD_Active"]').is(':checked') ? 1 : 0)
				});

				$.ajax({
					url: global.site_name + 'attributes/save',
					type: 'POST',
					dataType: 'json',
					data: data,
					success 	: function(result){
						if (result.success) {
							if (save_type == 'save-new') {
								$('#form-header input, #form-header select').val('').trigger('change');
								tableindex.ajax.reload();
							}
							else{
								$('#form-header input, #form-header select').val('').trigger('change');
								$('#detail-template').modal('hide');
								tableindex.ajax.reload();
							}
						}
						else{
							swal('Error', result.message, 'error');
							tableindex.ajax.reload();
						}
					},
					error 		: function(jqXHR){
						swal('Error ' + jqXHR.status, 'Contact your System Administrator.', 'error');
						tableindex.ajax.reload();
					}
				});
				
			}
		});
	}

	initTable();
	initAddUpdate();
	save_attribute();

});