(function () {
	// declare 3rd party event
	setTimeout(function(){
		if($('body select:not(#YearPeriod)').length > 0){
		    $('body select').select2({
		    	placeholder: 	'',
				allowClear: 	true
	    	}).on('change', function() {
			    $(this).valid();
			});
		}

		if($('body .input-group.date').length > 0){
		    $('body .input-group.date [name="dtFrom"]').datepicker({
		        todayBtn: "linked",
		        keyboardNavigation: false,
		        forceParse: false,
		        calendarWeeks: true,
		        autoclose: true,
		    });

		    let date_today = new Date();

		    $('body .input-group.date [name="dtFrom"]').datepicker('setDate', date_today);

		    $('body .input-group.date [name="dtTo"]').datepicker({
		        todayBtn: "linked",
		        keyboardNavigation: false,
		        forceParse: true,
		        calendarWeeks: true,
		        startDate: $('body .input-group.date [name="dtFrom"]').val(),
		        autoclose: true
		    });

		    $('body .input-group.date [name="dtTo"]').datepicker('setDate', date_today);

		    $('body .input-group.date [name="dtFrom"]').change(function(event) {
		    	if ($('body .input-group.date [name="dtFrom"]').val() != '') {
		    		let dtFrom = $('body .input-group.date [name="dtFrom"]').val();
		    		let dtTo = $('body .input-group.date [name="dtTo"]').val();

		    		if(new Date(dtFrom) > new Date(dtTo)){
		    			$('body .input-group.date [name="dtTo"]').datepicker('setDate', new Date(dtFrom));
		    			$('body .input-group.date [name="dtTo"]').datepicker('setStartDate', dtFrom);
		    		}
		    		else{
		    			$('body .input-group.date [name="dtTo"]').datepicker('setStartDate', dtFrom);
		    		}
		    	}
		    });
		}
	},500);

	if($('body .input-group.date').length > 0){
	}

	// REPORT TAB
	$(document).on('click','#create-report',function(e){
		e.preventDefault();
		$('.disable').prop('disabled', false);
   		var data = $("#reports-form").serializeArray();
   		$('.disable').prop('disabled', true);
   		let form = $("#reports-form");

   		if($('[name="ChkBox"]').length > 0){
   			let checkbox  = {
   				'name' 	: 'ChkBox',
   				'value' : ($('[name="ChkBox"]').is(':checked') ? 1 : 0)
   			};
   			data.push(checkbox);

   		}

   		if($('[name="LoanType"]').length > 0){
   			let loan_desc  = {
   				'name' 	: 'LoanTypeDesc',
   				'value' : $('[name="LoanType"] option:selected').text()
   			};
   			data.push(loan_desc);

   		}

   		form.validate({
                    errorPlacement: function (error, element)
                    {
                        element.before(error);
                    },
                    ignore: [],
                    rules: {
                        confirm: {
                            equalTo: "#password"
                        }
                    }
                });
   		let isValid = form.valid();
		var create_report = {
							    state : function(current_state){
							        if(current_state == 'loading'){
							            $('#create-report').empty();
							            $('#create-report').append('<i class="fa fa-refresh fa-spin"></i> Generating Report');
							            $('#create-report').attr('disabled', true);
							        }
							        if(current_state == 'click'){
							            $('#create-report').attr('disabled', false);
							            $('#create-report').empty();
							            $('#create-report').append('<span class="glyphicon glyphicon-print"></span> Generate Report');
							        }
							    }
							};

	   	if(isValid){
		    create_report.state('loading');

		   	var report_module = $(this).data('report-module');
		    $.ajax({
		    	url: module_url + '/print_document',
		    	type: 'POST',
		    	dataType: 'json',
		    	data: data,
		    	success : function(result){
		    		


		    		if (result.success) {
	    				let res = result.file_name.split(".");
		    			console.log(res);
		    			
			    		if(res[1] == 'pdf'){
				    		var win = window.open(global.site_name + 'pdf/'+result.file_name, '_blank');
				    		if (win) {
			                    //Browser has allowed it to be opened
			                    win.focus();
			                    create_report.state('click');
			                } else {
			                    //Browser has blocked it
			                    alert('Please allow popups for this website');
			                    create_report.state('click');
			                }
			    		}
			    		else{
			    			var win = window.open(global.site_name + 'xls/'+result.file_name, '_blank');
				    		if (win) {
			                    //Browser has allowed it to be opened
			                    win.focus();
			                    create_report.state('click');
			                } else {
			                    //Browser has blocked it
			                    alert('Please allow popups for this website');
			                    create_report.state('click');
			                }
			    		}
		    		}
		    		else{
		    			swal("Error", result.message, "error");
		    			create_report.state('click');
		    		}

		    	},error : function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					create_report.state('click');
				}
		    });
	   	}

	   	

	});

	// STANDARD PRINT OUT PER MODULE
	$(document).on('click','#print',function(e){
		e.preventDefault();
		var print_report = {
							    state : function(current_state){
							        if(current_state == 'loading'){
							        	$('#print').removeClass('btn-outline');
							            $('#print').empty();
							            $('#print').append('<span class="fa fa-refresh fa-spin"></span>');
							            $('#print').attr('disabled', true);
							        }
							        if(current_state == 'click'){
							        	$('#print').addClass('btn-outline');
							            $('#print').attr('disabled', false);
							            $('#print').empty();
							            $('#print').append('<span class="glyphicon glyphicon-print"></span>');
							        }
							    }
							};
	    print_report.state('loading');

	    $.ajax({
	    	url: module_url + '/print_document?id=' + $(this).data('doc-no') + (module_url == '/ttcfoodline/production/manufacturing-order' ? '&date=' + $(this).data('doc-date') : ''),
	    	type: 'get',
	    	dataType: 'json',
	    	success : function(result){
	    		var win = window.open(global.site_name + 'pdf/'+result.pdf_name, '_blank');
	    		if (win) {
                    //Browser has allowed it to be opened
                    win.focus();
                    print_report.state('click');
                } else {
                    //Browser has blocked it
                    alert('Please allow popups for this website');
                	print_report.state('click');
                }
	    	}
	    });	    
		
	});

	// EXTRA STANDARD PRINT OUT PER MODULE
	$(document).on('click','#print_doc',function(e){
		e.preventDefault();
		var print_report = {
							    state : function(current_state){
							        if(current_state == 'loading'){
							        	$('#print').removeClass('btn-outline');
							            $('#print').empty();
							            $('#print').append('<span class="fa fa-refresh fa-spin"></span>');
							            $('#print').attr('disabled', true);
							        }
							        if(current_state == 'click'){
							        	$('#print').addClass('btn-outline');
							            $('#print').attr('disabled', false);
							            $('#print').empty();
							            $('#print').append('<span class="glyphicon glyphicon-print"></span>');
							        }
							    }
							};
	    print_report.state('loading');
	    

	    $.ajax({
	    	url: module_url + '/print_doc?id=' + $(this).data('doc-no'),
	    	type: 'get',
	    	dataType: 'json',
	    	success : function(result){
	    		var win = window.open(global.site_name + 'pdf/'+result.pdf_name, '_blank');
	    		if (win) {
                    //Browser has allowed it to be opened
                    win.focus();
                    print_report.state('click');
                } else {
                    //Browser has blocked it
                    alert('Please allow popups for this website');
                	print_report.state('click');
                }
	    	}
	    });    
		
	});

})();

function disable_input(input_field){
	input_field.addClass('disable');
	input_field.prop('disabled', true);
}

function enable_input(input_field){
	input_field.removeClass('disable');
	input_field.prop('disabled', false);
}