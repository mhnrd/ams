$(document).ready(function(){

	function initTable(){
		// Datatable Setup
		tableindex = $('#'+table_id).DataTable( {
			"processing":true,
			"serverSide":true,
			"scrollY": "350px",
			"scrollX": true,
  			"scrollCollapse": true,
  			"paging": true,
			"order":[],
			"ajax":{
				url: global.site_name + 'asset_management/configuration/asset_class_setup/data',
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[0],
				"orderable": false,
			},{
				"targets": col_center,
				"className": 'text-center'
			}],
			pageLength: 10,
	        responsive: true,
	        "order": [[ 2, "asc" ]],
			"dom": 'lT<"dt-toolbar">gtip'
		});

		$('#'+table_id+'_paginate').addClass('pull-right');

		$('[name=FACS_UsefulLifeNoUOM]').select2({
			placeholder: '',
			allowClear: true
		}).on('change', function() {
            $(this).valid();
        });

	}

	function initAddUpdate(){
		$('.add-header').click(function(event) {
			$('#form-header input').val('').trigger('change');
			$('#form-header select').val('M').trigger('change');
			$('[name="FACS_ID"]').attr('readonly', false);
			$('#save-new').show()
			$('#save-close').html('<span class="glyphicon glyphicon-floppy-disk"></span> Add and Close');
			$('.save-asset-class').removeAttr('data-todo')
			$('.save-asset-class').attr('data-todo', 'add')
			$('#detail-template').modal('show');
		});

		$('#'+table_id).on('click', '.update-header', function(){
			let data = $(this).data('record');

			$('[name="FACS_ID"]').val(data['FACS_ID']);
			$('[name="FACS_ID"]').attr('readonly', true);
			$('[name="FACS_Description"]').val(data['FACS_Description']);
			$('[name="FACS_UsefulLifeNo"]').val(number_format(data['FACS_UsefulLifeNo'], 0, ".", ""));
			$('[name="FACS_UsefulLifeNoUOM"]').val(data['FACS_UsefulLifeNoUOM']).trigger('change');
			$('#save-close').html('<span class="glyphicon glyphicon-floppy-disk"></span> Update');
			$('.save-asset-class').removeAttr('data-todo')
			$('.save-asset-class').attr('data-todo', 'update')
			$('#save-new').hide()
			$('#detail-template').modal('show');
		});

	}

	function save_asset_class(){
		$('.save-asset-class').click(function(event) {

			let form = $('#form-header');
			let save_type = $(this).attr('data-action');
			let action = $(this).attr('data-todo');

			isValid = form.valid();

			if (isValid) {
				let data = form.serializeArray();

				data.push({
					name: 'action',
					value: action
				});

				$.ajax({
					url: global.site_name + 'asset_management/configuration/asset_class_setup/save_asset_class',
					type: 'POST',
					dataType: 'json',
					data: data,
					success 	: function(result){
						if (result.success) {
							if (save_type == 'save-new') {
								$('#form-header input, #form-header select').val('').trigger('change');
								tableindex.ajax.reload();
							}
							else{
								$('#form-header input, #form-header select').val('').trigger('change');
								$('#detail-template').modal('hide');
								tableindex.ajax.reload();
							}
						}
						else{
							swal('Error', result.message, 'error');
							tableindex.ajax.reload();
						}
					},
					error 		: function(jqXHR){
						swal('Error ' + jqXHR.status, 'Contact your System Administrator.', 'error');
						tableindex.ajax.reload();
					}
				});
				
			}
		});
	}

	initTable();
	initAddUpdate();
	save_asset_class();

});