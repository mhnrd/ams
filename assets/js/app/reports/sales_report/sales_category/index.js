$(document).ready(function(){
	 $('#date-from').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
	 $('#date-to').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

	$('[name="location_type_filter"]').select2({
		placeholder: ''
	});
	$('[name="filter_class"]').select2({
		placeholder: ''
	})

	var radio = "null";
	$(".no_option").on("click", function(){
	  if($(this).val() == radio){
	  	$('input[name=radio][value=null]').prop("checked",true);
	    radio = "null";
	  }else{
	  	radio = $(this).val();
	  }
	});


	//DataTable Setup
	tableindex = $('#sales_category_location').DataTable( {
		responsive: true,
		"processing": true,
		"serverSide": true,
		"order":[],
		"ajax":{
				url: global.site_name + 'reports/sales_report/sales_category/data_Location',
				type:"POST" 
		},
		"deferRender": true,
		"columnDefs": [{
			"targets": [0,1],
			"orderable": false,
		},{
			"targets": [0,1],
			"className": 'text-center'
		}],
		pageLength: 10,
        responsive: true,
        createdRow:function(row){
			$(row).addClass('breakdown-detail');
        },
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	$('#'+table_id+'_paginate').addClass('pull-right');
	
	$('#sales_category_category').DataTable({
		responsive: 	true,
		"processing": 	true,
		"serverSide": 	true,
		"order": [],
		"ajax":{
				url: global.site_name + 'reports/sales_report/sales_category/data_category',
				type: "POST"
		},
		"deferRender": 	true,
		"columnDefs": 	[{
			"targets": [0,1],
			"orderable": false,
		},{
			"targets": [0,1],
			"className": 'text-center'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('breakdown-detail');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});
	$('#'+table_id1+'_paginate').addClass('pull-right');
	
})
