$(document).ready(function(){
	 $('#date-from').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
	 $('#date-to').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
	var radio_details = "null";
	$("#radio_details").on("click", function(){
		if ($(this).val() == radio_details) {
			$('#radio_details[value=null]').prop("checked", true);
			radio_details = "null";
		}else{
			radio_details = $(this).val();
		}
	});
	var radio_summary = "null";
	$("#radio_summary").on("click", function(){
		if ($(this).val() == radio_summary) {
			$('#radio_summary[value=null]').prop("checked", true);
			radio_summary = "null";
		}else{
			radio_summary = $(this).val();
		}
	});

	$('[name="location_type_filter"]').select2({
		placeholder: ''
	});
	$('[name="filter_class"]').select2({
		placeholder: ''
	})


	//DataTable Setup
	tableindex = $('#sales_product_location').DataTable( {
		responsive: true,
		"processing": true,
		"serverSide": true,
		"order":[],
		"ajax":{
				url: global.site_name + 'reports/sales_report/sales_prod_dept/data_Location',
				type:"POST" 
		},
		"deferRender": true,
		"columnDefs": [{
			"targets": [0,1],
			"orderable": false,
		},{
			"targets": [0,1],
			"className": 'text-center'
		}],
		pageLength: 10,
        responsive: true,
        createdRow:function(row){
			$(row).addClass('breakdown-detail');
        },
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	$('#'+table_id+'_paginate').addClass('pull-right');
	
	$('#sales_product_category').DataTable({
		responsive: 	true,
		"processing": 	true,
		"serverSide": 	true,
		"order": [],
		"ajax":{
				url: global.site_name + 'reports/sales_report/sales_prod_dept/data_category',
				type: "POST"
		},
		"deferRender": 	true,
		"columnDefs": 	[{
			"targets": [0,1],
			"orderable": false,
		},{
			"targets": [0,1],
			"className": 'text-center'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('breakdown-detail');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});
	$('#'+table_id1+'_paginate').addClass('pull-right');
	
})
