$(document).ready(function(){
	$('.datepicker').datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: false,
		autoclose: true,
		format: "mm/dd/yyyy"
	});
	$('.js-select2').select2();
})