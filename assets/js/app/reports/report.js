(function () {

	// declare 3rd party event
	setTimeout(function(){
		if($('body select').length > 0){
		    $('body select').select2({
		    	placeholder: 	" ",
				allowClear: 	true
	    	});
		}

		if($('body .input-group.date').length > 0){
		    $('body .input-group.date').datepicker({
		        todayBtn: "linked",
		        keyboardNavigation: false,
		        forceParse: false,
		        calendarWeeks: false,
		        autoclose: true
		    });
		}
	},1000);


    $('[name="dtFrom"]').change(function(){
		$('[name="dtTo"]').datepicker('setStartDate',new Date($('[name="dtFrom"]').val()) );
		$('[name="dtTo"]').val($('[name="dtFrom"]').val());
	});

	// REPORT TAB
	$(document).on('click','#create-report',function(e){
		e.preventDefault();
		var create_report = {
							    state : function(current_state){
							        if(current_state == 'loading'){
							            $('#create-report').empty();
							            $('#create-report').append('<i class="fa fa-refresh fa-spin"></i> Generating Report');
							            $('#create-report').attr('disabled', true);
							        }
							        if(current_state == 'click'){
							            $('#create-report').attr('disabled', false);
							            $('#create-report').empty();
							            $('#create-report').append('<span class="glyphicon glyphicon-print"></span> Generate Report');
							        }
							    }
							};
	    create_report.state('loading');

	   	var report_module = $(this).data('report-module');

   		var data = $("#reports-form").serializeArray();
	    $.ajax({
	    	url: module_url + '/print_document',
	    	type: 'POST',
	    	dataType: 'json',
	    	data: data,
	    	success : function(result){
	    		var win = window.open(global.site_name + 'pdf/'+result.pdf_name, '_blank');
	    		if (win) {
                    //Browser has allowed it to be opened
                    win.focus();
                    create_report.state('click');
                } else {
                    //Browser has blocked it
                    alert('Please allow popups for this website');
                    create_report.state('click');
                }
	    	},error : function(jqXHR){
				swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				create_report.state('click');
			}
	    });
	   	

	});

	//report fields
	$(document).on('click','.print-header',function(e){
		e.preventDefault();
		var create_report = {
							    state : function(current_state){
							        if(current_state == 'loading'){
							            $('.print-header').empty();
							            $('.print-header').append('<span class="glyphicon glyphicon-print"></span>');
							            $('.print-header').attr('disabled', false);
							        }
							        if(current_state == 'click'){
							            $('.print-header').attr('disabled', false);
							            $('.print-header').empty();
							            $('.print-header').append('<span class="glyphicon glyphicon-print"></span>');
							        }
							    }
							};
	    create_report.state('loading');

	   	var report_module = $(this).data('report-module');

   		var data = {'doc_no' : $(this).data('doc-no')};
	    $.ajax({
	    	url: global.site_name + module_url + '/printout',
	    	type: 'POST',
	    	dataType: 'json',
	    	data: data,
	    	success : function(result){
	    		var win = window.open(global.site_name + 'pdf/'+result.file_name, '_blank');
	    		if (win) {
                    //Browser has allowed it to be opened
                    win.focus();
                    create_report.state('click');
                } else {
                    //Browser has blocked it
                    alert('Please allow popups for this website');
                    create_report.state('click');
                }
	    	},error : function(jqXHR){
				swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				create_report.state('click');
			}
	    });
	   	

	});

})();