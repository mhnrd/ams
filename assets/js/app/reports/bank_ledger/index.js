$(document).ready(function(){

	// Datatable Setup
	tableIndex = $('.dataTables-example').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'reports/bank_ledger/data',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0, 1],
			"orderable": false,
		},{
			"targets": col_center,
			"className": 'text-center'
		}],
		pageLength: 10,
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip<"pull-right">'
	});

	$('#'+table_id+'_paginate').addClass('pull-right');

	$('.datepicker').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: false,
				autoclose: true,
				format: "mm/dd/yyyy",

			});

	$('#CPC').select2({
			placeholder: 'Enter Cost/Profit Center Here',
			allowClear: true
	});

	$('#document_type').select2({
		placeholder: 'Enter Document Type Here',
		allowClear: true
	});

	$('#BA').select2({
		placeholder: 'Enter Bank Account Here',
		allowClear: true
	});


});