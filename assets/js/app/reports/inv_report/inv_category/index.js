$(document).ready(function(){

	function init(){

		initTableLocation();

		$('.datepicker').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "mm/dd/yyyy"
		});

		$('#as-of').select2({
			placeholder: '',
			allowClear: true,
		});

		$('#brand').select2({
			placeholder: '',
			allowClear: true,
		});

		$('#str').select2({
			placeholder: '',
			allowClear: true,
		});

		$('#cboClass').select2({
			placeholder: '',
			allowClear: true,
		});


	}

	init();
	initTableCategory();
});

	function initTableLocation(){

		// Datatable Setup
		tableindex = $('#tbl_location').DataTable( {
			"processing":true,
			"serverSide":true,
			"order":[],
			"ajax":{
				url: global.site_name + 'reports/inv_report/inv_category/data_L',
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[0, 1],
				"orderable": false,
			},{
				"targets": [0,1],
				"className": 'text-center'
			},{
				"targets": [2,3],
				"className": 'text-left'
			}],
			pageLength: 10,
			responsive: true,
			createdRow:function(row){
				//$(row).addClass('table-header');
			},
			"dom": 'lT<"dt-toolbar">fgtip'
		});

			$('#'+table_id+'_paginate').addClass('pull-right');
	}

	function initTableCategory(){

		// Datatable Setup
		table_category = $('#tbl_category').DataTable( {
			"processing":true,
			"serverSide":true,
			"order":[],
			"ajax":{
				url: global.site_name + 'reports/inv_report/inv_category/data_C',
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[0, 1],
				"orderable": false,
			},{
				"targets": [0,1],
				"className": 'text-center'
			},{
				"targets": [2,3],
				"className": 'text-left'
			}],
			pageLength: 10,
			responsive: true,
			createdRow:function(row){
				//$(row).addClass('table-header');
			},
			"dom": 'lT<"dt-toolbar">fgtip'
		});

			$('#'+table_id1+'_paginate').addClass('pull-right');
	}
	
	