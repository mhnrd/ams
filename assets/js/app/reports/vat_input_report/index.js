$(document).ready(function(){

	$('.js-select2').select2();

	$('.datepicker').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: false,
				autoclose: true,
				format: "mm/dd/yyyy",

			});
});