$(document).ready(function(){


	


		$('[name="P_Type"]').change(function(){

			let p_id = $(this).val();

			if(p_id == ''){
				$('[name="P_ID"]').val('');
			}
			else{
				$.ajax({
					url 		: global.site_name + 'administration/user_approval_setup/position/getCurrentPID',
					type 		: 'POST',
					dataType 	: 'json',
					data 		: {
						'p-id' 	: p_id
					},
					success 	: function(result){
						$('[name="P_ID"]').val(result);
					}
				});

				$.ajax({
					url 		: global.site_name + 'administration/user_approval_setup/position/getParentList',
					type 		: 'POST',
					dataType 	: 'json',
					data 		: {
						'p-id' 	: p_id
					},
					success 	: function(result){
						$('[name="P_Parent"]').empty();
						let option = '';
						option += '<option value=""></option>'
						$(result).each(function(index){
							option += '<option value="'+result[index].P_ID+'">'+result[index].P_Position+'</option>';
						});

						$('[name="P_Parent"]').append(option);
						$('[name="P_Parent"]').val('').trigger('change');
					}
				});
			}
		});


		if(output_type == 'update'){

			$('[name="P_Type"]').prop('disabled', true);
			

		}
		if(output_type == 'view'){
			$('[name= "P_ID"]').attr('readonly', true);
			$('[name= "P_Position"]').attr('readonly',true);
			$('[name="P_Type"], [name="P_Parent"]').prop('disabled', true);
			$('.select2-selection__arrow').hide();
			
		}

		if(output_type == 'add' || 'update'){
		$('.pt-js-select2').select2({
			placeholder: '',
			allowClear: true,
		});
		$('.pts-js-select2').select2({
			placeholder: '',
			allowClear: true,
		});	
		
	}
	
});



