$(document).ready(function(){

	function init(){

		initDetails();
		initSubdetails();
		initDetailTable();
		clickDetailRow();

		if(output_type == 'view'){
			$('[name="R_Name"], [name="R_Description"]').attr('readonly', true);
			$('body input[type="checkbox"], [name="chkActive[]"]').prop('disabled', true);
		}
	}

	function initDetailTable(){

		tabledetail = $('#tbl-detail').DataTable({
			responsive:true,
			"ordering"	:false,
			"scrollX"	:true,
			"scrollY"	:"500px",
			"scrollCollapse":true,
			"deferRender": true,
			"columnDefs": [{
				"targets": [1,2,3,4,5],
				"className": 'text-center'
			}],
			"paging":false,
		})

		table_subdetail = $('#tbl-sub-detail').DataTable({
			responsive:true,
			"ordering"	:false,
			"scrollX"	:true,
			"scrollY"	:"400px",
			"scrollCollapse":true,
			"deferRender": true,
			"bInfo"	: false,
			"columnDefs": [{
				"targets": [2],
				"className": 'text-center'
			}],
			"paging":false,
		})
		
	}

	function initDetails(){

		let detail_row = ''

		if(output_type == 'add'){

			$(modules).each(function(index){
				detail_row += 	'<tr class="tbl-detail" data-web-parent="' + modules[index].M_WebParent + '" data-web-level="' + modules[index].M_WebLevel + '" data-module="' + modules[index].M_Module_id + '" data-active="false">' +
									'<td '+(modules[index].M_WebLevel == 2 ? 'style="padding-left: 20px!important"' : (modules[index].M_WebLevel == 3 ? 'style="padding-left: 30px!important"' : (modules[index].M_WebLevel == 4 ? 'style="padding-left: 40px!important"' : '') ) ) +' data-name="RA_FK_Modules_Code" data-value="' + modules[index].M_Module_id + '">' + modules[index].M_DisplayName + '</td>' + 
									'<td class="function-check" data-name="RA_Add" data-value="false">' + '<input type="checkbox" name="chkAdd[]"/>' + '</td>' + 
									'<td class="function-check" data-name="RA_Edit" data-value="false">' + '<input type="checkbox" name="chkEdit[]"/>' + '</td>' + 
									'<td class="function-check" data-name="RA_Print" data-value="false">' + '<input type="checkbox" name="chkPrint[]"/>' + '</td>' + 
									'<td class="function-check" data-name="RA_Delete" data-value="false">' + '<input type="checkbox" name="chkDelete[]"/>' + '</td>' + 
									'<td class="function-check" data-name="RA_View" data-value="false">' + '<input type="checkbox" name="chkView[]"/>' + '</td>' + 
									'<td class="function-check" data-name="RA_Menu" data-value="false">' + '<input type="checkbox" name="chkMenu[]"/>' + '</td>' +
							    '</tr>';
			});

			$('#tbl-detail tbody').append(detail_row);
		}
		else{
			$(modules).each(function(index){
				detail_row += 	'<tr class="tbl-detail" data-web-parent="' + modules[index].M_WebParent + '" data-web-level="' + modules[index].M_WebLevel + '" data-module="' + modules[index].M_Module_id + '" data-active="'+ (modules[index].RA_Menu ? 'true' : 'false') +'">' +
									'<td '+(modules[index].M_WebLevel == 2 ? 'style="padding-left: 20px!important"' : (modules[index].M_WebLevel == 3 ? 'style="padding-left: 30px!important"' : (modules[index].M_WebLevel == 4 ? 'style="padding-left: 40px!important"' : '') ) ) +' data-name="RA_FK_Modules_Code" data-value="' + modules[index].M_Module_id + '">' + modules[index].M_DisplayName + '</td>' + 
									'<td class="function-check" data-name="RA_Add" data-value="'+ (modules[index].RA_Add ? 'true' : 'false') +'">' + '<input type="checkbox" ' + (modules[index].RA_Add ? 'checked' : '') + ' name="chkAdd[]"/>' + '</td>' + 
									'<td class="function-check" data-name="RA_Edit" data-value="' + (modules[index].RA_Edit ? 'true' : 'false') + '">' + '<input type="checkbox" ' + (modules[index].RA_Edit ? 'checked' : '') + ' name="chkEdit[]"/>' + '</td>' + 
									'<td class="function-check" data-name="RA_Print" data-value="' + (modules[index].RA_Print ? 'true' : 'false') + '">' + '<input type="checkbox" ' + (modules[index].RA_Print ? 'checked' : '') + ' name="chkPrint[]"/>' + '</td>' + 
									'<td class="function-check" data-name="RA_Delete" data-value="' + (modules[index].RA_Delete ? 'true' : 'false') + '">' + '<input type="checkbox" ' + (modules[index].RA_Delete ? 'checked' : '') + ' name="chkDelete[]"/>' + '</td>' + 
									'<td class="function-check" data-name="RA_View" data-value="' + (modules[index].RA_View ? 'true' : 'false') + '">' + '<input type="checkbox" ' + (modules[index].RA_View ? 'checked' : '') + ' name="chkView[]"/>' + '</td>' + 
									'<td class="function-check" data-name="RA_Menu" data-value="' + (modules[index].RA_Menu ? 'true' : 'false') + '">' + '<input type="checkbox" ' + (modules[index].RA_Menu ? 'checked' : '') + ' name="chkMenu[]"/>' + '</td>' +
							    '</tr>';
			});

			$('#tbl-detail tbody').append(detail_row);
		}

		checkBoxValue();

	}

	function initSubdetails(){

		let subdetail_row = ''

		if(output_type == 'add'){

			$(functions).each(function(index){
				subdetail_row += 	'<tr class="tbl-sub-detail" style="display: none" data-module-id="'+functions[index].F_FK_Module_id+'">' +
										'<td data-name="RF_FK_ModuleID" data-value="' + functions[index].F_FK_Module_id + '">' + functions[index].M_DisplayName + '</td>' + 
										'<td data-name="RF_FK_FunctionID" data-value="' + functions[index].F_Function_id + '">' + functions[index].F_FunctionName + '</td>' + 
										'<td data-name="RF_Active" data-value="false">' + '<input type="checkbox" name="chkActive[]"/>' + '</td>' + 
								    '</tr>';
			});

			$('#tbl-sub-detail tbody').append(subdetail_row);
		}
		else{
			$(functions).each(function(index){
				subdetail_row += 	'<tr class="tbl-sub-detail" style="display: none" data-module-id="'+functions[index].F_FK_Module_id+'">' +
										'<td data-name="RF_FK_ModuleID" data-value="' + functions[index].F_FK_Module_id + '">' + functions[index].M_DisplayName + '</td>' + 
										'<td data-name="RF_FK_FunctionID" data-value="' + functions[index].F_Function_id + '">' + functions[index].F_FunctionName + '</td>' + 
										'<td data-name="RF_Active" data-value="'+ (functions[index].DispFunc ? true : false) +'">' + '<input type="checkbox" name="chkActive[]" '+ (functions[index].DispFunc ? 'checked' : '') +'/>' + '</td>' + 
								    '</tr>';
			});

			$('#tbl-sub-detail tbody').append(subdetail_row);
		}

		subDetailCheckbox();
	}

	function clickDetailRow(){
		$('#tbl-detail tbody tr').on('click', 'td:eq(0)',function(){
			let current_cell = $(this)
			let current_row = $(this).closest('.tbl-detail');

			$('#tbl-detail tbody tr').removeClass('selected');
			current_row.addClass('selected');

			let chk_Menu 		= current_row.find('[name="chkMenu[]"]').is(':checked');

			if(chk_Menu){
				$('#tbl-sub-detail tbody tr').hide();
				$('#tbl-sub-detail tbody tr[data-module-id="'+current_cell.data('value')+'"]').show();
			}
			else{
				$('#tbl-sub-detail tbody tr').hide();
			}
		})
	}

	function subDetailCheckbox(){
		$('#tbl-sub-detail tbody tr').on('click', '[name="chkActive[]"]', function(){
			let current_row = $(this).closest('.tbl-sub-detail');
			let chk_active = current_row.find('[name="chkActive[]"]').is(':checked');

			if(chk_active){
				current_row.find('[data-name="RF_Active"]').removeAttr('data-value');
				current_row.find('[data-name="RF_Active"]').attr('data-value', true);
			}
			else{
				current_row.find('[data-name="RF_Active"]').removeAttr('data-value');
				current_row.find('[data-name="RF_Active"]').attr('data-value', false);
			}
		})
	}

	function checkBoxValue(){
		$('#tbl-detail tbody tr').on('click', '[name="chkAdd[]"]', function(){
			let current_row = $(this).closest('.tbl-detail');
			let chk_add = current_row.find('[name="chkAdd[]"]').is(':checked');

			if(chk_add){
				current_row.find('[data-name="RA_Add"]').removeAttr('data-value');
				current_row.find('[data-name="RA_Add"]').attr('data-value', 'true');
			}
			else{
				current_row.find('[data-name="RA_Add"]').removeAttr('data-value');
				current_row.find('[data-name="RA_Add"]').attr('data-value', 'false');
			}
		});

		$('#tbl-detail tbody tr').on('click', '[name="chkEdit[]"]', function(){
			let current_row = $(this).closest('.tbl-detail');
			let chk_edit = current_row.find('[name="chkEdit[]"]').is(':checked');

			if(chk_edit){
				current_row.find('[data-name="RA_Edit"]').removeAttr('data-value');
				current_row.find('[data-name="RA_Edit"]').attr('data-value', 'true');
			}
			else{
				current_row.find('[data-name="RA_Edit"]').removeAttr('data-value');
				current_row.find('[data-name="RA_Edit"]').attr('data-value', 'false');
			}
		});

		$('#tbl-detail tbody tr').on('click', '[name="chkDelete[]"]', function(){
			let current_row = $(this).closest('.tbl-detail');
			let chk_Delete = current_row.find('[name="chkDelete[]"]').is(':checked');

			if(chk_Delete){
				current_row.find('[data-name="RA_Delete"]').removeAttr('data-value');
				current_row.find('[data-name="RA_Delete"]').attr('data-value', 'true');
			}
			else{
				current_row.find('[data-name="RA_Delete"]').removeAttr('data-value');
				current_row.find('[data-name="RA_Delete"]').attr('data-value', 'false');
			}
		});

		$('#tbl-detail tbody tr').on('click', '[name="chkView[]"]', function(){
			let current_row 	= $(this).closest('.tbl-detail');
			let chk_View 		= current_row.find('[name="chkView[]"]').is(':checked');

			if(chk_View){
				current_row.find('[data-name="RA_View"]').removeAttr('data-value');
				current_row.find('[data-name="RA_View"]').attr('data-value', 'true');
			}
			else{
				current_row.find('[data-name="RA_View"]').removeAttr('data-value');
				current_row.find('[data-name="RA_View"]').attr('data-value', 'false');
			}
		});

		$('#tbl-detail tbody tr').on('click', '[name="chkMenu[]"]', function(){
			let current_row 	= $(this).closest('.tbl-detail');
			let chk_Menu 		= current_row.find('[name="chkMenu[]"]').is(':checked');
			let web_parent 		= current_row.data('web-parent'); 
			let web_module 		= current_row.data('module'); 
			let web_level 		= current_row.data('web-level');

			if(chk_Menu){
				current_row.find('td.function-check').removeAttr('data-value');
				current_row.find('td.function-check').attr('data-value', true);
				current_row.removeAttr('data-active');
				current_row.attr('data-active', true);
				current_row.find('td input[type="checkbox"]').prop('checked', true);
				let i;
				for(i = web_level; i >= 1; i--){
					$('#tbl-detail tbody tr').each(function(){
						let module_id 		= $(this).find('[data-name="RA_FK_Modules_Code"]').data('value'); 
						let module_parent 	= $(this).data('web-parent'); 

						if(web_parent == module_id){
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').removeAttr('data-active');
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').attr('data-active', true);
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('td input[type="checkbox"]').prop('checked', true);

							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_Add"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_Add"]').attr('data-value', 'true');

							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_Edit"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_Edit"]').attr('data-value', 'true');

							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_Print"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_Print"]').attr('data-value', 'true');
						
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_Delete"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_Delete"]').attr('data-value', 'true');
							
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_View"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_View"]').attr('data-value', 'true');
							
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_Menu"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-module="'+ web_parent +'"]').find('[data-name="RA_Menu"]').attr('data-value', 'true');
							web_parent = module_parent;
						}
					});
				}
			}
			else{
				current_row.find('td.function-check').removeAttr('data-value');
				current_row.find('td.function-check').attr('data-value', false);
				current_row.removeAttr('data-active');
				current_row.attr('data-active', false);
				current_row.find('td input[type="checkbox"]').prop('checked', false);
				$('#tbl-sub-detail tbody tr').hide();
				$('#tbl-sub-detail tbody tr[data-module-id="'+web_module+'"]').find('[name="chkActive[]"]').prop('checked', false);
				// $('#tbl-detail tbody tr[data-web-parent="'+ web_parent +'"]').find('td input[type="checkbox"]').attr('disabled', true);
				let i;
				for(i = 1; i <= 4; i++){
					web_module = current_row.data('module');
					$('#tbl-detail tbody tr').each(function(){ 
						let module_parent 	= $(this).data('web-parent'); 
						let module_id 		= $(this).data('module'); 
						let module_active 	= $(this).attr('data-active'); 

						if((web_module == module_parent) && (module_active == 'true')){
							$('#tbl-detail tbody tr[data-module="'+ web_module +'"]').removeAttr('data-active');
							$('#tbl-detail tbody tr[data-module="'+ web_module +'"]').attr('data-active', false);
							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('td input[type="checkbox"]').prop('checked', false);

							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_Add"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_Add"]').attr('data-value', 'false');

							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_Edit"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_Edit"]').attr('data-value', 'false');

							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_Print"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_Print"]').attr('data-value', 'false');

							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_Delete"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_Delete"]').attr('data-value', 'false');

							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_View"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_View"]').attr('data-value', 'false');

							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_Menu"]').removeAttr('data-value');
							$('#tbl-detail tbody tr[data-web-parent="'+ web_module +'"]').find('[data-name="RA_Menu"]').attr('data-value', 'false');
							web_module = module_id;
							
						}
					});
				}
			}
		});
	}

	init();

})