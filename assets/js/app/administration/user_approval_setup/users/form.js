$(document).ready(function(){

	function init(){


		initExtension();
		initDetails();
		initDetailTable();
		moduleFunctions();

		if(output_type != 'add'){
			$('[name="U_ID"]').attr('readonly', true)
			if(output_type == 'view'){
				$('#form-header :input').attr('readonly', true);
				$('#form-header select').prop('disabled', true);
				$('body input[type="checkbox"], [name="chkActive[]"]').prop('disabled', true);
			}
		}
	}

	init();

});

function initExtension(){
	$('[name="U_FK_Position_id"]').select2({
		placeholder 	: "Select Position",
		allowClear 		: true
	});

	$('[name="DefaultLocation"]').select2({
		placeholder 	: "",
		allowClear 		: true
	});

	$('[name="U_ParentUserID"]').select2({
		placeholder 	: "",
		allowClear 		: true
	});

	$('[name="UR_FK_RoleID[]"]').select2({
		tags: true,
		tokenSeparators: [',', '']
	})
}

function initDetailTable(){
	tabledetail = $('#tbl-detail').DataTable({
		responsive:true,
		"ordering"	:false,
		"scrollX"	:true,
		"scrollY"	:"300px",
		"scrollCollapse":true,
		"deferRender": true,
		"columnDefs": [{
			"targets": [0],
			"className": 'text-center'
		}],
		"paging":false,
	})
}

function initDetails(){

	let detail_row = ''

	if(output_type == 'add'){

		$(location_list).each(function(index){
			detail_row += 	'<tr class="tbl-detail" data-row-state="created" data-location="'+location_list[index].AD_ID+'">' +
									'<td data-name="CA_Active" data-value="false">' + '<input type="checkbox" name="chkActive[]"/>' + '</td>' + 
									'<td data-name="CA_FK_Location_id" data-value="' + location_list[index].AD_ID + '">' + location_list[index].AD_Desc + '</td>' + 
							    '</tr>';
		});

		$('#tbl-detail tbody').append(detail_row);
	}
	else{
		$(location_list).each(function(index){
			detail_row += 	'<tr class="tbl-detail" data-row-state="created" data-location="'+location_list[index].AD_ID+'">' +
									'<td data-name="CA_Active" data-value="'+ (location_list[index].LocAccess ? true : false) +'">' + '<input type="checkbox" name="chkActive[]" '+(location_list[index].LocAccess ? 'checked' : '')+'/>' + '</td>' + 
									'<td data-name="CA_FK_Location_id" data-value="' + location_list[index].AD_ID + '">' + location_list[index].AD_Desc + '</td>' + 
							    '</tr>';
		})

		$('#tbl-detail tbody').append(detail_row);
	}

	detailCheckbox();

}

function detailCheckbox(){

	$('#chkSelectAll').click(function(){
		if($(this).is(':checked')){
			$('[name="chkActive[]"]').prop('checked', true);
			$('#tbl-detail tbody tr').find('[data-name="CA_Active"]').removeAttr('data-value');
			$('#tbl-detail tbody tr').find('[data-name="CA_Active"]').attr('data-value', true);
		}
		else{
			$('[name="chkActive[]"]').prop('checked', false);
			$('#tbl-detail tbody tr').find('[data-name="CA_Active"]').removeAttr('data-value');
			$('#tbl-detail tbody tr').find('[data-name="CA_Active"]').attr('data-value', false);
		}
	})

	$('#tbl-detail tbody tr td').on('click', '[name="chkActive[]"]', function(){
		let current_row = $(this).closest('.tbl-detail');
		let checkedLoc = 0
		let countLoc = $('#tbl-detail tbody tr').length;

		if(current_row.find('[name="chkActive[]"]').is(':checked')){
			current_row.find('[data-name="CA_Active"]').removeAttr('data-value');
			current_row.find('[data-name="CA_Active"]').attr('data-value', true);

			$('#tbl-detail tbody tr').each(function() {
				let checkbox = $(this).find('[name="chkActive[]"]').is(':checked');
 				
				if(checkbox){
					checkedLoc++;
				}
			});
		}
		else{
			current_row.find('[data-name="CA_Active"]').removeAttr('data-value');
			current_row.find('[data-name="CA_Active"]').attr('data-value', false);

			$('#tbl-detail tbody tr').each(function() {
				let checkbox = $(this).find('[name="chkActive[]"]').is(':checked');
 				
				if(checkbox){
					checkedLoc++;
				}
			});
		}

		if(checkedLoc == countLoc){
			$('#chkSelectAll').prop('checked', true);
		}
		else{
			$('#chkSelectAll').prop('checked', false);
		}

		if(output_type == 'update'){
			current_row.removeAttr('data-row-state');
			current_row.attr('data-row-state', 'modified');
		}
	});
}

function moduleFunctions(){
	$('[name="DefaultLocation"]').change(function(){
		let default_loc = $(this).val();

		if($(this).val() != "" || $(this).val() !== null){
			$('#tbl-detail tbody tr[data-location="'+default_loc+'"] td').find('[name="chkActive[]"]').prop('checked', true);
			$('#tbl-detail tbody tr[data-location="'+default_loc+'"]').find('[data-name="CA_Active"]').removeAttr('data-value');
			$('#tbl-detail tbody tr[data-location="'+default_loc+'"]').find('[data-name="CA_Active"]').attr('data-value', true);
		}
	})
}