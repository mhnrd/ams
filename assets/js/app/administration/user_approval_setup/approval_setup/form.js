$(document).ready(function(){

	function initDetailTable(){
		tabledetail = $('.dataTables-example').DataTable({
			responsive:true,
			"ordering"	:false,
			"scrollX"	:true,
			"scrollY"	:"300px",
			"scrollCollapse":true,
			"deferRender": true,
			"columnDefs": [{
				"targets": [0,1],
				"className": 'text-left'
			}],
			"paging":false,
		})
	}

	function initDetails(){

		let detail_row = ''

		$(modules).each(function(index){
			detail_row += 	'<tr class="tbl-detail">' +
								'<td data-name="NS_Id" data-value="' + modules[index].NS_Id + '">' + modules[index].NS_Id + '</td>' +
								'<td data-name="NS_Description" data-value="' + modules[index].NS_Id + '">' + modules[index].NS_Description + '</td>' +  
						    '</tr>';
		});

		$('.dataTables-example tbody').append(detail_row);

	}

	$('.js-select2').select2({
		placeholder: 'Select Position ID Here',
		allowClear: true
	});

	// $('[name="AS_FK_Position_id"]').change(function(){
 //      	var displayuser = $('[name="AS_FK_Position_id"]').val();

 //      	if($(this).val() == ''){
 //      		$('[name="P_Type"]').val('');
 //      	}
 //      	else{
 //      		$('[name="P_Type"]').val(displayuser);
 //      	}

 //      });

 	$('select[name=AS_FK_Position_id]').on('change',function(){
		$.ajax({
				type: 'GET',
				dataType: 'json',
				url: global.site_name + 'administration/master_file/numberseries/getNextAvailableNumber',
				data : {'location' : $(this).val() , 'ns_id' : number_series },
				success : function(result){					
					if(result.success == 1){
						$('input[name="P_Type"]').attr('value',result.message);
					}
					else if(result.success == 0){
						swal(result.message, "Please contact your System Administrator.", "error")
						$('input[name="P_Type"]').attr('value','');
					}
				}
			});
	});
	
	initDetails();
	initDetailTable();
});