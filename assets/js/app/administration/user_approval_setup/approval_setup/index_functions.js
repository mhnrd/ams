
function InitializeClickEvent(){
	// clear color value 
	$('#approval_setup > tbody > tr button').removeClass('btn-default').addClass('btn-primary').css('color', '');
	$('#approval_setup > tbody > tr').find('[data-employee]').css('color', '');

    if ( $(_this).hasClass('selected') ) {
        $(_this).removeClass('selected');
        $(_this).find('button').removeClass('btn-default').addClass('btn-primary');
        selected_employee_id = '';
         $('#btnAddEntry').addClass('invisible');
    }
    else {
        tableindex.$('tr.selected').removeClass('selected');
        $(_this).addClass('selected');
        $(_this).find('[data-employee]').css('color', 'white');
        $(_this).find('button').removeClass('btn-primary').addClass('btn-default').css('color', 'white');
        selected_employee_id = $(_this).find('[data-employee-id]').data('employee-id');
         $('#btnAddEntry').removeClass('invisible');
         $('#btnAddEntry').removeAttr('data-doc-no');
         $('#btnAddEntry').attr('data-doc-no', $(_this).find('td:eq(0)').text());
    }
   

    let employee_id = '';
    if(selected_employee_id != ""){
    	employee_id = $(_this).find('[data-employee-id]').data('employee');
    }
    
    // table on change trigger
    tabledetail.ajax.url(  global.site_name + 'administration/user_approval_setup/approval_setup/data_detail?id=' + employee_id ).load();
	
}

function getCurrentApprovers(){

    let approvers = [];

    if(tabledetail.rows().count() > 0){        
        $('#tbl-detail tbody tr').each(function() {
            let approver_data = $(this).find('[data-record]').data('record');
            let apvr = approver_data['AS_FK_Position_id'];

            approvers.push(apvr);
        });
    }

    return approvers;
}


function InitializeModal_Add_Entry(mode){

    $('[name="AS_FK_Position_id"]').select2({
            placeholder:    "Select Position",
            allowClear:     true,
            // minimumInputLength: 1,
            ajax:
                {
                    url: global.site_name + 'administration/user_approval_setup/approval_setup/getAllPositions',
                    dataType: 'json',
                    delay: 250,
                    minimumInputLength : 1,
                    data: function (params) {
                      var queryParameters = {
                                'filter-type'       : 'P_Position'
                            ,   'filter-search'     : params.term
                            ,   'filter-approvers'  : getCurrentApprovers()
                        }
                        return queryParameters;
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function(cat) {
                                return {
                                    id: cat.P_ID,
                                    text:cat['P_Position']
                                }
                            })
                        };
                    }
                }
            });


        $('[name="AS_FK_Position_id"]').change(function(){
            let category_code = $(this).val();

            if(category_code == ''){
                $('[name="P_Type"]').val('');
            }
            else{
                $('[name="P_Type"]').val($('[name="AS_FK_Position_id"]').val())
            }
        });


    $('#detail-template-ap [name="mode"]').val(mode);
    if(approval_data.length > 0){
        $('#detail-template-ap [name="AS_Sequence"]').val(approval_data['AS_Sequence']);
    }

    $('body').on('click', '#detail-template-ap #btnSaveandClose ,#btnSaveandNew', function(e){
        console.log($(this).attr('id'));
        e.preventDefault();
        btnValue = $(this).val();
        InitializeSaveTimeEntries(btnValue);
    })



}

function InitializeDeleteData(NSID, PID){
    swal({
                title: "Are you sure?",
                text:  "This data will be deleted",
                type:  "warning",
                showCancelButton:  true,
                confirmButtonColor: "#1AB394",
                confirmButtonText:  "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true
        }, function(isConfirm){
            if(isConfirm){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: global.site_name + controller_url + 'delete',
                    data: { 
                        'ns-id'    : NSID,
                        'p-id'     : PID
                    },
                    success: function(data){
                        if(data.success){
                            // swal({
                            //     title: "Delete Success",
                            //     type:  "success"
                            // });
                            tabledetail.ajax.reload();
                        }else{
                            swal({
                                title: data.title,
                                text:  data.message,
                                type:  data.type
                            });
                        }
                    },
                    error : function(){
                        swal("Error", "Please contact your system administrator", "error")
                    }
                });
            }
        });
}

// save/update TimeEntries
function InitializeSaveTimeEntries(btnValue){

    let blnstatus = true
    var type = '';
    if(btnValue == 'SaveandClose' || btnValue == 'SaveandNew'){
        type = 'add';
    }
    else{
        type = 'update';
    }

    let id_data = $('#form-approval-setup').find(":input:not(:hidden)").serializeArray();
    let hidden = $('#form-approval-setup').find(".hidden").serializeArray();
    let _validate = true;

    $(id_data).each(function(index){
        if(id_data[index].value == ""){
            console.log(id_data[index]);
            _validate = true;
            blnstatus = false;
        }
    });

    $(hidden).each(function(index){
        if(id_data[index].value == ""){
            console.log(id_data[index]);
            _validate = false;
            blnstatus = false;
        }
    });

    hidden_data = $('#form-approval-setup').find(':input[type="hidden"]').serializeArray()
    id_data = id_data.concat(hidden_data);

    hidden_data1 = $('#form-approval-setup').find('.hidden').serializeArray()
    hidden = hidden.concat(hidden_data1);

    if($('[name="AS_Amount"]').val() == ''){
        $('[name="AS_Amount"]').val(0);
    }

    if(!$('[name="AS_Required"]').is(':checked')) {
        id_data.push({
            name    : "AS_Required",
            value   : "0"
        })
    }

    if(!$('[name="AS_Unlimited"]').is(':checked')) {
        id_data.push({
            name    : "AS_Unlimited",
            value   : "0"
        })
    }

    console.log(_validate)
    if(!_validate){
        swal("Information", "Complete all fields to proceed", "info");
        blnstatus = false;
        $('.required-label').show();
    }
    else{
        swal({
                title: "Are you sure?",
                text: type == 'add' ? "This will save the record?" : "This will update the record?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#1AB394",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
            }, function () {
                            
                    console.log(id_data,"Approval Setup");
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: global.site_name + 'administration/user_approval_setup/approval_setup/saveTimeEntries',
                        data : id_data,
                        success : function(result){
                            if(!result.success){
                                swal(result.title, result.message, result.type);
                                blnstatus = false;
                            }
                            else{
                                if(btnValue == 'SaveandNew'){
                                    $('#detail-template-ap input').val('');
                                    $('#detail-template-ap select').select2("val",'');
                                    $('#detail-template-ap [name="mode"]').val('add');
                                }
                                else{
                                    $('#detail-template-ap input').val('');
                                    $('#detail-template-ap select').select2("val",'');   
                                    $('#detail-template-ap .close').trigger('click');                                    
                                }
                                // if(btnValue == 'SaveandClose'){
                                //     $('#detail-template-ap input').val('');
                                //     $('#detail-template-ap select').val('').trigger('change');
                                //     $('#detail-template-ap').modal('hide');
                                //     $('.required-label').hide();
                                // }
                                tabledetail.ajax.reload();

                            }
                        },
                        error       : function(jqXHR){
                            swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
                        }
                    });
                    // end of saving POST
                }
       );
    }
    
    return blnstatus;
}