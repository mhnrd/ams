var selected_employee_id = '';
var approval_data = [];
var _this;
$(document).ready(function(){

	function init(){
		initHeaderTable();

		 // For Add Entry Modal
	    $('#btnAddEntry').click(function(e){
	    	let ns_docno = $(this).attr('data-doc-no')
	        $('[name="AS_Amount"]').val(0);
	        $('[name="AS_FK_NS_id"]').val(ns_docno);
	        // $('#btnSaveandClose').text('Save');
	        $("#detail-template-ap").modal('show');
	        InitializeModal_Add_Entry('add');
	    });

	    //Edit button
	    $('#tbl-detail').on('click','.button-edit',function(e){
		    approval_data = $(this).data('record');

		    let pos_id_sel = '';
		    pos_id_sel += '<option value="'+approval_data['AS_FK_Position_id']+'">'+approval_data['P_Position']+'</option>'
		    $('[name="AS_FK_Position_id"]').empty();
		    $('[name="AS_FK_Position_id"]').append(pos_id_sel);

		    $('[name="AS_Amount"]').val(number_format(approval_data['AS_Amount'], 2, ".", ""));
	        $('[name="AS_FK_NS_id"]').val(approval_data['AS_FK_NS_id']);
	        $('[name="AS_Sequence"]').val(approval_data['AS_Sequence']);
	        
	        // AS_Unlimited checkbox
	        if(approval_data['AS_Unlimited']){
	        	$('[name="AS_Unlimited"]').prop('checked', true);
	        }
	        else{
	        	$('[name="AS_Unlimited"]').prop('checked', false);
	        }

	        // AS_Required checkbox
	        if(approval_data['AS_Required']){
	        	$('[name="AS_Required"]').prop('checked', true);
	        }
	        else{
	        	$('[name="AS_Required"]').prop('checked', false);
	        }

		    $("#detail-template-ap").modal('show');
		    // $('#btnSaveandClose').text('Update');
		    InitializeModal_Add_Entry('update');
	    });

	     //Delete
	    $('body').on('click','.button-delete',function(e){
		    e.preventDefault();
		    approval_data = $(this).data('record');
		    InitializeDeleteData(approval_data['AS_FK_NS_id'], approval_data['AS_FK_Position_id']);
	    });

		$('#approval_setup tbody').on( 'click', 'tr', function () {
			_this = $(this);
			InitializeClickEvent();
   		});
	}

	init();
	initDetailTable();
});

function initHeaderTable(){

		// Datatable Setup for employee
		tableindex = $('#approval_setup').DataTable( {
			"processing"	: true,
			"serverSide"	: true,
			"responsive"	: true,
			"order":[],
			"ajax":{
				url: global.site_name + 'administration/user_approval_setup/approval_setup/data',
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[0,1],
				"orderable": false,
			},{
				"targets": [],
				"className": 'text-center'
			}],
			"paging": false,
			"scrollX": true,
			scrollY: 500,
  			scrollCollapse: true,
			"lengthChange": false,
			//"info": false,
			"order": [[ 1, "asc" ]],
			createdRow:function(row){
				$(row).addClass('approval_setup');
			},
			"dom": 'lT<>fgtip'	
		});

}


function initDetailTable(){
		tabledetail = $('#tbl-detail').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/user_approval_setup/approval_setup/data_detail',
			method: "POST",
		},	
		"deferRender": true,
		"columnDefs": [{
						"targets":[0],
						"orderable": false,
						},{
							"targets": [0,1,4,5],
							"className": 'text-center'
						},{
							"targets": [3],
							"className": 'text-right'
						}
		],
		createdRow:function(row){
			$(row).addClass('tbl-detail');
		},
		"order": [[ 1, "asc" ]],
		"paging": false,
		"dom": 'lT<"">fgtip'
	});

}