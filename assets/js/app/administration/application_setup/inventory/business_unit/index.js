$(document).ready(function(){

	InitHeaderTable();
	// activate_deactivate_button();

});

function InitHeaderTable(){

	// Datatable Setup
	tableindex = $('#'+table_id).DataTable( {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/application_setup/inventory/business_unit/data',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		},{
			"targets": col_center,
			"className": 'text-center'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	$('#'+table_id+'_paginate').addClass('pull-right')

}