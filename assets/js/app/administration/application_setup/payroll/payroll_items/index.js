$(document).ready(function(){

	

	//DataTable Setup
	tableindex = $('#payroll-items-details').DataTable({
		responsive: true,
		"processing": true,
		"serverSide": true,
		"order":[],
		"ajax":{
				url:global.site_name + 'administration/application_setup/payroll/payroll_items/data',
				type:"POST" 
		},
		"deferRender": true,
		"columnDefs": [{
			"targets": [0, 7],
			"orderable": false,
		},{
			"targets": [0,7],
			"className": 'text-center'
		},{
			"targets": [5,6],
			"className": 'text-right'
		}],
		pageLength: 10,
        responsive: true,
        createdRow:function(row){
			$(row).addClass('breakdown-detail');
        },
		"dom": 'lT<"dt-toolbar">fgtip'
	});
	$('#'+table_id+'_paginate').addClass('pull-right');
})