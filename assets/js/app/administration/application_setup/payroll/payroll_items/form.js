$(document).ready(function(){

	if (output_type == 'view') {
		$('[name="PI_ID"]').attr('readonly', true);
		$('[name="PI_Desc"]').attr('readonly', true);
		$('[name="PI_Type"]').prop("disabled",true);
		$('[name="PI_Basis2Compute"]').prop("disabled",true);
		$('[name="PI_Sperate"]').attr('readonly', true);
		$('[name="PI_Regrate"]').attr('readonly', true);
		$('[name="PI_Taxable"]').attr('disabled', true);
		$('[name="PI_Allowance"]').attr('disabled', true);
		$('[name="PI_CutoffType"]').attr("style", "-webkit-appearance: none; pointer-events: none;background: #E9ECEF").removeClass('pi-cutofftype-js-select2');
		$('[name="PI_Cutoff"]').attr("style", "-webkit-appearance: none; pointer-events: none;background: #E9ECEF").removeClass('pi-cutoff-js-select2');
		$('[name="PI_AttAllowTypeId"]').attr("style", "-webkit-appearance: none; pointer-events: none;background: #E9ECEF").removeClass('allowancetype-js-select2');
		$('[name="PI_AttendanceBasis"]').attr('disabled', true);
		$('[name="PI_Variability"]').attr("style", "-webkit-appearance: none; pointer-events: none;background: #E9ECEF").removeClass('pi-variability-js-select2');
		$('[name="PI_Divisor"]').attr('readonly', true);
		if ($('[name="PI_Allowance"]').is(':checked')) {
				$("#formhides").show();
		        $("#formhides1").show();
		        $("#formhides2").show();
		        $("#formhides3").show();
		        $("#formhides4").show();
		        $("#formhides5").show();
			}else{
				$("#formhides").hide();
		        $("#formhides1").hide();
		        $("#formhides2").hide();
		        $("#formhides3").hide();
		        $("#formhides4").hide();
		        $("#formhides5").hide();
		}
	}

	if (output_type == 'add' || 'update') {


		$('[name="PI_ID"]').attr('readonly', true);

		$('.pitype-js-select2').select2({
			placeholder: '',
			allowClear: true,
		});
		$('.basis2compute-js-select2').select2({
			placeholder: '',
			allowClear: true,
		});
		$('.pi-cutoff-js-select2').select2({
			placeholder: '',
			allowClear: true,
		});
		$('.pi-cutofftype-js-select2').select2({
			placeholder: '',
			allowClear: true,
		});
		$('.allowancetype-js-select2').select2({
			placeholder: '',
			allowClear: true,
		});
		$('.pi-variability-js-select2').select2({
			placeholder: '',
			allowClear: true,
		});
	}

	$('[name="PI_Divisor"]').on("keypress", function (evt) {
	    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
	    {
	        evt.preventDefault();
	    }
	});
	$('[name="PI_Sperate"]').on("keypress", function (evt) {
	    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
	    {
	        evt.preventDefault();
	    }
	});
	$('[name="PI_Regrate"]').on("keypress", function (evt) {
	    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
	    {
	        evt.preventDefault();
	    }
	});
    $('[name="PI_Divisor"]').on('keyup keydown', function(e){
    console.log($(this).val() > 100)
        if ($(this).val() > 100 
            && e.keyCode !== 46
            && e.keyCode !== 8
           ) {
           e.preventDefault();     
           $(this).val('99');
        }
    });
    
    if (output_type == "add") {
    	$("#box").hide();

		 $('[name="PI_Allowance"]').click(function(){
            if($(this).is(":checked")){
                $("#formhides").show();
		        $("#formhides1").show();
		        $("#formhides2").show();
		        $("#formhides3").show();
		        $("#formhides4").show();
		        $("#formhides5").show();
		        $("#box").show();
            }
            else if($(this).is(":not(:checked)")){
                $("#formhides").hide();
		        $("#formhides1").hide();
		        $("#formhides2").hide();
		        $("#formhides3").hide();
		        $("#formhides4").hide();
		        $("#formhides5").hide();
		        $("#box").hide();
            }
        });
    	
    }
     if (output_type == "update" || "view") {
     	

			if ($('[name="PI_Allowance"]').is(':checked')) {
				$("#formhides").show();
		        $("#formhides1").show();
		        $("#formhides2").show();
		        $("#formhides3").show();
		        $("#formhides4").show();
		        $("#formhides5").show();
		        $("#box").show();
				    $('[name="PI_Allowance"]').click(function(){
				        if($(this).is(":not(:checked)")){
		                $("#formhides").hide();
				        $("#formhides1").hide();
				        $("#formhides2").hide();
				        $("#formhides3").hide();
				        $("#formhides4").hide();
				        $("#formhides5").hide();
				        $("#box").hide();
				        $('[name="PI_CutoffType"]').val('').trigger('change');
				        $('[name="PI_Cutoff"]').val('').trigger('change');
				        $('[name="PI_AttAllowTypeId"]').val('').trigger('change');
				        $('[name="PI_AttendanceBasis"]').prop("checked", false);
				        $('[name="PI_Variability"]').val('').trigger('change');
				        $('[name="PI_Divisor"]').val('0');
		            }
		            else if($(this).is(":checked")){
		                $("#formhides").show();
				        $("#formhides1").show();
				        $("#formhides2").show();
				        $("#formhides3").show();
				        $("#formhides4").show();
				        $("#formhides5").show();
				        $("#box").show();
		            }
		        });
			}else{
				$("#formhides").hide();
		        $("#formhides1").hide();
		        $("#formhides2").hide();
		        $("#formhides3").hide();
		        $("#formhides4").hide();
		        $("#formhides5").hide();
		       $("#box").hide();
		       $('[name="PI_CutoffType"]').val('').trigger('change');
		       $('[name="PI_Cutoff"]').val('').trigger('change');
		       $('[name="PI_AttAllowTypeId"]').val('').trigger('change');
		       $('[name="PI_AttendanceBasis"]').prop("checked", false);
		       $('[name="PI_Variability"]').val('0');
				    $('[name="PI_Allowance"]').click(function(){
		            if($(this).is(":checked")){
		                $("#formhides").show();
				        $("#formhides1").show();
				        $("#formhides2").show();
				        $("#formhides3").show();
				        $("#formhides4").show();
				        $("#formhides5").show();
				        $("#box").show();
		            }
		            else if($(this).is(":not(:checked)")){
		                $("#formhides").hide();
				        $("#formhides1").hide();
				        $("#formhides2").hide();
				        $("#formhides3").hide();
				        $("#formhides4").hide();
				        $("#formhides5").hide();
				        $("#box").hide();
				        $('[name="PI_CutoffType"]').val('').trigger('change');
				        $('[name="PI_Cutoff"]').val('').trigger('change');
				        $('[name="PI_AttAllowTypeId"]').val('').trigger('change');
				        $('[name="PI_AttendanceBasis"]').prop("checked", false);
				        $('[name="PI_Variability"]').val('0');
		            }
		        });
			}
    	}
	
})