$(document).ready(function(){
	function init(){

		InitHeaderTable();
		InitDetailTable();

		}

	init();
})

function InitHeaderTable(){
	//DataTable Setup
	tableindex = $('#paygroup').DataTable( {
		"processing": true,
		"serverSide": true,
		"order"		:[],
		"ajax"		:{
				url	: global.site_name + 'administration/application_setup/payroll/pay_group/data',
				type:"POST" 
		},
		"deferRender" 	: true,
		"columnDefs"  	: [{
			"targets" 	: [0],
			"orderable"	: false,
		},{
			"targets" 	: [0],
			"className"	: 'text-center'
		}],
		pageLength 		: 10,
		responsive 		: true,
		"order"			: [[1, "desc"]],
		createdRow:function(row){
			$(row).addClass('paygroup_header');
		},
		"dom": 'lT<"dt-toolbar">fgtip<"pull-right">'
	});

	$('#'+table_id+'_paginate').addClass('pull-right');

}	
function InitDetailTable(){
    $('#paygroup').on('click','tr',function(){

    	let table = $('#pay_group_detail').DataTable({destroy: true}); 	
		table.destroy();
		$('#pay_group_detail tbody tr').remove();

    	let current_row = $(this).closest('.paygroup_header');
    	let paygroup_id = current_row.find('td:eq(2)').text();
    	//console.log(current_row.find('td:eq(2)').text());
    	//return;
    	$("#paygroup tbody tr").removeClass('row_selected');		
		$(this).addClass('row_selected');
		
    	let dtl_table 	= '';
		dtl_table = $('#pay_group_detail').DataTable({
			"processing": true,
			"serverSide": true,
			"order":[],
			"ajax":{
				url: global.site_name + 'administration/application_setup/payroll/pay_group/data_PGD?id='+paygroup_id,
				type: "POST"
			},
			"deferRender": true,
			pageLength 		: 10,
			responsive 		: true,
			"order"			: [[1, "desc"]],
			createdRow:function(row){
				$(row).addClass('paygroup_detail');
			},
			"dom": 'lT<"dt-toolbar">fgtip<"pull-right">'
		});	

    });
}

	
	
