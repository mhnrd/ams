$(document).ready(function(){


if(output_type == 'view'){
		$('[name="G_ID"]').css('background', '#E9ECEF');
		$('[name="G_FK_CompanyId"]').attr('readonly', true);
		$('[name="G_Desc"]').attr('readonly', true);
		$('[name="G_PAY_WorkDays"]').attr('readonly', true);
		$('[name="G_PAY_BankAccount"]').attr('readonly', true);
		$('[name="COM_PAY_TaxDeduct"]').prop("disabled",true);
		$('[name="G_PAY_TaxCuttoff"]').prop("disabled",true);
		$('[name="G_PAY_SSSDeduct"]').prop("disabled",true);
		$('[name="G_PAY_SSSCutoff"]').prop("disabled",true);
		$('[name="G_PAY_PagibigDeduct"]').prop("disabled",true);
		$('[name="G_PAY_PagibigCutoff"]').prop("disabled",true);
		$('[name="G_PAY_PhilhealthDeduct"]').prop("disabled",true);
		$('[name="G_PAY_PhilhealthCutoff"]').prop("disabled",true);
			
		}

	function init(){

		initExtension();
		initDetailTable();
		tableButtons();
		addButtons();

		if(output_type == 'update' || 'view'){

			$(details).each(function(index){
				$('[name="G_ID"]').attr('readonly', true);

				let temp = tabledetail.row.add([
				'<button class="btn btn-outline btn-xs edit-detail btn-primary"><i class="fa fa-pencil"></i></button>' + 
				'<button class="btn btn-outline btn-xs delete-detail btn-danger" style="margin-left: 5px"><i class="fa fa-trash"></i></button>',
				details[index].GA_FK_User_Id,
				details[index].U_AssociatedEmployeeId,
				details[index].U_Username,
				]).node();

				$(temp).attr("data-row-state", "saved");
			})

		}

		$('[name="G_PAY_SSSDeduct"]').change(function(){
			if($('[name="G_PAY_SSSDeduct"]').val() != ''){
				if($('[name="G_PAY_SSSDeduct"]').val() == 'M'){
					$('[name="G_PAY_SSSCutoff"]').val('1').trigger('change');
					$('[name="G_PAY_SSSCutoff"]').prop('disabled', true);
				}else{
					$('[name="G_PAY_SSSCutoff"]').prop('disabled', false);
				}
			}
		});

		$('[name="G_PAY_SSSDeduct"]').change(function(){
			if($('[name="G_PAY_SSSDeduct"]').val() != ''){
				if($('[name="G_PAY_SSSDeduct"]').val() == 'S'){
					 $("option[value='3']").prop('disabled',true);
					 $("option[value='4']").prop('disabled',true);
				}else{
					$("option[value='3']").prop('disabled',false);
					$("option[value='4']").prop('disabled',false);
					$('#G_PAY_SSSCutoff').select2({
						placeholder: "SSS Deduction CutOff",
						allowClear: true,
					});
				}
			}
		});


		$('[name="G_PAY_PagibigDeduct"]').change(function(){
			if($('[name="G_PAY_PagibigDeduct"]').val() != ''){
				if($('[name="G_PAY_PagibigDeduct"]').val() == 'M'){
					$('[name="G_PAY_PagibigCutoff"]').val('1').trigger('change');
					$('[name="G_PAY_PagibigCutoff"]').prop('disabled', true);
				}else{
					$('[name="G_PAY_PagibigCutoff"]').prop('disabled', false);
				}
			}
		});

		$('[name="G_PAY_PagibigDeduct"]').change(function(){
			if($('[name="G_PAY_PagibigDeduct"]').val() != ''){
				if($('[name="G_PAY_PagibigDeduct"]').val() == 'S'){
					 $("option[value='3']").prop('disabled',true);
					 $("option[value='4']").prop('disabled',true);
				}else{
					$("option[value='3']").prop('disabled',false);
					$("option[value='4']").prop('disabled',false);
					$('#G_PAY_PagibigCutoff').select2({
						placeholder: "Pagibig Deduction CutOff",
						allowClear: true,
					});
				}
			}
		});

		$('[name="G_PAY_PhilhealthDeduct"]').change(function(){
			if($('[name="G_PAY_PhilhealthDeduct"]').val() != ''){
				if($('[name="G_PAY_PhilhealthDeduct"]').val() == 'M'){
					$('[name="G_PAY_PhilhealthCutoff"]').val('1').trigger('change');
					$('[name="G_PAY_PhilhealthCutoff"]').prop('disabled', true);
				}else{
					$('[name="G_PAY_PhilhealthCutoff"]').prop('disabled', false);
				}
			}
		});

		$('[name="G_PAY_PhilhealthDeduct"]').change(function(){
			if($('[name="G_PAY_PhilhealthDeduct"]').val() != ''){
				if($('[name="G_PAY_PhilhealthDeduct"]').val() == 'S'){
					 $("option[value='3']").prop('disabled',true);
					 $("option[value='4']").prop('disabled',true);
				}else{
					$("option[value='3']").prop('disabled',false);
					$("option[value='4']").prop('disabled',false);
					$('#G_PAY_PhilhealthCutoff').select2({
						placeholder: "Pagibig Deduction CutOff",
						allowClear: true,
					});
				}
			}
		});



	}

	init();

});
	


function tableButtons(){
	$('.add-detail').click(function(){

		action = 'add';

		actionButtons(action);
		
		$('[name="U_Username"]').val('');
		$('[name="U_AssociatedEmployeeId"]').val('')
		$('[name="GA_FK_User_Id"]').val('').trigger('change');
		$('#detail-template').modal('show');


	});
	if (output_type == 'add' || 'update') {
	$('#tbl-detail').on('click', '.edit-detail', function(){
		let current_row = $(this).closest('.tbl-detail');

		let user_id 	= current_row.find('[data-name="GA_FK_User_Id"]').attr('data-value');
		let emp_id 	= current_row.find('td:eq(2)').text();
		let username 	= current_row.find('td:eq(3)').text();
		 
		 line_no	 = current_row.index();

		console.log(user_id)

		action = 'edit';

		actionButtons(action);

		
		$('[name="U_Username"]').val(username);
		$('[name="U_AssociatedEmployeeId"]').val(emp_id);
		$('[name="GA_FK_User_Id"]').val(username).trigger('change');

		$('#detail-template').modal('show');
	});
	}

	$('#tbl-detail').on('click', '.delete-detail', function(){
		let current_row = tabledetail.row($(this).closest('.tbl-detail'));
		
		swal({
			title 				: "Delete this row?",
			type 				: "warning",
			showCancelButton	: true,
			cancelButtonText 	: "No",
			cancelButtonColor 	: '#DD6B55',
			confirmButtonColor 	: '#1AB394',
			confirmButtonText 	: "Yes",
			closeOnConfirm 		: true
		}, function(isConfirm){
			if(isConfirm){
				if(output_type == 'add'){
			    	current_row.remove().draw();
				}
				else{
					$(current_row.node()).attr("data-row-state", "deleted");
					$(current_row.node()).hide();
				}
			}
		})
	});
}

function initExtension(){

	if(output_type == 'add' || 'update'){
			$('.js-select2').select2({
				placeholder: '',
				allowClear: true,
			});
			$(".company-js-select2").select2({
				placeholder: '',
				allowClear: true,
			});
			$(".taxdeduction-js-select2").select2({
				placeholder: '',
				allowClear: true,
			});
			$(".taxdeduction-cutoff-js-select2").select2({
				placeholder: '',
				allowClear: true,
			});
			$(".sssdeduction-js-select2").select2({
				placeholder: '',
				allowClear: true,
			});
			$(".sssdeduction-cutoff-js-select2").select2({
				placeholder: '',
				allowClear: true,
			});
			$(".pagibigdeduction-js-select2").select2({
				placeholder: '',
				allowClear: true,	
			});
			$(".pagibigdeduction-cutoff-js-select2").select2({
				placeholder: '',
				allowClear: true,
			});
			$(".philhealthdeduction-js-select2").select2({
				placeholder: '',
				allowClear: true,
			});
			$(".philhealthdeduction-cutoff-js-select2").select2({
				placeholder: '',
				allowClear: true,
			});
			$("#user-id").select2({
				placeholder: '',
				allowClear: true,
			});
	}
	


	$('[name="GA_FK_User_Id"]').select2({
		placeholder: 	"",
		allowClear: 	true,
		minimumInputLength: 3,
		ajax:
			{
				url: global.site_name + 'administration/application_setup/payroll/pay_group/getUser',
				dataType: 'json',
                delay: 250,
                data: function (params) {
                  var queryParameters = {
                            'filter-type'   : 'U_ID'
                        ,   'filter-search' : params.term
                    }
                    return queryParameters;
                },
                processResults: function (data, params) {
                    return {
                        results: $.map(data, function(cat) {
                            return {
                                id: cat.U_Username,
                                text:cat['U_ID']
                            }
                        })
                    };
                }
			}
	});

	$('[name="GA_FK_User_Id"]').change(function(){
		let category_code = $(this).val();

		if(category_code == ''){
			$('[name="U_Username"]').val('');
		}
		else{
			$('[name="U_Username"]').val($('[name="GA_FK_User_Id"]').val())
			$('[name="U_AssociatedEmployeeId"]').val($('[name="GA_FK_User_Id"]').val())
		}
	});
}



function initDetailTable(){

	if(output_type != 'view'){
		tabledetail = $('#tbl-detail').DataTable( {
			"scrollX"	:true,
			"scrollY"	:"200px",
			"scrollCollapse":true,
			"ordering": false,
			"paging": false,
			"columnDefs": [{
				"targets": [0],
				"className": 'text-center'
			},{
				"targets": [2],
				"className": 'text-left'
			},{
				  "targets":[1],  
				  "createdCell": function(td, cellData, rowData, row, col){
				  	$(td).attr('data-name', 'GA_FK_User_Id');
				  	$(td).attr('data-value', cellData);
				  }
			}],
			responsive: true,
			createdRow:function(row){
				$(row).addClass('tbl-detail');
			},
			"dom": 'lT<"dt-toolbar">fgtip'
		});
	}
	else{
		tabledetail = $('#tbl-detail').DataTable( {
			"processing":true,
			"serverSide":true,
			"scrollX"	:true,
			"scrollY"	:"200px",
			"scrollCollapse":true,
			"ajax": {
				url 	: global.site_name + 'administration/application_setup/payroll/pay_group/data_detail?id='+doc_no,
				type 	: 'POST'
			},
			"deferRender": true,
			"columnDefs": [{
				"targets": [1],
				"className": 'text-center'
			}],
			responsive: true,
			createdRow:function(row){
				$(row).addClass('tbl-detail');
			},
			"dom": 'lT<"dt-toolbar">fgtip'
		});
	}

}

function actionButtons(action){

	if(action == 'add'){
		$('#add-close').text('Add & Close');
		$('#add-new').show();
	}
	else{
		$('#add-close').text('Update');
		$('#add-new').hide();
	}

}

function addButtons(){

	$('#add-new').click(function(){
		let user_id 		= $('[name="GA_FK_User_Id"] option:selected').text();
		let username  		= $('[name="U_Username"]').val();
		let emp_id  		= $('[name="U_AssociatedEmployeeId"]').val();

		if(validate_details()){
			addDetail(user_id, username,emp_id);
			$('[name="U_AssociatedEmployeeId"]').val('');
			$('[name="GA_FK_User_Id"]').val('').trigger('change');
		}
	});

	$('#add-close').click(function(){
		let user_id 	= $('[name="GA_FK_User_Id"] option:selected').text();
		let username  	= $('[name="U_Username"]').val();
		let emp_id  		= $('[name="U_AssociatedEmployeeId"]').val();

		if(validate_details()){
			if(action == 'add'){
				addDetail(user_id, username,emp_id);
				$('#detail-template').modal('hide');
			}
			else{
				editDetail(line_no, user_id, username,emp_id);
				$('#detail-template').modal('hide');
			}
		}
	});

}

function addDetail(user_id, username,emp_id){
	let temp = tabledetail.row.add([
		'<button class="btn btn-outline btn-xs edit-detail btn-primary"><i class="fa fa-pencil"></i></button>' + 
		'<button class="btn btn-outline btn-xs delete-detail btn-danger" style="margin-left: 5px"><i class="fa fa-trash"></i></button>',
		user_id,
		emp_id,
		username
		]).node();

	$(temp).attr("data-row-state", "created");

	tabledetail.row().draw();
	tabledetail.draw();
}

function editDetail(line_no, user_id, username,emp_id){
	let temp = tabledetail.row(line_no).data([
		'<button class="btn btn-outline btn-xs edit-detail btn-primary"><i class="fa fa-pencil"></i></button>' + 
		'<button class="btn btn-outline btn-xs delete-detail btn-danger" style="margin-left: 5px"><i class="fa fa-trash"></i></button>',
		user_id,
		emp_id,
		username,
		]).node();

	if(output_type == 'update'){
		$(temp).attr("data-row-state", "modified");
	}
	else{
		$(temp).attr("data-row-state", "created");
	}
}


function validate_details(){
	let isComplete = true;
	let requiredField = $('#detail-template :input[required]:visible');

	$(requiredField).each(function(index, el) {
		if($(this).val() == "" || $(this).val() == 0){
			isComplete = false;
		}		
	});

	if(action == 'add'){
		$('#tbl-detail tbody tr:visible').each(function(){
			if($('[name="GA_FK_User_Id"] option:selected').text() == $(this).find('td:eq(1)').text()){
				isComplete = false;
			}
		});
	}

	return isComplete;

}