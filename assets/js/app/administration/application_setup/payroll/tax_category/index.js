$(document).ready(function(){


	//DataTable Setup
	tableindex = $('.dataTables-example').DataTable( {
		responsive: true,
		"processing": true,
		"serverSide": true,
		"order":[],
		"ajax":{
				url: global.site_name + 'administration/application_setup/payroll/tax_category/data',
				type:"POST" 
		},
		"deferRender": true,
		"columnDefs": [{
			"targets": [0,1],
			"orderable": false,
		},{
			"targets": [0,1],
			"className": 'text-center'
		},{
			"targets": [4],
			"className": 'text-right'
		}],
		pageLength: 10,
        responsive: true,
        createdRow:function(row){
			$(row).addClass('breakdown-detail');
        },
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	$('#'+table_id+'_paginate').addClass('pull-right');
	
	
	
})
