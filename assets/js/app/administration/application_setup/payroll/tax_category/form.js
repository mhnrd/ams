$(document).ready(function(){
	$('.datepicker').datepicker({
		todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "mm/dd/yyyy"
	});
	$(".js-select2").select2();

	if (output_type == 'update') {
		$('[name="TC_ID"]').attr('readonly', true);
	}
	if (output_type == 'view') {
		$('[name="TC_ID"]').attr('readonly', true);
		$('[name="TC_ExemtDesc"]').attr('readonly', true);
		$('[name="TC_ExemptAmount"]').attr('readonly', true);
	}

});