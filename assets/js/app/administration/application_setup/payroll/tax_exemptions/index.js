$(document).ready(function(){

	$('#btnActive').prop('disabled', true);
	$('#btnDeactive').prop('disabled', true);

	// Datatable Setup
	tableindex = $('#tax_exemptions').DataTable( {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/application_setup/payroll/tax_exemptions/data',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		},{
			"targets": col_center,
			"className": 'text-left'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

		$('#'+table_id+'_paginate').addClass('pull-right');

		// Datatable2 Setup
	tableindex2 = $('#tax_exemptions_detail').DataTable( {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/application_setup/payroll/tax_exemptions/data_TM',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
			"className": 'text-center'
		},{
			"targets": [2,3,4,5],
			"className": 'text-right'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});
	$('#'+table_dtl_id+'_paginate').addClass('pull-right');


		$('.dataTables-example').on('click', '.delete-button',function(e){
		e.preventDefault();
		var doc_no = $(this).data('id');
		swal({
				title: "Are You Sure",
				text:  "This data will be deleted",
				type:  "warning",
				showCancelButton:  true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText:  "Yes, Delete It!",
				closeOnConfirm: false
		}, function(isConfirm){
			if(isConfirm){
				$.ajax({
					type: 'GET',
					dataType: 'json',
					url: global.site_name + 'administration/application_setup/payroll/tax_exemptions/delete',
					data: {	'id'	: doc_no,
							'action': 'delete'},
					success: function(data){
						if(data.success == 1){
							swal({
								title: "Delete Success",
								type:  "success"
							});
							setInterval(function(){
								location.reload();
							}, 1500);
						}else{
							swal({
								title: "Record can't be deleted",
								text:  "Data is in used",
								type:  "error"
							});
						}
					}
				});
			}
		});
	});

});
