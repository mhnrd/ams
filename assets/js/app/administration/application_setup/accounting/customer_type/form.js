$(document).ready(function(){
	
	$(".js-select2").select2();

	if (output_type == 'update') {
		$('[name="CT_Id"]').attr('readonly', true);
	}
	if (output_type == 'view') {
		$('[name="CT_Id"]').attr('readonly', true);
		$('[name="CT_Description"]').attr('readonly', true);
	}

});