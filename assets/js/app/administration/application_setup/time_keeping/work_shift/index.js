$(document).ready(function(){

	$('#btnActive').prop('disabled', true);
	$('#btnDeactive').prop('disabled', true);

	// Datatable Setup
	tableindex = $('#'+table_id).DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/application_setup/time_keeping/work_shift/data',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0,1],
			"orderable": false,
		},{
			"targets": [0,1,4,5,6,7,8,9,10,11],
			"className": 'text-center'
		}],
		pageLength: 10,
		"order": [[ 2, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});


	$('#'+table_id+'_paginate').addClass('pull-right');

});