$(document).ready(function(){

	function init(){

		if(output_type != 'view'){
			$('.clockpicker-in').timepicker({
				timeFormat: 'hh:mmp',
	        	interval: 30 // 15 minutes
			});

			$('.clockpicker-out').timepicker({
				timeFormat: 'hh:mmp',
	        	interval: 30 // 15 minutes
			});

			$('.clockpicker-break').timepicker({
				timeFormat: 'hh:mmp',
	        	interval: 30 // 15 minutes
			});	

			if(output_type == 'update'){
				$('[name="S_ID"]').attr('readonly', true);
			}
		}
		else{
			$('.clockpicker-in').css('background', '#EEEEEE');
			$('.clockpicker-out').css('background', '#EEEEEE');
			$('.clockpicker-break').css('background', '#EEEEEE');
			$('[name="S_ID"], [name="S_Name"]').attr('readonly', true);
		}


	}

	init();

})