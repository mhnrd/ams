$(document).ready(function(){
	//DataTable Setup
	tableindex = $('#time-capture-devices').DataTable( {
		responsive: true,
		"processing": true,
		"serverSide": true,
		"order":[],
		"ajax":{
				url: global.site_name + 'application_setup/time_capture_devices/data',
				type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets": [0,1],
			"orderable": false,
		},{
			"targets": [0,1],
			"className": 'text-center'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('breakdown-detail');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});
})