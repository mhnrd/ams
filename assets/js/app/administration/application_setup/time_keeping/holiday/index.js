$(document).ready(function(){

	function init(){

	// Datatable Setup
	tableindex = $('#holiday').DataTable( {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/application_setup/time_keeping/holiday/data',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0,1],
			"orderable": false,
		},{
			"targets": [0,1,5,6,8],
			"className": 'text-center'
		},{
			"targets":[2,3,4,5,6],  
			"createdCell": function(td, cellData, rowData, row, col){
			 $(td).attr('data-id', cellData);
			}
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('holiday_header');
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	$('#'+table_id+'_paginate').addClass('pull-right');

}

   init();


})

