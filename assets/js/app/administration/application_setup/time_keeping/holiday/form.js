$(document).ready(function(){

function init(){

		// FOR VIEW SETUP - READONLY
	if(output_type == 'view'){
		$('[name="H_ID"]').attr('readonly', true);
		$('[name="H_Desc"]').prop('readonly', true);
		$('[name="H_Date"]').prop('readonly', true);
		$('[name="H_HolidayStart"]').css('background', '#EEEEEE').removeClass('datepicker');
		$('[name="H_HolidayEnd"]').css('background', '#EEEEEE').removeClass('datepicker');
		$('[name="H_Policy"]').prop('readonly', true);
		$('[name="H_Type"]').prop('disabled', true);
		$(".datepicker").removeClass('datepicker');
	}

	initExtension();
	initDetails();
	initDetailTable();
	initCheckbox();
	initEvents();

	// FOR UPDATE SETUP - READONLY
	if(output_type == 'update'){
		$('[name="H_ID"]').prop('readonly', true);
		let checked = 0;
		let countCheck = $('#tbl-detail tbody tr').length;

		$('.dataTables-example tbody tr').each(function() {
			let checkbox = $(this).find('[name="chkLocation[]"]').is(':checked');
				
			if(checkbox){
				checked++;
			}
		});

		console.log(checked);
		console.log(countCheck);

		if(checked == countCheck){
			$('#chkSelectAll').prop('checked', true);
		}
		else{
			$('#chkSelectAll').prop('checked', false);	
		}
	}
	
}
	
	init();

});

function initExtension(){
	// DATEPICKER
	$('.datepicker').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "mm/dd/yyyy"
	});

	// SELECT2
	$('#type').select2({
		placeholder: '',
		allowClear: true
		
	});

	$('#payroll_policy').select2({
		placeholder: '',
		allowClear: true
	});
}

function initDetails(){

	let detail_row = ''

	if(output_type == 'add'){

		$(detail).each(function(index){
			detail_row += 	'<tr class="tbl-detail" data-row-state="created">' +
								'<td data-name="LH_FK_Location_id" data-value="' + detail[index].SP_ID + '">' + detail[index].SP_ID + '</td>' + 
								'<td>' + detail[index].SP_StoreName + '</td>' + 
								'<td data-name="LH_Selected" data-value="false">' + '<input type="checkbox" name="chkLocation[]"/>' + '</td>' + 
						    '</tr>';
		});

		$('.dataTables-example tbody').append(detail_row);
	}
	else if(output_type == 'update'){
		$(detail).each(function(index){
			detail_row += 	'<tr class="tbl-detail" data-row-state="saved">' +
								'<td data-name="LH_FK_Location_id" data-value="' + detail[index].LH_FK_Location_id + '">' + detail[index].LH_FK_Location_id + '</td>' + 
								'<td>' + detail[index].SP_StoreName + '</td>' + 
								'<td data-name="LH_Selected" data-value="'+ detail[index].LH_Selected +'">' + '<input type="checkbox" name="chkLocation[]" ' + (detail[index].LH_Selected ? 'checked' : '') + '/>' + '</td>' + 
						    '</tr>';
		});

		$('.dataTables-example tbody').append(detail_row);
	}

}

function initDetailTable(){

	if(output_type != 'view'){
		tabledetail = $('.dataTables-example').DataTable({
					responsive:true,
					"ordering"	:false,
					"scrollX"	:true,
					"scrollY"	:"500px",
					"scrollCollapse":true,
					"deferRender": true,
					"columnDefs": [{
						"targets": [2],
						"className": 'text-center'
					}],
					"paging":false,
				})
	}
	else{
		tableindex = $('#tbl-detail').DataTable( {
				"processing" 		: true,
				"serverSide" 		: true,
				"ordering" 			: false,
				"scrollX"			:true,
				"scrollY"			:"400px",
				"scrollCollapse"	:true,
				"paging"			: false,
				"ajax" :{
					url: global.site_name + 'administration/application_setup/time_keeping/holiday/data_detail?id='+ doc_no,
					type: "POST"
				},
				"deferRender": true,
				"columnDefs": [{
					"targets": [2],
					"className": 'text-center'
				}],
				// pageLength: 10,
				responsive: true,
				"dom": 'lT<"dt-toolbar">fgtip'
			});

			$('#tbl-detail_paginate').addClass('pull-right');
	}

}

function initCheckbox(){
	$('#chkSelectAll').click(function(){
		if($(this).is(':checked')){
			$('[name="chkLocation[]"]').prop('checked', true);
			$('#tbl-detail tbody tr').find('[data-name="LH_Selected"]').removeAttr('data-value');
			$('#tbl-detail tbody tr').find('[data-name="LH_Selected"]').attr('data-value', true);
		}
		else{
			$('[name="chkLocation[]"]').prop('checked', false);
			$('#tbl-detail tbody tr').find('[data-name="LH_Selected"]').removeAttr('data-value');
			$('#tbl-detail tbody tr').find('[data-name="LH_Selected"]').attr('data-value', false);
		}
	})
}

function initEvents(){

	$('[name="H_Type"]').change(function(){
		if($(this).val() != ''){
			if($(this).val() == 'R'){
				$('[name="chkLocation[]"], #chkSelectAll').prop('checked', true);
				$('#tbl-detail tbody tr').find('[data-name="LH_Selected"]').removeAttr('data-value');
				$('#tbl-detail tbody tr').find('[data-name="LH_Selected"]').attr('data-value', true);
				if(output_type == 'update'){
					$('#tbl-detail tbody tr').removeAttr('data-row-state');
					$('#tbl-detail tbody tr').attr('data-row-state', 'modified');
				}
			}
			else{
				$('[name="chkLocation[]"], #chkSelectAll').prop('checked', false);
				$('#tbl-detail tbody tr').find('[data-name="LH_Selected"]').removeAttr('data-value');
				$('#tbl-detail tbody tr').find('[data-name="LH_Selected"]').attr('data-value', false);
				if(output_type == 'update'){
					$('#tbl-detail tbody tr').removeAttr('data-row-state');
					$('#tbl-detail tbody tr').attr('data-row-state', 'modified');
				}
			}
		}
		else{
			$('[name="chkLocation[]"], #chkSelectAll').prop('checked', false);
			$('#tbl-detail tbody tr').find('[data-name="LH_Selected"]').removeAttr('data-value');
			$('#tbl-detail tbody tr').find('[data-name="LH_Selected"]').attr('data-value', false);
			if(output_type == 'update'){
				$('#tbl-detail tbody tr').removeAttr('data-row-state');
				$('#tbl-detail tbody tr').attr('data-row-state', 'modified');
			}
		}
	});

	$('.dataTables-example tbody tr td').on('click', '[name="chkLocation[]"]', function(){
		let current_row = $(this).closest('.tbl-detail');
		let checkedLoc = 0
		let countLoc = $('.dataTables-example tbody tr').length;

		if(current_row.find('[name="chkLocation[]"]').is(':checked')){
			current_row.find('[data-name="LH_Selected"]').removeAttr('data-value');
			current_row.find('[data-name="LH_Selected"]').attr('data-value', true);

			$('.dataTables-example tbody tr').each(function() {
				let checkbox = $(this).find('[name="chkLocation[]"]').is(':checked');
 				
				if(checkbox){
					checkedLoc++;
				}
			});
		}
		else{
			current_row.find('[data-name="LH_Selected"]').removeAttr('data-value');
			current_row.find('[data-name="LH_Selected"]').attr('data-value', false);

			$('.dataTables-example tbody tr').each(function() {
				let checkbox = $(this).find('[name="chkLocation[]"]').is(':checked');
 				
				if(checkbox){
					checkedLoc++;
				}
			});
		}

		if(checkedLoc == countLoc){
			$('#chkSelectAll').prop('checked', true);
		}
		else{
			$('#chkSelectAll').prop('checked', false);
		}

		if(output_type == 'update'){
			current_row.removeAttr('data-row-state');
			current_row.attr('data-row-state', 'modified');
		}
	});

}