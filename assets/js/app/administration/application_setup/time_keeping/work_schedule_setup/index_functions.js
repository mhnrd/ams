function ReloadEmployeeTable (){
    let get      = '?chkInactive=' + chkInactiveValue;

    if(cboFilterby != undefined){
        get     += '&filterBy=' + cboFilterby;
    }

    if(cboSearchby != undefined){
        get     += '&searchBy=' + cboSearchby;
    }

    console.log(get);
    tableindex.ajax.url(  global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/data' + get ).load();
}

function InitializeClickEvent(){
    // clear color value 
    $('#work_schedule_setup_header > tbody > tr button').removeClass('btn-default').addClass('btn-primary').css('color', '');
    $('#work_schedule_setup_header > tbody > tr').find('[data-employee]').css('color', '');

    if ( $(_this).hasClass('selected') ) {
        $(_this).removeClass('selected');
        $(_this).find('button').removeClass('btn-default').addClass('btn-primary');
        selected_employee_id = '';
        $('#btnWorkScheduleAdd').addClass('invisible');
         
        //updateField(selected_employee_id);
    }
    else {
        tableindex.$('tr.selected').removeClass('selected');
        $(_this).addClass('selected');
        $(_this).find('[data-employee]').css('color', 'white');
        $(_this).find('button').removeClass('btn-primary').addClass('btn-default').css('color', 'white');
        selected_employee_id = $(_this).find('[data-employee-id]').data('employee-id');
        $('#btnWorkScheduleAdd').removeClass('invisible');
        // console.log(selected_employee_id); // show employee id 
        // updateField(selected_employee_id);
    }
    let employee_id = '';
    if(selected_employee_id != ""){
        employee_id = $(_this).find('[data-employee-id]').data('employee');
    }
    
    // table on change trigger
    tbl_WorkSchedule.ajax.url(  global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/data_workSchedule?id=' + employee_id ).load();

}

//Delete Work Schedule
function InitializeDeleteData(type,data){
    switch(type){
        
        case "Allowance":
            swal({
                        title: "Are you sure?",
                        text: "This will delete the record?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#1AB394",
                        confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                    }, function () {
                            $.ajax({
                                type: 'POST',
                                dataType: 'json',
                                url: global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/deleteWorkSchedule',
                                data : data,
                                success : function(result){
                                    if(!result.success){
                                        swal(result.title, result.message, result.type);
                                    }
                                    tbl_WorkSchedule.load().draw();
                                }
                            });
                    }
                    // end of saving POST
               );
            break;
        default:
            break;
    }
}
// work schedule modal events
    $('body').on('click','#btnWorkScheduleAdd',function(e){
        e.preventDefault();
        general_info_data = [];
        $("#detail-template-work-schedule").show();
        $('#btnWorkScheduleSaveandNew').show();
        $('#btnWorkScheduleSaveandClose').text('Save and Close');
        $("#detail-template-work-schedule").modal('show');
        $('.modal').css('overflow-y', 'auto');
        InitiliazeWorkScheduleModal('add');
    });

    $('body').on('click','.work-schedule-edit',function(e){
        e.preventDefault();
        general_info_data = $(this).data('record');
        $("#detail-template-work-schedule").show();
        $('#btnWorkScheduleSaveandNew').hide();
        $('#btnWorkScheduleSaveandClose').text('Update');
        $("#detail-template-work-schedule").modal('show');
        $('.modal').css('overflow-y', 'auto');
        InitiliazeWorkScheduleModal('update');
    });
function InitiliazeWorkScheduleModal(mode){
    selected_employee_id = $(_this).find('[data-employee-id]').data('employee-id');
     let employee_id = '';

    if(selected_employee_id != ""){
        employee_id = $(_this).find('[data-employee-id]').data('employee');
    }
    var now = new Date();
                var day = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);
                var today = (month)+"/"+(day)+"/"+now.getFullYear() ;
    

    // $('#detail-template-work-schedule [name="WS_FK_Shift_id"]').select2({
    //     placeholder: "",
    //     allowClear: true,
    //     minimumInputLength: 1,
    //     ajax:
    //     {
    //         url:global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/getShiftid',
    //         dataType: 'json',
    //         delay: 250,
    //         data: function(params){
    //             var queryParameters = {
    //                     'filter-type' : 'WS_FK_Shift_id',
    //                     'filter-search': params.term
    //             }
    //             return queryParameters;
    //         },
    //         processResults: function(data, params){
    //             return {
    //                 results: $.map(data, function(cat){
    //                     return{
    //                         id: cat.WS_Description,
    //                         text: cat['WS_FK_Shift_id']
    //                     }
    //                 })
    //             };
    //         }
    //     }
    // });

    // $('#detail-template-work-schedule [name="WS_FK_Shift_id"]').change(function(){
    //     let category_code = $(this).val();

    //     if (category_code == '') {
    //         $('[name="WS_Description"]').val('');
    //     }else{
    //         $('[name="WS_Description"]').val($('detail-template-work-schedule [name="WS_FK_Shift_id"]').val());
    //     }
    // });
    
    if(mode === 'add'){
        //$('#detail-template-siblings input').val('');
        //$('#detail-template-work-schedule [name="WS_DocDate"]').val(today);
        $('#detail-template-work-schedule [name="WS_Emp_id"]').val(employee_id);
        $('#detail-template-work-schedule [name="WS_DocDate"]').val(today);
        $('#detail-template-work-schedule select').val('').trigger('change');
        $('#detail-template-work-schedule [name="WS_Description"]').val('').attr('readonly', false);
    }
    else{
        let weekday = '';
        switch(general_info_data['WS_WeekDay']){
          case "Sunday":
            weekday = '1';
            break;
          case "Monday":
            weekday = '2';
            break;
          case "Tuesday":
            weekday = '3';
            break;
          case "Wednesday":
            weekday = '4';
            break;
          case "Thursday":
            weekday = '5';
            break;
          case "Friday":
            weekday = '6';
            break;
          case "Saturday":
            weekday = '7';
            break;
          default:
        }
        $('#detail-template-work-schedule [name="WS_DocDate"]').val(moment(general_info_data['WS_DocDate']).format('MM/DD/YYYY'));
        $('#detail-template-work-schedule [name="WS_WeekDay"]').select2("val",weekday);
        $('#detail-template-work-schedule [name="WS_FK_Shift_id"]').select2("val",general_info_data['WS_FK_Shift_id']);
        $('#detail-template-work-schedule [name="WS_Description"]').val(general_info_data['WS_Description']).attr('readonly', false);
        $('#detail-template-work-schedule [name="WS_Emp_id"]').val(employee_id);
        $('#detail-template-work-schedule [name="WS_LineNo"]').val(general_info_data['WS_LineNo']);
     
    }

    $('#detail-template-work-schedule [name="mode"]').val(mode);
    $('#detail-template-work-schedule [name="WS_Emp_id"]').val(employee_id);
    if(general_info_data.length > 0){
        $('#detail-template-work-schedule [name="WS_LineNo"]').val(general_info_data['WS_LineNo']);
    }

    $('body').on('click', '#detail-template-work-schedule #btnWorkScheduleSaveandNew', function(e){
        console.log($(this).attr('id'));
        e.preventDefault();
        btnValue = $(this).val();
        InitializeWorkSchedule(btnValue);

    })

    $('body').on('click', '#detail-template-work-schedule #btnWorkScheduleSaveandClose', function(e){
        console.log($(this).attr('id'));
        e.preventDefault();
        btnValue = $(this).val();
        InitializeWorkSchedule(btnValue);
    })

    $('body').on('click', '#detail-template-work-schedule .close-modal', function(e){
        e.preventDefault();
        $('.modal-backdrop').hide();
        $('.required-label').hide();
        $('#detail-template-work-schedule').hide();
    })
}

// save/update Work Schedule
function InitializeWorkSchedule(btnValue){
    //Auto insert date of date time in
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = (month)+"/"+(day)+"/"+now.getFullYear() ;

     selected_employee_id = $(_this).find('[data-employee-id]').data('employee-id');
     let employee_id = '';
    if(selected_employee_id != ""){
        employee_id = $(_this).find('[data-employee-id]').data('employee');
    }
    let blnstatus = true
    var type = '';
    if(btnValue == 'SaveandNew' || btnValue == 'SaveandClose'){
        type = 'add';
    }
    else{
        type = 'update';
    }

    let id_data = $('#form-work-schedule').find(":input:not(:hidden)").serializeArray();
    let _validate = true;

    $(id_data).each(function(index){
        if(id_data[index].value == ""){
            console.log(id_data[index]);
            _validate = false;
            blnstatus = false;
        }
    });

    hidden_data = $('#form-work-schedule').find(':input[type="hidden"]').serializeArray()
    id_data = id_data.concat(hidden_data);

    console.log(_validate)
    if(!_validate){
        swal("Information", "Complete all fields to proceed", "info");
        blnstatus = false;
        $('.required-label').show();
    }
    else{
        swal({
                title: "Are you sure?",
                text: type == 'add' ? "This will save the record?" : "This will update the record?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#1AB394",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
            }, function () {
                            
                    console.log(id_data,"Work Schedule Data");
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/saveWorkSchedule',
                        data : id_data,
                        success : function(result){
                            if(!result.success){
                                swal(result.title, result.message, result.type);
                                blnstatus = false;
                            }
                            else{
                                if(btnValue == 'SaveandNew'){
                                    $('#detail-template-work-schedule input').val('');
                                    $('#detail-template-work-schedule [name="WS_FK_Shift_id"]').val('').trigger('change');
                                    $('#detail-template-work-schedule [name="mode"]').val('add');
                                    $('#detail-template-work-schedule [name="WS_Emp_id"]').val(employee_id);
                                    $('#detail-template-work-schedule [name="WS_DocDate"]').val(today);
                                    $('.required-label').hide();
                                    //$("option[class='val1']").remove(); 
                                }
                                else{
                                    $('#detail-template-work-schedule input').val('');
                                    $('#detail-template-work-schedule select').val('').trigger('change');
                                     $('#detail-template-work-schedule [name="WS_Emp_id"]').val(employee_id);
                                    $('#detail-template-work-schedule [name="WS_DocDate"]').val(today);
                                    $('.modal-backdrop').hide();
                                    $('#detail-template-work-schedule').hide();
                                    $('.required-label').hide();
                                }
                                tbl_WorkSchedule.load().draw();

                            }
                        }
                    });
                    // end of saving POST
                }
       );
    }
    

    return blnstatus;
}