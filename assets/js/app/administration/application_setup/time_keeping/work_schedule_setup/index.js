// Global Variable Declaration
var selected_employee_id = '';
var general_info_data = [];
var chkInactiveValue = false;

$(document).ready(function(){


	function init(){
		initExtension();
		initHeaderTable();
		tableButton();

		$('#company').select2({
			placeholder: '',
			allowClear: true,
		});

		$('#paygroup').select2({
			placeholder: '',
			allowClear: true,
		});	
		$('#WS_WeekDay').select2({
			placeholder: '',
			allowClear: true,
			dropdownParent: $('#detail-template-work-schedule')
		});
		$('.shift-js-select2').select2({
			placeholder: '',
			allowClear: true,
			dropdownParent: $('#detail-template-work-schedule')
		});
		// declare 3rd party event
	    $('[name="searchedBy"]').select2({
			placeholder: '',
			allowClear: true,
		});
		$('[name="filteredBy"]').select2({
			placeholder: '',
			allowClear: true,
		});
		$('.datepicker').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "mm/dd/yyyy"
		});

		$('body').on('click','.work-schedule-delete',function(e){
		e.preventDefault();
		work_schedule_setup_data = $(this).data('record');
		InitializeDeleteData('Allowance',work_schedule_setup_data);
	});

		$('[name="export"]').click(function(){
			swal({
                title: "Success",
                text: 'Export Success',
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#1AB394",
                confirmButtonText: "Close",
                cancelButtonText: "No",
                closeOnConfirm: true,
            });
		})

		

		

	    // tableindex.rows('.important').select();
	
	$('[name="searchedBy"]').select2("enable",false);
	$('[name="filteredBy"]').on('select2:select', function (e) {
		var id = e.params.data.id;

		cboFilterby = id;
		// search by
		$('[name="searchedBy"]').select2("val","");
		$('[name="searchedBy"]').select2("enable",true);
	    $('[name="searchedBy"]').select2({
	    	ajax: {
				url: global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/getFilterByData',
				dataType: 'json',
				delay: 250,
				data: function (params) {
				  var queryParameters = {
						'filter-search'	: id,
						'filter-input'	: params.term,
				  }
				  return queryParameters;
				},
				processResults: function (data, params) {
					return {
						results: $.map(data, function(item) {
				            return {
				                id: item.Id,
				                text: item.Description
				            }
				        })
					};
				}
			}
	    });
	});

	$('[name="filteredBy"]').on('select2:unselecting', function (e) {
		cboFilterby = "";
		$('[name="searchedBy"]').select2("val","");
		$('[name="searchedBy"]').select2("enable",false);
	});

	$('[name="searchedBy"]').on('select2:select', function (e) {
		let id = e.params.data.id;
		cboSearchby = id;
		ReloadEmployeeTable();
	});

	$('[name="filteredBy"]').on('select2:unselecting', function (e) {
		cboSearchby = "";
		ReloadEmployeeTable();
	})

	$('[name="chkInactiveEmployee"]').click(function(event) {
		chkInactiveValue = true;
		if($(this).prop('checked') !== true){
			chkInactiveValue = false;
		}
		ReloadEmployeeTable();
	});


		
	}

	  init();
	  initDetailTable();
});

	function initHeaderTable(){

		// Datatable Setup
		tableindex = $('#work_schedule_setup_header').DataTable({
			"processing"	: true,
			"serverSide"	: true,
			"responsive"	:true,
			"order":[],
			"ajax":{
				url:global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/data',
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[],
				"orderable": false,
			},{
				"targets": [0,1],
				"className": 'text-center'
			}],
			"paging": false,
			"scrollX": true,
			scrollY: 500,
  			scrollCollapse: true,
			"lengthChange": true,
			// "info": false,
			"order": [[ 1, "asc" ]],
			createdRow:function(row){
				$(row).addClass('work_sched_header');
			},
			"dom": 'lT<>fgtip'	
		});

		 $('#work_schedule_setup_header tbody').on( 'click', 'tr', function () {
			_this = $(this);
			InitializeClickEvent();
   		});

}

	function initDetailTable(){

		tbl_WorkSchedule = $('#work_schedule_setup_detail').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/data_WorkSchedule?id=' + $('[name="EmployeeID"]').val(),
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		},{
			"targets": [0,1,3],
			"className": 'text-center'
		}],
		// pageLength: 10,
		"paging": false,
		// "scrollX": true,
		// scrollY: 300,
  		// scrollCollapse: true,
		"lengthChange": false,
		"info": false,
		"order": [[ 1, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

		$('#'+table_id+'_paginate').addClass('pull-right');
	}

	function tableButton(){

	// $('#btnIDHistoryAdd').on('click',function(e){
 //    	e.preventDefault();
 //    	// show modal
 //    	$("#detail-template-id").modal('show');
 //    	InitializeModal_ID_History();
 //    });


	// 	$('body #btnSaveID').on('click',function(e){
	// 	e.preventDefault();
	// 	      swal({
	//             title: "Are you sure?",
	//             text: "This will create new employee ID?",
	//             type: "warning",
	//             showCancelButton: true,
	//             confirmButtonColor: "#1AB394",
	//             confirmButtonText: "Yes",
	//             cancelButtonText: "No",
	//             closeOnConfirm: false,
	//         })

	// });

	
}
function initExtension(){
// $('[name="WS_FK_Shift_id"]').select2({
// 		placeholder: 	"",
// 		allowClear: 	true,
// 		minimumInputLength: 3,
// 		ajax:
// 			{
// 				url: global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/getShiftID',
// 				dataType: 'json',
//                 delay: 250,
//                 data: function (params) {
//                   var queryParameters = {
//                             'filter-type'   : 'S_ID'
//                         ,   'filter-search' : params.term
//                     }
//                     return queryParameters;
//                 },
//                 processResults: function (data, params) {
//                     return {
//                         results: $.map(data, function(cat) {
//                             return {
//                                 id: cat.S_Name,
//                                 text:cat['S_ID']
//                             }
//                         })
//                     };
//                 }
// 			}
// 	});

// 	$('[name="WS_FK_Shift_id"]').change(function(){
// 		let category_code = $(this).val();

// 		if(category_code == ''){
// 			$('[name="WS_Description"]').val('');
// 		}
// 		else{
// 			$('[name="WS_Description"]').val($('[name="WS_FK_Shift_id"]').val())
// 		}
// 	});


}

// var selected_employee_id = '';
// $(document).ready(function(){

// 	addButtons();


// 	$.fn.modal.Constructor.prototype.enforceFocus = function() {};


// 	// Datatable Setup
// 	//DataTable Setup
// 	tableindex = $('#work_schedule_setup').DataTable({
// 		responsive: true,
// 		"processing": true,
// 		"serverSide": true,
// 		"order":[],
// 		"ajax":{
// 				url:global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/data',
// 				type:"POST" 
// 		},
// 		"deferRender": true,
// 		"columnDefs": [{
// 			"targets": [0],
// 			"orderable": false,
// 		},{
// 			"targets": [0,1],
// 			"className": 'text-center'
// 		}],
// 		pageLength: 10,
// 		"paging": false,
// 		"scrollX": true,
// 		scrollY: 300,
//   		scrollCollapse: true,
//         responsive: true,
//         "order": [[ 1, "asc" ]],
//         createdRow:function(row){
// 			$(row).addClass('breakdown-detail');
//         },
// 		"dom": 'lT<"dt-toolbar">fgtip'
// 	});
// 	$('#'+table_id+'_paginate').addClass('pull-right');

// 	$('#work_schedule_setup tbody').on( 'click', 'tr', function () {
// 			_this = $(this);
// 			InitializeClickEvent();
//    		});

// 	tbl_IdHistory = $('#shift_detail').DataTable( {
// 		"processing":true,
// 		"serverSide":true,
// 		"responsive":true,
// 		"order":[],
// 		"ajax":{
// 			url: global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/data_IdHistory?id=' + $('[name="EmployeeID"]').val(),
// 			type: "POST"
// 		},
// 		"deferRender": true,
// 		"columnDefs": [{
// 			"targets":[0],
// 			"orderable": false,
// 		}],
// 		// pageLength: 10,
// 		"paging": false,
// 		// "scrollX": true,
// 		// scrollY: 300,
//   		// scrollCollapse: true,
// 		"lengthChange": false,
// 		"info": false,
// 		"order": [[ 1, "asc" ]],
// 		createdRow:function(row){
// 			$(row).addClass('table-header');
// 		},
// 		"dom": 'lT<"dt-toolbar">fgtip'
// 	});

	


//     $('.add-detail').on('click',function(e){
//     	e.preventDefault();
//     	// show modal
//     	$("#detail-template").modal('show');
//     });

	

   

// });


// function addButtons(){
//     $('#add-new').click(function(){
// 		let s_day 	= $('#S_Day option:selected').text();
// 		let s_id  		= $('#S_ID').val();

// 		// if(validate_details()){
// 		// 	addDetail(s_day, s_id,);
// 		// 	$('[name="PCBUD_MarkdownPrice"]').val('0.00');
// 		// 	$('[name="PCBUD_CategoryCode"]').val('').trigger('change');
// 		// }
// 	});
// }

// function addDetail(s_day, s_id){	

// let temp = tabledetail.row.add([
// 		'<button class="btn btn-outline btn-xs edit-detail btn-primary"><i class="fa fa-pencil"></i></button>' + 
// 		'<button class="btn btn-outline btn-xs delete-detail btn-danger" style="margin-left: 5px"><i class="fa fa-trash"></i></button>',
// 		s_day,
// 		s_id]).node();

// 	$(temp).attr("data-row-state", "created");

// 	tabledetail.row().draw(false);
// }

// function actionButtons(action){

// 	if(action == 'add'){
// 		$('#add-close').text('Add & Close');
// 		$('#add-new').show();
// 	}
// 	else{
// 		$('#add-close').text('Update');
// 		$('#add-new').hide();
// 	}

// }



// $(document).ready(function(){
// 		$('#paygroup').select2({
// 			placeholder: '',
// 		});
// 		$('#company').select2({
// 			placeholder: '',
			
// 		});

// 	function init(){

// 		initExtension();
// 		initDetailTable();
// 		tableButtons();
// 		addButtons();
// 		//DataTable Setup
// 	tableindex = $('#work_schedule_setup').DataTable({
// 		"responsive" : true,
// 		"processing": true,
// 		"serverSide": true,
// 		"order":[],
// 		"ajax":{
// 				url:global.site_name + 'administration/application_setup/time_keeping/work_schedule_setup/data',
// 				type:"POST" 
// 		},
// 		"deferRender": true,
// 		"columnDefs": [{
// 			"targets": [0],
// 			"orderable": false,
// 		},{
// 			"targets": [0],
// 			"className": 'text-center'
// 		}],
// 		"paging": false,
// 		"scrollX": true,
// 		scrollY: 500,
//   		scrollCollapse: true,
// 		"lengthChange": false,
// 		"info": false,
// 		"order": [[ 1, "asc" ]],
//         createdRow:function(row){
// 			$(row).addClass('table-header');
//         },
// 		"dom": 'lT<"dt-toolbar">fgtip'
// 	});

		

// 	}

// 	init();

// });
	


// function tableButtons(){
// 	$('.add-detail').click(function(){

// 		action = 'add';

// 		actionButtons(action);
		
// 		$('#S_Name').val('');
// 		$('#S_ID').val('');
// 		$('#S_Desc').val('');
// 		$('#S_Time').val('');
// 		$('#detail-template').modal('show');


// 	});
// 	if (output_type == 'add' || 'update') {
// 	$('#tbl-detail').on('click', '.edit-detail', function(){
// 		let current_row = $(this).closest('.tbl-detail');

// 		let s_day 	= current_row.find('td:eq(1)').text();
// 		let s_id 	= current_row.find('td:eq(2)').text();
// 		let s_desc 	= current_row.find('td:eq(3)').text();
// 		let s_time 	= current_row.find('td:eq(4)').text();
		 
// 		 line_no	 = current_row.index();

// 		console.log(s_id)

// 		action = 'edit';

// 		actionButtons(action);

		
// 		$('#S_Name').val(s_day);
// 		$('#S_ID').val(s_id);
// 		$('#S_Desc').val(s_desc);
// 		$('#S_Time').val(s_time);

// 		$('#detail-template').modal('show');
// 	});
// 	}

// 	$('#tbl-detail').on('click', '.delete-detail', function(){
// 		let current_row = tabledetail.row($(this).closest('.tbl-detail'));
		
// 		swal({
// 			title 				: "Delete this row?",
// 			type 				: "warning",
// 			showCancelButton	: true,
// 			cancelButtonText 	: "No",
// 			cancelButtonColor 	: '#DD6B55',
// 			confirmButtonColor 	: '#1AB394',
// 			confirmButtonText 	: "Yes",
// 			closeOnConfirm 		: true
// 		}, function(isConfirm){
// 			if(isConfirm){
// 				if(output_type == 'add'){
// 			    	current_row.remove().draw();
// 				}
// 				else{
// 					$(current_row.node()).attr("data-row-state", "deleted");
// 					$(current_row.node()).hide();
// 				}
// 			}
// 		})
// 	});
// }

// function initExtension(){

// }



// function initDetailTable(){
// tbl_IdHistory = $('#employee_id_details').DataTable( {
// 		"processing":true,
// 		"serverSide":true,
// 		"responsive":true,
// 		"order":[],
// 		"ajax":{
// 			url: global.site_name + 'administration/master_file/employee_profile/data_IdHistory?id=' + $('[name="EmployeeID"]').val(),
// 			type: "POST"
// 		},
// 		"deferRender": true,
// 		"columnDefs": [{
// 			"targets":[0],
// 			"orderable": false,
// 		}],
// 		// pageLength: 10,
// 		"paging": false,
// 		// "scrollX": true,
// 		// scrollY: 300,
//   		// scrollCollapse: true,
// 		"lengthChange": false,
// 		"info": false,
// 		"order": [[ 1, "asc" ]],
// 		createdRow:function(row){
// 			$(row).addClass('table-header');
// 		},
// 		"dom": 'lT<"dt-toolbar">fgtip'
// 	});
// // tabledetail = $('#tbl-detail').DataTable( {
// // 			"scrollX"	:true,
// // 			"scrollY"	:"200px",
// // 			"scrollCollapse":true,
// // 			"ordering": false,
// // 			"paging": false,
// // 			"columnDefs": [{
// // 				"targets": [0],
// // 				"className": 'text-center'
// // 			},{
// // 				"targets": [2],
// // 				"className": 'text-left'
// // 			},{
// // 				  "targets":[1],  
// // 				  "createdCell": function(td, cellData, rowData, row, col){
// // 				  	$(td).attr('data-name', 'S_Day');
// // 				  	$(td).attr('data-value', cellData);
// // 				  }
// // 			}],
// // 			responsive: true,
// // 			createdRow:function(row){
// // 				$(row).addClass('tbl-detail');
// // 			},
// // 			"dom": 'lT<"dt-toolbar">fgtip'
// // 		});

// }

// function actionButtons(action){

// 	if(action == 'add'){
// 		$('#add-close').text('Add & Close');
// 		$('#add-new').show();
// 	}
// 	else{
// 		$('#add-close').text('Update');
// 		$('#add-new').hide();
// 	}

// }

// function addButtons(){

// 	$('#add-new').click(function(){
// 		let s_day 		= $('#S_Name').val();
// 		let s_id  		= $('#S_ID').val();
// 		let s_desc 		= $('#S_Desc').val();
// 		let s_time 		= $('#S_Time').val();

// 		if(validate_details()){
// 			addDetail(s_day, s_id,s_desc,s_time);
// 			$('#S_ID').val('');
// 			$('#S_Name').val('');
// 			$('#S_Desc').val('');
// 			$('#S_Time').val('');
// 		}
// 	});

// 	// $('#add-close').click(function(){
// 	// 	let s_day 		= $('#S_Day option:selected').text();
// 	// 	let s_id  		= $('#S_ID').val();

// 	// 	if(validate_details()){
// 	// 		if(action == 'add'){
// 	// 			addDetail(s_day, s_id);
// 	// 			$('#detail-template').modal('hide');
// 	// 		}
// 	// 		else{
// 	// 			editDetail(line_no, user_id, username,emp_id);
// 	// 			$('#detail-template').modal('hide');
// 	// 		}
// 	// 	}
// 	// });

// }

// function addDetail(s_day, s_id,s_desc,s_time){
// 	let temp = tabledetail.row.add([
// 		'<button class="btn btn-outline btn-xs edit-detail btn-primary"><i class="fa fa-pencil"></i></button>' + 
// 		'<button class="btn btn-outline btn-xs delete-detail btn-danger" style="margin-left: 5px"><i class="fa fa-trash"></i></button>',
// 		s_day,
// 		s_id,
// 		s_desc,
// 		s_time,
// 		]).node();

// 	$(temp).attr("data-row-state", "created");

// 	tabledetail.row().draw(false);
// 	console.log(temp);
// }

// function editDetail(s_day, s_id,s_desc,s_time){
// 	let temp = tabledetail.row(line_no).data([
// 		'<button class="btn btn-outline btn-xs edit-detail btn-primary"><i class="fa fa-pencil"></i></button>' + 
// 		'<button class="btn btn-outline btn-xs delete-detail btn-danger" style="margin-left: 5px"><i class="fa fa-trash"></i></button>',
// 		s_day,
// 		s_id,
// 		s_desc,
// 		s_time
// 		]).node();

// 	if(output_type == 'update'){
// 		$(temp).attr("data-row-state", "modified");
// 	}
// 	else{
// 		$(temp).attr("data-row-state", "created");
// 	}
// }


// function validate_details(){
// 	let isComplete = true;
// 	let requiredField = $('#detail-template :input[required]:visible');

// 	$(requiredField).each(function(index, el) {
// 		if($(this).val() == "" || $(this).val() == 0){
// 			isComplete = false;
// 		}		
// 	});

// 	if(action == 'add'){
// 		$('#tbl-detail tbody tr:visible').each(function(){
// 			if($('#S_Name option:selected').text() == $(this).find('td:eq(1)').text()){
// 				isComplete = false;
// 			}
// 		});
// 	}

// 	return isComplete;

// }