// Global Variable Declaration
var selected_employee_id = '';
var time_entry_data = [];
var general_info_data = [];
var cboFilterby;
var cboSearchby;
var chkInactiveValue = false;
var _this;


$(document).ready(function(){

	function init(){
		// var date_swipped = "{ $C_DateSwipe }";
		// alert(date_swipped);
		initHeaderTable();

		$('#company').select2({
			placeholder: '',
			allowClear: true,
		});

		$('#pay_group').select2({
			placeholder: '',
			allowClear: true,
		});	

		$('#dowin').select2({
			placeholder: '',
			allowClear: true,
			//dropdownParent: $('#detail-template-ete')
		});	

		$('#dowout').select2({
			placeholder: '',
			allowClear: true,
			//dropdownParent: $('#detail-template-ete')
		});	

		$('#employee').select2({
			placeholder: '',
			allowClear: true,
			dropdownParent: $('#detail-template-print')
		});	

		$('#principal').select2({
			placeholder: '',
			allowClear: true,
			dropdownParent: $('#detail-template-print')
		});		

		// Clockpicker Time In
		$('.clockpicker-in').clockpicker({
			autoclose: true
		});

		// Clockpicker Time Out
		$('.clockpicker-out').clockpicker({
			autoclose: true
		});

		// Clockpicker Break
		$('.clockpicker-break').clockpicker({
			autoclose: true
		});	
	

		$('.datepicker').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "mm/dd/yyyy"
		});

		//Print
		$('.custom-file-input').on('change', function() {
		   let fileName = $(this).val().split('\\').pop();
		   $(this).next('.custom-file-label').addClass("selected").html(fileName);
		}); 


		// Filters
		$('[name="searchedBy"]').select2("enable",false);
		$('[name="filteredBy"]').on('select2:select', function (e) {
			var id = e.params.data.id;

		cboFilterby = id;
		// search by
		$('[name="searchedBy"]').select2("val","");
		$('[name="searchedBy"]').select2("enable",true);
	    $('[name="searchedBy"]').select2({
	    	ajax: {
				url: global.site_name + 'administration/application_setup/time_keeping/time_attendance/getFilterByData',
				dataType: 'json',
				delay: 250,
				data: function (params) {
				  var queryParameters = {
						'filter-search'	: id,
						'filter-input'	: params.term,
				  }
				  return queryParameters;
				},
				processResults: function (data, params) {
					return {
						results: $.map(data, function(item) {
				            return {
				                id: item.Id,
				                text: item.Description
				            }
				        })
					};
				}
			}
	    });
	});

	$('[name="filteredBy"]').on('select2:unselecting', function (e) {
		cboFilterby = "";
		$('[name="searchedBy"]').select2("val","");
		$('[name="searchedBy"]').select2("enable",false);
	});

	$('[name="searchedBy"]').on('select2:select', function (e) {
		let id = e.params.data.id;
		cboSearchby = id;
		ReloadEmployeeTable();
	});

	$('[name="filteredBy"]').on('select2:unselecting', function (e) {
		cboSearchby = "";
		ReloadEmployeeTable();
	})

	$('#employee_time_entries tbody').on( 'click', 'tr', function () {
			_this = $(this);
			InitializeClickEvent();
   		});

	// Delete
	$('body').on('click','.time-entry-delete',function(e){
		e.preventDefault();
		time_entry_data = $(this).data('record');
		InitializeDeleteData('Time_Entries',time_entry_data);
	});
		
	}

	  init();
	  initDetailTable();
	  initDetailTimeLog();
	  initDetailPTA();
});

	function initHeaderTable(){

		// Datatable Setup for employee
		tableindex = $('#employee_time_entries').DataTable( {
			"processing"	: true,
			"serverSide"	: true,
			"responsive"	:true,
			"order":[],
			"ajax":{
				url: global.site_name + 'administration/application_setup/time_keeping/time_attendance/data',
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[0,1],
				"orderable": false,
			}],
			"paging": false,
			"scrollX": true,
			scrollY: 500,
  			scrollCollapse: true,
			"lengthChange": false,
			//"info": false,
			"order": [[ 1, "asc" ]],
			createdRow:function(row){
				$(row).addClass('');
			},
			"dom": 'lT<>fgtip'	
		});

	}

	// Datatable setup for time entry
	function initDetailTable(){
		let table = $('#tbl-detail').DataTable({destroy: true}); 	
			table.destroy();
			$('#tbl-detail tbody tr').remove();
		tbl_time_entries = $('#tbl-detail').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/application_setup/time_keeping/time_attendance/data_detail?id=' + $('[name="EmployeeId"]').val(),
			method: "POST",
		},
		success:function(data){
			$('#tbl-detail').html(data)
		},	
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
			},{
				"targets": [0,2,3,6,7,8,9,10,11,12],
				"className": 'text-center'
			},{
				"targets": [1,4,5],
				"className": 'text-right'
			},{
				"targets": [7,8,9,10,11,12],
				"visible": false,
                "searchable": false
		}],
		"paging": false,
		"dom": 'lT<"">fgtip'
	});

	}

	// Datatable setup for process time attendance
	function initDetailPTA(){

		tbl_process_time_attendance = $('#tbl-process-time-attendance').DataTable( {
			"processing"	: true,
			"serverSide"	: true,
			"responsive"	:true,
			"order":[],
			"ajax":{
				url: global.site_name + 'administration/application_setup/time_keeping/time_attendance/data_PTA',
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
				"orderable": false,
			},{
				"targets": [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
				"className": 'text-center'
			}],
			"paging": false,
			"scrollX": true,
			scrollY: 500,
  			scrollCollapse: true,
			"lengthChange": false,
			//"info": false,
			//"order": [[ "asc" ]],
			// createdRow:function(row){
			// 	$(row).addClass('');
			// },
			"dom": 'lT<>fgtip'	
		});

	}

	// Datatable setup for time log
	function initDetailTimeLog(){

		tbl_time_log = $('#tbl-time-log').DataTable( {
			"processing"	: true,
			"serverSide"	: true,
			"responsive"	:true,
			"order":[],
			"ajax":{
				url: global.site_name + 'administration/application_setup/time_keeping/time_attendance/data_time_log',
				type: "POST"
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[0,1,2],
				"orderable": false,
			},{
				"targets": [0,1,2],
				"className": 'text-center'
			}],
			 "paging": false,	
		});

	}


	//Print Modal
	$('#btnPrint').click(function(){
		$('#detail-template-print').modal('show');
	});

	// For Generate Client Time Modal
	$('#btnGCT').click(function(){	
	 	$('#detail-template-gtk').modal('show'); 
	 	$('.modal').css('overflow-y', 'auto');
	});

	// For Upload DTR Modal
	$('#btnUploadtDTR').click(function(){
		$('#detail-template-upload-dtr').modal('show');
		$('.modal').css('overflow-y', 'auto');
		InitializeModal_UploadDTR();
	});


	//generate import
	function InitializeModal_UploadDTR(){
  
		$('#import_form').on('submit', function(event){
			event.preventDefault();

		$.ajax({
			url: global.site_name + 'administration/application_setup/time_keeping/time_attendance/import_file',
			method: "POST",
			data:new FormData(this),
			contentType:false,
			cache:false,
			processData:false,
            keyboard: false,
				success:function(data){
					
					let result = JSON.parse(data);
					$('#file').val('');
					
					if(result.success == '1'){
							swal({
					                title: "Success",
					                text: 'Importing Data Files Successful!',
					                type: "success",         
					                showConfirmButton: false,
					                closeOnConfirm: false,
					                timer: 2000,
						            },function(){
						            	window.location.reload(true);
				            });	
					}
					else{
						swal("Error",result.error_msg,"error");
						$('.modal-backdrop').hide();
						$('#detail-template-upload-dtr').hide();
						initDetailTable();
					}
				},
	            error: function(jqXHR){
		           	 
                            swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
              
		           	 $('.modal-backdrop').hide();
		           	 $('#detail-template-upload-dtr').hide();
	        	}
			})
		});
	}

	//process time attendance function button
	$('#btnProcessAttendance').on('click', function(e){
		e.preventDefault();
		swal({
	            title: "Process time attendance dated from 2/5/2020 to 2/6/2020",
	            text: "",
	            type: "info",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: false,
        });
	})
