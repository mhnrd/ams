function ReloadEmployeeTable (){
    let get      = '?chkInactive=' + chkInactiveValue;

    if(cboFilterby != undefined){
        get     += '&filterBy=' + cboFilterby;
    }

    if(cboSearchby != undefined){
        get     += '&searchBy=' + cboSearchby;
    }

    console.log(get);
    tableindex.ajax.url(  global.site_name + 'administration/application_setup/time_keeping/time_attendance/data' + get ).load();
}

function InitializeClickEvent(){
	// clear color value 
	$('#employee_time_entries > tbody > tr button').removeClass('btn-default').addClass('btn-primary').css('color', '');
	$('#employee_time_entries > tbody > tr').find('[data-employee]').css('color', '');

    if ( $(_this).hasClass('selected') ) {
        $(_this).removeClass('selected');
        $(_this).find('button').removeClass('btn-default').addClass('btn-primary');
        selected_employee_id = '';
         $('#btnETEAdd').addClass('invisible');
    }
    else {
        tableindex.$('tr.selected').removeClass('selected');
        $(_this).addClass('selected');
        $(_this).find('[data-employee]').css('color', 'white');
        $(_this).find('button').removeClass('btn-primary').addClass('btn-default').css('color', 'white');
        selected_employee_id = $(_this).find('[data-employee-id]').data('employee-id');
         $('#btnETEAdd').removeClass('invisible');
    }
   

    let employee_id = '';
    if(selected_employee_id != ""){
    	employee_id = $(_this).find('[data-employee-id]').data('employee');
    }
    
    // table on change trigger
    tbl_time_entries.ajax.url(  global.site_name + 'administration/application_setup/time_keeping/time_attendance/data_detail?id=' + employee_id ).load();
	
}

//Delete
function InitializeDeleteData(type,data){
switch(type){
    case "Time_Entries":
        swal({
                title: "Are you sure?",
                text: "This will delete the record?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#1AB394",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                }, function (isConfirm) {
                    if(isConfirm){
                        $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: global.site_name + 'administration/application_setup/time_keeping/time_attendance/deleteTimeEntries',
                        data : data,
                        success : function(data){
                            if(data.success){
                                swal({
                                            title: "Delete Success",
                                            type:  "success",
                                            showConfirmButton: false,
                                            closeOnConfirm: false,
                                            timer: 2000,
                                });
                                tbl_time_entries.ajax.reload();
                            }else{
                                swal({
                                        title: data.title,
                                        text:  data.message,
                                        type:  data.type
                                });
                            }
                            
                        },
                         error : function(){
                            swal("Error", "Please contact your system administrator", "error")
                        }
                    });
                 }
            }
        );
            break;  
        default:
            break;
    }
}

    // For Add Entry Modal
    $('body').on('click','#btnETEAdd',function(e){
    e.preventDefault();
    $("#detail-template-ete").show();
    $("#detail-template-ete").modal('show');
    $('#btnSaveandClose').text('Save');
    $('.modal').css('overflow-y', 'auto');
    InitializeModal_Add_Entry('add');
    });

    //Edit button
    $('body').on('click','.button-edit',function(e){
    e.preventDefault();
    time_entry_data = $(this).data('record');
    $("#detail-template-ete").show();
    $('#btnSaveandClose').text('Update');
    $("#detail-template-ete").modal('show');
    $('.modal').css('overflow-y', 'auto');
    InitializeModal_Add_Entry('update');
    });

    //Delete
    $('body').on('click','.button-delete',function(e){
    e.preventDefault();
    time_entry_data = $(this).data('record');
    InitializeDeleteData('Time_Entries',time_entry_data);
    });

//Initialize modal Time Entries
function InitializeModal_Add_Entry(mode){

    
    //employee ID show
    selected_employee_id = $(_this).find('[data-employee-id]').data('employee-id');
        let employee_id = '';
        if(selected_employee_id != ""){
            employee_id = $(_this).find('[data-employee-id]').data('employee');
        }

    //employee name show
    selected_employee_name = $(_this).find('[data-employee-name]').data('employee-name');
        let employee_name = '';
        if(selected_employee_name != ""){
            employee_name = $(_this).find('[data-employee-name]').data('employee');
        }

    //Validation for duplicate DateSwipped
    var table = $("#tbl-detail");
    $('[name="C_DateSwipe"]').change(function(){
        table.find("tbody tr").each(function () {
            var title2 = $(this).find("td:eq(1)").html();
        if (title2 == $('[name="C_DateSwipe"]').val()) {
           swal("Information!", "The Date Swipe Cannot Be Duplicate!", "info");
           $('#detail-template-ete [name="C_DateSwipe"]').val('');
        }
        });
    });

    if(mode === 'add'){
       $('#detail-template-ete #EmployeeID').val(employee_id);
       $('#detail-template-ete #EmployeeName').val(employee_name);

       //Clear data
       $('#detail-template-ete select').val('').trigger('change');
       $('#detail-template-ete input').val('');
      $('#detail-template-ete [name="C_DateSwipe"]').val('');
     
    }

    //update insert data
    else{
        console.log(time_entry_data);

        let DOW_In = '';
        switch(time_entry_data['C_DOW_In']){
          case "Sunday":
            DOW_In = '1';
            break;
          case "Monday":
            DOW_In = '2';
            break;
          case "Tuesday":
            DOW_In = '3';
            break;
          case "Wednesday":
            DOW_In = '4';
            break;
          case "Thursday":
            DOW_In = '5';
            break;
          case "Friday":
            DOW_In = '6';
            break;
          case "Saturday":
            DOW_In = '7';
            break;
          default:
        }

         let DOW_Out = '';
        switch(time_entry_data['C_DOW_Out']){
          case "Sunday":
            DOW_Out = '1';
            break;
          case "Monday":
            DOW_Out = '2';
            break;
          case "Tuesday":
            DOW_Out = '3';
            break;
          case "Wednesday":
            DOW_Out = '4';
            break;
          case "Thursday":
            DOW_Out = '5';
            break;
          case "Friday":
            DOW_Out = '6';
            break;
          case "Saturday":
            DOW_Out = '7';
            break;
          default:
        }

        $('#detail-template-ete [name="C_LineNo"]').val(time_entry_data['C_LineNo']);
        $('#detail-template-ete [name="C_DateSwipe"]').val(moment(time_entry_data['C_DateSwipe']).format('MM/DD/YYYY'));
        $('#detail-template-ete [name="C_DOW_In"]').select2("val",DOW_In);
        $('#detail-template-ete [name="C_DOW_Out"]').select2("val",DOW_Out);
        $('#detail-template-ete [name="C_TimeIn"]').val(time_entry_data['C_TimeIn']);
        $('#detail-template-ete [name="C_TimeOut"]').val(time_entry_data['C_TimeOut']);
        $('#detail-template-ete [name="C_BreakOut_1"]').val(time_entry_data['C_BreakOut_1']);
        $('#detail-template-ete [name="C_BreakIn_1"]').val(time_entry_data['C_BreakIn_1']);
        $('#detail-template-ete [name="C_BreakOut_2"]').val(time_entry_data['C_BreakOut_2']);
        $('#detail-template-ete [name="C_BreakIn_2"]').val(time_entry_data['C_BreakIn_2']);
        $('#detail-template-ete [name="C_BreakOut_3"]').val(time_entry_data['C_BreakOut_3']);
        $('#detail-template-ete [name="C_BreakIn_3"]').val(time_entry_data['C_BreakIn_3']);
        $('#detail-template-ete #EmployeeID').val(employee_id);

    }

    $('#detail-template-ete #EmployeeID').val(employee_id);
    $('#detail-template-ete #EmployeeName').val(employee_name);
    $('#detail-template-ete [name="mode"]').val(mode);
    if(time_entry_data.length > 0){
        $('#detail-template-ete [name="C_LineNo"]').val(time_entry_data['C_LineNo']);
    }

    $('body').on('click', '#detail-template-ete #btnSaveandClose', function(e){
        console.log($(this).attr('id'));
        e.preventDefault();
        btnValue = $(this).val();
        InitializeSaveTimeEntries(btnValue);
    })

    $('body').on('click', '#detail-template-ete .close-modal', function(e){
        e.preventDefault();
        $('.modal-backdrop').hide();
        $('.required-label').hide();
        $('#detail-template-ete').hide();
    })
}

// save/update TimeEntries
function InitializeSaveTimeEntries(btnValue){
        
    //employee ID show    
    selected_employee_id = $(_this).find('[data-employee-id]').data('employee-id');
    let employee_id = '';
    if(selected_employee_id != ""){
        employee_id = $(_this).find('[data-employee-id]').data('employee');
    }

    //employee name show
    selected_employee_name = $(_this).find('[data-employee-name]').data('employee-name');
        let employee_name = '';
        if(selected_employee_name != ""){
            employee_name = $(_this).find('[data-employee-name]').data('employee');
        }

    let blnstatus = true
    var type = '';
    if(btnValue == 'SaveandClose'){
        type = 'add';
    }
    else{
        type = 'update';
    }

    let id_data = $('#form-employee-time-entries').find(":input:not(:hidden)").serializeArray();
    let hidden = $('#form-employee-time-entries').find(".hidden").serializeArray();
    let _validate = true;

    $(id_data).each(function(index){
        if(id_data[index].value == ""){
            console.log(id_data[index]);
            _validate = true;
            blnstatus = false;
        }
    });

    $(hidden).each(function(index){
        if(id_data[index].value == ""){
            console.log(id_data[index]);
            _validate = false;
            blnstatus = false;
        }
    });

    hidden_data = $('#form-employee-time-entries').find(':input[type="hidden"]').serializeArray()
    id_data = id_data.concat(hidden_data);

    hidden_data1 = $('#form-employee-time-entries').find('.hidden').serializeArray()
    hidden = hidden.concat(hidden_data1);

    console.log(_validate)
    if(!_validate){
        swal("Information", "Complete all fields to proceed", "info");
        blnstatus = false;
        $('.required-label').show();
    }
    else{
        swal({
                title: "Are you sure?",
                text: type == 'add' ? "This will save the record?" : "This will update the record?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#1AB394",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
            }, function () {
                            
                    console.log(id_data,"Employee Time Entry");
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: global.site_name + 'administration/application_setup/time_keeping/time_attendance/saveTimeEntries',
                        data : id_data,
                        success : function(result){
                            if(!result.success){
                                swal(result.title, result.message, result.type);
                                blnstatus = false;
                            }
                            else{
                                if(btnValue == 'SaveandClose'){
                                    $('#detail-template-ete input').val('');
                                   $('#detail-template-ete select').val('').trigger('change');
                                   $('.modal-backdrop').hide();
                                   $('#detail-template-ete').hide();
                                $('.required-label').hide();
                                }
                                tbl_time_entries.load().draw();

                            }
                        },
                        error : function(jqXHR){
                            swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
                        }
                    });
                    // end of saving POST
                }
       );
    }
    
    return blnstatus;
}