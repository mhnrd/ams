$(document).ready(function(){

	function init(){

		$('[name="P_Type"]').change(function(){

			let p_type = $(this).val();

			if(p_type == ''){
				$('[name="P_Type"]').val('');
			}
			else{
				$.ajax({
					url 		: global.site_name + 'application_setup/attributes/getCurrentPtype',
					type 		: 'POST',
					dataType 	: 'json',
					data 		: {
						'p-type' 	: p_type
					},
					success 	: function(result){
						$('[name="P_Type"]').val(result);
					}
				})
			}


		})
	}

	init();
});