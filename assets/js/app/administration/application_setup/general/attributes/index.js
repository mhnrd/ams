$(document).ready(function(){

	$('#btnActive').prop('disabled', true);
	$('#btnDeactive').prop('disabled', true);

	// Datatable Setup
	tableindex = $('#'+table_id).DataTable( {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/application_setup/general/attributes/data?id='+attr+'&attr='+attr_module ,
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0,1],
			"orderable": false,
		},{
			"targets": [0,1],
			"className": 'text-center'
		}],
		pageLength: 10,
        responsive: true,
        "order": [[ 2, "asc" ]],
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	$('#'+table_id+'_paginate').addClass('pull-right');

});