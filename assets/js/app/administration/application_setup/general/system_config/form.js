$(document).ready(function(){

	function init(){
		if(output_type == 'update'){
			$('[name="SC_ModuleID"]').prop('disabled', true);
		}

		initExtension();
	}

	init();

})

function initExtension(){
	$('[name="SC_ModuleID"]').select2({
		placeholder 		: '',
		allowClear 			: true,
		minimumInputLength	: 3,
		ajax : {
			url: global.site_name + 'administration/application_setup/general/system_config/searchModules',
			dataType: 'json',
			delay: 250,
			data: function (params) {
			  var queryParameters = {
					'filter-input'		: params.term
			  }
			  return queryParameters;
			},
			processResults: function (data, params) {
				return {
					results: $.map(data, function(item) {
			            return {
			                id: item.M_Module_id,
			                text: item.M_DisplayName
			            }
			        })
				};
			}
		}
	});

	$('[name="SC_DataType"]').select2({
		placeholder 	: '',
		allowClear 		: true 
	})

	$('[name="SC_ModuleID"]').change(function(){

		let module_id = $(this).val();

		if(module_id == '' || module_id === null){
			$('[name="SC_ID"]').val('');
		}
		else{
			$.ajax({
				url 		: global.site_name + 'administration/application_setup/general/system_config/getConfigID',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
					'module-id' 	: module_id
				},
				success 	: function(result){
					$('[name="SC_ID"]').val(result);
				},
				error 		: function(jqXHR){
					swal('Error ' + jqXHR.status, 'Contact your System Administration', 'error');
				}
			})
		}

	});
}