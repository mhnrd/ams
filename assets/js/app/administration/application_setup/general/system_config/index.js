$(document).ready(function(){

	function init(){

		inputFunctions();
	}

   init();


})

function inputFunctions(){
	$('.numeric-input').on('keyup', function(){
		let current_input = $(this);

		if($(this).val() == '' || $(this).val() === null){
			$(this).val(0);
		}
		else{
			$.ajax({
				url 		: global.site_name + 'administration/application_setup/general/system_config/updateConfigValue',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
					'doc-no' 	: current_input.data('doc-no'),
					'value' 	: current_input.val()
				},
				error 		: function(jqXHR){
					if(jqXHR.status != 200){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				}
			})
		}
	});

	$('.bit-input').click(function(event) {
		let checked = 1;
		let current_input = $(this);

		if($(this).is(':checked')){
			checked = 1
		}
		else{
			checked = 0
		}

		$.ajax({
			url 		: global.site_name + 'administration/application_setup/general/system_config/updateConfigValue',
			dataType 	: 'json',
			type 		: 'POST',
			data 		: {
				'doc-no' 	: current_input.data('doc-no'),
				'value' 	: checked
			},
			error 		: function(jqXHR){
				if(jqXHR.status != 200){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			}
		});
	});
}