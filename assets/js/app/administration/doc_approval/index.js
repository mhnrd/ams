$(document).ready(function(){

	// Datatable Setup
	tableindex = $('.dataTables-example').DataTable( {
		"processing":true,  
		"serverSide":true,  
		"order":[],  
		"ajax":{  
		    url: global.site_name + 'approval_setup/doc_approval/data',  
		    type:"POST"  
		},  
        "deferRender": true,
		 "columnDefs": [{
			  "targets":[0],  
			  "orderable": false,
		},{
			  "targets": [0,2,3,5],
			  "className": 'text-center'
		}],
		pageLength: 10,
        responsive: true,
        "order": [[ 1, "desc" ]],
		"dom": 'lT<"dt-toolbar">fgtip'
	});

 });