$(document).ready(function(){

	// Datatable Setup
	tableindex = $('.dataTables-example').DataTable( {
		"processing":true,  
		"serverSide":true,  
		"order":[],  
		"ajax":{  
		    url: global.site_name + 'master_file/item_master/data',  
		    type:"POST"  
		},  
        "deferRender": true,
		 "columnDefs": [{
			  "targets":[0, 1],  
			  "orderable": false,
		},{
			  "targets": [0,1,5,6],
			  "className": 'text-center'
		}],
		pageLength: 10,
        responsive: true,
        createdRow:function(row){
			$(row).addClass('breakdown-detail');
        },
		"dom": 'lT<"dt-toolbar">fgtip'
	});


	$("#chkSelectAll").click(function(){
	    $('input:checkbox').not(this).prop('checked', this.checked);
	    InitializeButtonEvent();
	});

	$('body').on('click','[name="chkSelect[]"]',function(){
		InitializeButtonEvent();
	});

	$('.dataTables-example').on('click','.delete-button',function(e){
		e.preventDefault();
		var doc_no = $(this).data('id');
		swal({
                title: "Are you sure?",
                text: "Item ( " + $(this).data('desc') + " ) will be deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Delete it!",
                closeOnConfirm: false
            }, function (isConfirm){
            	if(isConfirm){
            		$.ajax({
						type: 'GET',
						dataType: 'json',
						url: global.site_name + 'master_file/item_master/delete',
						data : {	'id'		: doc_no,
									'action'	: 'delete'},
						success : function(data){
							if(data.success == 1){
								swal({
									title: "Delete Success",
									type:  "success"
								});
								setInterval(function(){
									location.reload();	
								}, 1500);
								
							}
							else{
								swal({
									title: 	"Record can't be deleted",
									text: 	"Data is in used",
									type:  	"error"
								});
							}
						}
					});
            	}            	
            });
		
	});

	$('.dataTables-example').on('click', 'a.soh-breakdown', function(){
		var current_row = $(this).closest('.breakdown-detail');
		var item_num = current_row.find('a.soh-breakdown').data('item-no');
		var item_desc = current_row.find('a.soh-breakdown').data('item-desc');
		var uom = current_row.find('a.soh-breakdown').data('uom');		
		$('#soh-detail-list tbody').html('');
		$('#soh-detail-list tbody').html('<tr><td colspan="3"><div class="spiner-example"><div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div></div></td></tr>');		
		$('#soh-detail').modal('show');
		$('[name="IM_Item_Id"]').val(item_num);
		$('[name="IM_Sales_Desc"]').val(item_desc);
		$('[name="AD_Code"]').val(uom);

		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: global.site_name + 'master_file/item_master/getSOHBreakdown?id='+ item_num,
			success : function(result){
				var html = '';
				setTimeout(function(){
					if(result.length != 0){
						$(result).each(function(index){

							html += '<tr class="soh-details">' + 
									   	'<td class="text-center" data-item-num="' + result[index].IL_Location + '">' + result[index].SP_StoreName + '</td>' +
									   	'<td class="text-center" data-item-desc="' + result[index].IL_SubLocation + '">' + result[index].IL_SubLocation + '</td>' +
									   	'<td class="text-right" data-bom-qty="' + result[index].IL_Qty +' ">' + number_format(result[index].IL_Qty, 4) + '</td>' +
								   	'</tr>';
						});
					}
					else{
						html += '<tr class="soh-details">' +
									'<td colspan="3" class="text-center">No Records Found</td>' +
								'</tr>';
					}
					
					$('#soh-detail-list tbody').html(html);
				},500)
				
			}
		});
		
	});

	//ACTIVATE SELECTED DATA
	$('#btnActive').on('click',function(e){
		e.preventDefault();
		status_update('active','Activate','#1B7BB7');
	});

	//ACTIVATE SELECTED DATA
	$('#btnDeactive').on('click',function(e){
		e.preventDefault();
		status_update('deactivate','Deactivate','#EB4757');
	});
	
 });


function status_update(action,msg,btnColor){
	swal({
        title: "Are you sure?",
        text: "This will " + msg + " all selected data",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: btnColor,
        confirmButtonText: "Yes, " + msg + " it!",
        closeOnConfirm: false
    }, function (isConfirm){
    	if(isConfirm){
    		// Looping get all array checkbox
    		var id = [];
    		$.each($('input[type="checkbox"]:checked'),function(){
    			if($(this).val() != 'on'){
    				id.push($(this).val());
    			}
    		});

    		// Request
    		$.ajax({
				type: 'POST',
				dataType: 'json',
				url: global.site_name + 'master_file/item_master/action',
				data : {	'id'		: id,
							'action'	: action},
				success : function(data){
					if(data.success == 1){
						swal({
							title: msg + " Successful",
							type:  "success"
						});
						setInterval(function(){
							location.reload();	
						}, 1500);
						
					}
				}
			});
    	}            	
    });
}

function InitializeButtonEvent(){
	let intCheck = 0;
    $.each($('input[type="checkbox"]:checked'),function(){
		if($(this).val() != 'on'){
			intCheck += 1;
		}
	});

	if(intCheck > 0){
		$('#btnActive').addClass('btn-outline').removeAttr('disabled');
		$('#btnDeactive').addClass('btn-outline').removeAttr('disabled');
	}
	else if(intCheck == 0){
		$('#btnActive').removeClass('btn-outline').attr('disabled', true);
		$('#btnDeactive').removeClass('btn-outline').attr('disabled', true);	
	}
}