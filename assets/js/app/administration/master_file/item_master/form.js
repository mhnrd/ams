$(document).ready(function(){
	

	function InitializeUI(){

		$.fn.modal.Constructor.prototype.enforceFocus = function() {};

		if(output_type == 'view'){
			$('#supplier-detail-list').removeClass('table-striped');
			$('#uom-detail-list').removeClass('table-striped');
			$('[name="IM_UPCCode"]').attr('readonly',true);
			$('[name="IM_Sales_Desc"]').attr('readonly',true);
			$('[name="IM_Purchased_Desc"]').attr('readonly',true);
			$('[name="IM_Short_Desc"]').attr('readonly',true);
			$('[name="IM_ManufacturerPartNo"]').attr('readonly',true);
			$('[name="IM_Model"]').attr('readonly',true);
			$('[name="IM_Weight"]').attr('readonly',true);
			$('[name="IM_SalePrice"]').attr('readonly',true);
			$('[name="IM_UnitCost"]').attr('readonly',true);
			$('[name="IM_UnitPrice"]').attr('readonly',true);
			$('[name="IM_Item_Waste"]').attr('readonly',true);
			$('[name="IM_ExpiryDays"]').attr('readonly',true);
			if(item_isScrap == true){
				$('[name="scrap-pct"]').attr('readonly', true);
			}
			// $('[name="TR_TransferTo"]').attr('readonly',true).removeClass('js-select2');
			// $('[name="TR_TransferFrom"]').attr('readonly',true).removeClass('js-select2');
		}

		if(output_type == 'add'){
			$('[name="chkScrap"]').prop('checked',false);
		}

		if(output_type == 'update'){
			if(item_isScrap == true){
				$('[name="IM_FK_ItemType_id"]').prop('disabled', true);
				$('[name="IM_FK_Category_id"]').prop('disabled', true);
				$('[name="IM_FK_SubCategory_id"]').prop('disabled', true);
				$('[name="IM_ProductionArea"]').prop('disabled', true);
				$('[name="IM_Item_Waste"]').attr('readonly', true);
				$('[name="IM_ExpiryDays"]').attr('readonly', true);
				$('[name="IM_FK_Attribute_UOM_id"]').prop('disabled', true);
				$('[name="IM_SalesUOM"]').prop('disabled', true);
				$('[name="IM_PurchaseUOM"]').prop('disabled', true);
				$('[name="IM_Production_UOM_id"]').prop('disabled', true);
				$('[name="IM_Transfer_UOM_id"]').prop('disabled', true);
				$('[name="IM_OF_UOM_id"]').prop('disabled', true);
				$('[name="IM_INVPosting_Group"]').prop('disabled', true);
				$('[name="IM_VATProductPostingGroup"]').prop('disabled', true);
				$('[name="IM_WHTProductPostingGroup"]').prop('disabled', true);
				$('#add-supp').hide();
				$('#add-uom').hide();
				$('.edit-uom-detail').hide();
				$('.delete-uom-detail').hide();
				$('.edit-supplier-detail').hide();
				$('.delete-supplier-detail-detail').hide();
			}
		}		

		if(output_type != 'view'){
			
			$(".item").select2();
			$(".js-select2").select2();
			$("[name=IM_Production_UOM_id]").select2({
				placeholder: 'Choose Production UOM',
				allowClear: true
			});
			$("[name=IM_Transfer_UOM_id]").select2({
				placeholder: 'Choose Transfer UOM',
				allowClear: true
			});
			$("[name=IM_OF_UOM_id]").select2({
				placeholder: 'Choose Order Forecast UOM',
				allowClear: true
			});
			$("[name=IM_ProductionArea]").select2({
				placeholder: 'Choose Production Area',
				allowClear: true
			});
			$(".scrap-select2").select2(item_detail.item_search('IM_Sales_Desc', 'Select item'));


			$('.i-checks').iCheck({
	            checkboxClass: 'icheckbox_square-green'
	        });

		}

        if(output_type == 'add'){
	        $('[name="IM_FK_Category_id"]').select2("enable",false);
	        $('[name="IM_FK_SubCategory_id"]').select2("enable",false);
	        $('#add-uom').css('display', 'none');
	        $('[name="IM_ProductionArea"]').select2('enable', false);
	        $(".not-required-field").select2('val', '');
        }

        if(output_type == 'update'){
        	if($('#uom-detail-list tbody tr').length > 0){
				$('[name="IM_FK_Attribute_UOM_id"]').prop('disabled', true);
			}
			else{
				$('[name="IM_FK_Attribute_UOM_id"]').prop('disabled', false);
			}

			if($('#uom-detail-list tbody tr').length > 0){
				var html 	= '';
				var html2 	= '';
				var html3 	= '';
				
				$('#uom-detail-list tbody tr').each(function(){
					var uom = $(this).find('[data-uom-name]').data('uom-name');
					var uom_id = $(this).find('[data-uom]').data('uom');
					html += '<option value="' + uom_id + '" '+ (prod_uom == uom_id ? 'selected' : '') +'>' + uom + '</option>';
				});

				$('#uom-detail-list tbody tr').each(function(){
					var uom = $(this).find('[data-uom-name]').data('uom-name');
					var uom_id = $(this).find('[data-uom]').data('uom');
					html2 += '<option value="' + uom_id + '" '+ (trans_uom == uom_id ? 'selected' : '') +'>' + uom + '</option>';
				});

				$('#uom-detail-list tbody tr').each(function(){
					var uom = $(this).find('[data-uom-name]').data('uom-name');
					var uom_id = $(this).find('[data-uom]').data('uom');
					html3 += '<option value="' + uom_id + '" '+ (of_uom == uom_id ? 'selected' : '') +'>' + uom + '</option>';
				});

				$('select[name="IM_Production_UOM_id"]').append(html).trigger('change');
				$('select[name="IM_Transfer_UOM_id"]').append(html2).trigger('change');
				$('select[name="IM_OF_UOM_id"]').append(html3).trigger('change');
			}
        }
	}

	function InitializeEvent(){

		$('[name="chkScrap"]').change(function(){
			if(this.checked){
				$('[name="chkScrap"]').val(true);
				$('.source-fg').show();
			}
			else{
				$('[name="chkScrap"]').val(false);
				$('[name="IM_Scrap_Item"]').select2('val', null);
				$('[name="scrap-pct"]').val(null);
				$('.source-fg').hide();
			}
		});
		
		$('[name="IM_FK_ItemType_id"]').on('change',function(e){			
			loadCategory();
		});

		$('[name="IM_Scrap_Item"]').change(function(){
			var item_num = $('[name="IM_Scrap_Item"]').val();

			if(item_num == ''){
				$('[name="IM_FK_ItemType_id"]').select2('val', null);
				$('[name="IM_FK_ItemType_id"]').prop('disabled', false);
				$('[name="IM_FK_Category_id"]').select2('val', null);
				$('[name="IM_FK_SubCategory_id"]').select2('val', null);
				$('[name="IM_ProductionArea"]').select2("val", null);
				$('[name="IM_Item_Waste"]').val(number_format(0, 2, ".", ""));			
				$('[name="IM_ExpiryDays"]').val(number_format(0, 0, ".", ""));	
				$('[name="IM_FK_Attribute_UOM_id"]').select2("val", null);
				$('[name="IM_FK_Attribute_UOM_id"]').prop('disabled', false);	
				$('[name="IM_SalesUOM"]').select2("val", null);	
				$('[name="IM_SalesUOM"]').prop('disabled', false);
				$('[name="IM_PurchaseUOM"]').select2("val", null);	
				$('[name="IM_PurchaseUOM"]').prop('disabled', false);
				$('[name="IM_Production_UOM_id"]').select2("val", null);
				$('[name="IM_Production_UOM_id"]').prop('disabled', false);	
				$('[name="IM_Transfer_UOM_id"]').select2("val", null);
				$('[name="IM_Transfer_UOM_id"]').prop('disabled', false);
				$('[name="IM_OF_UOM_id"]').select2("val", null);
				$('[name="IM_OF_UOM_id"]').prop('disabled', false);
				$('#supplier-detail-list tbody tr').remove();
				$('#uom-detail-list tbody tr').remove();
				$('[name="IM_INVPosting_Group"]').select2("val", null);
				$('[name="IM_INVPosting_Group"]').prop('disabled', false);
				$('[name="IM_VATProductPostingGroup"]').select2("val", null);
				$('[name="IM_VATProductPostingGroup"]').prop('disabled', false);
				$('[name="IM_WHTProductPostingGroup"]').select2("val", null);
				$('[name="IM_WHTProductPostingGroup"]').prop('disabled', false);
				setTimeout(function(){
					$('[name="IM_Item_Id"]').val('');
				}, 1000)
				$('#add-supp').show()
				$('#add-uom').css('display', 'none');
				$('[name="IM_Production_UOM_id"]').empty();
				$('[name="IM_Production_UOM_id"]').html('<option></option>');
				$('[name="IM_Production_UOM_id"]').prop('disabled', false);
				$('[name="IM_Transfer_UOM_id"]').empty();
				$('[name="IM_Transfer_UOM_id"]').html('<option></option>');
				$('[name="IM_Transfer_UOM_id"]').prop('disabled', false);
			}
			else{
				$.ajax({
					url : global.site_name + 'master_file/item-master/copyItemDetail',
					type : 'POST',
					dataType : 'json',
					data : {
						'item-no' : item_num
					},
					success : function(result){
						$('[name="IM_FK_ItemType_id"]').select2('val', result.IM_FK_ItemType_id);
						$('[name="IM_FK_ItemType_id"]').trigger('change');
						$('[name="IM_FK_Category_id"]').trigger('change');
						setTimeout(function(){
							$('[name="IM_FK_Category_id"]').select2('val', result.IM_FK_Category_id);
							$('[name="IM_FK_Category_id"]').prop('disabled', true);
						}, 1000)
						$('[name="IM_FK_SubCategory_id"]').trigger('change');
						setTimeout(function(){
							$('[name="IM_FK_SubCategory_id"]').select2('val', result.IM_FK_SubCategory_id);
							$('[name="IM_FK_SubCategory_id"]').prop('disabled', true);
							$('[name="IM_ProductionArea"]').select2("val", result.IM_ProductionArea);
							$('[name="IM_ProductionArea"]').prop('disabled', true);
						}, 1500)
						$('[name="IM_FK_ItemType_id"]').prop('disabled', true);
						$('[name="IM_Item_Waste"]').val(number_format(result.IM_Item_Waste, 2, ".", ""));
						$('[name="IM_Item_Waste"]').attr('readonly', true);
						$('[name="IM_ExpiryDays"]').val(number_format(result.IM_ExpiryDays, 0, ".", ""));
						$('[name="IM_ExpiryDays"]').attr('readonly', true);
						$('[name="IM_FK_Attribute_UOM_id"]').select2("val", result.IM_FK_Attribute_UOM_id);	
						$('[name="IM_FK_Attribute_UOM_id"]').trigger('change');	
						$('[name="IM_FK_Attribute_UOM_id"]').prop('disabled', true);	
						$('[name="IM_SalesUOM"]').select2("val", result.IM_SalesUOM);
						$('[name="IM_SalesUOM"]').prop('disabled', true);	
						$('[name="IM_PurchaseUOM"]').select2("val", result.IM_PurchaseUOM);
						$('[name="IM_PurchaseUOM"]').prop('disabled', true);
						$('[name="IM_INVPosting_Group"]').select2("val", result.IM_INVPosting_Group);
						$('[name="IM_INVPosting_Group"]').prop('disabled', true);
						$('[name="IM_VATProductPostingGroup"]').select2("val", result.IM_VATProductPostingGroup);
						$('[name="IM_VATProductPostingGroup"]').prop('disabled', true);
						$('[name="IM_WHTProductPostingGroup"]').select2("val", result.IM_WHTProductPostingGroup);
						$('[name="IM_WHTProductPostingGroup"]').prop('disabled', true);
						$('#add-supp').hide();
						$('#add-uom').hide();
						// $('[name="IM_FK_Category_id"]').prop('disabled', true);
						// $('[name="IM_FK_SubCategory_id"]').prop('disabled', true);
						// $('[name="IM_ProductionArea"]').prop('disabled', true);
					}
				});

				$.ajax({
					url : global.site_name + 'master_file/item-master/SupplierList',
					type : 'POST',
					dataType : 'json',
					data : {
						'item-no' : item_num
					},
					success : function(result){
						$('#supplier-detail-list tbody tr').remove();
						
						$.each(result, function(index){
							var html = '';

							html = '<tr class="success supplier-detail" data-action="'+ output_type +'" data-line-no="'+ index+1 +'">' +
										'<td>&nbsp;</td>' +
										'<td data-supplier-id="'+ result[index].IS_FK_Suppllier_id +'" data-supplier-name="'+ result[index].S_Name +'">' + result[index].S_Name + '</td>' +
										'<td data-supplier-item-code="'+ result[index].IS_SupplierItemCode +'">' + result[index].IS_SupplierItemCode + '</td>' + 
										'<td data-old-item-code="'+ result[index].IS_OldItemCode +'">' + result[index].IS_OldItemCode + '</td>' +
										'<td class="text-right" data-unit-cost="'+ result[index].IS_LastPOCost +'">' + result[index].IS_LastPOCost + '</td>' +
										'<td class="text-center" data-status="' + ((result[index].IS_Active == 1) ? "true" : "false") +'">' + ((result[index].IS_Active == 1) ? 'Active' : 'Inactive' ) + '</td>' +
									'</tr>';

							$('#supplier-detail-list tbody').append(html);
						});
					}
				});

				$.ajax({
					url : global.site_name + 'master_file/item-master/UOMList',
					type : 'POST',
					dataType : 'json',
					data : {
						'item-no' : item_num
					},
					success : function(result){
						$('#uom-detail-list tbody tr').remove();

						$.each(result, function(index){
							var html = '';
							var option = ''

							html = '<tr class="success uom-detail" data-action="'+ output_type +'" data-line-no="'+ index+1 +'">' +
										'<td>&nbsp;</td>' +
										'<td data-uom-name="'+ result[index].AD_Desc +'" data-uom="'+ result[index].IUC_FK_UOM_id +'" class="text-center">' + result[index].AD_Desc + '</td>' +
										'<td class="text-right" data-quantity="'+ result[index].IUC_Quantity +'">' + number_format(result[index].IUC_Quantity, 4) + '</td>' + 
									'</tr>';

							option = '<option value="'+ result[index].IUC_FK_UOM_id +'">'+ result[index].AD_Desc +'</option>';

							$('#uom-detail-list tbody').append(html);

							$('[name="IM_Production_UOM_id"]').append(option);
							$('[name="IM_Transfer_UOM_id"]').append(option);
							$('[name="IM_OF_UOM_id"]').append(option);
						});

					}
				});

				$.ajax({
					url : global.site_name + 'master_file/item-master/getProdTransOFUom',
					type : 'POST',
					dataType : 'json',
					data : {
						'item-no' : item_num
					},
					success : function(output){

						$('[name="IM_Production_UOM_id"]').select2("val", output.IM_Production_UOM_id);
						$('[name="IM_Transfer_UOM_id"]').select2("val", output.IM_Transfer_UOM_id);
						$('[name="IM_OF_UOM_id"]').select2("val", output.IM_OF_UOM_id);

						$('[name="IM_Production_UOM_id"]').prop('disabled', true);
						$('[name="IM_Transfer_UOM_id"]').prop('disabled', true);
						$('[name="IM_OF_UOM_id"]').prop('disabled', true);
					}
				});
			}
		});

		$('[name="IM_FK_Category_id"]').on('change',function(e){
			if($('[name="IM_FK_Category_id"]').val() == 'FG'){
		    	$('[name="IM_ProductionArea"]').select2("enable",true);
		    }
		    else{
		    	$('[name="IM_ProductionArea"]').select2("enable",false);
		    	$('[name="IM_ProductionArea"]').select2("val", '');
		    }
			loadSubCategory();
		});

		$('[name="IM_FK_SubCategory_id"]').on('change',function(e){
			getItemTypeSeries();
		});

		// Saving/Updating of Item
		$('#save').click(function(){

			var action = $(this).data('todo');
			swal({
                title: "Are you sure?",
                text: (action == 'add')? "You will add the current Item Master." : "You will update the current Item Master document.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#1AB394",
                confirmButtonText: (action == 'add')? "Save" : "Update",
                closeOnConfirm: false
            }, function () {

            	// use for validation of fields.
            	var blnValidate = validateFields();
            	if(!blnValidate){
            		swal("Field Required", "Please complete all required fields.", "error");
            	 	return;
            	}

            	var isScrap = $('[name="chkScrap"]').val();	
            	console.log($('[name="chkScrap"]').val());

				var supplier_data_details = [];
				$('#supplier-detail-list > tbody  > tr').each(function(){
					supplier_data_details.push({
						'todo' 						: $(this).data("action")
					,	'line-no' 					: $(this).data("line-no")
					,	'supplier-id'				: $(this).find("[data-supplier-id]").data("supplier-id")
					,	'supplier-name'				: $(this).find("[data-supplier-name]").data("supplier-name")
					,	'supplier-item-code'		: $(this).find("[data-supplier-item-code]").data("supplier-item-code")
					,	'old-item-code'				: $(this).find("[data-old-item-code]").data("old-item-code")
					,	'unit-cost'					: parseFloat($(this).find("[data-unit-cost]").data("unit-cost"))
					,	'status'					: $(this).find("[data-status]").data("status")
					});
				});

				var uom_data_details = [];
				$('#uom-detail-list > tbody  > tr').each(function(){
					uom_data_details.push({
						'todo' 						: $(this).data("action")
					,	'line-no' 					: $(this).data("line-no")
					,	'uom'						: $(this).find("[data-uom]").data("uom")
					,	'quantity'					: parseFloat($(this).find("[data-quantity]").data("quantity"))
					});
				});

				if(output_type == 'add'){
					var IM_FK_ItemType_id 		= $('[name="IM_FK_ItemType_id"]').val();
					var IM_FK_Category_id 		= $('[name="IM_FK_Category_id"]').val();
					var IM_FK_SubCategory_id 	= $('[name="IM_FK_SubCategory_id"]').val();
					var IM_ProductionArea 		= $('[name="IM_ProductionArea"').val();
				}
				else{
					var IM_FK_ItemType_id 		= $('[name="IM_FK_ItemType_id"]').data('itemtype-id');
					var IM_FK_Category_id 		= $('[name="IM_FK_Category_id"]').data('category-id');
					var IM_FK_SubCategory_id 	= $('[name="IM_FK_SubCategory_id"]').data('subcategory-id');
					var IM_ProductionArea 		= $('[name="IM_ProductionArea"').val();	
				}

				if(isScrap == 'true'){
					var IM_Scrap 			= isScrap;
					var IM_Scrap_Item_Id	= $('[name="IM_Scrap_Item"]').val();
					var IM_Scrap_Perc		= $('[name="scrap-pct"]').val();

				}
				else{
					var IM_Scrap 			= isScrap;
					var IM_Scrap_Item_Id	= null;
					var IM_Scrap_Perc		= 0;
				} 
				console.log(IM_Scrap_Item_Id);
				console.log(IM_Scrap_Perc);
						
				// SAVING FUNCTION OF WHOLE HEADER AND DETAIL
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: global.site_name + 'master_file/item_master/save',
					data : {
						'todo'									: $('#save').data('todo'),
						'IM_FK_ItemType_id'						: IM_FK_ItemType_id,
						'IM_FK_Category_id'						: IM_FK_Category_id,
						'IM_FK_SubCategory_id'					: IM_FK_SubCategory_id,
						'IM_ProductionArea'						: IM_ProductionArea,
						'IM_Item_Id'							: $('[name="IM_Item_Id"]').val(),
						'IM_UPCCode'							: $('[name="IM_UPCCode"]').val(),
						'IM_Sales_Desc'							: $('[name="IM_Sales_Desc"]').val(),
						'IM_Purchased_Desc'						: $('[name="IM_Purchased_Desc"]').val(),
						'IM_Short_Desc'							: $('[name="IM_Short_Desc"]').val(),
						'IM_ManufacturerPartNo'					: $('[name="IM_ManufacturerPartNo"]').val().trim(),
						'IM_Model'								: $('[name="IM_Model"]').val(),
						'IM_Weight'								: $('[name="IM_Weight"]').val(),
						'IM_FK_Attribute_UOM_id'				: $('[name="IM_FK_Attribute_UOM_id"]').val(),
						'IM_Production_UOM_id'					: $('[name="IM_Production_UOM_id"]').val(),
						'IM_Transfer_UOM_id'					: $('[name="IM_Transfer_UOM_id"]').val(),
						'IM_OF_UOM_id'							: $('[name="IM_OF_UOM_id"]').val(),
						'IM_SalesUOM'							: $('[name="IM_SalesUOM"]').val(),
						'IM_PurchaseUOM'						: $('[name="IM_PurchaseUOM"]').val(),
						'IM_CostOfGoods'						: parseFloat($('[name="IM_CostOfGoods"]').val()),
						'IM_UnitCost'							: parseFloat($('[name="IM_UnitCost"]').val()),
						'IM_UnitPrice'							: parseFloat($('[name="IM_UnitPrice"]').val()),
						'IM_SalePrice'							: parseFloat($('[name="IM_SalePrice"]').val()),
						'IM_Item_Waste'							: parseFloat($('[name="IM_Item_Waste"]').val()),
						'IM_ExpiryDays'							: $('[name="IM_ExpiryDays"]').val(),
						'IM_INVPosting_Group'					: $('[name="IM_INVPosting_Group"]').val(),
						'IM_VATProductPostingGroup'				: $('[name="IM_VATProductPostingGroup"]').val(),
						'IM_WHTProductPostingGroup'				: $('[name="IM_WHTProductPostingGroup"]').val(),
						'IM_Serialize'							: $('[name="IM_Serialize"]').val(),
						'IM_Imported'							: $('[name="IM_Imported"]').val(),
						'IM_PAR_Item'							: $('[name="IM_PAR_Item"]').val(),
						'IM_Scrap'								: IM_Scrap, 
						'IM_Scrap_Item_Id'						: IM_Scrap_Item_Id, 
						'IM_Scrap_Perc'							: IM_Scrap_Perc, 
						'supplier-list'							: supplier_data_details,
						'uom-list'								: uom_data_details

					},
					success : function(result){
						if(result.success){
							swal( (action == 'add') ? "Saving Success!" : "Updating Success!", (action == 'add') ? "Successfully saved Item Master document!" : "Successfully update Item Master document!", "success")
							setTimeout(function(){
								window.location.href = global.site_name + "master_file/item_master";									
							},1500);
						}
						else{
							if(result.message == 'item-no-null'){
								swal("Saving Failed!", "Document No cannot be empty!", "error")
							}
							else if(result.message == 'item-no-exists'){
								swal("Saving Failed!", "Error! Document already exists.", "error")
							}
							else if(result.message == 'item-scrap-inc'){
								swal("Saving Failed!", "Error! Incomplete Scrap Details.", "error")
							}
							else{
								swal("Saving Failed!", "Error! Contact your administrator.", "error")	
							}
							
						}
					}
				});
            });
		});

		$('[name="IM_FK_Attribute_UOM_id"]').on('change',function(e){
			var uom = $('[name="IM_FK_Attribute_UOM_id"]').val();
			console.log(uom);
			if(uom == null || uom == ''){
				$('#add-uom').hide();
			}
			else{
				$('#add-uom').show();
			}

			// Clear Value of UOM conversion List
			var table_list = $('#uom-detail-list > tbody  > tr');
			if(table_list.length > 0){
				$('#uom-detail-list > tbody').remove();
			}

		});

		$("#add-supp").click(function(){
			supplier_detail.show_modal('add');
		});

		$('#supplier-detail #add-new').click(function(){
			if(supplier_detail.validate_detail()){
				if($(this).val() == 'add'){
					
					supplier_detail.add_detail();
					supplier_detail.clear_form();
				}
				else if($(this).val() == 'update'){
					
					supplier_detail.edit_detail();
					supplier_detail.clear_form();
				}
				
			}
			
		});

		$('#supplier-detail #add-close').click(function(){
			if(supplier_detail.validate_detail()){
				if($(this).val() == 'add'){
					supplier_detail.add_detail();
					$("#supplier-detail .close").click();
				}
				else if($(this).val() == 'edit'){
					supplier_detail.edit_detail();
					$("#supplier-detail .close").click();
				}
				else if($(this).val() == 'readd' ){
					supplier_detail.edit_detail($(this).val());
					$("#supplier-detail .close").click();
				}
			}
			
		});

		$('#supplier-detail .item').on("select2:select", function(e) {
			
			if($(this).select2('data')[0] === undefined && e['params']['data'].id == '') return;
			var type = $(this).data('type');
			var data_att = $(this).select2('data')[0];
			
			// Data from Row Field
			var select_param = e['params'];
			// Array for Adding / Editing
			var item_data = [];
			if($(this).select2('data')[0] === undefined){
				item_data = {
					 'data-id' 		: select_param['data'].itemid
					,'data-text' 	: select_param['data'].desc
				};
			}
			else{
				item_data = {
					 'data-id' 		: data_att['id']
					,'data-text' 	: data_att['data-text']
				};
			}			
			
			// Load Data from Array
			if(select_param['data'].todo == "edit"){
				$("#supplier-detail #supplier").html('<option value="' + item_data['data-id']+ '" selected ="selected">' + $.trim(select_param['data'].desc) + ' </option>');
			}
			else{
				$("#supplier-detail #supplier").html('<option value="' + item_data['data-id']+ '" selected ="selected">' + item_data['data-text'] + ' </option>');
			}
			$("#supplier-detail #supplier").select2("val", item_data['data-id']);	
			

			
		});

		//Events goes here from supplier_detail
		supplier_detail.reload_row_events();
		supplier_detail.supplier_detail_item_event();

		// Events goes here from UOM Detail
		$("#add-uom").click(function(){
			uom_detail.show_modal('add');
		});

		$('#uom-detail #add-new').click(function(){
			if(uom_detail.validate_detail()){
				if($(this).val() == 'add'){
					
					uom_detail.add_detail();
					uom_detail.clear_form();
				}
				else if($(this).val() == 'update'){
					
					uom_detail.edit_detail();
					uom_detail.clear_form();
				}
				
			}
			
		});

		$('#uom-detail #add-close').click(function(){
			if(uom_detail.validate_detail()){
				if($(this).val() == 'add'){
					uom_detail.add_detail();
					uom_detail.clear_form();
					$("#uom-detail .close").click();
				}
				else if($(this).val() == 'edit'){
					uom_detail.edit_detail();
					uom_detail.clear_form();
					$("#uom-detail .close").click();
				}
				else if($(this).val() == 'readd' ){
					uom_detail.edit_detail($(this).val());
					uom_detail.clear_form();
					$("#uom-detail .close").click();
				}
			}
			
		});

		$('#uom-detail .uom').on("select2:select", function(e) {
			
			if($(this).select2('data')[0] === undefined && e['params']['data'].id == '') return;
			var type = $(this).data('type');
			var data_att = $(this).select2('data')[0];
			
			// Data from Row Field
			var select_param = e['params'];
			// Array for Adding / Editing
			var item_data = [];
			if($(this).select2('data')[0] === undefined){
				item_data = {
					 'data-id' 		: select_param['data'].itemid
					,'data-text' 	: select_param['data'].desc
				};
			}
			else{
				item_data = {
					 'data-id' 		: data_att['id']
					,'data-text' 	: data_att['data-text']
				};
			}
			
			
			// Load Data from Array
			if(select_param['data'].todo == "edit"){
				$("#uom-detail #uom-list").html('<option value="' + item_data['data-id']+ '" selected ="selected">' + $.trim(select_param['data'].desc) + ' </option>');
			}
			else{
				$("#uom-detail #uom-list").html('<option value="' + item_data['data-id']+ '" selected ="selected">' + item_data['data-text'] + ' </option>');
			}
			$("#uom-detail #uom-list").select2("val", item_data['data-id']);	
						
		});

		

		uom_detail.reload_row_events();
		uom_detail.uom_detail_item_event();
		
	}

	function loadCategory(itemType){

		var itemType = $('[name="IM_FK_ItemType_id"] option:selected').val();
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: global.site_name + 'master_file/item_master/getCategory',
			data: {ItemType: itemType},
			success : function(result){					
				var list = {
					'html'		: ''
				,	'format'	: function(data){
						return '<option value="' + data.CAT_id + '">' + data.CAT_Desc + '</option>';
					}
				,	'refresh'	: function(){
						$('[name="IM_FK_Category_id"]').empty();
						list.html+= '<option selected disabled></option>';
						result.forEach(function(item, index){
							list.html+=list.format(item);							
						});
						$('[name="IM_FK_Category_id"]').append(list.html);
						$('[name="IM_FK_Category_id"]').select2();													
					}
				}				
				list.refresh();	

				 // Event Change
			    $('[name="IM_FK_Category_id"]').select2("enable",true);
			    $('[name="IM_FK_SubCategory_id"]').empty();
			    $('[name="IM_FK_SubCategory_id"]').select2("enable",false);
			    $('[name="IM_Item_Id"]').val('');
			    $('#save').data('doc-no','');
			}
		});
		
	}

	function loadSubCategory(itemType){
		
		var category = $('[name="IM_FK_Category_id"] option:selected').val();
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: global.site_name + 'master_file/item_master/getSubCategory',
			data: {CategoryID: category},
			success : function(result){					
				var list = {
					'html'		: ''
				,	'format'	: function(data){
						return '<option value="' + data.SC_Id + '">' + data.SC_Description + '</option>';
					}
				,	'refresh'	: function(){
						$('[name="IM_FK_SubCategory_id"]').empty();
						list.html+= '<option selected disabled></option>';
						result.forEach(function(item, index){
							list.html+=list.format(item);							
						});
						$('[name="IM_FK_SubCategory_id"]').append(list.html);
						$('[name="IM_FK_SubCategory_id"]').select2();													
					}
				}				
				list.refresh();	

				 // Event Change
			    $('[name="IM_FK_SubCategory_id"]').select2("enable",true);
			    $('[name="IM_Item_Id"]').val('');
			    $('#save').data('doc-no','');

			}
		});
	}

	function getItemTypeSeries(){

		var category = $('[name="IM_FK_Category_id"] option:selected').val();
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: global.site_name + 'master_file/item_master/getItemNumber',
			data: {ItemType: category},
			success : function(result){
				$('[name="IM_Item_Id"]').val(result);				
				document.getElementById('save').dataset['docNo'] = result;
			}
		});
		
	}

	var item_detail = {
		item_search: function(filter, placeholder='Enter Item Here'){
			return {
				placeholder: placeholder,
				minimumInputLength: 3,
				allowClear: true,
				ajax: {
					url: global.site_name + 'master_file/item_master/getScrapParent',
					dataType: 'json',
					delay: 250,
					data: function (params) {
					  var queryParameters = {
								'filter-type'	: filter
							,	'filter-search'	: params.term
							,	'action'		: 'search'
							,	'category-type'	: ''
					    }
					    return queryParameters;
					},
					processResults: function (data, params) {
						return {
							results: $.map(data, function(item) {
					            return {
					                id: item.IM_Item_Id,
					                text:item[filter],
					                'data-text':item.IM_Sales_Desc,
					                'data-id':item.IM_Item_Id,
					            }
					        })
						};
					}
				}
			}
		}
	}

	var supplier_detail = {
		supplier_detail_item_event :function(){
			$("#supplier-detail #supplier").select2(supplier_detail.item_search('S_Name', "Enter Supplier Name Here"));
		},
		item_search: function(filter, placeholder='Enter Item Here'){
			return {
				placeholder: placeholder,
				minimumInputLength: 3,
				ajax: {
					url: global.site_name + 'master_file/item_master/getSupplier',
					dataType: 'json',
					delay: 250,
					data: function (params) {

						var supplierlist = [];
						$('#supplier-detail-list > tbody  > tr').each(function(){
							supplierlist.push($(this).find("[data-supplier-id]").data("supplier-id"));
						});

						var queryParameters = {
								'filter-type'		: filter
							,	'filter-search'		: params.term
							,	'action'			: 'search'
							,	'supplier-list'		: supplierlist
						}
						return queryParameters;
					},
					processResults: function (data, params) {
						return {
							results: $.map(data, function(item) {
					            return {
					                id: item.S_Id,
					                text:item[filter],
					                'data-text':item.S_Name,
					                'data-id':item.S_Id,
					            }
					        })
						};
					}
				}
			}
		},
		show_modal : function(method){
			supplier_detail.clear_form();
			if(method=='add'){
				$("#supplier-detail").modal('show');
				$("#supplier-detail #add-new").show();
				$("#supplier-detail #add-close").text('Add & Close');
				$("#supplier-detail #add-new").val(method)
				$("#supplier-detail #add-close").val(method)
			}			
			else if(method=='readd' || method=='edit'){
				$("#supplier-detail").modal('show');
				$("#supplier-detail #add-new").hide();
				$("#supplier-detail #add-close").text('Save');
				$("#supplier-detail #add-new").val(method)
				$("#supplier-detail #add-close").val(method)
			}
		},
		row_format	:	function(row){
			var str = row['unit-cost'];
			var res = str.replace(/,/g,"");	
			var status = row['status'] ? 'Active' : 'Inactive';

			return 	'<tr class="' + row['color'] + ' supplier-detail" data-action="' + row['action'] + '" data-line-no="' + row['line-no'] + '">' +
						'<td class="text-center">' +
							'<button class="edit-supplier-detail btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button" style="margin-right:10px;"><i class="fa fa-pencil"></i></button>' + 
							'<button class="delete-supplier-detail btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button"><i class="fa fa-trash-o"></i></button>' +
						'</td>' + 
						'<td data-supplier-id="' + row['supplier-id'] + '" data-supplier-name="' + row['supplier-name'] + '">' + row['supplier-name'] + '</td>' + 
						'<td class="text-center" data-supplier-item-code="' + row['supplier-item-code'] + '">' + row['supplier-item-code'] + '</td>' + 
						'<td data-old-item-code="' + row['old-item-code'] + '">' + row['old-item-code'] + '</td>' + 
						'<td class="text-right" data-unit-cost="' + row['unit-cost'] + '">' + number_format(row['unit-cost'], 2) + '</td>' + 
						'<td class="text-center" data-status="' + row['status'] + '">' + status + '</td>' + 
					'</tr>';
		},		
		reload_row_events	:	function(){
			$('.edit-supplier-detail').unbind();
			$('.delete-supplier-detail').unbind();

			$('.edit-supplier-detail').click(function(){

				var row = $(this).closest('.supplier-detail');
				var todo = row.data('action');
				if(todo!='delete'){
					//For Update
					supplier_detail.show_modal((todo =='add')? 'readd' : 'edit');
					supplier_detail.set_form_data(row);
				}
			});

			$('.delete-supplier-detail').click(function(){
				var current_row = $(this).closest('.supplier-detail');
				//Remove entire rOw
				swal({
		                title: "Are you sure?",
		                text: "The selected record will be deleted.",
		                type: "warning",
		                showCancelButton: true,
		                confirmButtonColor: "#DD6B55",
		                confirmButtonText: "Delete",
		                closeOnConfirm: true
		            }, function (isConfirm){
		            	if(isConfirm){
		            		current_row.remove();
		            	}            	
		        });
					
			});
		},
		add_detail	:	function(){
			var row = {
							'action'						: 	'add'
							,	'supplier-id'				:	$('#supplier-detail #supplier').val()
							,	'supplier-name'				:	$('#supplier-detail #supplier option:selected').text()
							,	'supplier-item-code'		:	$('#supplier-detail #supplier-item-code').val()
							,	'old-item-code'				:	$('#supplier-detail #old-item-code').val()
							,	'unit-cost'					:	$('#supplier-detail #unit-cost').val()						
							,	'status'					:	$('#supplier-detail input[type="checkbox"]').prop("checked")
						};

			row['action'] = 'add';
			row['color'] = 'success';
			row['line-no'] = supplier_detail.get_temp_line_no();
			var rowdetail = supplier_detail.row_format(row);			
			$('#supplier-detail-list tbody').append(rowdetail);
			supplier_detail.reload_row_events();
		},
		edit_detail	: function(isReadd = undefined){
			var row = {
							'action'						: 	'add'
							,	'supplier-id'				:	$('#supplier-detail #supplier').val()
							,	'supplier-name'				:	$('#supplier-detail #supplier option:selected').text()
							,	'supplier-item-code'		:	$('#supplier-detail #supplier-item-code').val()
							,	'old-item-code'				:	$('#supplier-detail #old-item-code').val()
							,	'unit-cost'					:	$('#supplier-detail #unit-cost').val()						
							,	'status'					:	$('#supplier-detail input[type="checkbox"]').prop("checked")
						};

			row['action'] = (isReadd === undefined)? 'update': 'add';
			row['color'] = (isReadd === undefined)? 'info': 'success';
			row['line-no'] = $('#supplier-detail #line-no').val();
			$('.supplier-detail[data-line-no="' + row['line-no'] + '"]').replaceWith(supplier_detail.row_format(row));
			supplier_detail.reload_row_events();
		},
		validate_detail	: function(){
			var isValid = true;
				if(!$("#supplier-detail #supplier").val()){
					swal("Error","Supplier is empty.","error");
					isValid = false;
				}
				if(!$("#supplier-detail #unit-cost").val()){
					swal("Error","Last PO Cost is empty.","error");
					isValid = false;
				}
			return isValid;
		},
		set_form_data	: function(row){
			
			var item_data = {
				"itemid" 	: row.find('[data-supplier-id]').data('supplier-id'),
				"desc" 		: row.find('[data-supplier-name]').data('supplier-name'),
				"unit_cost" : row.find('[data-unit-cost]').data('unit-cost'),
				"todo" 		: "edit"
			};	

			$('#supplier-detail #line-no').val(row.data('line-no'));
			$('#supplier-detail #supplier').select2("val", row.find('[data-supplier-id]').data('supplier-id') );
			// $('#supplier-detail #supplier').trigger("select2:select");	
			$('#supplier-detail #supplier').trigger({
				type: "select2:select",
				params:{
					data : item_data
				}
			});	

			$('#supplier-detail #supplier-item-code').val(row.find('[data-supplier-item-code]').data('supplier-item-code'));
			$('#supplier-detail #old-item-code').val(row.find('[data-old-item-code]').data('old-item-code'));
			$('#supplier-detail #unit-cost').val(number_format(row.find('[data-unit-cost]').data('unit-cost'), 2, '.', ''));
			
			if(row.find('[data-status]').data('status') == true){
				$('#supplier-detail #active').attr('checked',true);
				$('.icheckbox_square-green').addClass('checked');
			}
			else{
				$('#supplier-detail #active').attr('checked',false);
			}
		},
		clear_form	: function(){
			$('#supplier-detail #supplier').select2("val", "");
			$('#supplier-detail #supplier-item-code').val('');
			$('#supplier-detail #unit-cost').data('unit-cost', '0.00');
			$('#supplier-detail #unit-cost').val('0.00');
			$('#supplier-detail #old-item-code').val('');
		},
		get_temp_line_no	: function(){
			var start = 0;
			while($("#supplier-detail tbody").find('[data-line-no="' + ++start + '"]').length > 0){
				//console.log($("#supplier-detail tbody").find('[data-line-no="0"]').length)
			}
			return start;
		}
	};

	/*
		UOM CONVERSION DETAIL EVENT
		MADE BY JEKZEL LEONIDAS 
	*/
	var uom_detail = {
		uom_detail_item_event :function(){
			$("#uom-detail #uom-list").select2(uom_detail.item_search('AD_Code', "Enter UOM Here"));
		},
		item_search: function(filter, placeholder='Enter Item Here'){

			return {
				placeholder: placeholder,
				minimumInputLength: 1,
				ajax: {
					url: global.site_name + 'master_file/item_master/getUOM',
					dataType: 'json',
					delay: 250,
					data: function (params) {

					  	var uomlist = [];
						$('#uom-detail-list > tbody  > tr').each(function(){
							uomlist.push($(this).find("[data-uom]").data("uom"));
						});
						uomlist.push($('[name="IM_FK_Attribute_UOM_id"]').val());

						var queryParameters = {
								'filter-type'	: filter
							,	'filter-search'	: params.term
							,	'action'		: 'search'
							,	'uom-list'		: uomlist
						}
						return queryParameters;
					},
					processResults: function (data, params) {
						return {
							results: $.map(data, function(item) {
					            return {
					                id: item.AD_Id,
					                text:item[filter],
					                'data-text':item.AD_Code,
					                'data-id':item.AD_Id,
					            }
					        })
						};
					}
				}
			}
		},
		show_modal : function(method){
			uom_detail.clear_form();
			if(method=='add'){
				$("#uom-detail").modal('show');
				$("#uom-detail #add-new").show();
				$("#uom-detail #add-close").text('Add & Close');
				$("#uom-detail #add-new").val(method)
				$("#uom-detail #add-close").val(method)
			}			
			else if(method=='readd' || method=='edit'){
				$("#uom-detail").modal('show');
				$("#uom-detail #add-new").hide();
				$("#uom-detail #add-close").text('Save');
				$("#uom-detail #add-new").val(method)
				$("#uom-detail #add-close").val(method)
			}

		},
		row_format	:	function(row){
			return 	'<tr class="' + row['color'] + ' uom-detail" data-action="' + row['action'] + '" data-line-no="' + row['line-no'] + '">' +
						'<td class="text-center">' +
							'<button class="edit-uom-detail btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button"><i class="fa fa-pencil"></i></button> ' + 
							'<button class="delete-uom-detail btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button"><i class="fa fa-trash-o"></i></button>' +
						'</td>' + 
						'<td data-uom-name="' + row['uom'] + '" data-uom="' + row['uom-id'] + '" class="text-center">' + row['uom'] + '</td>' + 
						'<td class="text-right" data-quantity="' + row['quantity'] + '">' + number_format(row['quantity'],4) + '</td>' + 
					'</tr>';
		},		
		reload_row_events	:	function(){
			$('.edit-uom-detail').unbind();
			$('.delete-uom-detail').unbind();

			$('.edit-uom-detail').click(function(){

				var row = $(this).closest('.uom-detail')
				var todo = row.data('action');
				if(todo!='delete'){
					//For Update
					uom_detail.show_modal((todo =='add')? 'readd' : 'edit');
					uom_detail.set_form_data(row);
				}
			});

			$('.delete-uom-detail').click(function(){
				var current_row = $(this).closest('.uom-detail');
				//Remove entire rOw
				swal({
		                title: "Are you sure?",
		                text: "The selected record will be deleted.",
		                type: "warning",
		                showCancelButton: true,
		                confirmButtonColor: "#DD6B55",
		                confirmButtonText: "Delete",
		                closeOnConfirm: true
		            }, function (isConfirm){
		            	if(isConfirm){
		            		current_row.remove();
		            		uom_detail.update_header();
		            		$('[name="IM_Production_UOM_id"]').empty();
							$('[name="IM_Transfer_UOM_id"]').empty();
							$('[name="IM_OF_UOM_id"]').empty();

							if($('#uom-detail-list tbody tr').length > 0){
								
								var html = '';
								
								$('#uom-detail-list tbody tr').each(function(){
									var uom = $(this).find('[data-uom-name]').data('uom-name');
									var uom_id = $(this).find('[data-uom]').data('uom');
									html += '<option value="' + uom_id + '">' + uom + '</option>';
								});

								$('select[name="IM_Production_UOM_id"]').append(html).trigger('change');
								$('select[name="IM_Transfer_UOM_id"]').append(html).trigger('change');
								$('select[name="IM_OF_UOM_id"]').append(html).trigger('change');
							}
							else{
								$('[name="IM_FK_Attribute_UOM_id"]').prop('disabled', false);
								$('[name="IM_Production_UOM_id"]').select2('val',"");
								$('[name="IM_Transfer_UOM_id"]').select2('val',"");
								$('[name="IM_OF_UOM_id"]').select2('val',"");
							}
		            	}            	
		        });
					
			});
		},
		add_detail	:	function(){
			var row = {
							'action'				: 	'add'
						,	'quantity'				:	$('#uom-detail #quantity').val()
						,	'uom'					:	$('#uom-detail #uom-list option:selected').text()
						,	'uom-id'				:	$('#uom-detail #uom-list option:selected').val()
						};

			row['action'] = 'add';
			row['color'] = 'success';
			row['line-no'] = uom_detail.get_temp_line_no();
			var rowdetail = uom_detail.row_format(row);			
			$('#uom-detail-list tbody').append(rowdetail);
			$('[name="IM_Production_UOM_id"]').empty();
			$('[name="IM_Transfer_UOM_id"]').empty();
			$('[name="IM_OF_UOM_id"]').empty();

			if($('#uom-detail-list tbody tr').length > 0){
				$('[name="IM_FK_Attribute_UOM_id"]').prop('disabled', true);
				var html = '';
				
				$('#uom-detail-list tbody tr').each(function(){
					var uom = $(this).find('[data-uom-name]').data('uom-name');
					var uom_id = $(this).find('[data-uom]').data('uom');
					html += '<option value="' + uom_id + '">' + uom + '</option>';
				});

				$('select[name="IM_Production_UOM_id"]').append(html).trigger('change');
				$('select[name="IM_Transfer_UOM_id"]').append(html).trigger('change');
				$('select[name="IM_OF_UOM_id"]').append(html).trigger('change');
			}
			else{
				$('[name="IM_FK_Attribute_UOM_id"]').prop('disabled', false);
			}

			uom_detail.reload_row_events();
		},
		edit_detail	: function(isReadd = undefined){
			var row = {
							'action'				: 	'add'
						,	'quantity'				:	$('#uom-detail #quantity').val()
						,	'uom'					:	$('#uom-detail #uom-list option:selected').text()
						,	'uom-id'				:	$('#uom-detail #uom-list option:selected').val()
						};

			row['action'] = (isReadd === undefined)? 'update': 'add';
			row['color'] = (isReadd === undefined)? 'info': 'success';
			row['line-no'] = $('#uom-detail #line-no').val();
			$('.uom-detail[data-line-no="' + row['line-no'] + '"]').replaceWith(uom_detail.row_format(row));
			uom_detail.reload_row_events();
		},
		validate_detail	: function(){
			var isValid = true;
				if(!$('#uom-detail #uom-list').val()){
					swal("Error","UOM is empty.","error");
					isValid = false;
				}
				if(!$('#uom-detail #quantity').val()){
					swal("Error","Quantity is empty.","error");
					isValid = false;	
				}
				if($('#uom-detail #quantity').val() == 1){
					swal("Error","Quantity Conversion cannot be 1.","error");
					isValid = false;			
				}
				
			return isValid;
		},
		set_form_data	: function(row){
			
			var item_data = {
				"itemid" 	: row.find('[data-uom]').data('uom'),
				"desc" 		: row.find('[data-uom-name]').data('uom-name'),
				"todo" 		: "edit"
			};	

			$('#uom-detail #line-no').val(row.data('line-no'));
			$('#uom-detail #uom-list').select2("val", row.find('[data-uom]').data('uom') );
			$('#uom-detail #uom-list').trigger({
				type: "select2:select",
				params:{
					data : item_data
				}
			});	

			$('#uom-detail #quantity').val(number_format(row.find('[data-quantity]').data('quantity'), 2, '.', ''));	
		},
		clear_form	: function(){
			$('#uom-detail #uom-list').empty();
			$('#uom-detail #uom-list').select2("val", "");
			$('#uom-detail #quantity').val('1.0000');
		},
		get_temp_line_no	: function(){
			var start = 0;
			while($("#uom-detail-list tbody").find('[data-line-no="' + ++start + '"]').length > 0){
				//console.log($("#uom-detail-list tbody").find('[data-line-no="0"]').length)
			}
			return start;
		},
		update_header : function(){
			if($('#uom-detail-list tbody tr').length > 0){
				$('[name="IM_FK_Attribute_UOM_id"]').prop('disabled', true);
			}
			else{
				$('[name="IM_FK_Attribute_UOM_id"]').prop('disabled', false);
			}
		}
	};


	InitializeUI();
	InitializeEvent();
});