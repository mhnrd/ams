$(document).ready(function(){

	function init(){

	$('#btnActive').prop('disabled', true);
	$('#btnDeactive').prop('disabled', true);

	// DEPARTMENT HEADER
	tableindex = $('#department').DataTable( {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'master_file/department/data_D',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0, 1],
			"orderable": false,
		},{
			"targets": [0,1,4],
			"className": 'text-center'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	$('#'+table_id+'_paginate').addClass('pull-right');

	// DEPARTMENT DETAIL 
	tableindex2 = $('#department_detail').DataTable( {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'master_file/department/data_DSD',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		},{
			"targets": [],
			"className": 'text-center'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('table-header');
			//$(row).addClass('del-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	$('#'+table_dtl_id+'_paginate').addClass('pull-right');


	//Department Detail Template Show Modal
	$('#department_detail').on('click', '.add-position-setup',function(){
		$('#position_setup-detail').modal('show');
	});

	
	// SELECT ALL CHECKBOX
	$("#chkSelectAll").click(function(){
		$('input:checkbox').not(this).prop('checked', this.checked);
		   InitializeButtonEvent();
	});

	$('body').on('click','[name="chkSelect[]"]',function(){
		InitializeButtonEvent();
	});

	// DELETE PER ROW
	$('.dataTables-example').on('click', '.delete-button',function(e){
			e.preventDefault();
			var doc_no = $(this).data('id');
			swal({
					title: "Are you sure?",
					text: "Department will be deleted",
					type: "warning",
					showCancelButton: true,
               		confirmButtonColor: "#DD6B55",
                	confirmButtonText: "Yes, Delete it!",
                	closeOnConfirm: false
			 }, function (isConfirm){
            	if(isConfirm){
            		$.ajax({
						type: 'GET',
						dataType: 'json',
						url: global.site_name + 'master_file/department/delete',
						data : {	'id'		: doc_no,
									'action'	: 'delete'},
						success : function(data){
							if(data.success == 1){
								swal({
									title: "Delete Success",
									type:  "success"
								});
								setInterval(function(){
									location.reload();	
								}, 1500);
								
							}
							else{
								swal({
									title: 	"Record can't be deleted",
									text: 	"Data is in used",
									type:  	"error"
								});
							}
						}
					});
            	}            	
            });
		});

	//ACTIVATE SELECTED DATA
	$('#btnActive').on('click',function(e){
		e.preventDefault();
		status_update('active','Activate','#1B7BB7');
	});

	//ACTIVATE SELECTED DATA
	$('#btnDeactive').on('click',function(e){
		e.preventDefault();
		status_update('deactivate','Deactivate','#EB4757');
	});

	}

	init();
});

// ALL SELECTED STATUS UPDATE
function status_update(action,msg,btnColor){
	swal({
        title: "Are you sure?",
        text: "This will " + msg + " all selected data",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: btnColor,
        confirmButtonText: "Yes, " + msg + " it!",
        closeOnConfirm: false
    }, function (isConfirm){
    	if(isConfirm){
    		// Looping get all array checkbox
    		var id = [];
    		$.each($('input[type="checkbox"]:checked'),function(){
    			if($(this).val() != 'on'){
    				id.push($(this).val());
    			}
    		});

    		// Request
    		$.ajax({
				type: 'POST',
				dataType: 'json',
				url: global.site_name + 'master_file/department/action',
				data : {	'id'		: id,
							'action'	: action},
				success : function(data){
					if(data.success == 1){
						swal({
							title: msg + " Successful",
							type:  "success"
						});
						setInterval(function(){
							location.reload();	
						}, 1500);
						
					}
				}
			});
    	}            	
    });
}

// CONDITION FOR THE BTNACTIVE AND BTNDEACTIVE IF THE CHECKBOX IS NOT SELECTED
function InitializeButtonEvent(){
	let intCheck = 0;
    $.each($('input[type="checkbox"]:checked'),function(){
		if($(this).val() != 'on'){
			intCheck += 1;
		}
	});

	if(intCheck > 0){
		$('#btnActive').addClass('btn-outline').removeAttr('disabled');
		$('#btnDeactive').addClass('btn-outline').removeAttr('disabled');
	}
	else if(intCheck == 0){
		$('#btnActive').removeClass('btn-outline').attr('disabled', true);
		$('#btnDeactive').removeClass('btn-outline').attr('disabled', true);	
	}
}
