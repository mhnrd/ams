$(document).ready(function(){

	// Form View Setup - ReadOnly
	if(output_type == 'view'){
		$('[name= "S_DocNo"]').attr('readonly', true);
		$('[name= "S_Name"]').attr('readonly', true);
		$('[name= "S_Address"]').attr('readonly', true);
		$('[name= "S_TelNum"]').attr('readonly', true);
		$('[name= "S_TinNum"]').attr('readonly', true);
		$('[name= "S_Contact"]').attr('readonly', true);
		$('[name= "S_Country"]').attr('readonly', true);
		$('[name= "S_FaxNUm"]').attr('readonly', true);
		$('[name= "S_EmailAdd1"]').attr('readonly', true);
		$('[name= "S_EmailAdd2"]').attr('readonly', true);
		$('[name= "S_Addr_PostalCode"]').attr('readonly', true);
		$('[name= "S_BankAccountNo"]').attr('readonly', true);
		$('[name= "S_BankName"]').attr('readonly', true);
		$('[name= "S_BankAddress"]').attr('readonly', true);
		$('[name= "S_PrintCheckAs"]').attr('readonly', true);
		$('[name= "S_BalanceAsOf"]').attr('readonly',true);
		$('[name= "S_SwiftCode"]').attr('readonly', true);
		$('[name= "S_CreditLimit"]').attr('readonly', true);
		$('[name= "S_SupplierPostingGroup"]').attr("style", "-webkit-appearance: none;pointer-events: none;background: #E9ECEF").removeClass('spg-js-select2');
		$('[name= "S_WHT_PostingGroup"]').attr("style", "-webkit-appearance: none;pointer-events:none; background: #E9ECEF").removeClass('wht-js-select2');
		$('[name= "S_Vat_PostingGroup"]').attr("style", "-webkit-appearance: none;pointer-events: none;background: #E9ECEF").removeClass('vat-js-select2');
		$('[name= "S_FK_PayTerms"]').attr("style", "-webkit-appearance: none;pointer-events:none; background: #E9ECEF").removeClass('payterms-js-select2');
		$('[name= "S_SupplierType"]').attr("style", "-webkit-appearance: none;pointer-events:none;background: #E9ECEF").removeClass('suppliertype-js-select2')
		$('[name= "S_FK_Attribute_Currency_id"]').attr("style", "-webkit-appearance: none;pointer-events:none; background: #E9ECEF").removeClass('currency-js-select2')
	}
	if (output_type == 'update') {
		$('[name="S_DocNo"]').attr('readonly', true);
	}

	$('.datepicker').datepicker({
		todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "mm/dd/yyyy"
	});
	$(".js-select2").select2();
	$(".payterms-js-select2").select2({
		placeholder: 'Payment Terms',
		allowClear: true,
	});
	$(".vat-js-select2").select2({
		placeholder: 'VAT Posting Group',
		allowClear: true,
	});
	$(".spg-js-select2").select2({
		placeholder: 'Supplier Posting Group',
		allowClear: true,
	});
	$('.wht-js-select2').select2({
		placeholder: 'WHT Posting Group',
		allowClear: true,
	});
	$('.suppliertype-js-select2').select2({
		placeholder: 'Supplier Type',
		allowClear: true,
	});
	$('.currency-js-select2').select2({
		placeholder: 'Currency',
		allowClear: true,
	});
	$('[name="S_CreditLimit"]').keyup(function(){
		$('[name= "S_Credit_Balance"]').val($(this).val());
	});

	// $('[name="S_CreditLimit"]').keypress(function(event) {
 //        if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57)) {
 //            $(".alert").html("Enter number only!").show().fadeOut(2000);
 //            return false;
 //        }
 //    });
 //    $('[name="S_BalanceAsOf"]').keypress(function(event){
 //    	if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57)) {
 //    		$(".alert2").html("Enter number only").show().fadeOut(2000);
 //    		return false;
 //    	}
 //    })
	
});