$(document).ready(function(){

	tableindex = $('#bank_account').DataTable( {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'master_file/bank_account/data',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		},{
			"targets": [0,1,2,4,5],
			"className": 'text-center'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('breakdown-detail');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	$('.dataTables-example').on('click','.delete-button',function(e){
		e.preventDefault();
		var doc_no = $(this).data('id');
		swal({
                title: "Are you sure?",
                text: "Bank Account will be deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Delete it!",
                closeOnConfirm: false
            }, function (isConfirm){
            	if(isConfirm){
            		$.ajax({
						type: 'GET',
						dataType: 'json',
						url: global.site_name + 'master_file/bank_account/delete',
						data : {	'id'		: doc_no,
									'action'	: 'delete'},
						success : function(data){
							if(data.success == 1){
								swal({
									title: "Delete Success",
									type:  "success"
								});
								setInterval(function(){
									location.reload();	
								}, 1500);
								
							}
							else{
								swal({
									title: 	"Record can't be deleted",
									text: 	"Data is in used",
									type:  	"error"
								});
							}
						}
					});
            	}            	
            });
		});

});