$(document).ready(function(){

		$('.datepicker').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: false,
				autoclose: true,
				format: "mm/dd/yyyy"

		});
	
		$('#BPG').select2({
			placeholder: 'Enter Bank Posting Group Here',
			allowClear: true
		});

		$('#BAT').select2({
			placeholder: 'Enter Bank Account Type Here',
			allowClear: true
		});

		$('#currency').select2({
			placeholder: 'Enter Currency Here',
			allowClear: true
		});

		$('#BA').select2({
			placeholder: 'Enter Bank Account Here',
			allowClear: true
		});

		$('#CPC').select2({
			placeholder: 'Enter Cost/Profit Center Here',
			allowClear: true
		});

		$('#document_type').select2({
			placeholder: 'Enter Document Type Here',
			allowClear: true
		});

		/*$('#BPG').on('change', function(){
			$('#BPG').val($(this).find('option:selected').attr('value'));

			var BPG = $('#BPG').val();

			$.ajax({
				url: global.site_name + 'master_file/bank_account/', 
				type: 'post',
				data:{BPG1:BPG2},
				dataType: 'json',
				success:function(data){

					var next_id = data;
					$('#BPG').val(BPG1 + "_" + next_id);
					//alert('success');
				}
			});

		});*/

		$('#add-new').click(function(){
				if(ba_detail.validate_detail()){
					if($(this).val() == 'add'){
						ba_detail.add_detail();
						ba_detail.clear_form();
					}
					else if($(this).val() == 'update'){
						ba_detail.edit_detail();
						ba_detail.clear_form();
					}	
					//ba_detail.update_header();
				}
			});

		//BANK ACCOUNT DETAILS
		let detail = [];

		var tbl_dtl_lngt = $('#tbl-detail tbody tr:visible').length;

		$('#tbl-detail tbody tr:visible').each(function(){
			detail.push({
				'BAL_EntryNo'			: $(this).data('[data-entry-no]').data('entry-no'),
				'BAL_LineNo'			: $(this).data('line-no'),
				'BAL_DocType'			: $(this).data('[data-doctype]').data('doctype'),
				'BAL_DocNo'				: $(this).data('[data-docno]').data('docno'),
				'BAL_DocDate'			: $(this).data('[data-doc-date]').data('doc-date'),
				'BAL_DateCreated'		: $(this).data('[data-date-created]').data('date-created'),	
				'BAL_CPC'				: $(this).data('[data-cpc]').data('cpc'),
				'BAL_BankAccountNo'		: $(this).data('[data-bank-account-no]').data('bank-account-no'),
				'BAL_BankAccountName'	: $(this).data('[data-bank-account-name]').data('bank-account-name'),
				'BAL_Debit'				: $(this).data('[data-debit]').data('debit'),
				'BAL_Credit'			: $(this).data('[data-credit]').data('credit'),
				'BAL_Currency'			: $(this).data('[data-currency]').data('currency'),
				'BAL_Amount'			: $(this).data('[data-amount]').data('amount'),
				'BAL_AmountLCY'			: $(this).data('[data-amount-lcy]').data('amount-lcy'),
				'BAL_Status'			: $(this).data('[data-status]').data('status'),
				'BAL_PostedBy'			: $(this).data('[data-posted-by]').data('posted-by'),
				'BAL_DatePosted'		: $(this).data('[data-date-posted]').data('date-posted'),
			});
		});

		var ba_detail = {
			reload_row_events	:	function(){
				$('.edit-ba-detail').unbind();
				$('.delete-ba-detail').unbind();

				$('.edit-ba-detail').click(function(){

					var row = $(this).closest('.ba-detail')
					var todo = row.data('todo');
					if(todo!='delete'){

						//For Update
						ba_detail.show_modal((todo =='add')? 'readd' : 'edit');
						ba_detail.set_form_data(row);
					}
				});

					$('.delete-ba-detail').click(function(){
						var current_row = $(this).closest('.ba-detail');
						//Remove entire rOw
						swal({
				                title: "Are you sure?",
				                text: "The selected record will be deleted",
				                type: "warning",
				                showCancelButton: true,
				                confirmButtonColor: "#DD6B55",
				                confirmButtonText: "Delete",
				                closeOnConfirm: true
				            }, function (isConfirm){
				            	if(isConfirm){
				            		current_row.remove();
				            }            	
				        });
					
					});
			},
			row_format: function(row){
				row	
			
				return '<tr class="ba-detail" data-todo="' + row['todo'] + '" data-line-no="' + row['line-no'] + '">' +
								'<td class="text-center">' +
									'<button class="edit-ba-detail btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Update" type="button"><i class="fa fa-pencil"></i></button><span>&nbsp;</span>' + 
									'<button class="delete-ba-detail btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button" style=""><i class="fa fa-trash-o"></i></button>' +
								'</td>' +
								'<td class="text-center" data-entry-no="'+row['BAL_EntryNo']+'">' + row['BAL_EntryNo'] + '</td>' +
								'<td class="text-center" data-doctype="'+row['BAL_DocType']+'">' + row['BAL_DocType'] + '</td>' +
								'<td class="text-center" data-docno="'+row['BAL_DocNo']+'">' + row['BAL_DocNo'] + '</td>' +
								'<td class="text-center" data-doc-date="'+row['BAL_DocDate']+'">' + row['BAL_DocDate'] + '</td>' +
								'<td class="text-center" data-cpc="'+row['BAL_CPC']+'">' + row['BAL_CPC'] + '</td>' +
								'<td class="text-center" data-bank-account-no="'+row['BAL_BankAccountNo']+'">' + row['BAL_BankAccountNo'] + '</td>' +
								'<td class="text-center" data-bank-account-name="'+row['BAL_BankAccountName']+'">' + row['BAL_BankAccountName'] + '</td>' +
								'<td class="text-center" data-debit="'+row['BAL_Debit']+'">' + row['BAL_Debit'] + '</td>' +
								'<td class="text-center" data-credit="'+row['BAL_Credit']+'">' + row['BAL_Credit'] + '</td>' +
								'<td class="text-center" data-currency="'+row['BAL_Currency']+'">' + row['BAL_Currency'] + '</td>' +
								'<td class="text-center" data-amount-lcy="'+row['BAL_AmountLCY']+'">' + row['BAL_AmountLCY'] + '</td>' +
								'<td class="text-center" data-status="'+row['BAL_Status']+'">' + row['BAL_Status'] + '</td>' +
								'<td class="text-center" data-posted-by="'+row['BAL_PostedBy']+'">' + row['BAL_PostedBy'] + '</td>' +
								'<td class="text-center" data-date-posted="'+row['BAL_DatePosted']+'">' + row['BAL_DatePosted'] + '</td>' +
						'</tr>';
			},

			add_detail	:	function(){

			var row = {
						'BAL_DocDate'			: $('[name="BAL_DocDate"]').val(),
						'todo'					: 'add'	
					};

			row['todo'] = 'add';
			row['line-no'] = ba_detail.get_temp_line_no();

			var rowdetail = ba_detail.row_format(row);			
			$('#tbl-detail tbody').append(rowdetail);
			$('tr').css('background-color', '#D0E9C6');
			ba_detail.reload_row_events();

			},

			validate_detail	: function(){
				var isValid = true;
					if(!$('[name="BAL_DocDate"]').val()){
						isValid = false;
					}
					if(!$('[name="BAL_DateCreated"]').val()){
						isValid = false;
					}
					//if(!$('[name="BAL_DocType"]').val()){
						//isValid = false;
					//}
					if(!$('[name="BAL_CPC"]').val()){
						isValid = false;
					}
					//if(!$('[name="BAL_BankAccountNo"]').val()){
						//isValid = false;
					//}
				return isValid;
			},

			clear_form	: function(){
				$('[name="BAL_DocDate"]').val("");
				$('[name="BAL_DateCreated"]').val("");
				$('[name="BAL_DocType"]').val("");
				$('#cpc input[name="BAL_CPC"]').val("");
				$('[name="BAL_BankAccountNo"]').val("");

			},
			get_temp_line_no	: function(){
			var start = 0;
				while($("#tbl-detail tbody").find('[data-line-no="' + ++start + '"]').length > 0){
					//console.log($("#of-detail-list tbody").find('[data-line-no="0"]').length)
				}
				return start;
			},

		}
});