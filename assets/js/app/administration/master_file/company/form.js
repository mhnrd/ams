$(document).ready(function(){

	function init(){

		if(output_type == 'view'){
		$('[name="COM_Id"]').prop('readonly', true);
		$('[name="COM_Name"]').prop('readonly', true);
		$('[name="COM_Address"]').prop('readonly', true);
		$('[name="COM_PhoneNo"]').prop('readonly', true);
		$('[name="COM_FaxNum"]').prop('readonly', true);
		$('[name="COM_Email"]').prop('readonly', true);
		$('[name="COM_Website"]').prop('readonly', true);
		$('[name="COM_BusinessType"]').prop('readonly', true);
		$('[name="COM_Tin"]').prop('readonly', true);
		$('[name="COM_SSSNo"]').prop('readonly', true);
		$('[name="COM_PhilhealthNo"]').prop('readonly', true);
		$('[name="COM_PagibigNo"]').prop('readonly', true);
		//$('[name="COM_Currency"]').prop("style", "pointer-events:none;-webkit-appearance: none;background: #E9ECEF").removeClass('js-select2-currency');
		$('[name="COM_Currency"]').prop('disabled', true);
		$('[name="COM_FY_Start"]').css('background', '#EEEEEE').removeClass('datepicker');
		$('[name="COM_FY_End"]').css('background', '#EEEEEE').removeClass('datepicker');
		$(".datepicker").readonlyDatepicker(true);
		
	}

	// Text input allow only numeric input and specific symbols
	$(function(){
      $('[name="COM_PhoneNo"]').keyup(function(){
        var input_val = $(this).val();
        var inputRGEX = /^[0-9]*$/;
        var inputResult = inputRGEX.test(input_val);
          if(!(inputResult))
          {     
            this.value = this.value.replace(/[^-0-9-/()]/gi, '');
          }
       });
    });

    $(function(){
      $('[name="COM_FaxNum"]').keyup(function(){
        var input_val = $(this).val();
        var inputRGEX = /^[0-9]*$/;
        var inputResult = inputRGEX.test(input_val);
          if(!(inputResult))
          {     
            this.value = this.value.replace(/[^-0-9-/()]/gi, '');
          }
       });
    });
    
    // End

	if(output_type == 'update'){
		$('[name="COM_Id"]').prop('readonly', true);

		if($('[name="COM_FY_Start"]').val() == '01/01/1900'){
			$('[name="COM_FY_Start"]').val('');
		}

		if($('[name="COM_FY_End"]').val() == '01/01/1900'){
			$('[name="COM_FY_End"]').val('');
		}
	}

		$('.datepicker').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: false,
				autoclose: true,
				format: "mm/dd/yyyy",

			});


		// DISABLE CHARACTERS FROM INPUT FIELD
		// $(".datepicker").keypress( function(e) {
	 //    var chr = String.fromCharCode(e.which);
	 //    if ("12345678910".indexOf(chr) < 0)
	 //        return false;
		// });

		$(".js-select2").select2();

		$('.js-select2-currency').select2({
			placeholder: '',
			allowClear: true,
		});
	}
	
	init();
});