$(document).ready(function() {
	
	// Form View Setup - ReadOnly
	if(output_type == 'view'){
		$('[name="BOM_BOMDescription"]').attr('readonly',true);
		$('[name="BOM_YieldQty"]').attr('readonly',true)
		$('[name="BOM_BatchQty"]').attr('readonly',true);
		$('[name="BOM_Remarks"]').attr('readonly',true);

		// Datatable Setup
		tableindex = $('.dataTables-example').DataTable( {
			"processing":true,  
			"serverSide":true,  
			"order":[],  
			"ajax":{  
			    url: global.site_name + 'master_file/bom/data_view?id='+ doc_no,  
			    type:"POST"  
			},  
	        "deferRender": true,
			 "columnDefs": [{
				  "targets": [0,3],
				  "className": 'text-center'
			},{
				  "targets": [2,4,5,6,7],
				  "className": 'text-right'
			}],
			pageLength: 10,
	        responsive: true,
			"dom": 'lT<"dt-toolbar">fgtip'
		});
	}

	if(output_type == 'update'){
		$('.bom-item').prop('disabled', true);

		if($('#bom-detail-list tbody tr').length >= 5){
			$('#bom-fixed-header').css('width', '99%');
		}
		else{
			$('#bom-fixed-header').css('width', '100%');
		}

		if (window.matchMedia('(max-width: 768px)').matches) {
			$('.orig-header').show();
		}
	}
	
	
	function init(){
		toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "progressBar": false,
		  "preventDuplicates": true,
		  "positionClass": "toast-top-full-width",
		  "onclick": null,
		  "showDuration": "400",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}

		$.fn.modal.Constructor.prototype.enforceFocus = function() {};
		$("body").tooltip({ selector: '[data-toggle=tooltip]' });
		$(".bom-item").select2();
		$(".item").select2();
		$(".js-select2").select2();

		$("#bom-item-description").select2(bom_detail.item_search('IM_Sales_Desc', "Enter Item Description Here",'FG'));
		$("#bom-item-no").select2(bom_detail.item_search('IM_Item_Id', "Enter Item No Here",'FG'));
		// $("#bom-barcode").select2(bom_detail.item_search('IM_UPCCode', "Enter Item Barcode Here"));

		$('select[name=BOM_Location]').on('change',function(){
			$.ajax({
					type: 'GET',
					dataType: 'json',
					url: global.site_name + 'master_file/numberseries/getNextAvailableNumber',
					data : {'location' : $(this).val() , 'ns_id' : number_series },
					success : function(result){					
						if(result.success == 1){
							$('input[name="BOM_DocNo"]').attr('value',result.message);
						}
						else if(result.success == 0){
							swal(result.message, "Please contact your System Administrator.", "error")
							$('input[name="BOM_DocNo"]').attr('value','');
						}
					}
				});
		});

		$('.bom-item').on("select2:select", function(e) { 
			var type = $(this).data('type');
			var data_att = $(this).select2('data')[0];
			if(type=="item-description"){
				$("#bom-item-no").html('<option value="' + data_att['id']+ '" selected ="selected">' + data_att['data-id'] + ' </option>');
				$("#bom-item-no").select2("val", data_att['id']);
			}
			else if(type=="item-no"){
				$("#bom-item-description").html('<option value="' + data_att['id']+ '" selected ="selected">' + data_att['data-text'] + ' </option>');
				$("#bom-item-description").select2("val", data_att['id']);
			}
			$('#bom-description').val(data_att['data-text']);
			$('.version').val(data_att['data-version']);

			$.ajax({
				type: 'GET',
				dataType: 'json',
				url: global.site_name + 'master_file/bom/getUOMConversion',
				data : {'item-no' : data_att['data-id']},
				success : function(result){					
					var list = {
						'html'		: ''
					,	'format'	: function(data){
							return '<option value="' + data.AD_Id + '" data-qty="' + parseFloat(data.IUC_Quantity) + '">' + data.AD_Desc + '</option>';
						}
					,	'refresh'	: function(){
							$("#bom-yield-uom").empty();
							result.forEach(function(item, index){
								list.html+=list.format(item);							
							});
							$("#bom-yield-uom").append(list.html);
							$("#bom-yield-uom").select2();													
						}
					}				
					list.refresh();				
				}
			});
		});

		$('.waste-compute').change(function(){

			// if($(this).val() > 100){
			// 	$(this).val(0);
			// }

			var qty = parseFloat($('#quantity').val());
			var waste = parseFloat($('#waste').val());
			var rqqty = qty + (qty*(waste/100));
			$('#required-qty').val(rqqty);
		});

		$('.item').on("select2:select", function(e) {

				if($(this).select2('data')[0] === undefined && e['params']['data'].id == '') return;
				var type = $(this).data('type');
				var data_att = $(this).select2('data')[0];
				
				// Data from Row Field
				var select_param = e['params'];
				// Array for Adding / Editing
				var item_data = [];				
				if($(this).select2('data')[0] === undefined){
					item_data = {
						 'data-id' 		: select_param['data'].itemid
						,'data-text' 	: select_param['data'].desc
						,'data-price' 	: number_format(select_param['data'].unit_cost, 2)
					};
				}
				else{
					item_data = {
						 'data-id' 		: data_att['id']
						,'data-text' 	: data_att['data-text']
						,'data-price' 	: number_format(data_att['data-price'], 2)
					};
				}
				
				if(type=="item-description"){
					$("#bom-detail #item-no").html('<option value="' + item_data['data-id']+ '" selected ="selected">' + item_data['data-id'] + ' </option>');
					$("#bom-detail #item-no").select2("val", item_data['data-id']);
				}
				else if(type=="item-no"){

					if(select_param['data'].todo == "edit"){
						$("#bom-detail #item-description").html('<option value="' + item_data['data-id']+ '" selected ="selected">' + $.trim(select_param['data'].desc) + ' </option>');
						$("#bom-detail #item-no").html('<option value="' + item_data['data-id']+ '" selected ="selected">' + item_data['data-id'] + ' </option>');
						$("#bom-detail #item-no").select2("val", item_data['data-id']);
					}
					else{
						$("#bom-detail #item-description").html('<option value="' + item_data['data-id']+ '" selected ="selected">' + item_data['data-text'] + ' </option>');
						bom_detail.process_total_uom();
						bom_detail.process_required_qty();
					}
					$("#bom-detail #item-description").select2("val", item_data['data-id']);	
				}
				
				
				$.ajax({
					type: 'GET',
					dataType: 'json',
					url: global.site_name + 'master_file/bom/getUOMConversion',
					data : {'item-no' : item_data['data-id']},
					success : function(result){					
						var list = {
							'html'		: ''
						,	'format'	: function(data){
								return '<option value="' + data.AD_Id + '" data-qty="' + parseFloat(data.IUC_Quantity) + '">' + data.AD_Desc + '</option>';
							}
						,	'refresh'	: function(){
								$("#bom-detail #uom-list").empty();
								result.forEach(function(item, index){
									list.html+=list.format(item);							
								});
								$("#bom-detail #uom-list").append(list.html);
								$("#bom-detail #uom-list").select2();													
							}
						}				
						list.refresh();				
					}
				});
				
				$('#unit-cost').data('unit-cost', item_data['data-price']);
				$('#unit-cost').val(number_format(item_data['data-price'], 2));
				bom_detail.process_total_uom();

				// Added by Jekzel Leonidas - Get Base UOM and Base UOM Qty of the Item Selected
				$.ajax({
					type: 'GET',
					dataType: 'json',
					url: global.site_name + 'master_file/bom/getBaseUOMConversion',
					data : {'item-no' : item_data['data-id']},
					success : function(result){					
						$('[name="BOMD_BaseUOM"]').val(result.AD_Id);	
					}
				});
				//$('#bom-description').val(data_att['data-text'] );
		});

		$("#add-detail-template").click(function(){
			bom_detail.show_modal('add');
		});

		$('#add-new').click(function(){
			if(bom_detail.validate_detail()){
				if($(this).val() == 'add'){
					bom_detail.process_total_uom();
					bom_detail.add_detail();
					bom_detail.clear_form();
				}
				else if($(this).val() == 'edit'){
					bom_detail.process_total_uom();
					bom_detail.edit_detail();
					bom_detail.clear_form();
				}
				
			}
			
		});

		$('#add-close').click(function(){
			if(bom_detail.validate_detail()){
				if($(this).val() == 'add'){
					bom_detail.process_total_uom();
					bom_detail.add_detail();
					$("#bom-detail .close").click();
				}
				else if($(this).val() == 'edit'){
					bom_detail.process_total_uom();
					bom_detail.edit_detail();
					$("#bom-detail .close").click();
				}
				else if($(this).val() == 'readd' ){
					bom_detail.process_total_uom();
					bom_detail.edit_detail($(this).val());
					$("#bom-detail .close").click();
				}
			}
			
		});
		
		$('#quantity').change(function(){
			bom_detail.process_total_uom();
		});

		$('#unit-cost').change(function(){
			bom_detail.process_total_uom();
		});

		$('#uom-list').change(function(){
			bom_detail.process_total_uom();
		});

		$('#save').click(function(){

			var action = $('#save').data('todo');
			swal({
                title: "Are you sure?",
                text: (action == 'add')? "You will add the current Bill of Material." : "You will save updated changes in the current Bill of Material document.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#1AB394",
                confirmButtonText: (action == 'add') ? "Save" : "Update",
                closeOnConfirm: false
            }, function () {
				var bom_details = [];
				$('#bom-detail-list > tbody  > tr').each(function(){
					bom_details.push({
						'todo' 					: $(this).data("action")
					,	'line-no' 				: $(this).data("line-no")
					,	'item-no'				: $(this).find("[data-item-no]").data("item-no")
					,	'item-description'		: $(this).find("[data-item-description]").data("item-description")
					,	'quantity'				: parseFloat($(this).find("[data-quantity]").data("quantity"))
					,	'uom'					: $(this).find("[data-uom]").data("uom")
					,	'unit-cost'				: parseFloat($(this).find("[data-unit-cost]").data("unit-cost"))
					,	'total-cost'			: parseFloat($(this).find("[data-total-cost]").data("total-cost"))
					,	'base-uom-id'			: $(this).find("[data-base-uom-id]").data("base-uom-id") 
					,	'base-uom-qty'			: parseFloat($(this).find("[data-base-uom-qty]").data("base-uom-qty"))
					,	'waste'					: $(this).find("[data-waste]").data("waste")
					,	'comment'				: $(this).find("[data-comment]").data("comment")
					,	'is-bom'				: $(this).find("[data-is-bom]").data("is-bom")
					,	'bom-no'				: $(this).find("[data-bom-no]").data("bom-no")
					,	'bom-desc'				: $(this).find("[data-bom-desc]").data("bom-desc")
					,	'required-qty'			: parseFloat($(this).find("[data-required-qty]").data("required-qty"))
					});
				});

				var yield_qty  = $('#bom-yield').val();
				var batch_size = $('#bom-batch-size').val();

				if(bom_details == ''){
					swal("Error", "Please complete all required fields.", "error");
				}

				if(yield_qty == '' || batch_size == ''){
					swal("Error", "Please complete all required fields.", "error");
				}

				else{
					$.ajax({
						type: 'POST',
						dataType: 'json',
						url: global.site_name + 'master_file/bom/save',
						data : {
							'todo'					: $('#save').data('todo'),
							'doc-no'				: $('#save').data('doc-no'),
							'bom-item-no'			:  $('#bom-item-no').val().trim(),
							'bom-description'		:  $('#bom-description').val().trim(),
							'bom-doc-date'			:  $('#bom-doc-date').val(),
							'bom-yield'				:  $('#bom-yield').val(),
							'bom-yield-uom'			:  $('#bom-yield-uom').val(),
							'bom-batch-size'		:  $('#bom-batch-size').val(),
							'bom-batch-size-uom'	:  $('#bom-batch-size-uom').val(),
							'bom-status'			:  $('#bom-status').val(),
							'bom-version'			:  $('#bom-version').val(),
							'bom-remarks'			:  $('#bom-remarks').val().trim(),
							'bom-location'			:  $('[name="BOM_Location"]').val(),
							'bom-details'			: bom_details

						},
						success : function(result){
							if(result.success){
								swal((action == 'add') ? "Saving Success!" : "Update Success!",(action == 'add') ? "Succesfully saved Bill of Material document!" : "Succesfully update Bill of Material document!", "success")
								setTimeout(function(){
									window.location.href = global.site_name + "master_file/bom";
								},1500);
							}
							else{
								if(result.message == 'no-item'){
									swal("Saving Failed!", "Error! BOM Item No is empty.", "error")
								}
								if(result.message == 'doc-no-null'){
									swal("Saving Failed!", "Error! BOM No is empty.", "error")
								}
								if(result.message == 'bom-desc-null'){
									swal("Saving Failed!", "Error! BOM Description is empty.", "error")
								}
								if(result.message == 'bom-desc-exists'){
									swal("Saving Failed!", "Error! BOM Description already exists.", "error")
								}
								if(result.message == 'bom-yield-batch-null'){
									swal("Saving Failed!", "Error! Please complete all required fields.", "error")
								}
								if(result.message == 'no-detail'){
									swal("Saving Failed!", "Error! BOM Details is empty.", "error")
								}
								
							}
						}
					});
				}
            });
		});

		$('#bom-detail').on('hidden.bs.modal', function (e) {
			bom_detail.clear_form();
		});

		//Events goes here from bom_detail
		bom_detail.reload_row_events();
		bom_detail.bom_detail_item_event();
	}
	
	var bom_detail = {
		bom_detail_item_event :function(){
			$("#bom-detail #item-description").select2(bom_detail.item_search('IM_Sales_Desc', "Enter Item Description Here"));
			$("#bom-detail #item-no").select2(bom_detail.item_search('IM_Item_Id', "Enter Item No Here"));
		},
		item_search: function(filter, placeholder='Enter Item Here',category = null){
			return {
				placeholder: placeholder,
				minimumInputLength: 3,
				ajax: {
					url: global.site_name + 'master_file/bom/getItemNotScrap',
					dataType: 'json',
					delay: 250,
					data: function (params) {
					  var queryParameters = {
								'filter-type'	: filter
							,	'filter-search'	: params.term
							,	'action'		: 'search'
							,	'category-type'	: category
					    }
					    return queryParameters;
					},
					processResults: function (data, params) {
						return {
							results: $.map(data, function(item) {
					            return {
					                id: item.IM_Item_Id,
					                text:item[filter],
					                'data-price':item.IM_UnitCost,
					                'data-text':item.IM_Sales_Desc,
					                'data-id':item.IM_Item_Id,
					                'data-waste':number_format((item.IM_Item_Waste)? item.IM_Item_Waste : 0, 2),
					                'data-version' : item.IM_Version 
					            }
					        })
						};
					}
				}
			}
		},
		show_modal : function(method){
			if(method=='add'){
				$("#bom-detail").modal('show');
				$("#bom-detail #add-new").show();
				$("#bom-detail #add-close").text('Add & Close');
				$("#bom-detail #add-new").val(method)
				$("#bom-detail #add-close").val(method)
			}			
			else if(method=='readd' || method=='edit'){
				$("#bom-detail").modal('show');
				$("#bom-detail #add-new").hide();
				$("#bom-detail #add-close").text('Save');
				$("#bom-detail #add-new").val(method)
				$("#bom-detail #add-close").val(method)
			}
		},
		process_total : function(){
			//var total = Number($("#bom-detail #quantity").val()) * Number($("#bom-detail #unit-cost").data('unit-cost'))
			var total = Number($("#bom-detail #quantity").val()) * Number($("#bom-detail #unit-cost").val())
			
			$("#total-cost").val(number_format(total, 2));	
			$("#total-cost").data('total-cost', total);			
		},
		process_required_qty :function() {
			var rqqty = parseFloat($("#bom-detail #quantity").val()) + (parseFloat($("#bom-detail #quantity").val()) * ((parseFloat($("#bom-detail #waste").val())) / 100));
			$('#required-qty').val(number_format(rqqty, 2));
			$('#required-qty').text(number_format(rqqty, 2));
			$('#required-qty').data('required-qty', rqqty);
		},
		process_total_uom : function(){
			$('#unit-cost').val(number_format($("#bom-detail #uom-list option:selected").data('qty'),2,".",""));
			var total = Number($("#bom-detail #quantity").val()) * Number($("#bom-detail #unit-cost").val()) * $("#bom-detail #uom-list option:selected").data('qty');
			$("#total-cost").val(number_format(total, 2));	
			$("#total-cost").text(number_format(total, 2));	
			$("#total-cost").data('total-cost', total);
			$('[name="BOMD_BaseUomQty"]').val(parseFloat($("#bom-detail #uom-list option:selected").data('qty')));
		},
		row_format	:	function(row){
			var str = row['total-cost']; 
			str = str.replace(/,/g , "");
			return 	'<tr class="' + row['color'] + ' bom-detail" data-action="' + row['action'] + '" data-line-no="' + row['line-no'] + '">' +
						'<td class="text-center col-xs-1">' +
							'<button class="edit-bom-detail btn btn-primary btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Edit" type="button"><i class="fa fa-pencil"></i></button> ' + 
							'<button class="delete-bom-detail btn btn-danger btn-xs btn-outline" data-toggle="tooltip" data-placement="bottom" title="Delete" type="button"><i class="fa fa-trash-o"></i></button>' +
						'</td>' + 
						'<td class="text-center col-xs-1" data-item-no="' + row['item-no'] + '"  data-is-bom="0" data-bom-no="0">' + row['item-no'] + '</td>' + 
						'<td class="col-xs-2" data-item-description="' + row['item-description'] + '"">' + row['item-description'] + '</td>' + 
						'<td class="text-right col-xs-1" data-quantity=' + row['quantity'] + '>' + number_format(row['quantity'], 4) + '</td>' + 
						'<td class="text-center col-xs-1" data-uom-name="'+ row['uom'] +'" data-uom="' + row['uom-id'] + '" data-base-uom-id="' + row['base-uom-id'] + '" data-base-uom-qty="' + row['base-uom-qty'] + '">' + row['uom'] + '</td>' + 
						'<td class="text-right col-xs-1" data-unit-cost="' + row['unit-cost'] + '">' + number_format(row['unit-cost'], 4) + '</td>' + 
						'<td class="text-right col-xs-1" data-total-cost="' + str + '">' + number_format(row['total-cost'], 2) + '</td>' + 
						'<td class="text-right col-xs-1" data-waste="' + row['waste'] + '">' + number_format(row['waste'],2) + '</td>' + 
						'<td class="text-right col-xs-1" data-required-qty="' + row['required-qty'] + '">' + number_format(row['required-qty'],4) + '</td>' + 
						'<td class="col-xs-2" data-comment="' + row['comment'] + '" class="text-truncate">' + row['comment'] + '</td>' + 
					'</tr>';
		},		
		reload_row_events	:	function(){
			$('.edit-bom-detail').unbind();
			$('.delete-bom-detail').unbind();

			$('.edit-bom-detail').click(function(){
				var row = $(this).closest('.bom-detail');
				var todo = row.data('action');
				if(todo!='delete'){
					//For Update
					bom_detail.show_modal((todo =='add')? 'readd' : 'edit');
					bom_detail.set_form_data(row);
					
				}
			});

			$('.delete-bom-detail').click(function(){
				var current_row = $(this).closest('.bom-detail');
				// if(current_row.data('action')=='add'){
					//Remove entire rOw
					swal({
			                title: "Are you sure?",
			                text: "The selected record will be deleted.",
			                type: "warning",
			                showCancelButton: true,
			                confirmButtonColor: "#DD6B55",
			                confirmButtonText: "Delete",
			                closeOnConfirm: true
			            }, function (isConfirm){
			            	if(isConfirm){
			            		current_row.remove();
			            		if($('#bom-detail-list tbody tr').length >= 5){
									$('#bom-fixed-header').css('width', '99%');
								}
								else{
									$('#bom-fixed-header').css('width', '100%');
								}

			            	}            	
			            });
			});
		},
		add_detail	:	function(){
			var row = {
							'action'			: 	'add'
						,	'item-no'			:	$('#bom-detail #item-no').val()
						,	'item-description'	:	$('#bom-detail #item-description option:selected').text()
						,	'quantity'			:	$('#bom-detail #quantity').val()
						,	'uom'				:	$('#bom-detail #uom-list option:selected').text()
						,	'uom-id'			:	$('#bom-detail #uom-list').val()
						,	'unit-cost'			:	$('#bom-detail #unit-cost').val()
						,	'total-cost'		:	$('#bom-detail #total-cost').val()
						,	'base-uom-id'		:	$('#bom-detail [name="BOMD_BaseUOM"]').val()
						,	'base-uom-qty'		:	$('#bom-detail [name="BOMD_BaseUomQty"]').val()
						,	'waste'				:	$('#bom-detail #waste').val()
						,	'required-qty'		:	$('#bom-detail #required-qty').val()
						,	'comment'			:	$('#bom-detail #comment').val()
						};

			row['action'] = 'add';
			row['color'] = 'success';
			row['line-no'] = bom_detail.get_temp_line_no();

			var add_item_no = [];
			var add_item_desc = [];
			$('#bom-detail-list > tbody > tr').each(function(){
				var tbl_itemno 		= $(this).find('[data-item-no]').data('item-no');
				var tbl_itemdesc	= $(this).find('[data-item-description]').data('item-description');

				add_item_no.push(tbl_itemno);
				add_item_desc.push(tbl_itemdesc);
			});


			if(jQuery.inArray(row['item-no'], add_item_no) == -1 || jQuery.inArray(row['item-no'], add_item_no) == -1){
				var rowdetail = bom_detail.row_format(row);			
				$('#bom-detail-list tbody').append(rowdetail);
				if($('#detail-template-list tbody tr').length >= 6){
					$('#bom-fixed-header').css('width', '99%');
				}
				else{
					$('#bom-fixed-header').css('width', '100%');
				}
				bom_detail.reload_row_events();
			}
			else{
				swal("Error", "Cannot accept duplicate items.", "error");
			}

		},
		edit_detail	: function(isReadd = undefined){
			var row = {
							'action'			: 	'edit'
						,	'item-no'			:	$('#bom-detail #item-no').val()
						,	'item-description'	:	$('#bom-detail #item-description option:selected').text()
						,	'quantity'			:	$('#bom-detail #quantity').val()
						,	'uom'				:	$('#bom-detail #uom-list option:selected').text()
						,	'uom-id'			:	$('#bom-detail #uom-list').val()
						,	'unit-cost'			:	$('#bom-detail #unit-cost').val()
						,	'total-cost'		:	$('#bom-detail #total-cost').val()
						,	'base-uom-id'		:	$('#bom-detail [name="BOMD_BaseUOM"]').val()
						,	'base-uom-qty'		:	$('#bom-detail [name="BOMD_BaseUomQty"]').val()
						,	'waste'				:	$('#bom-detail #waste').val()
						,	'required-qty'		:	$('#bom-detail #required-qty').val()
						,	'comment'			:	$('#bom-detail #comment').val()
						};

			row['action'] = (isReadd === undefined)? 'edit': 'add';
			row['color'] = (isReadd === undefined)? 'info': 'success';
			row['line-no'] = $('#bom-detail #line-no').val();
			$('.bom-detail[data-line-no="' + row['line-no'] + '"]').replaceWith(bom_detail.row_format(row));
			bom_detail.reload_row_events();
		},
		validate_detail	: function(){
			var isValid = true;
				if(!$("#bom-detail #item-no").val()){
					isValid = false;
				}
				if(!$("#bom-detail #item-description").val()){
					isValid = false;
				}
				if(!$("#bom-detail #uom-list").val()){
					isValid = false;
				}
				if($('#bom-detail #quantity').val() == 0 || $('#bom-detail #quantity').val() == ''){
					swal("Information", "Cannot accept zero quantites", "info");
					isValid = false;
				}
				
			return isValid;
		},
		set_form_data	: function(row){

			var item_data = {
				"itemid" 	: row.find('[data-item-no]').data('item-no'),
				"desc" 		: row.find('[data-item-description]').data('item-description'),
				"unit_cost" : row.find('[data-unit-cost]').data('unit-cost'),
				"todo" 	: "edit"
			};	
			$('#bom-detail #line-no').val(row.data('line-no'));
			$('#bom-detail #item-no').select2("val", row.find('[data-item-no]') );
			$('#bom-detail #item-no').select2("val", row.find('[data-item-no]').data('item-no') );
			$('#bom-detail #item-no').trigger({
				type: "select2:select",
				params:{
					data : item_data
				}
			});			
			$('#bom-detail #uom-list').select2("val", row.find('[data-uom]').data('uom'));
			$('#bom-detail #quantity').val(number_format(row.find('[data-quantity]').data('quantity'), 4, '.', ''));
			$('#bom-detail #waste').val(number_format(row.find('[data-waste]').data('waste'), 2, '.', ''));
			$('#bom-detail #required-qty').val(number_format(row.find('[data-required-qty]').data('required-qty'), 4, '.', ''));
			$('#bom-detail #unit-cost').val(number_format(row.find('[data-unit-cost]').data('unit-cost'), 2, '.', ''));
			$('#bom-detail #total-cost').val(number_format(row.find('[data-total-cost]').data('total-cost'), 2, '.', ''));
			$('#bom-detail #comment').val(row.find('[data-comment]').data('comment'));
			//Reload Data from row UOM - Jekzel Leonidas
			setTimeout(function(){
				var uom_length = $('#bom-detail #uom-list option').length;
				if(uom_length>1){
					$('#bom-detail [name="BOMD_UOM"]').select2("val", row.find('[data-uom]').data('uom'));
					$('#bom-detail [name="BOMD_UOM"]').trigger("select2:select");	
				}
			},1500);
			
		},
		clear_form	: function(){
			$('#bom-detail #item-no').select2("val", "");
			$('#bom-detail #item-description').select2("val", "");
			$('#bom-detail #uom-list').empty();
			$('#bom-detail #uom-list').select2("val", "");
			$('#quantity').val('1.00');
			$('#bom-detail #unit-cost').data('unit-cost', '0.00');
			$('#bom-detail #unit-cost').val('0.00');
			$('#bom-detail #waste').data('waste', '0');
			$('#bom-detail #waste').val('0');
			$('#bom-detail #required-qty').data('required-quantity', '1.00');
			$('#bom-detail #required-qty').val('1.00');
			$('#bom-detail #total-cost').data('total-cost', '0.00');
			$('#bom-detail #total-cost').val('0.00');
			$('#bom-detail #comment').val('');
			$('#bom-detail #average-cost').val('0.00');
		},
		get_temp_line_no	: function(){
			var start = 0;
			while($("#bom-detail-list tbody").find('[data-line-no="' + ++start + '"]').length > 0);
			return start;			
		},
	}
	
	init();

});