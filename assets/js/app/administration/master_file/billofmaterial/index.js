$(document).ready(function(){	

	// Datatable Setup
	tableindex = $('.dataTables-example').DataTable( {
		"processing":true,  
		"serverSide":true,  
		"order":[],  
		"ajax":{  
		    url: global.site_name + 'master_file/bom/data',  
		    type:"POST"  
		},  
        "deferRender": true,
		 "columnDefs": [{
			  "targets":[0],  
			  "orderable": false,
		},{
			  "targets": [0,1,2,4,5],
			  "className": 'text-center'
		}],
		pageLength: 10,
        responsive: true,
        "order": [[ 1, "desc" ]],
		"dom": 'lT<"dt-toolbar">fgtip'
	});
	
	
	
	$(".send-approval").click(function(){
		var bom_doc_no =  $(this).val();
		swal({
                title: "Are you sure?",
                text: "BOM (" + bom_doc_no + ") will be sent for approval.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#1AB394",
                confirmButtonText: "Send",
                closeOnConfirm: false
            }, function (){
            	$.ajax({
					type: 'GET',
					dataType: 'json',
					url: global.site_name + 'bom/manageApproval',
					data : {	'doc-no'	: bom_doc_no
							,	'action'	: 'request-approval'},
					success : function(result){
						if(result){
							location.reload();
						}		
					}
				});
            });
		
	})

	// //ACTIVATE SELECTED DATA
	// $('.btnActive').on('click',function(e){
	// 	e.preventDefault();	
	// 	status_update($(this).data('id'), $(this).data('version'), 'active','Activate');
	// });

	// //DEACTIVATE SELECTED DATA
	// $('.btnDeactive').on('click',function(e){
	// 	e.preventDefault();
	// 	status_update($(this).data('id'), $(this).data('version'), 'deactivate','Deactivate');
	// });

	$('.dataTables-example').on('click', '.btnActive', function(e){
   		e.preventDefault();
		status_update($(this).data('bom-no'), $(this).data('id'), $(this).data('version'), 'active','Activate','#1AB394');
	});


	$('.dataTables-example').on('click', '.btnDeactive', function(e){
   		e.preventDefault();
		status_update($(this).data('bom-no'), $(this).data('id'), $(this).data('version'), 'deactivate','Deactivate','#DD6B55');
	});

 });

function status_update(bom_docno, id, version, action, msg, btnColor){
	console.log(bom_docno, id, version, action, msg, btnColor);
	swal({
        title: "Are you sure?",
        text: "This will " + msg + " all selected data",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: btnColor,
        confirmButtonText: "Yes, " + msg + " it!",
        closeOnConfirm: false
    }, function (isConfirm){
    	if(isConfirm){
    		// Request
    		$.ajax({
				type: 'POST',
				dataType: 'json',
				url: global.site_name + 'master_file/bom/action',
				data : {	'id'		: id,
							'version'	: version,
							'action'	: action,
							'bom-no'	: bom_docno},
				success : function(data){
					if(data.success == true){
						swal({
							title: msg + " Successful",
							type:  "success"
						});
						setInterval(function(){
							location.reload();	
						}, 1500);
						
					}
					else{
						swal("Error",data.message,"error");
					}
				}
			});
    	}            	
    });
}