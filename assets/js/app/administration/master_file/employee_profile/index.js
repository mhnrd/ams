// Global Variable Declaration
var selected_employee_id = '';
var general_info_data = [];
var family_data = [];
var siblings_data = [];
var beneficiaries_data = [];
var education_data = [];
var child_education_data = [];
var allowance_data = [];
var notes_data = [];
var leave_balance_data = [];

var blnStartPayroll = false;
var _this;

var chkInactiveValue = false;
var cboFilterby;
var cboSearchby;

$(document).ready(function(){

	$.fn.modal.Constructor.prototype.enforceFocus = function() {};

	$('#btnActive').prop('disabled', true);
	$('#btnDeactive').prop('disabled', true);

	$('.dataTables-example').addClass('display');

	// declare 3rd party event
    $('body select').select2({
    	placeholder: 	" ",
		allowClear: 	true
    });

    $('body .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

	// Datatable Setup
	tableindex = $('#employee_profile_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		},{
			"targets": col_center,
			"className": 'text-center'
		}],
		// pageLength: 10,
		"paging": false,
		"scrollX": true,
		scrollY: 500,
  		scrollCollapse: true,
		"lengthChange": false,
		"info": false,
		"order": [[ 1, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	tbl_IdHistory = $('#employee_id_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_IdHistory?id=' + $('[name="EmployeeID"]').val(),
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		},{
			"targets": [5],
			"className": 'text-center'
		}],
		// pageLength: 10,
		"paging": false,
		// "scrollX": true,
		// scrollY: 300,
  		// scrollCollapse: true,
		"lengthChange": false,
		"info": false,
		"order": [[ 1, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	tbl_Allowance = $('#employee_allowance_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_Allowance?id=' + $('[name="EmployeeID"]').val(),
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		},{
			"targets": [2],
			"className": 'text-right'
		},{
			"targets": [0,3],
			"className": 'text-center'
		}],
		// pageLength: 10,
		"paging": false,
		// "scrollX": true,
		// scrollY: 300,
  		// scrollCollapse: true,
		"lengthChange": false,
		"info": false,
		"order": [[ 1, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	tbl_Siblings = $('#siblings_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_Siblings?id=' + $('[name="EmployeeID"]').val(),
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		}],
		// pageLength: 10,
		"paging": false,
		// "scrollX": true,
		// scrollY: 300,
  		// scrollCollapse: true,
		"lengthChange": false,
		"info": false,
		"order": [[ 1, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	tbl_Beneficiaries = $('#beneficiaries_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_Beneficiaries?id=' + $('[name="EmployeeID"]').val(),
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		}],
		// pageLength: 10,
		"paging": false,
		// "scrollX": true,
		// scrollY: 300,
  		// scrollCollapse: true,
		"lengthChange": false,
		"info": false,
		"order": [[ 1, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	
	tbl_Education = $('#education_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_Education?id=' + $('[name="EmployeeID"]').val(),
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		}],
		// pageLength: 10,
		"paging": false,
		// "scrollX": true,
		// scrollY: 300,
  		// scrollCollapse: true,
		"lengthChange": false,
		"info": false,
		"order": [[ 1, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	// tbl_Leave = $('#leave_details').DataTable( {
	// 	"processing":true,
	// 	"serverSide":true,
	// 	"responsive":true,
	// 	"order":[],
	// 	"ajax":{
	// 		url: global.site_name + 'administration/master_file/employee_profile/data_Leave?id=' + $('[name="EmployeeID"]').val(),
	// 		type: "POST"
	// 	},
	// 	"deferRender": true,
	// 	"columnDefs": [{
	// 		"targets": [2,3,4,5,7],
	// 		"className": 'text-center'
	// 	}],
	// 	// pageLength: 10,
	// 	"paging": false,
	// 	// "scrollX": true,
	// 	// scrollY: 300,
 //  		// scrollCollapse: true,
	// 	"lengthChange": false,
	// 	"info": false,
	// 	"searching": false,
	// 	// "order": [[ 1, "asc" ]],
	// 	createdRow:function(row){
	// 		$(row).addClass('table-header');
	// 	},
	// 	"dom": 'lT<"dt-toolbar">fgtip'
	// });

	tbl_LeaveType = $('#leave_bal_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_LeaveBalance?id=' + $('[name="EmployeeID"]').val(),
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[2,3],
			"className" : 'text-right'
		},{
			"targets":[0],
			"orderable": false,
			"className" : 'text-center'
		}],
		// pageLength: 10,
		"paging": false,
		// "scrollX": true,
		// scrollY: 300,
  		// scrollCollapse: true,
		"lengthChange": false,
		"info": false,
		"searching": false,
		// "order": [[ 1, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	tbl_LeaveTypeView = $('#leave_bal_view').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_LeaveBalanceView?id=' + $('[name="EmployeeID"]').val() + '&leave_type=',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[2],
			"className" : 'text-center'
		},{
			"targets":[3],
			"orderable": false,
			"className" : 'text-right'
		}],
		// "paging": false,
		"lengthChange": false,
		"info": false,
		// "searching": false,
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	tbl_WorkSchedule = $('#work_sched_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_WorkSchedule?id=' + $('[name="EmployeeID"]').val(),
			type: "POST"
		},
		"columnDefs": [{
			"targets":[0,4,5],
			"className" : 'text-right'
		}],
		"deferRender": true,
		"paging": false,
		"lengthChange": false,
		"info": false,
		// "searching": false,
		"order": [[ 1, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	tbl_Notes = $('#notes_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_Notes?id=' + $('[name="EmployeeID"]').val(),
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		},{
			"targets":[0,1],
			"className":'text-center'
		}],
		// pageLength: 10,
		"paging": false,
		// "scrollX": true,
		// scrollY: 300,
  		// scrollCollapse: true,
		"lengthChange": false,
		"info": false,
		// "order": [[ 1, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	tbl_EmployeeMovement = $('#employee_movement_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_EmployeeMovement?id=' + $('[name="EmployeeID"]').val(),
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"className":'text-center'
		},{
			"targets":[4],
			"className":'text-right'
		}],
		// pageLength: 10,
		"paging": false,
		// "scrollX": true,
		// scrollY: 300,
  		// scrollCollapse: true,
		"lengthChange": false,
		"info": false,
		// "order": [[ 1, "asc" ]],
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	tbl_Memo = $('#memo_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_EmployeeMemo?id=' + $('[name="EmployeeID"]').val() + '&type=',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[1,2,5],
			"className":'text-center'
		}],
		"paging": false,
		"searching" : false,
		"lengthChange": false,
		"info": false,
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	tbl_MemoDocument = $('#memo_document_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"responsive":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/employee_profile/data_EmployeeMemoDocuments?id=' + $('[name="EmployeeID"]').val(),
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[4],
			"className":'text-center'
		},{
			"targets":[5,6],
			"visible": false
		}],
		"paging": false,
		"lengthChange": false,
		"info": false,
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	// tableindex.rows('.important').select();
	
	$('[name="searchedBy"]').select2("enable",false);
	$('[name="filteredBy"]').on('select2:select', function (e) {
		var id = e.params.data.id;

		cboFilterby = id;
		// search by
		$('[name="searchedBy"]').select2("val","");
		$('[name="searchedBy"]').select2("enable",true);
	    $('[name="searchedBy"]').select2({
	    	ajax: {
				url: global.site_name + 'administration/master_file/employee_profile/getFilterByData',
				dataType: 'json',
				delay: 250,
				data: function (params) {
				  var queryParameters = {
						'filter-search'	: id,
						'filter-input'	: params.term,
				  }
				  return queryParameters;
				},
				processResults: function (data, params) {
					return {
						results: $.map(data, function(item) {
				            return {
				                id: item.Id,
				                text: item.Description
				            }
				        })
					};
				}
			}
	    });
	});

	$('[name="filteredBy"]').on('select2:unselecting', function (e) {
		cboFilterby = "";
		$('[name="searchedBy"]').select2("val","");
		$('[name="searchedBy"]').select2("enable",false);
	});

	$('[name="Memo_filteredBy"]').on('select2:select',function(e){
		var id = e.params.data.id;
		tbl_Memo.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_EmployeeMemo?id=' + general_info_data['Emp_Id'] + '&type=' + id).load();
	});

	$('[name="Memo_filteredBy"]').on('select2:unselecting', function (e) {
		tbl_Memo.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_EmployeeMemo?id=' + general_info_data['Emp_Id'] + '&type=').load();
	});

	$('[name="searchedBy"]').on('select2:select', function (e) {
		let id = e.params.data.id;
		cboSearchby = id;
		ReloadEmployeeTable();
	});

	$('[name="filteredBy"]').on('select2:unselecting', function (e) {
		cboSearchby = "";
		ReloadEmployeeTable();
	});

	$('[name="chkInactiveEmployee"]').click(function(event) {
		chkInactiveValue = true;
		if($(this).prop('checked') !== true){
			chkInactiveValue = false;
		}
		ReloadEmployeeTable();
	});


	$('#'+table_id+'_paginate').addClass('pull-right');

	$('#employee_profile_details tbody').on( 'click', 'tr', function () {
		_this = $(this);
		InitializeClickEvent();
    });

    // tab events
    $('body').on('click', '[href="#tab-1"]', function(event) {
    	tbl_IdHistory.draw() // to fix table header
    });

    $('body').on('click', '[href="#tab-2"]', function(event) {
    	tbl_Allowance.draw() // to fix table header
    });

    $('body').on('click', '[href="#tab-4"]', function(event) {
    	// to fix table header
    	// tbl_Leave.draw() 
    	tbl_LeaveType.draw() 
    });

    $('body').on('click', '[href="#tab-5"]', function(event) {
    	tbl_WorkSchedule.draw() // to fix table header
    });

    $('body').on('click', '[href="#tab-6"]', function(event) {
    	tbl_Notes.draw() // to fix table header
    });

    $('body').on('click', '[href="#tab-8"]', function(event) {
    	tbl_Memo.draw() // to fix table header
    	tbl_MemoDocument.draw() // to fix table header
    });

	// button events

	// image_utils.initialize($('#flPhoto'), $('[name=FileUrl]'), base_url);
	$('[name="flPhoto"]').on('change',function(e){
		var input = this;
		// swal({
	 //            title: "Are you sure?",
	 //            text: "This will change the image.",
	 //            type: "warning",
	 //            showCancelButton: true,
	 //            confirmButtonColor: "#1AB394",
	 //            confirmButtonText: "Yes",
	 //            cancelButtonText: "No",
	 //            closeOnConfirm: true,
	 //        }, function () {

					
		// 	}
	 //   );

	 	var reader = new FileReader();
	    var filename = $('[name="flPhoto"]').val();
	    let pathFile = filename; 
	    console.log(filename);
	    filename = filename.substring(filename.lastIndexOf('\\')+1);
	    reader.onload = function(e) {
	      $('#image-preview').attr('src', e.target.result);
	      $('#image-preview').hide();
	      $('#image-preview').fadeIn(500);      
	      // $('input[name="FileURL"]').val(filename);             
	    }
	    reader.readAsDataURL(input.files[0]);
		
	});

	// $('[name="flDocument"]').on('change',function(e){
	// 	var input = this;

	//  	var reader = new FileReader();
	//     var filename = $('[name="flDocument"]').val();
	//     let pathFile = filename; 
	//     console.log(filename);
	//     filename = filename.substring(filename.lastIndexOf('\\')+1);
	//     reader.onload = function(e) {
	//       $('input[name="memo_document_name"]').val(filename.match(/[-_\w]+[.][\w]+$/i)[0]);             
	//     }
	//     reader.readAsDataURL(input.files[0]);
		
	// });

    $('#btnBasicRateEnc').on('click',function(e){
    	
    	e.preventDefault();
    	if($('[name="EmployeeID"').val() === ""){
    		return;
    	}

    	$.get(global.site_name + 'administration/master_file/employee_profile/getEmployeeBasicAmnt?id=' + $('[name="EmployeeID"').val(), function(value) {
    		var data = JSON.parse(value);    		
    		$('[name="BasicRateEnc"]').val(number_format(data.Basic_rate,2));
    	});
    })

    $('#btnIDHistoryAdd').on('click',function(e){
    	e.preventDefault();
    	// show modal
    	$("#detail-template-id").modal('show');
    	InitializeModal_ID_History();
    });

    $('#btnRelative').on('click',function(e){
		e.preventDefault();
    	
    	$.get(global.site_name + 'administration/master_file/employee_profile/getFamilyData?id=' + general_info_data['Emp_Id'], function(data) {
    		family_data = JSON.parse(data);
	    	// show modal
	    	$("#detail-template-relatives").modal('show');
	    	InitializeModal_PersonnelInfo_Relatives();

    	});
    });	

    $('#btnEducation').on('click',function(e){
		e.preventDefault();
		$.get(global.site_name + 'administration/master_file/employee_profile/getEducationData?id=' + general_info_data['Emp_Id'], function(data) {
    		education_data = JSON.parse(data);
	    	// show modal
	    	$("#detail-template-education").modal('show');
	    	InitializeModal_PersonnelInfo_Education();
    	});
    });	

    $('#btnAllowanceAdd').on('click',function(e){
    	e.preventDefault();
    	// show modal
    	$("#detail-template-allowance").modal('show');
    	InitializeModal_PersonnelInfo_Allowance('add');
    });

    $('body').on('click','.allowance-edit',function(e){
		e.preventDefault();
		allowance_data = $(this).data('record');
		$("#detail-template-allowance").modal('show');
		InitializeModal_PersonnelInfo_Allowance('update');		
	});

	$('body').on('click','.allowance-delete',function(e){
		e.preventDefault();
		allowance_data = $(this).data('record');
		InitializeDeleteData('Allowance',allowance_data);
	});

	$('#btnHired').on('click',function(e){
		e.preventDefault();

		// to be confirmed to sir jason regarding this process after 1st batch
		// if(general_info_data['Emp_PayrollStart'] == ''){
		// 	blnStartPayroll = true;
		// }
		// else{
		// 	blnStartPayroll = false;
		// }
		
		$("#detail-template-hired").modal('show');

		if(general_info_data['Emp_DateHired'] != ''){
			$('#detail-template-hired [name="Emp_DateHired"]').val(moment(general_info_data['Emp_DateHired']).format('MM/DD/YYYY'));
		}
		else{
			$('#detail-template-hired [name="Emp_DateHired"]').val('');
		}

		$('#detail-template-hired [name="Emp_PayrollStart"]').val('');
		$('#detail-template-hired [name="mode"]').val(blnStartPayroll);
		$('#detail-template-hired [name="Emp_Id"]').val(general_info_data['Emp_Id']);
		InitializeHired();
	});

	$('#btnResign').on('click',function(e){
		e.preventDefault();

		$("#detail-template-resign").modal('show');
		
		$('#detail-template-resign [name="Emp_Id"]').val(general_info_data['Emp_Id']);
		$('#detail-template-resign [name="Emp_DateHired"]').val(moment(general_info_data['Emp_DateHired']).format('MM/DD/YYYY'));

		if(general_info_data['Emp_DateResign'] != ''){
			$('#detail-template-resign [name="Emp_DateResign"]').val(moment(general_info_data['Emp_DateResign']).format('MM/DD/YYYY'));
		}
		else{
			$('#detail-template-resign [name="Emp_DateResign"]').val('');	
		}
		$('#detail-template-resign [name="Emp_Id"]').val(general_info_data['Emp_Id']);
		InitializeResign();

	});

	$('#btnNotesAdd').on('click',function(e){
		e.preventDefault();
		$("#detail-template-notes").modal('show');
		InitializeNotes('add');

	});


	$('body').on('click','.notes-edit',function(e){
		e.preventDefault();
		notes_data = $(this).data('record');
		$("#detail-template-notes").modal('show');
		InitializeNotes('update');
	});

	$('body').on('click','.notes-delete',function(e){
		e.preventDefault();
		notes_data = $(this).data('record');
		InitializeDeleteData('Notes',notes_data);
	});
	
	// Leave Monitoring button Events
	$('#btnLeaveBalAdd').on('click',function(e){
		e.preventDefault();
		$("#detail-template-leave-balance").modal('show');
		InitializeLeaveBalance('add');
	});

	$('body').on('click','.leave-balance-edit',function(e){
		e.preventDefault();
		leave_balance_data = $(this).data('record');
		$("#detail-template-leave-balance").modal('show');
		InitializeLeaveBalance('update');
	});

	$('body').on('click','.leave-balance-delete',function(e){
		e.preventDefault();
		leave_balance_data = $(this).data('record');
		InitializeDeleteData('Leave Balance',leave_balance_data);
	});

	$('body').on('click','.leave-balance-view',function(e){
		e.preventDefault();
		leave_balance_data = $(this).data('record');
		$("#detail-template-leave-balance-view").modal('show');
		tbl_LeaveTypeView.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_LeaveBalanceView?id=' + general_info_data['Emp_Id'] + '&leave_type=' + leave_balance_data['PKFK_LeaveType'] ).load();
	});

});
