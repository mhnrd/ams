var mode;

$(document).ready(function() {
	
	// declare global variable
	mode = output_type;

	InitializeEvents();
});

function InitializeEvents(){
	
	// default event declaration
	$('select').select2({
		placeholder: 	" ",
		allowClear: 	true
	});
	
	$('.input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    DisabledFields();
    InitializeTabGeneralInfo();
    InitializePersonnelInfo();
}

// disabled fields
function DisabledFields(){
	if(mode == 'add'){
		disabledTabGeneralInfoSelect2();
	}
}

// tab initialize - starts
function InitializeTabGeneralInfo(){

	// company event
	$('[name="Emp_CompanyId"]').on('select2:select', function (e) {
	    
	    clearTabGeneralInfoSelect2();
	    disabledTabGeneralInfoSelect2(true);
	    
	    var id = e.params.data.id;

	    // work location
	    $('[name="Emp_FK_StoreID"]').select2({
	    	ajax: {
				url: global.site_name + 'administration/master_file/employee_profile/getFilterData',
				dataType: 'json',
				delay: 250,
				data: function (params) {
				  var queryParameters = {
						'filter-search'	: id,
						'filter-type'	: 'store',
						'filter-addtl'	: '',
						'filter-input'	: params.term
				  }
				  return queryParameters;
				},
				processResults: function (data, params) {
					return {
						results: $.map(data, function(item) {
				            return {
				                id: item.Id,
				                text: item.Description
				            }
				        })
					};
				}
			}
	    });

	    // paygroup
	    $('[name="Emp_FK_GroupId"]').select2({
	    	ajax: {
				url: global.site_name + 'administration/master_file/employee_profile/getFilterData',
				dataType: 'json',
				delay: 250,
				data: function (params) {
				  var queryParameters = {
						'filter-search'	: id,
						'filter-type'	: 'paygroup',
						'filter-addtl'	: '',
						'filter-input'	: params.term
				  }
				  return queryParameters;
				},
				processResults: function (data, params) {
					return {
						results: $.map(data, function(item) {
				            return {
				                id: item.Id,
				                text: item.Description
				            }
				        })
					};
				}
			}
	    });
	});

	$('[name="Emp_CompanyId"]').on('select2:unselecting', function (e) {
		clearTabGeneralInfoSelect2();
		disabledTabGeneralInfoSelect2();
	});

	// department event
	$('[name="Emp_DepartmentId"]').on('select2:select', function (e) {
	    
	    $('[name="Emp_PositionId"]').select2("val", "");
		$('[name="Emp_PositionId"]').select2('enable',true);
	    
	    var id = e.params.data.id;

	    $('[name="Emp_PositionId"]').select2({
	    	ajax: {
				url: global.site_name + 'administration/master_file/employee_profile/getFilterData',
				dataType: 'json',
				delay: 250,
				data: function (params) {
				  var queryParameters = {
						'filter-search'	: id,
						'filter-type'	: 'department',
						'filter-addtl'	: '',
						'filter-input'	: params.term
				  }
				  return queryParameters;
				},
				processResults: function (data, params) {
					return {
						results: $.map(data, function(item) {
				            return {
				                id: item.Id,
				                text: item.Description
				            }
				        })
					};
				}
			}
	    });
	});

	$('[name="Emp_DepartmentId"]').on('select2:unselecting', function (e) {
		$('[name="Emp_PositionId"]').select2("val", "");
		$('[name="Emp_PositionId"]').select2('enable',false);
	});

	// pay group event
	$('[name="Emp_FK_GroupId"]').on('select2:select', function (e) {
	    
	    $('[name="Emp_AttendanceType"]').select2("val", "");
		$('[name="Emp_AttendanceType"]').select2('enable',true);
	    
	    var id = e.params.data.id;

	    $('[name="Emp_AttendanceType"]').select2({
	    	ajax: {
				url: global.site_name + 'administration/master_file/employee_profile/getFilterData',
				dataType: 'json',
				delay: 250,
				data: function (params) {
				  var queryParameters = {
						'filter-search'	: $('[name="Emp_CompanyId"]').select2('val'),
						'filter-type'	: 'attendancegroup',
						'filter-addtl'	: id,
						'filter-input'	: params.term
				  }
				  return queryParameters;
				},
				processResults: function (data, params) {
					return {
						results: $.map(data, function(item) {
				            return {
				                id: item.Id,
				                text: item.Description
				            }
				        })
					};
				}
			}
	    });
	});

	$('[name="Emp_FK_GroupId"]').on('select2:unselecting', function (e) {
		$('[name="Emp_AttendanceType"]').select2("val", "");
		$('[name="Emp_AttendanceType"]').select2('enable',false);
	});

	$('[name="chkSameCurrentAddress"]').click(function(event) {
		if($(this).prop('checked') !== true){
			$('[name="Emp_PermanentAddress"]').val('').removeAttr('readonly');
			$('[name="Emp_PermanentAddressTelNo"]').val('').removeAttr('readonly');
		}
		else{

			let permanent_contact = '';
			if($('[name="Emp_CurrentAddressTelNo"]').val() != '' && $('[name="Emp_CurrentAddressMobileNo"]').val() != ''){
				permanent_contact = $('[name="Emp_CurrentAddressMobileNo"]').val();
			}
			else if ($('[name="Emp_CurrentAddressTelNo"]').val() != '' && $('[name="Emp_CurrentAddressMobileNo"]').val() == '') {
				permanent_contact = $('[name="Emp_CurrentAddressTelNo"]').val();
			}
			else if($('[name="Emp_CurrentAddressTelNo"]').val() == '' && $('[name="Emp_CurrentAddressMobileNo"]').val() != ''){
				permanent_contact = $('[name="Emp_CurrentAddressMobileNo"]').val();
			}

			$('[name="Emp_PermanentAddress"]').val($('[name="Emp_CurrentAddress"]').val()).attr('readonly', true);
			$('[name="Emp_PermanentAddressTelNo"]').val(permanent_contact).attr('readonly', true);
		}
	});

	

}

function InitializePersonnelInfo(){
	
	$('#BankName').on('select2:select', function (e) {
	    let data = e.params.data;
	    $('[name="Emp_AttrBankCode"]').val(data.id);
	});

	$('#BankName').on('select2:unselecting', function (e) {
	    $('[name="Emp_AttrBankCode"]').val('');
	});
}

// tab initialize - end


//clear tab general info select2
function clearTabGeneralInfoSelect2(){
	$('[name="Emp_FK_StoreID"]').select2("val", "");
	$('[name="Emp_PositionId"]').select2("val", "");
	$('[name="Emp_FK_GroupId"]').select2("val", "");
	$('[name="Emp_AttendanceType"]').select2("val", "");
}

//disabled tab general info select2
function disabledTabGeneralInfoSelect2(blnStatus = false){
	$('[name="Emp_FK_StoreID"]').select2('enable',blnStatus);
	$('[name="Emp_PositionId"]').select2('enable',blnStatus);
	$('[name="Emp_FK_GroupId"]').select2('enable',blnStatus);
	$('[name="Emp_AttendanceType"]').select2('enable',blnStatus);
}