function ReloadEmployeeTable (){
	let get 	 = '?chkInactive=' + chkInactiveValue;

	if(cboFilterby != undefined){
		get 	+= '&filterBy=' + cboFilterby;
	}

	if(cboSearchby != undefined){
		get 	+= '&searchBy=' + cboSearchby;
	}

	console.log(get);
	tableindex.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data' + get ).load();
}

function InitializeClickEvent(){
	// clear color value 
	$('#employee_profile_details > tbody > tr button').removeClass('btn-default').addClass('btn-primary').css('color', '');
	$('#employee_profile_details > tbody > tr').find('[data-employee]').css('color', '');
	$('[href="#tab-1"]').trigger('click');

    if ( $(_this).hasClass('selected') ) {
        $(_this).removeClass('selected');
        $(_this).find('button').removeClass('btn-default').addClass('btn-primary');
        selected_employee_id = '';
        updateField(selected_employee_id);
    }
    else {
        tableindex.$('tr.selected').removeClass('selected');
        $(_this).addClass('selected');
        $(_this).find('[data-employee]').css('color', 'white');
        $(_this).find('button').removeClass('btn-primary').addClass('btn-default').css('color', 'white');
        selected_employee_id = $(_this).find('[data-employee-id]').data('employee-id');
        // console.log(selected_employee_id); // show employee id 
        updateField(selected_employee_id);
    }

    let employee_id = '';
    if(selected_employee_id != ""){
    	employee_id = $(_this).find('[data-employee-id]').data('employee');
    }
    
    // table on change trigger
	tbl_IdHistory.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_IdHistory?id=' + employee_id ).load();
	tbl_Allowance.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_Allowance?id=' + employee_id ).load();
	tbl_Siblings.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_Siblings?id=' + employee_id ).load();
	tbl_Beneficiaries.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_Beneficiaries?id=' + employee_id ).load();
	tbl_Education.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_Education?id=' + employee_id ).load();
	// tbl_Leave.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_Leave?id=' + employee_id ).load();
	tbl_LeaveType.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_LeaveBalance?id=' + employee_id ).load();
	tbl_WorkSchedule.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_WorkSchedule?id=' + employee_id ).load();
	tbl_Notes.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_Notes?id=' + employee_id ).load();
	tbl_EmployeeMovement.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_EmployeeMovement?id=' + employee_id ).load();
	tbl_Memo.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_EmployeeMemo?id=' + employee_id + '&type=').load();
	tbl_MemoDocument.ajax.url(  global.site_name + 'administration/master_file/employee_profile/data_EmployeeMemoDocuments?id=' + employee_id ).load();
}


function updateField(employee_id){
	
	$('input[type="checkbox"]:checked').prop('checked',false);

	if(employee_id == ''){
		// clear all value
		$('input').val('');
		$('textarea').val('');
		
		$('body [name="flPhoto"]').addClass('invisible');
		$('body [name="btnUpdateImage"]').addClass('invisible').val('Update Image');

		$('body [name="flDocument"]').addClass('invisible');
		$('body [name="btnUploadDocument"]').addClass('invisible').val('Upload Document');

		$('#btnRelative').addClass('invisible');
		$('#btnEducation').addClass('invisible');
		$('#btnIDHistoryAdd').addClass('invisible');
		$('#btnAllowanceAdd').addClass('invisible');
		$('#btnNotesAdd').addClass('invisible');
		$('#btnLeaveBalAdd').addClass('invisible');

		$('#btnHired').attr('disabled',true).removeClass('btn-outline');
		$('#btnResign').attr('disabled',true).removeClass('btn-outline');

		$('#image-preview').attr('src',blankimage_url);
	    $('#image-preview').hide();
	    $('#image-preview').fadeIn(500);      
	    $('input[name="FileURL"]').val("");

	}
	else{
		// insert data	
		$.get(global.site_name + 'administration/master_file/employee_profile/getEmployeeData?id=' + employee_id, function(data) {
			var rawData = JSON.parse(data);
			console.log(rawData,"rawData");
			general_info_data = rawData['general_info'];
			fieldUpdate_EmployeeInfo(general_info_data);
		});
	}
}

function fieldUpdate_EmployeeInfo(data){

	console.log(data,"GENERAL INFO DATA");

	$('#btnHired').removeAttr('disabled').addClass('btn-outline');
	$('#btnResign').removeAttr('disabled').addClass('btn-outline');	

	// additional validation for hired and resign
	if(general_info_data['Emp_Status'] == 'Hired'){
		$('#btnHired').attr('disabled',true).removeClass('btn-outline');
	}

	if(general_info_data['Emp_Status'] != 'Hired'){
		$('#btnResign').attr('disabled',true).removeClass('btn-outline');
	}

	// apply image
	if(general_info_data['Emp_PictureID'] != ''){
		$('#image-preview').attr('src', EMP_PHOTO_DIR + general_info_data['Emp_PictureID']);
	    $('#image-preview').hide();
	    $('#image-preview').fadeIn(500);      
	    $('input[name="FileURL"]').val(EMP_PHOTO_DIR + general_info_data['Emp_PictureID']);
	}
	else{
		$('#image-preview').attr('src',blankimage_url);
	    $('#image-preview').hide();
	    $('#image-preview').fadeIn(500);      
	    $('input[name="FileURL"]').val("");
	}

	$('[name="Emp_Id_Photo"]').val(data['Emp_Id']);
	$('[name="EmployeeID"]').val(data['Emp_Id']);
	$('[name="Title"]').val(data['Emp_Title']);
	$('[name="FirstName"]').val(data['Emp_FirstName']);
	$('[name="MiddleName"]').val(data['Emp_MiddleName']);
	$('[name="LastName"]').val(data['Emp_LastName']);
	$('[name="NickName"]').val(data['Emp_NickName']);
	$('[name="Position"]').val(data['P_Position']);
	$('[name="ProvincialAddress"]').val(data['Emp_PermanentAddress']);
	$('[name="ProvincialTelNo"]').val(data['Emp_PermanentAddressTelNo']);
	$('[name="CurrentAddress"]').val(data['Emp_CurrentAddress']);
	$('[name="CurrentTelNo"]').val(data['Emp_CurrentAddressTelNo']);
	$('[name="CurrentMobileNo"]').val(data['Emp_CurrentAddressMobileNo']);
	$('[name="PhoneNumber"]').val(data['Emp_PhoneNo']);
	$('[name="MobileNumber"]').val(data['Emp_MobileNo']);
	$('[name="EmailAddress"]').val(data['Emp_EmailAdd']);
	$('[name="CivilStatus"]').val(data['Emp_CivilStatus']);
	$('[name="Gender"]').val(data['Emp_Gender']);
	$('[name="Height"]').val(number_format(data['Emp_Height'],1));
	$('[name="Weight"]').val(number_format(data['Emp_Weight'],2));
	$('[name="Religion"]').val(data['Emp_Religion']);
	$('[name="Nationality"]').val(data['Emp_Nationality']);
	$('[name="BirthDate"]').val(data['Emp_Birthdate']);
	$('[name="PlaceOfBirth"]').val(data['Emp_PlaceofBirth']);
	$('[name="DateHired"]').val(data['Emp_DateHired']);
	$('[name="SeparationDate"]').val(data['Emp_DateResign']);
	$('[name="ContactPerson"]').val(data['Emp_ContactPerson']);
	$('[name="ContactPersonTelNo"]').val(data['Emp_ContactNumber']);
	$('[name="ContactPersonAddress"]').val(data['Emp_ContactAddress']);
	$('[name="ContactPersonRelationship"]').val(data['Emp_ContactRelationship']);
	$('[name="MotherMaidedName"]').val(data['Emp_MotherMaidenName']);
	$('[name="EmployeeStatus"]').val(data['Emp_Status']);
	$('[name="Department"]').val(data['DEP_Description']);
	$('[name="PayrollStartDate"]').val(data['Emp_PayrollStart']);
	$('[name="BasicRateEnc"]').val(number_format(data['Emp_BasicRate'],2));
	$('[name="WorkLocation"]').val(data['SP_StoreName']);
	$('[name="UPCExpiryDate"]').val(data['Emp_BarcodeExpiry']);
	$('[name="PayGroupName"]').val(data['Emp_FK_GroupId']);
	$('[name="PayGroupID"]').val(data['AttendanceType']);

	switch(data['Emp_BaseRate']) {
	  case "D":
	    $('[name="BasicRate"]').val('Daily');
	    break;
	  case "W":
	    $('[name="BasicRate"]').val('Weekly');
	    break;
	  case "W":
	    $('[name="BasicRate"]').val('Monthly');
	    break;
	  default:
	    $('[name="BasicRate"]').val('');
	}

	// $('[name="BasicRate"]').val(number_format(data['Emp_BaseRate'],2));
	// $('[name="ProfessionalLicense"]').val(data['']);
	// $('[name="ProfessionalDescription"]').val(data['']);
	// $('[name="Principal"]').val(data['']);
	// $('[name="ProfessionalLicenseNo"]').val(data['']);

	// Personnel Data
	$('[name="ExemptionCode"]').val(data['TC_ExemtDesc']);
	$('[name="SSSNo"]').val(data['Emp_SSSNum']);
	$('[name="TinNo"]').val(data['Emp_TINNum']);
	$('[name="PagIbigNo"]').val(data['Emp_PagIbigNum']);
	$('[name="PhilhealthNo"]').val(data['Emp_PhilHealthNum']);
	$('[name="GracePeriod"]').val(number_format(data['Emp_GracePeriod'],0));
	$('[name="WorkHours"]').val(number_format(data['Emp_WorkHours'],2));
	$('[name="SeparationSavingsFund"]').val(number_format(data['Emp_SavingsFund'],2));
	$('[name="BankAccount"]').val(data['Emp_BankAccountNum']);
	$('[name="ClearanceDate"]').val(data['Emp_ClearanceDate']);
	$('[name="BankCode"]').val(data['Emp_AttrBankCode']);
	$('[name="PayrollMode"]').val(data['Emp_PayrollMode']);
	$('[name="FirstEvaluationSchedule"]').val(data['E_FirstEval']);
	$('[name="SecondEvaluationSchedule"]').val(data['E_SecondEval']);
	$('[name="ThirdEvaluationSchedule"]').val(data['E_ThirdEval']);
	$('[name="BankName"]').val(data['Emp_BankName']);

	// show button
	$('body [name="flPhoto"]').removeClass('invisible');
	$('body [name="btnUpdateImage"]').removeClass('invisible').val('Update Image');

	$('body [name="flDocument"]').removeClass('invisible');
	$('body [name="btnUploadDocument"]').removeClass('invisible').val('Upload Document');
	$('[name="memo_employee_id"]').val(data['Emp_Id']);

	$('#btnRelative').removeClass('invisible');
	$('#btnEducation').removeClass('invisible');
	$('#btnIDHistoryAdd').removeClass('invisible');
	$('#btnAllowanceAdd').removeClass('invisible');
	$('#btnNotesAdd').removeClass('invisible');
	$('#btnLeaveBalAdd').removeClass('invisible');

	// checkbox
	(data['Emp_ComputeSSS'] == '1') ? $('[name="ComputeSSS"]').prop('checked','true') : $('[name="ComputeSSS"]').removeProp('checked');
	(data['Emp_ComputeTax'] == '1') ? $('[name="Taxable"]').prop('checked','true') : $('[name="Taxable"]').removeProp('checked');
	(data['Emp_ComputePagIbig'] == '1') ? $('[name="ComputePagibig"]').prop('checked','true') : $('[name="ComputePagibig"]').removeProp('checked');
	(data['Emp_ComputePhilHealth'] == '1') ? $('[name="ComputePhilhealth"]').prop('checked','true') : $('[name="ComputePhilhealth"]').removeProp('checked');
	(data['Emp_HoldPayroll'] == '1') ? $('[name="HoldPayroll"]').prop('checked','true') : $('[name="HoldPayroll"]').removeProp('checked');
	(data['Emp_WithTimeAttendance'] == '1') ? $('[name="ProcessTimeAttendance"]').prop('checked','true') : $('[name="ProcessTimeAttendance"]').removeProp('checked');
	(data['Emp_AgencyEmployee'] == '1') ? $('[name="Agency"]').prop('checked','true') : $('[name="Agency"]').removeProp('checked');
	(data['Emp_LastPay'] == '1') ? $('[name="LastPay"]').prop('checked','true') : $('[name="LastPay"]').removeProp('checked');
	(data['Emp_Active'] == '1') ? $('[name="Active"]').prop('checked','true') : $('[name="Active"]').removeProp('checked');
	(data['Emp_TardyExemp'] == '1') ? $('[name="TardyExempt"]').prop('checked','true') : $('[name="TardyExempt"]').removeProp('checked');
	(data['Emp_UndertimeExempt'] == '1') ? $('[name="UndertimeExempt"]').prop('checked','true') : $('[name="UndertimeExempt"]').removeProp('checked');
	(data['Emp_CompressSched'] == '1') ? $('[name="CompressedSchedule"]').prop('checked','true') : $('[name="CompressedSchedule"]').removeProp('checked');
	(data['Emp_CheckBreak'] == '1') ? $('[name="CheckBreak"]').prop('checked','true') : $('[name="CheckBreak"]').removeProp('checked');

	// $('[name="ComputeSSS"]').val(data['Emp_ComputeSSS']);
	// $('[name="Taxable"]').val(data['Emp_ComputeTax']);
	// $('[name="ComputePagibig"]').val(data['Emp_ComputePagIbig']);
	// $('[name="ComputePhilhealth"]').val(data['Emp_ComputePhilHealth']);
	// $('[name="HoldPayroll"]').val(data['Emp_HoldPayroll']);
	// $('[name="ProcessTimeAttendance"]').val(data['Emp_WithTimeAttendance']);
	// $('[name="Agency"]').val(data['Emp_AgencyEmployee']);
	// $('[name="LastPay"]').val(data['Emp_LastPay']);
	// $('[name="Active"]').val(data['Emp_Active']);
	// $('[name="TardyExempt"]').val(data['Emp_TardyExemp']);
	// $('[name="UndertimeExempt"]').val(data['Emp_UndertimeExempt']);

	// Customized Contribution
	$('[name="SSSEEAmount"]').val(number_format(data['Emp_FixedSSSEEAmt'],2));
	$('[name="SSSERAmount"]').val(number_format(data['Emp_FixedSSSERAmt'],2));
	$('[name="FixedECAmount"]').val(number_format(data['Emp_SavingsFund'],2));
	$('[name="PagIbigEEAmount"]').val(number_format(data['Emp_FixedPagibigEEAmt'],2));
	$('[name="PagIbigERAmount"]').val(number_format(data['Emp_FixedPagibigERAmt'],2));
	$('[name="PhilhealthEEAmount"]').val(number_format(data['Emp_FixedPHealthEEAmt'],2));
	$('[name="PhilhealthERAmount"]').val(number_format(data['Emp_FixedPHealthERAmt'],2));
	$('[name="TaxContributionBasisAmount"]').val(number_format(data['Emp_FixedTax'],2));

	(data['Emp_Month13thBasis'] == '1') ? $('[name="13thMonthBasis"]').prop('checked','true') : $('[name="13thMonthBasis"]').removeProp('checked');
	// $('[name="13thMonthBasis"]').val(data['Emp_Month13thBasis']);
}


function InitializeModal_ID_History(){

	// send temporary data
	$('#detail-template-id #CompanyName').val(general_info_data['COM_Name']);
	$('#detail-template-id [name="Emp_CompanyId"]').val(general_info_data['Emp_CompanyId']);
	$('#detail-template-id [name="Emp_Id"]').val(general_info_data['Emp_Id']);

	// clear value
	$('#detail-template-id [name="Emp_ID_Card"]').val('');
	$('#detail-template-id [name="EID_BiometrixNo"]').val('');
	$('#detail-template-id [name="Emp_BarcodeID"]').val('');

	// events
	$('body #btnGenerateBarcodeID').on('click',function(e){
		e.preventDefault();
		let employee_name = general_info_data['Emp_Name'];
		swal({
	            title: "Are you sure?",
	            text: "Generate barcode Id for \"" + employee_name.toUpperCase() + "\" ?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: true
	        }, function () {
	        	$.get(global.site_name + 'administration/master_file/employee_profile/getGeneratedBarcode?id=' + general_info_data['Emp_Id'], function(data) {
	        			var GetLastDigit = JSON.parse(data);
	        			$('#detail-template-id [name="Emp_BarcodeID"]').val(GetLastDigit);
	        	});
	        }
	   );
	})

	$('body #btnSaveID').on('click',function(e){
		e.preventDefault();
		swal({
	            title: "Are you sure?",
	            text: "This will create new employee ID?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: false,
	        }, function () {
	        	
	        	let id_data = $('#form-id-history').serializeArray();
	        	let _validate = true;

	        	$(id_data).each(function(index){
					if(id_data[index].value == ""){
						_validate = false;
					}
				});

				if(!_validate){
					swal("Information", "Complete all fields to proceed", "info");
				}
				else{
					// show loading while saving
		        	swal({
		        		title 				: "Please wait",
		            	text 				: "Saving new ID.",
		            	type 				: "info",
		            	showConfirmButton 	: false 
		        	});

		        	$.ajax({
						type: 'POST',
						dataType: 'json',
						url: global.site_name + 'administration/master_file/employee_profile/saveID',
						data : id_data,
						success : function(result){
							if(result.success){
								swal("Saving Success!"," saving complete!", "success");
								setTimeout(function(){							
									$('#detail-template-id .close').trigger('click');
									tbl_IdHistory.load();
								},1000);
							}
							else{
								swal(result.title, result.message, result.type);
							}
						},error : function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
					// end of saving POST
				}

	        	
	        }
	   );
	})
}

function InitializeModal_PersonnelInfo_Relatives(){

	tbl_Siblings.draw();
	tbl_Beneficiaries.draw();

	// console.table(family_data);
	$('[name="Emp_Id"]').val(general_info_data['Emp_Id']);
	$('[name="Emp_FatherName"]').val(family_data['Emp_FatherName']);
	$('[name="Emp_FatherOccupation"]').val(family_data['Emp_FatherOccupation']);
	$('[name="Emp_FatherCompany"]').val(family_data['Emp_FatherCompany']);
	$('[name="Emp_FatherContact"]').val(family_data['Emp_FatherContact']);
	$('[name="Emp_MotherName"]').val(family_data['Emp_MotherName']);
	$('[name="Emp_MotherOccupation"]').val(family_data['Emp_MotherOccupation']);
	$('[name="Emp_MotherCompany"]').val(family_data['Emp_MotherCompany']);
	$('[name="Emp_MotherContact"]').val(family_data['Emp_MotherContact']);
	$('[name="Emp_SpouseName"]').val(family_data['Emp_SpouseName']);
	$('[name="Emp_SpouseOccupation"]').val(family_data['Emp_SpouseOccupation']);
	$('[name="Emp_SpouseCompany"]').val(family_data['Emp_SpouseCompany']);
	$('[name="Emp_SpouseContact"]').val(family_data['Emp_SpouseContact']);
	$('[name="Emp_SpouseAge"]').val(number_format(family_data['Emp_SpouseAge'],0));

	let employementstatus = '';
	if(family_data['ED_SpouseEmploymentStat'] == 'R'){
		employementstatus = 'Regular';
	}
	else if(family_data['ED_SpouseEmploymentStat'] == 'P'){
		employementstatus = 'Probationary';
	}
	else if(family_data['ED_SpouseEmploymentStat'] == 'C'){
		employementstatus = 'Contractual';
	}
	else if(family_data['ED_SpouseEmploymentStat'] == 'S'){
		employementstatus = 'Seasonal';
	}

	// $('[name="ED_SpouseEmploymentStat"]').select2('data', {id: family_data['ED_SpouseEmploymentStat'], text: employementstatus});
	$('[name="ED_SpouseEmploymentStat"]').select2("val",family_data['ED_SpouseEmploymentStat']);

	$('body').on('click','#btnRelativeUpdate',function(e){
		e.preventDefault();
		swal({
	            title: "Are you sure?",
	            text: "This will update family record?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: false,
	        }, function () {
	        	
	        	let id_data = $('#form-relatives').serializeArray();
	        	
				// show loading while saving
	        	swal({
	        		title 				: "Please wait",
	            	text 				: "updating family record.",
	            	type 				: "info",
	            	showConfirmButton 	: false 
	        	});

	        	$.ajax({
					type: 'POST',
					dataType: 'json',
					url: global.site_name + 'administration/master_file/employee_profile/saveFamily',
					data : id_data,
					success : function(result){
						if(result.success){
							swal("Updating Success!"," updating complete!", "success");
							setTimeout(function(){							
								$('#detail-template-relatives .close').trigger('click');
							},1000);
						}
						else{
							swal(result.title, result.message, result.type);
						}
					},error : function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				});
				// end of saving POST
				

	        	
	        }
	   );
	})

	// sibling modal events
	$('body').on('click','#btnSiblingsAdd',function(e){
		e.preventDefault();
		siblings_data = [];
		$("#detail-template-siblings").show();
		$("#detail-template-siblings").modal('show');
		$('.modal').css('overflow-y', 'auto');
		InitiliazeChildModal_Siblings('add');
	});

	$('body').on('click','.sibling-edit',function(e){
		e.preventDefault();
		siblings_data = $(this).data('record');
		$("#detail-template-siblings").show();
		$("#detail-template-siblings").modal('show');
		$('.modal').css('overflow-y', 'auto');
		InitiliazeChildModal_Siblings('update');
	});

	$('body').on('click','.sibling-delete',function(e){
		e.preventDefault();
		siblings_data = $(this).data('record');
		InitializeDeleteData('Siblings',siblings_data);
	});

	// beneficiaries modal events
	$('body').on('click','#btnBeneficiariesAdd',function(e){
		e.preventDefault();
		beneficiaries_data = [];
		$("#detail-template-beneficiaries").show();
		$("#detail-template-beneficiaries").modal('show');
		$('.modal').css('overflow-y', 'auto');
		InitiliazeChildModal_Beneficiaries('add');
	});

	$('body').on('click','.beneficiaries-edit',function(e){
		e.preventDefault();
		beneficiaries_data = $(this).data('record');
		$("#detail-template-beneficiaries").show();
		$("#detail-template-beneficiaries").modal('show');
		$('.modal').css('overflow-y', 'auto');
		InitiliazeChildModal_Beneficiaries('update');
	});

	$('body').on('click','.beneficiaries-delete',function(e){
		e.preventDefault();
		beneficiaries_data = $(this).data('record');
		InitializeDeleteData('Beneficiaries',beneficiaries_data);
	});

}

function InitiliazeChildModal_Siblings(mode){
	
	if(mode === 'add'){
		$('#detail-template-siblings input').val('');
		$('#detail-template-siblings select').select2("val",'');

		$('#detail-template-siblings [name="R_FK_RelationAtt"]').select2({
			// dropdownParent: $('#detail-template-siblings')
			minimumResultsForSearch: -1
		});
		$('#detail-template-siblings [name="Status"]').select2({
			// dropdownParent: $('#detail-template-siblings')
			minimumResultsForSearch: -1
		});
	}
	else{
		console.log(siblings_data);

		let status_id = '';
		switch(siblings_data['Status']){
		  case "Single":
		    status_id = 'S';
		    break;
		  case "Married":
		    status_id = 'M';
		    break;
		  case "Divored":
		    status_id = 'D';
		    break;
		  case "Seperated":
		    status_id = 'S1';
		    break;
		  default:
		}

		$('#detail-template-siblings [name="R_RecordNum"]').val(siblings_data['R_RecordNum']);
		$('#detail-template-siblings [name="R_Name"]').val(siblings_data['R_Name']);
		$('#detail-template-siblings [name="R_Birthdate"]').val(moment(siblings_data['R_Birthdate']).format('MM/DD/YYYY'));
		$('#detail-template-siblings [name="R_FK_RelationAtt"]').select2("val",siblings_data['AD_Id']);
		$('#detail-template-siblings [name="Status"]').select2("val",status_id);
		$('#detail-template-siblings [name="WorkType"]').val(siblings_data['WorkType']);
		$('#detail-template-siblings [name="CompanyAdd"]').val(siblings_data['CompanyAdd']);
	}

	$('#detail-template-siblings [name="mode"]').val(mode);
	$('#detail-template-siblings [name="R_EmpId"]').val(general_info_data['Emp_Id']);
	if(siblings_data.length > 0){
		$('#detail-template-siblings [name="R_RecordNum"]').val(siblings_data['R_RecordNum']);
	}

	$('body').on('click', '#detail-template-siblings #btnSiblingSaveandNew', function(e){
		console.log($(this).attr('id'));
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveSiblings(btnValue);

	})

	$('body').on('click', '#detail-template-siblings #btnSiblingSaveandClose', function(e){
		console.log($(this).attr('id'));
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveSiblings(btnValue);
	})

	$('body').on('click', '#detail-template-siblings .close-modal', function(e){
		e.preventDefault();
		$('.modal-backdrop').hide();
		$('#detail-template-siblings').hide();
	})
}

// save/update Silbling
function InitializeSaveSiblings(btnValue){

	let blnstatus = true
	var type = '';
	if(btnValue == 'SaveandNew' || btnValue == 'SaveandClose'){
		type = 'add';
	}
	else{
		type = 'update';
	}

	let id_data = $('#form-siblings').find(":input:not(:hidden)").serializeArray();
	let _validate = true;

	$(id_data).each(function(index){
		if(id_data[index].value == ""){
			console.log(id_data[index]);
			_validate = false;
			blnstatus = false;
		}
	});

	hidden_data = $('#form-siblings').find(':input[type="hidden"]').serializeArray()
	id_data = id_data.concat(hidden_data);

	console.log(_validate)
	if(!_validate){
		swal("Information", "Complete all fields to proceed", "info");
		blnstatus = false;
	}
	else{
		swal({
	            title: "Are you sure?",
	            text: type == 'add' ? "This will save the record?" : "This will update the record?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: true,
	        }, function () {
	        				
		        	console.log(id_data,"Siblings Data");
		        	$.ajax({
						type: 'POST',
						dataType: 'json',
						url: global.site_name + 'administration/master_file/employee_profile/saveFamilySiblings',
						data : id_data,
						success : function(result){
							if(!result.success){
								swal(result.title, result.message, result.type);
								blnstatus = false;
							}
							else{
								if(btnValue == 'SaveandNew'){
									$('#detail-template-siblings input').val('');
									$('#detail-template-siblings select').select2("val",'');
									$('#detail-template-siblings [name="mode"]').val('add');
									$('#detail-template-siblings [name="R_EmpId"]').val(general_info_data['Emp_Id']);
								}
								else{
									$('#detail-template-siblings input').val('');
									$('#detail-template-siblings select').select2("val",'');
									$('.modal-backdrop').hide();
									$('#detail-template-siblings').hide();
								}
								tbl_Siblings.load().draw();

							}
						},error : function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
					// end of saving POST
				}
	   );
	}
	

	return blnstatus;
}

function InitiliazeChildModal_Beneficiaries(mode){
	
	if(mode === 'add'){
		$('#detail-template-beneficiaries input').val('');
		$('#detail-template-beneficiaries select').select2("val",'');
	}
	else{
		console.log(beneficiaries_data);

		let status_id = '';
		switch(beneficiaries_data['Status']){
		  case "Single":
		    status_id = 'S';
		    break;
		  case "Married":
		    status_id = 'M';
		    break;
		  case "Divored":
		    status_id = 'D';
		    break;
		  case "Seperated":
		    status_id = 'S1';
		    break;
		  default:
		}

		$('#detail-template-beneficiaries [name="B_RecordNum"]').val(beneficiaries_data['B_RecordNum']);
		$('#detail-template-beneficiaries [name="B_Name"]').val(beneficiaries_data['B_Name']);
		$('#detail-template-beneficiaries [name="B_FK_RelationAtt"]').select2("val",beneficiaries_data['AD_Id']);
		$('#detail-template-beneficiaries [name="B_Status"]').select2("val",status_id);
		$('#detail-template-beneficiaries [name="B_WorkType"]').val(beneficiaries_data['B_WorkType']);
		$('#detail-template-beneficiaries [name="B_CompanyAdd"]').val(beneficiaries_data['B_CompanyAdd']);
	}

	// events in add or update
	$('#detail-template-beneficiaries [name="mode"]').val(mode);
	$('#detail-template-beneficiaries [name="B_EmpId"]').val(general_info_data['Emp_Id']);
	if(beneficiaries_data.length > 0){
		$('#detail-template-beneficiaries [name="B_RecordNum"]').val(beneficiaries_data['B_RecordNum']);
	}

	$('body').on('click', '#detail-template-beneficiaries #btnBeneficiariesSaveandNew', function(e){
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveBeneficiaries(btnValue);		
	})	

	$('body').on('click', '#detail-template-beneficiaries #btnBeneficiariesSaveandClose', function(e){
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveBeneficiaries(btnValue);
	})	

	$('body').on('click', '#detail-template-beneficiaries .close-modal', function(e){
		e.preventDefault();
		$('.modal-backdrop').hide();
		$('#detail-template-beneficiaries').hide();
	})	
}

// save/update Beneficiaries
function InitializeSaveBeneficiaries(btnValue){

	var type = '';
	if(btnValue == 'SaveandNew' || btnValue == 'SaveandClose'){
		type = 'add';
	}
	else{
		type = 'update';
	}

	let id_data = $('#form-beneficiaries').find(":input:not(:hidden)").serializeArray();
	let _validate = true;

	$(id_data).each(function(index){
		if(id_data[index].value == ""){
			_validate = false;
		}
	});

	hidden_data = $('#form-beneficiaries').find(':input[type="hidden"]').serializeArray()
	id_data = id_data.concat(hidden_data);

	if(!_validate){
		swal("Information", "Complete all fields to proceed", "info");
	}
	else{
		swal({
	            title: "Are you sure?",
	            text: type == 'add' ? "This will save the record?" : "This will update the record?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: true,
	        }, function () {

		        	console.log(id_data,"Beneficiaries Data");
		        	$.ajax({
						type: 'POST',
						dataType: 'json',
						url: global.site_name + 'administration/master_file/employee_profile/saveFamilyBeneficiaries',
						data : id_data,
						success : function(result){
							if(!result.success){
								swal(result.title, result.message, result.type);
							}
							else{
								if(btnValue == 'SaveandNew'){
									$('#detail-template-beneficiaries input').val('');
									$('#detail-template-beneficiaries select').select2("val",'');
									$('#detail-template-beneficiaries [name="mode"]').val('add');
									$('#detail-template-beneficiaries [name="B_EmpId"]').val(general_info_data['Emp_Id']);
								}
								else{
									$('#detail-template-beneficiaries input').val('');
									$('#detail-template-beneficiaries select').select2("val",'');
									$('.modal-backdrop').hide();
									$('#detail-template-beneficiaries').hide();
								}
								tbl_Beneficiaries.load().draw();
							}
						},error : function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}

					});
				}
				// end of saving POST
	   );
	}
}

function InitializeDeleteData(type,data){
	switch(type){
		case "Siblings":
			swal({
			            title: "Are you sure?",
			            text: "This will delete the record?",
			            type: "warning",
			            showCancelButton: true,
			            confirmButtonColor: "#1AB394",
			            confirmButtonText: "Yes",
			            cancelButtonText: "No",
			            closeOnConfirm: true,
			        }, function () {
				        	$.ajax({
								type: 'POST',
								dataType: 'json',
								url: global.site_name + 'administration/master_file/employee_profile/deleteSiblings',
								data : data,
								success : function(result){
									if(!result.success){
										swal(result.title, result.message, result.type);
									}
									tbl_Siblings.load().draw();
								},error : function(jqXHR){
									swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
								}
							});
					}
					// end of saving POST
			   );
			break;
		case "Beneficiaries":
			swal({
			            title: "Are you sure?",
			            text: "This will delete the record?",
			            type: "warning",
			            showCancelButton: true,
			            confirmButtonColor: "#1AB394",
			            confirmButtonText: "Yes",
			            cancelButtonText: "No",
			            closeOnConfirm: true,
			        }, function () {
				        	$.ajax({
								type: 'POST',
								dataType: 'json',
								url: global.site_name + 'administration/master_file/employee_profile/deleteBeneficiaries',
								data : data,
								success : function(result){
									if(!result.success){
										swal(result.title, result.message, result.type);
									}
									tbl_Beneficiaries.load().draw();
								},error : function(jqXHR){
									swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
								}
							});
					}
					// end of saving POST
			   );
			break;	
		case "Education":
			swal({
			            title: "Are you sure?",
			            text: "This will delete the record?",
			            type: "warning",
			            showCancelButton: true,
			            confirmButtonColor: "#1AB394",
			            confirmButtonText: "Yes",
			            cancelButtonText: "No",
			            closeOnConfirm: true,
			        }, function () {
				        	$.ajax({
								type: 'POST',
								dataType: 'json',
								url: global.site_name + 'administration/master_file/employee_profile/deleteEducationChild',
								data : data,
								success : function(result){
									if(!result.success){
										swal(result.title, result.message, result.type);
									}
									tbl_Education.load().draw();
								},error : function(jqXHR){
									swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
								}
							});
					}
					// end of saving POST
			   );
			break;	
		case "Allowance":
			swal({
			            title: "Are you sure?",
			            text: "This will delete the record?",
			            type: "warning",
			            showCancelButton: true,
			            confirmButtonColor: "#1AB394",
			            confirmButtonText: "Yes",
			            cancelButtonText: "No",
			            closeOnConfirm: true,
			        }, function () {
				        	$.ajax({
								type: 'POST',
								dataType: 'json',
								url: global.site_name + 'administration/master_file/employee_profile/deleteAllowanceRecord',
								data : data,
								success : function(result){
									if(!result.success){
										swal(result.title, result.message, result.type);
									}
									tbl_Allowance.load().draw();
								},error : function(jqXHR){
									swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
								}
							});
					}
					// end of saving POST
			   );
			break;
		case "Notes":
			swal({
			            title: "Are you sure?",
			            text: "This will delete the record?",
			            type: "warning",
			            showCancelButton: true,
			            confirmButtonColor: "#1AB394",
			            confirmButtonText: "Yes",
			            cancelButtonText: "No",
			            closeOnConfirm: true,
			        }, function () {
				        	$.ajax({
								type: 'POST',
								dataType: 'json',
								url: global.site_name + 'administration/master_file/employee_profile/deleteNotesRecord',
								data : data,
								success : function(result){
									if(!result.success){
										swal(result.title, result.message, result.type);
									}
									tbl_Notes.load().draw();
								},error : function(jqXHR){
									swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
								}
							});
					}
					// end of saving POST
			   );
			break;
		case "Leave Balance":
			swal({
			            title: "Are you sure?",
			            text: "This will delete the record?",
			            type: "warning",
			            showCancelButton: true,
			            confirmButtonColor: "#1AB394",
			            confirmButtonText: "Yes",
			            cancelButtonText: "No",
			            closeOnConfirm: true,
			        }, function () {
				        	$.ajax({
								type: 'POST',
								dataType: 'json',
								url: global.site_name + 'administration/master_file/employee_profile/deleteLeaveBalance',
								data : data,
								success : function(result){
									if(!result.success){
										swal(result.title, result.message, result.type);
									}
									tbl_LeaveType.load().draw();
								},error : function(jqXHR){
									swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
								}
							});
					}
					// end of saving POST
			   );
			break;		
		default:
			break;
	}
}


function InitializeModal_PersonnelInfo_Education(){

	tbl_Education.draw();
	console.log(education_data, "Education Data");

	// $('[name="StudyCourse"]').val(education_data['ED_Course'];
	// $('[name="ED_Exam"]').val(education_data['ED_Exam']);

	(education_data['ED_ContinueStudy'] == '1') ? $('[name="ED_ContinueStudy"]').prop('checked','true') : $('[name="ED_ContinueStudy"]').removeProp('checked');

	// if true [continuestudy] event
	if(education_data['ED_ContinueStudy'] == '1'){
		$('[name="ED_Course"]').val('').removeAttr('readonly');
		$('[name="ED_ContinueStudyWhen"]').val('').removeAttr('readonly');
	}
	else{
		$('[name="ED_Course"]').val('').attr('readonly');
		$('[name="ED_ContinueStudyWhen"]').val('').attr('readonly');
	}

	$('body [name="ED_ContinueStudy"]').on('click',function(e){
		console.log($(this).prop('checked'));
		if($(this).prop('checked') === true){
			$('[name="ED_Course"]').val('').removeAttr('readonly');
			$('[name="ED_ContinueStudyWhen"]').val('').removeAttr('readonly');
		}
		else{
			$('[name="ED_Course"]').val('').attr('readonly');
			$('[name="ED_ContinueStudyWhen"]').val('').attr('readonly');
		}
	});

	(education_data['ED_Exam'] == '1') ? $('[name="ED_Exam"]').prop('checked','true') : $('[name="ED_Exam"]').removeProp('checked');

	// if true [continuestudy] event
	$('.fieldExamDesc').css('display', 'none');
	$('[name="ED_ExamDesc"]').val('');
	if(education_data['ED_Exam'] == '1'){
		$('[name="ED_ExamType"]').select2('enable',true);
		$('[name="ED_ExamRating"]').val('').removeAttr('readonly');
	}
	else{
		$('[name="ED_ExamType"]').select2('enable',false);
		$('[name="ED_ExamRating"]').val('').attr('readonly');
	}

	if(education_data['ED_ExamType'] == '3'){
		$('.fieldExamDesc').css('display', '');
		$('[name="ED_ExamDesc"]').val('').removeAttr('readonly');
	}
	else{
		$('.fieldExamDesc').css('display', 'none');
		$('[name="ED_ExamDesc"]').val('').attr('readonly');
	}

	$('body [name="ED_Exam"]').on('click',function(e){
		$('.fieldExamDesc').css('display', 'none');
		$('[name="ED_ExamDesc"]').val('');
		if($(this).prop('checked') === true){
			$('[name="ED_ExamType"]').select2('enable',true);
			$('[name="ED_ExamRating"]').val('').removeAttr('readonly');
		}
		else{
			$('[name="ED_ExamType"]').select2('enable',false);
			$('[name="ED_ExamRating"]').val('').attr('readonly');
		}
	})

	$('body [name="ED_ExamType"]').on('select2:select',function(e){
		var id = e.params.data.id;
		if(id == '3'){
			$('.fieldExamDesc').css('display', '');
			$('[name="ED_ExamDesc"]').val('').removeAttr('readonly');
		}
		else{
			$('.fieldExamDesc').css('display', 'none');
			$('[name="ED_ExamDesc"]').val('').attr('readonly');
		}
	});

	$('body [name="ED_ExamType"]').on('select2:unselecting', function (e) {
		$('.fieldExamDesc').css('display', 'none');
		$('[name="ED_ExamDesc"]').val('').attr('readonly');
	});
	
	$('[name="ED_Course"]').val(education_data['ED_Course']);
	$('[name="ED_ContinueStudyWhen"]').val(education_data['ED_ContinueStudyWhen']);
	$('[name="ED_ExamDesc"]').val(education_data['ED_ExamDesc']);
	$('[name="ED_ExamType"]').select2("val",education_data['ED_ExamType']);
	$('[name="ED_ExamRating"]').val(education_data['ED_ExamRating']);
	$('[name="Emp_Id"]').val(general_info_data['Emp_Id']);

	$('body').on('click','#btnEducationUpdate',function(e){
		e.preventDefault();
		swal({
	            title: "Are you sure?",
	            text: "This will update education record?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: false,
	        }, function () {
	        	
	        	let id_data = $('#form-education').serializeArray();
	        	var data = {};

	        	$(id_data).each(function(index){
					data[id_data[index].name] = id_data[index].value;
				});

	        	$('#form-education input[type="checkbox"]').each(function(index,e){
			        let chkName = $(this).attr('name');
			        id_data[chkName] = $(this).is(":checked") ? '1' : '0';
			        data[$(this).attr('name')] = $(this).is(":checked") ? '1' : '0';
			    });

			    console.log(data);

				// show loading while saving
	        	swal({
	        		title 				: "Please wait",
	            	text 				: "updating education record.",
	            	type 				: "info",
	            	showConfirmButton 	: false 
	        	});

	        	$.ajax({
					type: 'POST',
					dataType: 'json',
					url: global.site_name + 'administration/master_file/employee_profile/saveEducation',
					data : data,
					success : function(result){
						if(result.success){
							swal("Updating Success!"," updating complete!", "success");
							setTimeout(function(){							
								$('#detail-template-education .close').trigger('click');
							},1000);
						}
						else{
							swal(result.title, result.message, result.type);
						}
					},error : function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				});
				// end of saving POST
				

	        	
	        }
	   );
	});

	$('body').on('click','#btnEducationAdd',function(e){
		e.preventDefault();
		$("#detail-template-child-education").show();
		$("#detail-template-child-education").modal('show');
		$('.modal').css('overflow-y', 'auto');
		InitiliazeChildModal_Education('add');
	});

	$('body').on('click','.education-edit',function(e){
		e.preventDefault();
		child_education_data = $(this).data('record');
		$("#detail-template-child-education").show();
		$("#detail-template-child-education").modal('show');
		$('.modal').css('overflow-y', 'auto');
		InitiliazeChildModal_Education('update');
	});

	$('body').on('click','.education-delete',function(e){
		e.preventDefault();
		child_education_data = $(this).data('record');
		InitializeDeleteData('Education',child_education_data);
	});
}

function InitiliazeChildModal_Education(mode){
	if(mode === 'add'){
		$('#detail-template-child-education input').val('');
		$('#detail-template-child-education select').select2("val",'');
	}
	else{
		console.log(child_education_data);
		$('[name="EDU_Levels"]').select2("val",child_education_data['EDU_Levels']);
		$('[name="EDU_SchoolYear"]').select2("val",child_education_data['EDU_SchoolYear']);
		$('[name="EDU_SchoolAttended"]').val(child_education_data['EDU_SchoolAttended']);
		$('[name="EDU_CourseTaken"]').val(child_education_data['EDU_CourseTaken']);
		$('[name="EDU_HonorReceived"]').val(child_education_data['EDU_HonorReceived']);

	}

	$('#detail-template-child-education [name="mode"]').val(mode);
	$('#detail-template-child-education [name="EDU_Id"]').val(general_info_data['Emp_Id']);
	if($(child_education_data).length > 0){
		$('#detail-template-child-education [name="EDU_LineNo"]').val(child_education_data['EDU_LineNo']);
	}

	$('body').on('click', '#detail-template-child-education #btnChildEducationSaveandNew', function(e){
		console.log($(this).attr('id'));
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveEducation(btnValue);

	})

	$('body').on('click', '#detail-template-child-education #btnChildEducationSaveandClose', function(e){
		console.log($(this).attr('id'));
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveEducation(btnValue);
	})

	$('body').on('click', '#detail-template-child-education .close-modal', function(e){
		e.preventDefault();
		$('.modal-backdrop').hide();
		$('#detail-template-child-education').hide();
	})
}

function InitializeSaveEducation(btnValue){
	let blnstatus = true
	var type = '';
	if(btnValue == 'SaveandNew' || btnValue == 'SaveandClose'){
		type = 'add';
	}
	else{
		type = 'update';
	}

	let id_data = $('#form-child-education').find(":input:not(:hidden)").serializeArray();
	let _validate = true;

	$(id_data).each(function(index){
		if(id_data[index].value == ""){
			console.log(id_data[index]);
			_validate = false;
			blnstatus = false;
		}
	});

	hidden_data = $('#form-child-education').find(':input[type="hidden"]').serializeArray()
	id_data = id_data.concat(hidden_data);

	console.log(_validate)
	if(!_validate){
		swal("Information", "Complete all fields to proceed", "info");
		blnstatus = false;
	}
	else{
		swal({
	            title: "Are you sure?",
	            text: type == 'add' ? "This will save the record?" : "This will update the record?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: true,
	        }, function () {
	        				
		        	console.log(id_data,"Child Education Data");
		        	$.ajax({
						type: 'POST',
						dataType: 'json',
						url: global.site_name + 'administration/master_file/employee_profile/saveEducationChild',
						data : id_data,
						success : function(result){
							if(!result.success){
								swal(result.title, result.message, result.type);
								blnstatus = false;
							}
							else{
								if(btnValue == 'SaveandNew'){
									$('#detail-template-child-education input').val('');
									$('#detail-template-child-education select').select2("val",'');
									$('#detail-template-child-education [name="mode"]').val('add');
									$('#detail-template-child-education [name="EDU_Id"]').val(general_info_data['Emp_Id']);
								}
								else{
									$('#detail-template-child-education input').val('');
									$('#detail-template-child-education select').select2("val",'');
									$('.modal-backdrop').hide();
									$('#detail-template-child-education').hide();
								}
								tbl_Education.load().draw();

							}
						},error : function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
					// end of saving POST
				}
	   );
	}
	

	return blnstatus;
}

function InitializeModal_PersonnelInfo_Allowance(mode){

	if(mode == 'add'){
		$('#detail-template-allowance [name="mode"]').val('add');
	}
	else{
		console.log(allowance_data);
		$('#detail-template-allowance [name="Allow_Desc"]').html('<option value="' + allowance_data['Allow_Id']+ '" selected ="selected">' + allowance_data['PI_Desc'] + ' </option>');
		$('#detail-template-allowance [name="Allow_Desc"]').select2("val", allowance_data['Allow_Id']);
		$('#detail-template-allowance [name="Allow_Id"]').html('<option value="' + allowance_data['PI_Desc']+ '" selected ="selected">' + allowance_data['Allow_Id'] + ' </option>');
		$('#detail-template-allowance [name="Allow_Id"]').select2("val", allowance_data['PI_Desc']);
		getAllowanceDetailData(allowance_data['Allow_Id']);
		$('#detail-template-allowance [name="Allow_Amount"]').val(number_format(allowance_data['Allow_Amount'],2,".",""));
		$('#detail-template-allowance [name="mode"]').val('update');
		$('#detail-template-allowance [name="FK_Emp_Id"]').val(general_info_data['Emp_Id']);
	}
	
	$('#detail-template-allowance [name="FK_Emp_Id"]').val(general_info_data['Emp_Id']);

	 $('[name="Allow_Id"]').select2({
    	ajax: {
			url: global.site_name + 'administration/master_file/employee_profile/getFilterData',
			dataType: 'json',
			delay: 250,
			data: function (params) {

			  var queryParameters = {
					'filter-search'	: general_info_data['Emp_Id'],
					'filter-type'	: 'payitems',
					'filter-addtl'	: 'Id ASC',
					'filter-input'	: params.term
			  }
			  return queryParameters;
			},
			processResults: function (data, params) {
				return {
					results: $.map(data, function(item) {
			            return {
			                id: item.Description,
			                text: item.Id
			            }
			        })
				};
			}
		},
		placeholder: 	"Search by Id",
		allowClear: 	true,
		// tags: true,
    	dropdownParent: $("#detail-template-allowance")
    });

	$('[name="Allow_Desc"]').select2({
    	ajax: {
			url: global.site_name + 'administration/master_file/employee_profile/getFilterData',
			dataType: 'json',
			delay: 250,
			data: function (params) {
			  
			  var queryParameters = {
					'filter-search'	: general_info_data['Emp_Id'],
					'filter-type'	: 'payitems',
					'filter-addtl'	: 'Description ASC',
					'filter-input'	: params.term
			  }
			  return queryParameters;
			},
			processResults: function (data, params) {
				return {
					results: $.map(data, function(item) {
			            return {
			                id: item.Id,
			                text: item.Description
			            }
			        })
				};
			}
		},
		placeholder: 	"Search by Description",
		allowClear: 	true,
		// tags: true,
    	dropdownParent: $("#detail-template-allowance")
    });

    $('[name="Allow_Id"]').on("select2:select", function(e) {
    	//console.log($(this).data('type'),"TYPE");
		//console.log($(this).select2('data')[0],"DATA ATTR");

		let data = $(this).select2('data')[0];
		$('#detail-template-allowance [name="Allow_Desc"]').html('<option value="' + data['text']+ '" selected ="selected">' + data['id'] + ' </option>');
		$('#detail-template-allowance [name="Allow_Desc"]').select2("val", data['text']);
		getAllowanceDetailData(data['text']);
    });

    $('[name="Allow_Desc"]').on("select2:select", function(e) {
    	//console.log($(this).data('type'),"TYPE");
		//console.log($(this).select2('data')[0],"DATA ATTR");

		let data = $(this).select2('data')[0];
		$('#detail-template-allowance [name="Allow_Id"]').html('<option value="' + data['text']+ '" selected ="selected">' + data['id'] + ' </option>');
		$('#detail-template-allowance [name="Allow_Id"]').select2("val", data['text']);
		getAllowanceDetailData(data['id']);
    });

    $('[name="Allow_Id"] , [name="Allow_Desc"]').on('select2:unselecting', function (e) {
    	$('[name="Allow_Id"]').select2("val","");
    	$('[name="Allow_Desc"]').select2("val","");
		$('[name="Taxable"]').removeProp('checked');
		$('[name="DeductionType"]').val('');
		$('[name="Cutoff"]').val('');
		$('[name="Allow_Amount"]').val(0);
	});

	$('body').on('click', '#detail-template-allowance #btnSaveandNew', function(e){
		console.log($(this).attr('id'));
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveAllowance(btnValue);

	})

	$('body').on('click', '#detail-template-allowance #btnSaveandClose', function(e){
		console.log($(this).attr('id'));
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveAllowance(btnValue);
	})

	
}

function getAllowanceDetailData(id){
	$.get(global.site_name + 'administration/master_file/employee_profile/getAllowanceDetailData?id=' + general_info_data['Emp_Id'] + '&paymentId=' + id, function(value) {
		let allowance_data = JSON.parse(value);			
		InitializeAllowanceDetail(allowance_data);
	});
}

function InitializeAllowanceDetail(allowance_data){
	console.log(allowance_data);
	(allowance_data['PI_Taxable'] == '1') ? $('[name="Taxable"]').prop('checked','true') : $('[name="Taxable"]').removeProp('checked');

	switch(allowance_data['PI_Cutoff']){
		case 1:
			$('[name="Cutoff"]').val('1st CutOff');
			break;
		case 2:
			$('[name="Cutoff"]').val('2nd CutOff');
			break;
		case 3:
			$('[name="Cutoff"]').val('3rd CutOff');
			break;
		case 4:
			$('[name="Cutoff"]').val('4th CutOff');
			break;
		case 5:
			$('[name="Cutoff"]').val('Split');
			break;
		default:
			break;
	}

	switch(allowance_data['PI_CutoffType']){
		case "M":
			$('[name="DeductionType"]').val('Monthly');
			break;
		case "S":
			$('[name="DeductionType"]').val('Semi Monthly');
			break;
		case "W":
			$('[name="DeductionType"]').val('Weekly');
			break;
			break;
		default:
			break;
	}
}

function InitializeSaveAllowance(btnValue){
	let blnstatus = true
	var type = '';
	if(btnValue == 'SaveandNew' || btnValue == 'SaveandClose'){
		type = 'add';
	}
	else{
		type = 'update';
	}

	let id_data = $('#form-allowance').find(":input:not(:hidden)").serializeArray();
	let _validate = true;

	$(id_data).each(function(index){
		if(id_data[index].value == ""){
			console.log(id_data[index]);
			_validate = false;
			blnstatus = false;
		}
	});

	hidden_data = $('#form-allowance').find(':input[type="hidden"]').serializeArray()
	id_data = id_data.concat(hidden_data);

	console.log(_validate)
	if(!_validate){
		swal("Information", "Complete all fields to proceed", "info");
		blnstatus = false;
	}
	else{
		swal({
	            title: "Are you sure?",
	            text: type == 'add' ? "This will save the record?" : "This will update the record?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: true,
	        }, function () {
	        				
		        	console.log(id_data,"Allowance Data");
		        	$.ajax({
						type: 'POST',
						dataType: 'json',
						url: global.site_name + 'administration/master_file/employee_profile/saveAllowanceRecord',
						data : id_data,
						success : function(result){
							if(!result.success){
								swal(result.title, result.message, result.type);
								blnstatus = false;
							}
							else{
								console.log(btnValue,"allowance saving event");
								if(btnValue == 'SaveandNew'){
									$('#detail-template-allowance input').val('');
									$('#detail-template-allowance select').select2("val",'');
									$('#detail-template-allowance [name="mode"]').val('add');
									$('#detail-template-allowance [name="FK_Emp_Id"]').val(general_info_data['Emp_Id']);
								}
								else{
									$('#detail-template-allowance input').val('');
									$('#detail-template-allowance select').select2("val",'');	
									$('#detail-template-allowance .close').trigger('click');									
								}
								tbl_Allowance.load().draw();

							}
						},error : function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
					// end of saving POST
				}
	   );
	}
	

	return blnstatus;
}


function InitializeHired(){
	$('body').on('click','#btnApplyHired',function(e){
		e.preventDefault();
		let id_data = $('#form-hired').find(":input:not(:hidden)").serializeArray();
		
		hidden_data = $('#form-hired').find(':input[type="hidden"]').serializeArray()
		id_data = id_data.concat(hidden_data);

		swal({
	            title: "Are you sure?",
	            text: "This will hired the selected employee?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: false,
	        }, function () {
	        				
		        	$.ajax({
						type: 'POST',
						dataType: 'json',
						url: global.site_name + 'administration/master_file/employee_profile/updateHiredRecord',
						data : id_data,
						success : function(result){
							if(!result.success){
								swal(result.title, result.message, result.type);
								blnstatus = false;
							}
							else{
								swal("Success", "Employee hired updated.", "success");
								$('#detail-template-hired .close').trigger('click');
								InitializeClickEvent();
								tableindex.draw();
							}
						},error : function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
					// end of saving POST
			});
		
	});	
}

function InitializeResign(){
	$('body').on('click','#btnApplyResign',function(e){
		e.preventDefault();
		let id_data = $('#form-resign').find(":input:not(:hidden)").serializeArray();
		let _validate = true;

		hidden_data = $('#form-resign').find(':input[type="hidden"]').serializeArray()
		id_data = id_data.concat(hidden_data);

		swal({
	            title: "Are you sure?",
	            text: "This will resign the selected employee?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: false,
	        }, function () {
	        				
		        	$.ajax({
						type: 'POST',
						dataType: 'json',
						url: global.site_name + 'administration/master_file/employee_profile/updateResignRecord',
						data : id_data,
						success : function(result){
							if(!result.success){
								swal(result.title, result.message, result.type);
								blnstatus = false;
							}
							else{
								swal("Success", "Employee resign updated.", "success");
								$('#detail-template-resign .close').trigger('click');
								InitializeClickEvent();
								tableindex.draw();
							}
						},error : function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
					// end of saving POST
			});
		
	});	
}

function InitializeNotes(mode){

	if(mode == 'add'){
		$('#detail-template-notes [name="mode"]').val('add');
		$('#detail-template-notes [name="EmpN_RecNo"]').val('');
		$('#detail-template-notes [name="EmpN_Id"]').val(general_info_data['Emp_Id']);
		$('#detail-template-notes input').val('');
		$('#detail-template-notes textarea').val('');
	}
	else{
		$('#detail-template-notes [name="mode"]').val('update');
		$('#detail-template-notes [name="EmpN_RecNo"]').val(notes_data['EmpN_RecNo']);
		$('#detail-template-notes [name="EmpN_Id"]').val(notes_data['EmpN_Id']);
		$('#detail-template-notes [name="EmpN_Date"]').val(moment(notes_data['EmpN_Date']).format("MM/DD/YYYY"));
		$('#detail-template-notes [name="EmpN_Remarks"]').val(notes_data['EmpN_Remarks']);
	}

	$('body').on('click', '#detail-template-notes #btnNotesSaveandNew', function(e){
		console.log($(this).attr('id'));
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveNotes(btnValue);

	})

	$('body').on('click', '#detail-template-notes #btnNotesSaveandClose', function(e){
		console.log($(this).attr('id'));
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveNotes(btnValue);
	})
}

function InitializeSaveNotes(btnValue){
	let blnstatus = true
	var type = '';
	if(btnValue == 'SaveandNew' || btnValue == 'SaveandClose'){
		type = 'add';
	}
	else{
		type = 'update';
	}

	let id_data = $('#form-notes').find(":input:not(:hidden)").serializeArray();
	let _validate = true;

	$(id_data).each(function(index){
		if(id_data[index].value == ""){
			console.log(id_data[index]);
			_validate = false;
			blnstatus = false;
		}
	});

	hidden_data = $('#form-notes').find(':input[type="hidden"]').serializeArray()
	id_data = id_data.concat(hidden_data);

	console.log(_validate)
	if(!_validate){
		swal("Information", "Complete all fields to proceed", "info");
		blnstatus = false;
	}
	else{
		swal({
	            title: "Are you sure?",
	            text: type == 'add' ? "This will save the record?" : "This will update the record?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: true,
	        }, function () {
	        				
		        	console.log(id_data,"Notes Data");
		        	$.ajax({
						type: 'POST',
						dataType: 'json',
						url: global.site_name + 'administration/master_file/employee_profile/saveNotesRecord',
						data : id_data,
						success : function(result){
							if(!result.success){
								swal(result.title, result.message, result.type);
								blnstatus = false;
							}
							else{
								console.log(btnValue,"notes saving event");
								if(btnValue == 'SaveandNew'){
									$('#detail-template-notes input').val('');
									$('#detail-template-notes textarea').val('');
									$('#detail-template-notes [name="mode"]').val('add');
									$('#detail-template-notes [name="EmpN_Id"]').val(general_info_data['Emp_Id']);
								}
								else{
									$('#detail-template-notes input').val('');
									$('#detail-template-notes textarea').val('');
									$('#detail-template-notes .close').trigger('click');									
								}
								tbl_Notes.load().draw();

							}
						},error : function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
					// end of saving POST
				}
	   );
	}
	

	return blnstatus;
}

function InitializeLeaveBalance(mode){

	$('[name="PKFK_LeaveType"]').select2({
    	ajax: {
			url: global.site_name + 'administration/master_file/employee_profile/getFilterData',
			dataType: 'json',
			delay: 250,
			data: function (params) {
			  var queryParameters = {
					'filter-search'	: general_info_data['Emp_Id'],
					'filter-type'	: 'leavetype',
					'filter-addtl'	: '',
					'filter-input'	: params.term
			  }
			  return queryParameters;
			},
			processResults: function (data, params) {
				return {
					results: $.map(data, function(item) {
			            return {
			                id: item.Id,
			                text: item.Description
			            }
			        })
				};
			}
		},
		// dropdownParent: $('#detail-template-leave-balance')
		minimumResultsForSearch : -1
    });
	
	if(mode == 'add'){
		$('#detail-template-leave-balance input').val('');
		$('#detail-template-leave-balance select').select2("val",'');
		$('#detail-template-leave-balance [name="mode"]').val('add');
		$('[name="PKFK_LeaveType"]').select2('enable',true);
	}
	else{
		$('#detail-template-leave-balance [name="mode"]').val('update');
		$('[name="PKFK_LeaveType"]').select2('enable',false);
		$('#detail-template-leave-balance [name="PKFK_LeaveType"]').html('<option value="' + leave_balance_data['PKFK_LeaveType']+ '" selected ="selected">' + leave_balance_data['AD_Desc'] + ' </option>');
		$('#detail-template-leave-balance [name="PKFK_LeaveType"]').select2("val", leave_balance_data['PKFK_LeaveType']);
		$('#detail-template-leave-balance [name="LT_Balance"]').val(number_format(leave_balance_data['LT_Balance'],2,".",""));
	}
	$('#detail-template-leave-balance [name="PKFK_Empid"]').val(general_info_data['Emp_Id']);

	$('body').on('click', '#detail-template-leave-balance #btnLeaveBalSaveandNew', function(e){
		console.log($(this).attr('id'));
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveLeaveBal(btnValue);

	})

	$('body').on('click', '#detail-template-leave-balance #btnLeaveBalSaveandClose', function(e){
		console.log($(this).attr('id'));
		e.preventDefault();
		btnValue = $(this).val();
		InitializeSaveLeaveBal(btnValue);
	})

}

function InitializeSaveLeaveBal(btnValue){
	let blnstatus = true
	var type = '';
	if(btnValue == 'SaveandNew' || btnValue == 'SaveandClose'){
		type = 'add';
	}
	else{
		type = 'update';
	}

	let id_data = $('#form-leave-balance').find(":input:not(:hidden),select").serializeArray();
	let _validate = true;

	$(id_data).each(function(index){
		if(id_data[index].value == ""){
			console.log(id_data[index]);
			_validate = false;
			blnstatus = false;
		}
	});

	hidden_data = $('#form-leave-balance').find(':input[type="hidden"]').serializeArray();
	id_data = id_data.concat(hidden_data);

	if($('#detail-template-leave-balance [name="mode"]').val() == 'update'){
		id_data = id_data.concat({'name':'PKFK_LeaveType','value':$('[name="PKFK_LeaveType"]').val()});		
	}

	console.log(_validate)
	if(!_validate){
		swal("Information", "Complete all fields to proceed", "info");
		blnstatus = false;
	}
	else{
		swal({
	            title: "Are you sure?",
	            text: type == 'add' ? "This will save the record?" : "This will update the record?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#1AB394",
	            confirmButtonText: "Yes",
	            cancelButtonText: "No",
	            closeOnConfirm: true,
	        }, function () {
	        				
		        	console.log(id_data,"Leave Balance Data");
		        	$.ajax({
						type: 'POST',
						dataType: 'json',
						url: global.site_name + 'administration/master_file/employee_profile/saveLeaveBalanceRecord',
						data : id_data,
						success : function(result){
							if(!result.success){
								swal(result.title, result.message, result.type);
								blnstatus = false;
							}
							else{
								console.log(btnValue,"Leave Balance saving event");
								if(btnValue == 'SaveandNew'){
									$('#detail-template-leave-balance input').val('');
									$('#detail-template-leave-balance select').select2("val",'');
									$('#detail-template-leave-balance [name="mode"]').val('add');
									$('#detail-template-leave-balance [name="PKFK_Empid"]').val(general_info_data['Emp_Id']);
								}
								else{
									$('#detail-template-leave-balance input').val('');
									$('#detail-template-leave-balance .close').trigger('click');									
								}
								tbl_LeaveType.load().draw();

							}
						},error : function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
					// end of saving POST
				}
	   );
	}
	

	return blnstatus;
}