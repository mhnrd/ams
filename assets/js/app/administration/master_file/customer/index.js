$(document).ready(function(){

	$('#btnActive').prop('disabled', true);
	$('#btnDeactive').prop('disabled', true);
	
	// Datatable Setup
	tableindex = $('#customer_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/customer/data',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0, 1],
			"orderable": false,
		},{
			"targets": [0,1,5],
			"className": 'text-center'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'	});

	$('#'+table_id+'_paginate').addClass('pull-right');

});
