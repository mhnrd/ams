$(document).ready(function(){

	if(output_type == 'view'){
		$('[name="C_DocNo"]').prop('readonly', true);
		$('[name="C_Name"]').prop('readonly', true);
		$('[name="C_CompanyName"]').prop('readonly', true);
		$('[name="C_Address1"]').prop('readonly', true);
		$('[name="C_City"]').prop('readonly', true);
		$('[name="C_FaxNum"]').prop('readonly', true);
		$('[name="C_PostalCode"]').prop('readonly', true);
		$('[name="C_Country"]').prop('readonly', true);
		
		$('#supplieracct').prop('readonly', true);
		$('[name="C_FK_CustomerType"]').prop('readonly', true);
		$('[name="C_FK_PayTerms"]').prop('readonly', true);
		$('[name="C_BillTo"]').prop('readonly', true);
		$('[name="C_BillDate"]').prop('disabled', true);
		$('[name="C_FK_Attribute_Currency_id"]').prop('disabled', true);
		// $('[name="C_BillDate"]').prop("style", " background: #E9ECEF;-webkit-appearance: none;pointer-events:none").removeClass('js-select2-bill');
		// $('[name="C_FK_Attribute_Currency_id"]').prop("style", "-webkit-appearance: none;pointer-events:none;background: #E9ECEF").removeClass('js-select2-currency');

		$('[name="C_CustomerPostingGroup"]').prop('readonly', true);
		$('[name="C_VATPostingGroup"]').prop('readonly', true);
		$('[name="C_WHTPostingGroup"]').prop('readonly', true);

		$('[name="C_ContactName"]').prop('readonly', true);
		$('[name="C_ContactTitle"]').prop('readonly', true);
		$('[name="C_TelNum"]').prop('readonly', true);
		$('[name="C_Email"]').prop('readonly', true);
		$('#bankaccount').prop('readonly', true);
		$('[name="C_BankName"]').prop('readonly', true);
		$('[name="C_BankAddress"]').prop('readonly', true);
		$('[name="C_CreditLimit"]').prop('readonly', true);
		$('[name="C_TIN_No"]').prop('readonly', true);
	}

	function init(){

	if(output_type == 'update'){
		$('[name="C_ID"]').prop('readonly', true);
	}

		$('#supplieracct').select2({
			placeholder: '',
			allowClear: true
		});

		$('#cust_type').select2({
			placeholder: '',
			allowClear: true
		});
		$('#payment_terms').select2({
			placeholder: ''
		});

		$('#bill_to').select2({
			placeholder: '',
			allowClear: true
		});

		$('.js-select2-bill').select2({
			placeholder: '',
			allowClear: true
		});

		$('.js-select2-currency').select2({
			placeholder: '',
			allowClear: true
		});

		$('#cust_posting').select2({
			placeholder: '',
			allowClear: true
		});

		$('#WHTPG').select2({
			placeholder: '',
			allowClear: true
		});

		$('#VATPG').select2({
			placeholder: '',
			allowClear: true
		});

		$('.datepicker').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: false,
				autoclose: true,
				format: "mm/dd/yyyy"
		});	

		$('[name="C_CreditLimit"]').keyup(function(){
			$('[name="C_CreditBalance"]').val($(this).val());
		});
	}
	
	init();
});