$(document).ready(function(){

	function init(){

		if(output_type == 'view'){
		$('[name="SP_ID"]').prop('readonly', true);
		$('[name="SP_StoreName"]').prop('readonly', true);
		$('[name="SP_Address"]').prop('readonly', true);
		$('[name="SP_TelNo"]').prop('readonly', true);
		$('[name="SP_FaxNo"]').prop('readonly', true);
		$('[name="SP_TinNo"]').prop('readonly', true);

		$('[name="CPC_Desc"]').prop('readonly', true);
		//$('[name="LOC_FK_Company_id"]').prop("style", "pointer-events:none;-webkit-appearance: none;background: #E9ECEF; color:black").removeClass('js-select2-comname');
		$('[name="SP_FK_CompanyID"]').prop('disabled', true);
		
		
	}

	if(output_type == 'update'){
		$('[name="LOC_Id"]').prop('readonly', true);
		
	}
	

	$('.js-select2-comname').select2({
			placeholder: '',
			allowClear: true,
		});

	$('#CPC').select2({
			placeholder: '',
			allowClear: true
		});
	}

	$(function(){
      $('[name="LOC_TelNum"]').keyup(function(){
        var input_val = $(this).val();
        var inputRGEX = /^[0-9]*$/;
        var inputResult = inputRGEX.test(input_val);
          if(!(inputResult))
          {     
            this.value = this.value.replace(/[^-0-9-/()]/gi, '');
          }
       });
    });

    $(function(){
      $('[name="LOC_FaxNum"]').keyup(function(){
        var input_val = $(this).val();
        var inputRGEX = /^[0-9]*$/;
        var inputResult = inputRGEX.test(input_val);
          if(!(inputResult))
          {     
            this.value = this.value.replace(/[^-0-9-/()]/gi, '');
          }
       });
    });
		
	init();	
});