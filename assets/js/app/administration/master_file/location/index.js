$(document).ready(function(){

	$('#btnActive').prop('disabled', true);
	$('#btnDeactive').prop('disabled', true);

	// Datatable Setup
	tableindex = $('#location_details').DataTable( {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/location/data',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0, 1],
			"orderable": false,
		},{
			"targets": [0,1,7],
			"className": 'text-center'
		},{
			"targets": [4],
			"className": 'text-left'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	$('#'+table_id+'_paginate').addClass('pull-right');

 });

