$(document).ready(function(){
	$('.js-select2').select2();
	// form view setup

	if(output_type == 'view'){
		$('[name = "CPC_Id"]').attr('readonly', true);
		$('[name= "CPC_Desc"]').attr('readonly', true);
		$('[name= "CPC_FK_Class"]').prop("disabled",true);
	}
	if (output_type == 'update') {
		$('[name="CPC_Id"]').attr('readonly', true);
	}

	$('.cpcclass-js-select2').select2({
		placeholder: '',
		allowClear: true

	});
});