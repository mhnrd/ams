$(document).ready(function(){

	function init(){

		initExtension();
		initDetailTable();
		tableButtons();
		addButtons();
		clickPriceEvent();
		inputPriceEvent();
		dropdownEvents();

		$('[name="BUS_BU_ID"]').change(function(){

			let bu_code = $(this).val();

			if(bu_code == ''){
				$('[name="BUS_ID"]').val('');
			}
			else{
				$.ajax({
					url 		: global.site_name + module_folder + '/' + module_controller + '/getCurrentBUS_ID',
					type 		: 'POST',
					dataType 	: 'json',
					data 		: {
						'bu-code' 	: bu_code
					},
					success 	: function(result){
						$('[name="BUS_ID"]').val(result);
					}
				})
			}


		})

		if(output_type == 'view'){
			$('#form-header input').attr('readonly', true);
			$('.js-select2').prop('disabled', true);
		}
	}

	init();

});

function tableButtons(){
	$('.add-detail').click(function(){

		action = 'add';

		actionButtons(action);
		
		$('[name="BUSD_MDPrice"], [name="BUSD_RegPrice"], [name="BUSD_PriceOffPerc"]').val('0.00');
		$('[name="BUSD_ItemCategory"], [name="BUSD_ItemSeason"], [name="BUSD_ItemGender"], [name="BUSD_ItemDept"]').val('').trigger('change');
		$('#detail-template').modal('show');


	});

	$('#tbl-detail').on('click', '.delete-button', function(){
		let current_row = $(this).closest('.tbl-detail');
		
		swal({
			title 				: "Delete this row?",
			type 				: "warning",
			showCancelButton	: true,
			cancelButtonText 	: "No",
			cancelButtonColor 	: '#DD6B55',
			confirmButtonColor 	: '#1AB394',
			confirmButtonText 	: "Yes",
			closeOnConfirm 		: true
		}, function(isConfirm){
			if(isConfirm){
				$.ajax({
					url 		: global.site_name + 'administration/master_file/business_unit_sku/delete',
					dataType	: 'json',
					type 		: 'POST',
					data 		: {
						docno 		: doc_no,
						bu_code 	: $('[name="BUS_BU_ID"]').val(),
						category 	: current_row.find('td:eq(1)').text(),
						season 		: current_row.find('td:eq(2)').text(),
						gender 		: current_row.find('td:eq(3)').text(),
						dept 		: current_row.find('td:eq(4)').text(),
						type 		: current_row.find('td:eq(5)').text()
					},
					success 	: function(result){
						if(result.success){
							tabledetail.ajax.reload();
							table_subdetail.ajax.reload();
							swal('Success!', 'Deleted successfully', 'success');
						}
						else{
							tabledetail.ajax.reload();
							table_subdetail.ajax.reload();
							swal(result.title, result.message, result.type);
						}
					},
					error 		: function(jqXHR){
						tabledetail.ajax.reload();
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
 				})
			}
		})
	});
}

function initExtension(){
	$('.datepicker').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: false,
			autoclose: true,
			format: "mm/dd/yyyy"
	});

	if(output_type == 'add'){
		$('[name="BUS_BU_ID"]').select2({
			placeholder: 	"",
			allowClear: true
		})
	}
	if(output_type != 'view'){

		$('[name="BUSD_ItemCategory[]"]').select2({
			placeholder: 	"",
			minimumInputLength: 1,
			ajax: {
				url: global.site_name + 'administration/master_file/business_unit_sku/getDropdownList',
				dataType: 'json',
				delay: 250,
				data: function (params) {
				  var queryParameters = {
						'filter-cols'		: 'RTRIM(Category_Code) as ID, RTRIM(Category_Code) as Description',
						'filter-table'		: 'Retailflex.dbo.tblCategory',
						'filter-like' 		: ['Category_Code'],
						'filter-input'		: params.term
				  }
				  return queryParameters;
				},
				processResults: function (data, params) {
					return {
						results: $.map(data, function(item) {
				            return {
				                id: item.ID,
				                text: item.Description
				            }
				        })
					};
				}
			}
		});

		$('[name="BUSD_ItemSeason[]"], [name="BUSD_ItemGender[]"], [name="BUSD_ItemDept[]"], [name="BUSD_ItemType[]"], [name="BUSD_PricePoint[]"], [name="BUSSD_ItemSize[]"]').select2({
			placeholder: 	""
		});

	}


	$('[name="BUS_SellUOMCode"], [name="BUS_BuyingUOMCode"]').select2({
		placeholder: 	"",
		allowClear: 	true
	});

	$('#chkPricePoint').click(function(event) {
		if($(this).is(':checked')){
			$('[name="BUSD_PricePoint[]"]').val('').trigger('change');
			$('[name="BUSD_ItemCategory[]"], [name="BUSD_ItemSeason[]"], [name="BUSD_ItemGender[]"], [name="BUSD_ItemDept[]"], [name="BUSD_ItemType[]"]').val('').trigger('change');
			$('[name="BUSD_ItemSeason[]"], [name="BUSD_ItemGender[]"], [name="BUSD_ItemDept[]"], [name="BUSD_ItemType[]"]').empty();
			$('[name="BUSD_ItemSeason[]"], [name="BUSD_ItemGender[]"], [name="BUSD_ItemDept[]"], [name="BUSD_ItemType[]"]').prop('disabled', false);
			$('[name="BUSD_ItemCategory[]"]').prop('disabled', true);
			$('.toggle-select').show();
		}
		else{
			$('[name="BUSD_PricePoint[]"]').val('').trigger('change');
			$('[name="BUSD_ItemCategory[]"], [name="BUSD_ItemSeason[]"], [name="BUSD_ItemGender[]"], [name="BUSD_ItemDept[]"], [name="BUSD_ItemType[]"]').val('').trigger('change');
			$('[name="BUSD_ItemSeason[]"], [name="BUSD_ItemGender[]"], [name="BUSD_ItemDept[]"], [name="BUSD_ItemType[]"]').empty();
			$('[name="BUSD_ItemCategory[]"]').prop('disabled', false);
			$('[name="BUSD_ItemSeason[]"], [name="BUSD_ItemGender[]"], [name="BUSD_ItemDept[]"], [name="BUSD_ItemType[]"]').prop('disabled', true);
			$('.toggle-select').hide();
		}
	});

}

function dropdownEvents(){
	// Item Season Dropdown
	// If Price Point is checked
	$('[name="BUSD_PricePoint[]"]').change(function(event) {
		console.log($('[name="BUSD_PricePoint[]"]').val().length);
		if($('[name="BUSD_PricePoint[]"]').val().length != 0){
			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/getSeasonListByPricePoint',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
					'prc_point' 		: $('[name="BUSD_PricePoint[]"]').val()
				},
				success 	: function(result){
					let opt = '';
					$('[name="BUSD_ItemSeason[]"]').empty();
					opt += '<option value=""></option>'
					$(result).each(function(index, el) {
						opt += '<option value="'+result[index].FK_ProdSeason+'">'+result[index].FK_ProdSeason+'</option>';	
					});

					$('[name="BUSD_ItemSeason[]"]').append(opt);
				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})

			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/getSizeListByPricePoint',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
					'prc_point' 		: $('[name="BUSD_PricePoint[]"]').val()
				},
				success 	: function(result){
					let opt = '';
					$('[name="BUSSD_ItemSize[]"]').empty();
					opt += '<option value=""></option>'
					$(result).each(function(index, el) {
						opt += '<option value="'+result[index].FK_Size_Code+'">'+result[index].FK_Size_Code+'</option>';	
					});

					$('[name="BUSSD_ItemSize[]"]').append(opt);
				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		}
		else{
			$('[name="BUSD_ItemSeason[]"]').empty();
			$('[name="BUSD_ItemSeason[]"]').val('').trigger('change');
			$('[name="BUSSD_ItemSize[]"]').empty();
			$('[name="BUSSD_ItemSize[]"]').val('').trigger('change');
		}
	});

	// If Price Point is not checked
	$('[name="BUSD_ItemCategory[]"]').change(function(event) {
		console.log($('[name="BUSD_ItemCategory[]"]').val().length);
		if($('[name="BUSD_ItemCategory[]"]').val().length != 0){
			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/getCategoryDetails',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
					'category' 		: $('[name="BUSD_ItemCategory[]"]').val()
				},
				success 	: function(result){
					let opt_season = '';
					let opt_gender = '';
					let opt_dept = '';
					let opt_type = '';
					$('[name="BUSD_ItemSeason[]"]').empty();
					$('[name="BUSD_ItemGender[]"]').empty();
					$('[name="BUSD_ItemDept[]"]').empty();
					$('[name="BUSD_ItemType[]"]').empty();
					
					opt_season += '<option value=""></option>'
					opt_gender += '<option value=""></option>'
					opt_dept += '<option value=""></option>'
					opt_type += '<option value=""></option>'
					
					$(result.season).each(function(index, el) {
						opt_season += '<option value="'+result.season[index].ID+'" selected>'+result.season[index].ID+'</option>';	
					});

					$(result.gender).each(function(index, el) {
						opt_gender += '<option value="'+result.gender[index].ID+'" selected>'+result.gender[index].ID+'</option>';	
					});

					$(result.dept).each(function(index, el) {
						opt_dept += '<option value="'+result.dept[index].ID+'" selected>'+result.dept[index].ID+'</option>';	
					});

					$(result.type).each(function(index, el) {
						opt_type += '<option value="'+result.type[index].ID+'" selected>'+result.type[index].ID+'</option>';	
					});

					$('[name="BUSD_ItemSeason[]"]').append(opt_season).trigger('change');
					$('[name="BUSD_ItemGender[]"]').append(opt_gender).trigger('change');
					$('[name="BUSD_ItemDept[]"]').append(opt_dept).trigger('change');
					$('[name="BUSD_ItemType[]"]').append(opt_type).trigger('change');
				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})

			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/getSizeList',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
					'category' 		: $('[name="BUSD_ItemCategory[]"]').val()
				},
				success 	: function(result){
					let opt = '';
					$('[name="BUSSD_ItemSize[]"]').empty();
					opt += '<option value=""></option>'
					$(result).each(function(index, el) {
						opt += '<option value="'+result[index].FK_Size_Code+'">'+result[index].FK_Size_Code+'</option>';	
					});

					$('[name="BUSSD_ItemSize[]"]').append(opt);
				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		}
		else{
			$('[name="BUSD_ItemSeason[]"], [name="BUSD_ItemGender[]"], [name="BUSD_ItemDept[]"], [name="BUSD_ItemType[]"]').empty();
			$('[name="BUSD_ItemSeason[]"], [name="BUSD_ItemGender[]"], [name="BUSD_ItemDept[]"], [name="BUSD_ItemType[]"]').val('').trigger('change');
			$('[name="BUSSD_ItemSize[]"]').empty();
			$('[name="BUSSD_ItemSize[]"]').val('').trigger('change');
		}
	});

	// Item Gender Dropdown
	$('[name="BUSD_ItemSeason[]"]').change(function(event) {
		// Check Price point if length <> 0
		if($('[name="BUSD_PricePoint[]"]').val().length > 0){
			if($('[name="BUSD_ItemSeason[]"]').val().length != 0){
				$.ajax({
					url 		: global.site_name + 'administration/master_file/business_unit_sku/getGenderList',
					dataType 	: 'json',
					type 		: 'POST',
					data 		: {
						'category' 		: $('[name="BUSD_ItemCategory[]"]').val(),
						'season' 		: $('[name="BUSD_ItemSeason[]"]').val(),
						'prc_point' 	: $('[name="BUSD_PricePoint[]"]').val()
					},
					success 	: function(result){
						let opt = '';
						$('[name="BUSD_ItemGender[]"]').empty();
						opt += '<option value=""></option>'
						$(result).each(function(index, el) {
							opt += '<option value="'+result[index].FK_ProdGender+'">'+result[index].FK_ProdGender+'</option>';	
						});

						$('[name="BUSD_ItemGender[]"]').append(opt);
					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				})
			}
			else{
				$('[name="BUSD_ItemGender[]"]').empty();
				$('[name="BUSD_ItemGender[]"]').val('').trigger('change');
			}

			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/getSizeListByPricePoint',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
					'prc_point' 		: $('[name="BUSD_PricePoint[]"]').val(),
					'sea_arr' 			: $('[name="BUSD_ItemSeason[]"]').val()
				},
				success 	: function(result){
					let opt = '';
					$('[name="BUSSD_ItemSize[]"]').empty();
					opt += '<option value=""></option>'
					$(result).each(function(index, el) {
						opt += '<option value="'+result[index].FK_Size_Code+'">'+result[index].FK_Size_Code+'</option>';	
					});

					$('[name="BUSSD_ItemSize[]"]').append(opt);
				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		}
	});

	// Item Dept Dropdown
	$('[name="BUSD_ItemGender[]"]').change(function(event) {
		if($('[name="BUSD_PricePoint[]"]').val().length > 0){
			if($('[name="BUSD_ItemGender[]"]').val().length != 0){
				$.ajax({
					url 		: global.site_name + 'administration/master_file/business_unit_sku/getDeptList',
					dataType 	: 'json',
					type 		: 'POST',
					data 		: {
						'category' 		: $('[name="BUSD_ItemCategory[]"]').val(),
						'season' 		: $('[name="BUSD_ItemSeason[]"]').val(),
						'gender' 		: $('[name="BUSD_ItemGender[]"]').val(),
						'prc_point' 	: $('[name="BUSD_PricePoint[]"]').val()
					},
					success 	: function(result){
						let opt = '';
						$('[name="BUSD_ItemDept[]"]').empty();
						opt += '<option value=""></option>'
						$(result).each(function(index, el) {
							opt += '<option value="'+result[index].FK_ProdDept+'">'+result[index].FK_ProdDept+'</option>';	
						});

						$('[name="BUSD_ItemDept[]"]').append(opt);
					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				})
			}
			else{
				$('[name="BUSD_ItemDept[]"]').empty();
				$('[name="BUSD_ItemDept[]"]').val('').trigger('change');
			}

			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/getSizeListByPricePoint',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
					'prc_point' 		: $('[name="BUSD_PricePoint[]"]').val(),
					'sea_arr' 			: $('[name="BUSD_ItemSeason[]"]').val(),
					'gen_arr' 			: $('[name="BUSD_ItemGender[]"]').val()
				},
				success 	: function(result){
					let opt = '';
					$('[name="BUSSD_ItemSize[]"]').empty();
					opt += '<option value=""></option>'
					$(result).each(function(index, el) {
						opt += '<option value="'+result[index].FK_Size_Code+'">'+result[index].FK_Size_Code+'</option>';	
					});

					$('[name="BUSSD_ItemSize[]"]').append(opt);
				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		}
	});

	// Item Type Dropdown
	$('[name="BUSD_ItemDept[]"]').change(function(event) {
		if($('[name="BUSD_PricePoint[]"]').val().length > 0){
			if($('[name="BUSD_ItemDept[]"]').val().length != 0){
				$.ajax({
					url 		: global.site_name + 'administration/master_file/business_unit_sku/getTypeList',
					dataType 	: 'json',
					type 		: 'POST',
					data 		: {
						'category' 		: $('[name="BUSD_ItemCategory[]"]').val(),
						'season' 		: $('[name="BUSD_ItemSeason[]"]').val(),
						'gender' 		: $('[name="BUSD_ItemGender[]"]').val(),
						'dept' 			: $('[name="BUSD_ItemDept[]"]').val(),
						'prc_point' 	: $('[name="BUSD_PricePoint[]"]').val()
					},
					success 	: function(result){
						let opt = '';
						$('[name="BUSD_ItemType[]"]').empty();
						opt += '<option value=""></option>'
						$(result).each(function(index, el) {
							opt += '<option value="'+result[index].FK_ProdType+'">'+result[index].FK_ProdType+'</option>';	
						});

						$('[name="BUSD_ItemType[]"]').append(opt);
					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				})
			}
			else{
				$('[name="BUSD_ItemType[]"]').empty();
				$('[name="BUSD_ItemType[]"]').val('').trigger('change');
			}

			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/getSizeListByPricePoint',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
					'prc_point' 		: $('[name="BUSD_PricePoint[]"]').val(),
					'sea_arr' 			: $('[name="BUSD_ItemSeason[]"]').val(),
					'gen_arr' 			: $('[name="BUSD_ItemGender[]"]').val(),
					'dept_arr' 			: $('[name="BUSD_ItemDept[]"]').val()
				},
				success 	: function(result){
					let opt = '';
					$('[name="BUSSD_ItemSize[]"]').empty();
					opt += '<option value=""></option>'
					$(result).each(function(index, el) {
						opt += '<option value="'+result[index].FK_Size_Code+'">'+result[index].FK_Size_Code+'</option>';	
					});

					$('[name="BUSSD_ItemSize[]"]').append(opt);
				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		}
	});

	// Change Item Type
	$('[name="BUSD_ItemType"]').change(function(event) {
		// if($('[name="BUSD_PricePoint[]"]').val().length > 0){
			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/getSizeListByPricePoint',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
					'prc_point' 		: $('[name="BUSD_PricePoint[]"]').val(),
					'sea_arr' 			: $('[name="BUSD_ItemSeason[]"]').val(),
					'gen_arr' 			: $('[name="BUSD_ItemGender[]"]').val(),
					'dept_arr' 			: $('[name="BUSD_ItemDept[]"]').val(),
					'typ_arr' 			: $('[name="BUSD_ItemType[]"]').val()
				},
				success 	: function(result){
					let opt = '';
					$('[name="BUSSD_ItemSize[]"]').empty();
					opt += '<option value=""></option>'
					$(result).each(function(index, el) {
						opt += '<option value="'+result[index].FK_Size_Code+'">'+result[index].FK_Size_Code+'</option>';	
					});

					$('[name="BUSSD_ItemSize[]"]').append(opt);
				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		// }
	});

	
}

function clickPriceEvent(){
	// Marked Down Price Button
	$('#tbl-detail').on('click', '.md-price-btn', function(event) {
		let price = parseFloat($(this).text());
		$(this).replaceWith('<input type="number" class="form-control text-right md-price-input" value="'+price+'" />');
	});

	$('#tbl-sub-detail').on('click', '.md-price-btn', function(event) {
		let price = parseFloat($(this).text());
		$(this).replaceWith('<input type="number" class="form-control text-right md-price-input" value="'+price+'" />');
	});

	// Regular Price Button
	$('#tbl-detail').on('click', '.reg-price-btn', function(event) {
		let price = parseFloat($(this).text());
		$(this).replaceWith('<input type="number" class="form-control text-right reg-price-input" value="'+price+'" />');
	});

	$('#tbl-sub-detail').on('click', '.reg-price-btn', function(event) {
		let price = parseFloat($(this).text());
		$(this).replaceWith('<input type="number" class="form-control text-right reg-price-input" value="'+price+'" />');
	});

	// Price % Off Button
	$('#tbl-detail').on('click', '.prc-off-btn', function(event) {
		let price = parseFloat($(this).text());
		$(this).replaceWith('<input type="number" class="form-control text-right prc-off-input" value="'+price+'" />');
	});

	$('#tbl-sub-detail').on('click', '.prc-off-btn', function(event) {
		let price = parseFloat($(this).text());
		$(this).replaceWith('<input type="number" class="form-control text-right prc-off-input" value="'+price+'" />');
	});
}

function inputPriceEvent(){
	// Marked Down Price update
	// Detail
	$('#tbl-detail').on('keypress', '.md-price-input', function(event) {
		let current_row = $(this).closest('.tbl-detail');
		let current_input = $(this);
		let prc = current_row.find('td:eq(7)').text();
		let prc_point = prc.replace(',', '');

		if(event.keyCode == 13){
			let price = number_format(parseFloat( $(this).val() ), 2, ".", "");
			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/updatePrice',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
					docno 		: doc_no,
					bu_code 	: $('[name="BUS_BU_ID"]').val(),
					price 		: current_input.val(),
					category 	: current_row.find('td:eq(1)').text(),
					season 		: current_row.find('td:eq(2)').text(),
					gender 		: current_row.find('td:eq(3)').text(),
					dept 		: current_row.find('td:eq(4)').text(),
					type 		: current_row.find('td:eq(5)').text(),
					prc_point 	: number_format(prc_point, 2, ".", ""),
					priceType 	: 'MD'
				},
				success 	: function(result){
					if(result.success){
						tabledetail.ajax.reload();
					}
					else{
						swal(result.title, result.message, result.type);
						tabledetail.ajax.reload();
					}
				},
				error 		: function(jqXHR){
					tabledetail.ajax.reload();
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		}
	});

	// Sub-detail
	$('#tbl-sub-detail').on('keypress', '.md-price-input', function(event) {
		let current_row = $(this).closest('.tbl-sub-detail');
		let current_input = $(this);
		if(event.keyCode == 13){
			let price = number_format(parseFloat( $(this).val() ), 2, ".", "");
			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/updatePriceItem',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
					docno 		: doc_no,
					bu_code 	: $('[name="BUS_BU_ID"]').val(),
					price 		: current_input.val(),
					category 	: current_row.find('td:eq(0)').text(),
					item 		: current_row.find('td:eq(1)').text(),
					priceType 	: 'MD'
				},
				success 	: function(result){
					if(result.success){
						table_subdetail.ajax.reload();
					}
					else{
						swal(result.title, result.message, result.type);
						table_subdetail.ajax.reload();
					}
				},
				error 		: function(jqXHR){
					table_subdetail.ajax.reload();
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		}
	});


	// Price % Off
	// Detail
	$('#tbl-detail').on('keypress', '.prc-off-input', function(event) {
		let current_row 	= $(this).closest('.tbl-detail');
		let current_input 	= $(this);
		let prc = current_row.find('td:eq(7)').text();
		let prc_point = prc.replace(',', '');

		if(event.keyCode == 13){
			let price = number_format(parseFloat( $(this).val() ), 2, ".", "");
			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/updatePrice',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
					docno 		: doc_no,
					bu_code 	: $('[name="BUS_BU_ID"]').val(),
					price 		: current_input.val(),
					category 	: current_row.find('td:eq(1)').text(),
					season 		: current_row.find('td:eq(2)').text(),
					gender 		: current_row.find('td:eq(3)').text(),
					dept 		: current_row.find('td:eq(4)').text(),
					type 		: current_row.find('td:eq(5)').text(),
					prc_point 	: number_format(prc_point, 2, ".", ""),
					priceType 	: 'POFF'
				},
				success 	: function(result){
					if(result.success){
						tabledetail.ajax.reload();
					}
					else{
						swal(result.title, result.message, result.type);
						tabledetail.ajax.reload();
					}
				},
				error 		: function(jqXHR){
					tabledetail.ajax.reload();
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		}
	});

	// Sub-detail
	$('#tbl-sub-detail').on('keypress', '.prc-off-input', function(event) {
		let current_row 	= $(this).closest('.tbl-sub-detail');
		let current_input 	= $(this);
		if(event.keyCode == 13){
			let price = number_format(parseFloat( $(this).val() ), 2, ".", "");
			$.ajax({
				url 		: global.site_name + 'administration/master_file/business_unit_sku/updatePriceItem',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
					docno 		: doc_no,
					bu_code 	: $('[name="BUS_BU_ID"]').val(),
					price 		: current_input.val(),
					category 	: current_row.find('td:eq(0)').text(),
					item 		: current_row.find('td:eq(1)').text(),
					priceType 	: 'POFF'
				},
				success 	: function(result){
					if(result.success){
						table_subdetail.ajax.reload();
					}
					else{
						swal(result.title, result.message, result.type);
						table_subdetail.ajax.reload();
					}
				},
				error 		: function(jqXHR){
					table_subdetail.ajax.reload();
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		}
	});
}


function initDetailTable(){

	if(output_type != 'add'){
		let orderable = (output_type == 'update' ? [0] : []);
		let text_right = (output_type == 'update' ? [6,7,8] : [5,6,7]);
		let text_center = (output_type == 'update' ? [0] : []);

		tabledetail = $('#tbl-detail').DataTable( {
			"processing":true,
			"serverSide":true,
			"ajax": {
				url 	: global.site_name + 'administration/master_file/business_unit_sku/data_detail?id='+doc_no+'&type='+output_type,
				type 	: 'POST'
			},
			"deferRender": true,
			"columnDefs": [{
				"targets": orderable,
				"orderable": false,
			},{
				"targets": text_right,
				"className": 'text-right'
			},{
				"targets": text_center,
				"className": 'text-center'
			}],
			responsive: true,
			createdRow:function(row){
				$(row).addClass('tbl-detail');
			},
			"order": [[ 1, "desc" ]],
			"dom": 'lT<"dt-toolbar">fgtip'
		});

		table_subdetail = $('#tbl-sub-detail').DataTable( {
			"processing":true,
			"serverSide":true,
			"ajax": {
				url 	: global.site_name + 'administration/master_file/business_unit_sku/data_subdetail?id='+doc_no+'&type='+output_type,
				type 	: 'POST'
			},
			"deferRender": true,
			"columnDefs": [{
				"targets":[0],
				"orderable": false,
			},{
				"targets": [2,3,4],
				"className": 'text-right'
			},{
				"targets": [0],
				"className": 'text-center'
			}],
			responsive: true,
			createdRow:function(row){
				$(row).addClass('tbl-sub-detail');
			},
			"order": [[ 0, "desc" ]],
			"dom": 'lT<"dt-toolbar">fgtip'
		});
	}

}

function actionButtons(action){

	if(action == 'add'){
		$('#add-close').text('Add & Close');
		$('#add-new').show();
	}
	else{
		$('#add-close').text('Update');
		$('#add-new').hide();
	}

}

function addButtons(){

	$('#add-close').click(function(){

		if(validate_details()){

			createDetailArray();
		}
	});

}

function validate_details(){
	let isComplete = true;
	let requiredField = $('#detail-template :input[required]:visible');

	$(requiredField).each(function(index, el) {
		if($(this).val() == "" || $(this).val() == []){
			console.log($(this).is(':disabled'));
			if(!$(this).is(':disabled')){
				isComplete = false;
			}
		}	
	});

	return isComplete;

}

function createDetailArray(){
	let bu_code 		= $('[name="BUS_BU_ID"]').val();
	let item_cat 		= $('[name="BUSD_ItemCategory[]"]').val();
	let item_season 	= $('[name="BUSD_ItemSeason[]"]').val();
	let item_gender 	= $('[name="BUSD_ItemGender[]"]').val();
	let item_dept 		= $('[name="BUSD_ItemDept[]"]').val();
	let item_type 		= $('[name="BUSD_ItemType[]"]').val();
	let item_size 		= $('[name="BUSSD_ItemSize[]"]').val();
	let price_point 	= $('[name="BUSD_PricePoint[]"]').val();
	let md_price 		= $('[name="BUSD_MDPrice"]').val();

	swal({title: 'Please Wait...', text: 'Generating details', type: 'info', showConfirmButton: false});

	$.ajax({
		url 		: global.site_name + 'administration/master_file/business_unit_sku/saveSKUDetails',
		dataType 	: 'json',
		type 		: 'POST',
		data 		: {
			docno 		: doc_no,
			code 		: bu_code,
			cat_arr 	: item_cat,
			sea_arr 	: item_season,
			gen_arr 	: item_gender,
			dept_arr 	: item_dept,
			typ_arr 	: item_type,
			siz_arr 	: item_size,
			md_price 	: md_price,
			prc_point 	: price_point
		},
		success : function(){
			setTimeout(function(){
				location.reload(true);
			}, 1500);
		},
		error : function(jqXHR){
			swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
		}
	})

}