$(document).ready(function(){

	InitHeaderTable();
	initPrintBarcode();

});

function InitHeaderTable(){

	// Datatable Setup
	tableindex = $('#'+table_id).DataTable( {
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url: global.site_name + 'administration/master_file/business_unit_sku/data',
			type: "POST"
		},
		"deferRender": true,
		"columnDefs": [{
			"targets":[0],
			"orderable": false,
		},{
			"targets": col_center,
			"className": 'text-center'
		},{
			"targets": [11,12,13],
			"className": 'text-right'
		}],
		pageLength: 10,
		responsive: true,
		createdRow:function(row){
			$(row).addClass('table-header');
		},
		"dom": 'lT<"dt-toolbar">fgtip'
	});

	$('#'+table_id+'_paginate').addClass('pull-right');

	
}

function initPrintBarcode(){
	$('#'+table_id).on('click','#print-barcode',function(e){
		console.log(12345);
		
		let current_row = $(this).closest('.table-header');

		$('[name="BU_Code"]').val(current_row.find('[data-code]').data('code'));
		$('[name="BUSD_ItemCategory"]').val(current_row.find('[data-category]').data('category'));

		$.ajax({
			url 		: global.site_name + 'administration/master_file/business_unit_sku/getBSList',
			type 		: 'POST',
			dataType 	: 'json',
			data 		: {
							'bu-code' 	: current_row.find('[data-code]').data('code')
						  },
			success 	: function(result){
				$('[name="BU_Desc"]').empty();
				let option_desc = '';
				option_desc += '<option value=""></option>';

				$(result).each(function(index){
					option_desc += '<option value="'+result[index].BS_ID+'">'+result[index].BS_Description+'</option>'
				});

				$('[name="BU_Desc"]').append(option_desc);

				$('[name="BU_Desc"]').val('').trigger('change');
			}
 		});
 		
		$('#barcode-config').modal('show');   

		
	});

	$('[name="BU_Desc"], [name="BS_Desc"]').select2({
		placeholder: "Select Price Type",
		allowClear: true
	});

	$('#barcode-print').click(function(){

		let completeFields = validate_barcode();

		if(!completeFields){
			swal("Information", "Complete all required fields", "info");
		}
		else{
			var print_barcode = {
							    state : function(current_state){
							        if(current_state == 'loading'){
							            $('#barcode-print').empty();
							            $('#barcode-print').append('<span class="fa fa-refresh fa-spin"></span> Printing...');
							            $('#barcode-print').attr('disabled', true);
							        }
							        if(current_state == 'click'){
							            $('#barcode-print').attr('disabled', false);
							            $('#barcode-print').empty();
							            $('#barcode-print').append('Print');
							        }
							    }
							};
		    print_barcode.state('loading');

		    $.ajax({
		    	url: global.site_name + 'administration/master_file/business_unit_sku/print_barcode',
		    	type: 'POST',
		    	dataType: 'json',
		    	data: {
		    			'bu-id' 		: $('[name="BU_Desc"]').val(),
		    			'bu-code' 		: $('[name="BU_Code"]').val(),
		    			'category' 		: $('[name="BUSD_ItemCategory"]').val(),
		    			'qty-to-print' 	: $('[name="BU_QtyToPrint"]').val() 
		    		  },
		    	success : function(result){
		    		if(!result.success){
		    			swal(result.title, result.message, result.type)
		    			print_barcode.state('click');
		    		}
		    		else{
			    		printJS(global.site_name + 'pdf/'+result.pdf_name);
			    		print_barcode.state('click');
			    		
		    		}
		    	},
		    	error 	: function(jqXHR){
		    		swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
		    		print_barcode.state('click');
		    	}
		    });	
		}

	});
}

function validate_barcode(){
	let isComplete = true;
	let requiredField = $('#form-barcode :input[required]:visible');

	$(requiredField).each(function(index, el) {
		if($(this).val() == ""){
			isComplete = false;
		}		
	});

	return isComplete;

}